Playback messages from the testbed's .metadata files over their corresponding 
topics in realtime, simulating the time between messages like when the testbed
ran the trial originally.

python Playback.py [Metadata File Name]

Mosquitto MQTT server must be installed on the computer
Python 3.6+
The only python dependency is paho-mqtt

POC: jsteigerwald@cra.com

