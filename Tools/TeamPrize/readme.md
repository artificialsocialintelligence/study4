# teamprize

**teamprize** is a Python script that selects a weekly winning team using the following criteria:

- Select a list of participants for a time period (defaults to previous week, Tuesday 12:01am - Monday 11:59 PM)
- Reduce list to the top 50% of teams
- From the reduced list pick one team at random as the winner    
- Return information on the 3 participants that were part of that winning team    
- Obtained from data in participant database. Trial table and Player table    
- Information about gameplay: time and score

## Prerequisites

This script was tested against Python version [3.11.3](https://www.python.org/downloads/release/python-3113/).

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install required packages (requirements.txt).

```bash
pip install -r requirements.txt
```

## Usage

**teamprize** needs a config.ini file or the setting of command line arguments in order to run. By default, the previous week (Tuesday 12:01am - Monday 11:59 PM). If other dates are desired use the `--start-date START_DATE` and `--end-date END_DATE` arguments (see below).

## config.ini

The default `config.ini` file location is the directory where `teamprize.py` is executed. If another location is desired use the `--file FILE` argument (see below).
The `[postgres]` section is only used for situations where direct connection to the database is required. If you wish to connect directly to postgres use the `-d, --direct` argument (see below)
The `[http]` section is the preferred way to connect to the database.
```ini
[postgres]
host = ...
port = ...
database = ...
user = ...
password = ...

[http]
token = ...
url-trials = ...
url-users = ...
```

## Command Line Arguments

If any of the `--pg-*` or `--http=*` are used they will override the corresponding `config.ini` settings for that option.
```text
usage: teamprize.py [-h] [-f FILE] [-d] [--start-date START_DATE]
                    [--end-date END_DATE] [--pg-host PG_HOST]
                    [--pg-port PG_PORT] [--pg-database PG_DATABASE]
                    [--pg-user PG_USER] [--pg-password PG_PASSWORD]
                    [--http-url-trials HTTP_URL_TRIALS]
                    [--http-url-users HTTP_URL_USERS]
                    [--http-token HTTP_TOKEN] [--version]

ASIST Team Prize script.

options:
  -h, --help            show this help message and exit
  -f FILE, --file FILE  configuration file location to use instead of the
                        default location
  -d, --direct          enabling direct mode flag will try and connect
                        directly to postgres
  --start-date START_DATE
                        override default start date YYYY-MM-DD
  --end-date END_DATE   override default end date YYYY-MM-DD
  --pg-host PG_HOST     postgres host address
  --pg-port PG_PORT     postgres connection port number
  --pg-database PG_DATABASE
                        postgres database name
  --pg-user PG_USER     postgres user name to authenticate
  --pg-password PG_PASSWORD
                        postgres password to authenticate
  --http-url-trials HTTP_URL_TRIALS
                        url to GET user data
  --http-url-users HTTP_URL_USERS
                        url to GET trial data
  --http-token HTTP_TOKEN
                        authentication token
  --version             show program's version number and exit
```

## License


