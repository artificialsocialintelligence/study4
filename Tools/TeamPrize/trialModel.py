from datetime import datetime
from typing import Any, Dict, List

from pydantic import BaseModel, RootModel, field_validator


class TrialModel(BaseModel):
    experimentId: str | None = ...
    experimentName: str | None = ...
    trialId: str | None = ...
    trialName: str | None = ...
    missionVariant: str | None = ...
    startTimestamp: datetime | str
    teamId: str | None = ...
    members: List[str] | None = ...
    asiCondition: str | None = ...
    teamScore: int | None = ...
    bombSummaryPlayer: Dict[str, Any] | None = None
    bombsTotal: int | None = ...
    bombsExploded: Dict[str, Any] | None = None
    damageTaken: Dict[str, Any] | None = None
    bombBeaconsPlaced: Dict[str, Any] | None = None
    commBeaconsPlaced: Dict[str, Any] | None = None
    timesFrozen: Any | None = None
    flagsPlaced: Dict[str, Any] | None = None
    teammatesRescued: Any | None = None
    budgetExpended: Dict[str, Any] | None = None
    firesExtinguished: Dict[str, Any] | None = None
    totalStoreTime: int | None = ...
    trialEndCondition: str | None = ...

    @field_validator('startTimestamp')
    def no_empty_string(cls, v):
        if v == '':
            # raise ValueError('startTimestamp can not be an empty string!')
            return datetime.min
        elif v == 'NOT_SET':
            # raise ValueError('startTimestamp can not be an empty string!')
            return datetime.min
        return datetime.fromisoformat(v).replace(tzinfo=None)


class Trials(RootModel):
    root: List[TrialModel]

    def __iter__(self):
        return iter(self.root)

    def __getitem__(self, item):
        return self.root[item]
