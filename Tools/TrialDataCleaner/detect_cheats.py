
import json, sys, traceback

def check3Wirecutters(lines)->bool:    

    for i in range(len(lines)):
                  
        # line to json
        json_line = json.loads(lines[i])
        
        if 'msg' in json_line:

            msg = json_line['msg']

            if 'sub_type' in msg:
                sub_type = msg['sub_type']
                #print(sub_type)
                if sub_type == 'Event:InventoryUpdate':
                    inv = json_line['data']['currInv']
                    for key in inv.keys():
                        playerInv=inv[key]
                        rwc=playerInv['WIRECUTTERS_RED']
                        gwc=playerInv['WIRECUTTERS_GREEN']
                        bwc=playerInv['WIRECUTTERS_BLUE']                        
                        if rwc>0 and gwc>0 and bwc>0:
                            print ( "WC CHEAT!")
                            print( ' WCS : ' +str(rwc)+ ',' +str(gwc)+ ',' +str(bwc) + ' : ' + str(i+1) )
                            return True
    return False


def checkAutoJump(lines)->bool:    

    for i in range(len(lines)):
                  
        # line to json
        json_line = json.loads(lines[i])
        
        if 'msg' in json_line:

            msg = json_line['msg']

            if 'sub_type' in msg:
                sub_type = msg['sub_type']
                #print(sub_type)
                if sub_type == 'Event:PlayerState':
                    x = json_line['data']['x']
                    y = json_line['data']['y']
                    z = json_line['data']['z']
                    
                    # NORMAL FOR CHURCH
                    # if x  > 18 and x < 37 :
                    #     if z > 69 and z < 82 :
                    #         if json_line['data']['y']  > 54 :
                    #             print( ' CHURCH : ' +str(x)+ ',' +str(y)+ ',' +str(z) + ' : ' + str(i+1) )     
                    #             #return = True
                    
                    if x  > 1 and x < 50 :
                        # JUNGLE
                        if z > 1 and z < 50 :
                            if json_line['data']['y']  >= 53 :
                                print( ' TOO HIGH JUNGLE : ' +str(x)+ ',' +str(y)+ ',' +str(z) + ' : ' + str(i+1) )     
                                return True
                        # DESERT
                        if z > 100 and z < 150 :
                            if json_line['data']['y']  >= 55 :
                                print( ' TOO HIGH DESERT : ' +str(x)+ ',' +str(y)+ ',' +str(z) + ' : ' + str(i+1) )     
                                return True
                        # VILLAGE
                        if z > 50 and z < 100 :
                            if x  < 18 or x > 37 :
                                if z < 69 or z > 82 :
                                    if json_line['data']['y']  >= 55 :
                                        print( ' TOO HIGH VILLAGE : ' +str(x)+ ',' +str(y)+ ',' +str(z) + ' : ' + str(i+1) )     
                                        return True
    return False
            

def checkPreReconPurchase(lines)->bool:

    preReconMessageEnecountered = False

    for i in range(len(lines)):
                  
        # line to json
        json_line = json.loads(lines[i])
        
        if 'msg' in json_line:

            msg = json_line['msg']

            if 'sub_type' in msg:
                sub_type = msg['sub_type']
                #print(sub_type)
                if sub_type == 'Event:MissionStageTransition':
                    if json_line['data']['mission_stage']  == 'RECON_STAGE':
                        print( "Recon Stage Transition found @ line : " + str(i+1) )     
                        preReconMessageEnecountered = True

            if 'sub_type' in msg:
                sub_type = msg['sub_type']
                #print(sub_type)
                if sub_type == 'Event:UIClick':
                    if json_line['data']['additional_info']['meta_action']  == 'PROPOSE_TOOL_PURCHASE_+':                             
                        if preReconMessageEnecountered == False:
                            preReconPurchaseCheat = True
                            print( "PreReconPurchaseCheat found!" ) 
                            return True
    return False        
        


if __name__ == "__main__":

    print('------------------------------------------------------------------------')
    print('Supplied Arguments : ')
    print(sys.argv)
    print(len(sys.argv))
    print('------------------------------------------------------------------------')


    incoming_file = sys.argv[1]     

    f = open(incoming_file, 'r',encoding='utf-8')

    lines = f.readlines()

    preReconPurchaseCheat = False
    autoJumpCheat = False
    threeWirecuttersCheat = False

    preReconPurchaseCheat = checkPreReconPurchase(lines)
    autoJumpCheat = checkAutoJump(lines)
    threeWirecuttersCheat = check3Wirecutters(lines)      

    print ( 'Read ' + str(len(lines)) + ' lines from source file.')
    print ( 'PreReconPurchaseCheat : ' + str(preReconPurchaseCheat))
    print ( 'AutoJumpCheat : ' + str(autoJumpCheat))
    print ( 'ThreeWirecuttersCheat : ' + str(threeWirecuttersCheat))

    f.close()

   

    


 


 
