#firstlook_file

This script reads in an exported metadata file and generates some statistics about the data in the file and does a variety of checks on the file.

Run the script wil the following command:

```
python firstlook_file.py -f <.metadata file> -o <output file>
```
