import { AfterContentInit, EventEmitter, Injectable } from '@angular/core';
import { TitleStrategy } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ResizingService implements AfterContentInit {

  public screen_width:number = 0;
  public screen_height:number = 0;

  resizeEmitter:EventEmitter<[number,number]> = new EventEmitter();
  
  constructor() {
    window.addEventListener('loadstart',()=>{
      this.getScreenDims();
      this.resizeEmitter.emit([this.screen_width,this.screen_height])
    })
    window.addEventListener('resize',()=>{
      this.getScreenDims();
      this.resizeEmitter.emit([this.screen_width,this.screen_height])
    })
  }

  getScreenDims():void{
    this.screen_width = window.innerWidth;
    this.screen_height = window.innerHeight;
  }

  ngAfterContentInit(): void {
    this.getScreenDims();
  }
}
