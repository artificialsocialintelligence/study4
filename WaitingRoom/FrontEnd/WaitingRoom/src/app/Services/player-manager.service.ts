import { EventEmitter, Injectable } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';

import { Player, User } from '../Interfaces/PlayerModels';
import { ReadyDialogComponent } from '../ready-dialog/ready-dialog.component';
import { WebsocketService } from './websocket.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SessionService } from './session.service';
import { SurveysObject } from '../Interfaces/SurveyModels';

@Injectable({
  providedIn: 'root'
})
export class PlayerManagerService {

  public players:Map<string,Player> = new Map<string,Player>();

  public participant_id:string ="NOT_SET";

  public ready_to_play_on_selection = false;

  public selected_to_play = false;

  public selected_to_team =false;

  public entrance_survey_reminder = true; 
  
  public requiredSurveysTaken = false;

  public optionalSurveysTaken = false;
  
  public survey_session_active = false;

  private proceed_dialog_open = false;

  public experiment_abandoned = false; 

  public aggregatedPlayerData?:User = undefined;

  

  getPlayerInfoEmitter:EventEmitter<any> = new EventEmitter<any>();

  constructor(
    public websocketService:WebsocketService,
    public sessionService:SessionService,
    public http:HttpClient, 
    public dialog: MatDialog) {

    // TESTING ONLY, COMMENT OUT BEFORE PUSHING
    // this.players.set("Player1", {
    //   name: 'Player1',
    //   participant_id: '100',
    //   pings_connected: 0,
    //   status: 'READY_TO_PLAY_ON_SELECTION',
    //   socket: '',
    //   intro_dialog_ack: false,
    //   entrance_survey_taken: false,
    //   exit_survey_taken: false
    // });

 
    websocketService.player_list_emitter.subscribe( ( players:Player[] ) =>{
     
      players.forEach( (p) =>{              
        this.players.set(p.name,p)          
        if( this.participant_id.localeCompare("NOT_SET")==0 && 
            p.name.localeCompare(this.websocketService.assigned_name)==0)
        {
          this.participant_id = p.participant_id;
          //this.getPlayerInfoEmitter.emit()   
          this.getPlayerInformation();           
        }                    
      });

      const removedUsers:string[] = []
      
      this.players.forEach( (v,k)=>{
        let found = false;        
        for(let i =0; i< players.length; i++){
          if( players[i].name.localeCompare(k)==0 ){
            found = true;
            break;            
          }
        }
        if(!found){
          removedUsers.push(k)
        }
      } );

      for( let i=0; i<removedUsers.length; i++){
        this.players.delete(removedUsers[i]);
      }

    }); 
    
    websocketService.update_player_states_emitter.subscribe( ( players:Player[] )=> {

      players.forEach( (p) =>{        
        if ( this.players.has(p.name)) {
          this.players.set(p.name,p)         
        }
      });

    });

    websocketService.update_player_pings_emitter.subscribe( ( data:{"name":string,"pings_connected":number} )=> {
      const name = data['name'];
      if (this.players.has(name)){
        this.players.get(name)!.pings_connected = data["pings_connected"]        
      }
    });

    websocketService.player_ready_on_selection_emitter.subscribe( ( ready:boolean )=> {      
      
      if(this.ready_to_play_on_selection === false) {
        this.ready_to_play_on_selection = true;      
        this.selected_to_play = false;
        this.experiment_abandoned = false; 
        console.log("Ready on selection !");
        console.log( "Bouncing : " + this.websocketService.bounceInProgress);
        if(this.websocketService.bounceInProgress == true){
          this.sessionService.proceedDialogQueued = true;
        }
        else{
          if(!this.proceed_dialog_open){
            this.proceedDialog();
          }      
        }
      }
          
    });

    websocketService.player_selected_to_team_emitter.subscribe( ( ready:boolean )=> {
      
      if(this.selected_to_team === false){

        this.ready_to_play_on_selection = true;   
        this.selected_to_team = true;   
        this.selected_to_play = false;
        this.experiment_abandoned = false; 
        console.log("Ready on selection !");
        console.log( "Bouncing : " + this.websocketService.bounceInProgress);
        if(this.websocketService.bounceInProgress == true){
          this.sessionService.proceedDialogQueued = true;
        }
        else{
          if(!this.proceed_dialog_open){
            this.proceedDialog();
          }      
        }

      }
      
          
    });

    websocketService.player_selected_emitter.subscribe( ( ready:boolean )=> {
      if(this.selected_to_play === false) {
        this.entrance_survey_reminder = false;
        this.selected_to_play = true;
        this.ready_to_play_on_selection = false; 
        this.experiment_abandoned = false; 
        console.log("Selected to Play!");
        console.log( "Bouncing : " + this.websocketService.bounceInProgress);
        if(this.websocketService.bounceInProgress == true){
          this.sessionService.proceedDialogQueued = true;
        }
        else{
          if(!this.proceed_dialog_open){
            this.proceedDialog();
          }      
        }
      }    
    });

    websocketService.experiment_abandoned_emitter.subscribe( ()=> {
      
      if(this.experiment_abandoned === false) {
        this.entrance_survey_reminder = false;
        this.selected_to_team = false;
        this.selected_to_play = false;
        this.ready_to_play_on_selection = false;
        this.experiment_abandoned = true; 
        console.log("Experiment Abandoned!");      
        {
          if(!this.proceed_dialog_open){
            this.proceedDialog();
          }      
        }
      }    
    });

    websocketService.survey_reminder_emitter.subscribe( ( ready:boolean )=> {
      
      if( this.survey_session_active == false &&
          this.entrance_survey_reminder == true &&
          this.optionalSurveysTaken == false
      ){         
        if(!this.proceed_dialog_open){
          this.proceedDialog();
        }
      }      
              
    });

  }

  hasSurveysRemaining():boolean{

    const totalSurveys = (this.sessionService.surveysObject as SurveysObject).surveys.length;
    const takenSurveys = this.aggregatedPlayerData?.preTrialSurveys == null ? 0 : (this.aggregatedPlayerData?.preTrialSurveys as SurveysObject).surveys.length;
    console.log( takenSurveys + " of " + totalSurveys + " surveys completed.")
    this.sessionService.activeSurveyIndex = takenSurveys;
    
    if(takenSurveys<totalSurveys){
      if(takenSurveys < 4){
        // DO NOTHING - THIS SHOULD NOT BE ANYTHING OTHER THAN 0
        // OUTSIDE OF TESTING/PILOTS
      }
      if(takenSurveys >=4 && takenSurveys<6  ){
        this.requiredSurveysTaken = true;
      }
      return true
    }
    else {
      this.requiredSurveysTaken = true;
      this.optionalSurveysTaken = true;
      this.entrance_survey_reminder = false;
      this.websocketService.socket.emit('entrance_survey_taken',this.websocketService.assigned_name)
      return false
    };
  }

  getPlayerInformation(){
    const headers = new HttpHeaders(
      { 
        'Content-Type': 'application/json', 
        'Authorization': 'Bearer ' + this.sessionService.bearerToken
      }
    );
    const url:string = window.location.origin +'/SSOService/user/GetPlayerStatsForAgentsById/'+this.participant_id;
    this.http.get( url,{headers: headers} ).subscribe({
      next: (v) => {
        console.log(v);
        this.aggregatedPlayerData = v as User
      },
      error: (e) => console.error(e),
      complete: () => { 
        this.hasSurveysRemaining()
        this.websocketService.sessionEstablishedOnce = true;
        } 
      }      
    )
  }
  
  proceedDialog(){
    
    if ( !this.proceed_dialog_open ) {

      this.proceed_dialog_open=true;
      
      const dialogConfig = new MatDialogConfig();
      dialogConfig.autoFocus = true;

      const dialogRef = this.dialog.open( ReadyDialogComponent, dialogConfig);

      dialogRef.afterClosed().subscribe( from_dialog => {
        if (from_dialog !== null) {
                   console.log(from_dialog)
        }
        this.proceed_dialog_open=false;
      });
    
    }    
  }
}
