import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { PlayerManagerService } from '../Services/player-manager.service';
import { WebsocketService } from '../Services/websocket.service';

@Component({
  selector: 'app-ready-dialog',
  templateUrl: './ready-dialog.component.html',
  styleUrls: ['./ready-dialog.component.scss']
})
export class ReadyDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ReadyDialogComponent>,public websocketService:WebsocketService, public playerManagerService:PlayerManagerService) { 
    console.log("Ready-Dialog Survey Reminder : " + this.playerManagerService.entrance_survey_reminder)
    dialogRef.disableClose = true;
  }

  ngOnInit(): void {
  }

  takeSurvey(){    
    this.dialogRef.close();
  }

  waitForUser(){
    this.pulseReadyButton();
    this.dialogRef.close("Wait");    
  }

  close(){
    this.dialogRef.close(); 
  }

  pulseReadyButton(){
    setTimeout( ( )=>{
      const ready_button_element = document.getElementById("ready-to-play-button") as HTMLButtonElement
      ready_button_element.animate([      
        // keyframes
      { color: 'Blue'},
      { transform: 'scale(1)' },      
      { transform: 'scale(1.5)' },
      { color: 'White'},
      { transform: 'scale(1)' },
        
        ], {
        // timing options
        duration: 2000,
        iterations: Infinity
      });
      },
      500)
  }

  proceedToExperiment() {
    
    this.websocketService.socket.emit('proceedingToExperiment',this.websocketService.assigned_name)
    this.dialogRef.close("Proceeding to ClientMap");
    this.playerManagerService.players.delete(this.websocketService.assigned_name) 
    this.reroute_page();



    // DO THIS TO PASS DATA
    // this.dialogRef.close({
    //   trialDTO: this.trialDTO,
    //   experimentDTO: this.experimentDTO,
    // });

  }

  reroute_page():void{   
    
    // ADD PLAYER NAME AS URL PARAMETER AFTER TESTING AND UPDATING CLIENTMAP TO RECEIVE THAT ROUTE 
    let url = 'https://'+document.location.host+'/ClientMap/?player='+this.websocketService.assigned_name
    if(this.websocketService.config.scaling_mode == true){
      url = `https://${document.location.host}/${this.websocketService.instanceInternalIP}/ClientMap/?player=${this.websocketService.assigned_name}&externalIP=${this.websocketService.instanceIP}&internalIP=${this.websocketService.instanceInternalIP}`
    }
    
    window.location.href = url
  }

}
