export interface SurveysObject{
    'participant_id':string,
    'surveys':Array<Survey>
}

export interface Survey{

    'survey_name':string,
    'survey_id':string,
    'data': Array<SurveyQuestion|SurveyText>,
    'complete':boolean

}

export class SurveyQuestion{
    'qid':string;    
    'input_type': string;
    'image_asset':string;
    'labels':Array<string>;
    'response'?: string;

    public static getProperties() :string[] {

        return ['qid','text','input_type','image_asset','labels']

    }

}

export class SurveyText {
    'text':string;
    
    public static getProperties() :string[] {

        return ['text']

    }
}