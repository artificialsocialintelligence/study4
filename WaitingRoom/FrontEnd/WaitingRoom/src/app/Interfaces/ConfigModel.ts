export interface Config{   

    debugMode?:boolean
    team_assembly?:TeamAssembly
    scaling_mode?:boolean
    disconnectTimeout?:number    
    admin_stack_ip?:string  
    admin_stack_port?:string
    db_api_token:string   
}

export interface TeamAssembly{
    players_per_team:number;
    team_assembly_strategies:string[];
    team_assembly_strategy:string;
    callsigns:string[]
}

   
