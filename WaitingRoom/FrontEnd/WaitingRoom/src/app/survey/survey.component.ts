import { Component, EventEmitter, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatRadioChange } from '@angular/material/radio';
import { MatSliderChange } from '@angular/material/slider';
import { Survey, SurveyQuestion, SurveyText } from '../Interfaces/SurveyModels';

import { PlayerManagerService } from '../Services/player-manager.service';
import { SessionService } from '../Services/session.service';
import { WebsocketService } from '../Services/websocket.service';


@Component({
  selector: 'app-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.scss']
})
export class SurveyComponent implements OnInit {

  surveyCompleteEmitter:EventEmitter<boolean>=new EventEmitter();

  public test:number = 0;

  

  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<SurveyComponent>, 
    public websocketService:WebsocketService, 
    public sessionService:SessionService, 
    public playerManagerService:PlayerManagerService,
    public http:HttpClient
    ) { }

  ngOnInit(): void {
   
  }

  isSurveyQuestion( data:object){

    const incomingProperties:string[]  = Object.getOwnPropertyNames(data);
    const validationProperties:string[] = SurveyQuestion.getProperties();
    for(let i=0; i<validationProperties.length;i++){
      if( !incomingProperties.includes(validationProperties[i]) ){
        return false;
      }
    }
    return true;
  }

  isSurveyText( data:object):boolean{
    const incomingProperties:string[]  = Object.getOwnPropertyNames(data);
    const validationProperties:string[] = SurveyText.getProperties();

    //reverse of the other one - if there's anything but text ist not a text object
    for(let i=0; i<incomingProperties.length;i++){
      if( !validationProperties.includes(incomingProperties[i]) ){
        return false;
      }
    }

    return true;

  }

  isMultipleChoice(data:unknown):boolean{

    const castData = data as SurveyQuestion;
    if(castData.input_type.localeCompare('MULTI_CHOICE')==0){
      return true
    }
    return false;
  }

  isSlider(data:unknown):boolean{

    const castData = data as SurveyQuestion;
    if(castData.input_type.localeCompare('SLIDER')==0){
      return true
    }
    return false;
  }

  isTextField(data:unknown):boolean{

    const castData = data as SurveyQuestion;
    if(castData.input_type.localeCompare('TEXT')==0){
      return true
    }
    return false;
  }

  isTextArea(data:unknown):boolean{

    const castData = data as SurveyQuestion;
    if(castData.input_type.localeCompare('TEXTAREA')==0){
      return true
    }
    return false;
  }

  getMinLabel(surveyIndex:number, dataindex:number):number{
    const split = (this.sessionService.surveysObject.surveys[surveyIndex].data[dataindex] as SurveyQuestion).labels[0].split(new RegExp('[()]'));
    //console.log(split)
    return Number.parseInt(split[1]);
  }

  getMaxLabel(surveyIndex:number, dataindex:number):number{
    const labels:string[] = (this.sessionService.surveysObject.surveys[surveyIndex].data[dataindex] as SurveyQuestion).labels;
    const split = labels[labels.length-1].split(new RegExp('[()]'));
    //console.log(split)
    return Number.parseInt(split[1]);
  }

  getStepSize(surveyIndex:number, dataindex:number):number{
   
    const labels:string[] = (this.sessionService.surveysObject.surveys[surveyIndex].data[dataindex] as SurveyQuestion).labels;  
    const min:number = Number.parseInt(labels[0].split(new RegExp('[()]'))[1])
    const max:number = Number.parseInt(labels[labels.length-1].split(new RegExp('[()]'))[1])  
    //console.log(min,max, labels.length-1 , max-min)
    //console.log( (max-min) / (labels.length-1) )
    return (max-min) / (labels.length-1);
  }

  radioChange(event:MatRadioChange, surveyIndex:number, dataIndex:number){

    //console.log(event, surveyIndex,dataIndex);
    (this.sessionService.surveysObject.surveys[surveyIndex].data[dataIndex] as SurveyQuestion).response = event.value;
    //console.log(this.sessionService.surveysObject.surveys[surveyIndex].data[dataIndex]);
  }

  sliderChange(event:MatSliderChange, surveyIndex:number, dataIndex:number){
    //console.log(event, surveyIndex,dataIndex);
    (this.sessionService.surveysObject.surveys[surveyIndex].data[dataIndex]as SurveyQuestion).response = event.value?.toString();
    //console.log(this.sessionService.surveysObject.surveys[surveyIndex].data[dataIndex]);
  }

  textInputChange(event:any, surveyIndex:number, dataIndex:number){
    //console.log(event, surveyIndex,dataIndex);
    (this.sessionService.surveysObject.surveys[surveyIndex].data[dataIndex]as SurveyQuestion).response = event.target.value;
    //console.log(this.sessionService.surveysObject.surveys[surveyIndex].data[dataIndex]);
  }

  getImagePath( assetName:string ){
    return this.sessionService.getAssetPath(assetName);
  }

 

  completeSurvey(){

    const survey = this.sessionService.surveysObject.surveys[this.sessionService.activeSurveyIndex] as Survey
    let complete = true;
    
    for (let i=0; i< survey.data.length; i++){
      if ( this.isSurveyQuestion(survey.data[i])){
        const question = survey.data[i] as SurveyQuestion
        
        if(question.response == null){
          if(question.input_type.localeCompare('SLIDER')==0){
            
            question.response = '0';

          }
          else{

            alert( "Please answer all the survey questions before clicking 'Complete Survey'. ")
            complete = false;
            break;

          }
         
        }
        if( survey.survey_id.localeCompare("DEMO")==0 && question.qid.localeCompare('1')==0){
          let num = null;
          try{
            num = Number(question.response);
            console.log(num)
          }catch(e){
            console.log(e)
          }
          if(num==null || num==undefined || num<18 || num>130 || isNaN(num) ) {
            alert( "Please enter a valid age over 18 for the first Survey item.")
            complete = false;
            break;
          }
        }
      }
    }

    if ( complete ){      
      
      //const mainDiv:HTMLDivElement = document.getElementById("mainWrapper") as HTMLDivElement;
      //mainDiv.scrollTop=0;
      const surveyDialog:HTMLDivElement = document.getElementById("SurveyDialog") as HTMLDivElement;
      surveyDialog.scrollTop=0;
      
      this.playerManagerService.entrance_survey_reminder = false;

      this.sessionService.surveysObject.surveys[this.sessionService.activeSurveyIndex].complete=true;
      this.sessionService.surveysObject.participant_id = this.playerManagerService.participant_id;

      const headers = new HttpHeaders(
        { 
          'Content-Type': 'application/json', 
          'Authorization': 'Bearer ' + this.sessionService.bearerToken
        }
      );
        
      this.http.put(
        // can use this because waiting room is in the same stack under same ip as ssoservice
        window.location.origin+"/SSOService/survey/UpdatePreTrialSurveyData/"+this.playerManagerService.participant_id,
        this.sessionService.surveysObject.surveys[this.sessionService.activeSurveyIndex],
        {headers}
      ).subscribe( (response)=>{
        console.log("Survey Updated for : " + this.playerManagerService.participant_id)
      });

      
      if(this.sessionService.activeSurveyIndex == 3  ){
        this.websocketService.socket.emit('entrance_survey_taken',this.websocketService.assigned_name)
        this.playerManagerService.requiredSurveysTaken = true;
        this.pulseReadyButton();  
        this.dialogRef.close();
      } 
      else if(this.sessionService.activeSurveyIndex == 5){
        this.websocketService.socket.emit('entrance_survey_taken',this.websocketService.assigned_name)
        this.playerManagerService.optionalSurveysTaken = true;
        this.pulseReadyButton();  
        this.dialogRef.close();
      }
      else{
        this.sessionService.activeSurveyIndex++;
      }
      console.log(this.sessionService.surveysObject);
      console.log ( "Active Survey Index: " + this.sessionService.activeSurveyIndex);
    }    
  }

  
  pulseReadyButton(){
    setTimeout( ( )=>{
      const ready_button_element = document.getElementById("ready-to-play-button") as HTMLButtonElement
      if( ready_button_element){
        ready_button_element.animate([      
          // keyframes
          { color: 'Blue'},
          { transform: 'scale(1)' },      
          { transform: 'scale(1.5)' },
          { color: 'White'},
          { transform: 'scale(1)' },
          
          ], {
          // timing options
          duration: 2000,
          iterations: Infinity
        });
      }
      
      },500)
  }
}
