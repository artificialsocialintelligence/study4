import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from "@angular/material/dialog";
import { PlayerManagerService } from '../Services/player-manager.service';
import { WebsocketService } from '../Services/websocket.service';

@Component({
  selector: 'app-intro-dialog',
  templateUrl: './intro-dialog.component.html',
  styleUrls: ['./intro-dialog.component.scss']
})
export class IntroDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<IntroDialogComponent>, public websocketService:WebsocketService, public playerMangerService:PlayerManagerService) {
    dialogRef.disableClose = true;
  }

  ngOnInit(): void {
  }

  close() {
    

    this.websocketService.socket.emit('intro_dialog_ack',this.websocketService.assigned_name)

    this.dialogRef.close("Intro Dialog Closed");
    

    // DO THIS TO PASS DATA
    // this.dialogRef.close({
    //   trialDTO: this.trialDTO,
    //   experimentDTO: this.experimentDTO,
    // });

  }


}
