const backup = require("./backup_data");
const config = require("./WaitingRoomConfig.json");
const scaling_manager = require('./scaling_manager')
const http = require('http');
const https = require('https');
const { read } = require("fs");

const PlayerWaitingState={
    REGISTERED_DISCONNECTED:"REGISTERED_DISCONNECTED",
    REGISTERED_CONNECTED:"REGISTERED_CONNECTED",
    TAKING_SURVEY:"TAKING_SURVEY",
    READY_TO_PLAY_AFTER_SURVEY:"READY_TO_PLAY_AFTER_SURVEY",
    READY_TO_PLAY_ON_SELECTION:"READY_TO_PLAY_ON_SELECTION",
    READY_TO_PLAY:"READY_TO_PLAY",
    SELECTED_READY_TO_PLAY:"SELECTED_READY_TO_PLAY",
    PROCEEDING_TO_TRIAL:"PROCEEDING_TO_TRIAL",
    CURRENTLY_IN_TRIAL:"CURRENTLY_IN_TRIAL"

}

const SocketState = {
    CONNECTED:"CONNECTED",
    DISCONNECTED:"DISCONNECTED"
}

class Player {
    
    constructor(playerinfo){
        
        this.status = PlayerWaitingState.REGISTERED
        this.name = playerinfo.name,
        this.email = playerinfo.email,
        this.participant_id = playerinfo.participant_id,
        this.callsign = "NOT_SET",
        this.pings_connected = 0,
        this.team="NOT_SET";
        // when we create a player we should check this
        this.socket = SocketState.DISCONNECTED
        this.lastPingMilliseconds = -1;
        this.disconnectedTimestamp = -1;
        this.intro_dialog_ack = false; 
        this.entrance_survey_taken = false
        this.first_trial = true;
        this.advisor_condition = "NOT_SET" 
        this.destination_ip = "NOT_SET"    
        this.setJsonModel(playerinfo)
    }

    

    setJsonModel(playerinfo) {

        console.log("Setting Player JSON Model.")

        const keysComingIn = Object.keys(playerinfo)
        const keysWeHave = Object.keys(this)

        keysComingIn.forEach( (k)=>{
            console.log("Coming In :" + k)
            if(keysWeHave.includes(k)){
                console.log("Has this key - setting it now.")
                this[k]=playerinfo[k]
            }
        })

        console.log("SetJsonModel : " + this.getJsonModel())
    }
    
    // returns the player model the frontend expects
    getJsonModel(){
        return {
            "name":this.name,
            "participant_id":this.participant_id,
            "callsign":this.callsign,
            "team":this.team,
            "email":this.email,
            "pings_connected":this.pings_connected,
            "status":this.status,
            "socket":this.socket,
            "intro_dialog_ack":this.intro_dialog_ack, 
            "entrance_survey_taken":this.entrance_survey_taken,
            "advisor_condition":this.advisor_condition,
            "destination_ip":this.destination_ip                
        }
    }
}

class WaitingRoom{
    
    constructor(socket_manager_instance,mqttClient){
        
        this.waiting_players = new Map();        
        this.socket_manager = socket_manager_instance;
        this.mqtt_client = mqttClient;

        // WE NEED TO SET THIS TO EMPTY AFTER EACH TEAM RETURNS FROM THE EXPERIMENT
        this.selected_players = []
        this.team_selected_for_single_instance = false;        
        
        this.scalingValue = 0;
        var self = this;

        if (config.scaling_mode && config.scaling_metric.scaling_metric_service) {
            setInterval(function() {

                // Calculate scaling value
                var newScalingValue = ((self.waiting_players.size / config.team_assembly.players_per_team) + self.mqtt_client.numBusyInstances) * config.scaling_metric.metric_value_per_instance;

                if (config.scaling_mode && self.scalingValue != newScalingValue) {
                    console.log("should_start_experiment: Number of Players in the waiting room: " + self.waiting_players.size);
                    console.log("should_start_experiment: Number of non waiting instances: " + self.mqtt_client.numBusyInstances);
                    console.log("should_start_experiment: New scaling value: " + newScalingValue);

                    if (newScalingValue >= 0) {
                        self.scalingValue = newScalingValue;
                    }
                    else {            
                        self.scalingValue = 0;
                    }
                }

                console.log("Sending scaling metric value of: " + Math.floor(self.scalingValue));

                // Send post
                var options = {
                    hostname: config.scaling_metric.scaling_metric_service,
                    port:'80',
                    path:'/ScalingMetric?value=' + Math.floor(self.scalingValue),
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                };
                
                var request = http.request(options, function(req) {
                });
                request.on('error', function(err) {
                    // Handle error
                    console.log("Scaling metric post error: " + err.message);
                });
                
                request.end();
            }, 5000);        
        }
        
        process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
    }

    // player_info is a json object
    add_waiting_player(playerinfo){

        
        const name = playerinfo['name']
        if ( name !=null){
            const new_player = new Player(playerinfo)
            
            if( this.socket_manager.client_sockets.has(name) ){
                new_player.socket = SocketState.CONNECTED
            }

            this.waiting_players.set(name, new_player )
            this.waiting_players.forEach((v,k)=>{
                console.log(k +" : ")
                console.log(v.getJsonModel())
                
            })
            console.log( "Creating pidMap backup for : " + name)
            backup.pidMap.set(name,new_player.participant_id)
        }
        
    }

   
    // player_info is a json object
    return_player_from_trial(playerinfo){
        
        const name = playerinfo['name']
        if ( name !=null){
            
            const player = new Player(playerinfo)
            
            if( this.socket_manager.client_sockets.has(name) ){
                player.socket = SocketState.DISCONNECTED              
                player.first_trial=false;
                player.advisor_condition = "NOT_SET";
                player.destination_ip = "NOT_SET";
                player.team = "NOT_SET";
                this.socket_manager.client_sockets.get(name).emit('return_player_from_trial','{}');
            }

            this.waiting_players.set(name, player )
            
            console.log( " Player Returning from trial")
            this.waiting_players.forEach((v,k)=>{
                console.log(k +" : ")
                console.log(v.getJsonModel())
                
            })
            if(this.team_selected_for_single_instance){
                this.team_selected_for_single_instance = false;
            } 
            
        }
        
    }

    remove_waiting_player(name){
        // CHECK IF SELECTED PLAYERS HAS THIS ONE
        const player = this.waiting_players.get(name);
        console.log( "Removing Player :");
        console.log(JSON.stringify(player));
        if( player!=null && player!=undefined){
            const status = player.status
            console.log( "Removing player with status : " + status)
            if( status === PlayerWaitingState.SELECTED_READY_TO_PLAY 
                || status === PlayerWaitingState.PROCEEDING_TO_TRIAL ){
                    console.log("Removing Player that was already Selected To Play")                
                    this.team_selected_for_single_instance = false;                
                   
                    this.selected_players = []                
            } 
            this.waiting_players.delete(name)
            this.socket_manager.update_player_list_for_all();
        }        
    }

    get_waiting_players(){
        return this.waiting_players
    }

    setPlayerStates(){
        let players_meeting_ping_minimum = [];
            
        const sockets = this.socket_manager.client_sockets;

        this.waiting_players.forEach((v,k) =>{
            
            if( sockets.has(k) ) {                    
                
                //console.log( k + " has both socket and waiting record")

                // Here we are transferring the number of pings from the socket object to the waiting player object
                // So we don't get a circular dependency by importing waiting_room into websockets too
                v["pings_connected"]=sockets.get(k).pings_connected

                // set a configuration for how many pings required before a player is eligible for being ready
                if( v.pings_connected>15 ){
                    players_meeting_ping_minimum.push(v) 
                }           
            }
        })

        // sort according to pings ( most to least )       
        players_meeting_ping_minimum = players_meeting_ping_minimum.sort( function (a, b) {  return b["pings_connected"] - a["pings_connected"];  } )
        
                    
        const ready_players = []

        // CHECK IF PLAYERS WITH MINIMUM PING ARE READY, AND UPDATE THE FRONTENDS IF SO/NOT SO
        players_meeting_ping_minimum.forEach( (player) =>{ 
            const name = player.name                         
            const waiting_player = this.waiting_players.get(name) 
            if( waiting_player.intro_dialog_ack){
                
                if(player.status === PlayerWaitingState.REGISTERED_CONNECTED){    
                    if( player.entrance_survey_taken ){                        
                        waiting_player.status = PlayerWaitingState.READY_TO_PLAY_ON_SELECTION                        
                        ready_players.push(player)                                               
                        
                        if (sockets.has(name) ){
                            console.log("Sending READY_TO_PLAY : " + player)
                            sockets.get(name).emit('ready_to_play_on_selection', "This Player's Ready")
                        }                                     
                    }
                    else{                        
                        waiting_player.status = PlayerWaitingState.READY_TO_PLAY_AFTER_SURVEY                        
                        
                        if (sockets.has(name) ){
                            console.log("Sending survey_reminder : " + player)
                            sockets.get(name).emit('survey_reminder', "This Player Needs to Take Survey")
                        }
                    }
                }
                else if ( player.status === PlayerWaitingState.READY_TO_PLAY_AFTER_SURVEY ){
    
                    if( player.entrance_survey_taken ){
                        waiting_player.status = PlayerWaitingState.READY_TO_PLAY_ON_SELECTION                        
                        ready_players.push(player)                                               
                        
                        if (sockets.has(name) ){
                            console.log("Sending READY_TO_PLAY_ON_SELECTION : " + player)
                            sockets.get(name).emit('ready_to_play_on_selection', "This Player's Ready")
                        }                                     
                    }
                }
                else if( player.status === PlayerWaitingState.READY_TO_PLAY_ON_SELECTION ){
                    ready_players.push(player)                    
                }
            }              
        })

        return ready_players;
}

    should_start_experiment(){
        const sockets = this.socket_manager.client_sockets;
        // SINGLE INSTANCE
        if(!config.scaling_mode){

            if( this.selected_players.length == 0 ) {

                const ready_players = this.setPlayerStates();
                
                if( !this.team_selected_for_single_instance ){
                    
                    this.doPlayerSelection( ready_players,sockets)
                }
            }
        }
        // SCALING MODE
        else{

            const ready_players = this.setPlayerStates();
            
            if ( ready_players.length >= config.team_assembly.players_per_team) {
                    
                console.log( "TEAM SIZE REQUIREMENTS MET : " + config.team_assembly.players_per_team )
                                    
                // slice function does not include the final index so we add 1                    
                this.selected_players = ready_players.slice(0,config.team_assembly.players_per_team) 
                    
                console.log("Selected Players");

                // SEND THE READY TO PLAY MESSAGE - BUT ONLY AFTER SURVEY IS DONE
                var found = false;
                var selectedHost = 'clientmap';

                const sockets = this.socket_manager.client_sockets;
                this.selected_players.forEach((p)=>{                    
                    if (sockets.has(p.name) ){  
                        // send a team selected message
                        const team_id = this.selected_players.map( player=>player.participant_id).sort().join("_");
                        console.log("Sending Team formed for TEAM_ID : " + team_id)
                        this.waiting_players.get(p.name).team = team_id;                        
                        sockets.get(p.name).emit('player_selected_to_team', {"team": team_id });                       
                        
                        // first player check sets the available IP to send the team to
                        if (!found && this.mqtt_client != null && this.mqtt_client.availableHostIPs) {
                            const hosts = Object.keys(this.mqtt_client.availableHostIPs);
                            hosts.forEach( (host) => {
                                if (!found && this.mqtt_client.availableHostIPs[host]) {
                                    found = true;
                                    selectedHost = host;
                                    this.mqtt_client.availableHostIPs[selectedHost] = false;
                                }
                            });
                        }
                        // if availableIP already found then send selected to play
                        if (found) {
                            console.log("Sending selected_to_play on " + selectedHost + "  : " + p);
                            this.waiting_players.get(p.name).status = PlayerWaitingState.SELECTED_READY_TO_PLAY
                            // Need better calculation of instance IP to use from state of all instances
                            sockets.get(p.name).emit('selected_to_play', selectedHost);
                            this.getInternalIP(selectedHost, p.name);
                        }
                    }
                });

                if (found) {
                    this.send_players_to_clientmap(selectedHost);
                }   
                this.selected_players = []                                                 
            }
        }
    }

    // returns true or false to set this.team_selected_for_single_instance when we are not in scaling mode
    doPlayerSelection( ready_players,sockets ){
        //////////////////// START FIFO ////////////////////////////////////
        if (scaling_manager.clientMapReadyForNewTeam ){

            if( config.team_assembly.team_assembly_strategy === "FIFO"){
            
                //console.log( "FIFO" )
                
                if ( ready_players.length >= config.team_assembly.players_per_team) {
                    
                    console.log( "TEAM SIZE REQUIREMENTS MET : " + config.team_assembly.players_per_team )
                                        
                    // slice function does not include the final index so we add 1
                    // I removed the  +1 on 10/8. put back if this causes errors
                    this.selected_players = ready_players.slice(0,config.team_assembly.players_per_team) 
                        
                    console.log("Selected Players : " + JSON.stringify(this.selected_players))
    
                    // SEND THE READY TO PLAY MESSAGE - BUT ONLY AFTER SURVEY IS DONE
                    this.selected_players.forEach((p)=>{
                        const name = p.name
                        this.waiting_players.get(name).status = PlayerWaitingState.SELECTED_READY_TO_PLAY
                        const team_id = this.selected_players.map( player=>player.participant_id).sort().join("_");
                        console.log("Sending Team formed for TEAM_ID : " + team_id)
                        this.waiting_players.get(p.name).team = team_id;                        
                        sockets.get(p.name).emit('player_selected_to_team', {"team": team_id });  
                        if (sockets.has(name) ){
                            console.log("Sending selected_to_play : " + p)
                            sockets.get(p.name).emit('selected_to_play', 'clientmap')
                        }
                    })
                    
                    console.log(" Sending selected players to clientmap.")                    
                    this.send_players_to_clientmap('clientmap');
                    this.selected_players = [];                     
                    this.team_selected_for_single_instance=true;                                                     
                }            
            }
            //////////////////// END FIFO ////////////////////////////////////

        }
    }

    send_players_to_clientmap(hostname) {
        
        const callsigns = config.team_assembly.callsigns;
        const conditions = config.advisor_conditions;

        console.log( callsigns );
        console.log( conditions );
        console.log( "Advisor Index : " + scaling_manager.advisor_index );
        console.log( "Before Setting : " + this.selected_players[0].advisor_condition );
        console.log(conditions[ Number(scaling_manager.advisor_index) ])

        for( let i = 0; i<this.selected_players.length; i++){
            this.selected_players[i].callsign = callsigns[i]
            this.selected_players[i].advisor_condition = conditions[  Number(scaling_manager.advisor_index) ]
        }        

        if(scaling_manager.advisor_index >= conditions.length-1){
            scaling_manager.advisor_index = 0;
        }
        else{
            scaling_manager.advisor_index++;
        }
        

        console.log( "Advisor Index After Increment : " + scaling_manager.advisor_index );
        console.log( "After Setting condition is : " + conditions[Number(scaling_manager.advisor_index)] );
        
        var postData = JSON.stringify( this.selected_players );

        var options = null;
        var req = null;

        if(config.scaling_mode){
            options = {
                hostname: hostname,
                port:'443',
                path: `/ClientMap/incoming_players`,
                method: 'POST',
                headers: {
                     'Content-Type': 'application/json',
                     'Content-Length': postData.length
                   }
              };      

            req = https.request(options, (res) => {
                console.log('statusCode:', res.statusCode);
                console.log('headers:', res.headers);
                
                res.on('data', (d) => {
                    process.stdout.write(d);
                });
            });
      
        }
        else {
            options = {
                hostname: hostname,
                port:'3080',
                path:'/incoming_players',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Content-Length': postData.length
                    }
            };

            req = http.request(options, (res) => {
                console.log('statusCode:', res.statusCode);
                console.log('headers:', res.headers);
                
                res.on('data', (d) => {
                    process.stdout.write(d);
                });
            });
      
        }
                
        req.on('error', (e) => {
          console.error(e);
        });
        
        req.write(postData);
        req.end();
    }    

    getInternalIP(ip, name) {
        const sockets = this.socket_manager.client_sockets;
        var self = this;

        console.log("getInternalIP of " + ip);

        // Send post
        var options = {
            hostname: config.scaling_metric.scaling_metric_service,
            port:'80',
            path:'/Instance/GetInternalIP?ip=' + ip,
            method: 'GET',
            headers: {
                'Content-Type': 'text/plain'
            }
        };
        
        let internalIP = '';

        var request = http.request(options, function(req) {
            req.on("data", (chunk) => {
                internalIP += chunk;
            });  
            req.on('end', () => {
                console.log(" Sending selected players to clientmap. InternalIP = " + internalIP);
                self.waiting_players.get(name).status = PlayerWaitingState.SELECTED_READY_TO_PLAY;  
                sockets.get(name).emit('instance_internal_ip', internalIP);
            });                  
        });
      
        request.on('error', function(err) {
            // Handle error
            console.log("Error getting internal ip: " + err.message);
        });
        
        request.end();
    }
    
    abandonedExperimentProtocol(obj){

        console.log(obj)
        const playerNames = [] 
        const pids = [];
        for( let i =0; i<obj.length; i++){
            playerNames.push(obj[i].name)
            pids.push(obj[i].participant_id) 
        } 

        const team_id = pids.sort().join('_');

        console.log(playerNames)
        console.log(pids)
        console.log("Team Id "+team_id)
        console.log("Abandoned Experiment for Players : " + playerNames + " with Team Id: " + team_id)


        for( let i =0; i<playerNames.length; i++){
            const name = playerNames[i];
            
            const player = this.waiting_players.get(name);       
            if(player !== null && player !== undefined){
                player.status = PlayerWaitingState.REGISTERED_CONNECTED
                player.destination_ip = "NOT_SET";
                player.team = "NOT_SET";
                player.pings_connected = 0;

                console.log("Set Player.pings_connected to 0 : " + player.pings_connected )
            }
            
            const socket = this.socket_manager.client_sockets.get(name);
            
            if(socket !== null && socket !== undefined){
                socket.pings_connected=0;
                this.selected_players = this.selected_players.filter(
                    (player)=>{ 
                        player.name !== name
                    });                
                
                    console.log("Emitting to socket ... ")
                socket.emit('experiment_abandoned', {});
            }
            this.team_selected_for_single_instance = false
            
            
            
        }

        // UPDATE DB TEAM RECORD
        //const url = "http://ssoservice:5058/Team/NeverProceeded/"+team_id
       
        const header = { 
        'Content-Type': 'application/json', 
        'Authorization': 'Bearer '+ process.env.DB_API_TOKEN
        }
        
        const options = {
            hostname: 'ssoservice',
            port:'5000',
            path: '/Team/NeverProceeded/'+team_id,
            headers: header
        }
        
        http.get(options, (response) => {
        
            var result = ''
            response.on('data', function (chunk) {
                result += chunk;
            });
        
            response.on('end', function () {
                console.log(result);
            });
        
        });
    }
    
}

module.exports = {

    WaitingRoom,
    PlayerWaitingState,
    SocketState,
    Player
   
};

