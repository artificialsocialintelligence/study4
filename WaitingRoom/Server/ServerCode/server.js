// ASIST

console.log(process.cwd())

// REQUIRED DEPENDENCIES
const websockets = require('./websockets');
const config = require("./WaitingRoomConfig.json");
const mqtt = require('./mqtt_manager');
const waiting_room = require('./waiting_room');
const express = require('express');
const bodyParser = require("body-parser");
const cors = require('cors');


const admin_stack_ip = process.env.admin_stack_ip;                
const admin_stack_port = process.env.admin_stack_port;
config.admin_stack_ip = admin_stack_ip
config.admin_stack_port = admin_stack_port
config.db_api_token = process.env.DB_API_TOKEN;
console.log(config);


// SETUP EXPRESS APPLICATION
const app = express();
var corsOptions = {  
  "origin": "*",
  "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
  "preflightContinue": false,
  "optionsSuccessStatus": 200    
}
cors.options = corsOptions
app.use(bodyParser.json());
app.use(express.static(process.cwd()+"/frontend/"));
app.use(cors());

const port = 7777;

// SETUP SERVER
server = app.listen(port, () => {
    
    console.log(`Server listening on the port::${port}`);     

});

//MANAGE SOCKETS
console.log('Attempting to start mqtt connection ...')

const client = mqtt.mqttInit();
const socket_manager = new websockets.SocketManager(server,client);
websockets.socketManagerInstance = socket_manager;
const waiting_room_instance = new waiting_room.WaitingRoom(socket_manager,client)
socket_manager.link_waiting_room(waiting_room_instance)

checkForDisconnects = function() {  
  //player_manager.printPlayers()  
  waiting_room_instance.waiting_players.forEach( (v,k) =>{      
      if(v.disconnectedTimestamp > -1 ){
        console.log(k + ":" + (Date.now() - v.disconnectedTimestamp) );        
        // make this timeout a setting in seconds
        if( (Date.now() - v.disconnectedTimestamp) >= config.disconnectTimeout ){
            console.log(k + ": socket timeout exceeded "+config.disconnectTimeout+"ms. Removing Player from the Waiting Room.")
            waiting_room_instance.remove_waiting_player(k);
        }
      }
  })
}

setInterval(checkForDisconnects,5000)

// ROUTING
app.get('/player/:from', (req,res) => {  
  res.sendFile(process.cwd()+"/frontend/index.html")
});

app.get('', (req,res) => {
  res.sendFile(process.cwd()+"/frontend/index.html")
});


//bodyparser.json() automatically turns the body into json so it's not necessary to parse it
app.post('/enter_player',cors(), function(req, res){
  try{

    let obj = req.body
    const name = obj['name']
  
    waiting_room_instance.add_waiting_player(obj)    
    // This won't update a socket that has not been established yet   

    // SPSO SYSTEM SHOULD NOT REDIRECT UNTIL THIS RESPONSE IS RECEIVED
    res.send(obj)    
    
  }
  catch(error){
    console.log(error)
    res.send(error)    
  }  
});


// need to actually write this function for real
app.post('/return_player_from_trial',cors(), function(req, res){
  try{

    let obj = req.body
    const name = obj['name']
    const pid = obj['participant_id']
  
    waiting_room_instance.return_player_from_trial(obj) 
    
    res.send(obj)
    
    
  }
  catch(error){

    console.log(error)
    res.send(error)

  }  
});

///WaitingRoom/experimentAbandoned
// need to actually write this function for real
app.post('/experiment_abandoned',cors(), function(req, res){
  try{

    let obj = req.body 
  
    waiting_room_instance.abandonedExperimentProtocol(obj)   

    res.send(obj)
    
    
  }
  catch(error){

    console.log(error)
    res.send(error)

  }  
});



























