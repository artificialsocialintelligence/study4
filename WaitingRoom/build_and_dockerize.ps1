New-Variable -Name "date" -Visibility Public -Value $(Get-Date)
Write-Host $date
Set-Location .\FrontEnd\WaitingRoom
ng build -c production
Set-Location ..\..\
docker build -t waitingroom --build-arg CACHE_BREAKER=$date .
