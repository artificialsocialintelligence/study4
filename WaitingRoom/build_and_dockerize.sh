#!/bin/bash

cd ./FrontEnd/WaitingRoom
ng build -c production
cd ../../
docker build -t waitingroom --build-arg CACHE_BREAKER=$(date +%s) .
