import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminPageComponent } from './components/admin-page/admin-page.component';
import { DownloadPageComponent } from './components/download-page/download-page.component';
import { GameMechanicsComponent } from './components/game-mechanics/game-mechanics.component';
import { LeaderboardComponent } from './components/leaderboard/leaderboard.component';
import { LoginComponent } from './components/login/login.component';
import { FaqComponent } from './components/faq/faq.component';
import { RegistrationHomeComponent } from './components/registration-home/registration-home.component';
import { CanActivateAdmin } from './services/can-activate-admin.service';

const routes: Routes = [
  { path: '', redirectTo: '/Login', pathMatch: 'full' },
  { path: 'Login', component: LoginComponent },
  { path: 'Registration', component: RegistrationHomeComponent },
  { path: 'Download', component: DownloadPageComponent },
  { path: 'GameMechanics', component: GameMechanicsComponent },
  { path: 'Leaderboard', component: LeaderboardComponent },
  { path: 'FAQ', component: FaqComponent },
  { 
    path: 'Administration',
    component: AdminPageComponent,
    canActivate: [CanActivateAdmin] 
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'enabled',
    anchorScrolling: 'enabled',
    scrollOffset: [0, 25], // cool option, or ideal option when you have a fixed header on top.
  })],
  exports: [RouterModule],
  providers: [CanActivateAdmin]
})
export class AppRoutingModule { }
