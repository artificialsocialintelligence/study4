import { Component, Input, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-download-page',
  templateUrl: './download-page.component.html',
  styleUrls: ['./download-page.component.css']
})
export class DownloadPageComponent implements OnInit {
  checked: false;

  inputUser: User;

  constructor(private authService: AuthService, route: ActivatedRoute, titleService: Title, ) { 
    titleService.setTitle("Download");
    this.inputUser = this.authService.currentUser;
  }

  ngOnInit(): void {
  }

  downloadMod(){
    
  }

  isLoggedIn() {
    if(this.authService.currentUser != this.inputUser) {
      this.inputUser = this.authService.currentUser;
    }
    return (this.authService.currentUser != null);
  }



  goToLobby() {
    this.authService.sendWaitingRoomRequest(this.inputUser).subscribe({
      next: (v) => console.log(v),
      error: (e) => console.error(e),
      complete: () => this.authService.redirectToLobby(this.inputUser?.userId)
    });
  }

}
