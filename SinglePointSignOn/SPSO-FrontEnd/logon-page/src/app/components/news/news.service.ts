import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import {catchError, Observable, of, tap} from "rxjs";
import {News} from "./news";

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  // private ssoServicePath = environment.ssoServicePath + '/trials';  // URL to web api
  private newsUrl = environment.production ? window.location.origin + '/SSOService/news' : environment.ssoServicePath + 'news';


  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  constructor(
    private http: HttpClient,
  ) { }

  readNews(): Observable<News[]> {
    return this.http.get<News[]>(this.newsUrl)
      .pipe(
        tap(_ => this.log('Read news')),
        catchError(this.handleError<News[]>('readNews', []))
      );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private log(message: string) {
    // this.loggingService.add(`NewsService: ${message}`);
    console.log(`NewsService: ${message}`);
  }
}
