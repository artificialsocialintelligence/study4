export interface News {
  news_id: number;
  creation_date: string;
  content: string;
}
