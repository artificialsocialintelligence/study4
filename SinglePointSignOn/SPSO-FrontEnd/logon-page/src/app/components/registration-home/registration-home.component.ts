import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Title } from '@angular/platform-browser';
import { AuthService } from 'src/app/services/auth.service';
import { User } from '../../models/user'
import { InfoDialogComponent } from '../info-dialog/info-dialog.component';
import { UserListDialogComponent } from '../user-list-dialog/user-list-dialog.component';
import { RegistrationConfirmDialogComponent } from '../registration-confirm-dialog/registration-confirm-dialog.component';
import { Router, ActivatedRoute } from '@angular/router'


@Component({
  selector: 'app-registration-home',
  templateUrl: './registration-home.component.html',
  styleUrls: ['./registration-home.component.css']
})

export class RegistrationHomeComponent implements OnInit {
  currentUser: User;
  registeredUsers: User[];

  isLoading = false;

  registrationForm = this.fb.group({
    username: ['', [Validators.required, this.validUsername()]],
    email: ['', [Validators.required, Validators.email]]
  });
  constructor(
    titleService: Title, 
    private fb: FormBuilder, 
    private authService: AuthService,
    private _snackBar: MatSnackBar, 
    private router: Router, 
    private route: ActivatedRoute,
    public dialog: MatDialog) { 
    titleService.setTitle("Registration");
  }
  

  ngOnInit(): void {
  }

  getErrorMessage() {
    
    if (this.registrationForm.get('email')?.hasError('required')) {
      return 'You must enter a value';
    }

    return this.registrationForm.get('email')?.hasError('email') ? 'Not a valid email' : '';
  }

  validUsername(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      let re = /^\b\w{3,16}\b$/;
      let namestr = this.registrationForm?.get('username')?.value?.toString();
      let valid = re.exec(namestr!) !== null
      return valid ? null : {invalid: true}
    }

  }

  showInfo() {
    this.dialog.open(InfoDialogComponent);

  }


  register(user){
    let dialogRef = this.dialog.open(RegistrationConfirmDialogComponent, {
      data: { username: user.username },
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        let newUser: User = {userId: user.username, email: user.email}
        this.isLoading = true
        this.authService.register(newUser).subscribe({
          next: (v) => this.currentUser = v,
          error: (e) => {
            console.error(e),
            this._snackBar.open('Error creating user \"' + newUser.userId + '\"', 'Ok', {
              horizontalPosition: 'center',
              verticalPosition: 'top',
            })
            this.isLoading = false
          },
          complete: () => {
              console.log('Complete'),
              this._snackBar.open(
                'User \"' + this.currentUser.userId + '\" registered!', 'Ok', {
                horizontalPosition: 'center',
                verticalPosition: 'top',
              });
              this.registrationForm.reset();
              this.isLoading = false
            }
        });
      }
    });
  }

  viewUsers() {
    this.dialog.open(UserListDialogComponent)
  }

  reloadPage() {
    location.reload();
  }

  goToLogin() {
    this.router.navigate(['../Login'], { relativeTo: this.route });  
  }
}
