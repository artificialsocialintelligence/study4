import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-account-recovery',
  templateUrl: './account-recovery.component.html',
  styleUrls: ['./account-recovery.component.css']
})
export class AccountRecoveryComponent implements OnInit {

  email:string=null
  response:string=null
  spinner = false;

  constructor( 
    public dialogRef:MatDialogRef<AccountRecoveryComponent>,
    public authService:AuthService,
    public http:HttpClient
    ) { }

  ngOnInit(): void {
  }

  close(){
    this.dialogRef.close()
  }

  accountRecovery(){

    this. spinner = true;
    this.http.get<any>(`${this.authService.ssoServicePath}user/forgotPassword/${this.email}`, {headers: this.authService.headers})
    .subscribe( 
      { 
        next: (next)=>{ 
        this.response=next.result;
        console.log("Account Recovered : " + next.result);
        },
        error:(error)=>{ 
          this.response=error.error.result; 
          this. spinner = false;        
          console.log(" Account Recovery Error : ");
          console.log(error);
        },
        complete: () => { this. spinner = false;  } 
      }
    );
    this.email = ""
  }

}
