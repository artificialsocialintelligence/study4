import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { UserListDialogComponent } from '../user-list-dialog/user-list-dialog.component';
import { User } from '../../models/user';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.css']
})
export class AdminPageComponent implements OnInit {

  currentUser: User

  constructor(
    titleService: Title,
    public dialog: MatDialog,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute) { 
    titleService.setTitle("Administration");
  }

  ngOnInit(): void {
    this.currentUser = this.authService.currentUser
    this.viewUsers()
  }

  viewUsers() {
    this.dialog.open(UserListDialogComponent, { disableClose: true });
  }

}
