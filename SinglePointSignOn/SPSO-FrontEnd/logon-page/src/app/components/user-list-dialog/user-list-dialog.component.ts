import { AfterViewInit, Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';
import { UserEditDialogComponent } from '../user-edit-dialog/user-edit-dialog.component';

@Component({
  selector: 'app-user-list-dialog',
  templateUrl: './user-list-dialog.component.html',
  styleUrls: ['./user-list-dialog.component.css']
})
export class UserListDialogComponent implements AfterViewInit {
  registeredUsers: User[];
  // deletedUsers: User[];
  displayedColumns: string[] = ['userId', 'participantId', 'email', 'edit', 'delete'];
  dataSource;
  deletedDataSource;
  checked = false;

  constructor(
    private authService: AuthService,
    public dialog: MatDialog) {
  
   }

  ngAfterViewInit(): void {
    this.reloadTable();
  }

  deleteUser(element: User){
    this.authService.deleteUser(element.id).subscribe(e => this.reloadTable());
  }

  restoreUser(element: User){
    console.log(element)
    // this.authService.restoreUser(element).subscribe(e => this.reloadTable());
  }

  reloadTable(){
    this.authService.getUsers().subscribe(users => 
      this.dataSource = users
    );
    // this.authService.getDeletedUsers().subscribe(users => 
    //   this.deletedDataSource = users
    // );
  }

  openEditor(user: User) {
    let dialogRef = this.dialog.open(UserEditDialogComponent);
    dialogRef.componentInstance.user = user;
    dialogRef.afterClosed().subscribe(() => {
      this.reloadTable();
    })
  }

}
