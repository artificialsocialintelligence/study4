import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { AuthService } from './auth.service';


@Injectable()
export class CanActivateAdmin implements CanActivate {
  constructor(public auth: AuthService, public router: Router) {}

  canActivate(): Observable<boolean> {
    if (!this.auth.currentUser?.isAdmin) {
      this.router.navigate(['/Login']);
      return of(false);
    }
    return of(true);
  }
}