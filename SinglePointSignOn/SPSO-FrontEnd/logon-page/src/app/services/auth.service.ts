import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, Observable, of } from 'rxjs';
import { environment } from '../../environments/environment';
import { Team, User, UserInfo } from '../models/user';
import { Trial } from '../models/trial';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public ssoServicePath: string;
  private waitingRoomPath: string;
  public currentUser: User;
  public assetsPath: string;
  public bearerToken: string;
  public headers = new HttpHeaders(
    { 'Content-Type': 'application/json'     
    });

  constructor(private http: HttpClient) {
    this.ssoServicePath = environment.production? window.location.origin + '/SSOService/': environment.ssoServicePath; //environment.ssoServicePath
    this.waitingRoomPath = environment.production? window.location.origin+ '/': environment.waitingRoomPath; // environment.waitingRoomPath
    this.assetsPath = window.location.origin + "/assets/";
   }

  getAuth(credentials): Observable<{authentication: boolean, user: User}> {    
    return this.http.post<{authentication: boolean, user: User}>(`${this.ssoServicePath}user/authenticate`, credentials, {headers: this.headers}).pipe(catchError(err => {
      return of({authentication: false, user: null});
    }));
  }

  getCurrentUser(id: string) {
    
    const requestString = this.ssoServicePath +'user/id?id='+id;
    
    return this.http.get<User>(requestString, {headers: this.headers}).subscribe( (user: User) => 
      this.currentUser = user
    );
  }

  register(newUser: User): Observable<User>{
    console.log(newUser)
    return this.http.post<User>(`${this.ssoServicePath}user/register`, newUser, {headers: this.headers});
  }

  getUsers(): Observable<User[]>{
    return this.http.get<User[]>(`${this.ssoServicePath}user/users`, {headers: this.headers});
  }

  deleteUser(id: string): Observable<User> {
    return this.http.delete<User>(`${this.ssoServicePath}user/${id}`, {headers: this.headers});
  }

  updateUserProfile(id: string, user: User, isAuthorized: boolean): Observable<User> {
    return this.http.put<User>(`${this.ssoServicePath}user/updateProfileByUserId${id}?isAuthorized=${isAuthorized}`, user, {headers: this.headers});
  }

  sendWaitingRoomRequest(user: User){
    let userInfo: UserInfo = {
      id: user.id,
      name: user.userId,
      email: user.email,
      participant_id: user.participantId,
      surveys: user.surveys,
      successfulBombDisposals: user.successfulBombDisposals,
      unsuccessfulBombDisposals: user.unsuccessfulBombDisposals,
      teams: user.teams
    }
    return this.http.post<any>(`${this.waitingRoomPath}WaitingRoom/enter_player`, userInfo, {headers: this.headers});

  }

  redirectToLobby(userId: string) {
    window.location.href = `${this.waitingRoomPath}WaitingRoom/player/${userId}`;
  }

  getTeamData(): Observable<Team[]> {
    return this.http.get<Team[]>(`${this.ssoServicePath}team/topTeams`, {headers: this.headers})
  }

  getTrialData(): Observable<Trial[]> {
    return this.http.get<Trial[]>(`${this.ssoServicePath}trial/GetTopTrialRecords`, {headers: this.headers})
  }

  updateConsentTimestamp(id:string){
    this.http.get<any>(`${this.ssoServicePath}user/updateConsentTimestamp/${id}`, {headers: this.headers})
    .subscribe( (resp)=>{
      console.log("Consent Timestamp set : "+ resp);
    });
  }

  
}
