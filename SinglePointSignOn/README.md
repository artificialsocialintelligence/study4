
# Single Point Sign on Instructions

## About
The Single Point Sign On service consists of an Angular Front End and DotNet 6 backend. Its purpose is to provide a Landing, Login, and Administrative page to the ASIST Study 4 experiment sessions. The client had requested that this system be automated and without administrators for large scale data collection. Thus it was designed to have built into it, a Registration system, a News Update system, a Login System, and various information tabs created to help the naiive participant navigate successfully through the Study 4 experiment with minimal involvement from Tech Support.

This service containes the API's that store and update User data, Trial Summary Data, Team Data, and News Update data.  This information can be accessed via PGAdmin @ http://localhost:5433. The username and password should match those you setup in **Local/export_env_vars_and_testbed_up.sh**
  

## Building Docker container
  

The container can be built in Windows by running the **build_and_dockerize.ps1** script. This script can be easily adapted to run on Linux and Mac, as in the **build_and_dockerize.sh** script.
  

## Configuration  

The SPSO Service configuration is done in the **Local/SSOService/appsettings.json** file

            {
                  # Allows connections from any external machine
                  "AllowedHosts": "*",
                  # automatically set by Local/export_env_vars_and_testbed_up.sh
                  "ConnectionStrings": {
                        "SSODBConnection": ""
                  },
                  # If you have setup automated emails to your participants
                  "EmailSettings": {
                        # Don't change
                        "Server": "smtp.gmail.com",
                        # automatically set by Local/export_env_vars_and_testbed_up.sh
                        "FromName": "",
                        # automatically set by Local/export_env_vars_and_testbed_up.sh
                        "Email": "",
                        # automatically set by Local/export_env_vars_and_testbed_up.sh
                        "Password": "",
                        # Don't change
                        "Port": 587
                  },
                  "FrontEndSettings": {
                        "LoginUrl": "https://localhost:9000/SPSO/Login"
                  },
                  # automatically set by Local/export_env_vars_and_testbed_up.sh
                  "Keys": {
                        "SecretKey": ""
                  },
                  # Setting Log Levels in .Net Core 2.0 +
                  "Logging": {
                        "LogLevel": {
                              "Console": "Debug",
                              "Default": "Debug",
                              "Microsoft.AspNetCore": "Warning"
                        }
                  },
                  # Setting for the DB_API_TOKEN that secures connection to the Participant DB API's
                  # You can set the expiration dates for the token below for added security. Default is 3 months.
                  "Jwt": {
                        "Issuer": "localhost",
                        "Audience": "localhost",
                        "Key": "",
                        "Expiration": {
                              "Years": 0,
                              "Months": 3,
                              "Days": 0,
                              "Hours": 0
                        }
                  }
            }
## Debug environment

In order to use this debugging environment you will need to pre-build the Waiting Room, ClientMap, AsistControl, and SinglePointSignOn services first.

Once the SIngle Point Sign On system has been built, a convenient debugging environment can be started in the Local folder by running `docker compose up` in the **SinglePointSignOn/Local** folder. This debugging environment is one of the easiest ways to get most of the Asist testbed running on a developer's laptop WITHOUT all the extraneous agent containers eating up resources.

In order for the system to function correctly you should fill out the **SinglePointSignOn/Local/.env** file in the same way you filled out the **Local/export_env_vars_and_testbed_up.sh** to launch the full testbed.

Assuming you are debugging @ localhost, once you're docker compose command has completed, you may check it's success by visiting the Logger at https://localhost:9000/Logger/ and inspecting each container's logs.

You may begin the debugging process at the Landing Page, as if you were a true participant @ https://localhost:9000/SPSO/Login/ and follow the instruction on the main readme of this repo to test any functionality.
