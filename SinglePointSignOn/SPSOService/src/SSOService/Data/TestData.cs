
using ConsoleApp.PostgreSQL;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using SSOService.Helpers;
using Microsoft.EntityFrameworkCore;
using System.Security.Cryptography;
using SSOService.Database.DBModels;

namespace SSOService.Data
{
    public static class TestData
    {
        private static Keys keyConfig;

        public static void Initialize(SSODBContext context, IConfiguration configuration)
        {
            // Look for any users.
            if (context.Users.Any())
            {
                return;   // DB has been seeded
            }

            keyConfig = configuration.GetSection("Keys").Get<Keys>();

            string SSO_SECRET_KEY = Environment.GetEnvironmentVariable("SSO_SECRET_KEY");

            var users = new User[]
            {
                new User{UserId="admin", Password=EncryptPassword.Encrypt("administrator", SSO_SECRET_KEY), IsAdmin=true,  Email="", ParticipantId="admin"},
            };

            context.Users.AddRange(users);
            context.SaveChanges();
        }
    }
}