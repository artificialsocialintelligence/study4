﻿using System.Text.Json.Serialization;

namespace SSOService.DTO
{
    public class NewsDTO
    {
        [JsonPropertyName("news_id")]
        public Guid? NewsId { get; set; }
        [JsonPropertyName("creation_date")]
        public string? CreationDate { get; set; }
        [JsonPropertyName("content")]
        public string Content { get; set; } = string.Empty;
    }
}
