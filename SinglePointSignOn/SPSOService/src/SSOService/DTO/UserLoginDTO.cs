﻿using SSOService.Database.DBModels;
using System.Text.Json.Serialization;

namespace SSOService.DTO
{
    public class UserLoginDTO
    {
        //public Guid? Id { get; set; }
        public string? UserId { get; set; }
        public string? Password { get; set; }
        public string? Email { get; set; }
        public string? ParticipantId { get; set; }    }

    public class UserMessageBusDTO
    {
       
        public string? ParticipantId { get; set; }
        public int? ParticipationCount { get; set; }  
        public SurveysDTO? PreTrialSurveys { get; set; } = null;
        public SurveysDTO? PostTrialSurveys { get; set; } = null;
        public SurveysDTO? OptionalSurveys { get; set; } = null;        
        public List<string> TeamsList { get; set; } = new List<string>();
        public int SuccessfulBombDisposals { get; set; } = 0;
        public int UnsuccessfulBombDisposals { get; set; } = 0;

        public UserMessageBusDTO() { }

        public UserMessageBusDTO(User user)
        {            
            this.ParticipantId = user.ParticipantId;
            this.ParticipationCount = user.ParticipationCount;
            //this.PreTrialSurveys = user.PreTrialSurveys != null?System.Text.Json.JsonSerializer.Deserialize<SurveysDTO>(user.PreTrialSurveys):null;
            //this.PostTrialSurveys = user.PostTrialSurveys != null ? System.Text.Json.JsonSerializer.Deserialize<SurveysDTO>(user.PostTrialSurveys) : null;
            //this.OptionalSurveys = user.OptionalSurveys != null ? System.Text.Json.JsonSerializer.Deserialize<SurveysDTO>(user.OptionalSurveys) : null;
            this.PreTrialSurveys = user.PreTrialSurveys ;
            this.PostTrialSurveys = user.PostTrialSurveys;
            this.TeamsList = user.TeamsList;

        }
    }


}
