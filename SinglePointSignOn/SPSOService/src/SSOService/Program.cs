using ConsoleApp.PostgreSQL;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using System.Text.Json.Serialization;

var MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddAuthorization();

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
{
    string DB_API_ISSUER_KEY = Environment.GetEnvironmentVariable("DB_API_ISSUER_KEY");
    options.TokenValidationParameters = new TokenValidationParameters()
    {
        ValidateIssuer = false,
        ValidIssuer = builder.Configuration["Jwt:Issuer"],
        ValidateAudience = false,
        ValidateIssuerSigningKey = true,
        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(DB_API_ISSUER_KEY)),
    };

});


builder.Services.AddCors(options =>
{
    options.AddPolicy(name: MyAllowSpecificOrigins,
                      policy  =>
                      {
                          policy.AllowAnyOrigin()
                            .AllowAnyHeader()
                            .AllowAnyMethod();
                      });
});

builder.Services.AddDbContext<SSODBContext>(); 

builder.Services.AddDatabaseDeveloperPageExceptionFilter();

builder.Services.AddControllers()
    .AddJsonOptions((options) => {
        options.JsonSerializerOptions.PropertyNameCaseInsensitive = true;
        options.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles;
    });

builder.Services.AddLogging();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI(options =>
{
    options.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
    //options.RoutePrefix = "/Swagger";
});

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    //app.UseSwagger();
    //app.UseSwaggerUI();

}
else
{
    app.Urls.Add("http://0.0.0.0:5000");
    app.UseDeveloperExceptionPage();
    app.UseMigrationsEndPoint();
}

using (var scope = app.Services.CreateScope())
{
    var services = scope.ServiceProvider;

/*    var context = services.GetRequiredService<SSODBContext>();
    context.Database.EnsureCreated();*/

    // migrate any database changes on startup (includes initial db creation)
    var dataContext = scope.ServiceProvider.GetRequiredService<SSODBContext>();
    dataContext.MigrateStoredVolumeData();
    
    
    

}

app.UseCors(MyAllowSpecificOrigins);

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers().RequireAuthorization();

app.Run();


string GenerateToken()

{
    string DB_API_ISSUER_KEY = Environment.GetEnvironmentVariable("DB_API_ISSUER_KEY");
    var mySecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(DB_API_ISSUER_KEY));

    var tokenHandler = new JwtSecurityTokenHandler();
    var tokenDescriptor = new SecurityTokenDescriptor
    {
        Expires = 
            DateTime.UtcNow.AddYears( Int32.Parse(builder.Configuration["Jwt:Expiration:Years"]) )
            .AddMonths(Int32.Parse(builder.Configuration["Jwt:Expiration:Months"]))
            .AddDays(Int32.Parse(builder.Configuration["Jwt:Expiration:Days"]))
            .AddHours(Int32.Parse(builder.Configuration["Jwt:Expiration:Hours"])),            
        Issuer = builder.Configuration["Jwt:Issuer"],
        Audience = builder.Configuration["Jwt:Audience"],
        SigningCredentials = new SigningCredentials(mySecurityKey, SecurityAlgorithms.HmacSha256Signature)
    };

    var token = tokenHandler.CreateToken(tokenDescriptor);
    return tokenHandler.WriteToken(token);
}