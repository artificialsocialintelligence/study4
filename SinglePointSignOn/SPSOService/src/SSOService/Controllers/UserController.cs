﻿using ConsoleApp.PostgreSQL;
using CrypticWizard.RandomWordGenerator;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using static CrypticWizard.RandomWordGenerator.WordGenerator;
using System.Net.Mail;
using System.Net;
using SSOService.Helpers;
using SSOService.Database.DBModels;
using SSOService.DTO;
using System.Text.Json.Nodes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using System.Linq;

namespace SSOService.Controllers
{
    [Produces("Application/json")]
    [Consumes("Application/json")]
    [Route("user")]
    [ApiController]
    [Authorize]
    public class UserController : ControllerBase
    {
        private readonly SSODBContext _db;
        private readonly EmailSettings emailConfig;
        private readonly FrontEndSettings frontEndConfig;
        private readonly IConfiguration _configuration;
        private readonly Keys keyConfig;
        private readonly ILogger _logger;

        public UserController(IConfiguration configuration, SSODBContext context, ILogger<UserController> logger)
        {
            _db = context;
            emailConfig = configuration.GetSection("EmailSettings").Get<EmailSettings>();
            frontEndConfig = configuration.GetSection("FrontEndSettings").Get<FrontEndSettings>();
            keyConfig = configuration.GetSection("Keys").Get<Keys>();
            _logger = logger;
            _configuration = configuration;
        }

        [HttpGet("GetAllUsers")]
        public async Task<ActionResult<User>> GetAllUserRecords()
        {
            try
            {
                var users = _db.Users;

                if (users != null)
                {
                    return Ok(users);
                }

                JsonObject error = new JsonObject()
                {
                    ["not_found"] = "There was an error retreiving all the User Records."
                };
                return NotFound(error);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("users")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<User>>> GetUsers()
        {
            //return await _db.Users.ToListAsync();

            string SSO_SECRET_KEY = Environment.GetEnvironmentVariable("SSO_SECRET_KEY");

            var users = new List<User>();
            await _db.Users.AsNoTracking().ForEachAsync((User u) =>
            {
                u.Password = EncryptPassword.Decrypt(u.Password, SSO_SECRET_KEY);
                users.Add(u);
            });

            return Ok(users);

        }

        [HttpGet("id")]
        public async Task<ActionResult<User>> GetUserById(string id)
        {
            User user;
            string SSO_SECRET_KEY = Environment.GetEnvironmentVariable("SSO_SECRET_KEY");
            try {

                Console.WriteLine( "GetUserById : " + id);

                user = await _db.Users.AsNoTracking().FirstOrDefaultAsync(i => i.ParticipantId == id);

                Console.WriteLine("User.ParticipantId : " + user!.ParticipantId);

                if (user == null)
                {
                    return NotFound();
                }
                user.Password = EncryptPassword.Decrypt(user.Password, SSO_SECRET_KEY);
                
                return Ok(user);

            }
            catch(Exception ex) {
                
                Console.WriteLine(ex.StackTrace);
                
                return Ok(null);
                 
            }
            
        }

        [HttpGet("GetPlayerStatsForAgentsById/{id}")]
        public async Task<ActionResult<User>> GetPlayerStatsForBusById(string id)
        {
            User user;
            try
            {

                Console.WriteLine("GetUserById : " + id);

                user = await _db.Users.AsNoTracking().FirstOrDefaultAsync(i => i.ParticipantId == id);

                Console.WriteLine("User.ParticipantId : " + user!.ParticipantId);

                if (user == null)
                {
                    return NotFound();
                }

                UserMessageBusDTO dto = new UserMessageBusDTO( user );


                return Ok(dto);

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.StackTrace);

                return Ok(null);

            }

        }

        [Route("register")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult<User>> RegisterUser(UserLoginDTO inputUser)
        {
            var count = 1;
            // return if user in db
            if (await _db.Users.Where(i => EF.Functions.ILike(i.UserId, $"{inputUser.UserId}")).FirstOrDefaultAsync() is User user)
            {
                return Conflict(new { resisterSuccess = false });
            }
            var pid = "P" + (count).ToString("D6");
            while (_db.Users.Any(x => pid == x.ParticipantId))
            {
                count++;
                pid = "P" + (count).ToString("D6");
                Console.WriteLine(pid);

            }
            //generate pw and hash
            WordGenerator myWordGenerator = new WordGenerator();

            List<PartOfSpeech> pattern = new List<PartOfSpeech>();
            pattern.Add(PartOfSpeech.adj);
            pattern.Add(PartOfSpeech.noun);

            string tempPw = myWordGenerator.GetPatterns(pattern, '-', 1)[0];
            inputUser.Password = tempPw;
            string SSO_SECRET_KEY = Environment.GetEnvironmentVariable("SSO_SECRET_KEY");
            var newUser = new User
            {
                UserId = inputUser.UserId,
                Password = EncryptPassword.Encrypt(tempPw, SSO_SECRET_KEY),
                 
                Email = inputUser.Email,
                ParticipantId = pid,
            };

            newUser.registrationTimestamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.ffff") + 'Z';

            _db.Users.Add(newUser);
            await _db.SaveChangesAsync();

            inputUser.ParticipantId = newUser.ParticipantId;

            string? ip = Environment.GetEnvironmentVariable("admin_stack_ip");
            string? port = Environment.GetEnvironmentVariable("admin_stack_port");

            frontEndConfig.LoginUrl = "https://localhost:" + port + "/SPSO/Login";
            // Send email
            using (var client = new SmtpClient())
            {
                try
                {
                    var message = new MailMessage();
                    message.From = new MailAddress(emailConfig.FromName, emailConfig.FromName);
                    message.To.Add(new MailAddress(inputUser.Email!));
                    message.Subject = "Your ASIST account has been created";
                    message.IsBodyHtml = true;
                    message.Body = 
                        $@"

                        <p>Dear Participant,

                        <p>Thank you for registering your Minecraft character with the ASIST project!<br>

                        <p>The following account has been created for you:<br>
            
                        <p>Username : ""{inputUser.UserId}""<br>
                        <p>Password : ""{inputUser.Password}""<br>

                        <p>For instructions on how to install and play the game, please proceed to the <a href=""{"https://socialai.games/SPSO/Login"}"">login</a> page <br>. 

                        <p>Upon logging in to the server, you will be redirected to the Waiting Room. The Waiting Room will place you in a team of 3 as soon as 2 other players become available.<br> 

                        <p>For additional information about the experiment, the ASIST program, or the research being conducted, please visit artificialsocialintelligence.org. <br>
                        
                        <p>If you have any other questions, feel free to contact <a href=""{"support@socialai.games"}"">support@socialai.games</a>.<br>" 
                        ;

                    client.Host = emailConfig.Server;
                    client.Port = emailConfig.Port;

                    client.EnableSsl = true;
                    if (emailConfig.Password.Length == 0 || emailConfig.Password == null) { Console.WriteLine(" The SMTP EMAIL PASSWORD HAS NOT BEEN ENTERED!"); }
                    client.UseDefaultCredentials = false;
                    client.Credentials = new NetworkCredential(emailConfig.Email, emailConfig.Password);

                    client.Send(message);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error: " + emailConfig.Server + " " + emailConfig.Port + " " + inputUser.Email + " " + emailConfig.FromName);
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                }
            }
            Console.WriteLine("sending email");

            newUser.Password = tempPw;
            // return the userobj with password to front end
            return Created($"/users/{inputUser.UserId}", newUser);

        }

        [Route("authenticate")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult<User>> AuthenticateUser(UserLoginDTO inputUser)
        {
            string SSO_SECRET_KEY = Environment.GetEnvironmentVariable("SSO_SECRET_KEY");
            if (await _db.Users.Where(i => EF.Functions.ILike(i.UserId, inputUser.UserId)).FirstOrDefaultAsync() is User user && (EncryptPassword.Decrypt( user?.Password!, SSO_SECRET_KEY) == inputUser.Password))
            {
                user.Password = inputUser.Password;
                string token = this.GenerateToken();
                return Ok(new { authentication = true, user, token });
            }
            else {
                return NotFound(new { authentication = false });
            }
        }

        [Route("updateConsentTimestamp/{id}")]
        [HttpGet]        
        public async Task<ActionResult<string>> UpdateConsentTimestamp(string id)
        {
            if (await _db.Users.FindAsync(id) is User user)
            {
                user.lastConsentTimestamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.ffff") + 'Z'; 
                await _db.SaveChangesAsync();
                return Ok(user.lastConsentTimestamp);
            }

            return NotFound();
        }

        [Route("forgotPassword/{email}")]
        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<JsonObject>> ForgotPassword(string email)
        {
            Console.WriteLine("Forgot Password - EMAIL RECOVERY: " + email);

            //User user = await _db!.Users!.AsNoTracking()!.FirstOrDefaultAsync(i => i!.Email!.CompareTo(email!) == 0);
            IQueryable<User> users =  _db!.Users!.AsNoTracking()!.Where(i => i!.Email!.CompareTo(email!) == 0 );

            JsonObject obj = new JsonObject()
            {

                ["result"] = "Password recovery email successfilly sent to " + email + "."

            };

            if (users == null || users.Count()==0 )
            {
                obj["result"] = "The provided email was not found.";

                return NotFound(obj);
            }
            else
            {
                await users.ForEachAsync(user =>
                {

                    using (var client = new SmtpClient())
                    {
                        string SSO_SECRET_KEY = Environment.GetEnvironmentVariable("SSO_SECRET_KEY");
                        try
                        {
                            var message = new MailMessage();
                            message.From = new MailAddress(emailConfig.FromName, emailConfig.FromName);
                            message.To.Add(new MailAddress(user.Email!));
                            message.Subject = "Your ASIST account has been created";
                            message.IsBodyHtml = true;
                            message.Body =
                                $@"

                        <p>Dear Participant,

                        <p>Thank you for registering your Minecraft character with the ASIST project!<br>

                        <p>The following account has been created for you:<br>
            
                        <p>Username : ""{user.UserId}""<br>
                        <p>Password : ""{EncryptPassword.Decrypt(user.Password!, SSO_SECRET_KEY) }""<br>

                        <p>For instructions on how to install and play the game, please proceed to the <a href=""{"https://socialai.games/SPSO/Login"}"">login</a> page <br>. 

                        <p>Upon logging in to the server, you will be redirected to the Waiting Room. The Waiting Room will place you in a team of 3 as soon as 2 other players become available.<br> 

                        <p>For additional information about the experiment, the ASIST program, or the research being conducted, please visit artificialsocialintelligence.org. <br>
                        
                        <p>If you have any other questions, feel free to contact <a href=""{"support@socialai.games"}"">support@socialai.games</a>.<br>"
                                ;

                            client.Host = emailConfig.Server;
                            client.Port = emailConfig.Port;

                            client.EnableSsl = true;
                            if (emailConfig.Password.Length == 0 || emailConfig.Password == null) { Console.WriteLine(" The SMTP EMAIL PASSWORD HAS NOT BEEN ENTERED!"); }
                            client.UseDefaultCredentials = false;
                            client.Credentials = new NetworkCredential(emailConfig.Email, emailConfig.Password);

                            client.Send(message);

                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Error: " + emailConfig.Server + " " + emailConfig.Port + " " + user.Email + " " + emailConfig.FromName);
                            Console.WriteLine(ex.Message);
                        }
                    }

                });

                return Ok(obj);
            }
           
        }

        
        [HttpDelete("{id}")]
        public async Task<ActionResult<User>> DeleteUser(String id)
        {
            if (await _db.Users.FindAsync(id) is User user)
            {
                _db.Users.Remove(user);
                await _db.SaveChangesAsync();
                return Ok(user);
            }

            return NotFound();
        }


        [HttpDelete()]
        public async Task<ActionResult<User>> DeleteAllUsersAndCreateAdmin()
        {
            await _db.Users.ForEachAsync(user => _db.Users.Remove(user));
            string SSO_SECRET_KEY = Environment.GetEnvironmentVariable("SSO_SECRET_KEY");
            var users = new User[]
            {
                new User{UserId="admin", Password=EncryptPassword.Encrypt("administrator", SSO_SECRET_KEY), IsAdmin=true,  Email="", ParticipantId="admin"},
            };

            //context.PWKeys.AddRange(key);
            _db.Users.AddRange(users);

            await _db.SaveChangesAsync();
            return Ok();
        }

        [HttpPut("updateProfileByUserId{id}")]
        public async Task<ActionResult<User>> UpdateProfileByUserId(String id, UserLoginDTO inputUser, Boolean isAuthorized)
        {
            if (!isAuthorized) {
                return Unauthorized();
            }

            var user = await _db.Users.FindAsync(id);

            if (user is null) return NotFound();

            if (inputUser.Password is not null)
            {
                string SSO_SECRET_KEY = Environment.GetEnvironmentVariable("SSO_SECRET_KEY");
                user.Password = EncryptPassword.Encrypt(inputUser.Password, SSO_SECRET_KEY);
            }
            if (inputUser.UserId is not null) { user.UserId = inputUser.UserId; }
            if (inputUser.Email is not null) { user.Email = inputUser.Email; }
            if (inputUser.ParticipantId is not null) { user.ParticipantId = inputUser.ParticipantId; }

            await _db.SaveChangesAsync();

            return NoContent();
        }


        string GenerateToken()
        {

            string DB_API_ISSUER_KEY = Environment.GetEnvironmentVariable("DB_API_ISSUER_KEY");
            Console.WriteLine("DB_API_ISSUER_KEY LENGTH " + Encoding.UTF8.GetBytes(DB_API_ISSUER_KEY).Length);
            var mySecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(DB_API_ISSUER_KEY));

            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Expires =
                    DateTime.UtcNow.AddYears(Int32.Parse(_configuration["Jwt:Expiration:Years"]))
                    .AddMonths(Int32.Parse(_configuration["Jwt:Expiration:Months"]))
                    .AddDays(Int32.Parse(_configuration["Jwt:Expiration:Days"]))
                    .AddHours(Int32.Parse(_configuration["Jwt:Expiration:Hours"])),
                Issuer = _configuration["Jwt:Issuer"],
                Audience = _configuration["Jwt:Audience"],
                SigningCredentials = new SigningCredentials(mySecurityKey, SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }




    }
}
