using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Hosting;
using SSOService.DTO;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json;

namespace SSOService.Database.DBModels
{
    public class User
    {
        public string ParticipantId { get; set; } = "NOT_SET";   
        public string UserId { get; set; } = "NOT_SET";
        public string Password { get; set; } = "NOT_SET";
        public string Email { get; set; } = "NOT_SET";   
        public bool IsAdmin { get; set; } = false;
        public int ParticipationCount { get; set; } = 0;
        public List<string> TrialsList { get; set; } = new List<string>();

        [Column(TypeName = "jsonb")]
        public SurveysDTO? PreTrialSurveys { get; set; }
        [Column(TypeName = "jsonb")]
        public SurveysDTO? PostTrialSurveys { get; set; }      
        public string? registrationTimestamp { get; set; }
        public string? lastConsentTimestamp { get; set; }        
        public List<string> TeamsList { get; set; } = new List<string>();     
        
    }

}