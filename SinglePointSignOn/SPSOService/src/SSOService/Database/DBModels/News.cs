﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace SSOService.Database.DBModels
{
    public class News
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        [JsonPropertyName("news_id")]
        public Guid NewsId { get; set; } = Guid.NewGuid();
        [JsonPropertyName("creation_date")]
        public DateTime CreationDate { get; set; } = DateTime.UtcNow;
        [JsonPropertyName("content")]
        public string Content { get; set; } = "";

    }
}
