﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SSOService.Migrations
{
    public partial class registrationTimestamp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "registrationTimestamp",
                table: "Users",
                type: "text",
                nullable: true);

            migrationBuilder.AlterColumn<List<string>>(
                name: "NeverProceeded",
                table: "Teams",
                type: "text[]",
                nullable: false,
                oldClrType: typeof(List<string>),
                oldType: "text[]",
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "registrationTimestamp",
                table: "Users");

            migrationBuilder.AlterColumn<List<string>>(
                name: "NeverProceeded",
                table: "Teams",
                type: "text[]",
                nullable: true,
                oldClrType: typeof(List<string>),
                oldType: "text[]");
        }
    }
}
