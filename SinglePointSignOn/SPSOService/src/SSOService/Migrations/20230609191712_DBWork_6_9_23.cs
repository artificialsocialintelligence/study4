﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SSOService.Migrations
{
    public partial class DBWork_6_9_23 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "TotalStoreTime",
                table: "Trials",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddColumn<string>(
                name: "StartTimestamp",
                table: "Trials",
                type: "text",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StartTimestamp",
                table: "Trials");

            migrationBuilder.AlterColumn<int>(
                name: "TotalStoreTime",
                table: "Trials",
                type: "integer",
                nullable: false,
                oldClrType: typeof(long),
                oldType: "bigint");
        }
    }
}
