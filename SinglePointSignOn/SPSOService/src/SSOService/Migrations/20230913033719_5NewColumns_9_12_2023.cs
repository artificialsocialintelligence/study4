﻿using System.Text.Json;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SSOService.Migrations
{
    public partial class _5NewColumns_9_12_2023 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "LastActiveMissionTime",
                table: "Trials",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<JsonDocument>(
                name: "NumCompletePostSurveys",
                table: "Trials",
                type: "jsonb",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "NumFieldVisits",
                table: "Trials",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "NumStoreVisits",
                table: "Trials",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<JsonDocument>(
                name: "TextChatsSent",
                table: "Trials",
                type: "jsonb",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LastActiveMissionTime",
                table: "Trials");

            migrationBuilder.DropColumn(
                name: "NumCompletePostSurveys",
                table: "Trials");

            migrationBuilder.DropColumn(
                name: "NumFieldVisits",
                table: "Trials");

            migrationBuilder.DropColumn(
                name: "NumStoreVisits",
                table: "Trials");

            migrationBuilder.DropColumn(
                name: "TextChatsSent",
                table: "Trials");
        }
    }
}
