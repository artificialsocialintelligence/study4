﻿using System.Text;

namespace SSOService.Helpers
{
    public static class EncryptPassword
    {
        public static string Encrypt(string password, string key)
        {
            if (string.IsNullOrEmpty(password) || string.IsNullOrEmpty(key))
            {
                return "";
            }
            else
            {
                password = password + key;
                var passwordinBytes = Encoding.UTF8.GetBytes(password);
                return Convert.ToBase64String(passwordinBytes);
            }

        }
        public static string Decrypt(string encryptedPassword, string key)
        {
            if (string.IsNullOrEmpty(encryptedPassword) || string.IsNullOrEmpty(key))
            {
                return "";
            }
            else
            {
                var encodedBytes = Convert.FromBase64String(encryptedPassword);
                var actualPassword = Encoding.UTF8.GetString(encodedBytes);
                actualPassword = actualPassword.Substring(0, actualPassword.Length - key.Length);
                return actualPassword;
            }
        }

    }
}
