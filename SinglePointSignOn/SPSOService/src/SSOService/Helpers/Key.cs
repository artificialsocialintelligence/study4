﻿using System.ComponentModel.DataAnnotations;

namespace SSOService.Helpers
{
    public class PWKey
    {
        [Key]
        public byte[]? Seed { get; set; }

    }
}
