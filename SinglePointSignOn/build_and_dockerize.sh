#!/bin/bash

cd ./SPSO-FrontEnd/logon-page
ng build -c production
cd ../../SPSOService
docker build -t ssoservice:latest --build-arg CACHE_BREAKER=$(date +%s) .
cd ..
