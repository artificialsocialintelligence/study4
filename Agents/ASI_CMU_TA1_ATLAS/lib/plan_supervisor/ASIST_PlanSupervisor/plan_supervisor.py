# -*- coding: utf-8 -*-
"""
.. module:: plan_supervisor
   :platform: Linux, Windows, OSX
   :synopsis: PlanSupervisor top-level component class

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>


"""

import json

from MinecraftBridge.utils import Loggable
from MinecraftBridge.messages import (
	MissionStageTransition,
	MissionStage
)

from .ingesters import (
	CognitiveArtifactIngester,
	DialogAgentIngester
)

from .representation import Plan, NullPlan
from .representation.query import QueryBy, SortBy

from .monitors import (
	PlanExecutionMonitor,
	PlanAdherenceMonitor
)

from .message_gateways import (
	PlanQueryGateway,
	PlanDeviationGateway
)

class PlanSupervisor(Loggable):
	"""
	A PlanSupervisor acts as a light-weight top-level component for plan
	monitoring components and plan representation structures
	"""

	def __init__(self, mission_context, config={}):
		"""
		Arguments
		---------
		mission_context
			Context object containing mission relevant information
		config : dict
			Dictionary containing configuration information for this component
		"""

		self.logger.debug(f'{self}: Initiating PlanSupervisor')

		self._minecraft_interface = mission_context.minecraft_interface
		self._redis_interface = mission_context.redis_interface
		self._participants = mission_context.participants

		# Construct a plan representation and the cognitive artifiact ingester
		self._cognitive_artifact_ingester = CognitiveArtifactIngester(self, self._minecraft_interface)
		self._dialog_agent_ingester = DialogAgentIngester(self, self._minecraft_interface)

		# Maintain a library of plans, and the current active plan
		self._plan_library = {}
		self._current_plan = None

		# Set up a plan query gateway to be able to respond to requests regarding
		# plan state and information
		plan_deviation_gateway = PlanDeviationGateway(self, self._redis_interface)
		self._gateways = [ PlanQueryGateway(self, self._redis_interface),
		                   plan_deviation_gateway
		                 ]

		# Set up individual monitors
		self._monitors = [ PlanExecutionMonitor(self, None, self._minecraft_interface),
		                   PlanAdherenceMonitor(self, None, self._minecraft_interface, plan_deviation_gateway)
		                 ]


	def __str__(self):
		"""
		"""

		return self.__class__.__name__


	@property
	def library(self):
		return self._plan_library


	@property
	def participants(self):
		return self._participants
	

	def start(self):
		self.logger.debug(f'{self}:  Starting')

		# Register to receive StageTransition messages
		self._minecraft_interface.register_callback(MissionStageTransition, self.__onStageTransition)

		for gateway in self._gateways:
			gateway.connect()

		# Start the ingester(s)
		self._cognitive_artifact_ingester.start()
		self._dialog_agent_ingester.start()


	def stop(self):
		self.logger.debug(f'{self}:  Stopping')

		self.__pretty_print_plan_state(self._current_plan)

		# Stop the ingester(s)
		self._cognitive_artifact_ingester.stop()
		self._dialog_agent_ingester.stop()

		# Stop listening to the MinecraftBridge
		self._minecraft_interface.deregister_callback(MissionStageTransition, self.__onStageTransition)

		for monitor in self._monitors:
			monitor.disconnect()

		for gateway in self._gateways:
			gateway.disconnect()


	def __pretty_print_plan_state(self, plan):
		"""
		Debug method for printing out the current state of a plan
		"""

		# Don't print anything out if the plan doesn't exist
		if plan is None:
			return

		self.logger.info(f'{self}:  Plan State for {plan}')

		self.logger.info(f'{self}:      All Plan Steps')
		for step in plan.query(lambda x: x, SortBy.StepNumber):
			complete = 'Complete' if step.is_complete else 'Incomplete'
			self.logger.info(f'{self}:        Step #{step.sequence_number}:\t{step.action}\tPrioritization: {step.prioritization}; Time: {step.schedule}; {complete}')

		for participant in self._participants:
			self.logger.info(f'{self}:      Participant {participant.id} Steps')
			for step in plan.query(QueryBy.Actor(participant.id), SortBy.StepNumber):
				complete = 'Complete' if step.is_complete else 'Incomplete'
				self.logger.info(f'{self}:        Step #{step.sequence_number}:\t{step.action}\tPrioritization: {step.prioritization}; Time: {step.schedule}; {complete}')


	def __onStageTransition(self, message):
		"""
		Callback when Mission Stage changes
		"""

		self.logger.info(f'{self}:  Mission Stage Transition: {message.mission_stage}')

		# If the players enter the shop phase, create a new plan, attach it to
		# the cognitive artifact ingester, and start the ingester(s)
		if message.mission_stage in [ MissionStage.ReconStage, MissionStage.ShopStage ]:
			# For now, we'll maintain a single plan instance (self._current_plan),
			if self._current_plan == None:
				self._current_plan = Plan("plan")
				self._plan_library[self._current_plan.name] = self._current_plan
				for monitor in self._monitors:
					monitor.plan = self._current_plan

			if not "dialog_agent_plan" in self._plan_library:
				self._plan_library["dialog_agent_plan"] = Plan("dialog_agent_plan")

			# Have the ingester work on the current plan
			self._cognitive_artifact_ingester.plan = self._current_plan
			self._dialog_agent_ingester.plan = self._plan_library["dialog_agent_plan"]

###			# Start the ingester(s)
###			self._cognitive_artifact_ingester.start()

			# Set the plan monitors to development mode
			for monitor in self._monitors:
				monitor.development_mode()


		# If the players enter a field phase, then stop the ingester(s)
		elif message.mission_stage in [ MissionStage.FieldStage ]:
			
###			# Stop the ingester(s)
###			self._cognitive_artifact_ingester.stop()

			if self._current_plan is not None:

				self.__pretty_print_plan_state(self._current_plan)

			for monitor in self._monitors:
				monitor.execution_mode()

		else:
			self.logger.warning(f'{self}:  Unsure how to handle unknown mission stage: {message.mission_stage}')



	def reset(self, **kwargs):
		"""

		"""

		self.logger.debug(f'{self}:  Received reset, setting up representation for all participants')

