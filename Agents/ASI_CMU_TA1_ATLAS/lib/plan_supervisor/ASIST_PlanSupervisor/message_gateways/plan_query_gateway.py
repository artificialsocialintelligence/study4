# -*- coding: utf-8 -*-
"""
.. module:: plan_query_gateway
   :platform: Linux, Windows, OSX
   :synopsis: Definition of a gateway to handle queries about plans

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

The PlanQueryGateway class handles queries and responses regarding plans and
subplans.
"""

from MinecraftBridge.utils import Loggable

from ..representation.query import QueryBy, SortBy

class PlanQueryGateway(Loggable):
   """
   The PlanQueryGateway is responsible for listening for queries and generating
   responses regarding plans and plan states.
   """

   # Message Channels for different requests / responses
   PLAN_QUERY_CHANNEL = 'plan_supervisor/query/plan'
   FULL_PLAN_QUERY_CHANNEL = 'plan_supervisor/query/plan/full'
   NEXT_STEP_QUERY_CHANNEL = 'plan_supervisor/query/next_step'
   PLAN_NAMES_QUERY_CHANNEL = 'plan_supervisor/query/plan_names'
   PLAN_RESPONSE_CHANNEL = 'plan_supervisor/response/plan'
   NEXT_STEP_RESPONSE_CHANNEL = 'plan_supervisor/response/next_step'
   PLAN_NAMES_RESPONSE_CHANNEL = 'plan_supervisor/response/plan_names'


   @staticmethod
   def construct_query_from_dict(query_dict):
      """
      Decodes a dictionary encoding of a query into a query function.  The
      query_dict contains two fields: `type`, which provides a string indicating
      the type of query function, and `parameters`, which are used as arguments
      when constructing the query.

      Query Types
      -----------
      - 'has-actor'
        - Queries for plan steps that contain at least one assigned actor in
          the list of actors in `actors` parameter
        - parameters:
          - 'actors'
      - 'contains-response'
        - Queries for plan steps that contain a response whose response text
          matches the given `response_text` parameter
        - parameters:
          - 'response_text'
      """

      query_type = query_dict.get('type', '')
      parameters = query_dict.get('parameters', {})

      # Generate the query function based on type
      if query_type == 'contains-response':
         query = QueryBy.ContainsResponseText(parameters.get('response_text', '<NONE_PROVIDED>'))
      else:
         # Cannot construct a query, so simply return the identity
         query = lambda x: x

      return query

   
   def __init__(self, supervisor, redis_interface):
      """
      Arguments
      ---------
      supervisor : PlanSupervisor
         Top-level component hosting this gateway
      redis_interface : RedisBridge.CallbackInterface
         Interface to the internal message bus
      """

      self.logger.info(f'{self}:  Initializing PlanQueryGateway')

      self._supervisor = supervisor
      self._redis_interface = redis_interface


   def __str__(self):
      """
      String representation of the object
      """

      return self.__class__.__name__


   def connect(self):
      """
      Connect to the RedisBridge, and begin listening to channels of interest
      """

      self.logger.debug(f'{self}:  Registering callbacks for Redis bus messages')

      self._redis_interface.register_callback(self.__onPlanQuery, 
                                              PlanQueryGateway.PLAN_QUERY_CHANNEL)
      self._redis_interface.register_callback(self.__onFullPlanQuery, 
                                              PlanQueryGateway.FULL_PLAN_QUERY_CHANNEL)      
      self._redis_interface.register_callback(self.__onNextStepQuery,
                                              PlanQueryGateway.NEXT_STEP_QUERY_CHANNEL)
      self._redis_interface.register_callback(self.__onPlanNamesQuery,
                                              PlanQueryGateway.PLAN_NAMES_QUERY_CHANNEL)


   def disconnect(self):
      """
      Disconnect from the RedisBridge
      """

      self.logger.debug(f'{self}:  Deregistering callbacks for Redis bus')

      self._redis_interface.deregister_callback(self.__onPlanQuery)
      self._redis_interface.deregister_callback(self.__onFullPlanQuery)
      self._redis_interface.deregister_callback(self.__onNextStepQuery)
      self._redis_interface.deregister_callback(self.__onPlanNamesQuery)


   def __getParticipantStepsDict(self, plan, participant, queries=[]):
      """
      Helper function to extract participant's steps from the given plan, and
      convert it to a dictionary representation.

      Arguments
      ---------
      plan : representation.Plan
          Plan to extract steps from
      participant_id : str
          Participant of interest
      queries : List[function]
          List of functions to query the plan by.  The final query is assumed
          to be a conjunction of all query responses
      """

      steps = []

      query_list = [QueryBy.Actor(participant)] + queries

      for step in plan.query(QueryBy.And(*query_list), SortBy.StepNumber):

         steps.append(step.serialize()) 

      return steps



   def __onPlanQuery(self, message):
      """
      Callback when a plan query message is received.  The received message
      payload should be a dictionary containing the fields described in the
      Query Schema below.  The response payload will be a dictionary containing
      the fields in the Response Schema below.

      Message Channel:  plan_supervisor/query/plan
      Response Channel: plan_supervisor/response/plan

      Query Schema
      ------------
      'name': string name of the plan.  If None, then returns the _current_plan
      'participants': list of participant IDs.  If None, then assumes all
                      participants.
      'conditions': list of conditions that the returned steps need to satisfy
                    (optional)

      Query Response
      --------------
      'name': string name of the plan (None if the plan was not found)
      'exists': boolean indicating if a plan by that name was found
      'steps': dictionary mapping participant IDs to list of Step dictionaries

      Step Schema
      -----------
      'step_number': integer number of the step
      'action': action to be performed
      'complete': boolean indicating if the step has been completed
      'prerequisites': list of prerequisite conditions (not yet implemented)
      'actors': list of participant ids involved in the action
      """

      self.logger.debug(f'{self}:  Received Plan Query Message {message}')
      self.logger.debug(f'{self}:    Query : {message.data}')

      # Get the message data and check that it contains the required fields
      if not 'name' in message.data:
         self.logger.error(f'{self}:  Query does not contain `name` field.')
         return

      if not 'participants' in message.data:
         self.logger.error(f'{self}:  Query does not contain `participants` field.')
         return

      plan_name = message.data['name']
      participants = message.data['participants']

      # If no plan name is provided, use the current plan
      if plan_name is None or plan_name == "":
         plan = self._supervisor._current_plan
      else:
         plan = self._supervisor.library.get(plan_name, None)

      # If no participants are provided, return steps for each participant
      if participants is None:
         participants = [p.id for p in self._supervisor._participants]

      # Construct a query for each condition passed
      queries = [PlanQueryGateway.construct_query_from_dict(q) 
                 for q in message.data.get('conditions', [])]


      # If the plan was not found (i.e., is None), then create the response and return
      if plan is None:
         response = { 'name': None,
                      'exists': False,
                      'steps': { p: [] for p in participants }
                    }

      else:
         # Create the response and send
         response = { 'name': plan.name,
                      'exists': True,
                      'steps': { p: self.__getParticipantStepsDict(plan, p, queries) for p in participants}
                    }

      self.logger.debug(f'{self}:    Response: {response}')

      self._redis_interface.respond(response, PlanQueryGateway.PLAN_RESPONSE_CHANNEL, message.id)


   def __onFullPlanQuery(self, message):
      """
      Callback when a plan query message is received.  The received message
      payload should be a dictionary containing the fields described in the
      Query Schema below.  The response payload will be a dictionary containing
      the fields in the Response Schema below.

      Message Channel:  plan_supervisor/query/plan/full
      Response Channel: plan_supervisor/response/plan

      Query Schema
      ------------
      'name': string name of the plan.  If None, then returns the _current_plan
      'conditions': list of conditions that the returned steps need to satisfy
                    (optional)

      Query Response
      --------------
      'name': string name of the plan (None if the plan was not found)
      'exists': boolean indicating if a plan by that name was found
      'steps': dictionary mapping 'full' to list of Step dictionaries

      Step Schema
      -----------
      'step_number': integer number of the step
      'action': action to be performed
      'complete': boolean indicating if the step has been completed
      'prerequisites': list of prerequisite conditions (not yet implemented)
      'actors': list of participant ids involved in the action
      """

      self.logger.debug(f'{self}:  Received Plan Query Message {message}')
      self.logger.debug(f'{self}:    Query : {message.data}')   

      # Get the message data and check that it contains the required fields
      if not 'name' in message.data:
         self.logger.error(f'{self}:  Query does not contain `name` field.')
         return

      plan_name = message.data['name']

      # If no plan name is provided, use the current plan
      if plan_name is None or plan_name == "":
         plan = self._supervisor._current_plan
      else:
         plan = self._supervisor.library.get(plan_name, None)

      # Construct a query for each condition passed -- need at least one query,
      # so if one isn't provided, create an identity function
      queries = [PlanQueryGateway.construct_query_from_dict(q) 
                 for q in message.data.get('conditions', [])]
      if len(queries) == 0:
         queries = [lambda x: x]


      # If the plan was not found (i.e., is None), then create the response and return
      if plan is None:
         response = { 'name': None,
                      'exists': False,
                      'steps': { 'full': [] }
                    }

      else:
         # Create the response and send
         steps = []

         for step in plan.query(QueryBy.And(*queries), SortBy.StepNumber):
            steps.append(step.serialize()) 

         response = { 'name': plan.name,
                      'exists': True,
                      'steps': { 'full': steps }
                    }

      self.logger.debug(f'{self}:    Response: {response}')

      self._redis_interface.respond(response, PlanQueryGateway.PLAN_RESPONSE_CHANNEL, message.id)


   def __onNextStepQuery(self, message):
      """
      Callback when a next step query message is received.  The received 
      message payload should be a dictionary containing the fields described in
      the Query Schema below.  The response payload will be a dictionary 
      containing the fields in the Response Schema below.

      Message Channel:  plan_supervisor/query/next_step
      Response Channel: plan_supervisor/response/next_step

      Query Schema
      ------------
      'name': string name of the plan.  If None, then returns the _current_plan
      'participant': participant ID to get the step for

      Query Response
      --------------
      'name': string name of the plan (None if the plan was not found)
      'exists': boolean indicating if a plan by that name was found
      'step': dictionary containing step info, or None if no next step

      Step Schema
      -----------
      'step_number': integer number of the step
      'action': action to be performed
      'complete': boolean indicating if the step has been completed
      'prerequisites': list of prerequisite conditions (not yet implemented)
      'actors': list of participant ids involved in the action
      """

      self.logger.debug(f'{self}:  Received Next Step Query Message {message}')
      self.logger.debug(f'{self}:    Query : {message.data}')
      
      # Get the message data and check that it contains the required fields
      if not 'name' in message.data:
         self.logger.error(f'{self}:  Query does not contain `name` field.')
         return

      if not 'participant' in message.data:
         self.logger.error(f'{self}:  Query does not contain `participant` field.')
         return

      plan_name = message.data['name']
      participant = message.data['participant']


      if plan_name is None:
         plan = self._supervisor._current_plan
      else:
         plan = self._supervisor.library.get(plan_name, None)

      if participants is None:
         participants = [p.id for p in self._supervisor._participants]

      # If the plan was not found (i.e., is None), then create the response and return
      if plan is None:
         response = { 'name': None,
                      'exists': False,
                      'step': None
                    }
         self._redis_interface.respond(response, channel=PlanQueryGateway.NEXT_STEP_RESPONSE_CHANNEL)
         return

      # Get the steps for the participant, filter out the incomplete steps, and
      # extrat the first one (if available)
      steps = self.__getParticipantStepsDict(plan, participant)
      steps = { s for s in steps if not s['complete'] }

      # Create the response and send
      response = { 'name': plan.name,
                   'exists': True,
                   'step': None if len(steps) == 0 else steps[0]
                 }

      self._redis_interface.respond(response, channel=PlanQueryGateway.NEXT_STEP_RESPONSE_CHANNEL)


   def __onPlanNamesQuery(self, message):
      """
      Callback when a plan names query message is received.  The received message
      payload is ignored.  The response payload will be a dictionary containing
      the fields in the Response Schema below.

      Message Channel:  plan_supervisor/query/plan_names
      Response Channel: plan_supervisor/response/plan_names

      Query Response
      --------------
      'names': list string name of the plans in the dictionary
      """

      self.logger.debug(f'{self}:  Received Plan Names Query Message {message}')

      # Create the response and send
      response = { 'names': [plan.name for plan in self._supervisor.library] }

      self._redis_interface.respond(response, channel=PlanQueryGateway.PLAN_NAMES_RESPONSE_CHANNEL)