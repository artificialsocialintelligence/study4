# -*- coding: utf-8 -*-
"""
.. module:: plan_deviation_gateway
   :platform: Linux, Windows, OSX
   :synopsis: Definition of a gateway to handle generating deviation alerts

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

The PlanDeviationGateway class handles generating alert messages whenever a
participant deviates from their next expected plan step.
"""

from MinecraftBridge.utils import Loggable

from ..representation.query import QueryBy, SortBy

class PlanDeviationGateway(Loggable):
   """
   The PlanDeviationGateway is responsible for generating alert messages when
   a participant deviates from their next expected plan step.  This class is
   expected to be used in conjunction with a PlanAdherenceMonitor instance.
   """

   # Message Channels for different requests / responses
   PLAN_DEVIATION_CHANNEL = 'plan_supervisor/alert/plan_deviation'

   
   def __init__(self, supervisor, redis_interface):
      """
      Arguments
      ---------
      supervisor : PlanSupervisor
         Top-level component hosting this gateway
      redis_interface : RedisBridge.CallbackInterface
         Interface to the internal message bus
      """

      self.logger.debug(f'{self}:  Initiating PlanQueryGateway')

      self._supervisor = supervisor
      self._redis_interface = redis_interface


   def __str__(self):
      """
      String representation of the object
      """

      return self.__class__.__name__


   def connect(self):
      """
      Connect to the RedisBridge, and begin listening to channels of interest

      NOTE:  This gateway only _publishes_ alert messages, and shouldn't listen
             to any external messages
      """

      self.logger.debug(f'{self}:  Registering callbacks for Redis bus messages')


   def disconnect(self):
      """
      Disconnect from the RedisBridge
      """

      self.logger.debug(f'{self}:  Deregistering callbacks for Redis bus')


   def onDeviation(self, participant_id, plan_step, plan, message_history):
      """
      Callback from a PlanAdherenceMonitor whenever a deviation to the plan is
      detected.  The message payload will be a dictionary containing the fields
      described in the Deviation Schema below.

      Message Channel:  plan_supervisor/alert/deviation

      Arguments
      ---------
      participant_id : str
         Participant who deviated from the plan
      plan_step : PlanStep
         Plan step that was deviated
      message_history : List[MinecraftBridge.message]
         List of relevent messages demonstrating the deviation

      Deviation Schema
      ----------------
      'name': string name of the plan
      'participant_id': Participant who deviated from the plan
      'step_number': Step number of the plan
      'action': Action being performed
      """

      self.logger.debug(f'{self}:  Publishing Plan Deviation message')

      # Construct the repsonse
      repsonse = { 'name': None if plan is None else plan.name,
                   'particiapnt_id': participant_id,
                   'step_number': plan_step.step_number,
                   'action': plan_step.action.__class__.__name__
                 }

      self._redis_interface.respond(response, channel=PlanDeviationGateway.PLAN_DEVIATION_CHANNEL)