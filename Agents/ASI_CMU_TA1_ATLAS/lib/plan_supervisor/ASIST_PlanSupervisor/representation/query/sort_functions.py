class SortBy:
	"""
	Static functions for sorting filtered results.  Each sort function takes a
	list of steps as input, and returns a sorted version of the list, in
	ascending order.  If the optional `descending` argument is True, then the
	sort is reversed
	"""

	def StepNumber(steps, descending=True):
		"""
		Sort by PlanStep's step number
		"""

		sorting_list = [(step.sequence_number, step) for step in steps]
		sorting_list.sort()

		sorted_list = [item[1] for item in sorting_list]

		if descending:
			sorted_list.reverse()

		return sorted_list