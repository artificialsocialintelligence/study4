# -*- coding: utf-8 -*-
"""
.. module:: representation.region_visitation_sequence
   :platform: Linux, Windows, OSX
   :synopsis: Region Visitation Sequence Representation class

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Class for representing the sequence of map regions a participant intends to 
visit.  During a shop phase, the class monitors flag placement of participants
and maintains the order of map regions the participant indicates they'll visit.
"""

from collections import namedtuple

from MinecraftBridge.utils import Loggable

from MinecraftBridge.messages import UI_Click
from MinecraftBridge.messages import MissionStageTransition, MissionStage

"""
A PlanStep is a simple class that maintains information about each step
in a plan
"""

class PlanStep:
   """
   A PlanStep is a simple class for maintaining data regarding each step of a
   plan.

   Attributes
   ----------
   step_number : int
      Step number of the plan
   participant_id : str
      ID of the participant who intents to execute this step
   cognitive_artifact_location : Tuple[int]
      (x,y) location of where on the cognitive artifiact this step is located
   world_location : Tuple[float]
      (x,y,z) location in the world corresponding to this plan step
   map_section : str
      Map section (e.g., "E4") where this plan step is located
   completed : boolean
      Boolean indicating if this plan step has been completed (i.e., through
      visitation)
   """

   def __init__(self, step_number, participant_id, cognitive_artifact_location,
                      world_location, map_section):
      """
      """

      self._step_number = step_number
      self._participant_id = participant_id
      self._cognitive_artifact_location = cognitive_artifact_location
      self._world_location = world_location
      self._map_section = map_section
      self._completed = False


   def __str__(self):
      """
      """

      return self.__class__.__name__


   def __repr__(self):
      """
      """

      return self.__class__.__name__ + f'[Step #{self._step_number}; Participant {self._participant_id}; Map Section {self._map_section}; Completed={self._completed}]'

   @property
   def step_number(self):
      return self._step_number

   @property
   def participant_id(self):
      return self._participant_id

   @property
   def cognitive_artifact_location(self):
      return self._cognitive_artifact_location

   @property
   def world_location(self):
      return self._world_location

   @property
   def map_section(self):
      return self._map_section

   @property
   def completed(self):
      return self._completed

   @completed.setter
   def completed(self, done):
      self._completed = done



class ShopPhaseMonitor(Loggable):
   """
   The ShopPhaseMonitor is monitors the Minecraft bridge for messages during
   the Shop phase, and update its the RegionVisitationSequence representation.
   """

   def __init__(self, representation, minecraft_interface):
      """
      Arguments
      ---------
      representation : RegionVisitationSequence
         RegionVisitationSequence 
      minecraft_interface : 
         Interface to the Minecraft Bridge callback wrapper
      """

      self._representation = representation
      self._minecraft_interface = minecraft_interface

      self._active = False


   def __str__(self):
      """
      """

      return self.__class__.__name__ + '[' + self._representation.participant_id + ']'


   @property
   def active(self):
      return self._active


   def activate(self):
      """
      Connects to the Minecraft Bridge and begins to 
      """

      self.logger.debug('{self}: Activating')

      # Start listening for UI click messages
      self._minecraft_interface.register_callback(UI_Click, self.__onUI_Click)

      self._active = True


   def deactivate(self):
      """
      """

      self.logger.debug('{self}: Deactivating')

      # Stop listening for UI click messages
      self._minecraft_interface.deregister_callback(self.__onUI_Click, UI_Click)

      self._active = False


   def __onUI_Click(self, message):
      """
      Callback when a UI_Click message is received

      Arguments
      ---------
      message : MinecraftBridge.messages.UI_Click
      """

#      self.logger.debug(f'{self}:  Received message {message}')
#      self.logger.debug(f'{self}:    Participant ID: {message.participant_id}')
#      self.logger.debug(f'{self}:    Additional Info: {message.additional_info}')

      # Check to see if the message is about the participant of interest.  If 
      # not, no reason to continue looking at this
      if message.participant_id != self._representation.participant_id:
         return

      # If it's a PLANNING_FLAG_PLACED or UNDO_PLANNING_FLAG_PLACED meta-action
      # then indicate to the representation that a planning step has been added
      # or removed, respectively
      if message.additional_info.get('meta_action','') == "PLANNING_FLAG_PLACED":

         self.logger.debug(f'{self}:    Adding a planning step')
         self.logger.debug(f'{self}:      Step #{message.additional_info["flag_index"]}')
         self.logger.debug(f'{self}:      Location: ({message.x}, {message.y})')

         # Create a Plan Step using this info
         plan_step = PlanStep(message.additional_info.get('flag_index', -1),
                              message.participant_id,
                              (message.x, message.y),
                              (-1,-1,-1),                   # TODO:  Find the corresponding world coordinates
                              "E4"                          # TODO:  Find the corresponding map section
                             )

         self._representation.add_planning_step(plan_step, message)


      elif message.additional_info.get('meta_action', '') == "UNDO_PLANNING_FLAG_PLACED":

         self.logger.debug(f'{self}:    Removing a planning step')
         self.logger.debug(f'{self}:      Step #{message.additional_info["flag_index"]}')
         self.logger.debug(f'{self}:      Location: ({message.x}, {message.y})')

         self._representation.remove_planning_step(message.additional_info["flag_index"])

      else:
         # Nothing to do with this UI click message, simply return
         return



class FieldPhaseMonitor(Loggable):
   """
   The FieldPhaseMonitor is responsible for monitoring the Minecraft bridge for
   messages during the Field phase, and updates the RegionVisitationSequence
   representation as events occur in the region.
   """

   def __init__(self, representation, minecraft_interface):
      """
      Arguments
      ---------
      minecraft_interface : 
         Interface to the Minecraft Bridge callback wrapper
      """

      self._representation = representation
      self._minecraft_interface = minecraft_interface

      self._active = False


   def __str__(self):
      """
      """

      return self.__class__.__name__


   @property
   def active(self):
      return self._active
   

   def activate(self):
      """
      """

      self.logger.debug('{self}: Activating')

      self._active = True


   def deactivate(self):
      """
      """

      self.logger.debug('{self}: Deactivating')

      self._active = False



class RegionVisitationSequence(Loggable):
   """

   Attributes
   ----------
   parent : object
      Plan recognition module that is the parent of this component.
   participant_id : string
      Identifier of the participant whose plan is being monitored.
   """

   def __init__(self, parent, participant_id, bridge_interface, redis_interface=None):
      """
      """

      self._parent = parent
      self._participant_id = participant_id
      self._minecraft_interface = bridge_interface
      self._redis_interface = redis_interface

      # Simple representation of the plan -- map a step number to a PlanStep 
      # representation
      self._planning_steps = dict()

      # Create monitors for the field and shop phases
      self._field_monitor = FieldPhaseMonitor(self, self._minecraft_interface)
      self._shop_monitor = ShopPhaseMonitor(self, self._minecraft_interface)

      # Listen for when mission stage transitions occur
      self._minecraft_interface.register_callback(MissionStageTransition, self.__onStageTransition)

      self.logger.debug(f'{self}:  Created RegionVisitationSequence representation for participant {participant_id}')


   def __str__(self):

      return self.__class__.__name__ + '[' + self._participant_id + ']'


   @property
   def participant_id(self):
      return self._participant_id


   @property
   def parent(self):
      return self._parent


   def __onStageTransition(self, message):
      """
      Callback when a MissionStageTransition message is received
      """

      self.logger.debug(f'{self}: Recieved message {message}')

      # Determine if the new stage is a shop stage or field stage, and activate
      # and deactive the monitors appropriately
      if message.mission_stage == MissionStage.ShopStage:
         self._field_monitor.deactivate()
         self._shop_monitor.activate()

      elif message.mission_stage == MissionStage.FieldStage:
         self._field_monitor.activate()
         self._shop_monitor.deactivate()

      else:
         self.logger.warning(f'{self}: Unknown mission stage: {message.mission_stage}')
         return


   def add_planning_step(self, plan_step, message=None):
      """
      Add a planning step to the representation.

      Arguments
      ---------
      plan_step : PlanStep
         Plan step to add to the representation
      message : MinecraftBridge.messages.UI_Click, optional, default=None
         Message which resulted in this planning step being added
      """

      self.logger.info(f'{self}:  Adding PlanStep: {repr(plan_step)}')

      if plan_step.step_number in self._planning_steps:
         self.logger.warning(f'{self}:  Overwriting existing plan step at step number {plan_step.step_number}')

      self._planning_steps[plan_step.step_number] = plan_step


   def remove_planning_step(self, plan_step_number, message=None):
      """
      Remove a planning step from the representation

      Arguments
      ---------
      plan_step_number : int
         Which step number to remove
      message : MinecraftBridge.message.UI_Click, optional, default=None
         Message which resulted in this planning step being removed
      """

      self.logger.info(f'{self}:  Removing PlanStep number {plan_step_number}')

      if not plan_step_number in self._planning_steps:
         self.logger.warning(f'{self}:  Plan step number {plan_step_number} not present in representation')
         return

      del self._planning_steps[plan_step_number]


   def get_plan(self):
      """
      Get a dictionary mapping plan step number to plan step information

      Returns
      -------
      Dictionary mapping plan step numbers to ???
      """

      plan = dict()

      for step_number, plan_step in self._planning_steps.items():
         plan[step_number] = { 'map_section': plan_step.map_section,
                               'completed': plan_step.completed }

      return plan


   def get_next_step(self):
      """
      Get a dictionary containing the step number and map section of the next
      uncompleted step
      """

      next_step = dict()
      found = False 

      # Get a sorted list of step numbers to iterate over
      step_numbers = list(self._planning_steps.keys())
      step_numbers.sort()

      for step_number in step_numbers:
         # Only consider this step if the next_step is not found
         if not found:
            if not self._planning_steps[step_number].completed:
               found = True
               next_step = { 'step_number': step_number,
                             'map_section': self._planning_steps[step_number].map_section }

      return next_step


