# -*- coding: utf-8 -*-
"""
.. module:: plan_monitor.plan_components.actions.go_to_waypoint
   :platform: Linux, Windows, OSX
   :synopsis: Plan Step indicating that a player intends to go to a region

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

GoToWaypoint is an indicating that a participant plans on going to a
flagged waypoint.
"""

from .action import Action

class GoToWaypoint(Action):
   """
   """

   def __init__(self, location, **kwargs):
      """
      Arguments
      ---------
      step_number : int
         Which step number is this?
      participant_id : string
         Who does this apply to?
      location : Tuple[int]
         (x,z) location in the world of the waypoint
      """

      Action.__init__(self, location)

###      self._step_number = step_number
###      self._participant_id = participant_id


###   @property
###   def step_number(self):
###      return self._step_number
###
###   @property
###   def participant_id(self):
###      return self._participant_id

   def serialize(self):
      """
      Return a dictionary representation of this action
      """

      serialized_dict = Action.serialize(self)

      return serialized_dict


   def clone(self):
      """
      Return a copy of this action.
      """

      return GoToWaypoint(self._step_number, self._participant_id,
                          self._location)
   
