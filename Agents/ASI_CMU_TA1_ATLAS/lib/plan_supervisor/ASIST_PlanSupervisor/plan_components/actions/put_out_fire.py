# -*- coding: utf-8 -*-
"""
.. module:: plan_monitor.plan_components.actions.address_hazard
   :platform: Linux, Windows, OSX
   :synopsis: Plan Step indicating that fire needs to be put out

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

PutOutFire is an action indicating that there is fire to be put out.
"""

from .action import Action

class PutOutFire(Action):
   """
   """

   def __init__(self, location, **kwargs):
      """
      Arguments
      ---------
      step_number : int
         Which step number is this?
      participant_id : string
         Who does this apply to?
      location : Tuple[int]
         (x,z) location
      """

      Action.__init__(self, location)

###      self._step_number = step_number
###      self._participant_id = participant_id

###   @property
###   def step_number(self):
###      return self._step_number
###
###   @property
###   def participant_id(self):
###      return self._participant_id

   def serialize(self):
      """
      Return a dictionary representation of this action
      """

      serialized_dict = Action.serialize(self)

      return serialized_dict


   def clone(self):
      """
      Return a copy of this action.
      """

      return PutOutFire(self._location)