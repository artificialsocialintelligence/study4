# -*- coding: utf-8 -*-
"""
.. module:: plan_monitor.plan_components.actions.action
   :platform: Linux, Windows, OSX
   :synopsis: Abstract action class

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Action defines an abstract base class with common functionality
"""

class Action:
   """
   Action defines properties and methods common to all actions.

   Properties
   ----------
   location : Tuple[float]
      (x,z) location of the action to be performed
   actors : Set[str]
      Set of participant IDs associated with the action
   """

   def __init__(self, location):
      """
      Arguments
      ---------
      location : Tuple[int]
         (x,z) location
      """

      self._location = location

      self._actors = set()


   @property
   def location(self):
      return self._location

   @location.setter
   def location(self, location):
      self._location = location

   @property
   def actors(self):
      return self._actors
   

   def __str__(self):
      """
      String representation of this step
      """

      return f'{self.__class__.__name__} <location: {str(self._location)}; actors: {str(self._actors)}>'


   def add_actor(self, actor_id):
      """
      Add an identifier of an actor who will be involved in this action

      Arguments
      ---------
      actor_id
         Identifier of the actor who will be involved in this action
      """

      self._actors.add(actor_id)


   def remove_actor(self, actor_id):
      """
      Remove an actor from being involved with this action

      Arguments
      ---------
      actor_id
         Actor identifier who shouldn't be involved in this action
      """

      self._actors.discard(actor_id)


   def serialize(self):
      """
      Create a dictionary representation of the action
      """

      return { 'type': self.__class__.__name__,
               'location': self.location,
               'actors': list(self.actors)
             }
   

   def clone(self):
      """
      Return a copy of this action.
      """

      raise NotImplementedError