# -*- coding: utf-8 -*-
"""
.. module:: plan_monitor.plan_components.plan_step
   :platform: Linux, Windows, OSX
   :synopsis: Class representing atomic steps in plans

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

"""

import collections

from MinecraftBridge.utils import Loggable

class PlanStep(Loggable):
   """
   A PlanStep is an atomic step to be performed in a plan.

   Attributes
   ----------
   id
      Unique identifier for the plan step
   action : plan_component.Action
      Action to perform at this plan step
   is_complete : boolean
      Indicates whether this step has been performed
   history : List[object]
      Source of changes to this plan step (e.g., messages)
   """

   def __init__(self, id_, action, creator=None):
      """
      Arguments
      ---------
      id_
         Unique identifier of the step
      action : Action
         Action performed at this step
      creator : str, default=None
         Identifier of the entity that created this step
      prioritization : int
         Priority of the step
      history : List
         List of messages that updated this step
      responses : Map[str,str]
         Dictionary mapping participant ID to their most recent response to the
         plan step
      """

      self._id = id_
      self._sequence_number = -1
      self._action = action
      self._creator = creator
      self._prioritization = -1
      self._schedule = (-1,-1)

      self._is_complete = False
      self._history = []

      self._preconditions = []

      # String representation of responses by participant ID.  Provides an
      # empty string if no participants have responded
      self._responses = collections.defaultdict(lambda: "")


   def __str__(self):
      """
      String representation of the step
      """

      return f'{self.__class__.__name__} <{self._id}; step {self._sequence_number}; creator {self._creator}> (Priority: {self.prioritization}; Schedule: {self.schedule})'


   @property
   def is_complete(self):
      return self._is_complete

   @is_complete.setter
   def is_complete(self, complete):
      self._is_complete = True

   @property
   def action(self):
      return self._action

   @action.setter
   def action(self, action):
      self._action = action

   @property
   def id(self):
      return self._id

   @property
   def sequence_number(self):
      return self._sequence_number

   @property
   def prioritization(self):
      return self._prioritization

   @prioritization.setter
   def prioritization(self, priority):
      self._prioritization = priority  

   @property
   def creator(self):
      return self._creator
   
   @property
   def history(self):
      return self._history

   @property
   def responses(self):
      return self._responses

   @property
   def schedule(self):
      return self._schedule

   @schedule.setter
   def schedule(self, time):
      self._schedule = time


   def add_response(self, responder, response):
      self._responses[responder] = response


   def serialize(self):
      """
      Create a dictionary representation of the plan step
      """

      return { 'id': self.id,
               'sequence_number': self.sequence_number,
               'prioritization': self.prioritization,
               'creator': self.creator,
               'responses': dict(self.responses),
               'schedule': self.schedule,
               'is_complete': self.is_complete,
               'action': self.action.serialize()
             }

   def clone(self):
      """
      Return a copy of this plan step
      """

      clone = PlanStep(self.id, self.action.clone(), self.sequence_number, self.creator)
      clone.prioritization = self.prioritization
      clone.schedule = self.schedule
      clone.is_complete = self.is_complete

      for responder, response in self._responses.items():
         clone.add_response(responder, response)

      return clone