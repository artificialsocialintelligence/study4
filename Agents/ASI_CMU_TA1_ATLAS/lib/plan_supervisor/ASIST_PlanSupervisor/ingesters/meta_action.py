# -*- coding: utf-8 -*-
"""
.. module:: plan_monitor.injesters.meta_actions
   :platform: Linux, Windows, OSX
   :synopsis: Definitions of meta-actions used in planning

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

"""

import enum

class MetaAction(enum.Enum):
   """
   Enumeration of meta_actions that can be performed in UI_Click messages
   """

   NotSet = 'NOT_SET'
   PlanningFlagPlaced = "PLANNING_FLAG_PLACED"
   UndoPlanningFlagPlaced = "UNDO_PLANNING_FLAG_PLACED"
   PlanningFlagUpdate = "PLANNING_FLAG_UPDATE"
   PlanningBombPlaced = "PLANNING_BOMB_PLACED"
   PlanningBeaconUpdate = "PLANNING_BEACON_UPDATE"
   UndoPlanningBombPlaced = "UNDO_PLANNING_BOMB_PLACED"
   PlanningBombUpdate = "PLANNING_BOMB_UPDATE"
   VoteLeaveStore = "VOTE_LEAVE_STORE"
   ProposeToolPurchaseIncrease = "PROPOSE_TOOL_PURCHASE_+"
   ProposeToolPurchaseDecrease = "PROPOSE_TOOL_PURCHASE_-"