# -*- coding: utf-8 -*-
"""
.. module:: plan_monitor.injesters.dialog_agent
   :platform: Linux, Windows, OSX
   :synopsis: Class for ingesting events from dialog agent events

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

"""

# import enum

import MinecraftElements
from MinecraftBridge.messages import (
   EnvironmentCreatedSingle,
   DialogAgent
)
from MinecraftBridge.utils import Loggable

from .meta_action import MetaAction

from ..representation import NullPlan
from ..representation.query import QueryBy, SortBy

from ..plan_components import PlanStep
from ..plan_components.actions import (
   GoToWaypoint,
   Search,
   DefuseBomb,
   PutOutFire,
   GroupUp
)



class DialogAgentIngester(Loggable):
   """
   The DialogAgentInjeseter is used to consume UI_Click messages and
   convert these to Plan Event instances.

   Attributes
   ----------
   parent : PlanSupervisor
      Top-level plan supervisor component in charge of this injester
   plan : PlanRepresentation
      PlanRepresentation instance this injester is interacting with
   """

   def __init__(self, parent, minecraft_interface, plan=None, config={}):
      """
      Arguments
      ---------
      parent : PlanSupervisor
         Top-level superviser component in charge of the ingester
      minecraft_interface : MinecraftBridge.mqtt.callback_interface
         Interface to MinecraftBridge
      plan : PlanRepresentation, default=None
         PlanRepresentation instance this injester will interact with.  If None
         is provided (default) a NullPlan will be instantiated.
      config : dict
         Dictionary of configuration values
      """

      self.logger.debug(f'{self}:  Initializing Cognitive Artifact Ingester')

      self._parent = parent
      self._minecraft_interface = minecraft_interface

      self._plan = plan

      if self._plan == None:
         self._plan = NullPlan()


      # Each extraction label will have a handler
      self._label_handlers = { 'MoveTo':     self.__handleMoveTo,
                              'Defuse':     self.__handleDefuse,
                              'Proposal':   self.__handleProposal,
                              'ShouldPlan': self.__handleShouldPlan }

      # Mapping of named locations to (x,z) positions
      self._locations = { 'Desert': (25,25),
                          'Village': (75,25),
                          'Forest': (125,25)
                        }



   def __str__(self):
      """
      String representation of the object
      """

      return self.__class__.__name__


   @property
   def parent(self):
      return self._parent

   @property
   def plan(self):
      return self._plan

   @plan.setter
   def plan(self, plan):
      self._plan = plan
   


   def start(self):
      """
      Called by the parent when the injester is ready to be started.
      """

      self.logger.info(f'{self}:    Starting')

      self._minecraft_interface.register_callback(DialogAgent, self.__onDialog)


   def stop(self):
      """
      Called by the parent when the injester is to be stopped.
      """

      self.logger.info(f'{self}:    Stopping')

      self._minecraft_interface.deregister_callback(self.__onDialog)


   def __handleMoveTo(self, extraction, step_id, participant_id):
      """
      """

      self.logger.info(f'{self}:  === MoveTo ===')

      # Is there a target?  It'll be the location name, which will need to be
      # converted to an (x,z) position, if possible
      target = extraction.get('arguments', {}).get('target', '<UNKNOWN>')
      if not target in self._locations:
         self.logger.warning(f'{self}:    Unknown location name: {target}')

      location = self._locations.get(target, (-1,-1))

      action = GoToWaypoint(location)
      plan_step = PlanStep(step_id, action, participant_id)

      # Are there attachments to the extraction?
      attachments = extraction.get('attachments', [])
      if type(attachments) is not list:
         attachments = [attachments]

      for attachment in attachments:
         self.__process_attachment(plan_step, attachment)

      return plan_step


   def __handleDefuse(self, extraction, step_id, participant_id):
      """
      """

      self.logger.info(f'{self}:  === Defuse ===')


      # Is there a target?  It should be a bomb.  See where it's located
      target = extraction.get('arguments', {}).get('target', {'label': 'Bomb'})
      bomb_location = target.get('arguments', {}).get('location', '<UNKNOWN>')

      # The plan step is to defuse a bomb, regardless of what additional info
      # is available
      action = DefuseBomb(bomb_location) 
      plan_step = PlanStep(step_id , action, participant_id)

      # Are there attachments to the extraction?
      attachments = extraction.get('attachments', [])
      if type(attachments) is not list:
         attachments = [attachments]

      for attachment in attachments:
         self.__process_attachment(plan_step, attachment)

      return plan_step


   def __handleProposal(self, extraction, step_id, participant_id):
      self.logger.info(f'{self}:  === Proposal ===')

      # Get the topic from the extraction, and just use that as the plan
      topic = extraction.get('arguments', {}).get('topic', {})

      plan_step = self._label_handlers.get(topic.get('label', None), lambda x,y,z: None)(extraction, step_id, participant_id)

      # Are there attachments to the extraction?
      attachments = extraction.get('attachments', [])
      if type(attachments) is not list:
         attachments = [attachments]

      for attachment in attachments:
         self.__process_attachment(plan_step, attachment)

      return plan_step


   def __handleShouldPlan(self, extraction, step_id, participant_id):
      self.logger.info(f'{self}:  === ShouldPlan ===')

      # Get the topic from the extraction, and just use that as the plan
      topic = extraction.get('arguments', {}).get('topic', {})

      plan_step = self._label_handlers.get(topic.get('label', None), lambda x,y,z: None)(extraction, step_id, participant_id)

      # Are there attachments to the extraction?
      attachments = extraction.get('attachments', [])
      if type(attachments) is not list:
         attachments = [attachments]

      for attachment in attachments:
         self.__process_attachment(plan_step, attachment)

      return plan_step


   def __create_step_from_extraction(self, extraction, step_id, participant_id):
      """
      Attempt to create a PlanStep from a provided extraction.  If no plan step
      can be extracted, returns None.

      Argument
      --------
      extraction : dict
         Dictionary representation of a compact extraction to generate a Plan
         Step from
      step_id : str
         Unique identifier for the step, generated from the utterance id and
         index of the compact extraction
      participant_id : str
         Identifier of participant that generated this extraction
      """

      # There should be a label in the extraction.  If not, throw a warning
      if not "label" in extraction:
         self.logger.warning(f'{self}:  No label in extraction {extraction}')
         return None

      return self._label_handlers.get(extraction['label'], lambda x,y,z: None)(extraction, step_id, participant_id)


   def __process_attachment(self, plan_step, attachment):
      """
      Modify a plan step based on the attachment provided

      Arguments
      ---------
      plan_step : PlanStep
         Plan step to modify
      attachment : str
         Attachment that modifies the plan step
      """

      # I don't trust that *something* doesn't go wrong, so let's keep it in
      # a try block to avoid things going wrong
      try:
         if attachment == "Team":
            for participant in self._parent.participants:
               plan_step.action.add_actor(participant.participant_id)
         if attachment == "Self":
            if plan_step.creator is not None:
               plan_step.action.add_actor(plan_step.creator)
         if attachment == "Alpha":
            plan_step.action.add_actor(self._parent.participants['alpha'].participant_id)
         if attachment == "Bravo":
            plan_step.action.add_actor(self._parent.participants['bravo'].participant_id)
         if attachment == "Delta":
            plan_step.action.add_actor(self._parent.participants['delta'].participant_id)


      except Exception as e:
         self.logger.warning(f'{self}:  Exception raised when attemptingn to process attachment:')
         self.logger.warning(f'{self}:  {e}')


   def __onDialog(self, message):
      """
      Callback when a DialogAgent message is received
      """

      self.logger.info(f'{self}:  Received DialogAgent message')

      self.logger.info(f'{self}:  {message.corrected_text}')

      self.logger.info(f'{self}:  Compact Extractions:')

      for index, extraction in enumerate(message.compact_extractions):
         self.logger.info(f'{self}:  {index}. {extraction}')

         step_id = f'{message.utterance_id}-{index}'

         plan_step = self.__create_step_from_extraction(extraction, step_id, message.participant_id)

         if plan_step is not None:
            self.logger.info(f'{self}:  Generated a Plan Step: {plan_step}; {plan_step.action}')

      self.logger.info(f'{self}:=========================')

