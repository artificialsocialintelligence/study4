"""
"""

class PlanStepMonitor:
	"""
	"""

	def __init__(self, plan_step):
		"""
		"""

		self._plan_step = plan_step
		self._is_complete = False
		self._is_successful = None
		self._step_completers = set()

	@property
	def is_complete(self):
		return self._is_complete

	@is_complete.setter
	def is_complete(self, complete):
		self._is_complete = complete
		# Also indicate that the action is complete
		self._plan_step.action.is_complete = True

	@property
	def step_completers(self):
		return self._step_completers

	def add_completer(self, actor):

		self._step_completers.add(actor)


	def onPlayerState(self, message):
		"""
		"""

		pass


	def onObjectStateChange(self, message):
		"""
		"""

		pass

	
	def onEnvironmentRemovedSingle(self, message):
		"""
		"""

		pass