"""
"""

import math
import functools

from .plan_step_monitor import PlanStepMonitor

class PutOutFireMonitor(PlanStepMonitor):
	"""
	"""

	def __init__(self, plan_step):
		"""
		"""

		PlanStepMonitor.__init__(self, plan_step)



	def onEnvironmentRemovedSingle(self, message):
		"""
		Arguments
		---------
		message : MinecraftBridge.messages.EnvironmentRemovedSingle
			EnvironmentRemovedSignle message
		"""

		# Is this about the fire at the given location?
		location = message.object.x, message.object.y, message.object.z
		if location != self._plan_step.action.location:
			return

		# Assume the object is fire.  TODO:  Actually check this
		self._plan_step.is_complete = True
		self._is_successful = True
		self.add_completer(message.triggering_entity)
			