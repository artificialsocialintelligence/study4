"""
"""

import math
import functools

from .plan_step_monitor import PlanStepMonitor

class DefuseBombMonitor(PlanStepMonitor):
	"""
	"""

	def __init__(self, plan_step):
		"""
		"""

		PlanStepMonitor.__init__(self, plan_step)

		self._bomb_id = plan_step.action.bomb_id


	def onObjectStateChange(self, message):
		"""
		Arguments
		---------
		message : MinecraftBridge.messages.ObjectStateChange
			ObjectStateChange message
		"""

		# Is this about the bomb of interest?
		if message.id != self._bomb_id:
			# Nothing to do with this
			return

		# Is the bomb defused or blown up?
		bomb_state = message.current_attributes.get_attribute('outcome')

		# Don't do anything if the outcome attribute doesn't exist
		if bomb_state is None:
			return

		# Did the bomb explode?
		if 'EXPLODE' in bomb_state:
			self._plan_step.is_complete = True
			self._is_successful = False

		# Was it defused?
		if bomb_state == 'DEFUSED':
			self._plan_step.is_complete = True
			self._is_successful = True
			self.add_completer(message.triggering_entity)
