"""
"""

import setuptools

with open("README.md", "r") as readme_file:
	long_description = readme_file.read()

setuptools.setup(
	name="asist_plan_supervisor",
	version="0.0.6",
	author="Dana Hughes",
	author_email="danahugh@andrew.cmu.edu",
	description="Monitors development and execution of participant plans during shop and field phase.",
	long_description=long_description,
	long_description_content_type="text/markdown",
	url="https://gitlab.com/cmu_aart/asist/study4/plan_supervisor",
	packages=setuptools.find_packages(include=["asist_plan_supervisor"]),
	classifiers=[
		"Programming Language :: Python :: 3",
		"License :: OSI Approved :: MIT License",
		"operating System :: OS Independent",
	],
	python_requires=">=3.5",
#	package_dir={'': 'src'}
)
