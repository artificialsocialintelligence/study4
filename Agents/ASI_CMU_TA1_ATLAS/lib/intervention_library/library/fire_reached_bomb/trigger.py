# -*- coding: utf-8 -*-
"""
.. module:: fire_reach_bomb.trigger
   :platform: Linux, Windows, OSX
   :synopsis: Trigger for FireReachBombIntervention Intervention

.. moduleauthor:: Yu Quan Chong <yuquanc@andrew.cmu.edu>

Definition of a Trigger class designed to spawn FireReachBombIntervention 
interventions.
"""

from InterventionManager import InterventionTrigger

from MinecraftBridge.messages import FoVSummary

from MinecraftElements.blocks import Block

from .intervention import FireReachBombIntervention

class FireReachBombTrigger(InterventionTrigger):
   """
   Class that generates FireReachBombIntervention.  Interventions are triggered
   for each bomb as discovered. 
   """
   def __init__(self, manager, **kwargs):
      """
      Arguments
      ---------
      manager : InterventionManager
         Instance of the manager in charge of this trigger

      Keyword Arguments
      -----------------
      message : string
         Welcome message to greet the team with
      """

      InterventionTrigger.__init__(self, manager, **kwargs)
      self.kwargs = kwargs

      # Keep a dictionary mapping bomb ID to the spawned intervention
      self._interventions = {}

      # Keep an eye on when bombs are observed by participants
      self.add_minecraft_callback(FoVSummary, self.__onFoV)


   def __onFoV(self, message):
      """
      Callback when an FoV message is recieved.  Checks to see if 1) there are
      any bombs in the FoV and 2) spawns an intervention if one doesn't exist
      for the bomb.
      """

      # Check to see if there is a bomb in the list of observed blocks
      for block in message.blocks:
         # Check if the block is a bomb
         bomb_id = block.get_attribute('testbed_id')
         if bomb_id is None:
            bomb_id = 'UNKNOWN'

         if 'BOMB' in bomb_id:

            # Store the intervention, in case this bomb_id is seen at another
            # time
            if not bomb_id in self._interventions:
               intervention = FireReachBombIntervention(self.manager, bomb_id)
               self._interventions[bomb_id] = intervention

               self.logger.info(f'{self}:  Spawning {intervention} for Bomb {bomb_id}')
               self.manager.spawn(intervention)
            else:
               self.logger.debug(f'{self}:  Ignoring bomb {bomb_id}---FireReachedBombIntervention already spawned')

