# -*- coding: utf-8 -*-
"""
.. module:: bomb_started_without_resources.trigger
   :platform: Linux, Windows, OSX
   :synopsis: Trigger for BombStartedWithoutResources Intervention

.. moduleauthor:: Ini Oguntola <ioguntol@andrew.cmu.edu>

Definition of a Trigger class designed to spawn interventions to help
players use the right tool in the future.
"""

from InterventionManager import InterventionTrigger

import MinecraftElements

from MinecraftBridge.messages import ObjectStateChange, PlayerInventoryUpdate

from .intervention import BombStartedWithoutResourcesIntervention

class BombStartedWithoutResourcesTrigger(InterventionTrigger):
    """
    Class that generates BombStartedWithoutResourcesIntervention instances.
    Interventions are triggered when a player becomes frozen.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        message : string
            Welcome message to greet the team with
        """

        InterventionTrigger.__init__(self, manager, **kwargs)
        self.kwargs = kwargs

        # Register the trigger to receive messages
        self.add_minecraft_callback(ObjectStateChange, self.__onObjectStateChange)



    def __onObjectStateChange(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.ObjectStateChange
            Received ObjectStateChange message
        """

        self.logger.debug(f"{self}: Received Message {message}")
        self.logger.debug(f"{self}:    {message.type}")

        # Was the bomb triggered by a non-participant (e.g., SERVER)?
        if not message.triggering_entity in [p.id for p in self.manager.participants]:
            self.logger.debug(f'{self}:    Object triggered by {message.triggering_entity} (non-participant)')
            return

        # Is the object a bomb
        if message.type not in { MinecraftElements.Block.block_bomb_chained,
                                 MinecraftElements.Block.block_bomb_standard,
                                 MinecraftElements.Block.block_bomb_volatile,
                                 MinecraftElements.Block.block_bomb_fire,
                                 "block_bomb_chained",
                                 "block_bomb_standard",
                                 "block_bomb_volatile",
                                 "block_bomb_fire" }:
            self.logger.debug(f'{self}:    Object type {message.type} not a bomb.')
            return

        # Is the bomb active?  Check that the attribute exists, and is not
        # False
        is_active = message.changed_attributes.get_attribute('active')
        if is_active is None:
            self.logger.debug(f"{self}:    Object doesn't have 'active' attribute")
            return
        if not is_active:
            self.logger.debug(f"{self}:    Object 'active' attribute is False")
            return

        # An intervention should be spawned
        intervention = BombStartedWithoutResourcesIntervention(self.manager, 
                                                               message)
        self.logger.info(f'{self}:  Spawning {intervention}.')
        self.manager.spawn(intervention)