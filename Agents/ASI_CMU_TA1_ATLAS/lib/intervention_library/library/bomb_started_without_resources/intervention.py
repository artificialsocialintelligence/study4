# -*- coding: utf-8 -*-
"""
.. module:: bomb_started_without_resources.intervention
   :platform: Linux, Windows, OSX
   :synopsis: Intervention used to help players avoid getting
        frozen again in the future.

.. moduleauthor:: Ini Oguntola <ioguntol@andrew.cmu.edu>

Definition of an Intervention class designed to help players
use the right tool in the future.
"""

import math

from InterventionManager import Intervention


class BombStartedWithoutResourcesIntervention(Intervention):
    """
    A BombStartedWithoutResourcesIntervention is an Intervention which simply presents a 
    message to team members when one of their team mates is frozen.
    """

    def __init__(self, manager, bomb_message, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention

        bomb_message : MinecraftBridge.messages.ObjectStateChange
            ObjectStateChange message of the bomb beingn defused

        Keyword Arguments
        -----------------
        message : string
            Message to send to a player if presented

        engagement_distance : float
            Maximum distance to bomb to consider a player engaged
        """

        # Default values for intervention prioritization information
        kwargs["priority"] = kwargs.get("priority", 1)
        kwargs["expiration"] = kwargs.get("expiration", 10000)
        kwargs["view_duration"] = kwargs.get("view_duration", 15000)
        kwargs["present_immediately"] = kwargs.get("present_immediately", True)

        Intervention.__init__(self, manager, **kwargs)

        self.logger.debug(f'{self}:  {bomb_message}')

        self.addRelevantParticipant(bomb_message.triggering_entity)
        self._participant_id = bomb_message.triggering_entity

        self._engagement_distance = kwargs.get('engagement_distance', 15)

        # Relevant details about the bomb
        self._bomb_location = bomb_message.x, bomb_message.y, bomb_message.z
        self._remaining_colors = set(bomb_message.current_attributes.get_attribute('sequence'))
        self._remaining_colors = {x.upper() for x in self._remaining_colors}

        # Also create the set of needed tools
        self._needed_tools = { {'R': 'WIRECUTTERS_RED',
                                'G': 'WIRECUTTERS_GREEN',
                                'B': 'WIRECUTTERS_BLUE'}[color]
                                for color in self._remaining_colors }


        self.logger.debug(f'{self}:  Remaining Colors: {self._remaining_colors}')
        self.logger.debug(f'{self}:  Needed Tools: {self._needed_tools}')

        self._message = kwargs.get(
            'message',
            "You don't have the tools to defuse this bomb. You should ask your teammates for help."
        )


    def __distance(self, p0, p1):
        return math.sqrt((p0[0] - p1[0])**2 + (p0[2] - p1[2])**2)


    def _onActivated(self):
        """
        Callback when the Intervention is activated.  Checks to see which
        players are nearby, and if the needed tools are available.
        """

        # Determine who is close enough to the bomb
        engaged_participants = []

        for participant in self.manager.participants:
            participant_info = self.manager.participant_info[participant.id]
            self.logger.debug(f'{self}:      Participant: {participant.id}; Location: {participant_info.location}; Bomb: {self._bomb_location}')
            if self.__distance(participant_info.location, self._bomb_location) <= self._engagement_distance:
                engaged_participants.append(participant.id)

        self.logger.debug(f'{self}:  Engaged Participants: {engaged_participants}')

        # Determine available tools and which tools are lacking
        available_tools = set()

        for participant_id in engaged_participants:
            for tool in { 'WIRECUTTERS_RED', 'WIRECUTTERS_BLUE', 'WIRECUTTERS_GREEN' }:
                if self.manager.participant_info[participant_id].inventory[tool] > 0:
                    available_tools.add(tool)

        available_tools = { x.upper() for x in available_tools }
        lacking_tools = set.difference(self._needed_tools, available_tools)

        self.logger.debug(f'{self}:  Available Tools:  {available_tools}')
        self.logger.debug(f'{self}:  Lacking Tools:  {lacking_tools}')

        # Are there any lacking tools?
        if len(lacking_tools) > 0:
            self.queueForResolution()
        else:
            self.discard()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {     
            'default_recipients': [self._participant_id],
            'default_message': self._message,
            'default_responses': []
        }
    