# -*- coding: utf-8 -*-
"""
.. module:: injuries_cost_points.trigger
   :platform: Linux, Windows, OSX
   :synopsis: Trigger for Injuries Cost Points Intervention

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a Trigger class designed to spawn interventions to remind players
that injuries will reduce their score
"""

import random

from InterventionManager import InterventionTrigger

from MinecraftBridge.messages import MissionStageTransition, MissionStage

from .intervention import InjuriesCostPointsIntervention

class InjuriesCostPointsTrigger(InterventionTrigger):
    """
    Class that generates InjuriesCostPointsInterventions.  Interventions are 
    triggered at the start of the first field phase.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        message : string
            Welcome message to greet the team with
        """
        InterventionTrigger.__init__(self, manager, **kwargs)
        self.kwargs = kwargs

        # Want to give a small delay to when the intervention is presented, to
        # give the advisor some character
        self._min_presentation_time = kwargs.get("min_presentation_time", 10000)
        self._max_presentation_time = kwargs.get("max_presentation_time", 20000)

        # Register the trigger to receive messages when mission stage changes
        self.add_minecraft_callback(MissionStageTransition, self.__onMissionStageTransition)


    def __onMissionStageTransition(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.MissionStageTransition
            Received MissionStageTransition message
        """

        self.logger.debug(f"{self}: Received Message {message}")

        # An intervention should only be spawned if 1) the mission stage is the
        # Shop stage, and 2) then number of times the team has transitioned to 
        # the shop is 1

        if message.mission_stage == MissionStage.FieldStage and \
           message.transitionsToShop <= 1:

            for participant in self.manager.participants:
                presentation_delay = random.randint(self._min_presentation_time, self._max_presentation_time)

                intervention = InjuriesCostPointsIntervention(self.manager, 
                                                              participant.id, 
                                                              presentation_delay,
                                                              **self.kwargs)

                self.logger.info(f'{self}:  Spawning {intervention}')
                self.manager.spawn(intervention)