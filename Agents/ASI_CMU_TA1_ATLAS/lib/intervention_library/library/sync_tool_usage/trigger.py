# -*- coding: utf-8 -*-
"""
.. module:: sync_tool_usage.trigger
   :platform: Linux, Windows, OSX
   :synopsis: Trigger for sync tool usage interventions

.. moduleauthor:: Keyang Zheng <kez20@pitt.edu>

Definition of a Trigger class designed to spawn interventions regarding player's tool 
usage assignment after knowing the sequence of a multi-step bomb.
"""

from InterventionManager import InterventionTrigger

from .intervention import SyncToolUsageIntervention

from shared.bomb_defusal_success_monitor import BombDefusalSuccessMonitor

from MinecraftBridge.messages import (
    CommunicationEnvironment,
    MissionStageTransition, 
    MissionStage
)    

class SyncToolUsageTrigger(InterventionTrigger):
    """
    Class that generates SyncToolUsage Intervention.  Interventions
    are triggered when at least two players are near a multi-stage bomb, and 
    the bomb is identified.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        use_tom_for_engagement : boolean, default=False
            Identify whether engagement should be determined from ToM intent
            (True) or participant's distance to the bomb (False)
        """
        InterventionTrigger.__init__(self, manager, **kwargs)
        self._use_tom_for_engagement = kwargs.get("use_tom_for_engagement", False)

        # Dictionary mapping BOMB_ID to generated interventions, used to
        # determine which bombs have active interventions (or where none were
        # necessary with a `None`)
        self._interventions = {}

        self._defusal_monitor = BombDefusalSuccessMonitor(self)

        self._current_mission_stage = None

        self._engagement_distance = kwargs.get("engagement_distance", 5.0)

        # Listen for when bombs emit their defusal sequence --- this will
        # be used to determine if an intervention should be issued
        self.add_minecraft_callback(CommunicationEnvironment, self.__onCommunicationEnvironment)
        self.add_minecraft_callback(MissionStageTransition, self.__onMissionStageTransition)


    def __onMissionStageTransition(self, message):
        """
        Callback when the mission stage transition occurs.  
        """

        self._current_mission_stage = message.mission_stage

    
    def __onCommunicationEnvironment(self, message):
        """
        Check the content of the communication to see if it contains 1) BOMB_ID
        and 2) SEQUENCE in its message.
        """

        self.logger.debug(f"{self}: Received Message {message}")

        # Parse the message, checking if it contains lines starting with BOMB_ID
        # and SEQUENCE. 
        if not "BOMB_ID:" in message.message or not "SEQUENCE:" in message.message:
            self.logger.debug(f'{self}:   Message does not contain "BOMB_ID" and/or "SEQUENCE".  Ignoring.')
            return

        message_lines = message.message.split('\n')

        bomb_id = None
        sequence = None

        for line in message_lines:
            if line.startswith("BOMB_ID:"):
                try:
                    bomb_id = line.split(':')[1].strip()
                except:
                    self.logger.warning(f'{self}: Could not extract BOMB_ID from message line: {line}')

            if line.startswith("SEQUENCE:"):
                try:
                    sequence = line.split(':')[1].strip()
                    sequence = [s.strip() for s in sequence.strip('[').strip(']').split(',')]
                    sequence = ''.join(sequence)
                except:
                    self.logger.warning(f'{self}: Could not extract SEQUENCE from message line: {line}')

        # Only proceed if a bomb_id and sequence have been extracted
        if bomb_id is None or sequence is None:
            return

        # Check if there is already an intervention about this bomb id
        if bomb_id in self._interventions:
            self.logger.debug(f'{self}: Bomb with ID {bomb_id} already discovered.  No intervention triggered.')
            return

        # Only trigger an intervention if its a multi-stage bomb
        if len(sequence) <= 1:
            self.logger.info(f'{self}: Bomb with ID {bomb_id} and sequence {sequence} not multi-staged.  No intervention triggered.')
            # Store the bomb so that it will later be ignored
            self._interventions[bomb_id] = None
            return

        bomb_location = message.sender_position

        # Register multi-stage bomb location
        self.logger.debug(f'{self}: Discovered multi-stage bomb with ID {bomb_id} and sequence {sequence}.')

        intervention = SyncToolUsageIntervention(self.manager, 
                                                 bomb_id, 
                                                 bomb_location,
                                                 sequence,
                                                 engagement_distance=self._engagement_distance,
                                                 current_mission_stage=self._current_mission_stage,
                                                 defusal_monitor=self._defusal_monitor)

        self.logger.info(f'{self}: Spawning {intervention} for syncing player tool usage on multi-stage bomb {bomb_id} at {str(bomb_location)} with sequence {sequence}')
        self.manager.spawn(intervention)
