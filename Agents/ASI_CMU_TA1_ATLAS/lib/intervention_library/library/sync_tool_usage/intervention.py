"""
.. module:: broadcast_defusal_sequence.intervention
    :platform: Linux, Windows, OSX
    :synopsis: Intervention for broadcasting bomb defusal sequence to players

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class designed to update participants engaged in
defusing a bomb with its change in sequence as its being defused.
"""

from InterventionManager import Intervention

from MinecraftBridge.messages import (
     PlayerState,
     EnvironmentRemovedSingle,
     EnvironmentRemovedList,
     MissionStageTransition, 
     MissionStage     
)

import math


class SyncToolUsageIntervention(Intervention):
     
    def __init__(self, manager, bomb_id, bomb_location, bomb_sequence, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention
        bomb_id : int
            ID of the bomb being defused
        bomb_location : Tuple[int]
            Location of the bomb
        bomb_sequence : List[string]
            Sequence of defusal

        Keyword Arguments
        -----------------
        proximity_distance : float, default=8.0
            the distance to bomb that could be considered as contributing to the bomb
        """

        # Default values for intervention prioritization information
        kwargs["priority"] = kwargs.get("priority", 1)
        kwargs["expiration"] = kwargs.get("expiration", 5000)
        kwargs["view_duration"] = kwargs.get("view_duration", 15000)

        Intervention.__init__(self, manager, **kwargs)

        # Store relevant variables for Followup
        self._bomb_id = bomb_id
        self._bomb_location = bomb_location
        self._bomb_sequence = bomb_sequence

        self._current_mission_stage = kwargs.get("current_mission_stage", None)
        self._defusal_monitor = kwargs.get("defusal_monitor", None)
        self._min_team_failure_rate = kwargs.get("min_team_failure_rate", 0.34)
        self._min_distance_to_bomb = kwargs.get("_min_distance_to_bomb", 3)

        # Who will be receiving this intervention?
        self._participants = []

        self.add_minecraft_callback(PlayerState, self.__onPlayerState)
        self.add_minecraft_callback(EnvironmentRemovedList, self.__onEnvironmentRemovedList)
        self.add_minecraft_callback(EnvironmentRemovedSingle, self.__onEnvironmentRemovedSingle)

        # Intervention message
        self._message = f'I suggest coordinating with your teammages to defuse this bomb with sequence {bomb_sequence} by holding the tool you intent to use.'
#        self._message = f'Please coordinate with your teammates for defusing bomb ID {bomb_id} with ' + \
#                        f'bomb sequence {bomb_sequence} by holding the tool you want to use for this bomb.'

    def __distance_to_bomb(self, p):

        return math.sqrt((self._bomb_location[0]-p[0])**2 + 
                         (self._bomb_location[2]-p[2])**2)


    def __onMissionStageTransition(self, message):
        """
        Callback when the mission stage transition occurs.  
        """

        self._current_mission_stage = message.mission_stage


    def __onPlayerState(self, message):
        """
        """

        # Intervention is only relevant if the team is in the Field stage
        if self._current_mission_stage != MissionStage.FieldStage:
            return

        # This intervention is relevent if the team has demonstrated trouble 
        # with coordinating bomb defusal
        if self._defusal_monitor is None:
            team_failure_rate = 0.0
        else:
            team_failure_rate = self._defusal_monitor.team_failure_rate()

        if team_failure_rate < self._min_team_failure_rate:
            return

        # Figure out how far each player is from the bomb
        player_distances = { p_id: self.__distance_to_bomb(info.location) 
                             for p_id, info in self.manager.participant_info.items() }

        close_players = [p for p in player_distances if player_distances[p] < self._min_distance_to_bomb]

        # Are more than two players present?
        if len(close_players) >= 2:
            self._participants = close_players
            self.logger.info(f"{self}:  Resolving for {self._participants} due to team failure rate of {team_failure_rate}")
            self.queueForResolution()


    def __onEnvironmentRemovedList(self, message):
        """
        Callback when a  list of removed environment blocks is received

        Arguments
        ---------
        message : MinecraftBridge.messages.EnvironmentRemovedList
        """

        self.logger.debug(f'{self}:  Recieved Message {message}')

        # If any of the objects are this bomb, then this intervention is done
        for object_ in message.objects:
            if object_.id == self._bomb_id:
                self.discard()
                return


    def __onEnvironmentRemovedSingle(self, message):
        """
        Callback when an environment block is removed

        Arguments
        ---------
        message : MinecraftBridge.messages.EnvironmentRemovedSingle
        """

        self.logger.debug(f'{self}:  Recieved Message {message}')

        # Was the removed object this bomb?  Then we're done
        if message.object.id == self._bomb_id:
            self.logger.info(f'{self}:  Bomb with Bomb ID {message.object.id} has been removed.')
            self.discard()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
          
        return {     
            'default_recipients': self._participants,
            'default_message': self._message,
            'default_responses': ["Okay", "Skip"]            
        }   