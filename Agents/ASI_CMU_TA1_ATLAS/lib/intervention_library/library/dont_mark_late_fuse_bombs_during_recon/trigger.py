# -*- coding: utf-8 -*-
"""
.. module:: dont_mark_late_fuse_bombs_during_recon.trigger
   :platform: Linux, Windows, OSX
   :synopsis: Trigger for DontMarkLateFuseBombsDuringReconInterventions

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a Trigger class designed to spawn interventions to suggest that
players only mark bombs with short fuses duringn recon phase
"""

from InterventionManager import InterventionTrigger

import MinecraftElements

from MinecraftBridge.messages import (
    MissionStageTransition, 
    MissionStage,
    EnvironmentCreatedSingle
)

from .intervention import DontMarkLateFuseBombsDuringReconIntervention

class DontMarkLateFuseBombsDuringReconTrigger(InterventionTrigger):
    """
    Class that generates DontMarkLateFuseBombsDuringReconInterventions.
    Interventions are triggered at the start of the recon phase.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger
        """

        InterventionTrigger.__init__(self, manager, **kwargs)

        # What's the maximum fuse start considered "acceptable"
        self._max_fuse_start_minute = 7

        # Keep track of whether the tam is in the recon phase
        self._is_recon_phase = False

        # Register the trigger to receive messages when mission stage changes
        self.add_minecraft_callback(MissionStageTransition, self.__onMissionStageTransition)
        self.add_minecraft_callback(EnvironmentCreatedSingle, self.__onEnvironmentCreated)


    def __onMissionStageTransition(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.MissionStageTransition
            Received MissionStageTransition message
        """

        self.logger.debug(f"{self}: Received Message {message}")

        # An intervention should be spawned at the start of a Shop stage

        if message.mission_stage == MissionStage.ReconStage:
            self._is_recon_phase = True
        else:
            self._is_recon_phase = False


    def __onEnvironmentCreated(self, message):
        """
        Arguments
        ---------
        message: MinecraftBridge.messages.MissionStageTransition
            Received EnvironmentCreatedSingle message
        """

        self.logger.debug(f"{self}:  Received message {message}")

        # Is the team still in the recon phase?
        if not self._is_recon_phase:
            return

        # Is this a bomb beacon block?
        if message.object.type == MinecraftElements.Block.block_beacon_bomb:
            # Create the intervention with relevant information
            participant_id = message.triggering_entity
            beacon_id = message.object.id
            beacon_location = (message.object.x, 
                               message.object.y, 
                               message.object.z)
            message_time = message.elapsed_milliseconds

            # Create the intervention
            intervention = DontMarkLateFuseBombsDuringReconIntervention(self.manager,
                                                                        participant_id,
                                                                        beacon_id,
                                                                        beacon_location,
                                                                        message_time,
                                                                        max_fuse_start_minute = self._max_fuse_start_minute)
            self.manager.spawn(intervention)
