# -*- coding: utf-8 -*-
"""
.. module:: resolve_plan_disagreement
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for addressing plan disagreements

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to encourage discussion and agreement on any plan steps
prior to leaving the shop, if there are any steps that are disagreed with.

* Trigger: The intervention is triggered at the start of a Shop Phase

* Discard States:  The intervention is discarded when leaving the shop

* Resolve State:  The intervention is queued for resolution if a player votes
                  to leave the shop and one or more plan steps have disagreement

* Followups:  None
"""

from .intervention import ResolvePlanDisagreementIntervention
from .trigger import ResolvePlanDisagreementTrigger

InterventionClass = ResolvePlanDisagreementIntervention
TriggerClass = ResolvePlanDisagreementTrigger
IndividualComplianceFollowupClass = None
TeamComplianceFollowupClass = None

default_parameters = {}

AdditionalFollowupClasses = []