# -*- coding: utf-8 -*-
"""
.. module:: recon_search.trigger
   :platform: Linux, Windows, OSX
   :synopsis: Trigger for Recon Search Interventions

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a Trigger class designed to spawn interventions to help the team
search each region in a balanced manner
"""

from InterventionManager import InterventionTrigger

from shared.search_monitor import SearchMonitor

from .intervention import ReconSearchIntervention

from MinecraftBridge.messages import (
    MissionStageTransition,
    MissionStage
)


class ReconSearchTrigger(InterventionTrigger):
    """
    Class that generates AttendToShopVotesInterventions.  The initial
    intervention is triggered at the start of each field phase.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        message : string
            Welcome message to greet the team with
        """

        InterventionTrigger.__init__(self, manager, **kwargs)
        self.kwargs = kwargs

        self.search_monitor = SearchMonitor(self)

        # Register the trigger to receive messages when mission stage changes
        self.add_minecraft_callback(MissionStageTransition, self.__onMissionStageTransition)


    def __onMissionStageTransition(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.MissionStageTransition
            Received message
        """

        self.logger.debug(f"{self}: Received Message {message}")

        # Determine if the transition is to a shop phase, and issue an
        # intervention if so
        if message.mission_stage == MissionStage.ReconStage:
            intervention1 = ReconSearchIntervention(self.manager, self.search_monitor, 60000)
            intervention2 = ReconSearchIntervention(self.manager, self.search_monitor, 120000)

            self.logger.info(f'{self}:  Spawning {intervention1} and {intervention2}')
            self.manager.spawn(intervention1)
            self.manager.spawn(intervention2)