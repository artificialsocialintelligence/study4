# -*- coding: utf-8 -*-
"""
.. module:: attend_to_field_votes.trigger
   :platform: Linux, Windows, OSX
   :synopsis: Trigger for AttendToFieldVotes Intervention

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a Trigger class designed to spawn interventions informing other
players to attend to their field vote.
"""

from InterventionManager import InterventionTrigger

from .intervention import AttendToFieldVotesIntervention

from MinecraftBridge.messages import (
    MissionStageTransition,
    MissionStage
)


class AttendToFieldVotesTrigger(InterventionTrigger):
    """
    Class that generates AttendToFieldVotesInterventions.  The initial
    intervention is triggered at the start of each shop phase.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        message : string
            Welcome message to greet the team with
        """

        InterventionTrigger.__init__(self, manager, **kwargs)
        self.kwargs = kwargs

        # Register the trigger to receive messages when mission stage changes
        self.add_minecraft_callback(MissionStageTransition, self.__onMissionStageTransition)


    def __onMissionStageTransition(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.MissionStageTransition
            Received message
        """

        self.logger.debug(f"{self}: Received Message {message}")

        # Determine if the transition is to a shop phase, and issue an
        # intervention if so
        if message.mission_stage == MissionStage.ShopStage:
            intervention = AttendToFieldVotesIntervention(self.manager)

            self.logger.info(f'{self}:  Spawning {intervention}')
            self.manager.spawn(intervention)