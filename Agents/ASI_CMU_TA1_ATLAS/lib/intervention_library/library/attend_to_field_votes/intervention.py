# -*- coding: utf-8 -*-
"""
.. module:: attend_to_vielf_votes.intervention
   :platform: Linux, Windows, OSX
   :synopsis: Intervention used to inform team members that a team mate is
              frozen

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class designed to remind players to attend to 
votes to return to the field if 1) they are the only one who has voted, and 
other players have not (i.e., assuming they're still discussing), and 2)
two players have voted to return, and the third hasn't.
"""

from InterventionManager import Intervention

from MinecraftBridge.messages import PlayerState, UI_Click, MissionStageTransition, MissionStage


class AttendToFieldVotesIntervention(Intervention):
    """
    An AttendToFieldVotesIntervention is an Intervention which monitors the 
    team's votes to return to the field and alerts participants if they fail to
    vote (when two votes are present) or remove their vote for further
    discussion (when only one vote is present).
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention

        Keyword Arguments
        -----------------
        one_vote_duration : int, default=20000
            Number of milliseconds until advice is provided a participant who
            voted when only one vote is present
        two_vote_duration : int, default=20000
            Number of milliseconds until advice is given to a participant to 
            vote if two votes are present.
        participant_vote_times : dict, default={}
            Dictionary mapping participant ID to elapsed_milliseconds from
            their last vote
        """

        # Default values for intervention prioritization information
        kwargs["priority"] = kwargs.get("priority", 1)
        kwargs["expiration"] = kwargs.get("expiration", 30000)
        kwargs["view_duration"] = kwargs.get("view_duration", 10000)


        Intervention.__init__(self, manager, **kwargs)

        self._one_vote_duration = kwargs.get("one_vote_duration", 20000)
        self._two_vote_duration = kwargs.get("two_vote_duration", 20000)

        self._current_time = -1
        self._participant_vote_times = kwargs.get('participant_vote_times', {})
        self._recipients = []

        # Determine which participants are relevant -- if only one vote exists,
        # then the relevant participants is the voter, if two, then the 
        # participant who hasn't voted
        voted_participants = list(self._participant_vote_times.keys())

        if len(voted_participants) == 1:
            self.addRelevantParticipant(voted_participants[0])
        elif len(voted_participants) == 2:
            for participant in self.manager.participants:
                if not participant.participant_id in voted_participants:
                    self.addRelevantParticipant(participant.participant_id)

        self._message = ""

        self.add_minecraft_callback(PlayerState, self.__onPlayerState)
        self.add_minecraft_callback(UI_Click, self.__onUI_Click)
        self.add_minecraft_callback(MissionStageTransition, self.__onMissionStageTransition)

    @property
    def participant_vote_times(self):
        return self._participant_vote_times
    


    def __onMissionStageTransition(self, message):
        """
        Callback when the mission stage transition occurs.  Used to discard the
        intervention when transitioning into the field.  Note that this should
        never be called.
        """

        if message.mission_stage == MissionStage.FieldStage:
            self.logger.info(f'{self}:  Transitioning to Field Stage, discarding intervention')
            self.discard()


    def __onPlayerState(self, message):
        """
        Callback when a PlayerState message is received.  Used ot determine
        current elapsed_milliseconds (may be better to use scheduler?  would
        still need to reset...).
        """

#        self.logger.debug(f'{self}:  Received message {message}')

        self._current_time = message.elapsed_milliseconds
        elapsed_time = -1

        # Determine if we need to pay attention to this --- if no participants
        # have voted, then ignore, otherwise, calculate how much time has 
        # passed since the most recent vote
        if len(self._participant_vote_times) == 0:
            return
        else:
            elapsed_time = self._current_time - max(self._participant_vote_times.values())

        # Determine if the intervention should be resolved
        if len(self._participant_vote_times) == 2 and elapsed_time > self._two_vote_duration:
            self.__resolve_for_two_votes()
###        if len(self._participant_vote_times) == 1 and elapsed_time > self._one_vote_duration:
###            self.__resolve_for_one_vote()
###        elif len(self._participant_vote_times) == 2 and elapsed_time > self._two_vote_duration:
###            self.__resolve_for_two_votes()
###        else:
###            return


    def __onUI_Click(self, message):
        """
        Callback when a UI click message is received.
        """

#        self.logger.debug(f'{self}:  Received message {message}')

        # Was this UI click associated with a vote to leave the store?  If not,
        # simply ignore the message
        if message.additional_info.get('meta_action', None) != "VOTE_LEAVE_STORE":
            return

        # Who did the voting?  And was it to undo a vote?
        if message.participant_id in self._participant_vote_times:
            del self._participant_vote_times[message.participant_id]
        else:
            self._participant_vote_times[message.participant_id] = self._current_time

        # Did all three participants vote?  Then discard the message
        if len(self._participant_vote_times) == 3:
            self.discard()


    def __resolve_for_one_vote(self):
        """
        Set up this intervention to be resolved for a single vote, and resolve.

        NOTE:  Not currently used
        """

        self.logger.debug(f'{self}:  Resolving for a single active vote')

        relevant_participant = self._participant_vote_times.keys()
        self._message = "Your team mates do not seem to be ready to return to the field.  Consider cancelling your vote until everyone is ready."

        self.logger.debug(f'{self}:    Relevant Participant(s): {relevant_participant}')

        self.addRelevantParticipant(relevant_participant[0])


    def __resolve_for_two_votes(self):
        """
        Set up this intervention to be resolved for a single vote, and resolve
        """

        self.logger.debug(f'{self}:  Resolving for a two active votes')

##        # The relevant participant is the one who did not vote yet
##        relevant_participant = set([p.id for p in self.manager.participants]).difference(set(self._participant_vote_times))
##        self.logger.debug(f'{self}:    Relevant Participant(s): {relevant_participant}')        
##        self.addRelevantParticipant(list(relevant_participant)[0])

        self._message = "Your teammates are ready to go to the field.  You haven't voted to leave yet."
        self._recipients = [p.participant_id for p in self.manager.participants if not p.participant_id in self._participant_vote_times.keys()]

        self.queueForResolution()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """


        return {     
            'default_recipients': self._recipients,
            'default_message': self._message,
            'default_responses': []
        }