# -*- coding: utf-8 -*-
"""
.. module:: instruct_on_bomb_beacon.intervention
   :platform: Linux, Windows, OSX
   :synopsis: Intervention for providing just-in-time instructions on bomb
              beacons

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class designed to provide just-in-time instruction
on bomb beacons when a bomb is first encountered
"""

from InterventionManager import Intervention

import MinecraftElements
from MinecraftBridge.messages import FoVSummary

class InstructOnBombBeaconIntervention(Intervention):
    """
    An InstructOnBombBeacon intervention provides a just-in-time tutorial on
    bomb beacons when a bomb is first encountered, if this is the first time a 
    player is playing the game.
    """

    def __init__(self, manager, participant_id, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention
        participant_id : str
            Identifier of the participant to be monitored
        """

        # Default values for intervention prioritization information
        kwargs["priority"] = kwargs.get("priority", 1)
        kwargs["expiration"] = kwargs.get("expiration", 120000)
        kwargs["view_duration"] = kwargs.get("view_duration", 15000)

        Intervention.__init__(self, manager, **kwargs)

        self._min_number_of_pixels = kwargs.get("min_number_of_pixels", 1500)

        self._participant_id = participant_id
        self.addRelevantParticipant(participant_id)

        # Listen to the Minecraft bridge for FoV messages
        self.add_minecraft_callback(FoVSummary, self.__onFoV)


    @property
    def participant_id(self):
        return self._participant_id


    def __onFoV(self, message):
        """
        Callback when an FoV summary message is received
        """

        self.logger.debug(f'{self}:  Recieved {message}')

        # Was this FoV message related to the participant?
        if message.participant_id != self._participant_id:
            return

        # Is there a 'block_bomb_chained' in the list of blocks?
        for block in message.blocks:
            if block.type in MinecraftElements.Block.bombs() and block.number_pixels >= self._min_number_of_pixels:
                self.logger.info(f'{self}:    Saw block of type {str(block.type)} with {block.number_pixels} pixels.  Resolving.')
                self.queueForResolution()


    def _onActivated(self):
        """
        Callback when the intervention is activated.  Checks to see if the
        player has played before, and discards if so.
        """

        # TODO:  Query the player database to see if the player has played
        #        before
        pass


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {     
            'default_recipients': [self.participant_id],
            'default_message': 'You recently saw a bomb on your screen.  Bomb beacons can be placed on bombs to provide information about it during the Shop phase.',
            'default_responses': []
        }