# -*- coding: utf-8 -*-
"""
.. module:: buy_fire_extinguishers
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for intervention to inform the team of
              the estimated number of fire extinguishers needed to remove all
              existing fires

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to remind players of the required number of fire
extinguishers for the given number of fires

* Trigger: The intervention is triggered when the team enters the shop

* Discard States:  N/A

* Resolve State:  The intervention is immediately queued for a resolution

* Followups:  Determine individual compliance to intervention, i.e., 
              do the player evacuate to specified safe distance away?
"""

from .intervention import BuyFireExtinguishersIntervention
from .trigger import BuyFireExtinguishersTrigger

InterventionClass = BuyFireExtinguishersIntervention
TriggerClass = BuyFireExtinguishersTrigger
IndividualComplianceFollowupClass = None
TeamComplianceFollowupClass = None

default_parameters = {}

AdditionalFollowupClasses = []