# -*- coding: utf-8 -*-
"""
.. module:: buy_fire_extinguihsers.trigger
   :platform: Linux, Windows, OSX
   :synopsis: Trigger for BuyFireExtinuisherIntervention Intervention

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a Trigger class designed to spawn 
BuyFireExtinguishersIntervention interventions.
"""

from InterventionManager import InterventionTrigger

from MinecraftBridge.messages import MissionStageTransition, MissionStage

from .intervention import BuyFireExtinguishersIntervention

from shared.fire_monitor import FireMonitor


class BuyFireExtinguishersTrigger(InterventionTrigger):
   """
   Class to generate BuyFireExtinguishersIntervention.  The trigger maintains
   a singleton fire monitor instance for use by the Interventions.  
   Interventions are triggered at the start of each shop phase.
   """

   def __init__(self, manager, **kwargs):
      """
      Arguments
      ---------
      manager : InterventionManager
         Instance of the manager in charge of this trigger
      """

      InterventionTrigger.__init__(self, manager, **kwargs)

      # Create a Fire Monitor
      self._fire_monitor = FireMonitor(self)

      # Register the trigger to receive messages when mission stage changes
      self.add_minecraft_callback(MissionStageTransition, self.__onMissionStageTransition)


   def __onMissionStageTransition(self, message):
      """
      Arguments
      ---------
      message : MinecraftBridge.messages.MissionStageTransition
         Received MissionStageTransition message
      """

      self.logger.debug(f"{self}: Received Message {message}")

      # An intervention should be spawned at the start of a Shop stage

      if message.mission_stage == MissionStage.ShopStage:
         intervention = BuyFireExtinguishersIntervention(self.manager, self._fire_monitor)
         self.logger.info(f'{self}:  Spawning {intervention}')
         self.manager.spawn(intervention)
