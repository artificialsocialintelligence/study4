# -*- coding: utf-8 -*-
"""
.. module:: broadcast_defusal_sequence
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for coordinating bomb defusal

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to assist with the coordination / synchronization of a
bomb defusal

* Trigger: The intervention is triggered when a bomb's defusal sequence is
           first identified

* Discard States:  

* Resolve State:  

* Followups:  
"""

from .intervention import BroadcastDefusalSequenceIntervention
from .trigger import BroadcastDefusalSequenceTrigger

InterventionClass = BroadcastDefusalSequenceIntervention
TriggerClass = BroadcastDefusalSequenceTrigger
IndividualComplianceFollowupClass = None
TeamComplianceFollowupClass = None

default_parameters = {}

AdditionalFollowupClasses = []