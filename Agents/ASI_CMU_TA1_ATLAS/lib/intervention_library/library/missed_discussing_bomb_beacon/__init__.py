# -*- coding: utf-8 -*-
"""
.. module:: missed_discussing_bomb_beacon
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for alert players to missed bomb beacons

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to alert the team that a small percentage of bomb beacons
did not have someone assigned to attend to it.

* Trigger: The intervention is triggered at the start of a Shop Phase

* Discard States:  The intervention is discarded when leaving the shop

* Resolve State:  The intervention is queued for resolution if a player votes
                  to leave the shop and at least one bomb beacon is missing an
                  actor, and fewer than N% are missing an actor.

* Followups:  None
"""

from .intervention import MissedDiscussingBombBeaconIntervention
from .trigger import MissedDiscussingBombBeaconTrigger

InterventionClass = MissedDiscussingBombBeaconIntervention
TriggerClass = MissedDiscussingBombBeaconTrigger
IndividualComplianceFollowupClass = None
TeamComplianceFollowupClass = None

default_parameters = {}

AdditionalFollowupClasses = []