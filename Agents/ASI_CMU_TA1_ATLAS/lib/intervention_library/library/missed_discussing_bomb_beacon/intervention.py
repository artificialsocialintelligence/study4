# -*- coding: utf-8 -*-
"""
.. module:: resolve_plan_disagreement.intervention
   :platform: Linux, Windows, OSX
   :synopsis: Intervention for alerting teams to missed bomb beacons

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class designed to alert teams to bomb beacons
with no assigned actors.
"""

from InterventionManager import Intervention

from MinecraftBridge.messages import UI_Click
from MinecraftBridge.messages import MissionStageTransition, MissionStage

class MissedDiscussingBombBeaconIntervention(Intervention):
    """
    A MissedDiscussingBombBeaconIntervention is an Intervention which monitors
    the team during the shop phase, and determines if the team missed assigning
    a player to attend to bomb(s) during planning
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention
        """

        # Default values for intervention prioritization information
        kwargs["priority"] = kwargs.get("priority", 1)
        kwargs["expiration"] = kwargs.get("expiration", 60000)
        kwargs["view_duration"] = kwargs.get("view_duration", 15000)

        Intervention.__init__(self, manager, **kwargs)

        # Keep track of who's voting to leave
        self.__voting_to_leave = { p.id: False for p in self.manager.participants }

        # Need to keep track of response IDs, to know whether the response is
        # in conjunction with our given request
        self._response_ids = set()

        self._min_unassigned_bombs = kwargs.get('min_unassigned_bombs', 1)
        self._max_unassigned_percentage = kwargs.get('max_unassigned_percentage', 0.10)

        # Keep the unassigned steps for later query, if needed
        self._unassigned_steps = []
        self._unassigned_percentage = -1

        # Default message, and relevant participants
        self._message = "You seem to have missed assigning players to defuse some bombs.  Did you mean to do this?"
        for participant in self.manager.participants:
            self.addRelevantParticipant(participant.id)

        # Listen to the Minecraft bridge for UI_Clicks that may be associated
        # with voting to leave the store, and Redis bus responses to plan
        # queries
        self.add_minecraft_callback(MissionStageTransition, self.__onMissionStageTransition)
        self.add_minecraft_callback(UI_Click, self.__onUI_Click)
        self.add_redis_callback(self.__onPlanQueryResponse, 'plan_supervisor/response/plan')


    @property
    def unassigned_steps(self):
        return self._unassigned_steps

    @property
    def unassigned_percentage(self):
        return self._unassigned_percentage
    


    def __onUI_Click(self, message):
        """
        Callback when a UI_Click message is received
        """

        # Is this message a vote to leave the shop?
        if message.additional_info['meta_action'] != 'VOTE_LEAVE_STORE':
            return

        self.__voting_to_leave[message.participant_id] = not self.__voting_to_leave[message.participant_id]

        # Is the participant voting to leave the store?
        if self.__voting_to_leave[message.participant_id]:
            self.logger.debug(f'{self}:  Participant {message.participant_id} is voting to leave the store')

            # Query the plan supervisor for any plan steps where one of the
            # participants disagrees
            # TODO:  Need to fuse the message spec with the non-full version. 
            message_payload = { 'name': None, 
                                'participants': None,
                                'conditions': [
                                    { 'type': 'has-no-actors',
                                      'parameters': { 
                                      }
                                    }
                                ] }
            response_id = self.manager.request_redis(message_payload, 
                                                     "plan_supervisor/query/plan/full",
                                                     blocking=False)
            self._response_ids.add(response_id)

            self.logger.debug(f'{self}:  Waiting for response ID {response_id}')


    def __onPlanQueryResponse(self, message):
        """
        Process a response from the Plan Supervisor
        """

        self.logger.debug(f'{self}:  Query Response Received: {message}')
        self.logger.debug(f'{self}:    Data: {message.data}')

        # Ignore any responses that show up if this intervention is no longer
        # active
        if self.state != Intervention.State.ACTIVE:
            self.logger.debug(f'{self}:  Intervention no longer active, ignoring {message}')

        # Only handle response IDs that this Intervention sent the query for
        if not message.request_id in self._response_ids:
            self.logger.debug(f'{self}:  Ignoring response with no matching request ID {message}')
            return

        # Remove the request ID from the set of request IDs to respond to
        self._response_ids.discard(message.request_id)

        steps = message.data.get('steps', {}).get('full', [])

        # Only concerned about steps about disarming bombs
        defuse_bomb_steps = [s for s in steps if s.get('action', {'type': None})['type'] == 'DefuseBomb']
        self._unassigned_steps = [s for s in defuse_bomb_steps if len(s['action']['actors']) == 0]
        self._unassigned_percentage = len(self._unassigned_steps) / max(len(defuse_bomb_steps),1)

        self.logger.debug(f'{self}:    Total Defuse Bomb Steps: {len(defuse_bomb_steps)}')
        self.logger.debug(f'{self}:    Total Without Actors:    {len(self._unassigned_steps)}')

        self.logger.debug(f'{self}:    Defuse Bomb Steps')
        for s in defuse_bomb_steps:
            self.logger.debug(f'{self}:      {s}')

        # Do nothing if the number of defuse bomb steps without actors is below
        # the `min_unassigned_bombs` threshold
        if len(self._unassigned_steps) < self._min_unassigned_bombs:
            self.logger.debug(f'{self}:    Number of unassigned bomb steps below minimum threshold')
            return

        # Do nothing if the percentage of unassigned bombs is above the
        # `max_unassigned_percentage` threshold
        if self._unassigned_percentage > self._max_unassigned_percentage:
            self.logger.debug(f'{self}:    Percentage of unassigned bomb steps exceed maximum percentage')
            return

        # All conditions are met to issue the intervention
        if len(self._response_ids) > 0:
            self.logger.debug(f'{self}:  Resolving with unresolved Queries: {self._response_ids}')

        self.queueForResolution()



    def __onMissionStageTransition(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.MissionStageTransition
            Received MissionStageTransition message
        """

        self.logger.debug(f"{self}: Received Message {message}")

        # Discard the intervention if left the shop

        if message.mission_stage == MissionStage.FieldStage:

            if len(self._response_ids) > 0:
                self.logger.debug(f'{self}:  Resolving with unresolved Queries: {self._response_ids}')

            self.discard()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {     
            'default_recipients': [p.id for p in self.manager.participants],
            'default_message': self._message,
            'default_responses': ["Yes", "No", "Skip"]
        }