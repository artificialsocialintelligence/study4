# -*- coding: utf-8 -*-
"""
.. module:: ignore_bomb_due_to_fire
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for intervention to suggest to players 
              to ignore certain bombs if a fire will reach it before they can
              defuse it.

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to suggest that the team ignores certain bombs if a fire
will reach it before they can defuse it.

* Trigger: The intervention is triggered for each bomb seen.

* Discard States:  Bomb is removed from the environment, for whatever reason

* Resolve State:  A fire will reach the bomb before any players can

* Followups:  
"""

from .intervention import IgnoreBombDueToFireIntervention
from .trigger import IgnoreBombDueToFireTrigger

InterventionClass = IgnoreBombDueToFireIntervention
TriggerClass = IgnoreBombDueToFireTrigger
IndividualComplianceFollowupClass = None
TeamComplianceFollowupClass = None

default_parameters = {}

AdditionalFollowupClasses = []