# -*- coding: utf-8 -*-
"""
.. module:: ignore_bomb_due_to_fire.trigger
   :platform: Linux, Windows, OSX
   :synopsis: Trigger for Intervention to not bother with bombs that would be
              exploded due to fire if the team is unable to reach it in time.

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a Trigger class designed to spawn interventions to alert players
not to bother defusing a bomb if its going to be blown up by fire.
"""

from InterventionManager import InterventionTrigger

from MinecraftBridge.messages import FoVSummary
from MinecraftElements.blocks import Block

from shared.fire_monitor import FireMonitor

from .intervention import IgnoreBombDueToFireIntervention

class IgnoreBombDueToFireTrigger(InterventionTrigger):
    """
    Class that generates IgnoreBombDueToFireInterventions.  Interventions
    are triggered whenever a bomb is detected
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        fire_spread_rate : float, default = 6.0
            Number of seconds required for a fire to move one block
        player_velocity : float, default = 4.0
            Number of blocks a player can cover in one second
        """

        InterventionTrigger.__init__(self, manager, **kwargs)

        # Keep track of fires and spawned interventions
        self._fire_monitor = FireMonitor(self)
        self._interventions = {}

        self._fire_spread_rate = kwargs.get("fire_spread_rate", 15.0)
        self._player_velocity = kwargs.get("player_velocity", 4.6)

        # Register the trigger to receive messages when mission stage changes
        self.add_minecraft_callback(FoVSummary, self.__onFoV)


    def __onFoV(self, message):
        """
        Callback when an FoV message is recieved.  Checks to see if 1) there are
        any bombs in the FoV and 2) spawns an intervention if one doesn't exist
        for the bomb.
        """

        # Check to see if there is a bomb in the list of observed blocks
        for block in message.blocks:
            # Check if the block is a bomb
            bomb_id = block.get_attribute('testbed_id')
            if bomb_id is None:
                bomb_id = 'UNKNOWN'

            if 'BOMB' in bomb_id:
                bomb_location = block.location

                # Store the intervention, in case this bomb_id is seen at another
                # time
                if not bomb_id in self._interventions:
                    intervention = IgnoreBombDueToFireIntervention(self.manager,
                                                                   self._fire_monitor,
                                                                   bomb_id,
                                                                   bomb_location,
                                                                   fire_spread_rate=self._fire_spread_rate,
                                                                   player_velocity=self._player_velocity)
                    self._interventions[bomb_id] = intervention

                    self.logger.info(f'{self}:  Spawning {intervention} for Bomb {bomb_id} at location {bomb_location}')
                    self.manager.spawn(intervention)
                else:
                    self.logger.debug(f'{self}:  Ignoring bomb {bomb_id}---IgnoreBomdDueToFireIntervention already spawned')
