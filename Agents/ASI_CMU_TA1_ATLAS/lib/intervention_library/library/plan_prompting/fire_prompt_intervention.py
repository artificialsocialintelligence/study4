# -*- coding: utf-8 -*-
"""
.. module:: plan_prompting.fire_prompt_intervention
   :platform: Linux, Windows, OSX
   :synopsis: Intervention for planning prompts

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class designed to generate a planning prompt.
"""

from InterventionManager import Intervention

from MinecraftBridge.messages import MissionStageTransition, MissionStage

from MinecraftBridge.messages import (
    PlayerState
)


class FirePromptIntervention(Intervention):
    """
    A prompt given to the team do query on how they plan on dealing with fire.
    """

    def __init__(self, manager, delay=0, next_prompt=None, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention
        delay : int, default=0
            Number of milliseconds after activated that the intervention should
            wait before queuing for resolution
        next_prompt : PromptIntervention, default=None
            Next prompt to issue (if any)

        Keyword Arguments
        -----------------
        message : string
            Welcome message to greet the team with
        """

        # Default values for intervention prioritization information
        kwargs["priority"] = kwargs.get("priority", 0.5)
        kwargs["expiration"] = kwargs.get("expiration", 120000)
        kwargs["view_duration"] = kwargs.get("view_duration", 30000)

        Intervention.__init__(self, manager, **kwargs)

        self._delay = delay
        self._next_prompt = next_prompt

        self._prompt = "Discuss (in chat) with your team how you plan to deal with fire.  Which of the following options most closely aligns with your plan?"
        self._prefix = kwargs.get("prefix", "")
        self._responses = ["Assign one person to be responsible to put out all the fires",
                           "Distribute responsibility among the team",
                           "Return to the shop to re-plan when fire breaks out"]

        self._presentation_time = None

        # Disagreement intervention
        self._player_disagreement_intervention = kwargs.get("player_disagreement_intervention", None)        
        
        # Indicate that all participants are relevant to this intervention
        for participant in self.manager.participants:
            self.addRelevantParticipant(participant.participant_id)

        self.add_minecraft_callback(PlayerState, self.__onPlayerState)
        self.add_minecraft_callback(MissionStageTransition, self.__onMissionStageTransition)


    def _on_response_received(self):
        """
        """

        print("response_received")



    def __onPlayerState(self, message):
        """
        Used to monitor time
        """

        # Set the resolution time if it hasn't been done so yet
        if self._presentation_time is None:
            self.logger.debug(f'{self}:  Presentation time set to {self._presentation_time}')
            self._presentation_time = message.elapsed_milliseconds + self._delay

        # Check if it should be resolved
        if message.elapsed_milliseconds > self._presentation_time:
            self.logger.debug(f'{self}:  Reached presentation time: {self._presentation_time}')
            self.queueForResolution()


    def _onResolved(self):
        """
        Callback when the intervention has been resolved
        """

        self.logger.debug(f'{self}:  Intervention resolved, spawning next prompt: {self._next_prompt}')

        if self._next_prompt is not None:
            self.manager.spawn(self._next_prompt)


    def __onMissionStageTransition(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.MissionStageTransition
            Received MissionStageTransition message
        """

        self.logger.debug(f"{self}: Received Message {message}")

        # An intervention should only be spawned if 1) the mission stage is the
        # Shop stage, and 2) then number of times the team has transitioned to 
        # the shop is 1

        if message.mission_stage == MissionStage.FieldStage:
            self.discard()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {     
            'default_recipients': [p.id for p in self.manager.participants],
            'default_message': self._prefix + self._prompt,
            'default_responses': self._responses
        }