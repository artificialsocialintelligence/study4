# -*- coding: utf-8 -*-
"""
.. module:: plan_prompting.trigger
   :platform: Linux, Windows, OSX
   :synopsis: Trigger for Planning Prompts

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a Trigger class designed to spawn planning prompts.
"""

from InterventionManager import InterventionTrigger

from MinecraftBridge.messages import MissionStageTransition, MissionStage

from .fire_prompt_intervention import FirePromptIntervention
from .beacon_prompt_intervention import BeaconPromptIntervention
from .medic_prompt_intervention import MedicPromptIntervention

from .player_disagreement_intervention import PlayerDisagreementIntervention


class PlanPromptTrigger(InterventionTrigger):
    """
    Class that generates Planning Prompts.  The initial intervention is
    triggered at the start of the first Shop phase
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        message : string
            Welcome message to greet the team with
        """
        InterventionTrigger.__init__(self, manager, **kwargs)
        self.kwargs = kwargs

        # Register the trigger to receive messages when mission stage changes
        self.add_minecraft_callback(MissionStageTransition, self.__onMissionStageTransition)

        # Create a PlayerDisagreementIntervention, which will be passed to the
        # other interventions to track whether or not players agreed on the
        # prompts
        self._disagreement_intervention = PlayerDisagreementIntervention(self.manager)


        self._prefix = "Now, consider the information you collected during your recon as well as the guidance given in the pre-mission brief to create a plan for how your team will coordinate, dispose of the bombs, and deal with any threats. I will ask you a few questions to get you started. "


    def __onMissionStageTransition(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.MissionStageTransition
            Received MissionStageTransition message
        """

        self.logger.debug(f"{self}: Received Message {message}")

        # An intervention should only be spawned if 1) the mission stage is the
        # Shop stage, and 2) then number of times the team has transitioned to 
        # the shop is 1

        if message.mission_stage == MissionStage.ShopStage and \
           message.transitionsToShop <= 1:

           beacon_prompt = BeaconPromptIntervention(self.manager, 40000, player_disagreement_intervention=self._disagreement_intervention)
           medic_prompt = MedicPromptIntervention(self.manager, 40000, next_prompt=beacon_prompt, player_disagreement_intervention=self._disagreement_intervention)
           fire_prompt = FirePromptIntervention(self.manager, 40000, next_prompt=medic_prompt, prefix=self._prefix, player_disagreement_intervention=self._disagreement_intervention)

           self.logger.info(f'{self}:  Spawning {fire_prompt}')
           self.manager.spawn(fire_prompt)