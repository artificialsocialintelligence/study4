# -*- coding: utf-8 -*-
"""
.. module:: plan_prompting.player_disagreement_intervention
   :platform: Linux, Windows, OSX
   :synopsis: Intervention for planning prompts

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class designed to indicate when players disagree
on choices for the prompt questions
"""

from InterventionManager import Intervention

from .beacon_prompt_intervention import BeaconPromptIntervention
from .fire_prompt_intervention import FirePromptIntervention
from .medic_prompt_intervention import MedicPromptIntervention


class PlayerDisagreementIntervention(Intervention):
    """
    An intervention that monitors responses from players and, if there are
    disagreements, issues advice that the players disagree and should discuss.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention

        Keyword Arguments
        -----------------
        message : string
            Welcome message to greet the team with
        """

        # Default values for intervention prioritization information
        kwargs["priority"] = kwargs.get("priority", 0.5)
        kwargs["expiration"] = kwargs.get("expiration", 60000)
        kwargs["view_duration"] = kwargs.get("view_duration", 15000)

        Intervention.__init__(self, manager, **kwargs)

        # Response dictionary -- initialize all response classes to -1 to
        # indicate that a response hasn't been provided
        self.__player_responses = { p.id: { BeaconPromptIntervention: -1,
                                            FirePromptIntervention: -1,
                                            MedicPromptIntervention: -1 }
                                    for p in self.manager.participants }

        self._include_responses = False

        # Default message is to thank the players for responding to the prompts
        self.__message = "Good job discussing the high-level planning aspects.  This will help you perform better as a team."


    def set_response(self, participant_id, intervention_class, reponse_option):
        """
        Set the response that a player made to a given prompt

        Arguments
        ---------
        participant_id : str
            Identifier of the participant who made the response
        intervention_class : Intervention
            Prompt intervention
        response_option : int
            Response provided by the participant
        """

        self.__player_responses[participant_id][intervention_class] = response_option

        # Check if all reaponses have been returned, and intervene as necessary
        self.__check_responses()


    def __check_responses(self):
        """
        Checks to see if all responses have been gathered, and intervene if
        there is disagreement.
        """

        disagreements = []

        for InterventionClass in { BeaconPromptIntervention, 
                                   FirePromptIntervention, 
                                   MedicPromptIntervention }:

            responses = [response[InterventionClass] 
                         for response in self.__player_responses.values()]

            # Are any responses -1?  i.e., has any player _not_ responded to
            # a prompt?  If so, then there's no need to continue, as not all
            # responses have been received
            for response in responses:
                if response == -1:
                    return

            # Are all the responses in agreement?  This is the case if the list
            # of responses is converted to a set with a single item.  If not,
            # add the InterventionClass to the disagreements
            if len(set(responses)) > 1:
                disagreements.append(InterventionClass)

        # Convert the disagreements to text
        disagreement_text = { BeaconPromptIntervention: "responding to each other's beacons",
                              FirePromptIntervention: "dealing with fire",
                              MedicPromptIntervention: "helping injured teammates"}
        disagreements = [ disagreement_text[d] for d in disagreements ]

        # Create a message based on whether there are any disagreements
        if len(disagreements) == 0:
            self.__message = "You all agreed upon the high-level planning prompts.  You should do well having a shared understanding of how to respond to different scenarios."
            self._include_responses = False
        elif len(disagreements) == 1:
            self.__message = f"You seem to not agree upon {disagreements[0]}.  You should discuss this further before proceeding to the Field stage."
            self._include_responses = True
        elif len(disagreements) == 2:
            self.__message = f"You seem to not agree upon {disagreements[0]} and {disagreements[1]}.  You should discuss this further before proceeding to the Field stage."
            self._include_responses = True
        else:
            self.__message = f"You seem to not agree upon {disagreements[0]}, {disagreements[1]}, and {disagreements[2]}.  You should discuss this further before proceeding to the Field stage."
            self._include_responses = True

        # Resolve this intervention
        self.queueForResolution()




    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """

        if self._include_responses:
            responses = ["I agree", "I disagree", "Skip"]
        else:
            responses = []

        return {     
            'default_recipients': [p.id for p in self.manager.participants],
            'default_message': self.__message,
            'default_responses': responses
        }