# -*- coding: utf-8 -*-
"""
.. module:: team_welcome_message
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for introductory mmessage for team

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to prompt the team to certain planning questions.

* Trigger: The intervention is triggered at the start of the first Shop Phase

* Discard States:  N/A

* Resolve State:  

* Followups:  
"""

from .fire_prompt_intervention import FirePromptIntervention
from .medic_prompt_intervention import MedicPromptIntervention
from .beacon_prompt_intervention import BeaconPromptIntervention
from .player_disagreement_intervention import PlayerDisagreementIntervention

from .fire_prompt_response_followup import FirePromptResponseFollowup
from .medic_prompt_response_followup import MedicPromptResponseFollowup
from .beacon_prompt_response_followup import BeaconPromptResponseFollowup

from .trigger import PlanPromptTrigger

InterventionClass = None
TriggerClass = PlanPromptTrigger
IndividualComplianceFollowupClass = None
TeamComplianceFollowupClass = None

default_parameters = {}

AdditionalFollowupClasses = []