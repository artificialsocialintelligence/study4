# -*- coding: utf-8 -*-
"""
.. module:: plan_prompting.medic_prompt_response_followup
   :platform: Linux, Windows, OSX
   :synopsis: Followup to handle response to medic prompt

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a Reponse Followup class to handle responses to prompts to
responding to medic role.
"""

from InterventionManager import ResponseFollowup

from .medic_prompt_intervention import MedicPromptIntervention


class MedicPromptResponseFollowup(ResponseFollowup):
    """
    Followup that listens to responses to prompts about the medic role.
    """

    InterventionClass = MedicPromptIntervention

    def __init__(self, intervention, manager, participant_id, **kwargs):
        """
        Arguments
        ---------
        intervention : MedicPromptIntervention
            Intervention that this followup is listening for a response to
        manager : InterventionManager
        participant_id : str
            Identifier of the participant that should be responding
        """

        # Initialize the superclass, which handles listening to the response 
        # messages
        ResponseFollowup.__init__(self, intervention, manager, participant_id)

        # Populate handlers for the available options
        self.set_response_handler(0, self.__one_person_responsible_response)
        self.set_response_handler(1, self.__distribute_responsibility_response)
        self.set_response_handler(2, self.__most_health_response)


    def __one_person_responsible_response(self):
        """
        Handler when a player responds to the Medic prompt with option 0,
        'Assign one person to be responsible for helping them'.
        """

        self.logger.info(f'{self}:  Participant {self.participant_id} responded with Option 0')

        if self.intervention._player_disagreement_intervention is not None:
            self.intervention._player_disagreement_intervention.set_response(self.participant_id,
                                                                             MedicPromptIntervention,
                                                                             0)

            try:
                self.intervention._on_response_received()
            except Exception as e:
                print(f"Error when trying to indicate response was received by {intervention}: {e}")


    def __distribute_responsibility_response(self):
        """
        Handler when a player responds to the Medic prompt with option 1,
        'Distribute responsibility among the team'.
        """

        self.logger.info(f'{self}:  Participant {self.participant_id} responded with Option 1')

        if self.intervention._player_disagreement_intervention is not None:
            self.intervention._player_disagreement_intervention.set_response(self.participant_id,
                                                                             MedicPromptIntervention,
                                                                             1)
            try:
                self.intervention._on_response_received()
            except Exception as e:
                print(f"Error when trying to indicate response was received by {intervention}: {e}")


    def __most_health_response(self):
        """
        Handler when a player responds to the Medic prompt with option 2,
        'Whoever has the most health'.
        """

        self.logger.info(f'{self}:  Participant {self.participant_id} responded with Option 2')

        if self.intervention._player_disagreement_intervention is not None:
            self.intervention._player_disagreement_intervention.set_response(self.participant_id,
                                                                             MedicPromptIntervention,
                                                                             2)

            try:
                self.intervention._on_response_received()
            except Exception as e:
                print(f"Error when trying to indicate response was received by {intervention}: {e}")
