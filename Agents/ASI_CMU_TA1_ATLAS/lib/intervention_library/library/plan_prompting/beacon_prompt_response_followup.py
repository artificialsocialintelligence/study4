# -*- coding: utf-8 -*-
"""
.. module:: plan_prompting.beacon_prompt_response_followup
   :platform: Linux, Windows, OSX
   :synopsis: Followup to handle response to beacon prompt

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a Reponse Followup class to handle responses to prompts to
beacons.
"""

from InterventionManager import ResponseFollowup

from .beacon_prompt_intervention import BeaconPromptIntervention


class BeaconPromptResponseFollowup(ResponseFollowup):
    """
    Followup that listens to responses to prompts about the beacon.
    """

    InterventionClass = BeaconPromptIntervention

    def __init__(self, intervention, manager, participant_id, **kwargs):
        """
        Arguments
        ---------
        intervention : BeaconPromptIntervention
            Intervention that this followup is listening for a response to
        manager : InterventionManager
        participant_id : str
            Identifier of the participant that should be responding
        """

        # Initialize the superclass, which handles listening to the response 
        # messages
        ResponseFollowup.__init__(self, intervention, manager, participant_id)

        # Populate handlers for the available options
        self.set_response_handler(0, self.__closest_person_response)
        self.set_response_handler(1, self.__beacon_manager_response)
        self.set_response_handler(2, self.__group_evaluation_response)


    def __closest_person_response(self):
        """
        Handler when a player responds to the Beacon prompt with option 0,
        'Closest person should go to help right away'.
        """

        self.logger.info(f'{self}:  Participant {self.participant_id} responded with Option 0')

        if self.intervention._player_disagreement_intervention is not None:
            self.intervention._player_disagreement_intervention.set_response(self.participant_id,
                                                                             BeaconPromptIntervention,
                                                                             0)

            try:
                self.intervention._on_response_received()
            except Exception as e:
                print(f"Error when trying to indicate response was received by {intervention}: {e}")


    def __beacon_manager_response(self):
        """
        Handler when a player responds to the Beacon prompt with option 1,
        'One person should be assigned to monitor beacons, and respond when 
        beacons are placed'.
        """

        self.logger.info(f'{self}:  Participant {self.participant_id} responded with Option 1')

        if self.intervention._player_disagreement_intervention is not None:
            self.intervention._player_disagreement_intervention.set_response(self.participant_id,
                                                                             BeaconPromptIntervention,
                                                                             1)
            try:
                self.intervention._on_response_received()
            except Exception as e:
                print(f"Error when trying to indicate response was received by {intervention}: {e}")


    def __group_evaluation_response(self):
        """
        Handler when a player responds to the Beacon prompt with option 2,
        'Evaluate beacons as a group when we return to the shop'.
        """

        self.logger.info(f'{self}:  Participant {self.participant_id} responded with Option 2')

        if self.intervention._player_disagreement_intervention is not None:
            self.intervention._player_disagreement_intervention.set_response(self.participant_id,
                                                                             BeaconPromptIntervention,
                                                                             2)
            try:
                self.intervention._on_response_received()
            except Exception as e:
                print(f"Error when trying to indicate response was received by {intervention}: {e}")