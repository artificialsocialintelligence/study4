# -*- coding: utf-8 -*-
"""
.. module:: shared.hazard_monitor
   :platform: Linux, Windows, OSX
   :synopsis: Simple class to provide monitoring of hazard beacon creation / destruction

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

"""

import collections

import MinecraftElements

from MinecraftBridge.utils import Loggable
from MinecraftBridge.messages import (
   EnvironmentCreatedSingle,
   EnvironmentRemovedSingle
)


class HazardBeaconMonitor(Loggable):
   """
   The HazardBeaconMonitor class 

   Attributes
   ----------

   Methods
   -------

   Usage
   -----


   """


   def __init__(self, parent):
      """
      Arguments
      ---------
      parent
         Parent component that hosts this monitor.  Note that this should be a
         Trigger, to ensure that the callbacks are properly added prior to 
         events occuring in Minecraft.
      """

      self._parent = parent

      # hazard_beacons is a dictionary mapping the id of a hazard beacon to its
      # creation time (elapsed_milliseconds)
      self._hazard_beacons = dict()

      # Add callbacks for EnvironmentCreated and EnvironmentDestroyed messages
      self._parent.add_minecraft_callback(EnvironmentCreatedSingle, self.__onEnvironmentCreated)
      self._parent.add_minecraft_callback(EnvironmentRemovedSingle, self.__onEnvironmentDestroyed)


   @property
   def hazard_beacons(self):
      return self._hazard_beacons
   


   def __onEnvironmentCreated(self, message):
      """
      Arguments
      ---------
      message : EnvironmentCreatedSingle
      """

      self.logger.debug(f'{self}:  Received {message}')
      self.logger.debug(f'{self}:    Created Object: {message.object}')

      # Is the message about a hazard beacon?
      if message.object.type != MinecraftElements.Block.block_beacon_hazard:
         self.logger.debug(f'{self}:    Message not about a hazard beacon (block type = {message.object.type})')
         return

      # Store that this item was created at the current time
      self._hazard_beacons[message.object.id] = message.elapsed_milliseconds


   def __onEnvironmentDestroyed(self, message):
      """
      Arguments
      ---------
      message : EnvironmentRemovedSingle      
      """

      self.logger.debug(f'{self}:  Received {message}')
      self.logger.debug(f'{self}:    Removed Object: {message.object}')

      # Remove the entry with this hazard beacon id
      if not message.object.id in self._hazard_beacons:
         self.logger.debug(f'{self}:  Unable to find hazard beacon with ID {message.object.id} in set of hazard beacons')
      else:
         del self._hazard_beacons[message.object.id]