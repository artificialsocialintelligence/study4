# -*- coding: utf-8 -*-
"""
.. module:: shared.bomb_defusal_success_monitor
   :platform: Linux, Windows, OSX
   :synopsis: Simple class to monitor the success rate of defusing bombs

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

"""

import collections

import MinecraftElements

from MinecraftBridge.utils import Loggable
from MinecraftBridge.messages import (
   ObjectStateChange
)


class BombDefusalSuccessMonitor(Loggable):
   """
   The BombDefusalSuccess class 

   Attributes
   ----------

   Methods
   -------

   Usage
   -----


   """


   def __init__(self, parent):
      """
      Arguments
      ---------
      parent
         Parent component that hosts this monitor.  Note that this should be a
         Trigger, to ensure that the callbacks are properly added prior to 
         events occuring in Minecraft.
      """

      self._parent = parent

      self.__defused_bombs = collections.defaultdict(self.__create_new_entry)

      # Add callbacks for ObjectStateChange messages
      self._parent.add_minecraft_callback(ObjectStateChange, self.__onObjectStateChange)

   @property
   def defused_bombs(self):
      return self.__defused_bombs


   def team_failure_rate(self):
      """
      Return the percentage of bombs that exploded due to wrong tool used
      """

      # Count as success any bombs that 1) is in the "DEFUSED" outcome, and
      # 2) had more than one participant.  Count as failure any bomb that
      # 1) is in the "EXPLODED_TOOL_MISMATCH" outcome, and 2) had more than one
      # participant.
      success_count = 0
      failure_count = 0

      for bomb_info in self.__defused_bombs.values():
         if len(bomb_info['participants']) > 1 and bomb_info['outcome'] == 'DEFUSED':
            success_count += 1
         elif len(bomb_info['participants']) > 1 and bomb_info['outcome'] == "EXPLODE_TOOL_MISMATCH":
            failure_count += 1

      # Math time!
      total_count = success_count + failure_count

      if total_count == 0:
         return 0.0
      else:
         return failure_count / total_count


   def number_of_bombs_engaged(self, participant_id):
      """
      Get the number of bombs engaged in by the participant
      """

      count = 0

      for info in self.__defused_bombs.values():
         if participant_id in info['participants']:
            count += 1

      return count


   def number_of_successful_bombs_engaged(self, participant_id):
      """
      Get the number of bombs engaged in by the participant, if the bomb
      was successfully defused
      """

      count = 0

      for info in self.__defused_bombs.values():
         if participant_id in info['participants'] and info['successful'] == True:
            count += 1

      return count


   def number_of_unsuccessful_bombs_engaged(self, participant_id):
      """
      Get the number of bombs engaged in by the participant, if the bomb
      was not successfully defused
      """

      count = 0

      for info in self.__defused_bombs.values():
         if participant_id in info['participants'] and info['successful'] == False:
            count += 1

      return count


   def number_of_failed_actions(self, participant_id):
      """
      Get the number of times the participant performed an incorrect action
      """

      count = 0

      for info in self.__defused_bombs.values():

         for step in info['defusal_steps']:
            if step[1] == participant_id and step[0] == 'EXPLODE_TOOL_MISMATCH':
               count += 1

      return count

   def __create_new_entry(self):
      """
      Create a new entry for the defused bomb dictionary
      """

      return { 'outcome': None,
               'successful': None,
               'participants': set(),
               'defusal_steps': []
             }


   def __onObjectStateChange(self, message):
      """
      Callback when an object stat change message is recieved
      """

      self.logger.debug(f'{self}:  Received {message}')

      # Is the message about a bomb?
      if message.type not in {'block_bomb_standard',
                              'block_bomb_chained',
                              'block_bomb_fire'}:
         self.logger.debug(f'{self}:    Object State Change message for block of type {messag.type}')
         return

      # Get the bomb_id and populate the entry
      bomb_id = message.id

      # Determine if the triggering entity was a player, and add to the participant set
      if message.triggering_entity != 'SERVER':
         self.__defused_bombs[bomb_id]['participants'].add(message.triggering_entity)

      # Was this a defusal step?
      outcome = message.current_attributes.get_attribute("outcome")
      sequence = message.current_attributes.get_attribute("sequence")
      if outcome in { 'TRIGGERED_ADVANCE_SEQ', 'EXPLODE_CHAINED_ERROR', 'TRIGGERED', 'EXPLODE_TOOL_MISMATCH' }:
         self.__defused_bombs[bomb_id]['defusal_steps'].append((outcome, message.triggering_entity, sequence))

      # Is this a final outcome
      if outcome in { 'EXPLODE_CHAINED_ERROR', 
                      'EXPLODE_TIME_LIMIT',
                      'EXPLODE_TOOL_MISMATCH',
                      'EXPLODE_FIRE', 
                      'DEFUSED_DISPOSER', 
                      'DEFUSED' }:
         self.__defused_bombs[bomb_id]['outcome'] = outcome

         # Was it successfully defused?
         if outcome in { 'DEFUSED_DISPOSER', 'DEFUSED' }:
            self.__defused_bombs[bomb_id]['successful'] = True
         else:
            self.__defused_bombs[bomb_id]['successful'] = False

      self.logger.info(f"{self}:  Updated entry for {bomb_id}")
      self.logger.info(f"{self}:    Outcome:       {self.__defused_bombs[bomb_id]['outcome']}")
      self.logger.info(f"{self}:    Successful:    {self.__defused_bombs[bomb_id]['successful']}")
      self.logger.info(f"{self}:    Participants:  {self.__defused_bombs[bomb_id]['participants']}")
      self.logger.info(f"{self}:    Defusal Steps: {self.__defused_bombs[bomb_id]['defusal_steps']}")




