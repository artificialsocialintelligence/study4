# -*- coding: utf-8 -*-
"""
.. module:: shared.purchasing_monitor
   :platform: Linux, Windows, OSX
   :synopsis: Simple class to provide monitoring of what players are intending
              to purchase based on UI clicks

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

"""

import collections

import MinecraftElements

from MinecraftBridge.utils import Loggable
from MinecraftBridge.messages import (
   UI_Click
)


class PurchaseMonitor(Loggable):
   """
   The PurchaseMonitor class 

   Attributes
   ----------

   Methods
   -------

   Usage
   -----


   """

   tools = {''}


   def __init__(self, parent):
      """
      Arguments
      ---------
      parent
         Parent component that hosts this monitor.  Note that this should be a
         Trigger, to ensure that the callbacks are properly added prior to 
         events occuring in Minecraft.
      """

      self._parent = parent

      # Add callbacks for EnvironmentCreated and EnvironmentDestroyed messages
      self._parent.add_minecraft_callback(UI_Click, self.__onUI_Click)

      # Purchase dictionary
      self._purchase_dict = { p: collections.defaultdict(lambda: 0)
                              for p in self._parent.manager.participants }


      # Observers will be informed whenever a player's inventory purchase updates
      self.__observers = set()


   def __onUI_Click(self, message):
      """
      Callback when a UI click message is received, used to update the proposed
      purchases.
      """

      # Was this a purchase?
      if message.additional_info.get('meta_action','').startswith('PROPOSE_TOOL_PURCHASE_'):
         tool = self.tools[message.additional_info['item_code']]
         participant_id = message.participant_id

         if message.additional_info['meta_action'] == 'PROPOSE_TOOL_PURCHASE_+':
            self._purchase_dict[participant_id][self.tools[tool]] += 1
         else:
            self._purchase_dict[participant_id][self.tools[tool]] -= 1

         self._purchase_dict[participant_id][self.tools[tool]] = max(0, self._purchase_dict[participant_id][self.tools[tool]])

         self.__notify_of_intended_purchase_update(participant_id)


   def add_observer(self, observer):
      """
      Add an observer to the monitor to be informed whenever a fire group is
      fully removed.  Observers must implment the following methods::

      * onIntendedPurchaseUpdate(participant_id)

      Arguments
      ---------
      observer
         Object to be informed whenever an event is to be reported
      """

      self.__observers.add(observer)


   def purchase_counts(self, participant_id):
      """
      Provide a count of purchases that the participant is proposing
      """

      return self._purchase_dict[participant_id]


   def remove_observer(self, observer):
      """
      Remove an observer from the monitor

      Arguments
      ---------
      observer
         Object to be removed
      """

      self.__observers.discard(observer)


   def __notify_of_intended_purchase_update(self, participant_id):
      """
      Notify observers of a participant's intended purchases being updated

      Arguments
      ---------
      group_id
         Identifer of the fire group that has been removed
      """

      for observer in self.__observers:
         try:
            observer.onIntendedPurchaseUpdate(participant_id)
         except Exception as e:
            self.logger.warning(f'{self}:  Exception when notifying observer {observer}')
            self.logger.warning(f'{self}:      {str(e)}')
