# -*- coding: utf-8 -*-
"""
.. module:: shared.search_monitor
   :platform: Linux, Windows, OSX
   :synopsis: Simple class to provide monitoring of how much the team has
              searched (based on player state messages)

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

"""

import collections

import MinecraftElements

from MinecraftBridge.utils import Loggable
from MinecraftBridge.messages import (
   PlayerState
)


class SearchMonitor(Loggable):
   """
   The SearchMonitor class 

   Attributes
   ----------

   Methods
   -------

   Usage
   -----


   """


   def __init__(self, parent):
      """
      Arguments
      ---------
      parent
         Parent component that hosts this monitor.  Note that this should be a
         Trigger, to ensure that the callbacks are properly added prior to 
         events occuring in Minecraft.
      """

      self._parent = parent

      # Searched locations
      self._desert_region = set()
      self._village_region = set()
      self._forest_region = set()


      # Add callbacks for PlayerState messages
      self._parent.add_minecraft_callback(PlayerState, self.__onPlayerState)

      # Observers will be informed whenever a fire group is removed
      self.__observers = set()


   def add_observer(self, observer):
      """
      Add an observer to the monitor to be informed whenever a fire group is
      fully removed.  Observers must implment the following methods::

      * onFireGroupRemoved(group_id)

      Arguments
      ---------
      observer
         Object to be informed whenever an event is to be reported
      """

      self.__observers.add(observer)


   def remove_observer(self, observer):
      """
      Remove an observer from the monitor

      Arguments
      ---------
      observer
         Object to be removed
      """

      self.__observers.discard(observer)


   def __onPlayerState(self, message):
      """
      Arguments
      ---------
      message : PlayerState
      """

      self.logger.debug(f'{self}:  Received {message}')

      # Get the (x,z) location of the player, rounded down
      location = (int(message.x), int(message.z))

      # Only concern ourselves with locations within the field area
      if location[0] < 0 or location[0] > 50:
         return
      if location[1] < 0 or location[1] > 150:
         return

      if location[1] > 100:
         self._desert_region.add(location)
      elif location[1] > 50:
         self._village_region.add(location)
      else:
         self._forest_region.add(location)


   def desert_search_count(self):
      return len(self._desert_region)

   def village_search_count(self):
      return len(self._village_region)

   def forest_search_count(self):
      return len(self._forest_region)