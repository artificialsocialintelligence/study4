# -*- coding: utf-8 -*-
"""
.. module:: help_frozen_player.team_compliance
   :platform: Linux, Windows, OSX
   :synopsis: Followup to determine team compliance to HelpFrozenPlayer

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a Followup class designed to determine team compliance to
advice to helping frozen players.
"""


from InterventionManager import Followup

class HelpFrozenPlayerIndividualCompliance(Followup):
   """
   """

   def __init__(self, intervention, manager, **kwargs):
      """
      """

      Followup.__init__(self, intervention, manager, **kwargs)

      # Listen for if the player is unfrozen
      self.add_minecraft_callback(PlayerStateChange, self.__onPlayerStateChange)


   def participant_was_unfrozen(self):
      """
      Method invoked by other Followup instances when the player was unfrozen
      by a team mate
      """

      # Someone on the team has complied with the advice, so team compliance is
      # true
      if intervention.presented:
         self.participant_complied = True

      self.compete()


   def __onPlayerStateChange(self, message):
      """
      Callback when a player's state has changed

      Arguments
      ---------
      message : MinecraftBridge.message.PlayerStateChange
      """

      # Determine if this is about the frozen participant
      if message.participant_id == self.intervention.frozen_participant:

         # Did the player become unfrozen?
         is_frozen_attribute = message.changedAttributes.get_attribute('is_frozen')
         if is_frozen_attribute is not None and is_frozen_attribute.current == 'false':

            # If this message has been received, then the player's frozen state
            # has changed to unfrozen without help from and participant (if a
            # participant had unfrozen the player, it would have been indicated
            # by an ItemUsed message or participant_was_unfrozen call)

            if intervention.presented:
               self.participant_complied = False

            self.complete()