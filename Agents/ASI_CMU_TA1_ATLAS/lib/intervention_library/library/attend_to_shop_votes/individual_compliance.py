# -*- coding: utf-8 -*-
"""
.. module:: attend_to_field_votes.individual_compliance
    :platform: Linux, Windows, OSX
    :synopsis: Followup to determine individual compliance to AttendToFieldVotes

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a Followup class designed to determine individual compliance to
advice to attend to votes to return to the field
"""


from InterventionManager import Followup

from MinecraftBridge.messages import PlayerState
from MinecraftBridge.messages import CommunicationEnvironment

from .intervention import AttendToShopVotesIntervention


class AttendToShopVotesIndividualCompliance(Followup):
    """

    """

    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        intervention : AttendToShopVotesIntervention
            Intervention instance that this object is followuping up on
        manager : InterventionManager
        """

        Followup.__init__(self, intervention, manager, **kwargs)

        # Will wait until 1) a certain number of seconds has elapsed, or 2) when
        # the participant votes 
        self._noncompliance_time = self.intervention._current_time + kwargs.get('time_limit', 15000)

        self.add_minecraft_callback(CommunicationEnvironment, self.__onCommunication)
        self.add_minecraft_callback(PlayerState, self.__onPlayerState)


    def __onCommunication(self, message):
        """
        Callback when a CommunicationEnvironment message is received.
        """

        self.logger.debug(f'{self}:  Received message {message}')

        # Is the contents of the message about voting to go to the shop?
        if message.message.endswith('voted to go to the Shop!'):

            # Who voted to go to the shop?
            callsign = message.message.split(' ')[0]
            participant = self.manager.participants[callsign]

            if participant.id == self.intervention.relevant_participants[0]:
                # Player voted, go ahead and complete
                self._player_complied = True
                self.complete()
            else:
                # Someone else undid their vote---spawn a new AttendToShopVote
                # intervention to monitor now that there's only a single vote
                vote_times = { p: intervention.participant_vote_times[p]
                                  for p in intervention.participant_vote_times
                                  if p != self.intervention.relevant_participants[0] 
                                  and p != message.participant_id }

                intervention = AttendToShopVotesIntervention(self.manager, 
                                                             parent=self,
                                                             participant_vote_times=vote_times,
                                                             one_vote_duration=self.intervention._one_vote_duration,
                                                             two_vote_duration=self.intervention._two_vote_duration)
                self.manager.spawn(intervention)
                self.complete()               


    def __onPlayerState(self, message):
        """
        Callback when a player's state has changed, used to check if the time
        limit has been exceeded

        Arguments
        ---------
        message : MinecraftBridge.message.PlayerStateChange
        """

        # Is it time to record non-compliance?
        if message.elapsed_milliseconds > self._noncompliance_time:
            self._player_complied = False
            self.complete()
