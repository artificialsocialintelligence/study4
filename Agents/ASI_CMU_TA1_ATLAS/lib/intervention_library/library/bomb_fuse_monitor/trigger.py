# -*- coding: utf-8 -*-
"""
.. module:: bomb_fuse_monitor.trigger
   :platform: Linux, Windows, OSX
   :synopsis: Trigger for interventions to encourage players to plan for bombs
              that are about to explode

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a Trigger class designed to interventions related to bombs being
about to explode.
"""

from InterventionManager import InterventionTrigger

from MinecraftBridge.messages import MissionStageTransition, MissionStage

from .intervention import BombFuseMonitorIntervention

from shared.bomb_monitor import BombFuseMonitor

class BombFuseMonitorTrigger(InterventionTrigger):
    """
    Class that generates RemindTeamOfRemainingTimeInterventions.  Interventions
    are triggered at the start of the first field phase.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        reminder_times : List[Tuple]
            List of tuples defining when to remind players of remaining time, in
            mm:ss
        """

        InterventionTrigger.__init__(self, manager, **kwargs)

        self._fuse_times = kwargs.get('fuse_times', [5, 7, 9, 11]) 
        self._heads_up_time = kwargs.get('heads_up_time', 1)

        # Create a monitor to be used by all the interventions
        self._bomb_monitor = BombFuseMonitor(self)

        # Register the trigger to receive messages when mission stage changes
        self.add_minecraft_callback(MissionStageTransition, self.__onMissionStageTransition)


    def __onMissionStageTransition(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.MissionStageTransition
            Received MissionStageTransition message
        """

        self.logger.debug(f"{self}: Received Message {message}")

        # An intervention should only be spawned if 1) the mission stage is the
        # Shop stage, and 2) then number of times the team has transitioned to 
        # the shop is 1

        if message.mission_stage == MissionStage.FieldStage and \
           message.transitionsToShop <= 1:

            # Create an intervention for each reminder time
            for time in self._fuse_times:
                intervention = BombFuseMonitorIntervention(self.manager, self._bomb_monitor, time, self._heads_up_time)
                self.logger.debug(f'{self}:  Spawning {intervention}')
                self.manager.spawn(intervention)