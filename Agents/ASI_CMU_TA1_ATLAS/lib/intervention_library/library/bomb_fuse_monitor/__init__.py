# -*- coding: utf-8 -*-
"""
.. module:: bomb_fuse_monitor
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for monitoring bomb fuses

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to help the team monitor monitor bomb fuses.

* Trigger: The interventions are triggered at the start of the first Field Phase

* Discard States:  N/A

* Resolve State:  The intervention is immediately queued for a resolution

* Followups:  None
"""

from .intervention import BombFuseMonitorIntervention, BombFuseMonitorShopIntervention
from .trigger import BombFuseMonitorTrigger

InterventionClass = BombFuseMonitorIntervention
TriggerClass = BombFuseMonitorTrigger
IndividualComplianceFollowupClass = None
TeamComplianceFollowupClass = None

default_parameters = {}

AdditionalFollowupClasses = []