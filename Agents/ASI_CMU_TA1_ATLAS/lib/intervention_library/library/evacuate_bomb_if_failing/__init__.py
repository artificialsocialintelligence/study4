# -*- coding: utf-8 -*-
"""
.. module:: evacuate_bomb_if_failing
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for intervention to encourage 
      a player to evacuate an active bomb if player doesn't have the 
      required tools to help defuse the bomb.

.. moduleauthor:: Yu Quan Chong <yuquanc@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to encourage a player to evacuate an active bomb 
if the player doesn't have the required tools to help defuse the bomb.

* Trigger: The intervention is triggered when a player is near an active 
           bomb without the necessary tools to help defuse the bomb.

* Discard States:  N/A

* Resolve State:  The intervention is immediately queued for a resolution

* Followups:  Determine individual compliance to intervention, i.e., 
              do the player evacuate to specified safe distance away?
"""

from .intervention import EvacuateBombIfFailingIntervention
from .trigger import EvacuateBombIfFailingTrigger
from .individual_compliance import EvacuateBombIfFailingIndividualCompliance

InterventionClass = EvacuateBombIfFailingIntervention
TriggerClass = EvacuateBombIfFailingTrigger
IndividualComplianceFollowupClass = EvacuateBombIfFailingIndividualCompliance
TeamComplianceFollowupClass = None

default_parameters = {}

AdditionalFollowupClasses = []