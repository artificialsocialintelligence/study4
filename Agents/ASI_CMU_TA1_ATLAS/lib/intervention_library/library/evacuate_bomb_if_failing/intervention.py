# -*- coding: utf-8 -*-
"""
.. module:: evacuate_bomb_if_failing.intervention
   :platform: Linux, Windows, OSX
   :synopsis: Intervention used encourage a player to evacuate
      an active bomb if player doesn't have the required tools 
      to help defuse the bomb.

.. moduleauthor:: Yu Quan Chong <yuquanc@andrew.cmu.edu>

Definition of an Intervention class designed to encourage a player to evacuate
an active bomb if player doesn't have the required tools to help defuse the bomb.
"""

from InterventionManager import Intervention

class EvacuateBombIfFailingIntervention(Intervention):
   """
   An EvacuateBombIfFailingIntervention is an Intervention which monitors the
   active bombs near an agent and encourages the agent to evacuate an active
   bomb if agent doesn't have the required tools to help defuse the bomb
   """

   def __init__(self, manager, participant_id, participant_location, bomb_id, bomb_location, bomb_sequence, **kwargs):
      """
      Arguments
      ---------
      manager : InterventionManager
         Manager for this intervention
      participant_id : str
         ID of the participant being monitored
      participant_location : Tuple[float]
         Location of participant
      bomb_id : int
         ID of the bomb being defused
      bomb_location : Tuple[int]
         Location of the bomb
      bomb_sequence : List[string]
         Sequence of defusal

      Keyword Arguments
      -----------------
      safety_distance : float, default=8.0
         Safety distance to remain from active bomb
      """

      # Default values for intervention prioritization information
      kwargs["priority"] = kwargs.get("priority", 1)
      kwargs["expiration"] = kwargs.get("expiration", 10000)
      kwargs["view_duration"] = kwargs.get("view_duration", 5000)

      Intervention.__init__(self, manager, **kwargs)

      # Who is being monitored
      self._participant_id = participant_id
      self.addRelevantParticipant(self._participant_id)

      # Store relevant variables for Followup
      self._participant_id = participant_id
      self._bomb_location = bomb_location
      self._safety_distance = kwargs.get('safety_distance', 8.0)

      # Intervention message
      self._message = "Look out!  A bomb near you has triggered, and you lack the tools to defuse it."
###      self._message = f'An active bomb with bomb ID {bomb_id} with bomb sequence {bomb_sequence} is ' + \
###                      f'{round(self.__distance(bomb_location, position))} block away from you. You do not have the ' + \
###                      f'required tools to help to defuse the bomb. Please evacuate away from the bomb such that ' + \
###                      f'you are {self.safety_distance} blocks away.'


   @property
   def bomb_location(self):
     return self._bomb_location


   @property
   def participant_id(self):
     return self._participant_id


   @property
   def safety_distance(self):
     return self._safety_distance


   def __distance(self, position1, position2):
      """
      Simple function to determine the distance between two positions.

      Arguments
      ---------
      position1 : Tuple[float]
         (x,y,z) position of point 1
      position2 : Tuple[float]
         (x,y,z) position of point 2
      """

      return math.sqrt(sum(map(lambda x: (x[0]-x[1])**2, 
                              zip(position1, position2))))
   

   def _onActivated(self):
      """
      Callback when the Intervention is activated.
      Immediately queue the Intervention for resolution.
      """
      self.queueForResolution()


   def getInterventionInformation(self):
      """
      Provide a dictionary containing a information necessary to present the
      intervention.
      """
      return {     
         'default_recipients': [self._participant_id],
         'default_message': self._message,
         'default_responses': []
      }