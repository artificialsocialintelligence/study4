# -*- coding: utf-8 -*-
"""
.. module:: evacuate_bomb_if_failing.trigger
   :platform: Linux, Windows, OSX
   :synopsis: Trigger for EvacuateBombIfFailingIntervention Intervention

.. moduleauthor:: Yu Quan Chong <yuquanc@andrew.cmu.edu>

Definition of a Trigger class designed to spawn 
EvacuateBombIfFailingIntervention interventions.
"""

from InterventionManager import InterventionTrigger

from MinecraftBridge.messages import (
   CommunicationEnvironment, 
   ObjectStateChange,
   PlayerInventoryUpdate,
   PlayerState,
)

from .intervention import EvacuateBombIfFailingIntervention

class EvacuateBombIfFailingTrigger(InterventionTrigger):
   """
   Class that generates EvacuateBombIfFailingIntervention. 
   Interventions are triggered when agent is near an active bomb without
   the necessary tools to help defuse the bomb.
   """
   def __init__(self, manager, **kwargs):
      """
      Arguments
      ---------
      manager : InterventionManager
         Instance of the manager in charge of this trigger

      Keyword Arguments
      -----------------
      safety_distance : float, default=8.0
         Safety distance to remain from active bomb
      """

      InterventionTrigger.__init__(self, manager, **kwargs)
      self.kwargs = kwargs

      # Dictionary mapping discovered bomb IDs to their location 
      self.__discovered_bomb_locations = {}

      # Dictionary mapping bomb ID of active bombs to their current bomb sequence
      self.__active_bombs_sequences = {}

      # Safety distance
      self.__safety_distance = kwargs.get('safety_distance', 8.0)

      # Listen for when bombs emit their defusal sequence
      self.add_minecraft_callback(CommunicationEnvironment, self.__onCommunicationEnvironment)
      # Listen to check if any of the discovered bombs turn active
      self.add_minecraft_callback(ObjectStateChange, self.__onObjectStateChange)
      # Listen to player inventory updates
      self.add_minecraft_callback(PlayerInventoryUpdate, self.__onPlayerInventoryUpdate)
      # Listen to player states
      self.add_minecraft_callback(PlayerState, self.__onPlayerState)

   def __distance(self, position1, position2):
      """
      Simple function to determine the distance between two positions.

      Arguments
      ---------
      position1 : Tuple[float]
         (x,y,z) position of point 1
      position2 : Tuple[float]
         (x,y,z) position of point 2
      """

      return math.sqrt(sum(map(lambda x: (x[0]-x[1])**2, 
                              zip(position1, position2))))

   def __onCommunicationEnvironment(self, message):
      """
      Check the content of the communication to see if it contains 1) BOMB_ID
      and 2) SEQUENCE in its message.

      Arguments
      ---------
      message : MinecraftBridge.messages.PlanningStageEvent
         Received PlanningStageEvent message
      """

      self.logger.debug(f"{self}: Received Message {message}")

      # Parse the message, checking if it contains lines starting with BOMB_ID
      # and SEQUENCE.  If it doesn not contain both of these strings, simply
      # return
      if not "BOMB_ID:" in message.message or not "SEQUENCE:" in message.message:
         self.logger.debug(f'{self}:   Message does not contain "BOMB_ID" and/or "SEQUENCE".  Ignoring.')
         return

      message_lines = message.message.split('\n')

      bomb_id = None
      sequence = None

      for line in message_lines:
         if line.startswith("BOMB_ID:"):
             try:
                 bomb_id = line.split(':')[1].strip()
             except:
                 self.logger.warning(f'{self}: Could not extract BOMB_ID from message line: {line}')

         if line.startswith("SEQUENCE:"):
             try:
                 sequence = line.split(':')[1].strip()
                 sequence = [s.strip() for s in sequence.strip('[').strip(']').split(',')]
                 sequence = ''.join(sequence)
             except:
                 self.logger.warning(f'{self}: Could not extract SEQUENCE from message line: {line}')

      # Only proceed if a bomb_id and sequence have been extracted
      if bomb_id is None or sequence is None:
         return

      # Check if bomb with this bomb id is already tracked
      if bomb_id in self.__discovered_bomb_locations:
         self.logger.debug(f'{self}: Bomb with ID {bomb_id} already discovered.  Not tracked.')
         return

      # Track bomb
      self.logger.debug(f'{self}: Discovered bomb with ID {bomb_id} and sequence {sequence}.')
      self.__discovered_bomb_locations[bomb_id] = message.sender_position

   def __onObjectStateChange(self, message):
      """
      Callback when an object's state has changed.  
      Used to check if any discovered bomb has 1) turned active, 2) is destroyed

      Arguments
      ---------
      message : MinecraftBridge.messages.ObjectStateChange
         Received message
      """

      self.logger.debug(f'{self}:  Received Message {message}, about {message.id}')

      # Determine if the object being updated is any of the discovered bombs
      if message.id not in self.__discovered_bomb_locations:
         self.logger.debug(f'{self}:    Message is about object {message.id}.  Ignoring')
         return

      # Did the bomb explode or become defused?  Remove if so
      outcome_change = message.changed_attributes.get_attribute("outcome")

      if outcome_change is not None:
         # Check if the outcome is "DEFUSED"/"DEFUSED_DISPOSER"/"EXPLODE_CHAINED_ERROR"/"EXPLODE_TIME_LIMIT" \
         # /"EXPLODE_TOOL_MISMATCH"/"EXPLODE_FIRE"/"PERTURBATION_FIRE_TRIGGER", and remove sequence
         if outcome_change.current == "DEFUSED" or outcome_change.current == "DEFUSED_DISPOSER" or \
            outcome_change.current == "EXPLODE_CHAINED_ERROR" or outcome_change.current == "EXPLODE_TIME_LIMIT" or \
            outcome_change.current == "EXPLODE_TOOL_MISMATCH" or outcome_change.current == "EXPLODE_TOOL_MISMATCH" or \
            outcome_change.current == "EXPLODE_FIRE" or outcome_change.current == "PERTURBATION_FIRE_TRIGGER":
               if message.id in self.__active_bombs_sequences:
                  self.__active_bombs_sequences.pop(message.id)
                  return 

      # Did the bomb become / remain active? 
      active_change = message.changed_attributes.get_attribute("active")
      if active_change is not None:
         if outcome_change.current == True:
            # Obtain current active bomb sequence
            sequence_change = message.changed_attributes.get_attribute("sequence")
            sequence = sequence_change.current
            # Update active bombs
            self.__active_bombs_sequences[message.id] = sequence

   def __onPlayerInventoryUpdate(self, message):
      """
      Callback when a InventoryUpdate messasge is received. This used to update
      players' inventory

      Arguments
      ---------
      message : MinecraftBridge.messages.PlayerInventoryUpdate
         Received message
      """

      self.logger.debug(f'{self}: Received Message: {message}')

      # Update players current inventory
      self.__players_inventory = message.current_inventory

   def __onPlayerState(self, message):
      """
      Callback when a PlayerState message is received. 

      Arguments
      ---------
      message : MinecraftBridge.messages.PlayerState
         Received message
      """

      self.logger.debug(f'{self}: Received Message: {message}')

      # Get which participant the message is about and their position
      participant_id = message.participant_id
      position = message.position

      # Iterate over active bombs
      for bomb_id, bomb_sequence in self.__active_bombs_sequences.items():
         # Get location of active bomb
         bomb_location = self.__discovered_bomb_locations[bomb_id]
         # Check if less than safety distance
         if self.__distance(bomb_location, position) < self.__safety_distance:
            # Check for bomb disposer
            if self.__players_inventory[participant_id]['BOMB_DISPOSER'] > 0:
               continue
            # Boolean to check if agent has relevant color tool if it does not have bomb disposer
            has_relevant_color_tool = False
            # Iterate color in bomb_sequence
            for color in bomb_sequence:
               if color == 'R' and self.__players_inventory[participant_id]['WIRECUTTERS_RED'] > 0:
                  has_relevant_color_tool == True
                  break
               elif color == 'B' and self.__players_inventory[participant_id]['WIRECUTTERS_BLUE'] > 0:
                  has_relevant_color_tool == True
                  break
               elif color == 'G' and self.__players_inventory[participant_id]['WIRECUTTERS_GREEN'] > 0:
                  has_relevant_color_tool == True
                  break
            if has_relevant_color_tool == True:
               continue
            # Agent has no relevant tools + close to active bomb --> Spawn intervention
            intervention = EvacuateBombIfFailingIntervention(self.manager,
                                                             participant_id,
                                                             position,
                                                             bomb_id,
                                                             bomb_location,
                                                             bomb_sequence,
                                                             safety_distance=self.__safety_distance)
            self.logger.info(f'{self}:  Spawning {intervention} for active bomb with Bomb ID {bomb_id} at {str(bomb_location)} with sequence {bomb_sequence}.')
            self.manager.spawn(intervention)