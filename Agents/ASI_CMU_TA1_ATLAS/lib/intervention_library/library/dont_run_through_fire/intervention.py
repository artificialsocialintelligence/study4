# -*- coding: utf-8 -*-
"""
.. module:: fire_reach_bomb.intervention
   :platform: Linux, Windows, OSX
   :synopsis: Intervention to encourage players to not run through fire, and to
              propose an alternative.

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class designed to encourage a player 
to put out fires before they reach bombs and cause them to explode.
"""

import math

from InterventionManager import Intervention

from MinecraftBridge.messages import (
   PlayerState,
   MissionStageTransition, 
   MissionStage
#   EnvironmentRemovedSingle,
#   EnvironmentRemovedList
)

class DontRunThroughFireIntervention(Intervention):
   """
   A DontRunThroughFireIntervention is an Intervention which monitors the
   location of a player relative to fires, and alerts the player of alternative
   actions if it appears they're going to run through fire.
   """

   def __init__(self, manager, fire_monitor, participant_id, alert_time, **kwargs):
      """
      Arguments
      ---------
      manager : InterventionManager
         Manager for this intervention
      """

      # Default values for intervention prioritization information
      kwargs["priority"] = kwargs.get("priority", 1)
      kwargs["expiration"] = kwargs.get("expiration", 20000)
      kwargs["view_duration"] = kwargs.get("view_duration", 15000)
      kwargs["present_immediately"] = kwargs.get("present_immediately", True)

      Intervention.__init__(self, manager, **kwargs)

      self._alert_time = alert_time
      self._participant_id = participant_id
      self._alert_delay = kwargs.get("alert_delay", 30000)
      self._distance_threshold = kwargs.get('distance_threshold', 6)
      self._fire_monitor = fire_monitor

      # Register callbacks
      self.add_minecraft_callback(PlayerState, self.__onPlayerState)
      self.add_minecraft_callback(MissionStageTransition, self.__onStageTransition)


      # Intervention message and target participants.
      self._recipients = [self._participant_id]
      self._message = 'It looks like you are about to run through that fire.'


   def __distance(self, p0, p1):

      return math.sqrt((p0[0] - p1[0])**2 + (p0[2] - p1[2])**2)


   def __resolve_intervention(self, current_time):
      """
      """

      # The player is close enough to a fire.  Generate a message based on what
      # is in the player's inventory
      inventory = self.manager.participant_info[self._participant_id].inventory

      # Can the player put out the fire?
      if inventory["FIRE_EXTINGUISHER"] > 0:
         self._message = 'It looks like you are about to run through that fire.  I suggest putting it out to prevent hurting yourself and having it spreading.'
      elif inventory["HAZARD_BEACON"] > 0:
         self._message = 'It looks like you are about to run through that fire.  I suggest placing a hazard beacon to let your teammates know about the fire and avoiding it.'
      else:
         self._message = 'It looks like you are about to run through that fire.  I would suggest avoiding the fire, and possibly returning to the store to get supplies to handle it.'

      # Resolve the intervention and spawn the next one
      alert_time = current_time + self._alert_delay
      next_intervention = DontRunThroughFireIntervention(self.manager, self._fire_monitor,
                                                         self._participant_id, alert_time, 
                                                         alert_delay=self._alert_delay,
                                                         distance_threshold=self._distance_threshold)
      self.logger.info(f'{self}:  Spawning {next_intervention}')
      self.manager.spawn(next_intervention)

      self.queueForResolution()


   def __onPlayerState(self, message):
      """
      """

      # Don't do anything if the alert time hasent come yet
      if message.elapsed_milliseconds < self._alert_time:
         self.logger.debug(f"{self}:  Mission time {message.elapsed_milliseconds} hasn't exceeded {self._alert_time}")
         return

      # Ignore if it's not about this participant
      if message.participant_id != self._participant_id:
         self.logger.debug(f'{self}:  Not about {self._participant_id}')
         return

      # Get the fire locations that are close enough to the player
      close_fires = [f for f in self._fire_monitor._fire_locations.values() if self.__distance(f, message.position) < self._distance_threshold]
      self.logger.debug(f'{self}:  Of {len(self._fire_monitor._fire_locations)}, {len(close_fires)} are within the distance threshold.')

      # TODO:  Check that the player's moving towards the fires

      # Are there any fires remaining?
      if len(close_fires) > 0:
         self.__resolve_intervention(message.elapsed_milliseconds)


   def __onStageTransition(self, message):
      """
      """

      if message.mission_stage == MissionStage.ShopStage:

         self.logger.debug(f'{self}:  Entering Shop Stage, discarding')
         self.discard()


   def getInterventionInformation(self):
      """
      Provide a dictionary containing a information necessary to present the
      intervention.
      """
      return {     
         'default_recipients': self._recipients,
         'default_message': self._message,
         'default_responses': []
      }