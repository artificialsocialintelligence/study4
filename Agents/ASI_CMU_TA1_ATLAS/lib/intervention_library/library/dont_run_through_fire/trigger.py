# -*- coding: utf-8 -*-
"""
.. module:: dont_run_through_fire.trigger
   :platform: Linux, Windows, OSX
   :synopsis: Trigger for Intervention to not run through fire

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a Trigger class designed to spawn interventions to alert players
not to run through a fire, if they appear to do so
"""

from InterventionManager import InterventionTrigger

from MinecraftBridge.messages import MissionStageTransition, MissionStage

from shared.fire_monitor import FireMonitor
from .intervention import DontRunThroughFireIntervention

class DontRunThroughFireTrigger(InterventionTrigger):
    """
    Class that generates DontRunThroughFireInterventions.  Interventions
    are triggered at the start of the field phase.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        alert_delay : int
            Number of milliseconds before the intervention can start monitoring
        distance_threshold : float 
            Minimum distance to a fire to be considered approaching it.
        """

        InterventionTrigger.__init__(self, manager, **kwargs)

        self._fire_monitor = FireMonitor(self)

        self._alert_delay = kwargs.get('alert_delay', 30000)
        self._distance_threshold = kwargs.get('distance_threshold', 6)

        # Register the trigger to receive messages when mission stage changes
        self.add_minecraft_callback(MissionStageTransition, self.__onMissionStageTransition)


    def __onMissionStageTransition(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.MissionStageTransition
            Received MissionStageTransition message
        """

        self.logger.debug(f"{self}: Received Message {message}")

        # An intervention should be spawned at the start of a Shop stage

        if message.mission_stage == MissionStage.FieldStage:

            # When can the player be alerted?
            current_time = self.manager.mission_clock.elapsed_milliseconds
            if current_time is None:
                alert_time = self._alert_delay
            else:
                alert_time  = self.manager.mission_clock.elapsed_milliseconds

            self.logger.debug(f'{self}:  Triggering intervention to start alerting at {alert_time}, current_time is {current_time}.')

            for participant in self.manager.participants:
                intervention = DontRunThroughFireIntervention(self.manager, self._fire_monitor,
                                                              participant.id, alert_time, 
                                                              alert_delay=self._alert_delay,
                                                              distance_threshold=self._distance_threshold)
                self.logger.info(f'{self}:  Spawning {intervention}')
                self.manager.spawn(intervention)