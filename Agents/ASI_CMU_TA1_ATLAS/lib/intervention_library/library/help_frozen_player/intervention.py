# -*- coding: utf-8 -*-
"""
.. module:: help_frozen_player.intervention
   :platform: Linux, Windows, OSX
   :synopsis: Intervention used to inform team members that a team mate is
              frozen

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class designed to inform team members of a frozen
team mate.
"""

from InterventionManager import Intervention


class HelpFrozenPlayerIntervention(Intervention):
    """
    A HelpFrozenPlayerIntervention is an Intervention which simply presents a 
    message to team members when one of their team mates is frozen.
    """

    def __init__(self, manager, frozen_participant_id, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention

        frozen_participant_id : string
            ID of the participant that's frozen

        Keyword Arguments
        -----------------
        message : string
            Welcome message to greet the team with
        """
        
        # Default values for intervention prioritization information
        kwargs["priority"] = kwargs.get("priority", 1)
        kwargs["expiration"] = kwargs.get("expiration", 45000)
        kwargs["view_duration"] = kwargs.get("view_duration", 15000)
        kwargs["present_immediately"] = kwargs.get("present_immediately", True)


        Intervention.__init__(self, manager, **kwargs)

        self._frozen_participant = frozen_participant_id
        self._frozen_participant_map_section = ""
        
        self._message = kwargs.get('message', "One of your team mates is frozen, you should provide assistance if possible.")

        # Indicate that the non-frozen participants are relevant to this
        # intervention
        for participant in self.manager.participants:
            if participant.participant_id != self._frozen_participant:
                self.addRelevantParticipant(participant.participant_id)


    @property
    def frozen_participant(self):
        return self._frozen_participant

    def __location_to_map_section(self, location):
        """
        """

        row_number = max(min(int(9*location[2]/150),8),0)
        col_number = max(min(int(7*location[0]/50),6),0) + 1

        row_letter = ['A','B','C','D','E','F','G','H','I'][row_number]

        return row_letter + str(col_number)


    def _onActivated(self):
        """
        Callback when the Intervention is activated.
        Immediately queue the Intervention for resolution.
        """

        # Determine where the player is
        player_location = self.manager.participant_info[self._frozen_participant].location

        self._frozen_participant_map_section = self.__location_to_map_section(player_location)

        self._message = f"One of your team mates is frozen in section {self._frozen_participant_map_section}, you should provide assistance if possible."

        self.queueForResolution()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """

        return {     
            'default_recipients': [p.id for p in self.manager.participants],
            'default_message': self._message,
            'default_responses': ["I agree", "I disagree", "Skip"]
        }