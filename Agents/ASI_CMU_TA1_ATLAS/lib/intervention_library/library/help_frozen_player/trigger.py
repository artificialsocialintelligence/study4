# -*- coding: utf-8 -*-
"""
.. module:: team_welcome_message.trigger
   :platform: Linux, Windows, OSX
   :synopsis: Trigger for HelpFrozenPlayer Intervention

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a Trigger class designed to spawn interventions informing other
players to help if a team member is frozen.
"""

from InterventionManager import InterventionTrigger

from MinecraftBridge.messages import PlayerStateChange

from .intervention import HelpFrozenPlayerIntervention

class HelpFrozenPlayerTrigger(InterventionTrigger):
    """
    Class that generates TeamWelcomeMessageInterventions.  Interventions are 
    triggered at the start of the first shop phase.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        message : string
            Welcome message to greet the team with
        """

        InterventionTrigger.__init__(self, manager, **kwargs)
        self.kwargs = kwargs

        # Register the trigger to receive messages when mission stage changes
        self.add_minecraft_callback(PlayerStateChange, self.__onPlayerStateChange)


    def __onPlayerStateChange(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.PlayerStateChange
            Received PlayerStateChange message
        """

        self.logger.debug(f"{self}: Received Message {message}")

        # Determine if this message is due to a player being frozen
        is_frozen_attribute = message.changed_attributes.get_attribute('is_frozen')
        if is_frozen_attribute is not None and is_frozen_attribute.current == 'true':
            
            # A player has become frozen -- create and spawn an intervention
            intervention = HelpFrozenPlayerIntervention(self.manager, message.participant_id)
            self.logger.info(f'{self}:  Spawning {intervention}')
            self.manager.spawn(intervention)