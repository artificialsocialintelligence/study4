# -*- coding: utf-8 -*-
"""
.. module:: instruct_on_injuries.intervention
   :platform: Linux, Windows, OSX
   :synopsis: Intervention for providing just-in-time instructions on injuries

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class designed to provide just-in-time instruction
on injuries when first injured
"""

from InterventionManager import Intervention

import MinecraftElements
from MinecraftBridge.messages import PlayerStateChange

class InstructOnInjuriesIntervention(Intervention):
    """
    An InstructOnInjuries intervention provides a just-in-time tutorial on
    player injuries, if this is the first time a  player is playing the game.
    """

    def __init__(self, manager, participant_id, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention
        participant_id : str
            Identifier of the participant to be monitored
        """

        # Default values for intervention prioritization information
        kwargs["priority"] = kwargs.get("priority", 1)
        kwargs["expiration"] = kwargs.get("expiration", 30000)
        kwargs["view_duration"] = kwargs.get("view_duration", 15000)
        
        Intervention.__init__(self, manager, **kwargs)

        self._participant_id = participant_id
        self.addRelevantParticipant(participant_id)

        # Listen to the Minecraft bridge for FoV messages
        self.add_minecraft_callback(PlayerStateChange, self.__onPlayerStateChange)


    @property
    def participant_id(self):
        return self._participant_id


    def __onPlayerStateChange(self, message):
        """
        Callback when a PlayerStateChange message is received
        """

        self.logger.debug(f'{self}:  Recieved {message}')

        # Is the message about the target participant?
        if message.participant_id != self.participant_id:
            self.logger.debug(f'{self}:  Received message about {message.participant_id}, not {self.participant_id}')
            return

        # Is the message about a change in health?
        health = message.changed_attributes.get_attribute('health')

        if health is None:
            self.logger.debug(f"{self}:  Message doesn't contain health attribute")
            return

        if (float(health.current) - float(health.previous)) >= 0:
            self.logger.debug(f"{self}:  Health hasn't decreased")
            return

        # Health has decreased, inform the participant
        self.logger.debug(f"{self}:  Health for {message.participant_id} went from {health.previous} to {health.current}.  Resolving.")
        self.queueForResolution()


    def _onActivated(self):
        """
        Callback when the intervention is activated.  Checks to see if the
        player has played before, and discards if so.
        """

        # TODO:  Query the player database to see if the player has played
        #        before
        pass


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {     
            'default_recipients': [self.participant_id],
            'default_message': 'You recently got injured.  You lose health if a bomb goes off too close to you, or you touch fire.  Careful!  If you lose all your health, you will be frozen until a teammate rescues you.',
            'default_responses': []
        }