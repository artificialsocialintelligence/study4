# -*- coding: utf-8 -*-
"""
.. module:: not_following_plan
   :platform: Linux, Windows, OSX
   :synopsis: Intervention components for when a player is not following their plan

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to inform players when they deviate from their agreed-
upon plan.

* Trigger: The intervention is triggered at the start of a Field phase

* Discard States:  Start of a Shop phase

* Resolve State:  The intervention is resolved when a plan deviation message is
                  received

* Followups:  Determine individual compliance to intervention, i.e., does the
              player begin to follow their plan again?
"""

from .intervention import NotFollowingPlanIntervention
from .trigger import NotFollowingPlanTrigger
#from .individual_compliance import NotFollowingPlanIndividualCompliance

InterventionClass = NotFollowingPlanIntervention
TriggerClass = NotFollowingPlanTrigger
IndividualComplianceFollowupClass = None #NotFollowingPlanIndividualCompliance
TeamComplianceFollowupClass = None

default_parameters = {}

AdditionalFollowupClasses = []