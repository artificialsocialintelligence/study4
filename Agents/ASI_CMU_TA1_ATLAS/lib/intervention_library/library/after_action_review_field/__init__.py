# -*- coding: utf-8 -*-
"""
.. module:: after_action_review_field
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for providing after-action review for
              each player after each field phase

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to provide after-action review after each field phase

* Trigger: The intervention is triggered at the start of a Field phase

* Discard States:  N/A

* Resolve State:  The intervention is resolved at the start of the Shop phase

* Followups:  None
"""

from .intervention import AfterActionReviewFieldIntervention
from .trigger import AfterActionReviewFieldTrigger

InterventionClass = AfterActionReviewFieldIntervention
TriggerClass = AfterActionReviewFieldTrigger
IndividualComplianceFollowupClass = None
TeamComplianceFollowupClass = None

default_parameters = {}

AdditionalFollowupClasses = []