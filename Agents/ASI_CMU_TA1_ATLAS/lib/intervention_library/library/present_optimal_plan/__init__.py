# -*- coding: utf-8 -*-
"""
.. module:: present_optimal_plan
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for suggesting the optimal plan message

.. moduleauthor:: Simon Stepputtis <sstepput@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to present the optimal plan to players

* Trigger: The intervention is triggered whenever players enter the store

* Discard States:  N/A

* Resolve State:  The intervention is immediately queued for a resolution

* Followups:  None
"""

from .intervention import PresentOptimalPlanIntervention
from .trigger import PresentOptimalPlanTrigger

InterventionClass = PresentOptimalPlanIntervention
TriggerClass = PresentOptimalPlanTrigger
IndividualComplianceFollowupClass = None
TeamComplianceFollowupClass = None

default_parameters = {}

AdditionalFollowupClasses = []