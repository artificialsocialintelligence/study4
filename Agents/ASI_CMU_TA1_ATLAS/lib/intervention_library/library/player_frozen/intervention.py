# -*- coding: utf-8 -*-
"""
.. module:: player_frozen.intervention
   :platform: Linux, Windows, OSX
   :synopsis: Intervention used to help players avoid getting
        frozen again in the future.

.. moduleauthor:: Ini Oguntola <ioguntol@andrew.cmu.edu>

Definition of an Intervention class designed to help players avoid getting
frozen again in the future.
"""

import math

from InterventionManager import Intervention


class PlayerFrozenIntervention(Intervention):
    """
    A PlayerFrozenIntervention is an Intervention which simply presents a 
    message to team members when one of their team mates is frozen.
    """

    def __init__(self, manager, frozen_participant_id, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention

        frozen_participant_id : string
            ID of the participant that's frozen

        Keyword Arguments
        -----------------
        message : string
            Welcome message to greet the team with
        """

        # Default values for intervention prioritization information
        kwargs["priority"] = kwargs.get("priority", 1)
        kwargs["expiration"] = kwargs.get("expiration", 20000)
        kwargs["view_duration"] = kwargs.get("view_duration", 15000)

        Intervention.__init__(self, manager, **kwargs)
        self.addRelevantParticipant(frozen_participant_id)
        self._frozen_participant = frozen_participant_id

        # Conditions associated with resolution
        self._has_hazard_beacons = False
        self._nearby_teammates = []

        # TODO: find out reason bomb exploded
        self._teammate_distance_threshold = kwargs.get('_teammate_distance_threshold', 10.0)
        self._message = kwargs.get('message', "try not to get frozen")

    @property
    def frozen_participant(self):
        return self._frozen_participant


    def __distance(self, p0, p1):

        return math.sqrt((p0[0] - p1[0])**2 + (p0[2] - p1[2])**2)


    def _onActivated(self):
        """
        Callback when the Intervention is activated.
        Immediately queue the Intervention for resolution.
        """

        # Construct a useful message
        message = "You were frozen because your health dropped to zero.  Be more careful around fire and defusing bombs in the future."

        # Calculate conditions this intervention was presented in
        if self.manager.participant_info[self.frozen_participant].inventory['HAZARD_BEACON'] > 0:
            self._has_hazard_beacons = True

        for participant in self.manager.participants:
            if participant.id != self.frozen_participant:
                distance = self.__distance(self.manager.participant_info[participant.id].location, 
                                           self.manager.participant_info[self.frozen_participant].location)
                if distance < self._teammate_distance_threshold:
                    self._nearby_teammates.append(participant.id)

        self.logger.debug(f'{self}:  has_hazard_beacons = {self._has_hazard_beacons}')
        self.logger.debug(f'{self}:  nearby_teammates: {self._nearby_teammates}')

        # Does the player have nearby teammates or markers to alert their teammates?
        if len(self._nearby_teammates) > 0:
            message += "  You have a teammate nearby, they will likely attend to you."
        elif self._has_hazard_beacons:
            message += "  You should place a hazard beacon to inform your teammates that you need help."
        else:
            message += "  Also, you have no hazard beacons!  You should make sure to carry some in the field."

        self._message = message

        self.logger.debug(f'{self}:  Final Intervention Message: {self._message}')

        self.queueForResolution()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """

        if self._has_hazard_beacons:
            responses = ["I agree", "I disagree", "Skip"]
        else:
            responses = []


        return {     
            'default_recipients': [self.frozen_participant],
            'default_message': self._message,
            'default_responses': responses
        }