# -*- coding: utf-8 -*-
"""
.. module:: wrong_defusal_tool_used.trigger
   :platform: Linux, Windows, OSX
   :synopsis: Trigger for WrongDefusalToolUsed Intervention

.. moduleauthor:: Ini Oguntola <ioguntol@andrew.cmu.edu>

Definition of a Trigger class designed to spawn interventions to help
players use the right tool in the future.
"""

from InterventionManager import InterventionTrigger

from MinecraftBridge.messages import ObjectStateChange

from .intervention import WrongDefusalToolUsedIntervention

class WrongDefusalToolUsedTrigger(InterventionTrigger):
    """
    Class that generates WrongDefusalToolUsedIntervention instances.
    Interventions are triggered when a player becomes frozen.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        message : string
            Welcome message to greet the team with
        """

        InterventionTrigger.__init__(self, manager, **kwargs)
        self.kwargs = kwargs

        # Register the trigger to receive messages when mission stage changes
        self.add_minecraft_callback(ObjectStateChange, self.__onObjectStateChange)


    def __onObjectStateChange(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.ObjectStateChange
            Received ObjectStateChange message
        """

        self.logger.debug(f"{self}: Received Message {message}")

        ### Ignore if a player didn't cause the state change --Dana
        if message.triggering_entity == "SERVER":
            return

        # Determine if this message is due to a tool mismatch
###     Minor fix to work with trial 42 data -- Dana
###        bomb_state = message.current_attributes.get_attribute('bomb_state')
###        if bomb_state['outcome'] == 'EXPLODE_TOOL_MISMATCH':
        if message.current_attributes.get_attribute('outcome') == 'EXPLODE_TOOL_MISMATCH':
            # Create and spawn an intervention
###            intervention = WrongDefusalToolUsedIntervention(
###                self.manager, message.participant_id)
            intervention = WrongDefusalToolUsedIntervention(
                self.manager, message.triggering_entity)            
            self.logger.info(f'{self}:  Spawning {intervention}')
            self.manager.spawn(intervention)
