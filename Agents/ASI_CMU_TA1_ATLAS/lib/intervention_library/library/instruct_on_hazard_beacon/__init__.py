# -*- coding: utf-8 -*-
"""
.. module:: instruct_on_hazard_beacon
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for providing tutorial on the use of
              hazard beacons

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to provide just-in-time tutorial on using hazard beacons

* Trigger: The intervention is triggered at the start of the Recon phase

* Discard States:  The intervention is discarded if the player has played 
                   before, or upon seeing a fire while not owning a fire
                   extinguisher

* Resolve State:  The intervention is resolved when a fire block is in the FoV

* Followups:  None
"""

from .intervention import InstructOnHazardBeaconsIntervention
from .trigger import InstructOnHazardBeaconsTrigger

InterventionClass = InstructOnHazardBeaconsIntervention
TriggerClass = InstructOnHazardBeaconsTrigger
IndividualComplianceFollowupClass = None
TeamComplianceFollowupClass = None

default_parameters = {}

AdditionalFollowupClasses = []