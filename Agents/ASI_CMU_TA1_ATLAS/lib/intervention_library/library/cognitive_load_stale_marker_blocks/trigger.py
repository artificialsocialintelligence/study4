# -*- coding: utf-8 -*-
"""
.. module:: cogntive_load_stale_marker_blocks.trigger
   :platform: Linux, Windows, OSX
   :synopsis: CognitiveLoadStaleMarkerBlocksInterventions

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a Trigger class designed to spawn interventions to suggest to the
team to assign someone to attend to stale hazard beacons
"""

from InterventionManager import InterventionTrigger

import MinecraftElements

from MinecraftBridge.messages import (
    MissionStageTransition, 
    MissionStage,
)

from shared.hazard_monitor import HazardBeaconMonitor

from .intervention import CognitiveLoadStaleMarkerBlocksIntervention


class CognitiveLoadStaleMarkerBlocksTrigger(InterventionTrigger):
    """
    Class that generates CognitiveLoadStaleMarkerBlocksInterventions.
    Interventions are triggered at the start of the first field phase.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger
        """

        InterventionTrigger.__init__(self, manager, **kwargs)

        self._minimum_cognitive_load = kwargs.get("minimum_cognitive_load", 2.0)
        self._minimium_stale_beacons = kwargs.get("minimum_stale_beacons", 10)
        self._stale_beacon_time = kwargs.get("stale_beacon_time", 40000)

        # Create a hazard beacon monitor
        self._hazard_beacon_monitor = HazardBeaconMonitor(self)

        # Register the trigger to receive messages when mission stage changes
        self.add_minecraft_callback(MissionStageTransition, self.__onMissionStageTransition)


    def __onMissionStageTransition(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.MissionStageTransition
            Received MissionStageTransition message
        """

        self.logger.debug(f"{self}: Received Message {message}")

        # An intervention should be spawned at the start of a Shop stage
        if message.mission_stage == MissionStage.FieldStage:
            # Spawn an intervention
            intervention = CognitiveLoadStaleMarkerBlocksIntervention(self.manager, 
                                                                      self._hazard_beacon_monitor,
                                                                      minimum_cognitive_load=self._minimum_cognitive_load,
                                                                      minimum_stale_beacons=self._minimium_stale_beacons,
                                                                      stale_beacon_time=self._stale_beacon_time)
            self.manager.spawn(intervention)

