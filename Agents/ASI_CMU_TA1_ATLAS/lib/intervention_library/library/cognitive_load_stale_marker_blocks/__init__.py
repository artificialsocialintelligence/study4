# -*- coding: utf-8 -*-
"""
.. module:: cognitive_load_stale_marker_blocks
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger suggesting team to assign someone to
              clean up stale marker blocks

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to suggest to the team that someone clean up stale
hazard beacons.

* Trigger: The intervention is triggered at the start of the first Field phase

* Discard States:  NA

* Resolve State:  The intervention is resolved if these conditions are met:
    1.  The Cognitive Load AC is reporting a value over N
    2.  There are at least M hazard blocks that are at least T old

* Followups:  None
"""

from .intervention import CognitiveLoadStaleMarkerBlocksIntervention
from .trigger import CognitiveLoadStaleMarkerBlocksTrigger

InterventionClass = CognitiveLoadStaleMarkerBlocksIntervention
TriggerClass = CognitiveLoadStaleMarkerBlocksTrigger
IndividualComplianceFollowupClass = None
TeamComplianceFollowupClass = None

default_parameters = {}

AdditionalFollowupClasses = []