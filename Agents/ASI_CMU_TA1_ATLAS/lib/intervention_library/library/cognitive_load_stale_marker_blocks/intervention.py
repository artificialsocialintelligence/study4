# -*- coding: utf-8 -*-
"""
.. module:: cogntive_load_stale_marker_blocks.intervention
   :platform: Linux, Windows, OSX
   :synopsis: Intervention for suggesting to be clean up stale hazard beacons

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class designed to suggest to the team that 
someone remove stale hazard beacons
"""

from InterventionManager import Intervention

import MinecraftElements

from MinecraftBridge.messages import (
    MissionStageTransition, 
    MissionStage,
    IHMC_CognitiveLoad
)

class CognitiveLoadStaleMarkerBlocksIntervention(Intervention):
    """
    A CognitiveLoadStaleMarkerBlocksIntervention monitors the Cognitive Load AC
    and 
    """

    def __init__(self, manager, hazard_beacon_monitor, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention
        hazard_beacon_monitor : HazardBeaconMonitor
            A component that monitors when existing hazard beacons were placed
        """

        # Default values for intervention prioritization information
        kwargs["priority"] = kwargs.get("priority", 1)
        kwargs["expiration"] = kwargs.get("expiration", 60000)
        kwargs["view_duration"] = kwargs.get("view_duration", 15000)        

        Intervention.__init__(self, manager, **kwargs)

        self._hazard_beacon_monitor = hazard_beacon_monitor

        # Parameters to use
        self._minimum_cognitive_load = kwargs.get("minimum_cognitive_load", 2.0)
        self._minimum_stale_beacons = kwargs.get("minimum_stale_beacons", 10)
        self._stale_beacon_time = kwargs.get("stale_beacon_time", 40000)


        # Listen to the Minecraft bridge for when a hazard beacon is created
        self.add_minecraft_callback(IHMC_CognitiveLoad, self.__onCognitiveLoad)


    def __onMissionStageTransition(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.MissionStageTransition
            Received MissionStageTransition message
        """

        self.logger.debug(f"{self}: Received Message {message}")

        # An intervention should be spawned at the start of a Shop stage
        if message.mission_stage == MissionStage.ShopStage:
            self.discard()



    def __onCognitiveLoad(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.IHMC_CognitiveLoad
            Received IHMC_CognitiveLoad message
        """

        self.logger.debug(f"{self}: Received Message {message}")

        # Is the cognitive load high enough to check?
        if message.cognitive_load["Team"].value < self._minimum_cognitive_load:
            self.logger.debug(f'{self}:  Cognitive Load value of {message.cognitive_load["Team"].value} below threshold of {self._minimum_cognitive_load}')
            return

        # Is there enough hazard beacons that are considered stale?
        current_time = self.manager.mission_clock.elapsed_milliseconds

        hazard_beacon_count = 0
 
        for beacon_time in self._hazard_beacon_monitor.hazard_beacons.values():
            if current_time - beacon_time > self._stale_beacon_time:
                hazard_beacon_count += 1

        if hazard_beacon_count > self._minimum_stale_beacons:
            self.queueForResolution()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """

        return {     
            'default_recipients': [p.id for p in self.manager.participants],
            'default_message': f"There are quite a few stale hazard beacons confusing the team.  I would suggest removing excess hazard beacons.",
            'default_responses': ["I agree", "I disagree", "Skip"]
        }