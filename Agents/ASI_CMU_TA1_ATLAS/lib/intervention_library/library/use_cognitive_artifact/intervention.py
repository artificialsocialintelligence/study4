# -*- coding: utf-8 -*-
"""
.. module:: use_cognitive_artifact.intervention
   :platform: Linux, Windows, OSX
   :synopsis: Intervention used to tell players to populate the cognitive artifact

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class designed to inform players to populate the
cognitive artifact with their intended plan, if they seem to not be doing so.
"""

import collections

from InterventionManager import Intervention

from MinecraftBridge.messages import (
    UI_Click,
    PlayerState, 
    CommunicationEnvironment, 
    MissionStageTransition, 
    MissionStage
)


class UseCognitiveArtifactIntervention(Intervention):
    """
    A UseCogntiveArtifactIntervention is an Intervention which monitors the 
    team's use of the cognitive artifact to generate a plan, and alerts 
    participants if they fail to place sufficient number of icons on the 
    artifact.
    """

    def __init__(self, manager, participant_id, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention
        participant_id : str
            ID of the participant being monitored

        Keyword Arguments
        -----------------
        minimum_number_of_icons : int, default=2
            Minimum number of icons that are expected to be placed by a player
        minimum_percentage : float, default=0.1
            Minimum percentage of icons that is expected to be playced by a 
            player
        """

        # Default values for intervention prioritization information
        kwargs["priority"] = kwargs.get("priority", 1)
        kwargs["expiration"] = kwargs.get("expiration", 120000)
        kwargs["view_duration"] = kwargs.get("view_duration", 15000)
        
        Intervention.__init__(self, manager, **kwargs)

        # Who is being monitored
        self._participant_id = participant_id
        self.addRelevantParticipant(self._participant_id)

        # Keep track if the participant is ready to leave
        self._participant_ready_to_leave = False

        self._minimum_number_of_icons = kwargs.get('minimum_number_of_icons', 2)
        self._minimum_percentage = kwargs.get('minimum_percentage', 0.1)

        self._message = "You should add some flags to the client map on the left to indicate your intended plan."

        self.__participant_icon_counts = collections.defaultdict(lambda: 0)

        self.add_minecraft_callback(UI_Click, self.__onClick)
        self.add_minecraft_callback(MissionStageTransition, self.__onMissionStageTransition)


    def __onMissionStageTransition(self, message):
        """
        Callback when the mission stage transition occurs.  Used to discard the
        intervention when transitioning into the field.  Note that this should
        never be called.
        """

        if message.mission_stage == MissionStage.FieldStage:
            self.logger.info(f'{self}:  Transitioning to Field Stage, discarding intervention')
            self.discard()


    def __check_participant_did_plan(self):
        """
        Determine if the participant planned sufficiently, and if not, queue
        the intervention for resolution
        """

        # What's the total number of icons placed?
        total_icons = sum(self.__participant_icon_counts.values())

        # Did the participant place a sufficient number of icons?
        if self.__participant_icon_counts[self._participant_id] < self._minimum_number_of_icons:
            self.queueForResolution()
            return

        # What about the percentages?
        percentage = self.__participant_icon_counts[self._participant_id] / max(total_icons, 1)

        if percentage < self._minimum_percentage:
            self.queueForResolution()
            return


    def __onClick(self, message):
        """
        Callback when a UI_Click message is received.  Used to determine 1) if
        a player votes to leave the shop, or 2) if icons / flags are placed /
        removed from the cognitive artifact.

        Arguments
        ---------
        message : MinecraftBridge.message.UI_Click
        """

        self.logger.debug(f'{self}:  Recieved message {message}')

        # Get the meta-action, if it exists, and determine what to do based on that
        meta_action = message.additional_info.get("meta_action", None)

        if meta_action == "VOTE_LEAVE_STORE":
            # Did the participant vote to leave?
            if message.participant_id == self._participant_id:
                self._participant_ready_to_leave = not self._participant_ready_to_leave

                # Determine if the intervention should be resolved
                self.__check_participant_did_plan()

            return

        elif meta_action in ["PLANNING_FLAG_PLACED", "PLANNING_BOMB_PLACED"]:
            # Whoever placed this icon, increment their count
            self.__participant_icon_counts[message.participant_id] += 1

        elif meta_action in ["UNDO_PLANNING_FLAG_PLACED", "UNDO_PLANNING_BOMB_PLACED"]:
            # Whoever placed this icod, decrement their count
            self.__participant_icon_counts[message.participant_id] -= 1
            if self.__participant_icon_counts[message.participant_id] < 0:
                self.logger.warning(f'{self}:  Participant {message.participant_id} icon count negative')
                self.__participant_icon_counts[message.participant_id] = 0

        else:
            # Nothing to do
            return


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {     
            'default_recipients': [self._participant_id],
            'default_message': self._message,
            'default_responses': ["I agree", "I disagree", "Skip"]
        }