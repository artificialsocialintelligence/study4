# -*- coding: utf-8 -*-
"""
.. module:: use_cognitive_artifact
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for telling players to use their minimap

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to inform players to plan using their cognitive artifact
if they do not appear to be doing so.

* Trigger: The intervention is triggered when entering the Shop state

* Discard States:  Team enters Field stage and player has placed a minimum
                   number of icons _and_ the same percentage as others

* Resolve State:  Player votes to leave the shop and has not placed a minimum
                  number of icons, _or_ is below a certain count in icons

* Followups:  Determine individual and team compliance to intervention
"""

from .intervention import UseCognitiveArtifactIntervention
from .trigger import UseCognitiveArtifactTrigger
from .individual_compliance import UseCognitiveArtifactIndividualCompliance

InterventionClass = UseCognitiveArtifactIntervention
TriggerClass = UseCognitiveArtifactTrigger
IndividualComplianceFollowupClass = UseCognitiveArtifactIndividualCompliance
TeamComplianceFollowupClass = None

default_parameters = {}

AdditionalFollowupClasses = []