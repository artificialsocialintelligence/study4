import threading
import time
import copy
import heapq
import itertools
import numpy as np

from gym_dragon.bridge import DragonBridgeEnv

from .cbs import CBSSolver

class ThreadingWrapper(threading.Thread):
	"""
	A wrapper that allows planning to be performed on a separate thread.
	"""

	def __init__(self, env:DragonBridgeEnv, solution_timeout: float, *args, **kwargs):
		"""
		"""

		threading.Thread.__init__(self, **kwargs)

		# Store the passed environment and timeout
		self._env = env
		self._solution_timeout = solution_timeout

		# CBSSolver
		self._solver = CBSSolver(*args, **kwargs)

		# Flag for indicating that the thread should run or be stopped
		self._running = False

		self.solution = None


	def reset(self, pre_compute_heuristics=False):

		self._env.reset(env=self._env, seed=self._solver.random_seed)

		if pre_compute_heuristics:
			self._env.pre_compute_heuristics(
				seed=self._solver.random_seed, additional_node_id_list=[])


	def run(self):
		"""
		Start generating a plan
		"""

		# This was by and large copied from CBSSolver.  At the beginning of
		# each iteration, the thread will check if it is still running, and
		# return if not.  The solution node from the solver is stored in the
		# solution attribute


		self._running = True

		while self._running:

			if self._solver.verbose:
				print(f'Start {self._solver.algo}')
			# List to store compute time and optimality ratio
			compute_time_list = []
			optimality_ratio_list = []
			# Start time
			self._solver.start_time = time.time()
			# Solution node with the relevant variables
			self._solver.solution_node = {'cost': 0,
								  'epsilon': self._solver.epsilon,
								  'individual_cost': None,
								  'max_score': self._solver.max_score,
								  'last_time': 0,
								  'last_timestep': self._solver.start_timestep,
								  'paths': None,
								  'score': 0,
								  'task_allocation': None}

			self.solution = self._solver.solution_node  

			# Iterate over subtasks
			for i, sub_team_bomb_tup_list in enumerate(self._solver.team_bomb_tup_list):

				# If the thread is no longer running, then return whatever solution exists
				if not self._running:
					self.solution = self._solver.solution_node
					return

				# Try to obtain solution for subtask
				try:
					sub_solution_node = \
						self._solver.find_sub_solution(self._solver.start_time,
											   self._solution_timeout,
											   sub_team_bomb_tup_list,
											   self._solver.solution_node['last_timestep'],
											   self._solver.solution_node['score'],
											   self._solver.solution_node['paths'],
											   self._solver.solution_node['task_allocation'],
											   sub_bomb_dependency_order_list=self._solver.team_bomb_dependency_order_list[i] if \
																			  self._solver.include_chained_bombs else None,
											   sub_bomb_fuse_dict=self._solver.team_bomb_fuse_dict_list[i] if \
																  self._solver.include_fuse_bombs else None
											  )
				except Exception as e:
					# Compute time
					compute_time_list.append(time.time() - self._solver.start_time)
					# Optimality ratio
					optimality_ratio_list.append(self._solver.solution_node['score'] / self._solver.max_score)
					# Print metric/results if desired and get compute time
					if self._solver.verbose:
						print(f'Exception raised during find_sub_solution(): {str(e)}')
					# Solution equal/above desired bound
					if self._solver.solution_node['score'] >= self._solver.epsilon * self._solver.max_score:
						# Return whatever solution that is available given exception
						self.solution = self._solver.solution_node
						return ### self._solver.solution_node, True, compute_time_list, optimality_ratio_list, e
					else:
						# Return whatever solution that is available given exception
						self.solution = self._solver.solution_node
						return ### self._solver.solution_node, False, compute_time_list, optimality_ratio_list, e
			   
				# Add fillers to ensure that paths are of end at the same timestep
				for agent_id in sub_solution_node['paths']:
					# First sub solution node
					if self._solver.solution_node['last_timestep'] == self._solver.start_timestep:
						while len(sub_solution_node['paths'][agent_id]) - 1 < \
							sub_solution_node['last_timestep'] - self._solver.solution_node['last_timestep']:
							sub_solution_node['paths'][agent_id]\
								.append({'loc': sub_solution_node['paths'][agent_id][-1]['loc'],
										 'loc_centroid': sub_solution_node['paths'][agent_id][-1]['loc_centroid'], 
										 'action': 'go_to',
										 'bomb_id': None, 
										 'bomb_countdown_length': None,
										 'bomb_sequence': None,
										 'tool_color': None
										})
					# Remaining sub solution nodes
					else:
						while len(sub_solution_node['paths'][agent_id]) < \
							sub_solution_node['last_timestep'] - self._solver.solution_node['last_timestep']:
							sub_solution_node['paths'][agent_id]\
								.append({'loc': sub_solution_node['paths'][agent_id][-1]['loc'],
										 'loc_centroid': sub_solution_node['paths'][agent_id][-1]['loc_centroid'], 
										 'action': 'go_to',
										 'bomb_id': None, 
										 'bomb_countdown_length': None,
										 'bomb_sequence': None,
										 'tool_color': None
										})
			   
				# Update solution node
				self._solver.solution_node['cost'] += sub_solution_node['cost']
				self._solver.solution_node['last_time'] = sub_solution_node['last_timestep'] * self._solver.env.seconds_per_timestep
				self._solver.solution_node['last_timestep'] = sub_solution_node['last_timestep']
				self._solver.solution_node['score'] = sub_solution_node['score']
				if i == 0:
					self._solver.solution_node['individual_cost'] = sub_solution_node['individual_cost'].copy()
					self._solver.solution_node['paths'] = copy.deepcopy(sub_solution_node['paths'])
					self._solver.solution_node['task_allocation'] = copy.deepcopy(sub_solution_node['task_allocation'])
				else:
					for agent_id in self._solver.env.agents:
						self._solver.solution_node['individual_cost'][agent_id] += \
							sub_solution_node['individual_cost'][agent_id]
						self._solver.solution_node['paths'][agent_id] += sub_solution_node['paths'][agent_id]
						self._solver.solution_node['task_allocation'][agent_id] += \
							sub_solution_node['task_allocation'][agent_id]
			   
				# Print metrics if desired
				if self._solver.verbose:
					print(f'\n Solution for subtask {i} found! \n')
					self._solver.print_results(True)
	
				# Evaluate current solution node
				_, _, _, env_done, _, _, _ = \
					self._solver.evaluate_paths(bomb_id_tup_dict=copy.deepcopy(self._solver.solution_node['task_allocation']),
										paths=self._solver.solution_node['paths'])
	
				# If environment has terminated already given current paths and not last iteration
				if env_done == True and i < len(self._solver.team_bomb_tup_list) - 1:
					# Solution equal/above desired bound
					if self._solver.solution_node['score'] >= self._solver.epsilon * self._solver.max_score:
						# Print metric/results if desired and get compute time
						if self._solver.verbose:
							print("\n Found a solution above desired score bound! \n")
							compute_time = self._solver.print_results(True)
							compute_time_list.append(compute_time)
						else:
							# Compute time
							compute_time_list.append(time.time() - self._solver.start_time)
						# Optimality ratio
						optimality_ratio_list.append(self._solver.solution_node['score'] / self._solver.max_score)

						self.solution = self._solver.solution_node
						return ### self._solver.solution_node, True, compute_time_list, optimality_ratio_list, None
					# Solution below desired bound
					else:
						# Print metric/results if desired and get compute time
						if self._solver.verbose:
							print("\n Solution below desired score bound found \n")
							compute_time = self._solver.print_results(True)
							compute_time_list.append(compute_time)
						else:
							# Compute time
							compute_time_list.append(time.time() - self._solver.start_time)
						# Optimality ratio
						optimality_ratio_list.append(self._solver.solution_node['score'] / self._solver.max_score)

						self.solution = self._solver.solution_node
						return ### self._solver.solution_node, False, compute_time_list, optimality_ratio_list, None
				else:
					# Compute time
					compute_time_list.append(time.time() - self._solver.start_time)
					# Optimality ratio
					optimality_ratio_list.append(self._solver.solution_node['score'] / self._solver.max_score)
	
			# Evaluate final solution node
			absolute_temporal_range_conflicts, inter_goal_temporal_conflicts, precedence_conflicts, env_done, path_done, \
			_, score = self._solver.evaluate_paths(bomb_id_tup_dict=copy.deepcopy(self._solver.solution_node['task_allocation']), 
										   paths=self._solver.solution_node['paths'])
			assert len(absolute_temporal_range_conflicts) == 0, 'There must be zero absolute temporal range conflicts'
			assert len(inter_goal_temporal_conflicts) == 0, 'There must be zero inter-goal temporal conflicts'
			assert len(precedence_conflicts) == 0, 'There must be zero precedence conflicts'
			assert env_done == True, 'Environment must terminate'
			assert path_done == True, 'All agents paths must terminate'
			assert score == self._solver.solution_node['score'], \
				f"Evaluated score of {score} is not equal to solution node score of {self._solver.solution_node['score']}"
			for agent_id in self._solver.solution_node['paths']:
				assert len(self._solver.solution_node['paths'][agent_id]) - 1 + self._solver.start_timestep \
					== self._solver.solution_node['last_timestep'], \
					f"Path of agent {agent_id} has timestep of {len(self._solver.solution_node['paths'][agent_id]) - 1}, " + \
					f"which is not equal to the last timestep from environment of {self._solver.solution_node['last_timestep']}"
			if self.include_collisions:
				collisions = detect_collisions(self._solver.solution_node['paths'])
				assert len(collisions) == 0, 'There must be zero collisions'
	
			# Solution equal/above desired bound
			if self._solver.solution_node['score'] >= self._solver.epsilon * self._solver.max_score:
				# Print metric/results if desired and get compute time
				if self._solver.verbose:
					print("\n Found a solution above desired score bound! \n")
					compute_time = self._solver.print_results(True)
					compute_time_list.append(compute_time)
				else:
					# Compute time
					compute_time_list.append(time.time() - self._solver.start_time)
				# Optimality ratio
				optimality_ratio_list.append(self._solver.solution_node['score'] / self._solver.max_score)

				self.solution = self._solver.solution_node
				return ### self._solver.solution_node, True, compute_time_list, optimality_ratio_list, None
			# Solution below desired bound
			else:
				# Print metric/results if desired and get compute time
				if self._solver.verbose:
					print("\n Solution below desired score bound found \n")
					compute_time = self._solver.print_results(True)
					compute_time_list.append(compute_time)
				else:
					# Compute time
					compute_time_list.append(time.time() - self._solver.start_time)
				# Optimality ratio
				optimality_ratio_list.append(self._solver.solution_node['score'] / self._solver.max_score)

				self.solution = self._solver.solution_node
				return ### self._solver.solution_node, False, compute_time_list, optimality_ratio_list, None


	def stop(self):
		"""
		Stop the plan from generating
		"""

		self._running = False




