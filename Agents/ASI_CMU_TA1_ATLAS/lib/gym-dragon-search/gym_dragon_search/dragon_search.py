import copy
import heapq
import math
import numpy as np
import random

from collections import defaultdict
from gym_dragon.core import Bomb, Color, Node, Region, Tool
from gym_dragon.dragon import DragonBaseEnv
from gym_dragon.wrappers import DummyObs
from typing import Callable, Union, Optional
from .utils import (
    build_collision_constraint_dict,
    build_return_constraint_dict,
    build_temporal_constraint_dict, 
    compare_nodes,
    compute_distance,
    get_multi_label_a_star_h_value,
    get_path_and_cost,
    is_collision_constrained,
    is_return_constrained,
    is_temporal_constrained,
    is_solvable_temporal_constraint,
    pop_multi_label_a_star_node,
    push_multi_label_a_star_node,
    pop_multi_step_a_star_node,
    push_multi_step_a_star_node
)

class DragonBaseEnvSearch(DragonBaseEnv):
    """
    Wrapper over DragonBaseEnv for CBS

    **Task**

    An atomic task is defined as the utilisation of a specified color tool on a bomb with specified bomb ID.

    Examples
    --------
    Example 1: 
        Agent ID: 'alpha'
        Tools: 'red', 'green'
        Bomb Tuple/Task: (0, 'red')
        Bomb Sequence: ('red', 'green', 'blue')
        Execution: Agent 'alpha' travels to node with bomb id 0, executes defusal step using red tool 
    Example 2: 
        Agent ID: 'alpha'
        Tools: 'red', 'blue'
        Bomb Tuple/Task: (0, 'blue')
        Bomb Sequence: ('green', 'blue')
        Execution: Agent 'alpha' travels to node with bomb id 0, executes defusal step using blue tool. 
                   Bomb explodes as green sequence needs to be defused first. 

    **Termination Conditions**

    An episode terminates based on the following conditions:

        1) All bombs are defused (default in DragonBaseEnv)
        2) Mission time has elapsed (default in DragonBaseEnv)
        3) A bomb has exploded (NEW)
        4) Agents do not have the necessary tools to defuse a bomb sequence (NEW). Should not occur in theory given 
           infinite budget.

    Conditions 3 and 4 are used to make search for optimal solution efficient.
    Condition 3 can be optionally toggled off in order to search for sub-optimal solutions
    """

    def __init__(self, 
                 mission_length: float = 10*60,
                 recon_phase_length: float = 2*60, 
                 seconds_per_timestep: float = 2.0, 
                 valid_regions: set[Region] = set(Region),
                 same_start_loc: bool = True,
                 include_collisions: bool = False,
                 include_chained_bombs: bool = True,
                 include_fire_bombs: bool = False,
                 include_fuse_bombs: bool = True,
                 bomb_explode_terminate : bool = True
        ):
        """
        Parameters
        ----------
        mission_length : float, default=10*60
            Mission length, in seconds
        recon_phase_length : float, default=2*60
            Length of reconnaissance phase, in seconds
        seconds_per_timestep : float, default=2.0
            Number of seconds per timestep in the environment
        valid_regions : set[Region], optional
            Set of regions to include in the environment
        same_start_loc : bool, default=True
            Whether for all agents to start at the same location
            NOTE: Default DragonBaseEnv starts at same location/node
        include_collisions : bool, default=False
            Whether to consider collisions for environment
            NOTE: Default DragonBaseEnv has no collisions
        include_chained_bombs : bool, default=True
            Whether to include chained bombs in the environment
        include_fire_bombs : bool, default=False
            Whether to include fire bombs in the environment
        include_fuse_bombs : bool, default=True
            Whether to include fuse bombs in the environment
        bomb_explode_terminate : bool, default=True
            Whether to terminate environment when bomb explodes
        """
        # Initialise environment with specified mission length and regions
        # No perturbations, only color tools given (infinite uses)
        super().__init__(mission_length=mission_length,
                         recon_phase_length=recon_phase_length,
                         seconds_per_timestep=seconds_per_timestep,
                         valid_regions=valid_regions,
                         include_chained_bombs=include_chained_bombs,
                         include_fire_bombs=include_fire_bombs,
                         include_fuse_bombs=include_fuse_bombs,
                         obs_wrapper=DummyObs,
                         color_tools_only=True)
        
        # Parameters
        self.same_start_loc = same_start_loc
        self.include_collisions = include_collisions
        self.bomb_explode_terminate = bomb_explode_terminate
        self.start_timestep = math.ceil(self.recon_phase_length / self.seconds_per_timestep)

    @property
    def bombs(self) -> list[Bomb]:
        """
        List of all bombs currently in the environment.
        """
        return [bomb for node in self.graph.nodes.values() for bomb in node.bombs]

    @property
    def bomb_dependency(self) -> dict[int, int]:
        """
        Dictionary mapping bomb ID to bomb ID of bomb it is dependent on
        """
        return self._bomb_dependency

    @property
    def bomb_fuse(self) -> dict[int, Optional[int]]:
        """
        Dictionary mapping bomb ID to bomb fuses
        """
        return self._bomb_fuse

    @property
    def bomb_locations(self) -> dict[int, tuple[int, int]]:
        """
        Dictionary mapping bomb ID to tuples of bomb locations
        """
        return self._bomb_locations

    @property
    def bomb_sequence(self) -> dict[int, tuple[Color]]:
        """
        Dictionary mapping bomb ID to bomb sequence
        """
        return self._bomb_seq

    @property
    def max_score(self) -> float:
        """
        Return maximum possible score
        """
        return self._max_score

    @property
    def mission_length(self) -> float:
        """
        Return total mission length, in seconds
        """
        return self._mission_length

    @property
    def recon_phase_length(self) -> float:
        """
        Return recon phase length, in seconds
        """
        return self._recon_phase_length

    @property
    def seconds_per_timestep(self) -> float:
        """
        Return number of seconds per timestep in the environment
        """
        return self._seconds_per_timestep

    @property
    def start_nodes(self) -> dict[str, Node]:
        """
        Dictionary mapping agent ID to corresponding start node
        """
        return self._start_node_dict

    def bomb_id_to_bomb(self, bomb_id: int) -> Bomb:
        """
        Function to return the bomb with the given bomb ID

        Parameters
        ----------
        bomb_id : int
            Bomb ID of the bomb at a specific location (node ID)

        Returns
        -------
        bomb : Bomb
            Bomb with bomb ID
        """
        return self._bomb_id_to_bomb[bomb_id]

    def bomb_id_to_node_id(self, bomb_id: int) -> int:
        """
        Function to return the node ID of the node where the bomb, with the given bomb ID, is located

        Parameters
        ----------
        bomb_id : int
            Bomb ID of the bomb at a specific location (node ID)

        Returns
        -------
        node_id : int
            Node ID where the bomb is located
        """
        return self._bomb_id_to_node_id[bomb_id]

    def compute_heuristics(self, goal_node_id: int) -> dict[int, float]:
        """
        Function to precompute the h values of a location specified node ID using Dijkstra

        Parameters
        ----------
        goal_node_id : int
            Root goal location based on node ID

        Returns
        -------
        h_values : dict[int, float]
            Dictionary mapping node ID to h_values from goal location
        """
        # Maintain open and closed list
        open_list = []
        closed_list = dict()
        # Generate root node representing goal node where to bomb with given bomb ID is located
        # Location is represented with node ID
        root = {'loc': goal_node_id, 'cost': 0}
        # Push root to open and closed list
        heapq.heappush(open_list, (root['cost'], goal_node_id, root))
        closed_list[goal_node_id] = root

        # Iterate while open list is not empty
        while len(open_list) > 0:
            # Pop node with lowest cost
            # Break ties aribtrarily with node ID (smaller wins)
            (cost, curr_node_id, curr) = heapq.heappop(open_list)
            # Iterate over neighbouring nodes (not inclusive of current node)
            for neighbour_node_id in self.curr_and_neighbour_node_id(curr_node_id=curr_node_id, include_curr=False):
                # Generate child
                child = {
                         'loc': neighbour_node_id, 
                         'cost': cost + compute_distance(self.node_id_to_node_centroid(curr_node_id),
                                                         self.node_id_to_node_centroid(neighbour_node_id)
                                                        )
                        }
                # Eliminate duplicate and push child to open list
                if child['loc'] in closed_list:
                    existing_node = closed_list[child['loc']]
                    if existing_node['cost'] > child['cost']:
                        closed_list[child['loc']] = child
                        heapq.heappush(open_list, (child['cost'], child['loc'], child))
                else:
                    closed_list[child['loc']] = child
                    heapq.heappush(open_list, (child['cost'], child['loc'], child))

        # Build heuristics table
        h_values = dict()
        for loc, node in closed_list.items():
            h_values[loc] = node['cost']
        return h_values

    def curr_and_neighbour_node_id(self, curr_node_id: int, include_curr: bool = False) -> list[int]:
        """
        Function to list of node IDs of neighbouring nodes from current node

        Parameters
        ----------
        curr_node_id : int
            Node ID of current node
        include_curr : bool
            Whether to include current node in list (i.e wait at current node)

        Returns
        -------
        neighbours_list : tuple[float, float]
            List containing neighbouring nodes to current node with given node ID
        """
        if include_curr:
            return self.graph.neighbors(node_id=curr_node_id) + [curr_node_id]
        return self.graph.neighbors(node_id=curr_node_id)

    def multi_label_a_star(
            self,
            agent_id: str,
            algo: str,
            bomb_tup_list: list[tuple[int, Color]],
            collision_constraint_list: list[dict[str, Union[int, list[int], float, str]]],
            node_ids_list: list[int], 
            past_last_timestep: int, 
            start_loc: int, 
            absolute_temporal_range_completion_timestep_constraint_list: \
                list[dict[str, Union[Callable, Color, float, str]]],
            inter_goal_temporal_completion_timestep_constraint_list: \
                list[dict[str, Union[Callable, Color, float, str]]], 
            precedence_completion_timestep_constraint_list: list[dict[str, Union[Callable, Color, float, str]]],
            return_constraint_list: list[dict[str, Union[int, str]]]   
        ) -> tuple[Optional[list[dict[str, Union[int, str, None, Color, tuple[Color]]]]], Optional[float]]:
        """
        Function to execute low-level multi-label a star search for a single agent

        Parameters
        ----------
        agent_id : str
            Agent ID
        algo : str
            CBS algorithm ('cbs_ta', "cbs_ta_ptc")
        bomb_tup_list : list[tuple[int, Color]]
            List containing the sequence of tuples (bomb ID, Color) that the agent have to execute for defusal step 
            using corresponding color tool.
        collision_constraint_list : list[dict[str, Union[int, list[int], float, str]]]
            List of dictionary containing collision constraints of the following format:
            {'agent': agent ID, 'constraint_type': 'edge_collision'/'vertex_collision', 
             'loc': location (node ID) of vertex (list of len 1)/edge collision (list of len 2), 
             'timestep': float (at the end for edge collision)}
        node_ids_list : list[int]
            List of node IDs that are the sequence of nodes that the agent has to visit
        past_last_timestep : int
            Last timestep of previous solution
        start_loc : int
            Node ID of the node that is the starting location
        absolute_temporal_range_completion_timestep_constraint_list : list[dict[str, Union[Callable, Color, float, str]]]
            List of absolute temporal range completion timestep constraints that are dictionaries of the following 
            structure:
            {'agent_id': agent ID, 'bomb_id': bomb ID, 'conflict_type': str, 'operator': operator Callable, 
             'tool_color': Color, 'value': float}
        inter_goal_temporal_completion_timestep_constraint_list : list[dict[str, Union[Callable, Color, float, str]]]
            List of inter-goal temporal completion timestep constraints that are dictionaries of the following 
            structure:
            {'agent_id': agent ID, 'bomb_id': bomb ID, 'conflict_type': str, 'operator': operator Callable, 
             'tool_color': Color, 'value': float}
        precedence_completion_timestep_constraint_list : list[dict[str, Union[Callable, Color, float, str]]]
            List of precedence completion timestep constraints that are dictionaries of the following structure:
            {'agent_id': agent ID, 'bomb_id': bomb ID, 'conflict_type': str, 'operator': operator Callable, 
             'tool_color': Color, 'value': float}
        return_conflict_list : list[dict[str, Union[int, str]]]
            List containing return conflicts (vertex constraints) based on dictionary of following structure:
            {'agent_id': agent ID, 'conflict_type': str, 'loc': node ID, 'timestep': timestep}

        Returns
        -------
        global_path : Optional[list[dict[str, Union[int, str, None, Color, tuple[Color]]]]]
            List containing sequence of dictionaries that represents a path. Global path consists of all the node IDs 
            of nodes to be visited by node_ids_list. Dictionary has the following structure:
            {'loc': node ID, 'loc_centroid': (x, z) centroid of node, 
             'action': action to be executed after reaching stated node ID, 'bomb_id': bomb ID of bomb
             if action is 'apply_bomb_tool' else None, 'bomb_countdown_length': countdown length of bomb if action is 
             'apply_bomb_tool' else None, 'bomb_location': (x, z) location of bomb if action is 'apply_bomb_tool' else 
             None, 'bomb_sequence': bomb sequence of bomb if action is 'apply_bomb_tool' else None, 'tool_color': Color 
             of tool being applied to bomb if action is 'apply_bomb_tool' else None}

            Possible Actions
            ----------------
            'go_to': Travel to next location (node ID) in path
            'apply_bomb_tool': Apply bomb tool at bomb at current location (node ID) of specified color in bomb sequence
                               in task allocation
        global_cost : Optional[float]
            Cost of global_path
        """
        # Generate collision constraints for specified agent
        collision_constraint_dict, max_collision_timestep = \
            build_collision_constraint_dict(agent_id, collision_constraint_list)
        # CBS-TA-PTC
        if algo == 'cbs_ta_ptc':
            # Generate temporal constraints
            precedence_completion_timestep_constraint_dict = \
                build_temporal_constraint_dict(agent_id, precedence_completion_timestep_constraint_list)
            inter_goal_temporal_completion_timestep_constraint_dict = \
                build_temporal_constraint_dict(agent_id, inter_goal_temporal_completion_timestep_constraint_list)
            absolute_temporal_range_completion_timestep_constraint_dict = \
                build_temporal_constraint_dict(agent_id, absolute_temporal_range_completion_timestep_constraint_list)
            # Check if path exist given temporal constraints
            if not is_solvable_temporal_constraint(precedence_completion_timestep_constraint_dict,
                                                   inter_goal_temporal_completion_timestep_constraint_dict,
                                                   absolute_temporal_range_completion_timestep_constraint_dict,
                                                   self.mission_length):
                return None, None
        # CBS-TA
        elif algo == 'cbs_ta':
            # Generate return constraints
            return_constraint_dict, max_return_constraint_timestep = \
                build_return_constraint_dict(agent_id, return_constraint_list)
        # Track global timestep across multiple a star searches corresponding to environment timestep
        global_timestep = past_last_timestep if past_last_timestep == self.start_timestep else past_last_timestep + 1
        # Track global cost across multiple a star searches
        global_cost = 0
        # Global path across multiple a star searches
        global_path = []
        # Iterate over bomb IDs
        for i, node_id in enumerate(node_ids_list):
            # Set start and goal locations
            if i != 0:
                start_loc = node_ids_list[i - 1]
            goal_loc = node_id
            # Maintain open and closed list
            open_list = []
            closed_list = dict()
            # Obtain h_value for the to reach the goal_loc of bomb with given node ID given current start_loc
            h_value = get_multi_label_a_star_h_value(curr_loc=start_loc,
                                                     curr_goal_index=i,
                                                     node_ids_list=node_ids_list,
                                                     h_values_dict=self._h_values_dict)
            # Initialise root node
            if i != 0:
                root = {'loc': start_loc,
                        'loc_centroid': self.node_id_to_node_centroid(start_loc),
                        'goal_loc_ind': i, 
                        'g_val': 0, 
                        'h_val': h_value, 
                        'parent': None, 
                        'timestep': global_timestep + 1}
            else:
                root = {'loc': start_loc,
                        'loc_centroid': self.node_id_to_node_centroid(start_loc),
                        'goal_loc_ind': i, 
                        'g_val': 0, 
                        'h_val': h_value, 
                        'parent': None, 
                        'timestep': global_timestep}
            # Push root to open and closed list
            push_multi_label_a_star_node(open_list, root)
            closed_list[(root['loc'], root['goal_loc_ind'], root['timestep'])] = root
            # Boolean to track if path is found
            found_path = False
            # Timestep of bomb defusal
            bomb_defusal_timestep = None

            # Iterate while open list is not empty
            while len(open_list) > 0:
                # Pop node with lowest cost
                # Break ties in order of priority (smaller wins): 1) g_val + h_val, 2) h_val, 3) node ID
                curr = pop_multi_label_a_star_node(open_list)
                # Check if goal location is reached
                if curr['loc'] == goal_loc:
                    # Generate new path
                    # NOTE: Assume there is no time required to cycle through the multiple bombs in a node to find the
                    #       correct bomb to apply the tool.
                    new_path, new_cost = get_path_and_cost(curr)
                    # Generate global path, cost and timestep assuming path is valid
                    new_global_path = global_path + new_path
                    new_global_cost = global_cost + new_cost
                    new_global_timestep = curr['timestep']
                    # Timestep assertions 
                    if past_last_timestep == self.start_timestep:
                        assert curr['timestep'] == len(new_global_path) - 1 + past_last_timestep
                    else:
                        assert curr['timestep'] == len(new_global_path) + past_last_timestep
                    # CBS-TA-PTC
                    if algo == 'cbs_ta_ptc':
                        # Boolean for absolute, precedence, inter-goal temporal, future constraints affecting agent
                        absolute_temporal_range_constrained = False
                        precedence_constrained = False
                        inter_goal_temporal_constrained = False
                        future_constrained = False
                        # Consider path with bomb defusal involved
                        if len(bomb_tup_list) != 0:
                            # Obtain the current bomb ID and node ID of bomb that agent is trying to defuse
                            bomb_tup = bomb_tup_list[0]
                            bomb_id = bomb_tup[0]
                            bomb_node_id = self.bomb_id_to_node_id(bomb_id)
                            # Agent is defusing a bomb at its current location
                            if bomb_node_id == curr['loc']:
                                # Check if current defusal time satisfy absolute temporal range constraints
                                absolute_temporal_range_constrained, absolute_temporal_range_future_valid = \
                                    is_temporal_constrained(bomb_tup,
                                                            new_global_timestep,
                                                            absolute_temporal_range_completion_timestep_constraint_dict)
                                # Check if current defusal time satisfy completion timestep constraints
                                precedence_constrained, precedence_future_valid = \
                                    is_temporal_constrained(bomb_tup,
                                                            new_global_timestep,
                                                            precedence_completion_timestep_constraint_dict)
                                # Check if current defusal time satisfy inter-goal temporal constraints
                                inter_goal_temporal_constrained, inter_goal_temporal_future_valid = \
                                    is_temporal_constrained(bomb_tup,
                                                            new_global_timestep,
                                                            inter_goal_temporal_completion_timestep_constraint_dict)
                                # Check if its possible for any future paths to satisfy completion timestep and
                                # Inter-goal temporal constraints if they are not satisfied
                                if absolute_temporal_range_future_valid == False or \
                                    precedence_future_valid == False or inter_goal_temporal_future_valid == False:
                                    # Fail to find solution
                                    return None, None
                                # Check if completion timestep and inter-goal temporal constraints are satisfied
                                if not absolute_temporal_range_constrained and not precedence_constrained and \
                                    not inter_goal_temporal_constrained:
                                    # Update bomb defusal timestep
                                    bomb_defusal_timestep = new_global_timestep
                        # Check for future collision constraints only if absolute temporal range, precedence, 
                        # Inter-goal temporal constraints are satisfied
                        if not absolute_temporal_range_constrained and not precedence_constrained and \
                            not inter_goal_temporal_constrained:
                            for timestep, constraint_list in collision_constraint_dict.items():
                                if timestep <= new_global_timestep:
                                    continue
                                else:
                                    for constraint in constraint_list:
                                        if constraint['agent_id'] == agent_id and \
                                        (constraint['loc'] == curr['loc'] or curr['loc'] in constraint['loc']):
                                            future_constrained = True
                                            break
                                if future_constrained:
                                    break     
                        # Valid path only if all constraints are satisfied
                        if not absolute_temporal_range_constrained and not precedence_constrained and \
                            not inter_goal_temporal_constrained and not future_constrained:
                            found_path = True
                            # Consider path with bomb defusal involved
                            if len(bomb_tup_list) != 0:
                                # Obtain the current bomb ID, bomb's node ID and bomb that agent is trying to defuse
                                bomb_tup = bomb_tup_list[0]
                                bomb_id = bomb_tup[0]
                                bomb_node_id = self.bomb_id_to_node_id(bomb_id)
                                bomb = self.bomb_id_to_bomb(bomb_id)
                                # Agent is defusing a bomb at its current location
                                if bomb_node_id == curr['loc']:
                                    # Obtain index of bomb defusal timestep
                                    if past_last_timestep == self.start_timestep:
                                        bomb_defusal_ind = bomb_defusal_timestep - past_last_timestep
                                    else:
                                        bomb_defusal_ind = bomb_defusal_timestep - past_last_timestep - 1
                                    # Update global path 
                                    new_global_path[bomb_defusal_ind]['action'] = 'apply_bomb_tool'
                                    new_global_path[bomb_defusal_ind]['bomb_id'] = bomb_id
                                    new_global_path[bomb_defusal_ind]['bomb_countdown_length'] = bomb.countdown_length
                                    new_global_path[bomb_defusal_ind]['bomb_location'] = bomb.location
                                    new_global_path[bomb_defusal_ind]['bomb_sequence'] = bomb.sequence
                                    new_global_path[bomb_defusal_ind]['tool_color'] = bomb_tup[1]
                                    # Remove tuple (bomb ID, Color)
                                    bomb_tup_list.remove(bomb_tup)
                            # Update global paths, cost and timestep
                            global_path = new_global_path[:]
                            global_cost = new_global_cost
                            global_timestep = new_global_timestep
                            break
                    # CBS-TA
                    elif algo == 'cbs_ta':
                        # Boolean for return and future constraints affecting agent
                        return_constrained = False
                        future_constrained = False
                        # Consider path with bomb defusal involved
                        if len(bomb_tup_list) != 0:
                            # Obtain the current bomb ID and node ID of bomb that agent is trying to defuse
                            bomb_tup = bomb_tup_list[0]
                            bomb_id = bomb_tup[0]
                            bomb_node_id = self.bomb_id_to_node_id(bomb_id)
                            # Agent is defusing a bomb at its current location
                            if bomb_node_id == curr['loc']:
                                # Update bomb defusal timestep
                                bomb_defusal_timestep = new_global_timestep
                        # Check for future return constraints
                        for timestep, constraint_list in return_constraint_dict.items():
                            if timestep <= new_global_timestep:
                                continue
                            else:
                                for constraint in constraint_list:
                                    if constraint['agent_id'] == agent_id and constraint['loc'] == curr['loc']:
                                        return_constrained = True
                                        break
                            if return_constrained:
                                break
                        # Check for future collision constraints only if future return constraints are satisfied 
                        if not return_constrained:
                            for timestep, constraint_list in collision_constraint_dict.items():
                                if timestep <= new_global_timestep:
                                    continue
                                else:
                                    for constraint in constraint_list:
                                        if constraint['agent_id'] == agent_id and \
                                        (constraint['loc'] == curr['loc'] or curr['loc'] in constraint['loc']):
                                            future_constrained = True
                                            break
                                if future_constrained:
                                    break
                        # Valid path only if all constraints are satisfied
                        if not return_constrained and not future_constrained:
                            found_path = True
                            # Consider path with bomb defusal involved
                            if len(bomb_tup_list) != 0:
                                # Obtain the current bomb ID, bomb's node ID and bomb that agent is trying to defuse
                                bomb_tup = bomb_tup_list[0]
                                bomb_id = bomb_tup[0]
                                bomb_node_id = self.bomb_id_to_node_id(bomb_id)
                                bomb = self.bomb_id_to_bomb(bomb_id)
                                # Agent is defusing a bomb at its current location
                                if bomb_node_id == curr['loc']:
                                    # Obtain index of bomb defusal timestep
                                    if past_last_timestep == self.start_timestep:
                                        bomb_defusal_ind = bomb_defusal_timestep - past_last_timestep
                                    else:
                                        bomb_defusal_ind = bomb_defusal_timestep - past_last_timestep - 1
                                    # Update global path 
                                    new_global_path[bomb_defusal_ind]['action'] = 'apply_bomb_tool'
                                    new_global_path[bomb_defusal_ind]['bomb_id'] = bomb_id
                                    new_global_path[bomb_defusal_ind]['bomb_countdown_length'] = bomb.countdown_length
                                    new_global_path[bomb_defusal_ind]['bomb_location'] = bomb.location
                                    new_global_path[bomb_defusal_ind]['bomb_sequence'] = bomb.sequence
                                    new_global_path[bomb_defusal_ind]['tool_color'] = bomb_tup[1]
                                    # Remove tuple (bomb ID, Color)
                                    bomb_tup_list.remove(bomb_tup)
                            # Update global paths, cost and timestep
                            global_path = new_global_path[:]
                            global_cost = new_global_cost
                            global_timestep = new_global_timestep
                            break          
                # Iterate over neighbouring nodes (inclusive of current node == wait at current node)
                for neighbour_node_id in self.curr_and_neighbour_node_id(curr_node_id=curr['loc'], include_curr=True):
                    # Check for collision constraints
                    if curr['timestep'] + 1 <= max_collision_timestep:
                        if is_collision_constrained(curr['loc'], neighbour_node_id, curr['timestep'] + 1, 
                                                    collision_constraint_dict):
                            continue
                    # Check for return constraints for cbs_ta
                    if algo == 'cbs_ta' and curr['timestep'] + 1 <= max_return_constraint_timestep:
                        if is_return_constrained(curr['loc'], neighbour_node_id, curr['timestep'] + 1, 
                                                 return_constraint_dict):
                            continue
                    # For non-waiting actions
                    if curr['loc'] != neighbour_node_id:
                        # Generate child node
                        child = {'loc': neighbour_node_id,
                                 'loc_centroid': self.node_id_to_node_centroid(neighbour_node_id),
                                 'goal_loc_ind': i,
                                 'g_val': curr['g_val'] \
                                          + compute_distance(self.node_id_to_node_centroid(curr['loc']),
                                                             self.node_id_to_node_centroid(neighbour_node_id)
                                                            ),
                                 'h_val': get_multi_label_a_star_h_value(curr_loc=neighbour_node_id,
                                                                         curr_goal_index=i,
                                                                         node_ids_list=node_ids_list,
                                                                         h_values_dict=self._h_values_dict),
                                 'parent': curr,
                                 'timestep': curr['timestep'] + 1
                                }
                    # Waiting action
                    else:
                        # Use minimum distance between other neighbours as cost of waiting action
                        min_dist = np.inf
                        for neighbour_node_id_prime in \
                            self.curr_and_neighbour_node_id(curr_node_id=curr['loc'], include_curr=False):
                            dist = compute_distance(self.node_id_to_node_centroid(curr['loc']),
                                                    self.node_id_to_node_centroid(neighbour_node_id_prime))
                            if dist < min_dist:
                                min_dist = dist
                        # Generate child node
                        child = {'loc': neighbour_node_id,
                                 'loc_centroid': self.node_id_to_node_centroid(neighbour_node_id),
                                 'goal_loc_ind': i,
                                 'g_val': curr['g_val'] + min_dist, 
                                 'h_val': get_multi_label_a_star_h_value(curr_loc=neighbour_node_id,
                                                                         curr_goal_index=i,
                                                                         node_ids_list=node_ids_list,
                                                                         h_values_dict=self._h_values_dict),
                                 'parent': curr,
                                 'timestep': curr['timestep'] + 1
                                }
                    # Eliminate duplicate and push child to open list
                    if (child['loc'], child['goal_loc_ind'], child['timestep']) in closed_list:
                        existing_node = closed_list[(child['loc'], child['goal_loc_ind'], child['timestep'])]
                        if compare_nodes(child, existing_node):
                            closed_list[(child['loc'], child['goal_loc_ind'], child['timestep'])] = child
                            push_multi_label_a_star_node(open_list, child)      
                    else:
                        closed_list[(child['loc'], child['goal_loc_ind'], child['timestep'])] = child
                        push_multi_label_a_star_node(open_list, child)
            
            # Failed to find solution
            if not found_path:
                return None, None

        return global_path, global_cost

    def multi_step_a_star(
            self,
            agent_id: str,
            algo: str, 
            bomb_tup_list: list[tuple[int, Color]],
            collision_constraint_list: list[dict[str, Union[int, list[int], float, str]]], 
            node_ids_list: list[int],
            past_last_timestep: int,
            start_loc: int, 
            absolute_temporal_range_completion_timestep_constraint_list: \
                list[dict[str, Union[Callable, Color, float, str]]],
            inter_goal_temporal_completion_timestep_constraint_list: \
                list[dict[str, Union[Callable, Color, float, str]]],
            precedence_completion_timestep_constraint_list: list[dict[str, Union[Callable, Color, float, str]]],
            return_constraint_list: list[dict[str, Union[int, str]]]      
        ) -> tuple[Optional[list[dict[str, Union[int, str, None, Color, tuple[Color]]]]], Optional[float]]:
        """
        Function to execute low-level multi-step a star search for a single agent

        Parameters
        ----------
        agent_id : str
            Agent ID
        algo : str
            CBS algorithm ('cbs_ta', "cbs_ta_ptc")
        bomb_tup_list : list[tuple[int, Color]]
            List containing the sequence of tuples (bomb ID, Color) that the agent have to execute for defusal step 
            using corresponding color tool.
        collision_constraint_list : list[dict[str, Union[int, list[int], float, str]]]
            List of dictionary containing collision constraints of the following format:
            {'agent': agent ID, 'constraint_type': 'edge_collision'/'vertex_collision', 
             'loc': location (node ID) of vertex (list of len 1)/edge collision (list of len 2), 
             'timestep': float (at the end for edge collision)}
        node_ids_list : list[int]
            List of node IDs that are the sequence of nodes that the agent has to visit
        past_last_timestep : int
            Last timestep of previous solution
        start_loc : int
            Node ID of the node that is the starting location
        absolute_temporal_range_completion_timestep_constraint_list : list[dict[str, Union[Callable, Color, float, str]]]
            List of absolute temporal range completion timestep constraints that are dictionaries of the following 
            structure:
            {'agent_id': agent ID, 'bomb_id': bomb ID, 'conflict_type': str, 'operator': operator Callable, 
             'tool_color': Color, 'value': float}
        inter_goal_temporal_completion_timestep_constraint_list : list[dict[str, Union[Callable, Color, float, str]]]
            List of inter-goal temporal completion timestep constraints that are dictionaries of the following 
            structure:
            {'agent_id': agent ID, 'bomb_id': bomb ID, 'conflict_type': str, 'operator': operator Callable, 
             'tool_color': Color, 'value': float}
        precedence_completion_timestep_constraint_list : list[dict[str, Union[Callable, Color, float, str]]]
            List of precedence completion timestep constraints that are dictionaries of the following structure:
            {'agent_id': agent ID, 'bomb_id': bomb ID, 'conflict_type': str, 'operator': operator Callable, 
             'tool_color': Color, 'value': float}
        return_conflict_list : list[dict[str, Union[int, str]]]
            List containing return conflicts (vertex constraints) based on dictionary of following structure:
            {'agent_id': agent ID, 'conflict_type': str, 'loc': node ID, 'timestep': timestep}

        Returns
        -------
        global_path : Optional[list[dict[str, Union[int, str, None, Color, tuple[Color]]]]]
            List containing sequence of dictionaries that represents a path. Global path consists of all the node IDs 
            of nodes to be visited by node_ids_list. Dictionary has the following structure:
            {'loc': node ID, 'loc_centroid': (x, z) centroid of node, 
             'action': action to be executed after reaching stated node ID, 'bomb_id': bomb ID of bomb
             if action is 'apply_bomb_tool' else None, 'bomb_countdown_length': countdown length of bomb if action is 
             'apply_bomb_tool' else None, 'bomb_location': (x, z) location of bomb if action is 'apply_bomb_tool' else 
             None, 'bomb_sequence': bomb sequence of bomb if action is 'apply_bomb_tool' else None, 'tool_color': Color 
             of tool being applied to bomb if action is 'apply_bomb_tool' else None}

            Possible Actions
            ----------------
            'go_to': Travel to next location (node ID) in path
            'apply_bomb_tool': Apply bomb tool at bomb at current location (node ID) of specified color in bomb sequence
                               in task allocation
        global_cost : Optional[float]
            Cost of global_path
        """
        # Generate collision constraints for specified agent
        collision_constraint_dict, max_collision_timestep = \
            build_collision_constraint_dict(agent_id, collision_constraint_list)
        # CBS-TA-PTC
        if algo == 'cbs_ta_ptc':
            # Generate temporal constraints
            precedence_completion_timestep_constraint_dict = \
                build_temporal_constraint_dict(agent_id, precedence_completion_timestep_constraint_list)
            inter_goal_temporal_completion_timestep_constraint_dict = \
                build_temporal_constraint_dict(agent_id, inter_goal_temporal_completion_timestep_constraint_list)
            absolute_temporal_range_completion_timestep_constraint_dict = \
                build_temporal_constraint_dict(agent_id, absolute_temporal_range_completion_timestep_constraint_list)
            # Check if path exist given temporal constraints
            if not is_solvable_temporal_constraint(precedence_completion_timestep_constraint_dict,
                                                   inter_goal_temporal_completion_timestep_constraint_dict,
                                                   absolute_temporal_range_completion_timestep_constraint_dict,
                                                   self.mission_length):
                return None, None
        # CBS-TA
        elif algo == 'cbs_ta':
            # Generate return constraints
            return_constraint_dict, max_return_constraint_timestep = \
                build_return_constraint_dict(agent_id, return_constraint_list)
        # Track global timestep across multiple a star searches corresponding to environment timestep
        global_timestep = past_last_timestep if past_last_timestep == self.start_timestep else past_last_timestep + 1
        # Track global cost across multiple a star searches
        global_cost = 0
        # Global path across multiple a star searches
        global_path = []
        # Iterate over bomb IDs
        for i, node_id in enumerate(node_ids_list):
            # Set start and goal locations
            if i != 0:
                start_loc = node_ids_list[i - 1]
            goal_loc = node_id
            # Maintain open and closed list
            open_list = []
            closed_list = dict()
            # Obtain h_value for the to reach the goal_loc of bomb with given node ID given current start_loc
            h_value = self._h_values_dict[node_id][start_loc]
            # Initialise root node
            if i != 0:
                root = {'loc': start_loc,
                        'loc_centroid': self.node_id_to_node_centroid(start_loc), 
                        'g_val': 0, 
                        'h_val': h_value, 
                        'parent': None, 
                        'timestep': global_timestep + 1}
            else:
                root = {'loc': start_loc,
                        'loc_centroid': self.node_id_to_node_centroid(start_loc), 
                        'g_val': 0, 
                        'h_val': h_value, 
                        'parent': None, 
                        'timestep': global_timestep}
            # Push root to open and closed list
            push_multi_step_a_star_node(open_list, root)
            closed_list[(root['loc'], root['timestep'])] = root
            # Boolean to track if path is found
            found_path = False
            # Timestep of bomb defusal
            bomb_defusal_timestep = None

            # Iterate while open list is not empty
            while len(open_list) > 0:
                # Pop node with lowest cost
                # Break ties in order of priority (smaller wins): 1) g_val + h_val, 2) h_val, 3) node ID
                curr = pop_multi_step_a_star_node(open_list)
                # Check if goal location is reached
                if curr['loc'] == goal_loc:
                    # Generate new path
                    # NOTE: Assume there is no time required to cycle through the multiple bombs in a node to find the
                    #       correct bomb to apply the tool.
                    new_path, new_cost = get_path_and_cost(curr)
                    # Generate global path, cost and timestep assuming path is valid
                    new_global_path = global_path + new_path
                    new_global_cost = global_cost + new_cost
                    new_global_timestep = curr['timestep']
                    # Timestep assertions 
                    if past_last_timestep == self.start_timestep:
                        assert curr['timestep'] == len(new_global_path) - 1 + past_last_timestep
                    else:
                        assert curr['timestep'] == len(new_global_path) + past_last_timestep
                    # CBS-TA-PTC
                    if algo == 'cbs_ta_ptc':
                        # Boolean for absolute, precedence, inter-goal temporal, future constraints affecting agent
                        absolute_temporal_range_constrained = False
                        precedence_constrained = False
                        inter_goal_temporal_constrained = False
                        future_constrained = False
                        # Consider path with bomb defusal involved
                        if len(bomb_tup_list) != 0:
                            # Obtain the current bomb ID and node ID of bomb that agent is trying to defuse
                            bomb_tup = bomb_tup_list[0]
                            bomb_id = bomb_tup[0]
                            bomb_node_id = self.bomb_id_to_node_id(bomb_id)
                            # Agent is defusing a bomb at its current location
                            if bomb_node_id == curr['loc']:
                                # Check if current defusal time satisfy absolute temporal range constraints
                                absolute_temporal_range_constrained, absolute_temporal_range_future_valid = \
                                    is_temporal_constrained(bomb_tup,
                                                            new_global_timestep,
                                                            absolute_temporal_range_completion_timestep_constraint_dict)
                                # Check if current defusal time satisfy completion timestep constraints
                                precedence_constrained, precedence_future_valid = \
                                    is_temporal_constrained(bomb_tup,
                                                            new_global_timestep,
                                                            precedence_completion_timestep_constraint_dict)
                                # Check if current defusal time satisfy inter-goal temporal constraints
                                inter_goal_temporal_constrained, inter_goal_temporal_future_valid = \
                                    is_temporal_constrained(bomb_tup,
                                                            new_global_timestep,
                                                            inter_goal_temporal_completion_timestep_constraint_dict)
                                # Check if its possible for any future paths to satisfy completion timestep and
                                # Inter-goal temporal constraints if they are not satisfied
                                if absolute_temporal_range_future_valid == False or \
                                    precedence_future_valid == False or inter_goal_temporal_future_valid == False:
                                    # Fail to find solution
                                    return None, None
                                # Check if completion timestep and inter-goal temporal constraints are satisfied
                                if not absolute_temporal_range_constrained and not precedence_constrained and \
                                    not inter_goal_temporal_constrained:
                                    # Update bomb defusal timestep
                                    bomb_defusal_timestep = new_global_timestep
                        # Check for future collision constraints only if absolute temporal range, precedence, 
                        # Inter-goal temporal constraints are satisfied
                        if not absolute_temporal_range_constrained and not precedence_constrained and \
                            not inter_goal_temporal_constrained:
                            for timestep, constraint_list in collision_constraint_dict.items():
                                if timestep <= new_global_timestep:
                                    continue
                                else:
                                    for constraint in constraint_list:
                                        if constraint['agent_id'] == agent_id and \
                                        (constraint['loc'] == curr['loc'] or curr['loc'] in constraint['loc']):
                                            future_constrained = True
                                            break
                                if future_constrained:
                                    break     
                        # Valid path only if all constraints are satisfied
                        if not absolute_temporal_range_constrained and not precedence_constrained and \
                            not inter_goal_temporal_constrained and not future_constrained:
                            found_path = True
                            # Consider path with bomb defusal involved
                            if len(bomb_tup_list) != 0:
                                # Obtain the current bomb ID, bomb's node ID and bomb that agent is trying to defuse
                                bomb_tup = bomb_tup_list[0]
                                bomb_id = bomb_tup[0]
                                bomb_node_id = self.bomb_id_to_node_id(bomb_id)
                                bomb = self.bomb_id_to_bomb(bomb_id)
                                # Agent is defusing a bomb at its current location
                                if bomb_node_id == curr['loc']:
                                    # Obtain index of bomb defusal timestep
                                    if past_last_timestep == self.start_timestep:
                                        bomb_defusal_ind = bomb_defusal_timestep - past_last_timestep
                                    else:
                                        bomb_defusal_ind = bomb_defusal_timestep - past_last_timestep - 1
                                    # Update global path 
                                    new_global_path[bomb_defusal_ind]['action'] = 'apply_bomb_tool'
                                    new_global_path[bomb_defusal_ind]['bomb_id'] = bomb_id
                                    new_global_path[bomb_defusal_ind]['bomb_countdown_length'] = bomb.countdown_length
                                    new_global_path[bomb_defusal_ind]['bomb_location'] = bomb.location
                                    new_global_path[bomb_defusal_ind]['bomb_sequence'] = bomb.sequence
                                    new_global_path[bomb_defusal_ind]['tool_color'] = bomb_tup[1]
                                    # Remove tuple (bomb ID, Color)
                                    bomb_tup_list.remove(bomb_tup)
                            # Update global paths, cost and timestep
                            global_path = new_global_path[:]
                            global_cost = new_global_cost
                            global_timestep = new_global_timestep
                            break
                    # CBS-TA
                    elif algo == 'cbs_ta':
                        # Boolean for return and future constraints affecting agent
                        return_constrained = False
                        future_constrained = False
                        # Consider path with bomb defusal involved
                        if len(bomb_tup_list) != 0:
                            # Obtain the current bomb ID and node ID of bomb that agent is trying to defuse
                            bomb_tup = bomb_tup_list[0]
                            bomb_id = bomb_tup[0]
                            bomb_node_id = self.bomb_id_to_node_id(bomb_id)
                            # Agent is defusing a bomb at its current location
                            if bomb_node_id == curr['loc']:
                                # Update bomb defusal timestep
                                bomb_defusal_timestep = new_global_timestep
                        # Check for future return constraints
                        for timestep, constraint_list in return_constraint_dict.items():
                            if timestep <= new_global_timestep:
                                continue
                            else:
                                for constraint in constraint_list:
                                    if constraint['agent_id'] == agent_id and constraint['loc'] == curr['loc']:
                                        return_constrained = True
                                        break
                            if return_constrained:
                                break
                        # Check for future collision constraints only if future return constraints are satisfied 
                        if not return_constrained:
                            for timestep, constraint_list in collision_constraint_dict.items():
                                if timestep <= new_global_timestep:
                                    continue
                                else:
                                    for constraint in constraint_list:
                                        if constraint['agent_id'] == agent_id and \
                                        (constraint['loc'] == curr['loc'] or curr['loc'] in constraint['loc']):
                                            future_constrained = True
                                            break
                                if future_constrained:
                                    break
                        # Valid path only if all constraints are satisfied
                        if not return_constrained and not future_constrained:
                            found_path = True
                            # Consider path with bomb defusal involved
                            if len(bomb_tup_list) != 0:
                                # Obtain the current bomb ID, bomb's node ID and bomb that agent is trying to defuse
                                bomb_tup = bomb_tup_list[0]
                                bomb_id = bomb_tup[0]
                                bomb_node_id = self.bomb_id_to_node_id(bomb_id)
                                bomb = self.bomb_id_to_bomb(bomb_id)
                                # Agent is defusing a bomb at its current location
                                if bomb_node_id == curr['loc']:
                                    # Obtain index of bomb defusal timestep
                                    if past_last_timestep == self.start_timestep:
                                        bomb_defusal_ind = bomb_defusal_timestep - past_last_timestep
                                    else:
                                        bomb_defusal_ind = bomb_defusal_timestep - past_last_timestep - 1
                                    # Update global path 
                                    new_global_path[bomb_defusal_ind]['action'] = 'apply_bomb_tool'
                                    new_global_path[bomb_defusal_ind]['bomb_id'] = bomb_id
                                    new_global_path[bomb_defusal_ind]['bomb_countdown_length'] = bomb.countdown_length
                                    new_global_path[bomb_defusal_ind]['bomb_location'] = bomb.location
                                    new_global_path[bomb_defusal_ind]['bomb_sequence'] = bomb.sequence
                                    new_global_path[bomb_defusal_ind]['tool_color'] = bomb_tup[1]
                                    # Remove tuple (bomb ID, Color)
                                    bomb_tup_list.remove(bomb_tup)
                            # Update global paths, cost and timestep
                            global_path = new_global_path[:]
                            global_cost = new_global_cost
                            global_timestep = new_global_timestep
                            break
                # Iterate over neighbouring nodes (inclusive of current node == wait at current node)
                for neighbour_node_id in self.curr_and_neighbour_node_id(curr_node_id=curr['loc'], include_curr=True):
                    # Check for constraints
                    if curr['timestep'] + 1 <= max_collision_timestep:
                        if is_collision_constrained(curr['loc'], neighbour_node_id, curr['timestep'] + 1, 
                                                    collision_constraint_dict):
                            continue
                    # Check for return constraints for cbs_ta
                    if algo == 'cbs_ta' and curr['timestep'] + 1 <= max_return_constraint_timestep:
                        if is_return_constrained(curr['loc'], neighbour_node_id, curr['timestep'] + 1, 
                                                 return_constraint_dict):
                            continue
                    # For non-waiting actions
                    if curr['loc'] != neighbour_node_id:
                        # Generate child node
                        child = {'loc': neighbour_node_id,
                                 'loc_centroid': self.node_id_to_node_centroid(neighbour_node_id),
                                 'g_val': curr['g_val'] \
                                          + compute_distance(self.node_id_to_node_centroid(curr['loc']),
                                                             self.node_id_to_node_centroid(neighbour_node_id)
                                                            ),
                                 'h_val': self._h_values_dict[node_id][neighbour_node_id],
                                 'parent': curr,
                                 'timestep': curr['timestep'] + 1
                                }
                    # Waiting action
                    else:
                        # Use minimum distance between other neighbours as cost of waiting action
                        min_dist = np.inf
                        for neighbour_node_id_prime in \
                            self.curr_and_neighbour_node_id(curr_node_id=curr['loc'], include_curr=False):
                            dist = compute_distance(self.node_id_to_node_centroid(curr['loc']),
                                                    self.node_id_to_node_centroid(neighbour_node_id_prime))
                            if dist < min_dist:
                                min_dist = dist
                        # Generate child node
                        child = {'loc': neighbour_node_id,
                                 'loc_centroid': self.node_id_to_node_centroid(neighbour_node_id),
                                 'g_val': curr['g_val'] + min_dist, 
                                 'h_val': self._h_values_dict[node_id][neighbour_node_id],
                                 'parent': curr,
                                 'timestep': curr['timestep'] + 1
                                }
                    # Eliminate duplicate and push child to open list
                    if (child['loc'], child['timestep']) in closed_list:
                        existing_node = closed_list[(child['loc'], child['timestep'])]
                        if compare_nodes(child, existing_node):
                            closed_list[(child['loc'], child['timestep'])] = child
                            push_multi_step_a_star_node(open_list, child)      
                    else:
                        closed_list[(child['loc'], child['timestep'])] = child
                        push_multi_step_a_star_node(open_list, child)
            
            # Failed to find solution
            if not found_path:
                return None, None

        return global_path, global_cost

    def is_solvable(self) -> bool:
        """
        Function to check if the entire task allocation is solvable given the tool set of all the agents

        Returns
        -------
        solvable : bool 
            Whether entire task allocation is solvable given the tool set of all the agents. 
        """
        # Iterate over all bombs in environment
        for bomb in self.bombs:
            # List to store boolean that checks if each color in bomb sequence is solvable
            bomb_color_solvable_list = [None for _ in range(len(bomb.sequence))]
            # Iterate each color in bomb sequence
            for i, color in enumerate(bomb.sequence):
                # Iteraate over each agents
                for agent in self.agents.values():
                    # Attempt to obtain required color tool from agent
                    required_tool = agent.get_tool_from_inventory(Tool.from_color(color))
                    if required_tool != None:
                        # Required tool exist
                        bomb_color_solvable_list[i] = True
                        break
            # Check if each color in bomb sequence is solvable
            if bomb_color_solvable_list.count(None) != 0:
                return False
        return True

    def is_optimal_bomb_allocation(self, 
                                   bomb_id_tup_dict: dict[str, list[tuple[int, Color]]], 
                                   pruning_heuristic: str
        ) -> bool:
        """
        Function to check task allocation to each agent is "optimal" (SUBJECT TO UPDATES TO GYM_DRAGON) based on the 
        following possible heuristics:

            1) 'even_bomb_allocation' : Bomb sequence tasks must evenly distributed amongst agents
            2) 'none' : No pruning
        
        Parameters
        ----------
        bomb_id_tup_dict : dict[str, list[tuple[int, Color]]]
            Dictionary mapping of agent ID to the list containing the sequence of tuples (bomb ID, Color) that the agent 
            have to execute for defusal step using corresponding color tool.
        pruning_heuristic : str
            Heuristic to prune the solution.

        Returns
        -------
        optimal : bool 
            Whether sub task allocation is optimal.
        """
        # Even bomb distribution heuristic
        if pruning_heuristic == 'even_bomb_allocation':
            for bomb_tup_list in bomb_id_tup_dict.values():
                # Obtain list of bomb IDs
                bomb_ids_list = [tup[0] for tup in bomb_tup_list]
                # Count number of bomb IDs
                _, bomb_id_count_arr = np.unique(np.array(bomb_ids_list), return_counts=True)
                # Bomb sequence tasks must evenly distributed amongst agents
                if any(bomb_id_count_arr > 1):
                    return False
            return True
        # No pruning
        elif pruning_heuristic == 'none':
            return True
        else:
            raise NotImplementedError(f'Pruning method {pruning_heuristic} not implemented.')
        
    def is_valid_bomb_allocation(self, bomb_id_tup_dict: dict[str, list[tuple[int, Color]]]) -> bool:
        """
        Function to check if task allocation to each agent is valid based on simple heuristics as follows: 

            1) Single agent with two different tools cannot defuse 3 stage bomb alone
            2) Agent without the necessary tools cannot defuse the bomb

        Parameters
        ----------
        bomb_id_tup_dict : dict[str, list[tuple[int, Color]]]
            Dictionary mapping of agent ID to the list containing the sequence of tuples (bomb ID, Color) that the agent 
            have to execute for defusal step using corresponding color tool.

        Returns
        -------
        valid : bool 
            Whether sub task allocation is valid.
        """
        for agent_id, bomb_tup_list in bomb_id_tup_dict.items():
            # Obtain list of bomb IDs
            bomb_ids_list = [tup[0] for tup in bomb_tup_list]
            # Count number of bomb IDs
            _, bomb_id_count_arr = np.unique(np.array(bomb_ids_list), return_counts=True)
            # Single agent with two different tools cannot defuse 3 stage bomb
            if any(bomb_id_count_arr >= Bomb.max_num_stages):
                return False
            # Iterate over bomb_tup_list
            for bomb_tup in bomb_tup_list:
                # Attempt to obtain required color tool
                required_tool = self.agents[agent_id].get_tool_from_inventory(Tool.from_color(bomb_tup[1]))
                # Agent don't have the required tool
                if required_tool == None:
                    return False
        return True

    def node_id_to_node_centroid(self, node_id: int) -> tuple[float, float]:
        """
        Function to return centroid of node with given node ID

        Parameters
        ----------
        node_id : int
            Node ID of node

        Returns
        -------
        centroid : tuple[float, float]
            Tuple containing the cartesian coordinates of the node with given node ID
        """
        return self.graph.nodes[node_id].centroid

    def pre_compute_heuristics(self,
                               seed: Optional[int] = None,
                               additional_node_id_list: list[int] = []
        ):
        """
        Function to precompute the necessary heuristics for search

        Parameters
        ----------
        seed : int, optional
            Seed for randomness
        additional_node_id_list: list[int], default=[]
            List of additional node IDs of heuristics to be computed
        """
        self._bomb_id_to_bomb = {} # dict mapping bomb ID to bomb
        self._bomb_id_to_node_id = \
            {bomb.id: node.id for node in self.graph.nodes.values() for bomb in node.bombs} # dict mapping bomb ID to node ID
        self._bomb_dependency = {} # dict mapping bomb ID to bomb it is dependent upon
        self._bomb_fuse = {} # dict mapping bomb ID to fuse length (in seconds)
        self._bomb_locations = {} # dict mapping bomb ID store bomb location
        self._bomb_seq = {} # dict mapping bomb ID to store bomb sequence
        self._h_values_dict = {} # dict mapping node ID to pre-computed h values for each node
        self._max_score = 0 # store max score 

        # Generate h values for start location
        for agent_id in self.agents:
            if self._start_node_dict[agent_id].id not in self._h_values_dict:
                self._h_values_dict[self._start_node_dict[agent_id].id] = \
                    self.compute_heuristics(self._start_node_dict[agent_id].id)

        # Calculate h values for nodes with bombs
        for bomb in self.bombs:
            self._bomb_id_to_bomb[bomb.id] = bomb
            self._bomb_dependency[bomb.id] = bomb.dependency.id if bomb.dependency else None
            self._bomb_fuse[bomb.id] = bomb.fuse
            self._bomb_locations[bomb.id] = bomb.location
            self._bomb_seq[bomb.id] = bomb.sequence
            node_id = self.bomb_id_to_node_id(bomb.id)
            if node_id not in self._h_values_dict:
                self._h_values_dict[node_id] = self.compute_heuristics(node_id)
            self._max_score += bomb.value_per_stage * bomb.num_stages

        # Compute heuristics for any additional nodes
        for node_id in additional_node_id_list:
            if node_id not in self._h_values_dict:
                self._h_values_dict[node_id] = self.compute_heuristics(node_id)

        # print("\n PRE-COMPUTED HEURISTICS \n")
        # print(f'_bomb_id_to_bomb: {self._bomb_id_to_bomb}')
        # print(f'_bomb_id_to_node_id: {self._bomb_id_to_node_id}')
        # print(f'bomb_dependency: {self.bomb_dependency}')
        # print(f'bomb_fuse: {self.bomb_fuse}')
        # print(f'bomb_locations: {self.bomb_locations}')
        # print(f'bomb_seq: {self.bomb_sequence}')
        # print(f'h_values_dict: {self._h_values_dict}')
        # print(f'max_score: {self.max_score}')

    def reset(self, *args, **kwargs):
        """
        Reset the environment to an initial state with pre-computations for search.
        """
        super().reset(*args, **kwargs)

        # Dictionary to mapping agent ID to start nodes
        self._start_node_dict = {}
        # Set start nodes
        start_node = None
        start_location = kwargs.get('start_location')
        start_regions = kwargs.get('start_regions')
        # Same start location for all agents
        if start_location:
            if self._node_grid.is_in_bounds(start_location):
                start_node = self._node_grid[start_location]
                for agent_id in self.agents:
                    self._start_node_dict[agent_id] = start_node
        if not start_node:
            start_regions = start_regions.intersection(self.graph.regions)
            if not start_regions:
                start_regions = self.graph.regions
            start_node_candidates = [
                node for node in self.graph.nodes.values() if node.region in start_regions]
            if self.same_start_loc:
                start_node = self._random.choice(start_node_candidates)
                for agent_id in self.agents:
                    self._start_node_dict[agent_id] = start_node
            else:
                if self.include_collisions:
                    for agent_id, start_node in zip(self.agents, self._random.choice(start_node_candidates, 
                                                                                     size=len(self.agents), 
                                                                                     replace=False)):
                        self._start_node_dict[agent_id] = start_node
                else:
                    for agent_id, start_node in zip(self.agents, self._random.choice(start_node_candidates, 
                                                                                     size=len(self.agents), 
                                                                                     replace=True)):
                        self._start_node_dict[agent_id] = start_node
        # Go to start node
        for agent_id, agent in self.agents.items():
            agent.go_to(self._start_node_dict[agent_id])

    def step(self, 
             curr_path_dict: dict[str, dict[str, Union[int, str, None, Color, tuple[Color]]]],
             next_path_dict: dict[str, dict[str, Union[int, str, None, Color, tuple[Color]]]]
        ) -> tuple[bool, float]:
        """
        Run one timestep of the environment's dynamics.
        Simplified with heuristics for the case of DragonBaseEnvSearch

        Parameters
        ----------
        curr_path_dict : dict[str, dict[str, Union[int, str, None, Color, tuple[Color]]]]
            Dictionary mapping from agent ID to simplified dictionary from path at the current timestep with the 
            following structure:
            {'loc': node ID, 'loc_centroid': (x, z) centroid of node, 
             'action': action to be executed after reaching stated node ID, 'bomb_id': bomb ID of bomb
             if action is 'apply_bomb_tool' else None, 'bomb_countdown_length': countdown length of bomb if action is 
             'apply_bomb_tool' else None, 'bomb_location': (x, z) location of bomb if action is 'apply_bomb_tool' else 
             None, 'bomb_sequence': bomb sequence of bomb if action is 'apply_bomb_tool' else None, 'tool_color': Color 
             of tool being applied to bomb if action is 'apply_bomb_tool' else None}

            Possible Actions
            ----------------
            'go_to': Travel to next location (node ID) in path
            'apply_bomb_tool': Apply bomb tool at bomb at current location (node ID) of specified color in bomb sequence
                               in task allocation
        next_path_dict : dict[str, dict[str, Union[int, str, None, Color, tuple[Color]]]]
            Dictionary mapping from agent ID to simplified dictionary from path at the next timestep with the 
            following structure:
            {'loc': node ID, 'loc_centroid': (x, z) centroid of node, 
             'action': action to be executed after reaching stated node ID, 'bomb_id': bomb ID of bomb
             if action is 'apply_bomb_tool' else None, 'bomb_countdown_length': countdown length of bomb if action is 
             'apply_bomb_tool' else None, 'bomb_location': (x, z) location of bomb if action is 'apply_bomb_tool' else 
             None, 'bomb_sequence': bomb sequence of bomb if action is 'apply_bomb_tool' else None, 'tool_color': Color 
             of tool being applied to bomb if action is 'apply_bomb_tool' else None}

            Possible Actions
            ----------------
            'go_to': Travel to next location (node ID) in path
            'apply_bomb_tool': Apply bomb tool at bomb at current location (node ID) of specified color in bomb sequence
                               in task allocation

        Returns
        -------
        done : bool
            Termination flag
        score : float
            Current score
        """
        # List of current bombs
        current_bombs = list(self.bombs)

        # Iterate over agents
        for agent_id, path_dict in curr_path_dict.items():
            agent = self.agents[agent_id]
            # Handle movement actions
            if path_dict['action'] == 'go_to':
                # Go to next node obtained from next_path_dict
                node_id = next_path_dict[agent_id]['loc']
                agent.go_to(self.graph.nodes[node_id])
            # Ensure that agent looking at the intended bomb at its current node
            elif path_dict['action'] == 'apply_bomb_tool':
                # print(f"agent bomb ID from path dict: {path_dict['bomb_id']}")
                # print(f'agent {agent.id} node ID now: {agent.node.id}')
                # print(f'agent {agent.id} bomb now: {agent.bomb}')
                # print(f'agent {agent.id} bomb ID now: {agent.bomb.id if agent.bomb else None}')
                # print(f'agent {agent.id} is at node with node ID: {agent.node.id} with bombs {agent.node.bombs}')
                # print(f'agent {agent.id} bomb remaining color sequence: {agent.bomb.sequence if agent.bomb else None}')
                # Check if there are bombs on this node
                if agent.bomb:
                    # Boolean to track if node has the bomb agent intends to defuse
                    has_bomb = True
                    # Track bomb ID agent currently is viewing
                    first_bomb_id = agent.bomb.id
                    # Cycle to the next bomb at the current node till intended bomb is found         
                    while agent.bomb.id != path_dict['bomb_id']:
                        agent.find_next_bomb()
                        # Agent cycled back to the bomb with bomb ID it first saw 
                        # Node does not have the bomb agent intends to defuse 
                        # Bomb must have exploded due to fuse/countdown
                        # Should not occur when bomb_explode_terminate == True
                        if agent.bomb.id == first_bomb_id:
                            has_bomb = False
                            # print(f"node {path_dict['loc']} does not have bomb {path_dict['bomb_id']}")
                            break
                    # Execute action for next agent
                    if not has_bomb:
                        # print(f"executing next agents actions")
                        continue
                # No bombs on this node (bomb must have exploded due to fuse/countdown)
                # Should not occur when bomb_explode_terminate == True
                else:
                    # print(f"there are no bombs at {path_dict['loc']}")
                    # print(f"executing next agents actions")
                    # Execute action for next agent
                    continue
                # print(f'agent {agent.id} bomb ID after cycling: {agent.bomb.id}')
                # Obtain bomb that the agent is defusing
                defuse_bomb = agent.bomb
                # print(f'agent {agent_id} tool_remaining_uses: {self.agents[agent_id].tool_remaining_uses}')
                # Obtain required tool 
                required_tool = agent.get_tool_from_inventory(Tool.from_color(path_dict['tool_color']))
                # End episode if agent doesn't have the required tool 
                # Should only happen rarely with unlucky random tool allocation
                if required_tool == None:
                    # print(f'agent {agent_id} does not have tool with color {path_dict["tool_color"]}')
                    return True, self.score
                # Apply tool to bomb
                defuse_bomb.apply_tool(required_tool)
                # print(f'Agent {agent_id} applied tool {required_tool}')
                # Terminate episode if bomb has exploded
                if defuse_bomb.state == Bomb.BombState.exploded:
                    # print(f'Bomb with bomb id {defuse_bomb.id} with remaining bomb sequence {defuse_bomb.sequence} has EXPLODED.')
                    # print(f'Agent {agent_id} was using tool {required_tool}')
                    return True, self.score
                # print(f'Bomb with bomb id {defuse_bomb.id} with remaining bomb sequence {defuse_bomb.sequence} has been DEFUSED')

                # Only check for other bombs exploding if bomb_explode_terminate == True
                if self.bomb_explode_terminate:
                    # print("\n CHECK FOR BOMB EXPLOSIONS DUE TO DEPENDECIES IN TERMINAL BOMBS \n")
                    for bomb in self._terminal_bombs:
                        # print(f'bomb state of bomb with bomb id {bomb.id}: {bomb.state}')
                        # print(f'bomb countdown of bomb with bomb id {bomb.id}: {bomb._countdown}')
                        if bomb.state != Bomb.BombState.defused:
                            # print(f'Bomb with bomb id {bomb.id} with remaining bomb sequence {bomb.sequence} has is in terminal bomb but not DEFUSED.')
                            return True, self.score
                    # print("\n CHECK FOR BOMB EXPLOSIONS DUE TO DEPENDECIES IN TERMINAL BOMBS END \n")

        # Execute one timestep
        self.tick()

        # print("\n CHECK FOR BOMB EXPLOSIONS DUE TO DEPENDECIES IN TERMINAL BOMBS AFTER TICK \n")
        # Only check for other bombs exploding if bomb_explode_terminate == True
        if self.bomb_explode_terminate:
            # Check for bombs that explode due to countdown
            for bomb in current_bombs:
                # print(f'bomb state of bomb with bomb id {bomb.id}: {bomb.state}')
                # print(f'bomb countdown of bomb with bomb id {bomb.id}: {bomb._countdown}')
                if bomb.state == Bomb.BombState.exploded:
                    # print(f'Bomb with bomb id {bomb.id} with remaining bomb sequence {bomb.sequence} has EXPLODED.')
                    return True, self.score
        # print("\n CHECK FOR BOMB EXPLOSIONS DUE TO DEPENDECIES IN TERMINAL BOMBS END AFTER TICK \n")

        return self._get_done()['__all__'], self.score