# -*- coding: utf-8 -*-
"""
.. module:: object_state_change
   :platform: Linux, Windows, OSX
   :synopsis: Parser for Item State Change messages

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a parser for Object State Change messages
"""

from ....messages import ObjectStateChange, Attributes
from ..message_types import MessageType, MessageSubtype
from ..bus_header import BusHeaderParser
from ..message_header import MessageHeaderParser
from ..parser_exceptions import MissingHeaderException

import MinecraftElements

import logging


class ObjectStateChangeParser:
    """
    A class for parsing item state change messages from MQTT bus.

    MQTT Message Fields
    -------------------


    MQTT Blockage Fields
    --------------------

    """

    topic = "object/state/change"
    MessageClass = ObjectStateChange

    msg_type = MessageType.simulator_event
    msg_subtype = MessageSubtype.Event_ObjectStateChange
    alternatives = []
    
    # Class-level logger
    logger = logging.getLogger("ObjectStateChangeParser")


    @classmethod
    def parse(cls, json_message):
        """
        Convert the a Python dictionary format of the message to a BlockageList
        instance.

        Arguments
        ---------
        json_message : dictionary
            Dictionary representation of the message received from the MQTT bus
        """

        # Make sure that there's a "header" and "msg" field in the message
        if not "header" in json_message.keys() or not "msg" in json_message.keys():
            raise MissingHeaderException(json_message)

        # Parse the header and message header
        busHeader = BusHeaderParser.parse(json_message["header"])
        messageHeader = MessageHeaderParser.parse(json_message["msg"])

        # Check to see if this parser can handle this message type, if not, 
        # then return None
        if busHeader.message_type != MessageType.simulator_event:
            return None
        if messageHeader.sub_type != MessageSubtype.Event_ObjectStateChange:
            return None

        # Parse the data
        data = json_message["data"]

        message = ObjectStateChange(**data)
        message.addHeader("header", busHeader)
        message.addHeader("msg", messageHeader)

        # Add all the attributes -- add the attributes that have changed
        # first, then add the current attributes
        for name, change in data["changedAttributes"].items():
            message.add_attribute(name, change[1], change[0])

        for name, attribute in data["currAttributes"].items():
            if not name in message.get_attribute_names():
                # Todo: issue a warning that there is no previous value
                message.add_attribute(name, attribute)

        message.finalize()

        return message