# -*- coding: utf-8 -*-
"""
.. module:: environment_removed_list
   :platform: Linux, Windows, OSX
   :synopsis: Parser for Environment Removed List messages

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a parser for Environment Removed List messages
"""

from ....messages import EnvironmentRemovedList, EnvironmentObject
from ..message_types import MessageType, MessageSubtype
from ..bus_header import BusHeaderParser
from ..message_header import MessageHeaderParser
from ..parser_exceptions import MissingHeaderException

import MinecraftElements

import logging


class EnvironmentRemovedListParser:
    """
    A class for parsing environment created list messages from MQTT bus.

    MQTT Message Fields
    -------------------


    MQTT Blockage Fields
    --------------------

    """

    topic = "environment/removed/list"
    MessageClass = EnvironmentRemovedList

    msg_type = MessageType.simulator_event
    msg_subtype = MessageSubtype.Event_EnvironmentRemovedList
    alternatives = []
    
    # Class-level logger
    logger = logging.getLogger("EnvironmentRemovedListParser")


    @classmethod
    def parse(cls, json_message):
        """
        Convert the a Python dictionary format of the message to a BlockageList
        instance.

        Arguments
        ---------
        json_message : dictionary
            Dictionary representation of the message received from the MQTT bus
        """

        # Make sure that there's a "header" and "msg" field in the message
        if not "header" in json_message.keys() or not "msg" in json_message.keys():
            raise MissingHeaderException(json_message)

        # Parse the header and message header
        busHeader = BusHeaderParser.parse(json_message["header"])
        messageHeader = MessageHeaderParser.parse(json_message["msg"])

        # Check to see if this parser can handle this message type, if not, 
        # then return None
        if busHeader.message_type != MessageType.simulator_event:
            return None
        if messageHeader.sub_type != MessageSubtype.Event_EnvironmentRemovedList:
            return None

        # Parse the data
        data = json_message["data"]

        message = EnvironmentRemovedList(**data)
        message.addHeader("header", busHeader)
        message.addHeader("msg", messageHeader)

        # Parse all the environment objects in the list
        for object_data in data["list"]:
            location = (object_data["x"], 
                        object_data["y"], 
                        object_data["z"])

            object_id = object_data["id"]
            object_type = object_data["type"]
            elapsed_milliseconds = object_data["elapsed_milliseconds"]
            triggering_entity = object_data["triggering_entity"]
            attributes = object_data.get('currentAttributes', object_data.get('currAttributes', None))

            environment_object = EnvironmentObject(object_id,
                                                   location,
                                                   object_type,
                                                   elapsed_milliseconds,
                                                   triggering_entity,
                                                   **attributes)

            message.add_object(environment_object)

        message.finalize()

        return message