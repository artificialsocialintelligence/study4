# -*- coding: utf-8 -*-
"""
.. module:: stage_transition
   :platform: Linux, Windows, OSX
   :synopsis: Message class encapsulating Stage Transition messages

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a class encapsulating stage transition messages.
"""

import json

from ..message_exceptions import (
    MalformedMessageCreationException, 
    MissingMessageArgumentException, 
    ImmutableAttributeException
)
from ..base_message import BaseMessage

import enum

class MissionStage(enum.Enum):
    """
    Enumeration of possible mission stages
    """

    ShopStage = "SHOP_STAGE"
    FieldStage = "FIELD_STAGE"
    ReconStage = "RECON_STAGE"


class MissionStageTransition(BaseMessage):
    """
    A class encapsulating Stage Transition messages.

    Attributes
    ----------
    mission_stage : string
        Which stage has been transitioned to
    transitionsToShop : int
        The number of times the players have transitioned to the SHOP phase
    team_budget : int
        The current team budget
    """


    def __init__(self, **kwargs):
        """
        """

        BaseMessage.__init__(self, **kwargs)

        # Check to see if the necessary arguments have been passed, raise an 
        # exception if one is missing
        for arg_name in ['mission_stage', 'transitionsToShop', 'team_budget']:
            if not arg_name in kwargs:
                raise MissingMessageArgumentException(str(self), 
                                                    arg_name) from None

        self._mission_stage = MissionStage(kwargs['mission_stage'])
        self._transitionsToShop = int(kwargs['transitionsToShop'])
        self._team_budget = int(kwargs['team_budget'])


    def __str__(self):
        """
        String representation of the message.

        Returns
        -------
        string
            Class name of the message (i.e., 'ItemUsedEvent')
        """

        return self.__class__.__name__


    @property
    def mission_stage(self):
        return self._mission_stage

    @property
    def transitionsToShop(self):
        return self._transitionsToShop

    @property
    def team_budget(self):
        return self._team_budget


    def toDict(self):
        """
        Generates a dictionary representation of the message.  Message
        information is contained in a dictionary under the key "data".
        Additional named headers may also be present.

        Returns
        -------
        dict
            A dictionary representation of the MissionStageTransition.
        """

        jsonDict = BaseMessage.toDict(self)

        # Check to see if a "data" is in the dictionary, and add if not
        # Note that headers should have been added in jsonDict, as well as
        # common message data.
        if not "data" in jsonDict:
            jsonDict["data"] = {}

        # Add the message data
        jsonDict['mission_stage'] = self.mission_stage.value
        jsonDict['transitionsToShop'] = self.transitionsToShop
        jsonDict['team_budget'] = self.team_budget

        return jsonDict


    def toJson(self):
        """
        Generates a JSON representation of the message.  Message information is
        contained in a JSON object under the key "data".  Additional named
        headers may also be present.

        Returns
        -------
        string
            A JSON string mapping header names to a JSON representation of the
            Mission Stage Transition message.
        """

        return json.dumps(self.toDict())
