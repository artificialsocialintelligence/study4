# -*- coding: utf-8 -*-
"""
.. module:: attributes
   :platform: Linux, Windows, OSX
   :synopsis: Classes for representing attributes

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a classes providing immutable mapping of attribute names to 
vlaues.
"""

###import json
from collections import namedtuple

###from ..message_exceptions import (
###    MalformedMessageCreationException, 
###    MissingMessageArgumentException, 
###    ImmutableAttributeException
###)


# A simple tuple to handle changed attributes
ChangedAttributeValues = namedtuple("ChangedAttributeValues", "previous current")


class Attributes(object):
    """
    A simple non-mutable dictionary mapping a string attribute name to a value
    """

    def __init__(self):
        """
        Create a new ItemAttributes instance
        """

        self.__attributes = dict()

        self.__finalized = False


    def add_attribute(self, attribute, value):
        """
        Add an attribute to this instance
        """

        if self.__finalized:
            # TODO:  Issue a warning that this object has been finalized
            return

        self.__attributes[attribute] = value


    def get_attribute(self, attribute_name):
        """
        Get an attribute by the given name

        Arguments
        ---------
        attribute_name : string
            Name of the attribute to get
        """

        if not attribute_name in self.__attributes:
            return None

        return self.__attributes[attribute_name] 


    def finalize(self):
        """
        """

        self.__finalized = True


###    def __getattr__(self, item):
###        """
###        Returns an object-specific attribute
###
###        Arguments
###        ---------
###        item : string
###            Attribute to return
###        """
###
###        # TODO:  Set up a logger to log a warning that the attribute is not
###        #        available
###        if not item in self.__attributes:
###            # WARNING GOES HERE
###            return None
###
###        return self.__attributes[item]


###    def __getitem__(self, item):
###        """
###        Returns an object-specific attribute
###
###        Arguments
###        ---------
###        item : string
###            Attribute to return
###        """
###
###        return self.__attributes[item]



###    def __setattr__(self, item, value):
###        """
###        """
###
### 
###        # TODO:  Raise a warning that this is not allowed
###        return


###    def __setitem__(self, item, value):
###        """
###        """
###
###        # TODO:  Raise a warning that this is not allowed
###        return


    def get_attribute_names(self):
        """
        Return a list of the current attributes of the object
        """

        return list(self.__attributes.keys())


    def toDict(self):
        """
        Generates a dictionary representation of the ItemAttributes

        Returns
        -------
        dict
            A dictionary representation of the ItemAttributes.
        """

        attributeDict = dict()
        attributeDict['currentAttributes'] = dict()

        for name, value in self.__attributes.items():
            attributeDict['currentAttributes'][name] = value

        return attributeDict