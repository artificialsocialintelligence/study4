# -*- coding: utf-8 -*-
"""
.. module:: item_state_change
   :platform: Linux, Windows, OSX
   :synopsis: Message class encapsulating item state change messages

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a class encapsulating Item State Change messages.
"""

import json
from collections import namedtuple

from .attributes import Attributes, ChangedAttributeValues

from ..message_exceptions import (
    MalformedMessageCreationException, 
    MissingMessageArgumentException, 
    ImmutableAttributeException
)
from ..base_message import BaseMessage


class ItemStateChange(BaseMessage):
    """
    A class encapsulating ItemStateChange messages.

    Attributes
    ----------
    mission_timer : 
        Current mission timer
    elapsed_milliseconds : int
        Number of milliseconds since mission start
    item_id : string
        Unique identifier of the item
    item_name : string
        Name of the item
    owner : string
        ID of the owner of the item
    changedAttributes : ItemAttributes
        Mapping of changed attributes
    currAttributes : ItemAttributes
        Mapping of the current attributes
    """


    def __init__(self, **kwargs):
        """
        """
        
        BaseMessage.__init__(self, **kwargs)

        # TODO: Ensure that required arguments are provided

        self._item_id = kwargs['item_id']
        self._item_name = kwargs['item_name']
        self._owner = kwargs['owner']
        self._changed_attributes = Attributes()
        self._current_attributes = Attributes()


    def __str__(self):
        """
        String representation of the message.

        Returns
        -------
        string
            Class name of the message
        """

        return self.__class__.__name__

    @property
    def item_id(self):
        return self._item_id

    @property
    def item_name(self):
        return self._item_name

    @property
    def owner(self):
        return self._owner

    @property
    def changed_attributes(self):
        return self._changed_attributes

    @property
    def current_attributes(self):
        return self._current_attributes


    def add_attribute(self, name, current_value, previous_value=None):
        """
        Add an attribute to the ItemStateChange message.  If `previous_value`
        is given as `None`, then the attribute is only added to the current
        attributes.

        Arguments
        ---------
        name : string
            Name of the attribute to add
        current_value
            Value of the attribute after the change
        previous_value
            Value of the attribute prior to the change
        """

        self._current_attributes.add_attribute(name, current_value)

        # Add the attribute to the changed attributes, if suitable.  Otherwise,
        # issue a warning that no attribute was provided
        if previous_value is not None:
            self._changed_attributes.add_attribute(name, ChangedAttributeValues(previous_value, current_value))
        else:
            # TODO: Raise a warning
            pass


    def finalize(self):
        """
        """

        self._current_attributes.finalize()
        self._changed_attributes.finalize()


    def get_attribute_names(self):
        """
        """

        return self._current_attributes.get_attribute_names()


    def toDict(self):
        """
        Generates a dictionary representation of the DoorEvent message.  
        DoorEvent information is contained in a dictionary under the key "data".
        Additional named headers may also be present.

        Returns
        -------
        dict
            A dictionary representation of the DoorEvent.
        """

        jsonDict = BaseMessage.toDict(self)
    
        # Check to see if a "data" is in the dictionary, and add if not
        # Note that headers should have been added in jsonDict, as well as
        # common message data.
        if not "data" in jsonDict:
            jsonDict["data"] = {}

        jsonDict['data']['item_id'] = self.item_id
        jsonDict['data']['item_name'] = self.item_name
        jsonDict['data']['owner'] = self.owner
        jsonDict['data']['changedAttributes'] = { name: list(value) for name,value in self.changed_attributes.toDict().items() }
        jsonDict['data']['currAttributes'] = self.current_attributes.toDict()


        return jsonDict


    def toJson(self):
        """
        Generates a JSON representation of the DoorEvent message.  DoorEvent
        information is contained in a JSON object under the key "data".
        Additional named headers may also be present.

        Returns
        -------
        string
            A JSON string mapping header names to a JSON representation of the
            DoorEvent message.
        """

        return json.dumps(self.toDict())
