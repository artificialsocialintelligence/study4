# -*- coding: utf-8 -*-
"""
.. module:: object_state_change
   :platform: Linux, Windows, OSX
   :synopsis: Message class encapsulating object state change messages

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a class encapsulating Object State Change messages.
"""

import json

from .attributes import Attributes, ChangedAttributeValues

from ..message_exceptions import (
    MalformedMessageCreationException, 
    MissingMessageArgumentException, 
    ImmutableAttributeException
)
from ..base_message import BaseMessage


class ObjectStateChange(BaseMessage):
    """
    A class encapsulating ObjectStateChange messages.

    Attributes
    ----------
    mission_timer : 
        Current mission timer
    elapsed_milliseconds : int
        Number of milliseconds since mission start
    id : string
        Unique identifier of the object
    type : string
        The type of object that changed states
    triggering_entity : string
        ID of the entity that triggered the change
    location : Tuple(float)
        (x,y,z) location of the object
    x : int
        x-position of the object
    y : int
        y-position of the object
    z : int
        z-position of the object
    changed_attributes : ItemAttributes
        Mapping of changed attributes
    current_attributes : ItemAttributes
        Mapping of the current attributes
    """


    def __init__(self, **kwargs):
        """
        """
        
        BaseMessage.__init__(self, **kwargs)

        # TODO: Ensure that required arguments are provided

        self._id = kwargs['id']
        self._type = kwargs['type']

        location = kwargs.get('location', None)
        if location is None:
            try:
                location = (kwargs['x'],
                            kwargs['y'],
                            kwargs['z'])
            except KeyError:
                raise MissingMessageArgumentException(str(self),
                                                      'location') from None

        # Location needs to be able to be coerced into a tuple of ints.  Raise 
        # an exception if not possible
        try:
            self._location = tuple([float(x) for x in location][:3])
        except:
            raise MalformedMessageCreationException(str(self), 'location', 
                                                    kwargs['location']) from None

        self._changed_attributes = Attributes()
        self._current_attributes = Attributes()

        self._triggering_entity = kwargs.get('triggering_entity', None)


    def __str__(self):
        """
        String representation of the message.

        Returns
        -------
        string
            Class name of the message
        """

        return self.__class__.__name__

    @property
    def id(self):
        return self._id

    @property
    def type(self):
        return self._type

    @property
    def triggering_entity(self):
        return self._triggering_entity    

    @property
    def location(self):
        return self._location

    @property
    def x(self):
        return self._location[0]

    @property
    def y(self):
        return self._location[1]

    @property
    def z(self):
        return self._location[2]

    @property
    def changed_attributes(self):
        return self._changed_attributes

    @property
    def current_attributes(self):
        return self._current_attributes


    def add_attribute(self, name, current_value, previous_value=None):
        """
        Add an attribute to the ItemStateChange message.  If `previous_value`
        is given as `None`, then the attribute is only added to the current
        attributes.

        Arguments
        ---------
        name : string
            Name of the attribute to add
        current_value
            Value of the attribute after the change
        previous_value
            Value of the attribute prior to the change
        """

        self._current_attributes.add_attribute(name, current_value)

        # Add the attribute to the changed attributes, if suitable.  Otherwise,
        # issue a warning that no attribute was provided
        if previous_value is not None:
            self._changed_attributes.add_attribute(name, ChangedAttributeValues(previous_value, current_value))
        else:
            # TODO: Raise a warning
            pass


    def finalize(self):
        """
        """

        self._current_attributes.finalize()
        self._changed_attributes.finalize()


    def get_attribute_names(self):
        """
        """

        return self._current_attributes.get_attribute_names()


    def toDict(self):
        """
        Generates a dictionary representation of the DoorEvent message.  
        DoorEvent information is contained in a dictionary under the key "data".
        Additional named headers may also be present.

        Returns
        -------
        dict
            A dictionary representation of the DoorEvent.
        """

        jsonDict = BaseMessage.toDict(self)
    
        # Check to see if a "data" is in the dictionary, and add if not
        # Note that headers should have been added in jsonDict, as well as
        # common message data.
        if not "data" in jsonDict:
            jsonDict["data"] = {}

        jsonDict['data']['id'] = self.id
        jsonDict['data']['type'] = self.type
        jsonDict['data']['x'] = self.x
        jsonDict['data']['y'] = self.y
        jsonDict['data']['z'] = self.z
        jsonDict['data']['changedAttributes'] = { name: list(value) for name,value in self.changed_attributes.toDict().items() }
        jsonDict['data']['currAttributes'] = self.current_attributes.toDict()


        return jsonDict


    def toJson(self):
        """
        Generates a JSON representation of the DoorEvent message.  DoorEvent
        information is contained in a JSON object under the key "data".
        Additional named headers may also be present.

        Returns
        -------
        string
            A JSON string mapping header names to a JSON representation of the
            DoorEvent message.
        """

        return json.dumps(self.toDict())
