# -*- coding: utf-8 -*-
"""
.. module:: cornell_dyad_compliance
   :platform: Linux, Windows, OSX
   :synopsis: Message class encapsulating Dialog Agent messages

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a class encapsulating Study 4 Dyad Compliance messages.
"""

import json

from ...message_exceptions import (
    MalformedMessageCreationException, 
    MissingMessageArgumentException, 
    ImmutableAttributeException
)
from ...base_message import BaseMessage


class CornellDyadComplianceRequest(BaseMessage):
    """
    A class encapsulating Study 4 Dyad Compliance messages for requests.

    Attributes
    ----------
    bomb_id : str
        Identifier of the bomb associated with the request
    requester : str
    request_stage : str
    remaining_sequence : str
    requested_tool : str
    request_time : int
    request_stage : str
    """


    def __init__(self, **kwargs):
        """
        """

        BaseMessage.__init__(self, **kwargs)

        # TODO:  Put in some checks for keyword arguments
        self._bomb_id = kwargs.get("bomb_id", "")
        self._requester = kwargs.get("requester", "")
        self._request_stage = kwargs.get("request_stage", "")
        self._remaining_sequence = kwargs.get("remaining_sequence", "[]")
        self._requested_tool = kwargs.get("requested_tool", "")
        self._request_time = kwargs.get("request_time", -1)


    def __str__(self):
        """
        String representation of the message.

        Returns
        -------
        string
            Class name of the message (i.e., 'CornellDyadComplianceRequest')
        """

        return self.__class__.__name__


    @property
    def bomb_id(self):
        return self._bomb_id

    @property
    def requester(self):
        return self._requester

    @property
    def request_stage(self):
        return self._request_stage

    @property
    def remaining_sequence(self):
        return self._remaining_sequence

    @property
    def requested_tool(self):
        return self._requested_tool

    @property
    def request_time(self):
        return self._request_time


    def toDict(self):
        """
        Generates a dictionary representation of the message.  Message
        information is contained in a dictionary under the key "data".
        Additional named headers may also be present.

        Returns
        -------
        dict
            A dictionary representation of the ItemUsedEvent.
        """

        jsonDict = BaseMessage.toDict(self)

        # Check to see if a "data" is in the dictionary, and add if not
        # Note that headers should have been added in jsonDict, as well as
        # common message data.
        if not "data" in jsonDict:
            jsonDict["data"] = {}

        # Add the message data
        jsonDict["data"]["bomb_id"] = self.bomb_id
        jsonDict["data"]["requester"] = self.requester
        jsonDict["data"]["request_stage"] = self.request_stage
        jsonDict["data"]["remaining_sequence"] = self.remaining_sequence
        jsonDict["data"]["requested_tool"] = self.requested_tool
        jsonDict["data"]["request_time"] = self.request_time

        return jsonDict


    def toJson(self):
        """
        Generates a JSON representation of the message.  Message information is
        contained in a JSON object under the key "data".  Additional named
        headers may also be present.

        Returns
        -------
        string
            A JSON string mapping header names to a JSON representation of the
            ItemUsedEvent message.
        """

        return json.dumps(self.toDict())



class CornellDyadComplianceResponse(BaseMessage):
    """
    A class encapsulating Study 4 Dyad Compliance messages for requests.

    Attributes
    ----------
    bomb_id : str
        Identifier of the bomb associated with the request
    requester : str
    request_stage : str
    remaining_sequence : str
    requested_tool : str
    request_time : int
    responder : str
    respond_time : int
    respond_stage : str
    response_tool : str
    response_time_duration : int
    """


    def __init__(self, **kwargs):
        """
        """

        BaseMessage.__init__(self, **kwargs)

        self._bomb_id = kwargs.get("bomb_id", "")
        self._requester = kwargs.get("requester", "")
        self._request_stage = kwargs.get("request_stage", "")
        self._remaining_sequence = kwargs.get("remaining_sequence", "[]")
        self._requested_tool = kwargs.get("requested_tool", "")
        self._request_time = kwargs.get("request_time", -1)
        self._responder = kwargs.get("responder", "")
        self._response = kwargs.get("response", {})
        self._respond_time = kwargs.get("respond_time", -1)
        self._respond_stage = kwargs.get("respond_stage", "")
        self._response_tool = kwargs.get("response_tool", "")
        self._response_time_duration = kwargs.get("response_time_duration", -1)


    def __str__(self):
        """
        String representation of the message.

        Returns
        -------
        string
            Class name of the message (i.e., 'CornellDyadComplianceResponse')
        """

        return self.__class__.__name__


    @property
    def bomb_id(self):
        return self._bomb_id

    @property
    def requester(self):
        return self._requester

    @property
    def request_stage(self):
        return self._request_stage

    @property
    def remaining_sequence(self):
        return self._remaining_sequence

    @property
    def requested_tool(self):
        return self._requested_tool

    @property
    def request_time(self):
        return self._request_time

    @property
    def responder(self):
        return self._responder

    @property
    def response(self):
        return self._response

    @property
    def respond_time(self):
        return self._respond_time

    @property
    def respond_stage(self):
        return self._respond_stage

    @property
    def response_tool(self):
        return self._response_tool

    @property
    def response_time_duration(self):
        return self._response_time_duration    


    def toDict(self):
        """
        Generates a dictionary representation of the message.  Message
        information is contained in a dictionary under the key "data".
        Additional named headers may also be present.

        Returns
        -------
        dict
            A dictionary representation of the ItemUsedEvent.
        """

        jsonDict = BaseMessage.toDict(self)

        # Check to see if a "data" is in the dictionary, and add if not
        # Note that headers should have been added in jsonDict, as well as
        # common message data.
        if not "data" in jsonDict:
            jsonDict["data"] = {}

        # Add the message data
        jsonDict["data"]["bomb_id"] = self.bomb_id
        jsonDict["data"]["requester"] = self.requester
        jsonDict["data"]["request_stage"] = self.request_stage
        jsonDict["data"]["remaining_sequence"] = self.remaining_sequence
        jsonDict["data"]["requested_tool"] = self.requested_tool
        jsonDict["data"]["request_time"] = self.request_time        
        jsonDict["data"]["responder"] = self.responder
        jsonDict["data"]["response"] = self.response
        jsonDict["data"]["respond_time"] = self.respond_time
        jsonDict["data"]["respond_stage"] = self.respond_stage
        jsonDict["data"]["response_tool"] = self.response_tool
        jsonDict["data"]["response_time_duration"] = self.response_time_duration


        return jsonDict


    def toJson(self):
        """
        Generates a JSON representation of the message.  Message information is
        contained in a JSON object under the key "data".  Additional named
        headers may also be present.

        Returns
        -------
        string
            A JSON string mapping header names to a JSON representation of the
            ItemUsedEvent message.
        """

        return json.dumps(self.toDict())




class CornellDyadComplianceSummary(BaseMessage):
    """
    A class encapsulating Study 4 Dyad Compliance summaries (at stage transitions.

    Attributes
    ----------
    complied_dyad_raw : dict
        raw number of request-response pairs by each dyad
    complied_dyad_raw_balance : dict
        number of request-response pairs by each dyad divided by the number of
        all request-response pairs until time.
    dyad_compliance_by_requester_reqs : dict
        number of request-response pairs by each dyad divided by the number of
        requests made by the requester in the dyad
    res_player_compliance_by_all_reqs : dict
        number of requests fulfilled by player divided by the number of all
        requests made until time.
    """


    def __init__(self, **kwargs):
        """
        """

        BaseMessage.__init__(self, **kwargs)

        # TODO: Validate
        self._complied_dyad_raw = kwargs.get("complied_dyad_raw", {})
        self._complied_dyad_raw_balance = kwargs.get("complied_dyad_raw_balance", {})
        self._dyad_complience_by_requester_reqs = kwargs.get("compliance_by_requester_reqs", {})
        self._res_player_compliance_by_all_reqs = kwargs.get("res_player_compliance_by_all_reqs", {})


    def __str__(self):
        """
        String representation of the message.

        Returns
        -------
        string
            Class name of the message (i.e., 'DialogAgent')
        """

        return self.__class__.__name__

    @property
    def complied_dyad_raw(self):
        return self._complied_dyad_raw

    @property
    def complied_dyad_raw_balance(self):
        return self._complied_dyad_raw_balance

    @property
    def dyad_complience_by_requester_reqs(self):
        return self._dyad_complience_by_requester_reqs

    @property
    def res_player_compliance_by_all_reqs(self):
        return self._res_player_compliance_by_all_reqs
    
    
    def toDict(self):
        """
        Generates a dictionary representation of the message.  Message
        information is contained in a dictionary under the key "data".
        Additional named headers may also be present.

        Returns
        -------
        dict
            A dictionary representation of the ItemUsedEvent.
        """

        jsonDict = BaseMessage.toDict(self)

        # Check to see if a "data" is in the dictionary, and add if not
        # Note that headers should have been added in jsonDict, as well as
        # common message data.
        if not "data" in jsonDict:
            jsonDict["data"] = {}

        # Add the message data
        jsonDict["data"]["complied_dyad_raw"] = self.complied_dyad_raw
        jsonDict["data"]["complied_dyad_raw_balance"] = self.complied_dyad_raw_balance
        jsonDict["data"]["dyad_complience_by_requester_reqs"] = self.dyad_complience_by_requester_reqs
        jsonDict["data"]["res_player_compliance_by_all_reqs"] = self.res_player_compliance_by_all_reqs


        return jsonDict


    def toJson(self):
        """
        Generates a JSON representation of the message.  Message information is
        contained in a JSON object under the key "data".  Additional named
        headers may also be present.

        Returns
        -------
        string
            A JSON string mapping header names to a JSON representation of the
            ItemUsedEvent message.
        """

        return json.dumps(self.toDict())