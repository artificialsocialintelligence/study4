# -*- coding: utf-8 -*-
"""
.. module:: ui_click
   :platform: Linux, Windows, OSX
   :synopsis: Message class encapsulating User Interface click messages

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a class encapsulating UI click messages.
"""

import json

from ..message_exceptions import (
    MalformedMessageCreationException, 
    MissingMessageArgumentException, 
    ImmutableAttributeException
)
from ..base_message import BaseMessage


class UI_Click(BaseMessage):
    """
    A class encapsulating UI Click messages.

    Attributes
    ----------
    participant_id : string
    	the unique id of the participant in the study
    element_id : string
    	the unique id of the element clicked in the UI
    parent_id : string
    	the unique id of the parent of the element clicked in the UI
    type : enum
    	the type of element clicked
    x : number
    	the x location of the click
    y : number
    	the y location of the click
    additional_info : dictionary
    	Additional information about the click event
    """


    def __init__(self, **kwargs):
        """

        """


        BaseMessage.__init__(self, **kwargs)


        # TODO: Validate that all the keys are present
        self._participant_id = kwargs['participant_id']
        self._element_id = kwargs['element_id']
        self._parent_id = kwargs['parent_id']
        self._type = kwargs['type']
        self._x = kwargs['x']
        self._y = kwargs['y']
        self._additional_info = kwargs.get("additional_info", {})
###        self._meta_action = kwargs['meta_action']
###        self._call_sign_code = kwargs['call_sign_code']


    def __str__(self):
        """
        String representation of the message.

        Returns
        -------
        string
            Class name of the message (i.e., 'ItemUsedEvent')
        """

        return self.__class__.__name__


    @property
    def participant_id(self):
    	return self._participant_id

    @property
    def element_id(self):
    	return self._element_id

    @property
    def parent_id(self):
    	return self._parent_id

    @property
    def type(self):
    	return self._type

    @property
    def x(self):
    	return self._x

    @property
    def y(self):
    	return self._y

    @property
    def additional_info(self):
        return self._additional_info
    

###    @property
###    def meta_action(self):
###    	return self._meta_action
###
###    @property
###    def call_sign_code(self):
###    	return self._call_sign_code


    def toDict(self):
        """
        Generates a dictionary representation of the message.  Message
        information is contained in a dictionary under the key "data".
        Additional named headers may also be present.

        Returns
        -------
        dict
            A dictionary representation of the ItemUsedEvent.
        """

        jsonDict = BaseMessage.toDict(self)

        # Check to see if a "data" is in the dictionary, and add if not
        # Note that headers should have been added in jsonDict, as well as
        # common message data.
        if not "data" in jsonDict:
            jsonDict["data"] = {}

        # Add the message data
        jsonDict['participant_id'] = self.participant_id
        jsonDict['element_id'] = self.element_id
        jsonDict['parent_id'] = self.parent_id
        jsonDict['type'] = self.type
        jsonDict['x'] = self.x
        jsonDict['y'] = self.y
        jsonDict['additional_info'] = self.additional_info
###        jsonDict['additional_info']['meta_action'] = self.meta_action
###        jsonDict['additional_info']['call_sign_code'] = self.call_sign_code

        return jsonDict


    def toJson(self):
        """
        Generates a JSON representation of the message.  Message information is
        contained in a JSON object under the key "data".  Additional named
        headers may also be present.

        Returns
        -------
        string
            A JSON string mapping header names to a JSON representation of the
            ItemUsedEvent message.
        """

        return json.dumps(self.toDict())
