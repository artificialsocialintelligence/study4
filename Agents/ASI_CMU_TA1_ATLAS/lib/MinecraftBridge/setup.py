import setuptools
from setup_utils import filter_deps

with open("README.md", "r") as readme_file:
	long_description = readme_file.read()

extras = {
    'MinecraftElements': filter_deps([
        'MinecraftElements @ git+https://gitlab.com/cmu_aart/asist/MinecraftElements@v0.5.0'
    ])
}

setup_requirements = [ 'setuptools_scm' ]


install_deps = [ 'ciso8601',
				 'paho-mqtt',
				 'setuptools_scm'
               ]


classifiers = [
		"Programming Language :: Python :: 3",
		"License :: OSI Approved :: MIT License",
		"operating System :: OS Independent",
	]

setuptools.setup(
	name="MinecraftBridge",
	version="2.0.12",
	author="AART-Lab, CMU-RI",
	author_email="danahugh@andrew.cmu.edu",
	description="Bridge to multiple interfaces to Minecraft",
	long_description=long_description,
	long_description_content_type="text/markdown",
	url="https://gitlab.com/cmu_aart/asist/MinecraftBridge",
	packages=setuptools.find_packages(include=['MinecraftBridge']),
	classifiers=classifiers,
	python_requires=">=3.6",
	install_requires=install_deps,
	extras_require={
        **extras,
        'all': list(set(sum(extras.values(), [])))
    }
)
