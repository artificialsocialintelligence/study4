import imageio
import os

from gym_dragon import DragonEnv
from pathlib import Path



def random_walk_save_gif(path='dragon.gif'):
    """
    Visualize a trajectory where agents are taking random actions.
    """
    temp_dir = Path('./temp/')
    temp_dir.mkdir(parents=True, exist_ok=False)
    image_paths = []

    env = DragonEnv()
    env.reset()
    done = {agent_id: False for agent_id in env.get_agent_ids()}
    while not all(done.values()):
        image_paths.append(temp_dir / f'{env.time}.png')
        env.render(save_path=image_paths[-1], figsize=(12, 4), show_legend=False)
        random_action = env.action_space_sample()
        _, _, done, _ = env.step(random_action)

    # Build gif
    print('Saving GIF ...')
    with imageio.get_writer(path, mode='I') as writer:
        for path in image_paths:
            writer.append_data(imageio.imread(path))

    # Remove image paths
    print('Cleaning up ...')
    for path in set(image_paths):
        os.remove(path)
    temp_dir.rmdir()



if __name__ == '__main__':
    random_walk_save_gif()
