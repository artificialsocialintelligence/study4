# gym-dragon

The `gym-dragon` package contains a collection of multi-agent environments, specifically designed for research with the [DARPA Artificial Social Intelligence for Successful Teams (ASIST) project](https://www.darpa.mil/program/artificial-social-intelligence-for-successful-teams).

<br>

![dragon](https://i.imgur.com/eQmGwgQ.gif)

## Installation

    git clone -b https://gitlab.com/cmu_aart/asist/gym-dragon
    cd gym-dragon
    pip install -e .

## Demo

To visualize a trajectory from the default `DragonEnv` environment with agents taking random actions:

    python demos/random_walk.py

## Documentation

Documentation for this package can be found [here](https://cmu_aart.gitlab.io/asist/docs/gym-dragon/).

## Training

An example training script with RLlib can be found at [`scripts/train.py`](./scripts/train.py).
