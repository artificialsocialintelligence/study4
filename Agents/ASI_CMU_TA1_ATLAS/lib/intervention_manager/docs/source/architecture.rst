Architecture
============

The purpose of the `InterventionManager` packages is to encapsulate individual intervention logic, and provide management of intervention lifecycles.  

Components
----------

The class diagram for the InterventionManager is shown in :numref:`intervention_manager_class_diagram_figure`.  

.. _intervention_manager_class_diagram_figure:
.. figure:: uml/intervention_manager_class_diagram.png
   :alt: Class diagram for InterventionManager
   :scale: 50 %
   :align: center

   Class digram of the InterventionManager, showing major components.

The main components listed in :numref:`intervention_mamager_class_diagram_figure` are responsible for the following concerns:

*InterventionManager*

    The InterventionManager contains a collection of InterventionTriggers and InterventionResolution objects, and is
    responsible for maintaining the lifecycle of each Intervention and Followup.  In addition, the InterventionManager
    manages connection and disconnection of components to the external (Minecraft) message bus, and internal agent bus


*InterventionTrigger*

    InterventionTrigger objects are responsible for spawining one or more Intervention types based on received messages
    from the external or internal message busses.  The InterventionManager will typically contain multiple InterventionTriggers, 
    and InterventionTriggers can be added dynamically during the lifetime of the manager.  In general, each Intervention class
    will have a corresponding InterventionTrigger class, though there is no limitation on having an InterventionTrigger
    produce multiple types of Interventions, or having a type of Intervention being produced by different InterventionTriggers.
    An InterventionTrigger should *not* be responsible for maintaining state information about an Intervention, rather, the
    InterventionTrigger should spawn an Intervention when initial conditions for the Intervention are met.


*Intervention*

    An Intervention object will be spawned by an InterventionTrigger; once spawned, the InterventionManager will connect
    the Intervention to the internal and external bus.  The Intervention monitors messages from these busses, and either
    queues itself for resolution or discard, depending on the sequence of conditions received (see :ref:`intervention_lifecycle`).


*Resolution*

    A Resolution object is responsible for performing some behavior after an Intervention has queued itself for resolution.
    Each Resolution instance added to the InterventionManager will be provided Interentions of a given type, defined when the
    Resolution is added to the manager.  


*Followup*

    A Followup object is optionally created by a Resolution object after an Intervention is resolved.  A Followup instance monitors the external and internal busses, and enacts some behavior based on received messages.


InterventionManager Setup
-------------------------
The sequence diagram illustrating the construction of an InterventionManager is show in :ref:`intervention_manager_construction`.  When created, the manager must be provided with a
MinecraftBridge instance (i.e., the connection to the external message bus), and (currently optional),
a similar connection to the internal message bus.

.. _intervention_manager_construction:
.. figure:: uml/intervention_manager_setup_sequence_diagram.png
   :alt: Sequence diagram illustrating setup of the InterventionManager
   :scale: 50 %
   :align: center

   Sequence diagram illustrating the steps for setting up an InterventionManager.

Once the InterventionManager is created, the agent creating the manager can instantiate zero or
more InterventionTriggers, and add the triggers to the manager via the manager's `addTrigger` method.  Once added, the manager will connect the trigger to the interal and external busses.  InterventionResolutions are added in a similar manner:  the agent creates zero or more instances of InterventionResoultion, and adds the resolutions to the manager via the manager's `addResolution` method.


.. _intervention_lifecycle:
Intervention Lifecycle
----------------------

The Intervention base class maintains a high-level state of the intervention, based on the stage that the Intervention is in.  The states that the interventions may be in are shown in :ref:`intervention_state_machine`.

.. _intervention_state_machine:
.. figure:: uml/intervention_state_machine.png
   :alt: High-level states of an Intervention
   :scale: 50 %
   :align: center

   High-level state machine of an Intervention demonstrating Intervention lifecycle.

The high-level states are described as follows:

*Initialized*
    The intervention has been created by a corresponding InterventionTrigger, but has not yet been spawned to the
    InterventionManager.  The Base Intervention class will automatically activate itself when the manager 
    indicates that it has been spawned.

*Active*
    The intervention has been spawned to the InterventionManager, and connected to the external and internal busses.
    The repsonsibility of the intervention in this state is to either call `resolve` or `discard` on itself; the Base
    Intervention class manages interaction with the InterventionManager when either are called

*Queued for Resolution*
    The intervention has been disconnected from data streams, and is ready for being resolved.

*Discarded*
	The intervention has been disconnected from data streams, and has been discarded.

*Resolved*
	All resolutions for the intervention have been performed.  The intervention will be validated 
	through a follow-up.

*Validating*
	The intervention has been resolved and is currently in one or more Follow-ups.


Intervention Triggering
~~~~~~~~~~~~~~~~~~~~~~~
It is the repsonsibility of each InterventionTrigger to monitor the internal and external busses
for relevant messages, and to create and spawn an instance of an Intervention once some initial triggering
condition is satisfied.  Intervention Triggers should opt to spawn Intervention instances based on a
minimal initial condition---the role of the Intervention instance is to collect information necessary
to determine if the Intervention should be resolved (e.g., by presenting to the player) when some set of 
conditions are satisfied, or discarded when another set of conditions are satisfied. 

Once triggered, the lifecycle of the Intervention is described in the below sections, conditioned on whether
the intervention is resolved or discarded.  The triggering phase of an intervention is shown in :ref:`intervention_sequence_diagram_resolve` and :ref:`intervention_sequence_diagram_discard`.  In both of these figures, the blue arrow indicates the method
calls typically used when implementing concrete InterventionTrigger and Intervention classes; method calls in
red arrows are typically performed by the base classes or manager.


Intervention Resolution
~~~~~~~~~~~~~~~~~~~~~~~
An Intervention is resolved when a set of conditions occurs such that the Intervention is considered "complete".
For instance, an intervention that reaches a condition such that it should be presented to a participant would be
considered resolved.  Upon resolution, the intervention may optionally result in a `Followup` becoming instantiated.

The sequence diagram for an intervention is given in :ref:`intervention_sequence_diagram_resolve`.  Note that method calls
with red arrows are typically performed by the Intervention or InterventionTrigger base class; blue arrows indicate
method calls that are typically performed by concrete instances of the base classes

.. _intervention_sequence_diagram_resolve:
.. figure:: uml/intervention_sequence_diagram_resolve.png
   :alt: Sequence diagram of an intervention that is resolved.
   :scale: 50 %
   :align: center

   Sequence diagram for cases where an intervention is resolved.


Intervention Discard
~~~~~~~~~~~~~~~~~~~~
The alternative to an intervention reaching a resolution state is to have the intervention discard itself
when a some condition is met.  A discarded intervention is one which conditions have made the intervention
irrelevant, and thus requiring no resolution or followup.

The sequence diagram for an Intervention reaching a discard state is shown in :ref:`intervention_sequence_diagram_discard`.
As before, red arrows indicate method calls performed by the Intervention and InterventionTrigger base classes or manager,
and blue arrows are method calls typically involved by concrete Intervention / InterventionTrigger subclasses.

.. _intervention_sequence_diagram_resolve:
.. figure:: uml/intervention_sequence_diagram_discard.png
   :alt: Sequence diagram of an intervention that is discarded.
   :scale: 50 %
   :align: center

   Sequence diagram for cases where an intervention is discarded.


Intervention Followup
~~~~~~~~~~~~~~~~~~~~~
During resolution, a Resolution instance can optionally create and initiate a Followup.  Each Followup instance
is associated with a single completed Intervention.  It is the responsibility of the Resolution to create the
Followup and initiate it with the Intervention Manager through the `initiateFollowup` method.  The Intervention Manager
handles connection and disconnection of the Followup to the interal and external buses; the Followup should indicate that it
is finished by calling its `complete` method.

The sequence diagram for a followup is given in :ref:`intervention_sequence_diagram_followup`.  Method calls with red arrows
are typically performed by the Followup or InterventionManager; blue arrows indicate method calls that are performed by
concrete instances of the base class

.. _intervention_sequence_diagram_followup:
.. figure:: uml/intervention_sequence_diagram_followup.png
   :alt: Sequence diagram of a followup to an intervention
   :scale: 50 %
   :align: center

   Sequence diagram for a followup to a completed intervention.