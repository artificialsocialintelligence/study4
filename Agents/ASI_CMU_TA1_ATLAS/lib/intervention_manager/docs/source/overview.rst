Overview
========

The Intervention Manager package is a top-level component of the ATLAS agent developed for the DARPA ASIST program.  The package provides a framework for triggering interventions, managing intervention lifecycles, resolving / presenting interventions, and performing follow-up after an intervention was presented.

The goal of the framework is to allow for rapid development of interventions by allowing a developer to only need to implement intervention triggering and evolution logic.