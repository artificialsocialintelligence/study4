Usage
=====
Typical usage of the InterventionManager is to create an instance of the manager (:ref:`intervention_manager_usage`), and add one or more instances of an InterventionTrigger (:ref:`intervention_trigger_usage`) and Resolution (:ref:`resolution_usage`).  In addition, the InterventionManager allows for configuration through a JSON file (:ref:`intervention_configuration`), and the repository provides a simple agent to house an InterventionManager (:ref:`intervention_manager_agent_usage`).


.. _intervention_manager_usage:
InterventionManager
--------------------
Typically, a single `InterventionManager` instance will be utilized by an agent.  An `InterventionManager` takes an instance of a `MinecraftBridge` as an argument during creation, and can optionally take an instance of the internal agent bridge::

    from MinecraftBridge.mqtt import Bridge
    from InterventionManager import InterventionManager

    bridge = Bridge(...)
    manager = InterventionManager(bridge)

Once the manager is created, `InterventionTriggers` (:ref:`intervention_trigger_usage`) and `Resolutions` (:ref:`resolution_usage`) instances can be added to the manager.  When adding an `InterventionTrigger` using the `addTrigger` method, the instance of the trigger is passed as an argument; when adding a `Resolution` using the `addResolution` method, the instance of the resolution *and* the `Intervention` class the resolution can handle needs to be passed.  Note that the same `Resolution` instance can be coerced to handle multiple `Intervention` classes by calling `addResolution` multiple times, passing the same resolution instance and different Intervention classes.  Similarly, a single Intervention class can be handled by multiple resolutions through multiple calls to `addResolution`.  In the latter case, resolutions will be performed for an Intervention class in the order they were added.::

    # Create two InterventionTriggers and add to the manager
    trigger1 = InterventionTrigger1(manager)
    trigger2 = InterventionTrigger2(manager)

    manager.addTrigger(trigger1)
    manager.addTrigger(trigger2)

    # Create two Resolutions and add to the manager.  The first resolution
    # will handle Interventions of class InterventionClassA, while the second
    # resolution will handle both InterventionClassA and InterventionClassB
    # instances
    resolution1 = Resolution1(manager)
    resolution2 = Resolution2(manager)

    # InterventionClassA will be first resolved with resolution1, then by
    # resolution2.
    manager.addResolution(resolution1, InterventionClassA)
    manager.addResolution(resolution2, InterventionClassA)
    manager.addResolution(resolution2, InterventionClassB)

Assuming the `InterventionTrigger` and `Resolution` instances are constructed correctly, the `InterventionManager` will automate the following tasks:

1.  Connecting `InterventionTrigger` instances to the `MinecraftBridge` instance and forwarding messages to the trigger's registered callbacks.

2.  Maintain the lifecycle of individual `Intervention` instances, and connect or disconnect the instances to the `MinecraftBridge` instance as appropriate.

3.  Forward `Intervention` instances to appropriate `Resolution` instances when suitable.

4.  Maintain the lifecycle of individual `Followup` instances, and connect or disconnect the instances to the `MinecraftBridge`
instance as appropriate.

4.  Internally maintain queues of `Intervention` instances, based on the stage each instance is in.

5.  Internally maintain queues of `Followup` instances, based on the stage each followup is in.


.. _intervention_trigger_usage:
InterventionTrigger
-------------------
The task of an intervention trigger is to create and spawn an intervention when some condition has been met.  In general, the trigger observes the external and internal busses, and spawns interventions upon receiving a specific message or short sequence of messages.

All developed triggers should subclass `InterventionTrigger`, and call the `__init__` method during initialization prior to any other initialization.  The `__init__` method of the `InterventionTrigger` requires an instance of an `InterventionManager` to be passed as an argument; in practice, any subclass of the `InterventionTrigger` should similarly require an `InterventionManager` instance be passed as an argument.

Once created, the trigger should indicate which messages to receive and what method to call for each received message with the `register_callback` method.  In the typical use case, this would be performed during initialization of the trigger.  Registered callbacks need to accept a single MinecraftBridge message (of the type registered for) as an argument.  In summary, trigger initialization would typically appear as below::

    from InterventionManager import InterventionTrigger
    from MinecraftBridge.messages import PlayerState

    class ConcreteInterventionTrigger(InterventionTrigger):

        def __init__(self, manager, **kwargs):

            # The superclass __init__ method should always be called first
            InterventionTrigger.__init__(self, manager)

            # The trigger may now register callbacks for specific messages
            self.register_callback(PlayerState, self.on_player_state_message)


The trigger is responsible for creating an instance of a concrete `Intervention`, and providing the instance to the manager through the `spawn` method.  Typically, this will be done within one of the callbacks of the trigger::

    class ConcreteInterventionTrigger(InterventionTrigger):

        def __init__(self, manager, **kwargs):

            ...


        def on_player_state_message(self, message):

            ...

            # Spawn an intervention if some condition is met.
            if condition_met:

                intervention = ConcreteIntervention(...)
                self.manager.spawn(intervention)


Upon spawning, the `InterventionManager` and `Intervention` base class is responsible for managing the lifecycle of the intervention instance.  The intervention trigger should not need to keep a references to the spawned intervention.  


.. _intervention_usage:
Intervention
------------
An `Intervention` is designed to observed data streams from the external and internal busses once spawned, and flag itself for resolution or discard, depending on what sequence of conditions occur after spawning.  An intervention being triggered / spawned does not imply that the intervention will be resolved (e.g., presenting a suggestion to one or more participants).  Rather, the `Intervention` class implies the potential for a resolution to be performed by the agent, assuming relevant conditions are satisfied.

A concrete Intervention should always subclass the `Intervention` base class, and call the `__init__` method of the base class when initializing as a first step.  The base `Intervention` class requires an instance of an `InterventionManager` to be passed to the `__init__` method.  When an intervention is created, it indicated which callbacks should be called whenever messages of a given type are received, using the `register_callback` method.  A concrete intervention would typically appear as below::

    from InterventionManager import Intervention
    from MinecraftBridge.messages import PlayerState

    class ConcreteIntervention(Intervention):

        def __init__(self, manager, **kwargs):

            # The superclass __init__ method should always be called first
            Intervention.__init__(self, manager)

            # Register callback method when a PlayerState message is received
            self.register_callback(PlayerState, self.on_player_state_message)

        def on_player_state_message(self, message):
            """
            This is the callback method that is called whenever a PlayerState
            message is received
            """

            ...

Intervention classes should _not_ spawn themselves when created (i.e., a concrete Intervention subclass should not call 
`self.manager.spawn(self)`).  While the Intervention could spawn itself, this task is generally performed by the 
InterventionTrigger, or other external component.

Once spawned, the InterventionManager will connect the Intervention to the MinecraftBridge (and other data buses).  Once
connected, the registered callbacks will be invoked whenever a relevant message is received.  Depending on the state of the
Intervention, conditions may be met which results in the Intervention needing to be resolved or discarded.  In order to 
indicate this, a concrete intervention should call its `queueForResolution()` or `discard()` method.  Note that the
Intervention should _not_ call the Intervention Manager's `queueForResolution` or `discard` method (e.g., the Intervention
should not invoke `self.manager.discard(self)`)---the Intervention should call its versions of the methods in order to ensure
that state change is handled properly.  As a typical example, one or more message callback methods in an Intervention would
typically function as shown below::

   class ConcreteIntervention(Intervention):

       ...

       def on_player_state_message(self, message):
           """
           This is the callback method that is called whenever a PlayerState
           message is received
           """

           ...

           if resolve_condition_met:
               self.queueForResolution()
               return

            if discard_condition_met:
                self.discard()
                return


State Change Callback
~~~~~~~~~~~~~~~~~~~~~
The `InterventionManager` will invoke callbacks to the Intervention whenever the Intervention's state changes.  These 
callbacks consist of the following method names:

* _onSpawn:  Called when the Intervention is spawned
* _onActivated: Called when the Intervention is connected to the MinecraftBridge
* _onQueuedForResolution: Called when the Intervention is disconnected from the MinecraftBridge and 
* _onResolved: Called when the Intervention has completed resolution
* _onValidating: Called when a Followup is initiated on the Intervention
* _onValidated: Called when a Followup is complete
* _onDiscard: Called when the intervetion is discarded


In most cases, the `Intervetion` base class does nothing when these are called, with the exception of _onSpawn.  Thus, 
in most cases, it suffices for a concrete subclass of Intervention to simply override the callbacks (if desired).
However, a concrete subclass should call the base `Intervention` _onSpawn method if overriding this method, and should
do so at the end of the overriden method.


.. _resolution_usage:
Resolution
----------
The `InterventionManager` will send `Intervention` instances that are queued for resolution to one or more `Resolution` instances.  `Resolution` instances are persistent for the lifecycle of the `InterventionManager`.  There is currently not abstract `Resolution` class to inherit from.  The only requirement on a `Resolution` instance is to implement a `resolve(intervention)` method, which accepts _only_ a single `Intervention` instance, as illustrated below::

    class ConcreteResolution:

        def __init__(self, manager, **kwargs):

            # There are no requirements on what each Resolution requires during
            # initialization.  The DI framework will pass the instance of the
            # InterventionManager and any keywork arguments

        def resolve(self, intervention):

            # Perform some behavior based on the provided intervention

The dependency injection framework will attempt to construct resolutions by passing the instance of the InterventionManager and any further arguments as keyword arguments.  Thus, if the `__init__` method signature accepts a manager and keyword arguments, then the Resolution can be injected into the manager through a configuration file.

.. _followup_usage:
Followup
--------
A `Followup` class is similar in behavior to an `Intervention` class:  `Followup` instances have state reflecting lifecycle of the Followup, Followup instances are connected to the MinecraftBridge and internal bus, and the InterventionManager is responsible for lifecycle management and data stream connections.  A Followup should be associated with a single instance of an Intervention; however, there is no limit to the number of Followups that can be created for a single Intervention.

All Followup classes should inherit from the `Followup` base class.  A subclass should call the `Followup` base class's `__init__` method during initialization, which requires the Intervention instance and InterventionManager instance be passed as arguments::

    from InterventionManager import Followup
    from MinecraftBridge.messages import PlayerState

    class ConcreteFollowup(Followup):

        def __init__(self, intervention, manager, **kwargs):

            # The base class __init__ method should be called first
            Followup.__init__(self, intervention, manager)

            # Register any callbacks in the same manner as Interventions
            self.register_callback(PlayerState, self.on_player_state_message)

        def on_player_state_message(self, message):
            """
            Callback method invoked when a PlayerState message is received
            """

            ...

Typically, a `Resolution` instance will create a `Followup` instance, and can provide the followup to the manager through the `initiateFollowup` method::

    class ConcreteResolution:

        ...

        def resolve(self, intervention):

            ...

            followup = ConcreteFollowup(intervention, self.manager)

            self.manager.initiateFollowup(followup)

When some condition is met to consider the Followup complete, the Followup instance should call its `complete` method::

    class ConcreteFollowup(Followup):

        ...

        def on_player_state_message(self, message):
            """
            Callback method invoked when a PlayerState message is received
            """

            ...

            if complete_condition_met:
                self.complete()

The base `Followup` class and intervention manager will interact to change the state of the followup instance and disconnect it from the bridges.


State Change Callback
~~~~~~~~~~~~~~~~~~~~~
The `InterventionManager` will invoke callbacks to the Followup whenever the Followup's state changes.  These 
callbacks consist of the following method names:

* _onInitiated:  Called when the Followup is received by the manager
* _onActivate: Called when the Followup is connected to the MinecraftBridge
* _onComplete: Called when the Followup is disconnected from the MinecraftBridge and completed

In most cases, the `Followup` base class does nothing when these are called, with the exception of _onInitiated.  Thus, 
in most cases, it suffices for a concrete subclass of Followup to simply override the callbacks (if desired).
However, a concrete subclass should call the base `Followup` _onInitiated method if overriding this method, and should
do so at the end of the overriden method.


.. _intervention_manager_agent_usage:
Agent
-----
The provided `Agent` class is deprecated, and is only useful for testing out interventions which do not rely on external modules (e.g., TToM or NLP modules).

Intervention Configuration
~~~~~~~~~~~~~~~~~~~~~~~~~~
