@startuml

queue "Message /\nAgent Bus" as Bus
participant InterventionManager as Manager
participant Trigger
participant Intervention
participant Resolution
participant Followup


== Intervention Triggering ==

Bus -> Trigger : Triggering Message

create Intervention
Trigger -[#blue]> Intervention : << create >>

Trigger -[#blue]> Manager : spawn(intervention)

Manager -> Bus : << connect intervention >>

Manager -> Intervention : _onSpawn

Intervention -> Manager : activate

Manager -> Intervention : _onActivated


== Intervention Resolved ==

Bus -> Intervention : << Resolve Condition >>

Intervention -[#blue]> Intervention : resolve

Intervention -> Manager : queueForResolution

Manager -> Bus : << disconnect intervention >> 

Manager -> Intervention : _onQueuedForResolution

Manager -> Manager : resolve(intervention)

Manager -> Resolution : resolve(intervention)

Resolution --> Manager

Manager -> Intervention : _onResolved

@enduml
