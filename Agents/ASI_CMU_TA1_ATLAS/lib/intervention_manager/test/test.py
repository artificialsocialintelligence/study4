# -*- coding: utf-8 -*-
"""
.. module:: test
   :platform: Linux, Windows, OSX
   :synopsis: A module for testing and debugging interventions.

.. moduleauthor:: Ini Oguntola <danahugh@andrew.cmu.edu>

This file provides an agent that injects messages and passes them to an
intervention manager.
"""

import argparse
import BaseAgent
import glob
import importlib
import json
import os
import socket
import time

from InterventionManager import InterventionManager, Intervention, InterventionTrigger, Followup
from MinecraftBridge.mqtt.interfaces import Scheduler
from pathlib import Path



### Constants

LIBRARY_PATH = Path(__file__).parent.parent.resolve() / 'InterventionManager/library'
RESOURCES_PATH = Path(__file__).parent.resolve() / 'resources'



### Agent

class InterventionTestAgent(BaseAgent.BootstrapAgent):
    """
    A top-level agent class for testing the intervention manager and
    interventions as they are developed.
    """

    def __init__(self, args):
        # Make config
        config = self._make_config(args)

        # Initialize agent
        super().__init__(args.input_path, config)

        # Create intervention manager
        self.logger.info(f"{self}:  Creating InterventionManager")
        self.intervention_manager = InterventionManager(
            self.context, config['intervention_config'], agent_name=config['agent_name'])

        # Create TToM model
        if args.tom:
            self.logger.info(f"{self}:  Creating TToM")
            try:
                from tom import TToM
                self.ttom = TToM(self.context)
            except Exception as e:
                self.logger.error(f"{self}:  {e}")

        # Create NLP Manager
        if args.nlp:
            self.logger.info(f"{self}:  Creating NLPManager")
            try:
                from asist_nlp import NLPManager
                self.nlp_manager = NLPManager(self.context)
            except Exception as e:
                self.logger.error(f"{self}:  {e}")

        # Show timestamps
        if args.show_timestamps:
            scheduler = Scheduler(self.context.minecraft_interface)
            scheduler.repeat(
                lambda: print(time.strftime('%M:%S', time.gmtime(max(0, scheduler.time_remaining)))), 
                interval=1,
            )


    def run(self, *args, **kwargs):
        super().run(*args, **kwargs)
        self.intervention_manager._intervention_statistics_thread.cancel()


    def _make_config(self, args):
        config = {
            'agent_name': self.__class__.__name__,
            'loggers': {
                '': {'level': 'WARNING'}, # default logger
                self.__class__.__name__: {'level': 'INFO'}, # agent logger
            },
            'intervention_config': {'triggers': []},
            'redis_port': args.redis_port,
        }

        # Handle 'all' argument
        intervention_modules = args.interventions
        if len(intervention_modules) == 1 and intervention_modules[0] == 'all':
            intervention_modules = glob.glob(str(LIBRARY_PATH / '*.py'))
            intervention_modules = {Path(path).stem for path in intervention_modules}
            intervention_modules.discard('__init__')

        # Load each intervention
        for intervention in intervention_modules:
            try:
                module_path = f"InterventionManager.library.{intervention}"
                module = importlib.import_module(module_path)
                variables = [getattr(module, attr) for attr in dir(module)]
                triggers = self._get_subclasses(module, InterventionTrigger)
                interventions = self._get_subclasses(module, Intervention)
                followups = self._get_subclasses(module, Followup)

                # Set loggers
                config['loggers'].update({x.__name__: {'level': 'DEBUG'} for x in triggers})
                config['loggers'].update({x.__name__: {'level': 'DEBUG'} for x in interventions})
                config['loggers'].update({x.__name__: {'level': 'DEBUG'} for x in followups})

                # Set triggers
                trigger_class_names = {'.'.join([t.__module__, t.__name__]) for t in triggers}
                trigger_config = [{'trigger_class': t} for t in trigger_class_names]
                config['intervention_config']['triggers'] += trigger_config

            except Exception as e:
                self.logger.error(f"{self.__class__.__name__}:  Could not load intervention {intervention}")
                self.logger.error(f"{self.__class__.__name__}:  {e}")

        self.configure(config['loggers'])
        return config


    def _get_subclasses(self, module, superclass):
        variables = [getattr(module, attr) for attr in dir(module)]
        return {
            x for x in variables 
            if x != superclass
            and isinstance(x, type)
            and issubclass(x, superclass) 
        }



def print_terminal_header(text):
    print()
    print('-' * os.get_terminal_size().columns)
    print(text)
    print('-' * os.get_terminal_size().columns)
    print()


def run_metadata(args):
    print_terminal_header(args.input_path)
    agent = InterventionTestAgent(args)
    agent.run()


def run_hsr(args, metadata_dir='/data/ASIST/aptima/study-3_2022/'):
    if socket.gethostname() != 'ripley':
        raise EnvironmentError

    paths = BaseAgent.utils.get_metadata_paths(metadata_dir, hsr=True)
    for path in paths:
        args.input_path = path
        run_metadata(args)


def run_spiral(args):
    if socket.gethostname() != 'ripley':
        raise EnvironmentError
    if args.spiral in {'1', '2'}:
        metadata_dir = f'/data/ASIST/aptima/study-3_spiral-{args.spiral}_pilot/merged_metadata/'
    elif args.spiral in {'3', '4'}:
        metadata_dir = f'/data/ASIST/aptima/study-3_spiral-{args.spiral}_pilot/'
    else:
        raise NotImplementedError(args.spiral)

    paths = BaseAgent.utils.get_metadata_paths(metadata_dir, hsr=False)
    for path in paths:
        args.input_path = path
        run_metadata(args)


if __name__ == '__main__':
    # Parse command line arguments
    parser = argparse.ArgumentParser(description='InterventionTestAgent')
    parser.add_argument('interventions', nargs='+', help='Names of intervention modules in the InterventionManager library (e.g "time_elapsed"). To test all interventions in the library, use "all".')
    parser.add_argument('--input_path', '-i', default=(RESOURCES_PATH / 'test_run_2022_03_13.metadata'), help='Input metadata file, for file replay.')
    parser.add_argument('--hsr', action='store_true', help='If running on ripley, run through all study 3 HSR metadata.')
    parser.add_argument('--spiral', choices=['1', '2', '3', '4'], help='If running on ripley, run through all metadata from a particular spiral.')
    parser.add_argument('--tom', action='store_true', help='Include the TToM model as a component.')
    parser.add_argument('--nlp', action='store_true', help='Include the NLPManager as a component.')
    parser.add_argument('--show-timestamps', action='store_true', help='Log mission timestamps (time remaining).')
    parser.add_argument('--redis-port', type=int, default=6379, help='Port for the internal Redis bus.')
    args = parser.parse_args()

    # Create and run agent
    if args.hsr:
        run_hsr(args)
    if args.spiral:
        run_spiral(args)
    else:
        run_metadata(args)
