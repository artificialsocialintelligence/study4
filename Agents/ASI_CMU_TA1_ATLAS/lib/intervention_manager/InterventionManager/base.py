from MinecraftBridge.utils import Loggable

from collections import namedtuple
import uuid

class BaseComponent(Loggable):
    """
    Interface for objects that listen to both the internal and external
    buses through MinecraftBridge and RedisBridge objects.

    Nested Classes
    --------------
    Time : namedtuple
        A named tuple consisting of `elapsed_milliseconds` (int) and 
        `mission_timer` (tuple of ints)

    Attributes
    ----------
    manager : InterventionManager
        The top-level InterventionManager for the component
    uuid : UUID-4
        Unique identifier for the component

    Methods
    -------
    add_minecraft_callback(message_class, callback)
        Add a callback for the Minecraft bridge for a given message class
    add_redis_callback(callback, channel, message_type=None)
        Add a callback for RedisBridge messages of a given channel and message type
    """


    Time = namedtuple("Time", ["elapsed_milliseconds", "mission_timer"])


    def __init__(self):

        self._minecraft_callbacks = set()
        self._redis_callbacks = set()

        self._uuid = uuid.uuid4()


    def __str__(self):
        """
        Provide a string representation of the object.

        Returns
        -------
        string
            The name of the object class
        """

        return self.__class__.__name__


    @property
    def uuid(self):
        """
        Unique identifier (UUID-4) for the Intervention / Followup
        """

        return self._uuid
    


    def add_minecraft_callback(self, message_class, callback):
        """
        Register a callback, indicating the function that should be called when
        a message with the given message_class is received.

        Arguments
        ---------
        message_class : MinecraftBridge.message class
            Message class to register
        callback : function
            Function to call when a message of message_class is received
        """

        self._minecraft_callbacks.add((message_class, callback))


    def add_redis_callback(self, callback, channel, message_type=None):
        """
        Add a callback for RedisBridge messages of a given channel and message 
        type.

        Arguments
        ---------
        callback : function
            Function to call when a message on the channel is received
        channel : str
            Name of the channel to listen to
        message_type : str or RedisBridge.messages class
            The type of message that should trigger the callback
        """

        self._redis_callbacks.add((callback, channel, message_type))
