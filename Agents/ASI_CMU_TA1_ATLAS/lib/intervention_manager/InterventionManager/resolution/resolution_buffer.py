# -*- coding: utf-8 -*-
"""
.. module:: resolution.resolution_buffer
    :platform: Linux, Windows, OSX
    :synopsis: Definition of a class to buffer resolutions
.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a class to buffer resolutions, allowing for multiple resolutions
to be in a queue prior to resolving
"""

##from MinecraftBridge.messages import AgentChatIntervention
from MinecraftBridge.messages import BusHeader, MessageHeader
from MinecraftBridge.mqtt.parsers import MessageType, MessageSubtype

from MinecraftBridge.utils import Loggable

from MinecraftBridge.messages import PlayerState


class ResolutionBuffer(Loggable):
    """
    This class is used to store a buffer of interventions that are queued to
    resolve, and perform the actual resolution at a fixed frequency.

    Methods
    -------
    resolve
        Store an intervention in the buffer to resolve
    """

    def __init__(self, manager, resolver, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the Intervention Manager that the buffer is
            associated with
        resolver : Resolver
            Resolution instance that interventions should be sent to
        """

        self.manager = manager
        self._resolver = resolver

        # Default values for interventions
        self._agent_name = kwargs.get("agent_name", "asi_cmu_ta1_atlas")

        # Create a buffer
        self._buffer = []

        # Store how often buffered resolutions should be resolved (in ms)
        self._resolution_rate = kwargs.get("resolution_rate", 1000)
        self._next_available_resolution_time = 0

        # Listen to player state messages, to be able to get the time as it
        # passes
        self.logger.debug(f'{self}:  Registering to hear PlayerState messages')
        self.manager.minecraft_bridge.register_callback(PlayerState, self.__onPlayerState)
        

    def __str__(self):
        """
        String representation of the ResolutionBuffer
        """

        return self.__class__.__name__

    def reset(self):
        """
        """

        self.logger.debug(f'{self}:  Resetting resolver')
        self._next_available_resolution_time = 0
        self._buffer.clear()

        self._resolver.reset()


    def __onPlayerState(self, message):
        """
        """

        # Is it possible to send an intervention?
        if message.elapsed_milliseconds < self._next_available_resolution_time:
            return

        self.logger.debug(f'{self}:  Able to send an intervention')

        if len(self._buffer) == 0:
            self.logger.debug(f'{self}:    No intervention available')
            return

        # Pull out the intervention and present
        intervention = self._buffer[0]
        self._buffer = self._buffer[1:]
        self.logger.info(f'{self}:    Presenting {intervention} at time {message.elapsed_milliseconds}')
        self._next_available_resolution_time = message.elapsed_milliseconds + self._resolution_rate

        self._resolver.resolve(intervention)


    def resolve(self, intervention):
        """
        Store the intervention in the buffer

        Arguments
        ---------
        intervention : Intervention
            Intervention to buffer
        """

        self._buffer.append(intervention)
