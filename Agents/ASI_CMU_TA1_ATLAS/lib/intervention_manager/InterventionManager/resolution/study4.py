from collections import defaultdict

from MinecraftBridge.utils import Loggable

from MinecraftBridge.messages import GELP

from MinecraftBridge.messages import MissionStage

from ..interventions import InterventionProxy, AggregatedInterventionProxy

from ..interventions import Intervention


class LeadershipTransformer(Loggable):
    """
    The LeadershipTransformer monitors GELP messages to update participant
    leadership scores.  Upon receiving an intervention, if an individual is
    exhibiting high enough leadership, then messages will be transformed to 
    be rerouted to the leader.
    """

    def __init__(self, manager, **kwargs):
        """
        """

        self._manager = manager

        # Leadership scores of each participant, and the minimum leadership
        # score needed to reroute a message
        self.leadership_scores = defaultdict(lambda: 0)
        self._min_leadership_score = kwargs.get("min_leadership_score", 8.0)

        # Register to receive GEM messages in order to check on leadership
        # scores
        self._manager.minecraft_interface.register_callback(GELP, self.__onGELP)


    def __onGELP(self, message):
        """
        Callback when GELP messages are recieved.  Used to update participant
        leadership profiles of participants
        """

        pass


    def __call__(self, intervention):
        """
        """

        intervention_proxy = None

        # Is there a leader?
        leader = None
        highest_leadership_score = 0.0

        for participant_id, score in self.leadership_scores.items():
            if score >= self._min_leadership_score:
                highest_leadership_score = max(score, highest_leadership_score)
                if score == highest_leadership_score:
                    leader = participant_id

        # If there's no leader, do nothing and return
        if leader is None:
            return None


        # Otherwise, create a proxy based on the message class
        if intervention.__class__.__name__ == "ResolvePlanDisagreementIntervention":
            # Original message, broadcast to team:  "There still seems to be some disagreement with the plan, you should stay in the store to resolve it."
            proxy = InterventionProxy(intervention)
            proxy.set_recipients([leader])
            proxy.set_message("The team seems to disagree on part of the plan, I would suggest asking your teammates if they're clear on the plan, and resolve any disagreement.")
            return proxy
        elif intervention.__class__.__name__ == "CognitiveLoadStaleMarkerBlocksIntervention":
            # Original message, broadcast to team:  "There are quite a few stale hazard beacons confusing the team.  I would suggest removing excess hazard beacons."
            proxy = InterventionProxy(intervention)
            proxy.set_recipients([leader])
            proxy.set_message("There are quite a few stale hazard beacons confusing the team.  I would suggest assigning someone to removing excess hazard beacons.")
            return proxy
        elif intervention.__class__.__name__ == "CompleteCognitiveArtifactIntervention":
            # Original message, broadcast to team: "Team, you seem to have come up with several plan steps that aren't reflected in the cognitive artifact.  You should update the map to indicate who will be performing the discussed tasks."
            proxy = InterventionProxy(intervention)
            proxy.set_recipients([leader])
            proxy.set_message("The team has discussed several plan steps not reflected in the cognitive artifact.  You should encourage your teammates to update the map with their proposed tasks.")
            return proxy
        else:
            return None



"""
'AfterActionReviewReconIntervention',
'AttendToFieldVotesIntervention',
'AttendToShopVotesIntervention',
'BombStartedWithoutResourcesIntervention',
'BroadcastDefusalSequenceIntervention',
'BuyFireExtinguishersIntervention',
'CognitiveLoadStaleMarkerBlocksIntervention',
'CompleteCognitiveArtifactIntervention',
'DontMarkLateFuseBombsDuringReconIntervention',
'DontPlaceHazardBeaconsDuringReconIntervention',
'DontRunThroughFireIntervention',
'EvacuateBombIfFailingIntervention',
'FireReachBombIntervention',
'HelpFrozenPlayerIntervention',
'IgnoreBombDueToFireIntervention',
'InstructOnBombBeaconIntervention',
'InstructOnChainBombsIntervention',
'InstructOnCognitiveArtifactIntervention',
'InstructOnFireExtinguishersIntervention',
'InstructOnHazardBeaconsIntervention',
'InstructOnInjuriesIntervention',
'InstructOnPPEIntervention',
'MinimapExcessiveUseIntervention',
'MissedDiscussingBombBeaconIntervention',
'NotFollowingPlanIntervention',
'BeaconPromptIntervention',
'FirePromptIntervention',
'MedicPromptIntervention',
'PlayerDisagreementIntervention',
'PlanRehersalIntervention',
'PlayerFrozenIntervention',
'PresentOptimalPlanIntervention',
'ReconSearchIntervention',
'RemindTeamOfRemainingTimeIntervention',
'ResolvePlanDisagreementIntervention',
'SuggestScoutingIntervention',
'SyncToolUsageIntervention',
'TeamWelcomeMessageIntervention',
'UseCognitiveArtifactIntervention',
'WrongDefusalToolUsedIntervention',
"""

class WrongMissionStageFilter(Loggable):
    """
    A filter that withholds interventions if currently in the wrong mission 
    phase.
    """

    def __init__(self, manager, **kwargs):
        """
        """


        self._current_stage = MissionStage.ReconStage

        self._invalid_interventions = {
            MissionStage.ReconStage: {

            },
            MissionStage.ShopStage: {
                'BombStartedWithoutResourcesIntervention',
                'BroadcastDefusalSequenceIntervention',
                'DontMarkLateFuseBombsDuringReconIntervention',
                'DontPlaceHazardBeaconsDuringReconIntervention',
                'DontRunThroughFireIntervention',
                'EvacuateBombIfFailingIntervention',
                'PlayerFrozenIntervention',
                'ReconSearchIntervention',
                'SuggestScoutingIntervention',
                'SyncToolUsageIntervention',
                'WrongDefusalToolUsedIntervention'
            },
            MissionStage.FieldStage: {
                'AfterActionReviewReconIntervention',
                'CompleteCognitiveArtifactIntervention',
                'DontMarkLateFuseBombsDuringReconIntervention',
                'DontPlaceHazardBeaconsDuringReconIntervention',
                'MinimapExcessiveUseIntervention',
                'MissedDiscussingBombBeaconIntervention',
                'BeaconPromptIntervention',
                'FirePromptIntervention',
                'MedicPromptIntervention',
                'PlayerDisagreementIntervention',
                'ReconSearchIntervention',
                'ResolvePlanDisagreementIntervention',
                'SuggestScoutingIntervention',
                'UseCognitiveArtifactIntervention'
             }
        }

    def update_mission_stage(self, mission_stage):
        """
        """

        self._current_stage = mission_stage


    def intervention_presented(self, intervention):
        """
        """

        pass


    def __call__(self, intervention, elapsed_milliseconds=-1):
        """
        """

        # What's the class name of the intervention?  Note that the intervention
        # could be wrapped in one or more proxies, so will need to dig down to
        # the base_intervention until a non-proxy class is returned
        if type(intervention) is InterventionProxy:
            intervention_class = intervention.get_base_class_name()
        elif type(intervention) is AggregatedInterventionProxy:
            # Ignore this
            return False
        else:
            intervention_class = intervention.__class__.__name__

        if intervention_class in self._invalid_interventions[self._current_stage]:
            return True
        else:
            return False




class MaxPresentationCountFilter(Loggable):
    """
    A simple filter that withholds interventions if they have been presented a
    certain number of times.
    """

    def __init__(self, manager, **kwargs):
        """
        """

        self._max_intervention_count = { #"DontRunThroughFireIntervention": 3,
                                         #"NotFollowingPlanIntervention": 3,
                                         "DontRunThroughFireIntervention": 0,
                                         "NotFollowingPlanIntervention": 0,
                                         "InstructOnBombBeaconIntervention": 0,
                                         "InstructOnChainBombsIntervention": 0,
                                         "InstructOnCognitiveArtifactIntervention": 1,
                                         "InstructOnFireExtinguishersIntervention": 0,
                                         "InstructOnHazardBeaconsIntervention": 0,
                                         "InstructOnInjuriesIntervention": 0,
                                         "InstructOnPPEIntervention": 1,
                                         "MinimapExcessiveUseIntervention": 1,
                                         "PresentOptimalPlanIntervention": 0,
                                         "InjuriesCostPointsIntervention": 0 }
        self._intervention_counts = defaultdict(lambda: 0)


    def update_mission_stage(self, mission_stage):
        """
        """

        pass


    def intervention_presented(self, intervention):
        """
        Increment the number of times this intervention class had been 
        presented.
        """

        # What's the class name of the intervention?  Note that the intervention
        # could be wrapped in one or more proxies, so will need to dig down to
        # the base_intervention until a non-proxy class is returned
        if type(intervention) is InterventionProxy:
            intervention_class = intervention.get_base_class_name()
        elif type(intervention) is AggregatedInterventionProxy:
            # Recurse over the interventions that this proxy aggregates,
            # updating the count for each
            for _intervention in intervention.interventions:
                self.intervention_presented(_intervention)
        else:
            intervention_class = intervention.__class__.__name__

        self._intervention_counts[intervention_class] += 1



    def __call__(self, intervention, elapsed_milliseconds=-1):
        """
        Determine if the intervention should be filtered out, based on 
        """

        # Get the intervention class --- if it's an aggregate, just ignore,
        # since the component interventions would have been filtered out
        if type(intervention) is InterventionProxy:
            intervention_class = intervention.get_base_class_name()
        elif type(intervention) is AggregatedInterventionProxy:
            return
        else:
            intervention_class = intervention.__class__.__name__

        # Is this intervention in the dictionary of max counts?
        if not intervention_class in self._max_intervention_count:
            return False

        # Otherwise, filter if this has been presented too many times
        if self._intervention_counts[intervention_class] >= self._max_intervention_count[intervention_class]:
            return True
        else:
            return False



class ExpirationFilter(Loggable):
    """
    A simple filter that withholds interventions if a certain duration has 
    passed.
    """

    def __init__(self, manager, **kwargs):
        """
        """

        self._odd_error_logged = False


    def update_mission_stage(self, mission_stage):
        """
        """

        pass


    def intervention_presented(self, intervention):
        """
        """

        pass


    def __call__(self, intervention, elapsed_milliseconds=-1):
        """
        Determine if the intervention should be filtered out, based on whether
        the amount of time since it entered the RESOLVED state has exceeded the
        expirtation time.
        """

        # elapsed_milliseconds may be `None' (possible due to end of trial)
        if elapsed_milliseconds is None:
            elapsed_milliseconds = -1

        # When did this intervention enter the into the QUEUED_FOR_RESOLUTION
        # state?
        intervention_time = intervention.state_entry_times[Intervention.State.QUEUED_FOR_RESOLUTION].elapsed_milliseconds

        # Just a catch in case this happens
        if intervention_time is None:
            intervention_time = -1

        # What's the class name of the intervention?  Note that the intervention
        # could be wrapped in one or more proxies, so will need to dig down to
        # the base_intervention until a non-proxy class is returned
        if type(intervention) is InterventionProxy:
            intervention_class = intervention.get_base_class_name()
        else:
            intervention_class = intervention.__class__.__name__


        # Has it been so long that this intervention is expired?  NOTE:  After
        # the trial ends, this sometimes causes a bug, so we'll just wrap it in
        # a try/catch block
        try:
            if (elapsed_milliseconds - intervention_time) > intervention.expiration: #self._expiration_times[intervention_class]:
                self.logger.debug(f"{self}:  Intervention {intervention} expired; queuing time: {intervention_time}; current time: {elapsed_milliseconds}")
                return True
        except Exception as e:
            if not self._odd_error_logged:
                self.logger.warning(f"{self}:  Received this odd exception: {e}")
                self._odd_error_logged = True

        # Otherwise, simply return False
        return False


class IgnoreBombDueToFireAggregator(Loggable):
    """
    An aggregator that will aggregate multiple IgnoreBombDueToFireIntervention
    instances into a single message.
    """

    def __init__(self, manager, **kwargs):
        """
        """

        self._manager = manager


    def update_mission_stage(self, mission_stage):
        """
        """

        pass


    def intervention_presented(self, intervention):
        """
        """

        pass


    def __call__(self, interventions):
        """
        Gather all interventions whose base class is IgnoreBombDueToFireIntervention
        and put into a single proxy
        """

        ignore_bomb_due_to_fire_interventions = []

        # Get all interventions that are IgnoreBombDueToFireIntervention 
        # instances or a proxy for them
        for intervention in interventions:
            if type(intervention) is InterventionProxy:
                class_name = intervention.get_base_class_name()
            else:
                class_name = intervention.__class__.__name__

            if class_name == "IgnoreBombDueToFireIntervention":
                ignore_bomb_due_to_fire_interventions.append(intervention)

        # Don't do anything if there isn't at least two interventions
        if len(ignore_bomb_due_to_fire_interventions) < 2:
            return None, []


        # Get all the BOMB_IDs from the intervention
        bomb_info = []
        for intervention in ignore_bomb_due_to_fire_interventions:

            # Dig down to the base intervention
            while type(intervention) is InterventionProxy:
                intervention = intervention.base_intervention

            # Grab the bomb info from the intervention
            bomb_info.append((intervention.bomb_id, intervention.bomb_location, intervention.bomb_section))

        # Create an aggregated intervention proxy
        aggregated_intervention = AggregatedInterventionProxy(self._manager)
        for intervention in ignore_bomb_due_to_fire_interventions:
            aggregated_intervention.add_intervention(intervention)

        bomb_ids_string = ','.join([f"{x[0]} in section {x[2]}"  for x in bomb_info[:-1]]) + ', and ' + bomb_info[-1][0]

        # The aggregate message should be the only thing changed
        message = "I would suggest not bothering trying to defuse " + bomb_ids_string + "---they will likely be destroyed by fire before you can reach them."
        recipients = ignore_bomb_due_to_fire_interventions[0].getInterventionInformation().get('default_recipients', [])
        responses = ignore_bomb_due_to_fire_interventions[0].getInterventionInformation().get('default_responses', ['I agree', 'I disagree', 'Skip'])

        aggregated_intervention.set_message(message)
        aggregated_intervention.set_recipients(recipients)
        aggregated_intervention.set_responses(responses)

        return aggregated_intervention, ignore_bomb_due_to_fire_interventions 


class BombInstructionAggregator(Loggable):
    """
    An aggregator that will aggregate multiple IgnoreBombDueToFireIntervention
    instances into a single message.
    """

    def __init__(self, manager, **kwargs):
        """
        """

        self._manager = manager


    def update_mission_stage(self, mission_stage):
        """
        """

        pass



    def intervention_presented(self, intervention):
        """
        """

        pass


    def __call__(self, interventions):
        """
        Gather all interventions whose base class is IgnoreBombDueToFireIntervention
        and put into a single proxy
        """

        aggregated_intervention = None
        ignore_bomb_due_to_fire_interventions = []

        return aggregated_intervention, ignore_bomb_due_to_fire_interventions 