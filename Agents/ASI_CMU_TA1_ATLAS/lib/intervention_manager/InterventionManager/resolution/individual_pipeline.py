# -*- coding: utf-8 -*-
"""
.. module:: resolution.resolution_pipeline
    :platform: Linux, Windows, OSX
    :synopsis: Definition of a pipeline for 
.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a class that acts as a pipeline for resolving interventions for 
each individual.
"""


from collections import defaultdict

##from MinecraftBridge.messages import AgentChatIntervention
from MinecraftBridge.messages import BusHeader, MessageHeader
from MinecraftBridge.mqtt.parsers import MessageType, MessageSubtype

from MinecraftBridge.utils import Loggable

from MinecraftBridge.messages import (
    InterventionResponse
)

from ..interventions import InterventionProxy, AggregatedInterventionProxy

from ..interventions import Intervention

from .study4 import (
    ExpirationFilter, 
    WrongMissionStageFilter,
    IgnoreBombDueToFireAggregator
)



class IndividualResolutionPipeline(Loggable):
    """
    The IndividualResolutionPipeline class propagates an InterventionProxy for
    an individual through the following steps:


    Methods
    -------
    resolve
        Resolve an intervention
    """

    def __init__(self, manager, resolution_sink, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the Intervention Manager that the buffer is
            associated with
        resolution_sink : Resolution
            Resolution that the final intervention should be sent to
        """

        self.manager = manager
        self.resolution_sink = resolution_sink

        # Internally will keep track of when interventions can be resolved next
        self.__next_presentation_time = -1
        self.__current_time = -1

        # Presentation buffer
        self.__presentation_buffer = []

        # Filters and aggregators
        self.__filters = []
        self.__aggregators = []

        # Default values for interventions
        self._agent_name = kwargs.get("agent_name", "asi_cmu_ta1_atlas")
        self._participant_id = kwargs.get("participant_id", "<UNKNOWN>")

        # The response delay is the amount of time after a player responds
        # until the next response
        self.__response_delay = kwargs.get("response_delay", 2000)

        # Listen for intervention responses
        self.manager.minecraft_interface.register_callback(InterventionResponse, self.__onInterventionResponse)

        
    def __str__(self):
        """
        String representation of the ResolutionBuffer
        """

        return self.__class__.__name__ + "[" + str(self._participant_id) + "]"


    def reset(self):
        """
        """

        self.logger.debug(f'{self}:  Resetting resolver')


    def __onInterventionResponse(self, message):
        """
        Callback when an intervention response is received
        """

        # Is this response from the participant
        if message.sender_id == self._participant_id:
            self.logger.debug(f'{self}:  Received Intervention Response from {message.sender_id}')
            self.__next_presentation_time = self.__current_time + self.__response_delay
            self.logger.debug(f'{self}:    Next Presentation Time: {self.__next_presentation_time}')


    def add_filter(self, filter_, priority=20):
        """
        Add a filter to the pipeline
        """

        # NOTE:  Cannot really sort the filters and aggregators list, as the
        #        objects themselves are not comparable

        self.__filters.append((priority, filter_))
###        self.__filters.sort()


    def add_aggregator(self, aggregator, priority=20):
        """
        Add an aggregator to the pipeline
        """

        self.__aggregators.append((priority, aggregator))
###        self.__aggregators.sort()


    def update_mission_stage(self, mission_stage):
        """
        """

        for _, filter_ in self.__filters:
            filter_.update_mission_stage(mission_stage)
        for _, aggregator in self.__aggregators:
            aggregator.update_mission_stage(mission_stage)

        # New mission stage -- it's okay to present the next intervention
        # whenever
        self.__next_presentation_time = -1


    def __select_next_intervention(self, interventions):
        """
        Select the next intervention to present from the provided list of 
        interventions
        """

        # Sort by priority and the time that the intervention was queued
        intervention_list = {(intervention.priority, intervention.state_entry_times[Intervention.State.QUEUED_FOR_RESOLUTION]) : intervention
                              for intervention in interventions}
        intervention_keys = list(intervention_list.keys())
        intervention_keys.sort()

        return intervention_list[intervention_keys[0]]


    def update_time(self, elapsed_milliseconds):
        """
        """

        # Store the time
        self.__current_time = elapsed_milliseconds

        # Check if an intervention can be presented
        if elapsed_milliseconds < self.__next_presentation_time:
            return

        # Are there any interventions to resolve?
        if len(self.__presentation_buffer) == 0:
            return

        # Otherwise, filter the buffer and aggregate
        filtered_interventions = []

        for intervention in self.__presentation_buffer:
            for _, filter_ in self.__filters:
                if filter_(intervention, elapsed_milliseconds):
                    filtered_interventions.append(intervention)

        # Remove the filtered out interventions from the presentation buffer.
        # If there are no more interventions, simple return
        for intervention in filtered_interventions:
            self.__presentation_buffer.remove(intervention)
        if len(self.__presentation_buffer) == 0:
            return

        # Aggregate interventions --- will need to keep track of which
        # interventions are in each aggregate, so that they can be removed
        # from the presentation buffer if presented
        aggregated_interventions = {}

        for _, aggregator in self.__aggregators:

            aggregated_intervention, intervention_list = aggregator(self.__presentation_buffer)

            if aggregated_intervention is not None:
                aggregated_interventions[aggregated_intervention] = intervention_list

###        self.logger.info(f"{self}:  Presentation Buffer Size: {len(self.__presentation_buffer)}; Aggregated Intervention Size: {len(aggregated_interventions)}")

        # Grab the next intervention to present
        next_intervention = self.__select_next_intervention(list(aggregated_interventions.keys()) + self.__presentation_buffer)

        # Was the selected intervention in the aggregated interventions?
        if next_intervention in aggregated_interventions:
###            self.logger.info(f"{self}:  Aggregated intervention selected: {next_intervention}")
            for intervention in aggregated_interventions[next_intervention]:
###                self.logger.info(f"{self}:    Removing {intervention}")
                self.__presentation_buffer.remove(intervention)
        else:
###            self.logger.info(f"{self}:    Removing {next_intervention}")
            self.__presentation_buffer.remove(next_intervention)

###        self.logger.info(f"{self}:  Presentation Buffer Size (post selection): {len(self.__presentation_buffer)}")



        # Determine when the next time an intervention can be presented
        # TODO:  Have this based on the expected amount of time 
        self.__next_presentation_time = elapsed_milliseconds + next_intervention.view_duration

        self.logger.info(f"==")
        self.logger.info(f"== Presenting {next_intervention}")
        self.logger.info(f"==     Time:    {self.__current_time}")
        self.logger.info(f"==     Next Presentation Time: {self.__next_presentation_time}")
        self.logger.info(f"==     Message: {next_intervention.getInterventionInformation().get('default_message', '<NOT PROVIDED>')}")
        self.logger.info(f"==     Recipients: {next_intervention.getInterventionInformation().get('default_recipients', '<NOT PROVIDED>')}")
        self.logger.info(f"==     Response Options: {next_intervention.getInterventionInformation().get('default_responses', '<NOT PROVIDED>')}")
        self.logger.info(f"==")

        self.resolution_sink.resolve(next_intervention)

        # Indicate to the filters and aggregators that this intervention was
        # presented
        for _, filter_ in self.__filters:
            filter_.intervention_presented(next_intervention)
        for _, aggregator in self.__aggregators:
            aggregator.intervention_presented(next_intervention)





    def add_intervention(self, intervention):
        """
        Add an intervention to be presented to the buffer.

        Arguments
        ---------
        intervention : Intervention
            Intervention to present
        """

        self.logger.debug(f"==")
        self.logger.debug(f"== Adding {intervention} to buffer")
        self.logger.debug(f"==     Time:    {self.__current_time}")
        self.logger.debug(f"==     Next Presentation Time: {self.__next_presentation_time}")
        self.logger.debug(f"==     Message: {intervention.getInterventionInformation().get('default_message', '<NOT PROVIDED>')}")
        self.logger.debug(f"==     Recipients: {intervention.getInterventionInformation().get('default_recipients', '<NOT PROVIDED>')}")
        self.logger.debug(f"==     Response Options: {intervention.getInterventionInformation().get('default_responses', '<NOT PROVIDED>')}")
        self.logger.debug(f"==")


        self.__presentation_buffer.append(intervention)

