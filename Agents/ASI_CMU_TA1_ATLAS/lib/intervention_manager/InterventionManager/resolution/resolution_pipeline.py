# -*- coding: utf-8 -*-
"""
.. module:: resolution.resolution_pipeline
    :platform: Linux, Windows, OSX
    :synopsis: Definition of a pipeline for buffering, routing, and prioritizing
               resolutions
.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a class that acts as a pipeline for resolving interventions.  
"""

from collections import defaultdict

##from MinecraftBridge.messages import AgentChatIntervention
from MinecraftBridge.messages import BusHeader, MessageHeader
from MinecraftBridge.mqtt.parsers import MessageType, MessageSubtype

from MinecraftBridge.utils import Loggable

from MinecraftBridge.messages import PlayerState, MissionStageTransition

from ..interventions import InterventionProxy

from .individual_pipeline import IndividualResolutionPipeline, ExpirationFilter, IgnoreBombDueToFireAggregator

from .study4 import MaxPresentationCountFilter, WrongMissionStageFilter

from .chat_presentation import ChatPresentation


class TestInterventionTransformer(Loggable):
    """
    Just a quick test
    """

    def __init__(self, manager, **kwargs):
        """
        """


        self.manager = manager


    def __call__(self, intervention):
        """
        Create a proxy that changes the text and recipients of RemindTeamOfRemainingTimeIntervention
        """


        if intervention.__class__.__name__ != "RemindTeamOfRemainingTimeIntervention":
            self.logger.debug(f'{self}:  Cannot handle {intervention}')
            return

        # Create the proxy and change the message
        intervention_proxy = InterventionProxy(intervention)

        intervention_proxy.set_message("Hi, I changed this message!")
        intervention_proxy.remove_response("Skip")

        return intervention_proxy




class ResolutionPipeline(Loggable):
    """
    The ResolutionPipeline class handles processing interventions through
    various stages, transforming aspects of the intervention through the
    pipeline.  Interventions pushed through this pipeline are ferried through
    the following steps:

    1.  Message Transformation:  Intervention message, recipients and responses
                                 are transformed based on context and current 
                                 message and recipients.  As an example, an
                                 intervention targeted at multiple participants
                                 can be rerouted to an individual identified as
                                 the team leader.
    2.  Individual Dispatch:  Intervention is sent to a separate resolution
                              pipeline for each participant.
    4.  Followup:  TBD

    The ResolutionPipeline makes use of InterventionProxy instances to handle
    transformations and rerouting in order to preserve the contents of the
    intervention as initially queued.  The Intervention instance itself is not
    considered fully resolved until the proxy for each recipient has completed
    resolution.

    Methods
    -------
    resolve
        Resolve an intervention
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the Intervention Manager that the buffer is
            associated with
        resolution_sink : Resolution
            Resolution that proxied interventions should be sent to at end of
            pipeline
        """

        self.manager = manager

        self._sink = ChatPresentation(self.manager)
        self.logger.debug(f"{self}:   -== Here's the resolution sink: {self._sink} ==-")

        # Default values for interventions
        self._agent_name = kwargs.get("agent_name", "asi_cmu_ta1_atlas")

        # Create a transformation chain on responsibility---each transformer
        # will determine if it can handle the intervention to be transformed,
        # and return an InterventionProxy with the transformed data if so, or
        # None if not.
        self.__transformers = []

        # Create a resolution pipeline for each participant for post-routing
        # transformation and buffering
        self._participant_pipelines = { p.id: self.__create_individual_pipeline(p.id)
                                        for p in self.manager.participants }

        self.logger.debug(f"{self}:    {self._participant_pipelines}")


###        self.add_transformer(TestInterventionTransformer(self.manager))

        self.manager.minecraft_interface.register_callback(PlayerState, self.__onPlayerState)
        self.manager.minecraft_interface.register_callback(MissionStageTransition, self.__onMissionStageTransition)


    def __str__(self):
        """
        String representation of the ResolutionBuffer
        """

        return self.__class__.__name__


    def reset(self):
        """
        """

        self.logger.debug(f'{self}:  Resetting resolver')


    def add_transformer(self, transformer, priority=20):
        """
        Add a transformer to the chain of transformers, as well as the 
        priority of the transformer.

        Arguments
        ---------
        transformer : Callable
            Callable that accepts an Intervention as an argument, and returns
            an InterventionProxy or None, depending on whether or not the
            transformer could transform it.
        priority : float, default=20
            Priority of the transformer.  Transformers with lower priority will
            be attempted prior to those with higher priority.
        """

        # Add the transformer and sort (sorting should not take long, as not
        # many transformers are expected)
        self.__transformers.append((priority, transformer))

        ## NOTE:  Cannot really sort according to the class, so we'll just
        ##        ignore this for now...
#        self.__transformers.sort()


    def __create_individual_pipeline(self, participant_id="<UNKNOWN>"):
        """
        Factory method for creating pipelines for individual resolution handling

        Arguments
        ---------
        participant_id : string
            Participant for whom this pipeline is being created
        """

        self.logger.debug(f"{self}:   -== Here's the resolution sink: {self._sink} ==-")

        pipeline = IndividualResolutionPipeline(self.manager, self._sink, participant_id=participant_id)
        pipeline.add_filter(ExpirationFilter(self.manager))
        pipeline.add_filter(MaxPresentationCountFilter(self.manager))
        pipeline.add_filter(WrongMissionStageFilter(self.manager))
        pipeline.add_aggregator(IgnoreBombDueToFireAggregator(self.manager))

        return pipeline


    def __onPlayerState(self, message):
        """
        Callback when a PlayerState message is received.  Used to get the
        elapsed milliseconds in the game, and to update the individual
        pipelines to see if an intervention should be presented
        """

        elapsed_milliseconds = message.elapsed_milliseconds

        # Do nothing if the elapsed milliseconds is -1, which indicates that
        # a trail is no longer running
        if elapsed_milliseconds == -1:
            self.logger.debug(f'{self}:  Elapsed milliseconds is -1.  Doing nothing.')
            return

        # Indicate that each individual's pipeline should update with the
        # current time elapsed
        for pipeline in self._participant_pipelines.values():
            pipeline.update_time(elapsed_milliseconds)


    def __onMissionStageTransition(self, message):
        """
        Callback when a MissionStageTransition message is received.  Used to 
        inform the individual pipelines of new mission stages
        """

        for pipeline in self._participant_pipelines.values():
            pipeline.update_mission_stage(message.mission_stage)


    def __transform(self, intervention):
        """
        Perform the initial tranformation step of the provided intervention

        Argument
        --------
        intervention: Intervention
            Recieved intervention

        Returns
        -------
        InterventionProxy
            Transformed intervention
        """

        # Iterate through the transformers, stopping once the intervention is
        # transformed
        intervention_proxy = None

        for _, transformer in self.__transformers:
            if intervention_proxy is None:
                intervention_proxy = transformer(intervention)

        # If no proxy is created, create a simply proxy around the intervention
        # for the following phase
        if intervention_proxy is None:
            intervention_proxy = InterventionProxy(intervention)

        return intervention_proxy


    def resolve(self, intervention):
        """
        Runs an intervention through the resolution pipeline.

        Arguments
        ---------
        intervention : Intervention
            Intervention to buffer
        """

        intervention_proxy = self.__transform(intervention)

        # DEBUG MONITORING
        self.logger.info(f"++  Transformed {intervention} to {intervention_proxy}")
        self.logger.info(f"++     Original Message: '{intervention.getInterventionInformation().get('default_message', '<NOT PROVIDED>')}'")
        self.logger.info(f"++     Proxy Message:    '{intervention_proxy.getInterventionInformation().get('default_message', '<NOT PROVIDED>')}'")
        self.logger.info(f"++     Original Recipients:  {intervention.getInterventionInformation().get('default_recipients', '<NOT PROVIDED>')}")
        self.logger.info(f"++     Proxy Recipients:     {intervention_proxy.getInterventionInformation().get('default_recipients', '<NOT PROVIDED>')}")
        self.logger.info(f"++     Original Responses:   {intervention.getInterventionInformation().get('default_responses', '<NOT PROVIDED>')}")
        self.logger.info(f"++     Proxy Responses:      {intervention_proxy.getInterventionInformation().get('default_responses', '<NOT PROVIDED>')}")

        # Create and route the proxied intervention to each participant
        participants = intervention_proxy.getInterventionInformation().get('default_recipients', [])

        for participant in participants:
            # Create a new proxy specifically for this participant
            proxy = InterventionProxy(intervention_proxy.base_intervention, intervention_proxy.handler)
            proxy.set_message(intervention_proxy.getInterventionInformation().get('default_message', None))
            proxy.set_responses(intervention_proxy.getInterventionInformation().get('default_responses', ['I agree', 'I disagree', 'Skip']))
            proxy.set_recipients([participant])

            # Add to the participant's individual pipeline
            pipeline = self._participant_pipelines.get(participant, None)

            if pipeline is None:
                self.logger.debug(f"{self}:  Could not find individual pipeline for {participant} in {self._participant_pipelines}")
                self._participant_pipelines[participant] = self.__create_individual_pipeline()

            pipeline.add_intervention(proxy)
