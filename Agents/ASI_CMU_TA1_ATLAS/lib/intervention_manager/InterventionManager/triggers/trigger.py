# -*- coding: utf-8 -*-
"""
.. module:: triggers.trigger
    :platform: Linux, Windows, OSX
    :synopsis: Definition of a base InterventionTrigger class
.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Defines a base InterventionTrigger class to encapsulate functionality common
to all triggers.
"""

from ..base import BaseComponent



class InterventionTrigger(BaseComponent):
    """
    Definition of an abstract intervention trigger encapsulating functionality
    common to all triggers.  In order to work with the Intervention Manager,
    all concrete triggers should subclass this.

    Attributes
    ----------
    manager : InterventionManager
        Handle to the top-level intervention manager
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the top-level intervention manager
        """

        super().__init__()
        self.manager = manager


    def reset(self):
        """
        Reset the trigger to an initial state
        """

        pass
