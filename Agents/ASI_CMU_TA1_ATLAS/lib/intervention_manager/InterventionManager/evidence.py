# -*- coding: utf-8 -*-
"""
.. module:: evidence
   :platform: Linux, Windows, OSX
   :synopsis: Set of classes used to allow for storage and processing of 
              evidence with interventions and followups.

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Evidence is a set of classes that can be used to wrap disparate data types
(e.g., MinecraftBridge messages, Redis Bus messages, internal intervention
comments, etc.) into a set of classes for later analysis of explanation of
interventions or followups.
"""

from MinecraftBridge.utils import Loggable


class Evidence(Loggable):
   """
   A top-level abstract class for evidence.

   Attributes
   ----------
   comments : list
      List of Comments associated with a piece of evidence.
   time : int
      Mission time (in elapsed milliseconds) when the evidence is presented
   """

   def __init__(self, *args):
      """
      Arguments
      ---------
      List of comments for the evidence
      """

      self._comments = []

      self._time = None

      # Add any provided comments
      for comment in args:
         self.addComment(comment)


   def __str__(self):

      return self.__class__.__name__


   @property
   def comments(self):
      return self._comments


   @property
   def time(self):

      # Return -1 as the default if time is not set
      if self._time is None:
         return -1

      return self._time


   @time.setter
   def time(self, time):

      if time is not None:
         self.logger.warning("%s: Trying to reset evidence time", self)
         return

      self._time = time
   

   def addComment(self, comment):
      """
      Add a comment to the evidence

      Argument
      --------
      comment : Comment
         Comment to be added to the evidence
      """

      # TODO: check that this is a comment
      # NOTE: should other pieces of evidence be allowed?
      self._comments.append(comment)



class Comment(Evidence):
   """
   An evidence class that stores a comment.  A comment is a simple string
   providing a natural language comment to a piece of evidence.

   Attributes
   ----------
   comment_text : string
      Text of the comment
   """

   def __init__(self, comment):

      Evidence.__init__(self)

      self._comment_text = comment


   @property
   def comment_text(self):
      return self._comment_text
   


class MinecraftMessage(Evidence):
   """
   A MinecraftMessage is a simple wrapper around external messages from the
   Minecraft bridge
   """

   def __init__(self, message, *args):

      # Store all the comments
      Evidence.__init__(self, *args)

      self._message = message


   @property
   def message(self):
      return self._message


class ToM_BeliefMessage(Evidence):
   """
   A simple wrapper of belief messages from the TToM model
   """

   def __init__(self, message, *args):

      # Store the comments
      Evidence.__init__(self, *args)

      self._message = message


   @property
   def message(self):
      return self._message
   
   
class ToM_IntentMessage(Evidence):
   """
   A simple wrapper of intent messages from the TToM model
   """

   def __init__(self, message, *args):

      # Store the comments
      Evidence.__init__(self, *args)

      self._message = message


   @property
   def message(self):
      return self._message


class Conjunction(Evidence):
   """
   A conjunction of two pieces of evidence whose occurance are concurrent for
   the purposes of explanation
   """

   def __init__(*args, **kwargs):

      # Initialize with any comments
      Evidence.__init__(self, *kwargs.get("comments", []))

      self._evidence = []


   @property
   def evidence(self):
      return self._evidence


   def addEvidence(self, evidence):
      """
      Add a piece of evidence to this conjunction

      Arguments
      ---------
      evidence : Evidence
         Evidence to include in this conjuctino
      """

      self._evidence.append(evidence)



