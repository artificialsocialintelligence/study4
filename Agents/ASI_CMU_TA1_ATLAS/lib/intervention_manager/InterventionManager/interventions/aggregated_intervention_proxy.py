# -*- coding: utf-8 -*-
"""
.. module:: interventions.aggregated_intervention_proxy
    :platform: Linux, Windows, OSX
    :synopsis: Definition of an Intervention proxy class
.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Defines an Aggregated Intervention Proxy class as a wrapper for Interventions
to allow for defining a proxy that contains multiple interventions
"""

from .intervention import Intervention

class AggregatedInterventionProxy(Intervention):
    """
    Definition of a class to act as a Proxy for an intervention.  This class is
    technically a subclass of Intervention, and thus can be handled by the 
    InterventionManager as though an Intervention.


    Attributes
    ----------
    intervention : Intervention
        The intervention that is being proxied.
    handler : ResolutionPipeline
        Reference to the handler responsible for monitoring the proxy


    Methods
    -------
    """


    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------

        Keyword Arguments
        -----------------

        """

        # Initialize the Intervention superclass using the manager of the 
        # passed intervention as the argument for the intervention
        Intervention.__init__(self, manager, **kwargs)

        # Store the passed components
        self._interventions = []
        self._handlers = {}

        self._message = None
        self._recipients = []
        self._responses = ["I agree", "I disagree", "Skip"]


    def __str__(self):
        """
        Provide a string representation of the object.

        Returns
        -------
        string
            The name of the class and current state
        """

        return "%s[Interventions: %s]" % (self.__class__.__name__, self._interventions)


    @property
    def handlers(self):
        return self._handlers
    

    @property
    def interventions(self):
        """
        The list of interventions that this object is serving as a proxy for
        """

        return self._interventions


    @property
    def presented(self):
        return self._presented

    @presented.setter
    def presented(self, presented_):
        self._presented = presented_

        # Indicate that the base interventions have been presented
        for intervention in self.interventions:
            intervention.presented = intervention.presented or presented_


    def on_presented(self):
        """
        Callback when the intervention is presented.
        """

        # Add a note the base intervention
        self.base_intervention._resolution_notes.append({'proxy_id': self.uuid4,
                                                         'presented_message': self.presented_message,
                                                         'recipients': self.message_recipients})

        self.base_intervention.on_presented()
        

    def add_intervention(self, intervention, handler=None):
        """
        """

        if intervention in self._interventions:
            self.logger.debug(f"{self}:  Intervention {intervention} already in aggregate")
            return

        self._interventions.append(intervention)

        if handler is not None:
            self._handlers[intervention] = handler

        # Set the state entry times to be the minimum of all interventions
        for state in self.state_entry_times:
            entry_time = self.state_entry_times[state]
            intervention_entry_time = intervention.state_entry_times[state]

            if entry_time.elapsed_milliseconds == -1:
                self.state_entry_times[state] = intervention_entry_time
            elif intervention_entry_time.elapsed_milliseconds > 0:
                if intervention_entry_time.elapsed_milliseconds < entry_time.elapsed_milliseconds:
                    self.state_entry_times[state] = intervention_entry_time

        self.logger.debug(f'{self}:  State Entry Times: {self.state_entry_times}')


    def set_message(self, message):
        """
        Change the default message presented

        Arguments
        ---------
        message : str
            New message to present
        """

        self._message = message


    def set_recipients(self, recipients):
        """
        Set the participant list

        Arguments
        ---------
        recipients : List[str]
            List of recipients
        """

        self._recipients = recipients


    def add_recipient(self, recipient):
        """
        Add a recipient to the list of recipients

        Arguments
        ---------
        recipient : str
            Recipient to add
        """

        if recipient in self._recipients:
            self.logger.debug(f"{self}:  Attempting to add recipient already in proxy: {recipient}.")
            return

        self._recipients.append(recipient)


    def remove_recipient(self, recipient):
        """
        Remove a recipient from the list of recipients

        Arguments
        ---------
        recipient : str
            Recipient to remove
        """

        if not recipient in self._recipients:
            self.logger.debug(f"{self}:  Attempting to remove recipient not in proxy: {recipient}")
            return

        self._recipients.remove(recipient)


    def set_responses(self, responses):
        """
        """

        self._responses = responses


    def add_response(self, response):
        """
        Add a response to the list of responses

        Arguments
        ---------
        response : str
            Response to add
        """

        if response in self._responses:
            self.logger.debug(f"{self}:  Attempting to add response already in proxy: {response}.")
            return

        self._responses.append(response)


    def remove_response(self, response):
        """
        Remove a response from the list of responses

        Arguments
        ---------
        response : str
            Response to remove
        """

        if not response in self._responses:
            self.logger.debug(f"{self}:  Attempting to remove response not in proxy: {response}")
            return

        self._responses.remove(response)


    def getInterventionInformation(self):
        """
        Provide a dictionary containing the recipients and message.  Note that
        these may have changed from the original intervention, which will _not_
        have been changed by the proxy.
        """

        # Get information from base intervention
        base_interventions_information = [ intervention.getInterventionInformation() for intervention in self.interventions ]


        return { 'default_recipients': self._recipients,
                 'default_message': self._message,
                 'default_responses': self._responses,
                 'base_interventions': base_interventions_information
               }
