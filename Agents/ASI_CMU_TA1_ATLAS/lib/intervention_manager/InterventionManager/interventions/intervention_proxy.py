# -*- coding: utf-8 -*-
"""
.. module:: interventions.intervention_proxy
    :platform: Linux, Windows, OSX
    :synopsis: Definition of an Intervention proxy class
.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Defines a Intervention Proxy class as a wrapper for Interventions to allow for
redefinition of some aspect of the intervention.  Primarily, this is used to 
handle re-routing of interventions to different participants.  Note that this
class is not necessarily intended to act as a decorator for Interventions 
per se, rather, it is to allow a component (e.g., Resolution) to handle more
complex lifecycle state (e.g., presenting an intervention to different or
multiple participants at different times).
"""

from .intervention import Intervention

class InterventionProxy(Intervention):
    """
    Definition of a class to act as a Proxy for an intervention.  This class is
    technically a subclass of Intervention, and thus can be handled by the 
    InterventionManager as though an Intervention.


    Attributes
    ----------
    intervention : Intervention
        The intervention that is being proxied.
    handler : ResolutionPipeline
        Reference to the handler responsible for monitoring the proxy


    Methods
    -------
    """


    def __init__(self, intervention, handler=None, **kwargs):
        """
        Arguments
        ---------
        intervention : Intervention
            Intervention being proxied
        handler : Component, default=None
            Component currently handling the proxy(ies) for the intervention

        Keyword Arguments
        -----------------

        """

        # Want to simply copy priority, expiration, and view duration from the
        # base intervention, if it isn't in the kwargs
        kwargs["priority"] = kwargs.get("priority", intervention.priority)
        kwargs["expiration"] = kwargs.get("expiration", intervention.expiration)
        kwargs["view_duration"] = kwargs.get("view_duration", intervention.view_duration)
        kwargs["present_immediately"] = kwargs.get("present_immediately", intervention.present_immediately)


        # Initialize the Intervention superclass using the manager of the 
        # passed intervention as the argument for the intervention
        Intervention.__init__(self, intervention.manager, **kwargs)

        # Store the passed components
        self._intervention = intervention
        self._handler = handler

        # Set the state of the proxy to match that of the base intervention, 
        # as well as the state entry times and presented flag
        self._state = self._intervention.state
        self.presented = self._intervention.presented


        for state, entry_time in self._intervention.state_entry_times.items():
            self._state_entry_times[state] = Intervention.Time(entry_time.elapsed_milliseconds, entry_time.mission_timer)

        # Set the default message, recipients, and response to the current
        # values from the intervention
        intervention_info = self._intervention.getInterventionInformation()

        self._message = intervention_info.get("default_message", None)
        self._recipients = intervention_info.get("default_recipients", [])
        self._responses = intervention_info.get("default_responses", ["I agree", "I disagree", "Skip"])


    def __str__(self):
        """
        Provide a string representation of the object.

        Returns
        -------
        string
            The name of the class and current state
        """

        return "%s[Intervention: %s; Handler: %s]" % (self.__class__.__name__, self._intervention, self._handler)


    @property
    def handler(self):
        return self._handler
    

    @property
    def base_intervention(self):
        """
        The base intervention that this object is serving as a proxy for
        """

        return self._intervention


    @property
    def presented(self):
        return self._presented

    @presented.setter
    def presented(self, presented_):
        self._presented = presented_

        # Indicate to the base intervention that it has been presented
        self.base_intervention.presented = self.base_intervention.presented or presented_


    @property
    def presented_message(self):
        """
        Return the actual message that was presented to the participant(s).  If
        no message was presented, returns None.

        `presented_message` can only be set once, and should be set by the 
        resolution class that presents the message
        """

        return self._presented_message


    @presented_message.setter
    def presented_message(self, message):

        if self._presented_message is not None:
            self.logger.warning("%s:  Attempting to set already existing presented_message for Intervenion", self)
            self.logger.warning("%s:    Intervention message:  %s", self, self._presented_message)
            self.logger.warning("%s:    Attempted new message: %s", self, message)
            return

        self._presented_message = message


    @property
    def message_recipients(self):
        """
        Return the list of recipients that the intervention message was
        presented to.  If the message was not presented, returns None.

        `message_recipients` can only be set one, and should be set by the
        resolution class that presented the message.
        """

        return self._message_recipients


    @message_recipients.setter
    def message_recipients(self, recipients):

        if self._message_recipients is not None:
            self.logger.warning("%s:  Attempting to set already existing message_recipients for Intervention", self)
            self.logger.warning("%s:    Recipient list: %s", self, self._message_recipients)
            self.logger.warning("%s:    Attemted new recipients: %s", self, self._message_recipients)
            return

        self._message_recipients = recipients

        if self.base_intervention._message_recipients is None:
            self.base_intervention._message_recipients = recipients
        else:
            self.base_intervention._message_recipients = self.base_intervention._message_recipients + recipients


    def on_presented(self):
        """
        Callback when the intervention is presented.
        """

        # Add a note the base intervention
        self.base_intervention._resolution_notes.append({'proxy_id': self.uuid4,
                                                         'presented_message': self.presented_message,
                                                         'recipients': self.message_recipients})

        self.base_intervention.on_presented()

        

    def get_base_class_name(self):
        """
        Return the name of the base intervention class that this is a proxy for.
        If this proxy wraps another proxy, recurse down until a non-proxy class
        is reached.
        """

        if type(self.base_intervention) is InterventionProxy:
            return self.base_intervention.get_base_class_name()

        return self.base_intervention.__class__.__name__


    def set_message(self, message):
        """
        Change the default message presented

        Arguments
        ---------
        message : str
            New message to present
        """

        self._message = message


    def set_recipients(self, recipients):
        """
        Set the participant list

        Arguments
        ---------
        recipients : List[str]
            List of recipients
        """

        self._recipients = recipients


    def add_recipient(self, recipient):
        """
        Add a recipient to the list of recipients

        Arguments
        ---------
        recipient : str
            Recipient to add
        """

        if recipient in self._recipients:
            self.logger.debug(f"{self}:  Attempting to add recipient already in proxy: {recipient}.")
            return

        self._recipients.append(recipient)


    def remove_recipient(self, recipient):
        """
        Remove a recipient from the list of recipients

        Arguments
        ---------
        recipient : str
            Recipient to remove
        """

        if not recipient in self._recipients:
            self.logger.debug(f"{self}:  Attempting to remove recipient not in proxy: {recipient}")
            return

        self._recipients.remove(recipient)


    def set_responses(self, responses):
        """
        """

        self._responses = responses


    def add_response(self, response):
        """
        Add a response to the list of responses

        Arguments
        ---------
        response : str
            Response to add
        """

        if response in self._responses:
            self.logger.debug(f"{self}:  Attempting to add response already in proxy: {response}.")
            return

        self._responses.append(response)


    def remove_response(self, response):
        """
        Remove a response from the list of responses

        Arguments
        ---------
        response : str
            Response to remove
        """

        if not response in self._responses:
            self.logger.debug(f"{self}:  Attempting to remove response not in proxy: {response}")
            return

        self._responses.remove(response)


    def getInterventionInformation(self):
        """
        Provide a dictionary containing the recipients and message.  Note that
        these may have changed from the original intervention, which will _not_
        have been changed by the proxy.
        """

        # Get the information on the base intervention
        base_intervention_info = self.base_intervention.getInterventionInformation()
        base_intervention_info['intervention_class'] = self.base_intervention.__class__.__name__
        base_intervention_info['intervention_id'] = self.base_intervention.uuid4

        return { 'default_recipients': self._recipients,
                 'default_message': self._message,
                 'default_responses': self._responses,
                 'base_intervention': base_intervention_info
               }
