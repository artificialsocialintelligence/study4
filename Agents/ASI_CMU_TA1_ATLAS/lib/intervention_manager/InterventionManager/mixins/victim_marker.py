from ..utils import VICTIM_TO_MARKER_TYPES, distance
from ..components import InterventionScheduler as Scheduler
from MinecraftBridge.messages import (
    MarkerPlacedEvent, PlayerState, TriageEvent, VictimPickedUp,
)
from MinecraftBridge.utils import Loggable



class VictimMarkerMixin(Loggable):
    """
    Mixin that reports events relevant to placing marker blocks for victims.

    This mixin can be used by any component that inherits from BaseComponent
    (i.e., Interventions or Followups).  Note that the __init__ method of the
    BaseComponent must be called prior to calling this mixin's __init__ method.

    Callbacks
    ---------
    _onVictimMarkerCallback(callback)
        Default behavior for all VictimMarkerMixin callbacks unless they are overriden
    _onCorrectMarkerPlaced(message)
        Callback for when a correct marker block is placed near the victim
    _onIncorrectMarkerPlaced(message)
        Callback for when an incorrect marker block is placed near the victim
    _onVictimTriaged()
        Callback for when the victim is triaged
    _onAnotherVictimTriaged()
        Callback for when a different victim is triaged
    _onVictimPickedUp()
        Callback for when the victim is picked up
    _onDistanceExceeded()
        Callback for when the player leaves the proximity of the victim
    _onTimeout()
        Callback for when a given time interval has elapsed in the mission
        without the component being disconnected (e.g. discard / resolution / completion)
    """

    def __init__(self, manager, **kwargs):
        """
        Must provide kwargs for either:
            A. `triage_event`
            B. `player` AND `victim` (will also override the above)

        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention

        Keyword Arguments
        -----------------
        triage_event : TriageEvent
            TriageEvent message for the victim
        player : string
            Key for the relevant player (e.g. participant ID, playername, etc)
        victim : dict
            Dictionary of victim info with keys for 'id', 'location', and 'type'
        distance_threshold : float, default=5
            Maximum distance from victim that a player can go
            before the victim is considered to have been "left"
        timeout : float, default=15
            Maximum mission time (in seconds) before calling the _onTimeout() callback
        """
        self.__manager = manager
        self.__scheduler = Scheduler(self)

        # Validate required kwargs
        triage_event = kwargs.get('triage_event', None)
        player = kwargs.get('player', None)
        victim = kwargs.get('victim', None)

        if (triage_event is None) and (player is None or victim is None):
            # Log an error and return without initializing
            self.logger.error(
                "%s:  %s %s.",
                self,
                f"VictimMarkerMixin.__init__() requires kwargs for either ",
                f"A) `triage_event` or B) `player` AND `victim`",
            )
            return

        # Get player & victim info from triage event
        if triage_event is not None:
            self.__participant = manager.participants[triage_event.playername]
            self.__victim_id = triage_event.victim_id
            self.__victim_location = triage_event.victim_location
            self.__victim_type = triage_event.type

        # Get player info from kwargs
        if player is not None:
            self.__participant = manager.participants[player]

        # Get victim info from dictionary
        if victim is not None:
            self.__victim_id = victim['id']
            self.__victim_location = victim['location']
            self.__victim_type = victim['type']

        # Set attributes
        self.__distance_threshold = kwargs.get('distance_threshold', 5)

        # Initialize distance function
        if isinstance(self.__victim_location, str):
            # Location is a room name
            self.__dist_func = manager.semantic_map.distance
        else:
            # Location is some (x, z) or (x, y, z) iterable
            self.__dist_func = distance

        # Add Minecraft callbacks
        self.add_minecraft_callback(MarkerPlacedEvent, self.__onMarkerPlaced)
        self.add_minecraft_callback(PlayerState, self.__onPlayerState)
        self.add_minecraft_callback(TriageEvent, self.__onTriageEvent)
        self.add_minecraft_callback(VictimPickedUp, self.__onVictimPickedUp)

        # Schedule timeout
        timeout = kwargs.get('timeout', 15)
        if timeout:
            self.__scheduler.wait(self._onTimeout, delay=timeout)


    def _onVictimMarkerCallback(self, callback):
        """
        Default behavior for all VictimMarkerMixin callbacks unless they are overriden.

        Arguments
        ---------
        callback : callable
            The VictimMarkerMixin callback that was called
        """
        pass


    def _onCorrectMarkerPlaced(self, message):
        """
        Callback for when a correct marker block is placed near the victim.

        Arguments
        ---------
        message : MinecraftBridge.message.MarkerPlacedEvent
            Received MarkerPlacedEvent message
        """
        self._onVictimMarkerCallback(self._onCorrectMarkerPlaced)


    def _onIncorrectMarkerPlaced(self, message):
        """
        Callback for when an incorrect marker block is placed near the victim.

        Arguments
        ---------
        message : MinecraftBridge.message.MarkerPlacedEvent
            Received MarkerPlacedEvent message
        """
        self._onVictimMarkerCallback(self._onIncorrectMarkerPlaced)


    def _onVictimTriaged(self):
        """
        Callback for when the victim is triaged.
        """
        self._onVictimMarkerCallback(self._onVictimTriaged)


    def _onAnotherVictimTriaged(self):
        """
        Callback for when a different victim is triaged.
        """
        self._onVictimMarkerCallback(self._onAnotherVictimTriaged)


    def _onVictimPickedUp(self):
        """
        Callback for when the victim is picked up.
        """
        self._onVictimMarkerCallback(self._onVictimPickedUp)


    def _onDistanceExceeded(self):
        """
        Callback for when player has left proximity of the victim.
        """
        self._onVictimMarkerCallback(self._onDistanceExceeded)


    def _onTimeout(self):
        """
        Callback for when a given time interval has elapsed in the mission
        without the BaseComponent being disconnected (e.g. via 
        discard, resolution, completion).
        """
        self._onVictimMarkerCallback(self._onTimeout)


    def __onMarkerPlaced(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.message.MarkerPlacedEvent
            Received MarkerPlacedEvent message
        """

        # Ignore if message is for another participant
        participant = self.__manager.participants[message.playername]
        if participant != self.__participant:
            return

        # Get information from marker type and location
        is_correct_marker = (message.type in VICTIM_TO_MARKER_TYPES[self.__victim_type])
        dist = self.__dist_func(message.location, self.__victim_location)

        # Check if we are within a threshold distance of the victim location
        if dist <= self.__distance_threshold:
            if is_correct_marker:
                self._onCorrectMarkerPlaced(message)
            else:
                self._onIncorrectMarkerPlaced(message)

        # Player is inferred to have "left" the victim location
        else:
            self._onDistanceExceeded()


    def __onPlayerState(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.message.PlayerState
            Received PlayerState message
        """
        if message.elapsed_milliseconds <= 0:
            return

        # Ignore if message is for another participant
        participant = self.__manager.participants[message.participant_id]
        if participant != self.__participant:
            return

        # Check if player distance from location is above threshold
        dist = self.__dist_func(message.position, self.__victim_location)
        if dist > self.__distance_threshold:
            self._onDistanceExceeded()


    def __onTriageEvent(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.message.VictimPickedUp
            Received VictimPickedUp message
        """
        if message.triage_state == TriageEvent.TriageState.SUCCESSFUL:
            if message.victim_id == self.__victim_id:
                    self._onVictimTriaged()
            else:
                self._onAnotherVictimTriaged()


    def __onVictimPickedUp(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.message.VictimPickedUp
            Received VictimPickedUp message
        """
        if message.victim_id == self.__victim_id:
            self._onVictimPickedUp()
