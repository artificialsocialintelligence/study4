#!/bin/bash

echo "Running ATLAS from a metadata file"

if [[ "$#" -ne 3 ]]; then
	echo "USAGE:  sh atlas_from_file.sh <config_path> <input_path> <output_path>"
	exit 1
fi

echo "  Config Path: $1";
echo "  Input Path: $2";
echo "  Output Path: $3";

# Spin up the Redis server, and sleep.  If a Redis server is already running,
# it will print an error message, but won't affect the rest of the script
redis-server &
sleep 2

# Start the dashboard -- Would like to supress printing the stack output, 
# hence the /dev/null piping
#pushd . > /dev/null
#cd lib/dashboard
#node src/server/run.js &
#popd > /dev/null

# Do the standard BaseAgent invokation
python3 src/atlas.py $1 -i $2 -o $3

# Clean up the Redis server
trap 'kill $(jobs -p)' EXIT