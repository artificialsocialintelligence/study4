git submodule update --init --remote --recursive
source ./settings.env
docker compose --env-file settings.env down --remove-orphans
docker build -t ${DOCKER_IMAGE_NAME_LOWERCASE} --build-arg CACHE_BREAKER=$(date +%s) .
docker compose --env-file settings.env up -d