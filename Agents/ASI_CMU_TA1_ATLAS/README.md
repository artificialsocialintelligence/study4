# ATLAS

The Assisting Teamwork via Learning and Advising System (ATLAS) is an artificial social intelligence (ASI) agent designed to improve team performance through the use of interventions, i.e., suggestions to team members. This repository contains the main agent code and the scripts to create/initialize a docker container that will spin up our agent and either, a) connect to a live MQTT bus or b) play back recorded metadata files.

## Requirements

This repository is dependent on a number of other repositories for full functionality:
- `MinecraftBridge`
- `MinecraftElements`
- `InterventionManager`
- `ToM` (not currently required -- will add for Study 4 when updated)
- `NLP` (not currently required -- will add for Study 4 if necessary)
- `RedisBridge`
- `PredictionModule` (not currently required -- will add for Study 4 if necessary and updated)
- `PlanSupervisor`

These repositories are included as sub-modules (see instructions) so do not need to be explicitly installed beforehand.

## Running the agent from command line

After cloning this repository, initialize the above sub-modules via:
```
git submodule update --init --remote
```

This only needs to be done on initial clone. If the submodules get out-of-sync after this point, they can be brought back to the current reference with
```
git submodule pull
```

In order to test against a recorded metadata file, copy one into the `ConfigFolder` directory and rename it to `test.metadata`. If this file is not present, then the agent will look for an active MQTT bus to interface with the testbed.

To build the agent's docker container, run
```
source build.sh
```

Note that currently we install some heavy pip packages (including tensorflow and torch) as they are required by some sub-modules, so if you will be building multiple times it is better to utilize the build process with caching for pip packages, which can be invoked with
```
source build.sh -c
```

After the container is built, it can be started with the script
```
source up.sh
```

This will automatically launch the container, start the Redis server, start the agent, and attempt to connect to an active MQTT bus.

Note that the `ConfigFolder/config.json` file contains the IP and port number of the MQTT bus to connect to.

## Running a metadata file through the agent
After cloning this repository, sub-modules can be initialized as above:
```
git submodule update --init --remote --recursive
```

and dependencies can be installed locally via:
```
pip install --user -r requirements.txt
pip install --user -r requirements_local_nondocker.txt
```

Once the dependencies are installed, the agent can be run using the `atlas_from_file.sh` script
```
./atlas_from_file.sh src/ConfigFolder/config.json <input_metadata_file> <output_metadata_file>
```

Where `<input_metadata_file>` points to a path containing trial messages stored in a metadata file, and `<output_metadata_file>` will be created to store messages published by ATLAS.


## Notes about caching

Once development slows down, we should switch the base docker image to something that already includes most of the large pip packages so that they do not need to be re-downloaded everytime.

However, until then, building the docker container with caching is recommended. There is a caveat to this process though, caching is currently implemented using BuildKit which doesn't delete old image caches. As a result, the cache can grow quite large if builds are initiated many times.

To view the cache you can run
```
docker buildx du --verbose
```

and to clean it up run
```
docker buildx prune
```

## Configuration

When run, ATLAS needs to be provided with a configuration file in JSON format.  By default, the file used in the docker image is located under `src/ConfigFolder/config.json`.  In order to manage complexity, values in any configuration file can be referenced to a file containing the contents; the path to the file is indicated by `file:///<path_to_json_file>`.  Paths are assumed to be relative to the folder in the config file.


### Main Configuration

The main configuration file contains the following fields:

* `agent_name` (string) - name of the agent ("ASI_CMU_TA1_ATLAS")
* `mqtt_bus` (JSON object) - configuration information for the MQTT bus
* `redis_bus` (JSON object) - configuration information for the Redis bus
* `version_info` (JSON object) - version information for the agent
* `intervention_manager` (JSON object) - configuration information for the Intervention Manager
* `loggers` (JSON object) - logging configuration for individual classes


### mqtt_bus

The `mqtt_bus` configuration object contains the following fields:

* `host` (string) - host name of the MQTT broker
* `port` (int) - port number of the MQTT broker


### redis_bus

Like the `mqtt_bus`, the `redis_bus` configuration object contains the following fields:

* `host` (string) - host name of the Redis server
* `port` (int) - port number of the Redis server


### version_info

The `version_info` configuration object contains the information required by the `AgentVersionInfo` messages.  This includes the following fields:

* `agent_name` (string) - name of the agent (NOTE:  This will be deprecated in the near future)
* `verion` (string) - version string of ATLAS
* `owner` (string) - name of the group developing ATLAS ("CMU TA1")
* `agent_type` (string) - ATLAS's agent type ("ASI")
* `config` (list) - configuration information of the agent
* `source` (list) - list of locations of the source of the agent
* `dependencies` (list) - information on the dependencies of the agent
* `publishes` (list) - summary of message types the agent publishes
* `subscribes` (list) - summary of message type the agent subscribes to


#### config

The `config` object should contain a dictionary of the configuration information for ATLAS.  Each item in the config list requires two fields:

* `name` (string) - the name of the configuration value
* `value` (string) - the configuration parameter


#### source

The `source` list contains a list of URLS indicating where ATLAS can be retrieved from.


#### dependencies

The `dependencies` list contains a list of packages required to run ATLAS


#### publishes

The `publishes` list contains a list of messages that the agent may publish.  Each entry in the list is a JSON object with the following fields:

* `topic` (string) - topic of the message published
* `message_type` (string) - message type
* `sub_type` (string) - message subtype


#### subscribes

The `subscribes` list contains a list of messages that the agent subscribes to.  Each entry in the list is a JSON object with the following fields:

* `topic` (string) - topic of the message being subscribed to
* `message_type` (string) - message type
* `sub_type` (string) - message subtype


### loggers

The `loggers` field contains a JSON object, which maps class names to a JSON objects with the following fields:

* `level` (string) - the logging level for the class ("DEBUG", "INFO", "WARNING", "ERROR")


### intervention_manager

The `intervention_manager` field consists of two fields, defined as:

* `triggers` (list) - list of JSON objects describing the intervention triggers to include in ATLAS
* `resolvers` (list) - list of JSON objects describing the intervention resolutions to include in ATLAS

The list of intervention triggers and resolvers contain information needed to construct and inject specific trigger and resolution objects

#### triggers

Each object in the list of triggers contains the following entries:

* `trigger_class` (string) - fully qualified class name for the trigger class to construct
* `parameters` (JSON object) - object containing the arguments needed to create the trigger instance.  The content of this field is passed as keyword arguments for the trigger class when the instance is created.

#### resolvers

Each object in the list of resolvers contains the following entries:

* `resolution_class` (string) - fully qualified class name for the resolution class to construct
* `parameters` (JSON object) - object containing the arguments needed to create the resolution instance.  The content of this field is passed as keyword arguments for the trigger class when the instance is created.
* `handles` (list) - list of fully qualified intervention class names that the resolver will be provided.

#### follow_throughs

Each object in the list of follow throughs contains the following entries:

* `follow_through_class` (string) - fully qualified class name for the follow through class to construct
* `parameters` (JSON object) - object containing the arguments needed to create the resolution instance.  The content of this field is passed as keyword arguments for the trigger class when the instance is created.
* `handles` (list) - list of fully qualified followup class names that the follow through will be provided.