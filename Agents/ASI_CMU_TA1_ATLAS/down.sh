#!/bin/bash

echo "Bring down ASI_CMU_TA1 ATLAS Agent"
docker compose --env-file settings.env down --remove-orphans
docker ps
