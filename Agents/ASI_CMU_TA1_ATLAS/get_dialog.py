import os
import sys
import json

INPUT_FILE = sys.argv[1]
OUTPUT_FILE = sys.argv[2]

with open(INPUT_FILE) as f:
	data = []
	for line in f.readlines():
		try:
			data.append(json.loads(line))
		except:
			print("ERROR:  " + line)


chat_messages = [d['data'] for d in data if 'topic' in d and d['topic'] == 'communication/chat']
dialog_messages = [d['data'] for d in data if 'topic' in d and d['topic'] == 'agent/AC_UAZ_TA1_DialogAgent']

with open(OUTPUT_FILE, 'w') as f:
	for d in dialog_messages:
		f.write(json.dumps(d))
		f.write('\n')
