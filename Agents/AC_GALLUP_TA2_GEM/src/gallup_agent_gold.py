#!/usr/bin/env python3

"""
Gallup Object Library Distributor (GOLD)
Authors: Nathan Kress, Maxim van Klinken, Anirban Pal, Les DeBusk-Lane
email: nathan_kress@gallup.com, max_vanklinken@gallup.com, anirban_pal@gallup.com, Les_Debusk-Lane@gallup.com
"""
import pandas as pd
import numpy as np
from sentence_transformers import SentenceTransformer
# from tqdm.notebook import tqdm
import glob

class gold_prediction():
    def __init__(self):
        self.em_model = SentenceTransformer('all-MiniLM-L12-v2')
        self.label_avgs = pd.read_csv('resource/study4_avg_labels.csv')
        self.pkl_models = self._initialization()

    def _initialization(self):
        all_mods = pd.DataFrame()
        # for path in glob.glob('*/*/lgb_st_only*'):        
        for path in glob.glob('resource/lgb_st_only*'):
            dict = pd.read_pickle(path)
            df = pd.DataFrame.from_records([dict])
            all_mods = all_mods.append(df, ignore_index=True)
            # all_mods = pd.concat([all_mods,df], index=1)
        mod_lst = list(zip(all_mods['model_name'], all_mods['model']))

        return mod_lst

    def _incoming_data_processing(self, incoming_data):
        data_exp = incoming_data.explode('text')
        max_time = data_exp['file time'].max()
        sentences = data_exp['text'].tolist()
        embeddings = self.em_model.encode(sentences)
        embeddings_df = pd.DataFrame(embeddings).reset_index(drop = True)

        return data_exp, embeddings_df, max_time

    def prediction_zscore(self, incoming_data):
        data_exp, prediction_data, max_time = self._incoming_data_processing(incoming_data)
        get_all_zscores = {}
        get_all_adj_z_scores = {}
        # for model_name, lgb_model in tqdm(self.pkl_models):
        for model_name, lgb_model in self.pkl_models:
            # Non-adjusted z-scores:
            predictions = lgb_model.predict(prediction_data)
            participants = data_exp[['participant_id']].reset_index(drop = True)
            col_name = model_name
            output_df = pd.DataFrame()
            output_df[col_name] = pd.DataFrame(predictions)
            output_w_data_exp = pd.concat([participants, output_df], axis = 1)
            output_w_data_exp_agg = output_w_data_exp.groupby('participant_id')[col_name].sum().reset_index()
            target_label_stat = self.label_avgs[self.label_avgs['label']==col_name]
            output_w_data_exp_agg[['mean', 'std']] = target_label_stat[['mean', 'std']].iloc[0]
            output_w_data_exp_agg['zscore'] = ((output_w_data_exp_agg[col_name] / max_time) - output_w_data_exp_agg['mean']) / output_w_data_exp_agg['std']
            output_w_data_exp_agg['count'] = output_w_data_exp_agg[col_name]
            output_z_score = output_w_data_exp_agg[['participant_id', 'zscore', 'count']]
            get_all_zscores[col_name] = output_z_score
            
            # Adjusted z-scores:
            predictions_proba = lgb_model.predict_proba(prediction_data)
            output_df_proba = pd.DataFrame()
            output_df_proba[col_name] = pd.DataFrame(predictions_proba[:,1])
            output_df_proba = output_df_proba.add_suffix('_proba')
            target_label_stat = self.label_avgs[self.label_avgs['label']==col_name]
            label_per_row = (target_label_stat['mean'].iloc[0] / len(output_df_proba))
            predictions_proba = pd.concat([output_df, output_df_proba, participants], axis = 1)
            predictions_proba['adj'] = np.where(predictions_proba[col_name] == 0, 0 - label_per_row, 1 - label_per_row)
            predictions_proba['adj_zscore'] = np.where(predictions_proba[col_name] == 0, (1-predictions_proba.iloc[:,1])*predictions_proba['adj'], predictions_proba.iloc[:,1]*predictions_proba['adj'])
            get_all_adj_z_scores[col_name] = predictions_proba.groupby('participant_id')['adj_zscore'].sum().reset_index() 

        return get_all_zscores, get_all_adj_z_scores