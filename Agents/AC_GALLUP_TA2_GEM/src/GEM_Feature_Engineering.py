import pandas as pd
import numpy as np
import sklearn as skl #not used but you need it installed for the pickled model
import os
from functools import reduce

def process_surveys(df,participant_lookup):
    if len(df) == 0:
        return pd.DataFrame()

    message_df = pd.DataFrame([eval(message) for message in df.message])

    # surveys = {'psych_col': {'section': 'SectionD_IntakeSurvey_Study3', 'qids':[f'QID13_{i}' for i in range(1,16)]},
    # 'us_ord': {'section': 'SectionD_IntakeSurvey_Study3', 'qids':['QID5']},
    # 'age': {'section': 'SectionD_IntakeSurvey_Study3', 'qids':['QID1_TEXT']},
    # 'english_ord': {'section': 'SectionD_IntakeSurvey_Study3', 'qids':['QID4']},
    # 'education_ord': {'section': 'SectionD_IntakeSurvey_Study3', 'qids':['QID6']},}
    #'leadership':{'section':'SectionB_Mission1Reflection_Study3','qids':["QID803", "QID804", "QID805"]},
    #'leadership_components':{'section':'SectionB_Mission1Reflection_Study3','qids':[f'QID80{i}_{j}' for j in range(8,16) for i in range(6,9)]}}

    # Removed "us_ord" since it is no longer in the survey for study 4
    surveys = {'psych_col': {'section': 'PreTrialSurveys', 'qids':[f'QID13_{i}' for i in range(1,16)]},
    'age': {'section': 'PreTrialSurveys', 'qids':['QID1_TEXT']},
    'english_ord': {'section': 'PreTrialSurveys', 'qids':['QID4']},
    'education_ord': {'section': 'PreTrialSurveys', 'qids':['QID6']}}

    #leadership_component_names = ["ideas", "focus", "coordinate", "monitor", "share", "plan", "agree", "help"]

    responses_df = pd.DataFrame(list(message_df['values']))

    frames = []

    if len(responses_df.participant_id.unique()) != 3: 
        return pd.DataFrame()

    for name, survey in surveys.items():
        section_name = survey['section']
        qids = survey['qids']

        section = responses_df[responses_df.surveyname == section_name]
        if all(qid in section for qid in qids):

            # Removed rename since everything now is participant_id, not participantid
            # survey_subset = section[qids + ['participantid']].rename({'participantid':'participant_id'},axis=1).set_index('participant_id')
            survey_subset = section[qids + ['participant_id']].set_index('participant_id')
            if len(survey_subset) != 3:
                continue
            survey_subset.columns = name + '_'+survey_subset.columns

            columns_to_drop = []

            if name == 'psych_col':
                survey_subset['psych_col_overall'] = survey_subset[[f'psych_col_QID13_{i}' for i in range(1,16)]].mean(skipna=True,axis=1)

                columns_to_drop = [f'psych_col_{qid}' for qid in surveys['psych_col']['qids']]
            
            survey_subset.drop(columns_to_drop,inplace = True,axis = 1)

            frames.append(survey_subset)
        else:
            #print([qid for qid in qids if qid not in section], 'are not in ', section_name)
            continue
    if len(frames) == 0:
        return pd.DataFrame()
    return reduce(lambda a,b : pd.merge(a,b, left_index = True, right_index=True),frames)

def process_asr(df,participant_lookup):
    if len(df) == 0:
        return pd.DataFrame()
    message_df = pd.DataFrame([eval(message) for message in df.message])
    #print(message_df)
    #print(message_df[['participant_id','text']].groupby('participant_id')['text'].apply(list))
    return pd.DataFrame(message_df[['participant_id','text']].groupby('participant_id')['text'].apply(list))

def process_chat(df,participant_lookup):
    if len(df) == 0:
        return pd.DataFrame()
    message_df = pd.DataFrame([eval(message) for message in df.message])
    #print(message_df)
    #print(message_df[['participant_id','text']].groupby('participant_id')['text'].apply(list))
    return pd.DataFrame(message_df[['participant_id','message']].groupby('participant_id')['message'].apply(list))

def process_beard(df, participant_lookup):
    if len(df) == 0:
        return pd.DataFrame()
    
    beard_messages = [eval(message) for message in df.message]
    df = df.copy().reset_index(drop=True)
    df['beard_messages'] = pd.Series([np.array([data for participant_id, data in message.items()]) for message in beard_messages])
    df = df.explode('beard_messages').reset_index(drop=True)
    df = pd.concat([df,pd.DataFrame(list(df.beard_messages))],axis= 1)

    if 'participant_id' in df:
        return df.groupby('participant_id').last().drop(['message_topic','artifact','message_timestamp','curr_minute','message_elapsed_ms','message','beard_messages'],axis = 1)
    else:
        return pd.DataFrame()
    
def process_gold(df, participant_lookup):
    
    if len(df) == 0:
        return pd.DataFrame()
    message_df = pd.DataFrame([eval(message) for message in df.message])
    
    df = pd.concat([df.reset_index(drop=True),message_df[['gold_pub_minute','gold_results']]], axis = 1).explode('gold_results').reset_index(drop=True)

    gold_df = pd.DataFrame(list(df.gold_results))

    features = ['participant_id','agg_long_utterance','agg_num_utterances','mod_lgb_Deliberate_Planning_count','agg_time_spent_speaking','agg_first_word','mod_lgb_Transactive_Memory_count'] + \
        'mod_lgb_Motivation_per_min	mod_lgb_Deliberate_Planning_per_min	mod_lgb_Transactive_Memory_per_min	mod_lgb_Contingent_Planning_per_min	mod_lgb_Compensatory_Helping_per_min	mod_lgb_Reactive_Planning_per_min'.split('\t')

    def process_gold_individual(participant_dataframe):
        pdf = participant_dataframe.copy()

        pdf.drop(['message_topic','artifact','message_timestamp','curr_minute','message_elapsed_ms','gold_results','message'],axis = 1,inplace=True)
        pdf = pdf.sort_values(by = 'gold_pub_minute')

        pdf.loc[:,pdf.columns[pdf.columns.str.endswith('per_min')]] = pdf.loc[:,pdf.columns[pdf.columns.str.endswith('per_min')]].cumsum(axis = 0)

        return pdf.iloc[-1]

    if all(column in gold_df for column in features):
        subset = gold_df[features]
        df = pd.concat([df,subset], axis = 1)
        df = df.groupby('participant_id').apply(process_gold_individual)
        df = df.drop('participant_id',axis = 1).div(df.gold_pub_minute, axis = 0)
        df = df.drop('gold_pub_minute',axis = 1)
        return df
    else:
        return pd.DataFrame()
    
def process_odin(df,participant_lookup):
    if len(df) == 0:
        return pd.DataFrame()
    
    message_df = pd.DataFrame([eval(message) for message in df.message])
    df = pd.concat([df.reset_index(drop=True),message_df[['text','extractions','participant_id']]], axis = 1)
    df.drop('message',inplace=True, axis = 1)
    df['participant_id'] = df['participant_id'].replace({player['playername'] : player['participant_id'] for player in participant_lookup.to_dict('records')})

    #extractions_df = pd.DataFrame(df.extractions.to_list())
    #df = pd.concat([df.reset_index(drop=True),extractions_df], axis = 1)

    df = df.rename({'text':'odin_text'},axis = 1)
    df = df[df.participant_id != 'Server']
    return df[['participant_id','odin_text','extractions']].groupby('participant_id')[['odin_text','extractions']].agg(lambda x: x.tolist())
    
def merge_sketchy_data(processed_data):
    def combine_two(a,b):
        a_name, a_data = a
        b_name, b_data = b
        if len(a_data) == 0:
            #print(a_name,'was empty (a)')
            return b
        elif len(b_data) == 0:
            #print(b_name,'was empty (b)')
            return a
        else:
            columns_to_keep = list(b_data.columns.difference(a_data.columns))
            return 'combined', a_data.merge(b_data[columns_to_keep],left_index=True,right_index = True,how='outer')
    #print(processed_data)
    return reduce(lambda a,b: combine_two(a,b), processed_data.items())

cleaning_functions =    {'status/asistdataingester/surveyresponse':process_surveys,
                        #'agent/asr/final':process_asr,
                        'communication/chat':process_chat,
                        #'agent/ac/ac_cmu_ta2_beard/beard':process_beard,
                        'agent/gold': process_gold}
                        #'agent/dialog_raw':process_odin}

def create_and_clean_data(df,team, trial, minute):

    df = df[df.artifact == False]

    lookup = pd.DataFrame(eval(df[df.message_topic == 'gem_internal/participant_lookup'].iloc[0].message))
    df = df[df.message_topic != 'gem_internal/participant_lookup']
    df = df[~df.message_topic.isna()].iloc[:,1:].reset_index(drop=True)

    processed_dataframes = {}
    for topic, cleaning_function in cleaning_functions.items():
        topic_subset = df[df.message_topic == topic]
        cleaned = cleaning_function(topic_subset,lookup)
        
        processed_dataframes[topic] = cleaned
    
    _ , merged = merge_sketchy_data(processed_dataframes)

    merged['team'] = int(team)
    merged['trial'] = int(trial)
    merged['file time'] = int(minute)
    return merged.reset_index()#.set_index(['trial','team','file time','participant_id'])