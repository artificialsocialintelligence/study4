# #!/usr/bin/env python3

# """
# Gallup Emergent Leadership Prediction Agent (GELP)
# Authors: Nathan Kress, Maxim van Klinken, Anirban Pal
# email: nathan_kress@gallup.com, max_vanklinken@gallup.com, anirban_pal@gallup.com
# """
# __author__ = 'Gallup'

import pickle
import pandas as pd
import numpy as np
from GEM_Feature_Engineering import create_and_clean_data
import sklearn as skl #not used but you need it installed for the pickled model
import os

class Gelp():
    def __init__(self):
        pickle_data = pickle.load(open('resource/models.pkl','rb'))
        self.trained_models = pd.DataFrame(pickle_data['models'])
        self.features = pickle_data['features']

    def predict(self,df):
        data = df.copy()
        missing_columns = list(set(self.features) - set(data.columns))
        if len(missing_columns) > 0:
            print(f'WARNING: GELP predicting with missing data: {missing_columns}')
            for column in missing_columns:
                data[column] = np.NAN
        
        results = {}
        for participant in data.reset_index().participant_id:
            results[participant] = {}
        for name, model, upper, lower in zip(self.trained_models.name,self.trained_models.model,self.trained_models['upperbound model'],self.trained_models['lowerbound model']):
            data['prediction'] = model.predict(data[self.features])
            upperbound = upper.predict(data[self.features + ['prediction']])
            lowerbound = lower.predict(data[self.features + ['prediction']])
            for i,participant in enumerate(data.reset_index().participant_id):
                prediction_value = data['prediction'].iloc[i]
                upperbound_value = prediction_value + upperbound[i]
                lowerbound_value = prediction_value + lowerbound[i]
                results[participant][name] = {'prediction':prediction_value,'upperbound':upperbound_value,'lowerbound':lowerbound_value}
        
        return results