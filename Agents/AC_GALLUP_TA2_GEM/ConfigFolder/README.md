Notes:

- 2022-12-16 NAK: I think this folder should be present within agents themselves, it appears to only ever contain config.json files. It may be possible to string/combine those files together at this level but it could introduce errors. Leave this folder empty until proven necessary / unless placing angent config.json files within agent subfolders to GEM proves the incorrect approach

example config file contents (note specific to Agent):
{
    "host":"mosquitto",
    "port": "1883",
    "heartbeats_per_minute": 6,
    "version_info": {
        "owner": "Gallup - TA2",
        "version": "0.6.0",
        "source": [
            "https://gitlab.asist.aptima.com/asist/testbed/-/tree/develop/Agents/gallup_agent_gelp"
        ],
        "publishes": [
            {"topic": "agent/gelp"}
        ],
        "subscribes": [
            {"topic": "trial"},
            {"topic": "status/asistdataingester/surveyresponse"},
            {"topic": "agent/dialog"},
            {"topic": "agent/asr/final"},
            {"topic": "observations/events/player/rubble_destroyed"},
            {"topic": "observations/events/mission"}
        ]
    }
}

* Please refer to the README file in the root folder (`AC_GALLUP_TA2_GEM`) for further details.
