Notes:

- 2022-12-16 NAK: This folder and subfolders are to include any agent resources, shared between agents or not. It may be useful to git ignore large files and simply hand them to Bobby for default inclusion in the container. Edward Weiss has advised we should attempt to compress pickled model files (especially large ones) when at rest, unzip them at time of agent initialization.
- This folder will also need to be set up to be writable at time of container setup, as agents will be placing temporary files for use in agent calculations and event collection as well as export files for various use post experiment (log files, formatted event collections for analytics and modeling, etc.). Permissions for this folder at container setup time are handled here: /AC_GALLUP_TA2_GEM/dockerfile:16-17.
- 2023-12-16 AP: All latest GELP and GOLD models updated in resource folder as of today.
- Please refer to the README file in the root folder (`AC_GALLUP_TA2_GEM`) for further details.
