#!/usr/bin/env python3

import warnings

from utils_ac import *
from update_goal import *
from update_reqres import *

warnings.simplefilter(action='ignore', category=FutureWarning)


from asistagenthelper import ASISTAgentHelper
import logging, uuid

__author__ = 'hrc2lab'


def update_comm_state(topic, data, ac_data,logger):
    if topic == 'communication/environment': 
        # bomb info  
        if 'block_bomb' in data['sender_type']:
            bomb_id = data['sender_id']
            if bomb_id not in ac_data['bomb_state']:
                ac_data['bomb_state'][bomb_id] = {
                    'player_sequence' : [],
                    'bomb_type' : data['sender_type'],
                    'x' : data['sender_x'],
                    'y' : data['sender_y'],
                    'z' : data['sender_z'],
                }
            ac_data['bomb_state'][bomb_id].update(data['additional_info'])
            recipient = get_player_callsign(ac_data, data['recipients'][0])
            ac_data['player_last_visited_bomb'][recipient] = bomb_id
            # logger.info('%s_comms_env(info): %s', bomb_id, data)

        # beacon info
        elif 'Wirecutter!' in data['message']:
            beacon_id = data['sender_id'] 
            bomb_id, dist_to_bomb = get_nearest_bomb_id(data,ac_data) # this is a best guess
            if beacon_id not in ac_data['beacon_state']:
                ac_data['beacon_state'][beacon_id] = {
                    'bomb_id' : bomb_id,
                    'dist_to_bomb' : dist_to_bomb,
                    'x' : data['sender_x'],
                    'y' : data['sender_y'],
                    'z' : data['sender_z'],
                    'additional_info' : data['additional_info']
                }

            #handle beacon cuz this id matches the stat change id
            # logger.info('wirecutter_%s_comms_env: %s', data['sender_id'], data)
        
    return ac_data

def publish_ac_message(ac_message, text, logger, helper):
    logger.info('publish_ac_message%s | %s', text, ac_message)
    if 'goal' in text:
        helper.send_msg("agent/ac/dyad_alignment",
                    "AC",
                    "AC:Dyad_alignment",
                    "0.0.1",
                    data=ac_message)
    else:
        helper.send_msg("agent/ac/dyad_compliance",
                    "AC",
                    "AC:Dyad_compliance",
                    "0.0.1",
                    data=ac_message)



# This is the function which is called when a message is received for a topic which this Agent is subscribed to.
def on_message(topic, header, msg, data, mqtt_message):
    global ac_data, logger, helper
    # any updates to be made each second
    if 'mission_timer' in data:
        if ac_data['mission_timer']!=data['mission_timer']:
            # update tentative player goals each second
            recent_goals = [ac_data['player_goal'][pl][-1] for pl in ac_data['player_enum']]
            for k,v in ac_data['player_goal'].items():
                ac_data['player_goal'][k] = pad_list_to_n(v, mission_timer_to_sec(data['mission_timer'])+1)
            ac_data['mission_timer']==data['mission_timer']
            # publish_ac_message(recent_goals, ' ('+data['mission_timer']+')', logger)

            
    # initialize AC at game start
    if topic == 'trial' and msg['sub_type']=='start':
        ac_data = initialize_ac()
        ac_data = compile_player_info(ac_data, data)  
        for pl in ac_data['player_enum']:
            ac_data['player_goal'][pl] = ['Recon']
            ac_data['player_last_goal_update_sec'][pl] = 0
        ac_data['ac_summary']['curr_stage']['start_time'] = 0
        
    # any updates to be made each stage
    elif topic == 'observations/events/stage_transition':
        logger.info('stage_transition: %s', data) 
        ac_data['curr_stage'] = data['mission_stage'] + 'F' + str(data['transitionsToField']) + 'S' + str(data['transitionsToShop'])
        ac_message, ac_data = find_player_goal(topic, data,ac_data,logger)  
        publish_ac_message(ac_message,'_update_goal',logger, helper)  
        if data["mission_stage"] == "SHOP_STAGE":
            ac_message, ac_data = compute_goal_alignment(topic, data, ac_data,logger)
            publish_ac_message(ac_message,'_stage_goal_alignment',logger, helper)
            ac_message, ac_data = compute_dyad_compliance(topic, data, ac_data,logger)
            publish_ac_message(ac_message,'_stage_compliance',logger, helper)
    
    # update info
    elif topic == 'communication/environment': 
        # bomb info  
        if 'block_bomb' in data['sender_type']:
            update_comm_state(topic, data, ac_data,logger)
        # bomb beacon info
        elif 'Wirecutter!' in data['message']:
            update_comm_state(topic, data, ac_data,logger)
        # shop 
        elif 'voted' in data['message']:
            ac_message, ac_data = find_player_goal(topic, data,ac_data,logger)
            publish_ac_message(ac_message,'_update_goal',logger, helper)
            ac_message, ac_data = compute_goal_alignment(topic, data, ac_data,logger)
            publish_ac_message(ac_message,'_goal_alignment',logger, helper)
        else:
            #bomb beacon(duplicate)
            pass
    
    # beacons
    elif topic == 'item/state/change':
        # bomb beacon requests
        if (data['item_name'] == 'BOMB_BEACON' or \
            (data['item_name'] == 'block_beacon_hazard' and 'Wirecutter' in data['currAttributes']['message'])):
            ac_message, ac_data, team = open_new_request(topic, data,ac_data,logger)
            publish_ac_message(ac_message,'_make_request',logger, helper)
            if team:
                ac_message, ac_data = find_player_goal(topic, data,ac_data,logger)
                publish_ac_message(ac_message,'_goal_update',logger, helper)
                ac_message, ac_data = compute_goal_alignment(topic, data, ac_data,logger)
                publish_ac_message(ac_message,'_goal_alignment',logger, helper)

        # help hazard beacons
        elif data['item_name'] == 'block_beacon_hazard': 
            if 'Ignore' in data['currAttributes']['message']:
                pass
            elif 'help' in data['currAttributes']['message']:
                # player frozen request, hazard
                ac_message, ac_data, _ = open_new_request(topic, data,ac_data,logger)
                publish_ac_message(ac_message,'_make_request',logger, helper)
                ac_message, ac_data = find_player_goal(topic, data,ac_data,logger)
                publish_ac_message(ac_message,'_goal_update',logger, helper)
                ac_message, ac_data = compute_goal_alignment(topic, data, ac_data,logger)
                publish_ac_message(ac_message,'_goal_alignment',logger, helper)
            elif 'Help' in data['currAttributes']['message']: #fire
                ac_message, ac_data = find_player_goal(topic, data,ac_data,logger)
                publish_ac_message(ac_message,'_goal_update',logger, helper)
                ac_message, ac_data = compute_goal_alignment(topic, data, ac_data,logger)
                publish_ac_message(ac_message,'_goal_alignment',logger, helper)
            elif 'fire' in data['currAttributes']['message']:
                pass
            elif 'clear' in data['currAttributes']['message']:
                # clear any request/responses in the area
                pass
        # fire extinguisher
        elif data['item_name'] == 'FIRE_EXTINGUISHER':
            ac_message, ac_data = find_player_goal(topic, data,ac_data,logger)
            publish_ac_message(ac_message,'_goal_update',logger, helper)
            ac_message, ac_data = compute_goal_alignment(topic, data, ac_data,logger)
            publish_ac_message(ac_message,'_goal_alignment',logger, helper)
    elif topic == 'player/state/change':
        if data['participant_id'] in ac_data['open_requests'] and \
        data['changedAttributes']['is_frozen'][0]=='true' and \
        data['changedAttributes']['is_frozen'][1]=='false':
            ac_message, ac_data = find_player_goal(topic,data,ac_data, logger)
            publish_ac_message(ac_message,'_respond',logger, helper)
            ac_message, ac_data = respond_to_request(topic,data,ac_data, logger)
            publish_ac_message(ac_message,'_respond',logger, helper)
            ac_data['closed_requests']['fulfilled'].append(ac_data['open_requests'][data['participant_id']])
            del ac_data['open_requests'][data['participant_id']]
            ac_message, ac_data = compute_goal_alignment(topic, data, ac_data,logger)
            publish_ac_message(ac_message,'_goal_alignment',logger, helper)
            ac_message, ac_data = compute_dyad_compliance(topic, data, ac_data,logger)
            publish_ac_message(ac_message,'_dyad_compliance',logger, helper)

    # pertains to bombs, respond
    elif topic == 'object/state/change':
        # respond to an open request
        if data['id'] in ac_data['open_requests']:
            # fire triggered bomb state change
            if data['triggering_entity'] == 'SERVER':
                if data['currAttributes']['outcome']== 'EXPLODE_TIME_LIMIT':
                    ac_data['closed_requests']['fire'].append(ac_data['open_requests'][data['id']])
                    del ac_data['open_requests'][data['id']]
                    del ac_data['bomb_state'][data['id']]
            # bomb state change by player
            elif data['currAttributes']['outcome'] == 'EXPLODE_TOOL_MISMATCH':
                ac_data['closed_requests']['mismatch'].append(ac_data['open_requests'][data['id']])
                del ac_data['open_requests'][data['id']]
                del ac_data['bomb_state'][data['id']]
            elif data['currAttributes']['outcome'] == 'DEFUSED':
                del ac_data['bomb_state'][data['id']]
                pass
            else:
                bomb_id = data['id']
                pl = get_player_callsign(ac_data, data['triggering_entity'])
                ac_data['bomb_state'][bomb_id]['player_sequence'].append(pl)

                ac_message, ac_data = respond_to_request(topic,data,ac_data, logger)
                publish_ac_message(ac_message,'_respond',logger, helper)


                ac_message, ac_data = find_player_goal(topic,data,ac_data, logger)
                publish_ac_message(ac_message,'_update_goal',logger, helper)

                
                if (ac_data['open_requests'][data['id']]['response_tool'] in ac_data['open_requests'][data['id']]['requested_tool']) or\
                   (data['currAttributes']['outcome'] == 'DEFUSED_DISPOSER') or\
                   (ac_data['open_requests'][data['id']]['requested_tool']==''):
                    ac_data['closed_requests']['fulfilled'].append(ac_data['open_requests'][data['id']])
                    del ac_data['open_requests'][data['id']]
                    ac_message, ac_data = compute_goal_alignment(topic, data, ac_data,logger)
                    publish_ac_message(ac_message,'_goal_alignment',logger, helper)
                    ac_message, ac_data = compute_dyad_compliance(topic, data, ac_data,logger)
                    publish_ac_message(ac_message,'_dyad_compliance',logger, helper)
        else:
            if data['currAttributes']['outcome']  == 'TRIGGERED_ADVANCE_SEQ':
                bomb_id = data['id']
                pl = get_player_callsign(ac_data, data['triggering_entity'])
                ac_data['bomb_state'][bomb_id]['player_sequence'].append(pl)

                ac_message, ac_data = find_player_goal(topic,data,ac_data, logger)
                publish_ac_message(ac_message,'_update_goal',logger, helper)
                ac_message, ac_data = compute_goal_alignment(topic, data, ac_data,logger)
                publish_ac_message(ac_message,'_goal_alignment',logger, helper)


    # make updates to ac
    elif topic == 'trial' and msg['sub_type']=='stop':
        ac_data['ac_summary']['curr_stage']['end_time'] = len(ac_data['player_goal']['Alpha'])
        ac_message, ac_data = compute_goal_alignment(topic, data, ac_data,logger)
        publish_ac_message(ac_message,'_final_goal_alignment',logger, helper)
        ac_message, ac_data = compute_dyad_compliance(topic, data, ac_data,logger)
        publish_ac_message(ac_message,'_final_compliance',logger, helper)
        publish_ac_message({'open_requests': ac_data['open_requests'].keys()},'_open_requests',logger, helper)
        publish_ac_message({'closed_requests': ac_data['closed_requests']},'_closed_requests',logger, helper)

    elif topic == "player/inventory/update":
        for k, v in data['currInv'].items():
            ac_data['player_currInv'][get_player_callsign(ac_data,k)] = v
            ac_data['player_currInv'][get_player_callsign(ac_data,k)].update({
                'R':v['WIRECUTTERS_RED'],
                'G':v['WIRECUTTERS_GREEN'],
                'B':v['WIRECUTTERS_BLUE']
            })

    return ac_data


# Agent Initialization
ac_data={}       
helper = ASISTAgentHelper(on_message)

# Set the helper's logging level to INFO
LOG_HANDLER = logging.StreamHandler()
LOG_HANDLER.setFormatter(logging.Formatter("%(asctime)s | %(name)s | %(levelname)s — %(message)s"))
helper.get_logger().setLevel(logging.INFO)
helper.get_logger().addHandler(LOG_HANDLER)

# Create our own logger 
logger = logging.getLogger(helper.agent_name)
logger.setLevel(logging.INFO)
logger.addHandler(LOG_HANDLER)

# Set the agents status to 'up' and start the agent loop 
helper.set_agent_status(helper.STATUS_UP)
logger.info("Starting Agent Loop on a separate thread.")
helper.start_agent_loop_thread()
logger.info("Agent is now running...")

# if you need to do anything else you can do it here and if you want to stop the agent thread
# you can run the following, but until the agent loop is stopped, the process will continue to run.
#
# helper.stop_agent_loop_thread()



    