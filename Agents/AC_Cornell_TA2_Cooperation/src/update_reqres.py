# This script contains functions related to calculating the goal alignment metrics of the team


import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)



from utils_ac import *
from collections import defaultdict


def open_new_request(topic, data,ac_data,logger):
    """
    Requests are made by players when they place a bomb beacon in the environment
    or a beacon indicating the type of wire cutter they need
    multiple requests can be made for one bomb

    request message will be published each time a new request is made
    """
    if topic == 'item/state/change':
        requester = get_player_callsign(ac_data, data['owner'])
        mission_time =  mission_timer_to_sec(data['mission_timer'])

        # bomb beacons 
        if data['item_name'] == 'BOMB_BEACON':
            # has a env comm attached to this
            bomb_id = data['currAttributes']['bomb_id']
            request_params ={
                'bomb_id': bomb_id,
                'requester': requester,
                'request_stage' : ac_data['curr_stage'],
                'remaining_sequence' : ac_data['bomb_state'][bomb_id]['remaining_sequence'],
                'request_time':mission_time,
            }
            # tools not in inventory
            request_params['requested_tool'] = ''.join([t for t in request_params['remaining_sequence'] if t in ac_data['player_currInv'][requester] and ac_data['player_currInv'][requester][t]==0])

        elif data['item_name'] == 'block_beacon_hazard' and 'Wirecutter' in data['currAttributes']['message']:
            beacon_id = data['item_id']
            if beacon_id in ac_data['beacon_state']:
                bomb_id= ac_data['beacon_state'][beacon_id]['bomb_id'] # closest, best guess
            else:
                bomb_id= ac_data['player_last_visited_bomb'][requester] # last visited, best guess
            ac_data['beacon_state'][beacon_id]['owner'] = requester # update comm
            requested_tool = data['currAttributes']['message'].split()[3][0]
            request_params ={
                'bomb_id': bomb_id,
                'requester': requester,
                'request_stage' : ac_data['curr_stage'],
                'requested_tool' : requested_tool,
                'request_time':mission_time,
            }
        elif data['item_name'] == 'block_beacon_hazard' and 'help' in data['currAttributes']['message']:
            bomb_id= data['owner']
            requested_tool = 'unfreeze'
            request_params ={
                'bomb_id': bomb_id,
                'requester': requester,
                'request_stage' : ac_data['curr_stage'],
                'requested_tool' : requested_tool,
                'request_time':mission_time,
            }
        # make new or replace existing request
        ac_data['open_requests'][bomb_id]=request_params
        ac_message = request_params
        try:
            # if requester did not need any tools
            team = True if request_params['requested_tool']=='' else False
        except:
            return ac_message, ac_data , ''


    return ac_message, ac_data , team

def respond_to_request(topic, data,ac_data,logger):
    """
    Responses are published each time a new response is detected on an existing request.
    there may be multiple responses to the same request.

    a response can be to multiple requests? or 
    we assume that the response is for the most recent request on the same bomb
    """
    if topic == 'object/state/change':
        if data['id'] in ac_data['open_requests']:
            respond_param = {
                    'responder' : get_player_callsign(ac_data,data['triggering_entity']),
                    'response' : data['changedAttributes'],
                    'respond_time' : mission_timer_to_sec(data['mission_timer']),
                    'respond_stage' : ac_data['curr_stage'],
            }
            if data['currAttributes']['outcome'] == 'DEFUSED_DISPOSER':
                res_tool = data['currAttributes']['outcome']
            else:
                res_tool = data['changedAttributes']['sequence'][0][0]
            
            respond_param['response_tool'] = res_tool
            respond_param['response_time_duration'] = respond_param['respond_time']-ac_data['open_requests'][data['id']]['request_time']
            
            
            ac_data['open_requests'][data['id']].update(respond_param)
            ac_message = ac_data['open_requests'][data['id']]

            # update dyad compliance
            if (res_tool == 'DEFUSED_DISPOSER') or\
               (ac_data['open_requests'][data['id']]['requested_tool']=='') or\
               (res_tool in ac_data['open_requests'][data['id']]['requested_tool']):
                dyad = ac_data['open_requests'][data['id']]['requester'][0] + respond_param['responder'][0]
            else:
                return ac_message, ac_data
            if dyad in ac_data['dyad_compliance']:
                ac_data['dyad_compliance'][dyad] += 1
            else: 
                ac_data['dyad_compliance'][dyad] = 1
            
    elif topic == 'player/state/change':
        respond_param = {
            'responder' : get_player_callsign(ac_data,data['source_id']),
            'response' : data['changedAttributes'],
            'respond_time' : mission_timer_to_sec(data['mission_timer']),
            'respond_stage' : ac_data['curr_stage'],
        }
        respond_param['response_tool'] = 'unfreeze'
        respond_param['response_time_duration'] = respond_param['respond_time']-ac_data['open_requests'][data['participant_id']]['request_time']
        
        ac_data['open_requests'][data['participant_id']].update(respond_param)
        ac_message = ac_data['open_requests'][data['participant_id']]

        # update dyad compliance
        dyad = ac_data['open_requests'][data['participant_id']]['requester'][0] + respond_param['responder'][0]
        ac_data['dyad_compliance'][dyad] += 1


    return ac_message, ac_data


def compute_dyad_compliance(topic, data, ac_data,logger):
    """
    this function tracks the number of times each dyad had a request-response interaction
    it is to easily determine which dyad likes to collaborate together.
    We also compare each player's total requests and responses
    as well as the number of times they responded to themselves, or defused a bomb on their own
    """

    ac_message = defaultdict(dict)     
    ac_message['complied_dyad_raw'] = ac_data['dyad_compliance']
    requests_by_requester = {pl[0]:0 for pl in ac_data['player_enum']}
    requests_by_responder = {pl[0]:0 for pl in ac_data['player_enum']}
    for _,v in ac_data['open_requests'].items():
        requests_by_requester[v['requester'][0]] += 1
    for k,v in ac_data['closed_requests'].items():
        for l in v:
            requests_by_requester[l['requester'][0]] += 1
            if k=='fulfilled':
                requests_by_responder[l['responder'][0]] += 1
    for dyad, n in ac_data['dyad_compliance'].items():
        ac_message['complied_dyad_raw_balance'][dyad] = n/sum(ac_data['dyad_compliance'].values())
        ac_message['dyad_compliance_by_requester_reqs'][dyad] = (n/requests_by_requester[dyad[0]]) if requests_by_requester[dyad[0]]!=0 else None
    if sum(requests_by_requester.values()):
        for pl in ac_data['player_enum']:
            ac_message['res_player_compliance_by_all_reqs'][pl[0]] = requests_by_responder[pl[0]]/sum(requests_by_requester.values())
    
    return ac_message, ac_data