"""
CMU FMS Cognitive Load Analytic Component
"""

import dateutil.parser
import json
import logging
import math
import os
import uuid

from pprint import pprint       # only used for testing and debugging

import pyactup

from asistagenthelper import ASISTAgentHelper

EMIT_RESULTS = True

# Cognitive parameters
NOISE = 0.0
DECAY = 0.5
TEMPERATURE = 1
THRESHOLD = -4.0

CONFIDENCE_OFFSET = 0.25

MAX_MESSAGE_AGE = 6 * 60 * 60

PLAYERS = "Alpha,Bravo,Delta,Team".split(",")

def parse_header_timestamp(header):
    return dateutil.parser.parse(header["timestamp"]).timestamp()


class CMUFMSCognitiveLoadAC:

    def __init__(self):
        self.helper = ASISTAgentHelper(self.on_message)
        log_handler = logging.StreamHandler()
        log_handler.setFormatter(logging.Formatter("%(asctime)s | %(name)s | %(levelname)s — %(message)s"))
        self.helper.get_logger().setLevel(logging.INFO)
        self.helper.get_logger().addHandler(log_handler)
        self.logger = logging.getLogger(self.helper.agent_name)
        self.logger.setLevel(logging.INFO)
        self.logger.addHandler(log_handler)
        self.memory = pyactup.Memory(noise=NOISE,
                                     decay=DECAY,
                                     temperature=TEMPERATURE,
                                     threshold=THRESHOLD)
        self.reset()

    def reset(self):
        self.memory.reset()
        self.player_map = {}
        self.bomb_map = {}
        self.discovered_bombs = {p: set() for p in PLAYERS}
        self.resolved_bombs = set()
        self.start_time = None
        self.last_team_time = {}
        self.last_update_clock = None
        self.last_update_memory = None
        self.finished = False

    def update_time(self, data):
        t = data.get("elapsed_milliseconds") or data.get("elapsed_milliseconds_global")
        if t is None:
            self.logger.warning(f"no time in {data}")
            return
        elif t < 0:
            # time is -1 during initialization, I think we can ignore it
            return
        t /= 1000               # we work in seconds
        # Danger: beware of rounding issues
        if t > (mt := self.memory.time):
            self.memory.advance(t - mt)

    def note_player_bomb(self, bomb_id, player):
        self.discovered_bombs[player].add(bomb_id)
        self.memory.learn({"bomb": bomb_id, "player": player})

    def note_bomb(self, bomb_id, participant_id):
        # assumes time has already been updated appropriately
        if bomb_id not in self.resolved_bombs:
            if ((t := self.last_team_time.get(bomb_id)) is None
                    or not math.isclose(t, self.memory.time)):
                # time comparison avoids spurious duplicates
                self.note_player_bomb(bomb_id, "Team")
                self.last_team_time[bomb_id] = self.memory.time
            self.note_player_bomb(bomb_id, self.player_map[participant_id])

    def resolve_bomb(self, bomb_id):
        self.resolved_bombs.add(bomb_id)

    def activations(self, player):
        with self.memory.current_time:
            self.memory.advance(1)
            result = []
            for b in self.discovered_bombs[player]:
                if b not in self.resolved_bombs:
                    values, chunks, ignore = self.memory._activations({"bomb": b, "player": player})
                    if values:
                        value = values[0]
                        log_references = 1 + math.log(chunks[0].reference_count)
                        if value < self.memory.threshold:
                            self.logger.warning(f"activation {value} unexpectedly below threshold ({player})")
                        result.append((value, log_references))
            return result

    def exponentials(self, activations, offset=0):
        return [ math.exp((self.memory.threshold - (a[0] + offset / a[1])) / self.memory._temperature)
                 for a in activations ]

    def generate_results(self, player):
        activations = self.activations(player)
        mid = self.exponentials(activations)
        low = self.exponentials(activations, CONFIDENCE_OFFSET)
        high = self.exponentials(activations, -CONFIDENCE_OFFSET)
        load = cognitive_load(mid)
        forgetting = probability_of_forgetting(mid)
        return {
            "cognitive_load": {
                "value": load,
                "confidence": confidence(cognitive_load(low),
                                         load,
                                         cognitive_load(high)) },
            "probability_of_forgetting": {
                "value": forgetting,
                "confidence": confidence(probability_of_forgetting(low),
                                         forgetting,
                                         probability_of_forgetting(high))}}

    def respond(self, header):
        data = {
            "id": str(uuid.uuid4()),
            "agent": self.helper.agent_name,
            "created": self.helper.generate_timestamp(),
            "elapsed_milliseconds": round(self.memory.time * 1000)}
        for p in PLAYERS:
            data[p] = self.generate_results(p)
        self.logger.debug(f"sending cognitive agent information: {json.dumps(data)}")
        if EMIT_RESULTS:
            # time (s),Alpha load,Alpha load conf,Alpha prob,Alpha prob conf,Beta load,Beta load conf,Beta prob,Beta prob conf,Delta load,Delta load conf,Delta prob,Delta prob conf,Team load,Team load conf,Team prob,Team prob conf
            print(f"{data['elapsed_milliseconds'] / 1000},"
                  f"{data['Alpha']['cognitive_load']['value']:.3f},"
                  f"{data['Alpha']['cognitive_load']['confidence']:.3f},"
                  f"{data['Alpha']['probability_of_forgetting']['value']:.3f},"
                  f"{data['Alpha']['probability_of_forgetting']['confidence']:.3f},"
                  f"{data['Bravo']['cognitive_load']['value']:.3f},"
                  f"{data['Bravo']['cognitive_load']['confidence']:.3f},"
                  f"{data['Bravo']['probability_of_forgetting']['value']:.3f},"
                  f"{data['Bravo']['probability_of_forgetting']['confidence']:.3f},"
                  f"{data['Delta']['cognitive_load']['value']:.3f},"
                  f"{data['Delta']['cognitive_load']['confidence']:.3f},"
                  f"{data['Delta']['probability_of_forgetting']['value']:.3f},"
                  f"{data['Delta']['probability_of_forgetting']['confidence']:.3f},"
                  f"{data['Team']['cognitive_load']['value']:.3f},"
                  f"{data['Team']['cognitive_load']['confidence']:.3f},"
                  f"{data['Team']['probability_of_forgetting']['value']:.3f},"
                  f"{data['Team']['probability_of_forgetting']['confidence']:.3f}")
        self.helper.send_msg(f"agent/measure/{self.helper.agent_name}/load",
                             "agent",
                             "Measure:cognitive_load",
                             "1.0",
                             timestamp=header["timestamp"],
                             data=data)

    def start(self):
        self.helper.set_agent_status(self.helper.STATUS_UP)
        self.logger.info("Starting Agent Loop on a separate thread.")
        self.helper.start_agent_loop_thread()
        self.logger.info("Agent is now running...")

    def on_message(self, topic, header, msg, data, mqtt_message):
        self.logger.debug(f"{topic} message received")
        self.logger.debug(f"{data}")
        if topic == "trial":
            if self.start_time is None and msg.get("sub_type") == "start":
                self.start_time = parse_header_timestamp(header)
                for i in data.get("client_info"):
                    self.player_map[i["participant_id"]] = i["callsign"]
                self.logger.debug(f"player map: {self.player_map}")
            elif self.start_time and data.get("mission_state") == "Stop":
                self.finished = True
            return
        if topic == "groundtruth/environment/created/list":
            for b in data.get("list") or []:
                self.bomb_map[(b.get("x"), b.get("z"))] = b.get("id")
            return
        if topic == "player/state":
            if (self.last_update_memory is None and
                    (gt := data.get("elapsed_milliseconds_global")) is not None and
                    gt >= 0 and
                    self.start_time is not None and
                    not self.finished):
                self.update_time(data)
                self.last_update_clock = parse_header_timestamp(header)
                self.last_update_memory = self.memory.time
                self.respond(header)
            return
        if topic == "status/ac_ihmc_ta2_joint-activity-interdependence/heartbeats":
            if self.start_time is None or self.finished:
                return
            clock_time = parse_header_timestamp(header)
            # if abs(clock_time - self.start_time) <= MAX_MESSAGE_AGE:
            #     # When testing there are sometimes spurious heartbeats with timestamps
            #     # that correspond not to actual data times, rather when the captured date
            #     # was rewritten. Only use the real ones.
            #     return
            if self.last_update_clock is not None and self.last_update_memory is not None:
                clock_difference = clock_time - self.last_update_clock
                memory_difference = self.memory.time - self.last_update_memory
                if clock_time > memory_difference:
                    self.memory.advance(clock_difference - memory_difference)
                self.respond(header)
            self.last_update_clock = clock_time
            self.last_update_memory = self.memory.time
            return
        if topic == "environment/created/single":
            if not ((obj := data.get("obj")) and obj.get("type") == "block_beacon_bomb"):
                return
            self.update_time(data)
            coordinates = (obj.get("x"), obj.get("z"))
            if (b := self.bomb_map.get(coordinates)):
                self.note_bomb(b, data.get("triggering_entity"))
            return
        if topic != "observations/events/player/jag":
            self.logger.warning(f"Unexpected topic {topic}")
            return
        self.update_time(data)
        jag = data.get("jag")
        if not jag:
            self.logger.warning(f"missing jag {data}")
            return
        if not (urn := jag.get("urn")):
            self.logger.warning(f"missing urn {data}")
            return
        if urn not in {"urn:asist:defuse-bomb", "urn:asist:cut-wire" }:
            return
        bomb = (i := jag.get("inputs")) and i.get("bomb_id")
        if not bomb:
            self.logger.warning(f"missing bomb_id {data}")
            return
        if urn == "urn:asist:defuse-bomb" and data.get("status") in ["success", "failure"]:
            self.resolve_bomb(bomb)
            return
        if urn != "urn:asist:cut-wire" or data.get("status") == "start":
            return
        participant_id = (p := data.get("participant")) and p.get("id")
        if not participant_id:
            self.logger.warning(f"missing participant id {data}")
            return
        self.note_bomb(bomb, participant_id)


def cognitive_load(exponentials):
    return sum(exponentials)

def probability_of_forgetting(exponentials):
    return 1 - math.prod(1 / (1 + e) for e in exponentials)

def confidence(low, middle, high):
    if middle < 0:
        self.logger.warning(f"unexpected negative measure {middle}")
        return 0
    if high >= low:
        diff = high - low
    else:
        self.logger.warning(f"mis-ordered confidence interval ({low}, {high})")
        diff = low - high
    try:
        return math.exp(-diff / middle)
    except ZeroDivisionError:
        return 1


if __name__ == "__main__":
    CMUFMSCognitiveLoadAC().start()
