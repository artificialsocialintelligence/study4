#!/usr/bin/env python3

"""
UCF Flocking AC

"""

import argparse
import logging
from datetime import datetime

from asistagenthelper import ASISTAgentHelper

from Flocking_AC import handlemessage, reset_ac

global helper

FORMATTER = logging.Formatter("%(asctime)s | %(name)s | %(levelname)s — %(message)s\n")

STREAM_HANDLER = logging.StreamHandler()
STREAM_HANDLER.setFormatter(FORMATTER)
STREAM_HANDLER.setLevel(logging.INFO)

FILE_HANDLER = logging.FileHandler(f"{datetime.now().strftime('%Y%m%dT%H%M%S')}.log")
FILE_HANDLER.setFormatter(FORMATTER)
FILE_HANDLER.setLevel(logging.DEBUG)


def arguments():
    ap = argparse.ArgumentParser()
    ap.add_argument('--visualizer', help='starts the trial visualizer', action='store_true', default=False)
    return ap.parse_args()


def init_logging():
    # configures the root logger
    logger = logging.getLogger()
    logger.setLevel(logging.NOTSET)
    logger.addHandler(STREAM_HANDLER)
    logger.addHandler(FILE_HANDLER)

# Define the method to call when subscribed to messages are received
def on_message(topic, header, msg, data, mqtt_message):
    global helper
    #print("Received a message on the topic: ", topic)
    if topic == "trial":
        if msg["sub_type"] == "start":
            print("Resetting the AC")
            reset_ac()
    elif topic == "player/state":
        #print("data:", data)
        # Need to insert timestamp into data
        if data.get("timestamp") == None:
            data["timestamp"] = header.get("timestamp")
        publications = handlemessage(data)
        for pub in publications or []:  #handle the case of None
            print("Publishing the Flocking message: " , pub)
            helper.send_msg("agent/measure/AC_UCF_TA2_Flocking/flocking",
                            "agent",
                            "Measure:flocking",
                            "1.0",
                            timestamp=header["timestamp"],
                            data=pub)
    else:
        print("Received a message on the topic: ", topic, " but will ignore it.")


def start_agent():
    global helper
    # Initialize the helper class and tell it our method to call
    helper = ASISTAgentHelper(on_message)

    # Set the agent's status to 'up' and start the Main loop (which does not return)!!!
    helper.set_agent_status(helper.STATUS_UP)
    helper.run_agent_loop()

args = arguments()
init_logging()
start_agent()
