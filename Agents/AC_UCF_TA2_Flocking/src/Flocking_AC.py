#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import time
import json
import math
import sys
import matplotlib.pyplot as plt
import numpy as np
from shapely.geometry import Polygon
from pprint import pprint
from dateutil import parser
from datetime import datetime


# In[ ]:


# GLOBAL VARIABLES

global currentTime
global sampled10time
global sampled30time
global sampled60time
global sampled180time
global players
global boids
global boids_to_players
global players_to_boids
global test_data
global availablecolors
global inStore
global inStoreISO
global visitsToStore
global timeInStore
global MaxSpeed         
global MaxForce         
global SeparationRadius 
global AlignmentRadius 
global CohesionRadius 
global SeparationWeight 
global AlignmentWeight 
global CohesionWeight 

# Define the positions and velocities of the three actors
global actor1_position 
global actor1_velocity 
global actor2_position 
global actor2_velocity 
global actor3_position 
global actor3_velocity 

# Define the "search slice" parameters
triangle_side_length = 28
triangle_angle = 80

availablecolors = ['Red', 'Green', 'Blue', 'Orange', 'Yellow', 'Indigo', 'Violet', 'Grey', 'Pink', 'Magenta', 'Cyan']
currentTime   = 0
sampled10time = 0
sampled30time = 0
sampled60time = 0
sampled180time = 0
players = []
boids   = []
players = []
boids_to_players = {}
players_to_boids = {}
test_data = None
inStore = False
inStoreISO = 0
visitsToStore = 0
timeInStore = 0


# In[ ]:


# PARAMETERS
MaxSpeed         = 30.0
MaxForce         = 20.0
SeparationRadius = 30.0
AlignmentRadius  = 40 # 10.0
CohesionRadius   = 36 #10.0
SeparationWeight = 0.1 # was 0.2
AlignmentWeight  = 0.3
CohesionWeight   = 0.2 # was 0.4

# Define the "search slice" parameters
triangle_side_length = 28
triangle_angle = 80


# Example data drawn from a run for testing within Jupyter without loading a file
x1  = 25.9699
y1  = 147.877
vx1 = -0.1145
vy1 = -0.2216

x2  = 22.5905
y2  = 148.3346
vx2 = 0.0531
vy2 = -0.3415

x3  = 25.5776
y3  = 150.6622
vx3 = -0.3691
vy3 = -0.0378

# Define the positions and velocities of the three actors
actor1_position = [x1, y1]
actor1_velocity = [vx1, vy1]
actor2_position = [x2, y2]
actor2_velocity = [vx2, vy2]
actor3_position = [x3, y3]
actor3_velocity = [vx3, vy3]


# In[ ]:


# Major regions in the Study 4 testbed
forestX  = [1, 51]
forestZ  = [1, 49]
villageX = [1, 51]
villageZ = [51, 99]
desertX  = [1, 51]
desertZ  = [101, 150]

# The shop is outside of the fleif of operation
shopX    = [-47, -38]
shopZ    = [2, 10]

# The three regions Forest, Village, and Desert
fieldX   = [1, 51]
fieldZ   = [1, 150]


# In[ ]:


def in_region(aregionX, aregionZ, x, z):
    return (x >= aregionX[0]) and (x <= aregionX[1]) and \
           (z >= aregionZ[0]) and (z <= aregionZ[1])
    
def inForest(x, z):
    return in_region(forestX, forestZ, x, z)

def inVillage(x, z):
    return in_region(villageX, villageZ, x, z)

def inDesert(x, z):
    return in_region(desertX, desertZ, x, z)

def inField(x, z):
    return in_region(fieldX, fieldZ, x, z)

def inShop(x, z):
    return in_region(shopX, shopZ, x, z)

def myRegion(x, z):
    if inForest(x, z):
        return 'Forest'
    elif inVillage(x, z):
        return 'Village'
    elif inDesert(x, z):
        return 'Desert'
    elif inField(x, z):
        return 'Field'
    elif inShop(x, z):
        return 'Shop'
    else:
        return 'Portal' # Portal is between one region and another


# In[ ]:


def convert_isodatetime_to_timestamp(isodt):
    dt = parser.parse(isodt)
    return datetime.timestamp(dt)

#dtts = convert_isodatetime_to_timestamp('2023-04-27T21:37:56.685Z')
# print(dtts)


# In[ ]:


def elapsed_ms_in_the_field():
    global timeInStore
    global currentTime
    return currentTime - timeInStore


# In[ ]:


# Simple vector functions using [x, y] as a vector representation
def scalevec(xyvec, mag):
    return [xyvec[0]*mag, xyvec[1]*mag]

def displacevec(xyvec, dirn):
    return [xyvec[0]+dirn[0], xyvec[1]+dirn[1]]

def subtractvecs(v1, v2):
    return [v1[0]-v2[0], v1[1]-v2[1]]

def magnitude(xyvec):
    x = xyvec[0]
    y = xyvec[1]
    return math.sqrt(x*x+y*y)

def dividevec(xyvec, denominator):
    x = xyvec[0]
    y = xyvec[1]
    return [x/denominator, y/denominator]

# Note that norm is the same as magnitude when it is a vector
def norm(xyvec):
    x = xyvec[0]
    y = xyvec[1]
    return math.sqrt(x*x+y*y)

def normalize(xyvec):
    x = xyvec[0]
    y = xyvec[1]
    mag = math.sqrt(x*x+y*y)
    if mag>0:
        return [x/mag, y/mag]
    else:
        return [0.0, 0.0]

# round values for easier debugging

def cleanround(v):
    return round(v*1000000)/1000000

# rotate an xy vector by ang degrees
def rotate(xyvec, ang):
    rads   = -ang*(math.pi/180.0)
    cosang = math.cos(rads)
    sinang = math.sin(rads)
    return [cleanround(xyvec[0]*cosang-xyvec[1]*sinang), cleanround(xyvec[0]*sinang+xyvec[1]*cosang)]


# In[ ]:


# Define a function to calculate the vertices of a regular triangle with one vertex at the given position and the other two vertices in front of the actor's current velocity
def calculate_search_slice_vertices(position, velocity):
    # Calculate the direction of the actor's current velocity
    velocity_direction = normalize(velocity)
    # Calculate the two points in front of the actor's current position in the direction of their velocity
    point1 = displacevec(position, scalevec(rotate(velocity_direction, -triangle_angle/2), triangle_side_length))
    point2 = displacevec(position, scalevec(rotate(velocity_direction, triangle_angle/2), triangle_side_length))
    # Return the vertices of the regular triangle with one vertex at the actor's current position and the other two vertices 10 units in front of the actor's current velocity
    return [position, point1, point2]

def calculate_polygon_area(vertices):
    unzipped = list(zip(*vertices))
    x = unzipped[0]
    y = unzipped[1]
    # print("unzipped vertices="+str(unzipped))
    # print("x="+str(x)+" y="+str(y))
    return 0.5*np.abs(np.dot(x,np.roll(y,1))-np.dot(y,np.roll(x,1)))

# Define a function to calculate the area of a triangle given its vertices
# We could use calculate_polygon_area here+++

#def calculate_triangle_area(vertices):
    # Calculate the area of the triangle using the cross product of two of its sides
#    side1 = subtractvecs(vertices[1], vertices[0])
#    side2 = subtractvecs(vertices[2], vertices[0])
#    area = abs(np.cross(side1, side2)) / 2
    # Return the area of the triangle
#    return area


# In[ ]:


# Calculate the "search slices" for each actor at the current timestep
actor1_search_slice_vertices = calculate_search_slice_vertices(actor1_position, actor1_velocity)
actor1_search_slice_area = calculate_polygon_area(actor1_search_slice_vertices)
actor2_search_slice_vertices = calculate_search_slice_vertices(actor2_position, actor2_velocity)
actor2_search_slice_area = calculate_polygon_area(actor2_search_slice_vertices)
actor3_search_slice_vertices = calculate_search_slice_vertices(actor3_position, actor3_velocity)
actor3_search_slice_area = calculate_polygon_area(actor3_search_slice_vertices)

# Calculate the total area coverage for each actor
actor1_total_coverage = actor1_search_slice_area
actor2_total_coverage = actor2_search_slice_area
actor3_total_coverage = actor3_search_slice_area


# In[ ]:


# Given a vector of three vertices draw a polygon with specified linethinkness and color
def draw_polygon(vertices, linethick, linecolor):
    if len(vertices)>1:
        prevvertex = vertices[-1]
        for v in range(0, len(vertices)):
            currentvertex = vertices[v]
            plt.plot([prevvertex[0], currentvertex[0]], 
                     [prevvertex[1], currentvertex[1]],
                     linewidth=linethick, color=linecolor)
            prevvertex = currentvertex


# In[ ]:


# Calculates the intersection point of a line segment and a plane defined by a normal and a point
def calculate_intersection_point(start_point, end_point, normal, point):
    line_vector = subtractvecs(end_point, start_point)
    line_direction = normalize(line_vector)
    distance_to_plane = np.dot(normal, subtractvecs(point, start_point)) / np.dot(normal, line_direction)
    intersection_point =  displacevec(start_point, scalevec(line_direction, distance_to_plane))
    return intersection_point


# In[ ]:


# A function to clip a polygon against a plane defined by a normal and a point
def clip_polygon(vertices, normal, point):
    clipped_vertices = []
    prev_vertex = vertices[-1]
    prev_distance = np.dot(normal, subtractvecs(prev_vertex, point))
    for i in range(0, len(vertices)):
        current_vertex = vertices[i]
        current_distance = np.dot(normal, subtractvecs(current_vertex, point))
        if (current_distance * prev_distance < 0):
            intersection_point = calculate_intersection_point(prev_vertex, current_vertex, normal, point)
            clipped_vertices.append(intersection_point)
        if (current_distance >= 0):
            clipped_vertices.append(current_vertex)
        prev_vertex = current_vertex
        prev_distance = current_distance
    return clipped_vertices


# In[ ]:


# Calculate the overlap of two convex polygons and return the intersection area and outline
def calculate_polygon_overlap(vertices1, vertices2):
    p1 = Polygon(vertices1)
    p2 = Polygon(vertices2)
    if p1.intersects(p2):
        overlap = p1.intersection(p2)
        overlaparea = overlap.area
        if overlaparea > 0.0:
            return overlaparea, list(overlap.exterior.coords)[:-1]
        else:
            return 0, []
    else:
        return 0, []


# In[ ]:


# Calculate the redundant area coverage for each pair of actors
def debugcalculations():
    actor1_actor2_overlap = calculate_polygon_overlap(actor1_search_slice_vertices, actor2_search_slice_vertices)
    actor1_actor2_redundant_coverage = actor1_actor2_overlap
    actor1_actor3_overlap = calculate_polygon_overlap(actor1_search_slice_vertices, actor3_search_slice_vertices)
    actor1_actor3_redundant_coverage = actor1_actor3_overlap
    actor2_actor3_overlap = calculate_polygon_overlap(actor2_search_slice_vertices, actor3_search_slice_vertices)
    actor2_actor3_redundant_coverage = actor2_actor3_overlap

    # Calculate the total area coverage for each actor
    actor1_total_coverage = actor1_search_slice_area
    actor2_total_coverage = actor2_search_slice_area
    actor3_total_coverage = actor3_search_slice_area

    # Calculate the redundant area coverage for each pair of actors
    actor1_actor2_overlap, a1cv = calculate_polygon_overlap(actor1_search_slice_vertices, actor2_search_slice_vertices)
    # print("a1cv = "+str(a1cv))
    actor1_actor2_redundant_coverage = actor1_actor2_overlap
    actor1_actor3_overlap, a2cv = calculate_polygon_overlap(actor1_search_slice_vertices, actor3_search_slice_vertices)
    # print("a2cv = "+str(a2cv))
    actor1_actor3_redundant_coverage = actor1_actor3_overlap
    actor2_actor3_overlap, a3cv = calculate_polygon_overlap(actor2_search_slice_vertices, actor3_search_slice_vertices)
    # print("a3cv = "+str(a3cv))
    actor2_actor3_redundant_coverage = actor2_actor3_overlap

    # Calculate the total area coverage for all three actors
    total_coverage = actor1_total_coverage + actor2_total_coverage + actor3_total_coverage

    # Calculate the redundant area coverage for all three actors
    redundant_coverage = actor1_actor2_overlap + actor1_actor3_overlap + actor2_actor3_overlap


# In[ ]:


# Debugging printout
# print("actor total coverage "+str([actor1_total_coverage, actor2_total_coverage, actor3_total_coverage]))
# print("1-2, 1-3, 2-3 overlaps "+str([actor1_actor2_overlap, actor1_actor3_overlap, actor2_actor3_overlap]))


# In[ ]:


# actor1 = red, actor2 = green, actor3 = blue
def drawPolygons(pred, pgreen, pblue):
    draw_polygon(pred,   2, 'red')
    draw_polygon(pgreen, 2, 'green')
    draw_polygon(pblue,  2, 'blue')
    draw_polygon(a1cv,   3, 'black')
    draw_polygon(a2cv,   3, 'black')
    draw_polygon(a3cv,   3, 'black')
    plt.show()


# In[ ]:


# Debugging plot
# drawPolygons(actor1_search_slice_vertices, actor2_search_slice_vertices, actor3_search_slice_vertices)


# In[ ]:


class Player:
    def __init__(self, pname, pcolor):
        self.inRegion = "Nowhere"    # Which region te Player is in
        self.pname = pname
        self.search_slice = []       # The vertices
        self.search_slice_area = []  # Area of the triangle
        self.pcolor = pcolor         # Color for drawing
        self.position = [0.0, 0.0]   # Position of the player
        self.velocity = [0.0, 0.0]   # Velocity vector
        self.stationarySince = 0
        self.overlap = {}
        self.position10s = [0.0, 0.0]
        self.position30s = [0.0, 0.0]
        self.position60s = [0.0, 0.0]
        self.position180s = [0.0, 0.0]
        self.distance10s = 0.0
        self.distance30s = 0.0
        self.distance60s = 0.0
        self.distance180s = 0.0
        self.stationary10s = 0.0
        self.stationary30s = 0.0
        self.stationary60s = 0.0
        self.stationary180s = 0.0

    def set_position_and_velocity(self, ppos, pvel):
        self.position = ppos
        self.velocity = pvel
        self.search_slice = calculate_search_slice_vertices(self.position, self.velocity)
        self.search_slice_area = calculate_polygon_area(self.search_slice)
    def set_vertices(self, slice_vertices):
        self.search_slice = slice_vertices
    def pprinter(self):
        print("name = "+str(self.pname))
        print("position = "+str(self.position))
        print("velocity = "+str(self.velocity))
        print("color = "+str(self.pcolor))
        print("area = "+str(self.search_slice_area))
        print("vertices = "+str(self.search_slice))
        print("overlap = "+str(self.overlap))
        print()
    def reset_overlap(self):
        self.overlap={}
    def add_overlap(self, playername, olarea, olvertices):
        self.overlap[playername]=[olarea, olvertices]
    def initialize_player(self, initialposition):
        self.position = initialposition
        self.position10s = initialposition
        self.position30s = initialposition
        self.position60s = initialposition
        self.position180s = initialposition
    def distance_period(self, period):
        if period == 10:
            return magnitude(subtractvecs(self.position, self.position10s))
        elif period == 30:
            return magnitude(subtractvecs(self.position, self.position30s))
        elif period == 60:
            return magnitude(subtractvecs(self.position, self.position60s))
        elif period == 180:
            return magnitude(subtractvecs(self.position, self.position180s))
        else:
            print("Bad argument 'period', to distance_period, you provided "+str(period)+" it must be 10,30, 60, or 180")            
    def reset_position_period(self, period):
        if period == 10:
            self.position10s = self.position
        elif period == 30:
            self.position30s = self.position
        elif period == 60:
            self.position60s = self.position
        elif period == 180:
            self.position180s = self.position
        else:
            print("Bad argument 'period', to reset_position_period, you provided "+str(period)+" it must be 10,30, 60, or 180")


# In[ ]:


# Automatically generate Monads, Diads, and Triads from a list of objects.

def monads(x):
    monads = []
    for p1 in x:
        monads.append([p1])
    return monads

def dyads(x):
    dyads = []
    used = []
    for p1 in x:
        used.append(p1)
        for p2 in x:
            if not (p2 in used):
                dyads.append([p1, p2])
    return dyads

def triads(x):
    triads = []
    used = []
    for p1 in x:
        used.append(p1)
        used2 = []
        for p2 in x:
            if not (p2 in used):
                used2.append(p2)
                for p3 in x:
                    if (not (p3 in used)) and (not (p3 in used2)):
                        triads.append([p1, p2, p3])
    return triads

def monads_dyads_and_triads(players):
    cases = monads(players)
    for adyad in dyads(players):
        cases.append(adyad)
    for atriad in triads(players):
        cases.append(atriad)
    return cases
                 
# print("monads = "+str(monads([1, 2, 3, 4])))
# print("dyads = "+str(dyads([1, 2, 3, 4])))
# print("triads = "+str(triads([1, 2, 3, 4])))
# print("monads, dyads, triads = "+str(monads_dyads_and_triads([1, 2, 3, 4])))
# print("monads, dyads, triads = "+str(monads_dyads_and_triads([1, 2, 3])))


# In[ ]:


# The following function takes in a list of players and first calculates the area covered
# by each player's search slice using the calculate_area function. It then checks for overlaps
# between each pair of players using the calculate_overlap_area function. If an overlap is
# found, the area of overlap is added to the areas covered by each of the overlapping players
# in the areas dictionary. Finally, the function returns the areas dictionary containing the
# total areas covered by each player.

def calculate_overlap(players):
    #  Check for overlaps between each pair of players
    for i in range(len(players)):
        players[i].reset_overlap()
    for i in range(len(players)):
        for j in range(i+1, len(players)):
            player1 = players[i]
            player2 = players[j]
            p1name = player1.pname
            p2name = player2.pname
            
            # print("Computing overlap between "+p1name+" and "+p2name)
            overlap, ov = calculate_polygon_overlap(player1.search_slice, player2.search_slice)
            if overlap > 0:
                # print(p1name+" and "+p2name+" overlap with area "+str(overlap))
                player1.add_overlap(p2name, overlap, ov)
                player2.add_overlap(p1name, overlap, ov)


# In[ ]:


def calculate_overlap_and_non_overlap_summations(players):
    summation = []
    team_subgroups = monads_dyads_and_triads(players)
    # print("Team subgroups = "+str(team_subgroups))
    for subteam in team_subgroups:
        # print("subteam = "+str(subteam))
        team_names = [pl.pname for pl in subteam]
        subteam_overlap = []
        subteam_nonoverlap = [pl.search_slice_area for pl in subteam]
        for aplayer in subteam:
            other_players = [pl for pl in team_names if not (pl == aplayer.pname)]
            other_player_overlaps = [aplayer.overlap[pl][0] 
                                     for pl in other_players 
                                     if ((pl in aplayer.overlap) and aplayer.overlap[pl])]
            # print("Other player overlaps of "+aplayer.pname+" = "+str(other_player_overlaps)+" sums to "+str(sum(other_player_overlaps)))
            subteam_overlap.append([aplayer, sum(other_player_overlaps), other_player_overlaps])
        rnonoverlap = sum(subteam_nonoverlap)-sum(sub[1] for sub in subteam_overlap)
        roverlap = sum(sub[1] for sub in subteam_overlap)
        ratio = roverlap / rnonoverlap if rnonoverlap != 0 else roverlap / 0.000001
        summation.append({"nad" : team_names, "nonoverlap" : rnonoverlap, "overlap": roverlap, 'ratio': ratio})
    # pprint(summation)
    return summation


# In[ ]:


def print_players():
    global players
    for i in range(len(players)):
        players[i].pprinter()
    pprint(calculate_overlap_and_non_overlap_summations(players))


# In[ ]:


def player_region_map():
    global players
    regionmap = {}
    for pl in players:
        posX = pl.position[0]
        posZ = pl.position[1]
        regionmap[pl.pname] = myRegion(posX, posZ)
    return regionmap


# In[ ]:


def get_player_by_name(pname):
    global players
    for i in range(len(players)):
        if players[i].pname == pname:
            return players[i]
    return None


# In[ ]:


def forceAspect(ax,aspect=1):
    im = ax.get_images()
    extent =  im[0].get_extent()
    ax.set_aspect(abs((extent[1]-extent[0])/(extent[3]-extent[2]))/aspect)


# In[ ]:


def plot_players():
    global players
    if len(players) == 0:
        print("No players found")
    figure, axes = plt.subplots() 
    axes.axis('equal')
    axes.set(xlim=(-47, 51), ylim=(0, 151))
    axes.set_axis_off()
    plt.gca().invert_yaxis()
    # Plot the testbed area consisting of the field and the area that containst the store.
    plt.plot([-47, 51, 51, -47, -47], [1, 1, 150, 150, 1],  linewidth=1, color='blue')
    # Plot the wall separating the desert and the village.
    plt.plot([1, 51],  [50, 50], linewidth=1, color='blue')
    # Plot the wall separating the village and the forest.
    plt.plot([1, 51],  [100, 100], linewidth=1, color='blue')
    # Plot the wall separating the field from the store side
    plt.plot([1, 1],  [1, 150], linewidth=1, color='blue')
    # Plot the store
    plt.plot([-47, -47, -38, -38, -47],  [2, 10, 10, 2, 2], linewidth=1, color='blue')    
    for i in range(len(players)):
        aplayer=players[i]
        pc = plt.Circle(aplayer.position, 1.0, color=aplayer.pcolor)
        axes.add_artist(pc)
        draw_polygon(aplayer.search_slice, 2, aplayer.pcolor)
        overlaps = list(map(list, (vl for vl in aplayer.overlap.values())))
        print("overlaps = "+str(overlaps))
        for poly in overlaps:
            draw_polygon(poly[1], 3, 'black')
    plt.show()


# In[ ]:


class Boid:
    def __init__(self, initialposition, initialVelocity):
        self.position         = initialposition
        self.velocity         = initialVelocity
        self.acceleration     = [0.0, 0.0]
        self.maxspeed         = MaxSpeed
        self.maxforce         = MaxForce
        self.separationRadius = SeparationRadius
        self.alignmentRadius  = AlignmentRadius
        self.cohesionRadius   = CohesionRadius
        self.separationWeight = SeparationWeight
        self.alignmentWeight  = AlignmentWeight
        self.cohesionWeight   = CohesionWeight


# In[ ]:


# Calculate the separation vector
def calculateSeparationVector(boid, boidList):
    global currentTime
    separationVector = [0, 0]     # average
    steering = [0, 0]
    count = 0
    # Loop through each boid in the list
    for otherboid in boidList:
        if boid != otherboid:
            # Calculate the distance between the two boids
            displacement = subtractvecs(boid.position, otherboid.position)
            distance = magnitude(displacement)
            print("@ "+str(currentTime)+" Distance = "+str(distance))
            # If the other boid is within the separation radius, add the separation vector
            if distance < boid.separationRadius:
                separationVector = displacevec(separationVector, dividevec(normalize(displacement), distance))
                count += 1
    # If there are other boids within the separation radius, divide the separation vector by the count
    if count > 0:
        separationVector = dividevec(separationVector, count+1)
    if norm(steering) > 0:
        separationVector = scalevec(dividevec(separationVector, norm(steering)), boid.maxspeed)
    steering = subtractvecs(separationVector, boid.velocity)
    if norm(steering) > boid.maxforce:
        steering = scalevec(dividevec(steering, norm(steering)), boid.maxforce)
    return steering # was separationVector


# In[ ]:


# Calculate the alignment vector
def calculateAlignmentVector(boid, boidList):
    alignmentVector = [0, 0]     # average
    steering = [0, 0]
    count = 0
    # Loop through each boid in the list
    for otherboid in boidList:
        # Calculate the distance between the two boids
        distance = magnitude(subtractvecs(otherboid.position, boid.position))
        # If the other boid is within the alignment radius, add the other boid
        if distance < boid.alignmentRadius:
            alignmentVector = displacevec(alignmentVector, otherboid.velocity)
            count += 1
    if count > 0:
        alignmentVector = dividevec(alignmentVector, count)
        if norm(steering) > 0:
            alignmentVector = scalevec(dividevec(alignmentVector, norm(steering)), boid.maxspeed)
        steering = subtractvecs(alignmentVector, boid.velocity)
        if norm(steering) > boid.maxforce:
            steering = scalevec(dividevec(steering, norm(steering)), boid.maxforce)
    return steering


# In[ ]:


# Calculate cohesion vector
def calculateCohesionVector(boid, boidList):
    centerOfMass = [0, 0]     # center of mass
    steering = [0, 0]
    count = 0
    # Loop through each boid in the list
    for otherboid in boidList:
        displacement = subtractvecs(otherboid.position, boid.position)
        if norm(displacement) < boid.cohesionRadius:
            cohesionVector = displacevec(centerOfMass, otherboid.position)
            count += 1
    if count > 1:
        centerOfMass = dividevec(centerOfMass, count)
        vecToCoM = subtractvecs(centerOfMass, boid.position)
        if norm(vecToCoM) > 0.0:
            vecToCoM = scalevec(dividevec(vecToCoM, norm(vecToCoM)), boid.maxspeed)
        steering = subtractvecs(vecToCoM, boid.velocity)
        if norm(steering) > boid.maxforce:
            steering = scalevec(dividevec(steering, norm(steering)), boid.maxforce)
    return steering


# In[ ]:


# Define the function to update the boid motion variables
def updateBoidMotion(boidList, deltaTime):
  # Loop through each boid in the list
  for boid in boidList: 
    # Calculate the separation, alignment, and cohesion vectors
    separationVector =  calculateSeparationVector(boid, boidList)
    print("SeparationVector="+str(separationVector))
    alignmentVector  =  calculateAlignmentVector(boid, boidList)
    print("AlignmentVector="+str(alignmentVector))
    cohesionVector   =  calculateCohesionVector(boid, boidList)
    print("CohesionVector="+str(cohesionVector))
    
    # Apply the weights to each vector
    separationVector = scalevec(separationVector, boid.separationWeight)
    alignmentVector  = scalevec(alignmentVector, boid.alignmentWeight)
    cohesionVector   = scalevec(cohesionVector, boid.cohesionWeight)

    # Add the vectors to the boid's acceleration
    boid.acceleration = displacevec(boid.acceleration, 
                                    displacevec(separationVector, 
                                                displacevec(alignmentVector, cohesionVector)))

    # Limit the acceleration to the boid's maximum speed
    if (magnitude(boid.acceleration) > boid.maxspeed):
        boid.acceleration = scalevec(normalize(boid.acceleration), boid.maxspeed)

    # Update the boid's position and velocity
    boid.velocity = displacevec(boid.velocity, scalevec(boid.acceleration, deltaTime))
    if (magnitude(boid.velocity) > boid.maxspeed): 
        boid.velocity = scalevec(normalize(boid.velocity), boid.maxspeed)
        
    boid.position = displacevec(boid.position, scalevec(boid.velocity, deltaTime))

    # Reset the boid's acceleration
    boid.acceleration = [0, 0]


# In[ ]:


def reset_ac():
    global currentTime
    global sampled10time
    global sampled30time
    global sampled60time
    global sampled180time
    global players
    global boids
    global boids_to_players
    global players_to_boids
    global test_data
    global inStore
    global inStoreISO
    global visitsToStore
    global timeInStore

    currentTime = -1.0
    players = []
    boids   = []
    boids_to_players = {}
    players_to_boids = {}
    currentTime    = 0.0
    sampled10time  = 0.0
    sampled30time  = 0.0
    sampled60time  = 0.0
    sampled180time = 0.0
    inStore        = False
    inStoreISO     = False
    visitsToStore  = 0
    timeInStore    = 0.0

    print("AC reset")
# reset_ac()


# In[ ]:


def update_player_stationary_status(aplayer, x, y, z, motion_x, motion_y, motion_z):
    if (motion_x == 0.0) and (motion_y == 0.0) and (motion_z == 0.0):
        if not aplayer.stationarySince:
            aplayer.stationarySince = elapsed_ms_in_the_field()
    else:
        if aplayer.stationarySince:
            stationaryTime = elapsed_ms_in_the_field() - aplayer.stationarySince
            aplayer.stationary10s  += stationaryTime
            aplayer.stationary30s  += stationaryTime
            aplayer.stationary60s  += stationaryTime
            aplayer.stationary180s += stationaryTime
        aplayer.stationarySince = False


# In[ ]:


def maybe_add_player(name, em, x, y, z, motion_x, motion_y, motion_z):
    global currentTime, boids_to_players, players_to_boids, players, boids, availablecolors
    existingPlayer = get_player_by_name(name)
    if not existingPlayer:
        newPlayerColor = availablecolors[len(players)%len(availablecolors)]
        newPlayer = Player(name, newPlayerColor)
        newPlayer.stationarySince = em
        newPlayer.set_position_and_velocity([x, z], [motion_x, motion_z])
        newPlayer.initialize_player([x, z])
        players.append(newPlayer)
        newBoid = Boid([x, z], [motion_x, motion_z])
        boids.append(newBoid)
        boids_to_players[newBoid] = newPlayer
        players_to_boids[newPlayer] = newBoid
        print("New player "+str(newPlayer.pname)+" added")
    else:
        previous_position = existingPlayer.position
        previous_velocity = existingPlayer.velocity
        distanceTravelled = magnitude(subtractvecs([x, z], previous_position))
        if distanceTravelled > 0:
            existingPlayer.distance10s    += distanceTravelled
            existingPlayer.distance30s    += distanceTravelled
            existingPlayer.distance60s    += distanceTravelled
            existingPlayer.distance180s   += distanceTravelled
        existingPlayer.set_position_and_velocity([x, z], [motion_x, motion_z])
        existingBoid = players_to_boids[existingPlayer]
        existingBoid.position = [x, z]
        existingBoid.velocity = [motion_x, motion_z]
        update_player_stationary_status(existingPlayer, x, y, z, motion_x, motion_y, motion_z)
    calculate_overlap(players)
    return get_player_by_name(name)


# In[ ]:


#####################################################################
# Initialization

# BOIDS have their future paths calculated by the flocking algorithm
# Players have their position updated by arriving messages
# Player values represent the current known state of reality
# Boid values are updated to project in to the future.

def initialize_in_notebook():
    global currentTime 
    global boids_to_players
    global players_to_boids
    global players
    global boids
    global actor1_position 
    global actor1_velocity 
    global actor2_position 
    global actor2_velocity 
    global actor3_position 
    global actor3_velocity 
    players = []
    boids   = []
    boids_to_players = {}
    players_to_boids = {}
    maybe_add_player('Player1', currentTime, actor1_position[0], 0, actor1_position[1], actor1_velocity[0], 0, actor1_velocity[1])
    maybe_add_player('Player2', currentTime, actor2_position[0], 0, actor2_position[1], actor2_velocity[0], 0, actor2_velocity[1])
    maybe_add_player('Player3', currentTime, actor3_position[0], 0, actor3_position[1], actor3_velocity[0], 0, actor3_velocity[1])
    print("Players and Boids Initialized")
    
initialize_in_notebook()


# In[ ]:


# Uncomment to plot players

# plot_players()


# In[ ]:


# print_players()


# In[ ]:


def plot_boids():
    global players, players_to_boids
    if len(players) == 0:
        print("No players found")
    else:
        figure, axes = plt.subplots() 
        axes.axis('equal')
        axes.set(xlim=(-47, 51), ylim=(0, 151))
        axes.set_axis_off()
        plt.gca().invert_yaxis()
        # Plot the testbed area consisting of the field and the area that containst the store.
        plt.plot([-47, 51, 51, -47, -47], [1, 1, 150, 150, 1],  linewidth=1, color='blue')
        # Plot the wall separating the desert and the village.
        plt.plot([1, 51],  [50, 50], linewidth=1, color='blue')
        # Plot the wall separating the village and the forest.
        plt.plot([1, 51],  [100, 100], linewidth=1, color='blue')
        # Plot the wall separating the field from the store side
        plt.plot([1, 1],  [1, 150], linewidth=1, color='blue')
        # Plot the store
        plt.plot([-47, -47, -38, -38, -47],  [2, 10, 10, 2, 2], linewidth=1, color='blue')    
        for i in range(len(players)):
            aplayer=players[i]
            aboid=players_to_boids[aplayer]
            cc = plt.Circle(aboid.position, 2.0, color=aplayer.pcolor)
            axes.add_artist(cc) 
        plt.show()    


# In[ ]:


# print(get_player_by_name('Player2'))
# print(get_player_by_name('Player5'))


# In[ ]:


# Calculate the separation, alignment, and cohesion parameters for a single boid
def calculateSACParameters(boid, otherBoids, deltaTime):
    global currentTime
    # Calculate the separation, alignment, and cohesion vectors for the boid
    separationVector = [0.0, 0.0]
    alignmentVector  = [0.0, 0.0]
    cohesionVector   = [0.0, 0.0]
    separationCount  = 0
    alignmentCount   = 0
    cohesionCount    = 0
    for otherBoid in otherBoids:
        if not (otherBoid == boid):
            distance = magnitude(subtractvecs(boid.position, otherBoid.position))
            # print("@ "+str(currentTime)+" boid distance = "+str(distance))
            if distance < boid.separationRadius:
                separationVector = displacevec(separationVector, 
                                               dividevec(normalize(subtractvecs(boid.position, 
                                                                                otherBoid.position)), 
                                                         distance))
                separationCount += 1
            if distance < boid.alignmentRadius:
                alignmentVector = displacevec(alignmentVector, normalize(otherBoid.velocity))
                alignmentCount += 1
            if distance < boid.cohesionRadius:
                cohesionVector = displacevec(cohesionVector, otherBoid.position)
                cohesionCount += 1
    if separationCount > 0:
        separationVector = dividevec(separationVector, separationCount)
    if alignmentCount > 0:
        alignmentVector = dividevec(alignmentVector, alignmentCount)
    if cohesionCount > 0:
        cohesionVector = dividevec(cohesionVector, cohesionCount)
        # cohesionVector = normalize(subtractvecs(cohesionVector, boid.position)) 
    # Calculate the separation, alignment, and cohesion parameters for the boid
    separationRadius = boid.separationRadius
    if separationCount > 0:
        separationRadius = magnitude(separationVector)*separationRadius
    separationWeight = 1 / (2 * separationRadius) 
    alignmentRadius = boid.alignmentRadius
    if alignmentCount > 0:
        alignmentRadius = magnitude(alignmentVector)*alignmentRadius
    if alignmentRadius > 0:
        alignmentWeight = 1 / (2 * alignmentRadius)
    else: 
        alignmentWeight = 0.0
    cohesionRadius = boid.cohesionRadius
    if cohesionCount > 0:
        cohesionRadius = magnitude(cohesionVector)*cohesionRadius 
    if cohesionRadius > 0:
        cohesionWeight = 1 / (2 * cohesionRadius)
    else:
        cohesionWeight = 0.0
    return separationWeight, alignmentWeight, cohesionWeight


# In[ ]:


# Calculate the average separation, alignment, and cohesion parameters of the three boids at each timestep
def calculateAverageSACParameters(boids, deltaTime):
    separationWeightSum = 0
    alignmentWeightSum  = 0
    cohesionWeightSum   = 0
    for boid in boids:
        otherBoids = [otherBoid for otherBoid in boids if otherBoid != boid]
        separationWeight, alignmentWeight, cohesionWeight = calculateSACParameters(boid, otherBoids, deltaTime)
        separationWeightSum += separationWeight
        alignmentWeightSum  += alignmentWeight
        cohesionWeightSum   += cohesionWeight
    separationWeightAvg = separationWeightSum / len(boids)
    alignmentWeightAvg  = alignmentWeightSum  / len(boids)
    cohesionWeightAvg   = cohesionWeightSum   / len(boids)
    return separationWeightAvg, alignmentWeightAvg, cohesionWeightAvg


# In[ ]:


def player_distance_for_period(aplayer, period):
    if period == 10:
        return aplayer.distance10s
    if period == 30:
        return aplayer.distance30s
    if period == 60:
        return aplayer.distance60s
    if period == 180:
        return aplayer.distance180s
    else:
        print("incorrect period "+str(period))
        return -1.0


# In[ ]:


def reset_player_distance_for_period(aplayer, period):
    aplayer.reset_position_period(period)
    if period == 10:
        aplayer.distance10s  = 0.0
    elif period == 30:
        aplayer.distance30s  = 0.0
    elif period == 60:
        aplayer.distance60s  = 0.0
    elif period == 180:
        aplayer.distance180s = 0.0
    else:
        print("reset incorrect period "+str(period))


# In[ ]:


def player_stationary_for_period(aplayer, period):
    if period == 10:
        return aplayer.stationary10s
    if period == 30:
        return aplayer.stationary30s
    if period == 60:
        return aplayer.stationary60s
    if period == 180:
        return aplayer.stationary180s
    else:
        print("incorrect period "+str(period))
        return -1.0


# In[ ]:


def reset_player_stationary_for_period(aplayer, period):
    if period == 10:
        aplayer.stationary10s  = 0.0
    elif period == 30:
        aplayer.stationary30s  = 0.0
    elif period == 60:
        aplayer.stationary60s  = 0.0
    elif period == 180:
        aplayer.stationary180s = 0.0
    else:
        print("reset incorrect period "+str(period))


# In[ ]:


def calculate_distance_results(players, period, deltaTime):
    distances = {}
    for pl in players:
        distances[pl.pname] = {'incremental':player_distance_for_period(pl, period), 'straightline': pl.distance_period(period)}
        reset_player_distance_for_period(pl, period)
    return distances


# In[ ]:


def calculate_stationary_results(players, period, deltaTime):
    stationary = {}
    for pl in players:
        stationary[pl.pname] = player_stationary_for_period(pl, period)
        reset_player_stationary_for_period(pl, period)
    return stationary


# In[ ]:


def generate_result_payload_to_publish(period, deltaTime):
    global players
    global boids
    global inStore
    global visitsToStore
    global timeInStore
    global currentTime
    
    summation_results = calculate_overlap_and_non_overlap_summations(players)
    separationWeightAvg, alignmentWeightAvg, cohesionWeightAvg = calculateAverageSACParameters(boids, deltaTime)
    distance_results = calculate_distance_results(players, period, deltaTime)
    
    stationary_results = calculate_stationary_results(players, period, deltaTime)
    payload = {"elapsed_milliseconds_global" : currentTime,
               "period"                      : period,
               "td"                          : deltaTime,
               "elapsed_ms_field"            : elapsed_ms_in_the_field(),
               "time_in_store"               : timeInStore,
               "visits_to_store"             : visitsToStore,
               "phase"                       : "exploration" if visitsToStore == 0 else "bomb_disposal",
               "region"                      : player_region_map(),
               "reconaissance":{"summation"    : summation_results,
                              "distance"     : distance_results,
                              "stationary"   : stationary_results},
               "flocking":   {"separation"   : separationWeightAvg,
                              "cohesion"     : cohesionWeightAvg,
                              "alignment"    : alignmentWeightAvg}}
    return payload


# In[ ]:


def maybe_generate_publishable_payload():
    global currentTime
    global sampled10time
    global sampled30time
    global sampled60time
    global sampled180time
    global players
    global boids
    global inStore
    global boids_to_players
    global players_to_boids

    if inStore:
        return None
    fieldTime = elapsed_ms_in_the_field()
    publications = []
    deltaTime10  = fieldTime - sampled10time
    deltaTime30  = fieldTime - sampled30time
    deltaTime60  = fieldTime - sampled60time
    deltaTime180 = fieldTime - sampled180time
    # print("currentTime = "+str(currentTime)+" deltaTime10 = "+str(deltaTime10))
    if (deltaTime10/10000) >= 1.0:
        publications.append(generate_result_payload_to_publish(10, deltaTime10))
        sampled10time = fieldTime
    if (deltaTime30/30000) >= 1.0:
        publications.append(generate_result_payload_to_publish(30, deltaTime30))
        sampled30time = fieldTime
    if (deltaTime60/60000) >= 1.0:
        publications.append(generate_result_payload_to_publish(60, deltaTime60))
        sampled60time = fieldTime
    if (deltaTime180/180000) >= 1.0:
        publications.append(generate_result_payload_to_publish(180, deltaTime180))
        sampled180time = fieldTime
    if publications:
        return publications
    else:
        return None


# In[ ]:


# Uncomment to see example data

# pprint(generate_result_payload_to_publish(10, 10.0)) 


# In[ ]:


def import_test_data(path):
    global test_data
    testdatafile = open(path, "r")
    datastring = testdatafile.read()  # Read the whole file as a string
    test_data = json.loads(datastring)


# In[ ]:


def updatePlayerState(pid, x, y, z, motion_x, motion_y, motion_z):
    thisplayer = maybe_add_player(pid, currentTime, x, y, z, motion_x, motion_y, motion_z)
    return maybe_generate_publishable_payload()


# In[ ]:


def handlemessage(message):
    global inStore
    global inStoreISO
    global visitsToStore
    global timeInStore
    global currentTime
    
    currentTime = message['elapsed_milliseconds_global']
    msTimeISO = convert_isodatetime_to_timestamp(message['timestamp'])*1000
    if not (currentTime == -1.0):   # Don't track players until the mission timer has started.
        xPos = message['x']
        if xPos < -40:
            if (not inStore) and (len(players) >= 3):
                print("Entering store")
                inStore = currentTime
                inStoreISO = msTimeISO
                return None
        else:
            if inStore:
                print("Leaving store")
                #timeInStore += (currentTime - inStore)  # current time minus time entered
                timeInStore += (msTimeISO - inStoreISO)  # current time minus time entered
                visitsToStore += 1
                inStore = False
                inStoreISO = False
            return updatePlayerState(message['participant_id'],
                                     xPos, 
                                     message['y'], 
                                     message['z'],
                                     message['motion_x'], 
                                     message['motion_y'], 
                                     message['motion_z'])
    else:
        return None


# In[ ]:


def run_boid_iteration():
    global boids
    plot_boids()
    updateBoidMotion(boids, 1.0)


# In[ ]:


# Uncomment to see the BOID simulation for 100 time steps starting from the current position.

#for i in range(100):
#    run_boid_iteration()


# In[ ]:


def test_data_run(sleeptimesecs, maxiterations):
    global currentTime
    reset_ac()
    iteration = 0
    for msg in test_data:
        iteration += 1
        if (maxiterations > 0) and (iteration > maxiterations):
            print("Maximum iterations ("+str(iteration-1)+")")
            return
        # pprint(msg)
        publications = handlemessage(msg)
        if publications:
            print("Publications at "+str(int(currentTime/1000))+
                  " seconds, in field time = "+str(elapsed_ms_in_the_field()))
            plot_players()
            pprint(publications)
        if sleeptimesecs:
            time.sleep(sleeptimesecs) 


# In[ ]:


def type_of_script():
    try:
        ipy_str = str(type(get_ipython()))
        if 'zmqshell' in ipy_str:
            return 'jupyter'
        if 'terminal' in ipy_str:
            return 'ipython'
    except:
        return 'terminal'
    
# type_of_script()


# In[ ]:


if type_of_script() == 'jupyter':
    import_test_data("./PTM000045_TestbedData_Trial-2.log_movements.json")
#    import_test_data("./1492fbf9-474c-4d8d-9ece-e4e279da36148249129227323234193.log_movements.json")
#    import_test_data("./4dcf07c5-6a5e-422a-bcf1-65113f1663e59936519975145533567.log_movements.json")
#    import_test_data("./5505c059-e11b-46f6-9078-5959b2a9a027.log_movements.json")
#    import_test_data("./5f8f8263-a79a-41e3-83ab-5edec17d820d16235526726260656117.log_movements.json")
#    import_test_data("./616e855d-b48c-4985-9229-69432f45a3cc.log_movements.json")
#    import_test_data("./61d5b4bb-ebff-4151-877a-657ab6be4fba29027595564643673.log_movements.json")
#    import_test_data("./79f186f2-28c5-481a-8b8f-47207b1225e317552820408735178903.log_movements.json")
#    import_test_data("./856fb7d1-faf4-4cab-9f40-1f658cd2f2418615004979383107323.log_movements.json")
#    import_test_data("./PTM000018_TestbedData.log_movements.json")
#    import_test_data("./PTM000023.log_movements.json")
#    import_test_data("./PTM000045_TestbedData_Trial-2.log_movements.json")
#    import_test_data("./a2e6307f-9722-4b09-b56c-358174d3c49613895295083685407482.log_movements.json")
#    import_test_data("./ad596a70-510d-43d9-8162-1fc3a03ef58b.log_movements.json")
#    import_test_data("./b2972ecc-5e69-4a52-bd92-1a5f3bb6eca1.log_movements.json")
#    import_test_data("./b2e94baf-9fe7-4115-95c2-9d0bc8468c5e15500241895528530220.log_movements.json")
#    import_test_data("./b5f3829c-b1bd-409b-8694-7bc0d6c0437911191332257780485152.log_movements.json")
#    import_test_data("./bfa06462-192e-4bd3-ba8f-bebc7e21c60a2246195786309801026.log_movements.json")
#    import_test_data("./bff3736d-d45f-42ed-9b99-6587d2f4cc813073921555802756336.log_movements.json")
#    import_test_data("./d74b9300-369d-4eda-adf5-1ee407fae6da6643582940789747473.log_movements.json")
#    import_test_data("./e1ddde3f-0cf5-4ce4-b051-ba4b9164bfaf9411723968892376208.log_movements.json")
#    import_test_data("./fe155686-cbed-4bed-b81c-d0a05ddfdbdd17112165289456741459.log_movements.json")
#    import_test_data("./team_12_reprocessed.log_movements.json")
#    import_test_data("./team_15_reprocessed.log_movements.json")
#    import_test_data("./team_17_reprocessed.log_movements.json")
#    import_test_data("./team_18_reprocessed.log_movements.json")
#    import_test_data("./team_19_reprocessed.log_movements.json")
#    import_test_data("./team_20_reprocessed.log_movements.json")
#    import_test_data("./team_21_reprocessed.log_movements.json")
#    import_test_data("./team_22_reprocessed.log_movements.json")
#    import_test_data("./team_23_reprocessed.log_movements.json")
#    import_test_data("./team_24_reprocessed.log_movements.json")
#    import_test_data("./team_25_reprocessed.log_movements.json")
#    import_test_data("./team_27_reprocessed.log_movements.json")
#    import_test_data("./team_28_reprocessed.log_movements.json")
#    import_test_data("./team_29_reprocessed.log_movements.json")
#    import_test_data("./team_33_reprocessed.log_movements.json")
#    import_test_data("./team_36_reprocessed.log_movements.json")
#    import_test_data("./team_37_reprocessed.log_movements.json")
#    import_test_data("./team_38_1_reprocessed.log_movements.json")
#    import_test_data("./team_38_2_reprocessed.log_movements.json")


# In[ ]:


# Uncomment to do test run from a file.
# test_data_run(False, 30000)


# In[ ]:


def pp_test_data():
    pprint(test_data)


# In[ ]:


# Uncomment to print the json data

# pp_test_data()


# In[ ]:


# Check if the user provided a file name
# if type_of_script() == 'terminal':
#     if len(sys.argv) < 2:
#         print("Please provide a file name as a command line argument.")
#         sys.exit(1)
#     # Open the file and read its contents
#     try:
#         with open(sys.argv[1], 'r') as file:
#             datastring = file.read()
#             test_data = json.loads(datastring)
#     except FileNotFoundError:
#         print(f"File not found: {sys.argv[1]}")
#         sys.exit(1)
#     test_data_run(False, -1 if len(sys.argv) < 3 else int(sys.argv[2]))

