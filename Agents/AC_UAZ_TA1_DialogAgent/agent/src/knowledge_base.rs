use std::collections::HashMap;

// The agent's working memory
#[derive(Debug, Default)]
pub struct KnowledgeBase {
    /// Mission stage (shop or field)
    pub stage: String,

    /// Mapping of participant IDs to callsigns
    pub callsign_mapping: HashMap<String, String>,

    /// Trial ID
    pub trial_id: Option<String>,

    /// Experiment ID
    pub experiment_id: Option<String>,
}
