use crate::messages::chat::Extraction;
use color_eyre::eyre::{Report, Result, WrapErr};
use color_eyre::Help;
use serde_json as json;

/// Get Odin extractions.
pub fn get_extractions(text: &str, event_extractor_url: &str) -> Result<Vec<Extraction>, Report> {
    let client = reqwest::blocking::Client::new();
    let response = client
        .post(event_extractor_url)
        .body(text.to_string())
        .send()
        .wrap_err("Failed to send request to event extractor!")
        .suggestion("Make sure the Odin event extraction service is running.")?
        .error_for_status()?;

    let response = &response.text()?;
    let extractions: Vec<Extraction> =
        json::from_str(response).expect(&format!("Unable to deserialize response {}", &response));

    Ok(extractions)
}

/// Get Odin extractions.
pub fn get_extraction_schemas(event_extractor_url: &str) -> Result<()> {
    let client = reqwest::blocking::Client::new();
    let response = client
        .get(event_extractor_url.to_owned() + "/schemas")
        .send()
        .wrap_err("Failed to send request to event extractor!")
        .suggestion("Make sure the Odin event extraction service is running.")?
        .error_for_status()?;

    let response = &response.text()?;

    Ok(json::from_str(response)?)
}
