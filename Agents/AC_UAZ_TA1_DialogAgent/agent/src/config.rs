use crate::messages::versioninfo::PubSub;
use serde::{Deserialize, Serialize};

#[derive(Default, Debug, Serialize, Deserialize, Clone)]
pub struct MqttOpts {
    pub host: String,
    pub port: u16,
}

#[derive(Default, Debug, Serialize, Deserialize, Clone)]
pub struct VersionInfo {
    pub agent_type: String,
    pub version: String,
    pub owner: String,
    pub publishes: Vec<PubSub>,
    pub subscribes: Vec<PubSub>,
}

/// Configuration
#[derive(Default, Debug, Serialize, Deserialize, Clone)]
pub struct Config {
    pub subscribe_topics: Vec<String>,
    pub client_id: String,
    pub mqtt: MqttOpts,
    pub event_extractor_url: String,
    pub custom_unigram_dict: Option<String>,
    pub custom_bigram_dict: Option<String>,
    pub custom_replacement_dict: Option<String>,
    pub spellchecker_ignore_list: Option<String>,
    pub boosted_phrase_list: Option<String>,
    pub versioninfo: VersionInfo,
}
