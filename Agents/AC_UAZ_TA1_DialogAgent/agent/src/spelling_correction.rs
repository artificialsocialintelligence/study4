use color_eyre::eyre::Result;
use log::{debug, error, info};
use serde_yaml;
use std::{
    collections::{BTreeMap, BTreeSet},
    path::Path,
};
use symspell::{AsciiStringStrategy, SymSpell, SymSpellBuilder, Verbosity};
use tokenizers::{
    normalizers::BertNormalizer,
    pre_tokenizers::{
        sequence::Sequence,
        split::{Split, SplitPattern},
        PreTokenizerWrapper,
    },
    tokenizer::{
        normalizer::{NormalizedString, OffsetReferential, SplitDelimiterBehavior},
        pre_tokenizer::{OffsetType, PreTokenizedString},
        Normalizer, PreTokenizer,
    },
};

pub struct SpellChecker {
    checker: SymSpell<AsciiStringStrategy>,
    normalizer: BertNormalizer,
    pre_tokenizer: Sequence,
    max_dictionary_edit_distance: i64,

    /// Dictionary for word replacement after tokenization.
    replacement_dictionary: Option<BTreeMap<String, String>>,

    /// Domain-specific words for the spellchecker to ignore.
    ignore_list: Option<BTreeSet<String>>,

    /// Domain-specific words for the spellchecker to boost.
    boosted_phrases: Option<BTreeSet<String>>,
}

impl SpellChecker {
    pub fn new(
        custom_unigram_dict: &Option<String>,
        custom_bigram_dict: &Option<String>,
        custom_replacement_dict: &Option<String>,
        ignore_list: &Option<String>,
        boosted_phrase_list: &Option<String>,
    ) -> Result<Self> {
        let max_dictionary_edit_distance = 3;

        // Initialize SymSpell checker
        let mut checker: SymSpell<AsciiStringStrategy> = SymSpellBuilder::default()
            .max_dictionary_edit_distance(max_dictionary_edit_distance)
            .build()
            .unwrap();

        let mut unigram_dict_path = "config/frequency_dictionary_en_82_765.txt";
        if let Some(path) = &custom_unigram_dict {
            if !Path::new(path).is_file() {
                error!(
                    "Specified custom bigram dictionary not found at {path}!\
                     Resorting to the default ({unigram_dict_path})."
                );
            } else {
                unigram_dict_path = path;
            }
        }
        info!("Loading unigram dictionary from {unigram_dict_path}");
        checker.load_dictionary(unigram_dict_path, 0, 1, " ");

        let mut bigram_dict_path = "config/frequency_bigramdictionary_en_243_342.txt";
        if let Some(path) = &custom_bigram_dict {
            if !std::path::Path::new(path).is_file() {
                error!(
                    "Specified custom bigram dictionary not found at {path}!\
                     Resorting to the default ({bigram_dict_path})."
                );
            } else {
                bigram_dict_path = path;
            }
        }

        //info!("Loading bigram dictionary from {bigram_dict_path}");
        //checker.load_bigram_dictionary(bigram_dict_path, 0, 2, " ");

        let replacement_dictionary = match &custom_replacement_dict {
            Some(path) => {
                if !std::path::Path::new(path).is_file() {
                    error!(
                        "Specified custom replacement dictionary not found at {path}!
                        Continuing without loading the custom replacement dictionary. \
                        This will likely lead to lower-quality spellchecking."
                    );
                    None
                } else {
                    let contents = std::fs::read_to_string(path).unwrap();
                    info!("Loading custom replacement dictionary from {path}...");
                    let dict: BTreeMap<String, String> = serde_yaml::from_str(&contents).unwrap();
                    Some(dict)
                }
            }
            None => None,
        };

        let ignore_list = match &ignore_list {
            Some(path) => {
                if !std::path::Path::new(path).is_file() {
                    error!(
                        "Specified custom ignore list not found at {path}!
                        Continuing without loading the custom ignore list. \
                        This will likely lead to lower-quality spellchecking."
                    );
                    None
                } else {
                    let contents = std::fs::read_to_string(path).unwrap();
                    info!("Loading custom ignore list from {path}...");
                    let list: BTreeSet<String> = serde_yaml::from_str(&contents).unwrap();
                    Some(list)
                }
            }
            None => None,
        };

        let boosted_phrases = match &boosted_phrase_list {
            Some(path) => {
                if !std::path::Path::new(path).is_file() {
                    error!(
                        "Specified boosted phrase list not found at {path}!
                        Continuing without loading the boosted phrase list. \
                        This will likely lead to lower-quality spellchecking."
                    );
                    None
                } else {
                    let contents = std::fs::read_to_string(path).unwrap();
                    info!("Loading boosted phrase list from {path}...");
                    let list: BTreeSet<String> = serde_yaml::from_str(&contents).unwrap();
                    Some(list)
                }
            }
            None => None,
        };

        let normalizer = BertNormalizer::default();

        // TODO: Implement the ability to perform custom text replacements prior to
        // pre-tokenization.
        let pre_tokenizer = Sequence::new(vec![PreTokenizerWrapper::Split(
            Split::new(
                SplitPattern::Regex(r"(\w+[;'](s|ve|m|t|ll|d|re))+|\w+|[^\w\s]".to_string()),
                SplitDelimiterBehavior::Removed,
                true,
            )
            .unwrap(),
        )]);

        Ok(SpellChecker {
            checker,
            normalizer,
            pre_tokenizer,
            max_dictionary_edit_distance,
            replacement_dictionary,
            ignore_list,
            boosted_phrases,
        })
    }

    pub fn correct(&self, text: &str) -> String {
        debug!("Correcting spelling.");
        debug!("Original text: {text}");

        let mut normalized_string = NormalizedString::from(text);
        self.normalizer.normalize(&mut normalized_string).unwrap();

        let mut pretokenized_string = PreTokenizedString::from(normalized_string);

        self.pre_tokenizer
            .pre_tokenize(&mut pretokenized_string)
            .unwrap();

        let corrected = pretokenized_string
            .get_splits(OffsetReferential::Original, OffsetType::Char)
            .iter()
            .map(|split| {
                let string = split.0.to_string();

                // Ignore words in the ignore list
                if let Some(list) = &self.ignore_list {
                    if list.contains(&string) {
                        return string;
                    }
                }

                // Ignore non-alphanumeric single-character strings
                if string.len() == 1 && !string.chars().nth(0).unwrap().is_alphanumeric()
                    // Ignore strings that are fully numeric
                    || string.chars().any(|c| c.is_numeric())
                {
                    string
                } else {
                    if let Some(dict) = &self.replacement_dictionary {
                        if let Some(entry) = dict.get(&string) {
                            return entry.to_string();
                        }
                    }

                    let suggestions: Vec<String> = self
                        .checker
                        .lookup(
                            &string,
                            Verbosity::Closest,
                            self.max_dictionary_edit_distance,
                        )
                        .into_iter()
                        .map(|s| s.term)
                        .collect();

                    // If there are no suggestions, return the string unchanged.
                    if suggestions.is_empty() {
                        return string;
                    }

                    // If there are suggestions, check whether they are in the boosted
                    // phrase list.

                    // Construct a list of suggestions that match ones in the set of boosted
                    // phrases.
                    let mut boosted_suggestions = Vec::<String>::new();

                    for suggestion in &suggestions {
                        if let Some(phrases) = &self.boosted_phrases {
                            if phrases.contains(suggestion) {
                                boosted_suggestions.push(suggestion.clone());
                            }
                        }
                    }

                    // If there is at least one boosted suggestion, take the first element in the
                    // list of boosted suggestions.
                    if !boosted_suggestions.is_empty() {
                        return boosted_suggestions[0].clone();
                    } else {
                        // If there are no matches in the boosted suggestion list, return the
                        // top suggestion.

                        // We may need to handle the case later when there are multiple matches.
                        if boosted_suggestions.len() > 1 {
                            debug!(
                                "Multiple boosted suggestion matches found, taking the first one."
                            );
                        }
                        return suggestions[0].clone();
                    }
                }
            })
            .collect::<Vec<String>>()
            .join(" ");

        debug!("Corrected text: {corrected}");
        corrected
    }
}
