//! File-based replayer.

use clap::Parser;
use color_eyre::eyre::Result;
use std::{
    fs::File,
    io::{self, BufRead},
};
use tomcat::{
    agent::Agent, config::Config, knowledge_base::KnowledgeBase,
    reprocessor_agent::ReprocessorAgent,
};

/// Command line arguments
#[derive(Parser, Debug)]
pub struct Cli {
    /// Input file
    pub input: String,

    /// Config file
    #[arg(short, long, default_value_t = String::from("config.yml"))]
    pub config: String,
}

fn main() -> Result<()> {
    // Initialize the logger from the environment
    pretty_env_logger::init();
    color_eyre::install()?;

    let args = Cli::parse();

    let cfg: Config = confy::load_path(&args.config)
        .unwrap_or_else(|_| panic!("Unable to load config file {}!", &args.config));

    let mut agent = ReprocessorAgent::new(cfg.clone(), KnowledgeBase::default())?;
    let input_file = File::open(&args.input)?;
    let lines = io::BufReader::new(input_file).lines();

    for line in lines {
        if let Ok(l) = line {
            agent.process_message(&l)?;
        }
    }
    Ok(())
}
