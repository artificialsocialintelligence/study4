//! ToMCAT NLU agent

use async_std::{
    sync::{Arc, Mutex},
    task,
};
use clap::Parser;
use color_eyre::eyre::Result;
use tomcat::{config::Config, knowledge_base::KnowledgeBase, mqtt_agent::MqttAgent};

/// Command line arguments
#[derive(Parser, Debug)]
pub struct Cli {
    /// MQTT broker host
    #[arg(long)]
    pub mqtt_host: Option<String>,

    /// MQTT broker port
    #[arg(short, long)]
    pub mqtt_port: Option<u16>,

    /// Config file
    #[arg(short, long, default_value_t = String::from("config.yml"))]
    pub config: String,

    /// Event extractor URL
    #[arg(long)]
    pub event_extractor_url: Option<String>,
}

#[async_std::main]
async fn main() -> Result<()> {
    // Initialize the logger from the environment
    pretty_env_logger::init();

    color_eyre::install()?;

    let args = Cli::parse();

    let mut cfg: Config = confy::load_path(&args.config)
        .unwrap_or_else(|_| panic!("Unable to load config file {}!", &args.config));

    // Allow command line args to override config file settings
    if let Some(host) = args.mqtt_host {
        cfg.mqtt.host = host;
    }
    if let Some(port) = args.mqtt_port {
        cfg.mqtt.port = port;
    }
    if let Some(url) = args.event_extractor_url {
        cfg.event_extractor_url = url;
    }

    let kb = Arc::new(Mutex::new(KnowledgeBase::default()));
    let agent_ref_1 = Arc::new(MqttAgent::new(cfg, kb)?);
    let agent_ref_2 = agent_ref_1.clone();
    let a = task::spawn(async move {
        agent_ref_1.publish_heartbeats().await.unwrap();
    });
    let b = task::spawn(async move {
        agent_ref_2.process_messages().await.unwrap();
    });
    a.await;
    b.await;
    Ok(())
}
