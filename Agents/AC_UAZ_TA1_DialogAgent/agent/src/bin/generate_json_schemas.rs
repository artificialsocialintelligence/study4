use color_eyre::eyre::Result;
use schemars::schema_for;
use tomcat::messages::nlu::NLUMessage;

fn main() -> Result<()> {
    color_eyre::install()?;
    let schema = schema_for!(NLUMessage);
    println!("{}", serde_json::to_string_pretty(&schema)?);
    Ok(())
}
