use log::info;
use mqtt::{Message, Receiver};
use paho_mqtt as mqtt;

pub struct MqttClient {
    pub client: mqtt::Client,
    client_id: String,
    pub stream: Receiver<Option<Message>>,
}

impl MqttClient {
    pub fn new(host: &str, port: &u16, client_id: &str) -> Self {
        let address = format!("tcp://{}:{}", host, port);
        // Create the client. Use an ID for a persistent session.
        // A real system should try harder to use a unique ID.
        let create_opts = mqtt::CreateOptionsBuilder::new()
            .mqtt_version(mqtt::MQTT_VERSION_5)
            .server_uri(address)
            .client_id(client_id)
            .finalize();

        // Create the client connection
        let client = mqtt::Client::new(create_opts).unwrap_or_else(|e| {
            panic!("Error creating the client: {e:?}");
        });
        // Get message stream before connecting.
        //let stream = client.get_stream(25);
        let stream = client.start_consuming();
        Self {
            client,
            client_id: client_id.to_string(),
            stream,
        }
    }

    pub fn connect(&self) -> Result<(), mqtt::Error> {
        // Define the set of options for the connection
        let lwt = mqtt::Message::new("test", "Async subscriber lost connection", mqtt::QOS_1);

        let conn_opts = mqtt::ConnectOptionsBuilder::new()
            .mqtt_version(mqtt::MQTT_VERSION_5)
            .clean_start(true)
            .properties(mqtt::properties![mqtt::PropertyCode::SessionExpiryInterval => 3600])
            .will_message(lwt)
            .finalize();

        // Make the connection to the broker
        info!(
            "Connecting to the MQTT server with client id \"{}\"",
            &self.client_id
        );

        let _ = &self.client.connect(conn_opts)?;
        Ok::<(), mqtt::Error>(())
    }

    pub fn subscribe(&self, topics: Vec<String>) -> Result<(), mqtt::Error> {
        let qos = vec![mqtt::QOS_2; topics.len()];

        info!(
            "Subscribing to topics: {:?} with QOS {}",
            topics,
            mqtt::QOS_2
        );

        let sub_opts = vec![mqtt::SubscribeOptions::default(); topics.len()];
        let _ = &self.client.subscribe_many_with_options(
            topics.as_slice(),
            qos.as_slice(),
            &sub_opts,
            None,
        )?;
        Ok::<(), mqtt::Error>(())
    }
}
