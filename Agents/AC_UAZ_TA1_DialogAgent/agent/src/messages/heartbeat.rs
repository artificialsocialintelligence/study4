use crate::messages::common::{Header, Msg};
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Default)]
#[allow(non_camel_case_types)]
pub enum State {
    #[default]
    ok,
    info,
    warn,
    error,
    fail,
}

#[derive(Debug, Serialize, Deserialize, Default)]
#[serde(deny_unknown_fields)]
pub struct HeartbeatData {
    pub state: State,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub status: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub active: Option<bool>,
}

#[derive(Debug, Serialize, Deserialize, Default)]
pub struct HeartbeatMessage {
    pub header: Header,
    pub msg: Msg,
    pub data: HeartbeatData,
}
