use derive_builder::Builder;
use iso8601_timestamp::Timestamp;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

#[derive(Builder, Clone, Debug, Eq, Hash, JsonSchema, PartialEq, Serialize, Deserialize)]
pub struct Header {
    pub timestamp: String,
    pub version: String,
    pub message_type: String,
}

impl Default for Header {
    fn default() -> Self {
        HeaderBuilder::default()
            .timestamp(Timestamp::now_utc().to_string())
            .version("0.1".to_string())
            .message_type("agent".to_string())
            .build()
            .unwrap()
    }
}

#[derive(
    Builder, Default, Clone, Debug, Eq, Hash, JsonSchema, PartialEq, Serialize, Deserialize,
)]
pub struct Msg {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub experiment_id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub replay_id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub replay_parent_id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub replay_parent_type: Option<String>,
    pub source: String,
    pub sub_type: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub trial_id: Option<String>,
    pub version: String,
}
