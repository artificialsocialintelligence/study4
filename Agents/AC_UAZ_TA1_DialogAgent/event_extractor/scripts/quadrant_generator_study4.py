# python script for procedurally generating rules for the study 4 map quadrants


letters = ["a","b","c","d","e","f","g","h","i"]

writefile = open("../src/main/resources/org/clulab/asist/grammars/study4/study4quadrants.yml", "w+", encoding="utf-8")
writefile.write('#These are procedurally generated rules for the study 4 map quadrants' + "\n")
for letter in letters:
	for i in range(1,8):
		writefile.write("- name: quadrant"+letter+str(i))
		writefile.write("\n")
		writefile.write('  priority: ${ first_priority }')
		writefile.write("\n")
		writefile.write("  label: " + letter.upper()+str(i))
		writefile.write("\n")
		writefile.write("  type: token")
		writefile.write("\n")
		writefile.write('  keep: ${ keep_locations}')
		writefile.write("\n")
		writefile.write("  pattern: |")
		writefile.write("\n")
		writefile.write(f"          [lemma=/(?i)^{letter}[-\s_]?{i}$/]| [lemma=/(?i)^{i}[-\s_]?{letter}$/] | [lemma=/(?i)^{letter}$/] [lemma=/(?i)^{i}$/]")
		writefile.write("\n")
		writefile.write("\n")

writefile.close()