package org.clulab.asist.agents

import com.typesafe.scalalogging.LazyLogging
import java.io.{File, PrintWriter}
import java.nio.file.Paths
import java.time.Clock
import org.clulab.asist.messages._
import org.clulab.utils.LocalFileUtils
import org.clulab.asist.extraction.TomcatRuleEngine
import org.json4s.{Extraction,_}

import scala.annotation.tailrec
import scala.util.control.NonFatal
import scala.io.Source


/**
 * Authors:  Joseph Astier, Adarsh Pyarelal, Rebecca Sharp
 *
 * Reprocess metadata JSON files by reading each line as a JValue and then 
 * processing according to the topic field.  Lines with topics not addressed
 * below are copied to the output file.
 *
 * If an input file has no dialog agent related metadata, an output file is
 * not generated.  If there are no output files to be written, the output
 * directory is not created.
 *
 * Reprocessed DialogAgentMessage metadata is a copy of the existing metadata
 * with the extractions regenerated. 
 *
 * Reprocessed Dialog Agent error messages use the error.data field to generate
 * a new DialogAgentMessageData struct with new extractions.  This replaces
 * the error field to transform the metadata into DialogAgentMessage metadata.
 *
 * VersionInfoMessage metadata with the DialogAgent topic are not reprocessed 
 * or copied to the output file.
 *
 * Trial Start metadata are copied to the output file followed by a new
 * VersionInfoMessage metadata message with the DialogAgent topic.
 *
 * If a .metadata file ends with Vers-<N>.metadata, the corresponding file
 * in the output directory should end with Vers-<N+1>.metadata (instead of
 * preserving the original fileName). This is to comply with the TA3 file
 * naming scheme.
 *
 */

class DialogAgentReprocessor (
  val inputDir: String = "",
  val outputDir: String = "",
  val ta3Version: Option[Int] = None,
  override val ruleEngine: TomcatRuleEngine = new TomcatRuleEngine

) extends DialogAgent(ruleEngine) with LazyLogging {

  logger.info(s"DialogAgentReprocessor version ${BuildInfo.version} running")

  // for the JSON extractor
  implicit val formats = org.json4s.DefaultFormats

  // make sure output directory is available
  val valid_files: List[String] = 
  if((outputDir == "/dev/null") || (LocalFileUtils.ensureDir(outputDir))) {
    // Find the .metadata files and see if we can reprocess them
    LocalFileUtils
      .getFileNames(inputDir)
      .filter(_.endsWith(".metadata"))
      .filter(name => 
         withDialogAgentMetadata(name, Source.fromFile(name).getLines)
       )
  } else List()

  if(valid_files.isEmpty) {
    logger.info("No files containing Dialog Agent metadata were found.")
  }
  else {
    // advise of files to be processed
    logger.info(".metadata files containing Dialog Agent messages.  These will be reprocessed:")
    valid_files.foreach(file => logger.info(s"  ${file}"))
    // get rule engine lazy init out of the way
    startEngine()
    // process the files containing Dialog Agent data
    valid_files.foreach(processFile(_))
    logger.info("All .metadata file reprocessing completed.")
  }

  // return true if the file has at least one DialogAgentMessage
  @tailrec
  private def withDialogAgentMetadata (
    fileName: String,
    iter: Iterator[String]
  ): Boolean = {
    if(!iter.hasNext) false
    else {
      if(readTopic(iter.next) == DialogAgentMessage.topic) {
        true
      }
      else withDialogAgentMetadata(fileName, iter)
    }
  }

  def processFile(inputFileName: String): Unit = {
    val outputFileName: String = ta3FileName(inputFileName)
    logger.info(s"Processing file: ${inputFileName}")
    val lineIterator = Source.fromFile(inputFileName).getLines
    if(outputDir == "/dev/null") {
      processFileWithPrinter(lineIterator, None)
    } else try {
      processFileWithPrinter(
        lineIterator,
        Some(new PrintWriter(new File(outputFileName)))
      )
    } catch {
      case NonFatal(t)  =>
        logger.error(s"Problem opening ${outputFileName} for writing:")
        logger.error(t.toString)
    }
  }

  def processFileWithPrinter(
    lineIterator: Iterator[String],
    pw: Option[PrintWriter]
  ): Unit = {
    val step = 1000
    var count = 0 
    var last_logged = count
    while(lineIterator.hasNext) {
      processLine(lineIterator.next, pw)
      count += 1
      if((count % step) == 0) {
        logger.info(s"Input lines processed: ${count}")
        last_logged = count
      }
    }
    if(last_logged != count) {
      logger.info(s"Input lines processed: ${count}")
    }
    pw.foreach(_.close)
  }

  /** Reprocess line if DialogAgent-related metadata, otherwise copy
   * @param line A complete JSON message from the Message Bus
   * @param rs State with input line loaded
   */
  def processLine(
    line: String,
    pw: Option[PrintWriter]
  ): Unit = readTopic(line) match {
    case TrialMessage.topic => 
      processTrialMetadata(line, pw)
    case DialogAgentMessage.topic => 
      reprocessDialogAgentMetadata(line, pw)
    case VersionInfoMessage.topic => 
      // Delete existing DialogAgent-generated VersionInfoMessage
    case _ => 
      // Transcribe cases we don't produce.  
      // Also transcribe the Rollcall Response, which we do not reprocess
      pw.foreach(_.write(s"${line}\n"))
  }

  /** Parse a TrialMessage and report our configuration if trial start.
   * @param inputText JSON representation of one Trial message
   */
  def processTrialMetadata(
    line: String,
    pw: Option[PrintWriter]
  ): Unit = JsonUtils.readJson[TrialMessage](line) match {
    case Some(trialMessage) =>
      // transcribe the trial message 
      pw.foreach(_.write(s"${line}\n"))
      TrialMessage(line) match {
        case Some(trial) =>
          // write the version info message if trial start
          if(TrialMessage.isStart(trial)) { 
            // metadata timestamp JValue
            val metadataTimestamp:JValue = Extraction.decompose(
              "@timestamp",trialMessage.msg.timestamp)
            // VersionInfoMessage struct
            val versionInfoMessage:VersionInfoMessage = 
              VersionInfoMessage(trialMessage)
            // JValue representation of struct
            val versionInfoMessageJValue:JValue =
              Extraction.decompose(versionInfoMessage)
            // Merge of @timestamp into JValue 
            val outputJValue:JValue = 
              versionInfoMessageJValue.merge(metadataTimestamp)
            // Write JValue to JSON
            val versionInfoMessageJson:String = 
              JsonUtils.writeJsonNoNulls(outputJValue) + "\n"
            // write the version info message
            // TEST
            pw.foreach(_.write(versionInfoMessageJson))
          }
        case _ =>
      } 
    case _ =>
  }

  /** Reprocess a metadata line that has the Dialog Agent topic
   * @param inputText: Metadata line to reprocess
   */
  def reprocessDialogAgentMetadata(
    line: String,
    pw: Option[PrintWriter]
  ): Unit = JsonUtils.parseJValue(line) match {
    case Some(metadataJValue: JValue) =>
      metadataJValue \ "data" match { 
        case dataJObject: JObject => 
          val data = dataJObject.extract[DialogAgentMessageData]
          val newData = data.copy(extractions = getExtractions(data.text))
          val newMetadata = metadataJValue.replace(
            "data"::Nil,
            Extraction.decompose(newData)
          )
          val json = JsonUtils.writeJsonNoNulls(newMetadata) + "\n"
          // TEST
          pw.foreach(_.write(json))
        case JNothing =>
          reprocessDialogAgentError(line, metadataJValue, pw)
        case _ => 
          logger.error("Unexpected non-JObject data in top level metadata")
          // TEST
          pw.foreach(_.write(line))
      }
    case _ => 
      logger.error("Unexpected non-JValue data in top level metadata")
      // TEST
      pw.foreach(_.write(line))
  }

  /** Recover a Dialog Agent Error report as a Dialog Agent Message
   * @param line: JSON text for error reporting
   * @param metadataJValue: JSON representation of input line
   */
  def reprocessDialogAgentError(
    line: String,
    metadataJValue: JValue,
    pw: Option[PrintWriter]
  ): Unit = {
    metadataJValue \ "error" \ "data" match {
      case dataJString: JString => 
        val text: String = dataJString.extract[String]
        JsonUtils.readJson[DialogAgentMessageData](text) match {
          case Some(data) =>
            val newData:DialogAgentMessageData = data.copy(
              extractions = getExtractions(data.text)
            )
            val newJson = Extraction.decompose(newData)
            val newMetadata = metadataJValue.transformField {
              case ("error", _) => ("data", newJson)
            }
            // TEST that this output is correct
            val json = JsonUtils.writeJsonNoNulls(newMetadata) + "\n"
            pw.foreach(_.write(json))
          case None =>
        }
      case _ =>
        logger.error("Expected error/data field not found in metadata")
        pw.foreach(_.write(line))
    }
  }

  /** Change filename if it has a TA3 version number
   * @param inputFileName fileName that may have a TA3 version number
   * @return the inputFileName, with any TA3 version number incremented
   */
  def ta3FileName(inputFileName: String): String = {
    // get local fileName out of the input directory
    val localFileName = inputFileName.split("/").last
    // the output fileName is the local fileName in the output directory
    val outputPath = Paths.get(outputDir,localFileName)
    val outputFileName = outputPath.toFile.getAbsolutePath
    // if the output fileName has a TA3 version number, increment it by 1
    val regex = """Vers-(\d+).metadata""".r
    regex.replaceAllIn(outputFileName, _ match {
      case regex(version) => 
        val newVersion: Int = ta3Version.getOrElse(version.toInt +1)
        s"Vers-${newVersion}.metadata"
      case _ => outputFileName  // otherwise do not change the fileame
    })
  }

  /** Scan the line for just the topic so we can branch on it
   *  @param line:  A single JSON line
   *  @return the topic if found, or the Topic.topic default value
   */
  def readTopic(line: String): String =
    JsonUtils.readJson[Topic](line).getOrElse(new Topic).topic
}
