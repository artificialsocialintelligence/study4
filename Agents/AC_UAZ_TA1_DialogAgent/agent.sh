#!/bin/bash
source .env

if [ "$1" = "up" ]; then
    echo "Bring up Agent: $AGENT_NAME"
    docker compose --compatibility up
elif [ "$1" = "upd" ]; then
    echo "Bring up Agent: $AGENT_NAME"
    docker compose --compatibility up -d
elif [ "$1" = "upb" ]; then
    echo "Bring up Agent: $AGENT_NAME"
    docker compose up --build
elif [ "$1" = "down" ]; then
    echo "Bring down Agent: $AGENT_NAME"
    docker compose down --remove-orphans
elif [ "$1" = "build" ]; then
    echo "Updating the docker image for $AGENT_NAME"
    pushd agent
        docker build -t $DOCKER_IMAGE_NAME_LOWERCASE --build-arg CACHE_BREAKER=$(date +%s) .
    popd

    pushd event_extractor
        docker build -f Dockerfile.agent -t tomcat_event_extractor --build-arg CACHE_BREAKER=$(date +%s) .
    popd
elif [ "$1" = "build_clean" ]; then
    echo "Rebuilding the docker image for $AGENT_NAME"
    docker build --no-cache -t $DOCKER_IMAGE_NAME_LOWERCASE .
    pushd agent
        docker build --no-cache -t ac_uaz_ta1_dialogagent .
    popd

    pushd event_extractor
        docker build --no-cache -f Dockerfile.agent -t tomcat_event_extractor .
    popd

elif [ "$1" = "export" ]; then
    echo "exporting the docker image for $AGENT_NAME"
    docker save -o $DOCKER_IMAGE_NAME_LOWERCASE.tar $DOCKER_IMAGE_NAME_LOWERCASE
    docker save -o tomcat_event_extractor.tar tomcat_event_extractor
else
    echo "Usage: ./agent.sh [up|upd|down|build|export]"
fi
