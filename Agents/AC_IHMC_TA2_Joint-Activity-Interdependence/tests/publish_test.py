import unittest

import uuid
import json
import unittest

from src.agents.joint_activity_monitor import JointActivityMonitor
from src.models.participant import Participant

def unpack(message):
    return message['header'], message['msg'], message['data']


class PublishTest(unittest.TestCase):

    def test_trial(self):
        jam = JointActivityMonitor()

        # init trial
        header, message, data = unpack(TRIAL_START)
        jam.handle_trial(header, message, data)

        # handle bomb 8 beacon
        header, message, data = unpack(COMMUNICATION_ENVIRONMENT_BOMB_BEACON_1)
        jam.handle_communication_environment(header, message, data)

        # handle bomb 12 beacon
        header, message, data = unpack(COMMUNICATION_ENVIRONMENT_BOMB_12_BEACON)
        jam.handle_communication_environment(header, message, data)

        # handles repetition
        header, message, data = unpack(COMMUNICATION_ENVIRONMENT_BOMB_BEACON_1)
        jam.handle_communication_environment(header, message, data)

        # p3 cuts first wire
        header, message, data = unpack(OBJECT_STATE_CHANGE_BOMB8_STEP_0)
        jam.handle_object_state_change(header, message, data)

        # p1 cuts second wire
        header, message, data = unpack(OBJECT_STATE_CHANGE_BOMB8_STEP_1)
        jam.handle_object_state_change(header, message, data)

        # p3 cuts third wire
        header, message, data = unpack(OBJECT_STATE_CHANGE_BOMB8_STEP_3)
        jam.handle_object_state_change(header, message, data)

        # defused state
        header, message, data = unpack(OBJECT_STATE_CHANGE_BOMB8_DEFUSED)
        jam.handle_object_state_change(header, message, data)

        # dealing with 12
        header, message, data = unpack(OBJECT_STATE_CHANGE_BOMB12_STEP_0)
        jam.handle_object_state_change(header, message, data)

        header, message, data = unpack(OBJECT_STATE_CHANGE_BOMB12_STEP_1)
        jam.handle_object_state_change(header, message, data)

        header, message, data = unpack(OBJECT_STATE_CHANGE_BOMB12_ERROR)
        jam.handle_object_state_change(header, message, data)


TRIAL_START = json.loads('''{
	"msg": {
		"trial_id": "295ec4af-3c41-4d06-bf21-9eed3cc654e2",
		"replay_parent_type": null,
		"sub_type": "start",
		"experiment_id": "7091e4f2-f000-4f0a-afcd-888743c6bc0c",
		"replay_id": null,
		"source": "gui",
		"replay_parent_id": null,
		"version": "0.1",
		"timestamp": "2023-04-19T19:42:06.4786Z"
	},
	"@timestamp": "2023-04-19T19:42:06.489Z",
	"data": {
		"date": "2023-04-19T19:42:06.4789Z",
		"client_info": [
			{
				"unique_id": "NOT SET ",
				"participant_id": "P000001",
				"callsign": "Alpha",
				"playername": "FloopDeeDoop",
				"markerblocklegend": "NOT SET ",
				"staticmapversion": "NOT SET "
			},
			{
				"unique_id": "NOT SET ",
				"participant_id": "P000002",
				"callsign": "Bravo",
				"playername": "Aaronskiy1",
				"markerblocklegend": "NOT SET ",
				"staticmapversion": "NOT SET "
			},
			{
				"unique_id": "NOT SET ",
				"participant_id": "P000003",
				"callsign": "Delta",
				"playername": "RED_ASIST1",
				"markerblocklegend": "NOT SET ",
				"staticmapversion": "NOT SET "
			}
		],
		"experiment_name": "[object Object]_[object Object]_[object Object]_Wed, 19 Apr 2023 19:42:06 GMT",
		"notes": [],
		"experiment_mission": "Study 4 MISSION A",
		"testbed_version": "V3.2.0-01_11_2023-667-g1e77cc1d4",
		"subjects": [
			"P000001",
			"P000002",
			"P000003"
		],
		"map_name": "Dragon_1.0_3D",
		"experimenter": "ADMINLESS",
		"experiment_author": "adminless",
		"study_number": "NOT SET ",
		"condition": "NOT SET ",
		"group_number": "NOT SET ",
		"map_block_filename": "MapBlocks_Orion_1.3.csv",
		"name": "P000001_P000002_P000003_0",
		"trial_number": "NOT SET ",
		"intervention_agents": [
			"ASI_CMU_TA1_ATLAS"
		],
		"experiment_date": "2023-04-19T19:42:06.4790Z",
		"observers": []
	},
	"@version": "1",
	"host": "858eedecf5a9",
	"topic": "trial",
	"header": {
		"message_type": "trial",
		"version": "0.6",
		"timestamp": "2023-04-19T19:42:06.4785Z"
	}
}''')

COMMUNICATION_ENVIRONMENT_BOMB_BEACON_1 = json.loads('''{
    "msg": {
        "trial_id": "295ec4af-3c41-4d06-bf21-9eed3cc654e2",
        "sub_type": "Event:CommunicationEnvironment",
        "experiment_id": "7091e4f2-f000-4f0a-afcd-888743c6bc0c",
        "source": "MINECRAFT_SERVER",
        "version": "1.0.0"
    },
    "@timestamp": "2023-04-19T19:43:43.177Z",
    "data": {
        "mission_timer": "0 : 6",
        "sender_z": 125,
        "elapsed_milliseconds": 11507,
        "additional_info": {
            "bomb_id": "BOMB8",
            "fuse_start_minute": "5",
            "remaining_sequence": "[G, B, R]",
            "chained_id": "NONE"
        },
        "sender_type": "block_beacon_bomb",
        "recipients": [
            "P000001",
            "P000002",
            "P000003"
        ],
        "message_id": "COMM_ENV1",
        "sender_x": 25,
        "message": "BOMB_ID: BOMB8\\nCHAINED_ID: NONE\\nFUSE_START_MINUTE: 5\\nSEQUENCE: [G, B, R]",
        "sender_y": 54,
        "sender_id": "ITEMSTACK30_ITEM20"
    },
    "@version": "1",
    "host": "858eedecf5a9",
    "topic": "communication/environment",
    "header": {
        "message_type": "simulator_event",
        "version": "1.2",
        "timestamp": "2023-04-19T19:43:43.172Z"
    }
}''')

OBJECT_STATE_CHANGE_BOMB8_STEP_0 = json.loads('''{
	"msg": {
		"trial_id": "295ec4af-3c41-4d06-bf21-9eed3cc654e2",
		"sub_type": "Event:ObjectStateChange",
		"experiment_id": "7091e4f2-f000-4f0a-afcd-888743c6bc0c",
		"source": "MINECRAFT_SERVER",
		"version": "0.0.0"
	},
	"@timestamp": "2023-04-19T19:51:50.397Z",
	"data": {
		"mission_timer": "3 : 33",
		"currAttributes": {
			"sequence": "BR",
			"sequence_index": "1",
			"fuse_start_minute": "5",
			"active": "true",
			"outcome": "TRIGGERED_ADVANCE_SEQ"
		},
		"triggering_entity": "P000003",
		"elapsed_milliseconds": 498732,
		"x": 25,
		"y": 53,
		"z": 125,
		"id": "BOMB8",
		"type": "block_bomb_fire",
		"changedAttributes": {
			"sequence": [
				"GBR",
				"BR"
			],
			"sequence_index": [
				"0",
				"1"
			],
			"active": [
				"false",
				"true"
			],
			"outcome": [
				"INACTIVE",
				"TRIGGERED_ADVANCE_SEQ"
			]
		}
	},
	"@version": "1",
	"host": "858eedecf5a9",
	"topic": "object/state/change",
	"header": {
		"message_type": "simulator_event",
		"version": "1.2",
		"timestamp": "2023-04-19T19:51:50.397Z"
	}
}''')

OBJECT_STATE_CHANGE_BOMB8_STEP_1 = json.loads('''{
	"msg": {
		"trial_id": "295ec4af-3c41-4d06-bf21-9eed3cc654e2",
		"sub_type": "Event:ObjectStateChange",
		"experiment_id": "7091e4f2-f000-4f0a-afcd-888743c6bc0c",
		"source": "MINECRAFT_SERVER",
		"version": "0.0.0"
	},
	"@timestamp": "2023-04-19T19:51:51.350Z",
	"data": {
		"mission_timer": "3 : 34",
		"currAttributes": {
			"sequence": "R",
			"sequence_index": "2",
			"fuse_start_minute": "5",
			"active": "true",
			"outcome": "TRIGGERED_ADVANCE_SEQ"
		},
		"triggering_entity": "P000001",
		"elapsed_milliseconds": 499683,
		"x": 25,
		"y": 53,
		"z": 125,
		"id": "BOMB8",
		"type": "block_bomb_fire",
		"changedAttributes": {
			"sequence": [
				"BR",
				"R"
			],
			"sequence_index": [
				"1",
				"2"
			]
		}
	},
	"@version": "1",
	"host": "858eedecf5a9",
	"topic": "object/state/change",
	"header": {
		"message_type": "simulator_event",
		"version": "1.2",
		"timestamp": "2023-04-19T19:51:51.348Z"
	}
}''')

OBJECT_STATE_CHANGE_BOMB8_STEP_3 = json.loads('''{
	"msg": {
		"trial_id": "295ec4af-3c41-4d06-bf21-9eed3cc654e2",
		"sub_type": "Event:ObjectStateChange",
		"experiment_id": "7091e4f2-f000-4f0a-afcd-888743c6bc0c",
		"source": "MINECRAFT_SERVER",
		"version": "0.0.0"
	},
	"@timestamp": "2023-04-19T19:51:53.448Z",
	"data": {
		"mission_timer": "3 : 36",
		"currAttributes": {
			"sequence": "",
			"sequence_index": "3",
			"fuse_start_minute": "5",
			"active": "true",
			"outcome": "TRIGGERED_ADVANCE_SEQ"
		},
		"triggering_entity": "P000003",
		"elapsed_milliseconds": 501783,
		"x": 25,
		"y": 53,
		"z": 125,
		"id": "BOMB8",
		"type": "block_bomb_fire",
		"changedAttributes": {
			"sequence": [
				"R",
				""
			],
			"sequence_index": [
				"2",
				"3"
			]
		}
	},
	"@version": "1",
	"host": "858eedecf5a9",
	"topic": "object/state/change",
	"header": {
		"message_type": "simulator_event",
		"version": "1.2",
		"timestamp": "2023-04-19T19:51:53.448Z"
	}
}''')

OBJECT_STATE_CHANGE_BOMB8_DEFUSED = json.loads('''{
	"msg": {
		"trial_id": "295ec4af-3c41-4d06-bf21-9eed3cc654e2",
		"sub_type": "Event:ObjectStateChange",
		"experiment_id": "7091e4f2-f000-4f0a-afcd-888743c6bc0c",
		"source": "MINECRAFT_SERVER",
		"version": "0.0.0"
	},
	"@timestamp": "2023-04-19T19:51:53.449Z",
	"data": {
		"mission_timer": "3 : 36",
		"currAttributes": {
			"sequence": "",
			"sequence_index": "3",
			"fuse_start_minute": "5",
			"active": "false",
			"outcome": "DEFUSED"
		},
		"triggering_entity": "P000003",
		"elapsed_milliseconds": 501784,
		"x": 25,
		"y": 53,
		"z": 125,
		"id": "BOMB8",
		"type": "block_bomb_fire",
		"changedAttributes": {
			"active": [
				"true",
				"false"
			],
			"outcome": [
				"TRIGGERED_ADVANCE_SEQ",
				"DEFUSED"
			]
		}
	},
	"@version": "1",
	"host": "858eedecf5a9",
	"topic": "object/state/change",
	"header": {
		"message_type": "simulator_event",
		"version": "1.2",
		"timestamp": "2023-04-19T19:51:53.449Z"
	}
}''')

COMMUNICATION_ENVIRONMENT_BOMB_12_BEACON = json.loads('''{
	"msg": {
		"trial_id": "295ec4af-3c41-4d06-bf21-9eed3cc654e2",
		"sub_type": "Event:CommunicationEnvironment",
		"experiment_id": "7091e4f2-f000-4f0a-afcd-888743c6bc0c",
		"source": "MINECRAFT_SERVER",
		"version": "1.0.0"
	},
	"@timestamp": "2023-04-19T19:44:02.854Z",
	"data": {
		"mission_timer": "0 : 26",
		"sender_z": 106,
		"elapsed_milliseconds": 31184,
		"additional_info": {
			"bomb_id": "BOMB12",
			"fuse_start_minute": "4",
			"remaining_sequence": "[R, G, B]",
			"chained_id": "NONE"
		},
		"sender_type": "block_beacon_bomb",
		"recipients": [
			"P000001",
			"P000002",
			"P000003"
		],
		"message_id": "COMM_ENV19",
		"sender_x": 14,
		"message": "BOMB_ID: BOMB12\\nCHAINED_ID: NONE\\nFUSE_START_MINUTE: 4\\nSEQUENCE: [R, G, B]",
		"sender_y": 53,
		"sender_id": "ITEMSTACK39_ITEM17"
	},
	"@version": "1",
	"host": "858eedecf5a9",
	"topic": "communication/environment",
	"header": {
		"message_type": "simulator_event",
		"version": "1.2",
		"timestamp": "2023-04-19T19:44:02.849Z"
	}
}''')

OBJECT_STATE_CHANGE_BOMB12_STEP_0 = json.loads('''{
	"msg": {
		"trial_id": "295ec4af-3c41-4d06-bf21-9eed3cc654e2",
		"sub_type": "Event:ObjectStateChange",
		"experiment_id": "7091e4f2-f000-4f0a-afcd-888743c6bc0c",
		"source": "MINECRAFT_SERVER",
		"version": "0.0.0"
	},
	"@timestamp": "2023-04-19T19:52:01.999Z",
	"data": {
		"mission_timer": "3 : 45",
		"currAttributes": {
			"sequence": "GB",
			"sequence_index": "1",
			"fuse_start_minute": "4",
			"active": "true",
			"outcome": "TRIGGERED_ADVANCE_SEQ"
		},
		"triggering_entity": "P000002",
		"elapsed_milliseconds": 510333,
		"x": 14,
		"y": 52,
		"z": 106,
		"id": "BOMB12",
		"type": "block_bomb_standard",
		"changedAttributes": {
			"sequence": [
				"RGB",
				"GB"
			],
			"sequence_index": [
				"0",
				"1"
			],
			"active": [
				"false",
				"true"
			],
			"outcome": [
				"INACTIVE",
				"TRIGGERED_ADVANCE_SEQ"
			]
		}
	},
	"@version": "1",
	"host": "858eedecf5a9",
	"topic": "object/state/change",
	"header": {
		"message_type": "simulator_event",
		"version": "1.2",
		"timestamp": "2023-04-19T19:52:01.998Z"
	}
}''')

OBJECT_STATE_CHANGE_BOMB12_STEP_1 = json.loads('''{
	"msg": {
		"trial_id": "295ec4af-3c41-4d06-bf21-9eed3cc654e2",
		"sub_type": "Event:ObjectStateChange",
		"experiment_id": "7091e4f2-f000-4f0a-afcd-888743c6bc0c",
		"source": "MINECRAFT_SERVER",
		"version": "0.0.0"
	},
	"@timestamp": "2023-04-19T19:52:06.349Z",
	"data": {
		"mission_timer": "3 : 49",
		"currAttributes": {
			"sequence": "B",
			"sequence_index": "2",
			"fuse_start_minute": "4",
			"active": "true",
			"outcome": "TRIGGERED_ADVANCE_SEQ"
		},
		"triggering_entity": "P000002",
		"elapsed_milliseconds": 514683,
		"x": 14,
		"y": 52,
		"z": 106,
		"id": "BOMB12",
		"type": "block_bomb_standard",
		"changedAttributes": {
			"sequence": [
				"GB",
				"B"
			],
			"sequence_index": [
				"1",
				"2"
			]
		}
	},
	"@version": "1",
	"host": "858eedecf5a9",
	"topic": "object/state/change",
	"header": {
		"message_type": "simulator_event",
		"version": "1.2",
		"timestamp": "2023-04-19T19:52:06.348Z"
	}
}''')

OBJECT_STATE_CHANGE_BOMB12_ERROR = json.loads('''{
	"msg": {
		"trial_id": "295ec4af-3c41-4d06-bf21-9eed3cc654e2",
		"sub_type": "Event:ObjectStateChange",
		"experiment_id": "7091e4f2-f000-4f0a-afcd-888743c6bc0c",
		"source": "MINECRAFT_SERVER",
		"version": "0.0.0"
	},
	"@timestamp": "2023-04-19T19:52:07.212Z",
	"data": {
		"mission_timer": "3 : 50",
		"currAttributes": {
			"sequence": "B",
			"sequence_index": "2",
			"fuse_start_minute": "4",
			"active": "false",
			"outcome": "EXPLODE_TOOL_MISMATCH"
		},
		"triggering_entity": "P000003",
		"elapsed_milliseconds": 515547,
		"x": 14,
		"y": 52,
		"z": 106,
		"id": "BOMB12",
		"type": "block_bomb_standard",
		"changedAttributes": {
			"active": [
				"true",
				"false"
			],
			"outcome": [
				"TRIGGERED_ADVANCE_SEQ",
				"EXPLODE_TOOL_MISMATCH"
			]
		}
	},
	"@version": "1",
	"host": "858eedecf5a9",
	"topic": "object/state/change",
	"header": {
		"message_type": "simulator_event",
		"version": "1.2",
		"timestamp": "2023-04-19T19:52:07.212Z"
	}
}''')
