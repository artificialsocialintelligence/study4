import uuid
import unittest

from src.models.jags.jag import Jag

class JagTest(unittest.TestCase):

    def test_new_jag(self):
        urn = 'urn:test:jag-test'
        jag = Jag(urn)
        self.assertIsInstance(jag.uid, uuid.UUID)
        self.assertEqual(jag.urn, urn)
        self.assertEqual(len(jag.inputs), 0)

    def test_new_jag_with_inputs(self):
        urn = 'urn:test:jag-test'
        inputs = {
                'test-key': 'test-value'
                }

        jag = Jag(urn, inputs)
        self.assertEqual(len(jag.inputs), 1)
        self.assertEqual(jag.inputs['test-key'], 'test-value')

    def test_get_input_returns_none_if_it_does_not_exists(self):
        urn = 'urn:test:jag-test'
        jag = Jag(urn)
        self.assertIsNone(jag.inputs.get('test-key'))

    def test_jag_get_input(self):
        urn = 'urn:test:jag-test'
        jag = Jag(urn)

        key = 'test-input'
        value = 'test-value'

        jag.inputs[key] = value
        self.assertEqual(jag.inputs.get(key), value)
