import uuid
import json
import unittest

from src.agents.joint_activity_monitor import JointActivityMonitor
from src.models.participant import Participant

def unpack(message):
    return message['header'], message['msg'], message['data']


class CommunicationEventTest(unittest.TestCase):

    def assert_cut_wire_jag(self, jag, bomb_id, wire):
        self.assertIsInstance(jag.uid, uuid.UUID)
        self.assertEqual(jag.urn, 'urn:asist:cut-wire')
        self.assertEqual(jag.inputs['bomb_id'], bomb_id)
        self.assertEqual(jag.inputs['wire'], wire)
        # self.assertEqual(jag['outputs'], {})

    def assert_defuse_bomb_jag(self, jag, bomb_id, sequence, fuse_start):
        self.assertIsInstance(jag.uid, uuid.UUID)
        self.assertEqual(jag.urn, 'urn:asist:defuse-bomb')
        self.assertEqual(jag.inputs['bomb_id'], bomb_id)
        self.assertEqual(jag.inputs['sequence'], sequence)
        self.assertEqual(jag.inputs['fuse_start'], fuse_start)
        # self.assertEqual(jag['outputs'], {})

        for i, child in enumerate(jag.children):
            self.assert_cut_wire_jag(child, bomb_id, sequence[i])

    def test_create_cut_wire_jag(self):
        jam = JointActivityMonitor()

        bomb_id = 'BOMB1'
        wire = 'R'

        jag = jam.create_cut_wire_jag(bomb_id, wire)
        self.assert_cut_wire_jag(jag, bomb_id, wire)

    def test_defuse_bomb_jag_one_wire(self):
        jam = JointActivityMonitor()

        bomb_id = 'BOMB1'
        sequence = ['R']
        fuse_start = 5

        jag = jam.create_defuse_bomb_jag(bomb_id, sequence, fuse_start)
        self.assert_defuse_bomb_jag(jag, bomb_id, sequence, fuse_start)

    def test_defuse_bomb_jag_two_wire(self):
        jam = JointActivityMonitor()

        bomb_id = 'BOMB10'
        sequence = ['R', 'G']
        fuse_start = 11

        jag = jam.create_defuse_bomb_jag(bomb_id, sequence, fuse_start)
        self.assert_defuse_bomb_jag(jag, bomb_id, sequence, fuse_start)

    def test_creates_jags_when_bomb_beacon_is_placed(self):
        jam = JointActivityMonitor()

        # init trial
        header, message, data = unpack(TRIAL_START)
        jam.handle_trial(header, message, data)

        # handle bomb beacon
        header, message, data = unpack(COMMUNICATION_ENVIRONMENT_BOMB_BEACON_1)
        jam.handle_communication_environment(header, message, data)

        for participant in jam.participants.values():
            self.assertEqual(len(participant.jags), 1)

            jag = list(participant.jags.values())[0]
            self.assertEqual(jag.urn, 'urn:asist:defuse-bomb')
            self.assertEqual(len(jag.children), 3)

        # handles repetition
        header, message, data = unpack(COMMUNICATION_ENVIRONMENT_BOMB_BEACON_1)
        jam.handle_communication_environment(header, message, data)

        for participant in jam.participants.values():
            self.assertEqual(len(participant.jags), 1)

            jag = list(participant.jags.values())[0]
            self.assertEqual(jag.urn, 'urn:asist:defuse-bomb')
            self.assertEqual(len(jag.children), 3)

            # jags = participant.get_jags_by_urn_and_inputs('urn:asist:defuse-bomb', {})
        # what happens on bomb beacon being sent twice


TRIAL_START = json.loads('''{
	"msg": {
		"trial_id": "295ec4af-3c41-4d06-bf21-9eed3cc654e2",
		"replay_parent_type": null,
		"sub_type": "start",
		"experiment_id": "7091e4f2-f000-4f0a-afcd-888743c6bc0c",
		"replay_id": null,
		"source": "gui",
		"replay_parent_id": null,
		"version": "0.1",
		"timestamp": "2023-04-19T19:42:06.4786Z"
	},
	"@timestamp": "2023-04-19T19:42:06.489Z",
	"data": {
		"date": "2023-04-19T19:42:06.4789Z",
		"client_info": [
			{
				"unique_id": "NOT SET ",
				"participant_id": "P000001",
				"callsign": "Alpha",
				"playername": "FloopDeeDoop",
				"markerblocklegend": "NOT SET ",
				"staticmapversion": "NOT SET "
			},
			{
				"unique_id": "NOT SET ",
				"participant_id": "P000002",
				"callsign": "Bravo",
				"playername": "Aaronskiy1",
				"markerblocklegend": "NOT SET ",
				"staticmapversion": "NOT SET "
			},
			{
				"unique_id": "NOT SET ",
				"participant_id": "P000003",
				"callsign": "Delta",
				"playername": "RED_ASIST1",
				"markerblocklegend": "NOT SET ",
				"staticmapversion": "NOT SET "
			}
		],
		"experiment_name": "[object Object]_[object Object]_[object Object]_Wed, 19 Apr 2023 19:42:06 GMT",
		"notes": [],
		"experiment_mission": "Study 4 MISSION A",
		"testbed_version": "V3.2.0-01_11_2023-667-g1e77cc1d4",
		"subjects": [
			"P000001",
			"P000002",
			"P000003"
		],
		"map_name": "Dragon_1.0_3D",
		"experimenter": "ADMINLESS",
		"experiment_author": "adminless",
		"study_number": "NOT SET ",
		"condition": "NOT SET ",
		"group_number": "NOT SET ",
		"map_block_filename": "MapBlocks_Orion_1.3.csv",
		"name": "P000001_P000002_P000003_0",
		"trial_number": "NOT SET ",
		"intervention_agents": [
			"ASI_CMU_TA1_ATLAS"
		],
		"experiment_date": "2023-04-19T19:42:06.4790Z",
		"observers": []
	},
	"@version": "1",
	"host": "858eedecf5a9",
	"topic": "trial",
	"header": {
		"message_type": "trial",
		"version": "0.6",
		"timestamp": "2023-04-19T19:42:06.4785Z"
	}
}''')

COMMUNICATION_ENVIRONMENT_BOMB_BEACON_1 = json.loads('''{
    "msg": {
        "trial_id": "295ec4af-3c41-4d06-bf21-9eed3cc654e2",
        "sub_type": "Event:CommunicationEnvironment",
        "experiment_id": "7091e4f2-f000-4f0a-afcd-888743c6bc0c",
        "source": "MINECRAFT_SERVER",
        "version": "1.0.0"
    },
    "@timestamp": "2023-04-19T19:43:43.177Z",
    "data": {
        "mission_timer": "0 : 6",
        "sender_z": 125,
        "elapsed_milliseconds": 11507,
        "additional_info": {
            "bomb_id": "BOMB8",
            "fuse_start_minute": "5",
            "remaining_sequence": "[G, B, R]",
            "chained_id": "NONE"
        },
        "sender_type": "block_beacon_bomb",
        "recipients": [
            "P000001",
            "P000002",
            "P000003"
        ],
        "message_id": "COMM_ENV1",
        "sender_x": 25,
        "message": "BOMB_ID: BOMB8\\nCHAINED_ID: NONE\\nFUSE_START_MINUTE: 5\\nSEQUENCE: [G, B, R]",
        "sender_y": 54,
        "sender_id": "ITEMSTACK30_ITEM20"
    },
    "@version": "1",
    "host": "858eedecf5a9",
    "topic": "communication/environment",
    "header": {
        "message_type": "simulator_event",
        "version": "1.2",
        "timestamp": "2023-04-19T19:43:43.172Z"
    }
}''')
