# -*- coding: utf-8 -*-
"""
.. module:: item_used
   :platform: Linux, Windows, OSX
   :synopsis: Message class encapsulating ItemUsed messages

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a class encapsulating Item Used messages.
"""

import json

from ..message_exceptions import (
    MalformedMessageCreationException, 
    MissingMessageArgumentException, 
    ImmutableAttributeException
)
from ..base_message import BaseMessage


class ItemUsed(BaseMessage):
    """
    A class encapsulating ItemUsed messages.


    Attributes
    ----------
    item_id : string
        Unique identifier of the item
    item_name : string
        Registered name of the item
    input_mode : string
        Which user input key was pressed to use the item
    location : tuple of floats
        Location of the target of the item use
    target_x : float
        x location of the target
    target_y : float
        y location of the target
    target_z : float
        z location of the target
    """


    def __init__(self, **kwargs):
        """

        """


        BaseMessage.__init__(self, **kwargs)


        # TODO: Validate that all the keys are present
        self._item_id = kwargs['item_id']
        self._item_name = kwargs['item_name']
        self._input_mode = kwargs['input_mode']


        location = kwargs.get('location', None)
        if location is None:
            try:
                location = (kwargs['target_x'],
                            kwargs['target_y'],
                            kwargs['target_z'])
            except KeyError:
                raise MissingMessageArgumentException(str(self),
                                                      'location') from None

        # Location needs to be able to be coerced into a tuple of ints.  Raise 
        # an exception if not possible
        try:
            self._location = tuple([float(x) for x in location][:3])
        except:
            raise MalformedMessageCreationException(str(self), 'location', 
                                                    kwargs['location']) from None


    def __str__(self):
        """
        String representation of the message.

        Returns
        -------
        string
            Class name of the message (i.e., 'ItemUsedEvent')
        """

        return self.__class__.__name__


    @property
    def item_id(self):
        return self._item_id

    @property
    def item_name(self):
        return self._item_name

    @property
    def input_mode(self):
        return self._input_mode

    @property
    def location(self):
        return self._location
    
    @property
    def target_x(self):
        return self._location[0]

    @property
    def target_y(self):
        return self._location[1]

    @property
    def target_z(self):
        return self._location[2]


    def toDict(self):
        """
        Generates a dictionary representation of the message.  Message
        information is contained in a dictionary under the key "data".
        Additional named headers may also be present.

        Returns
        -------
        dict
            A dictionary representation of the ItemUsedEvent.
        """

        jsonDict = BaseMessage.toDict(self)

        # Check to see if a "data" is in the dictionary, and add if not
        # Note that headers should have been added in jsonDict, as well as
        # common message data.
        if not "data" in jsonDict:
            jsonDict["data"] = {}

        # Add the message data
        jsonDict["data"]["item_id"] = self.item_id
        jsonDict["data"]["item_name"] = self.item_name
        jsonDict["data"]["input_mode"] = self.input_mode
        jsonDict["data"]["target_x"] = self.target_x
        jsonDict["data"]["target_y"] = self.target_y
        jsonDict["data"]["target_z"] = self.target_z

        return jsonDict


    def toJson(self):
        """
        Generates a JSON representation of the message.  Message information is
        contained in a JSON object under the key "data".  Additional named
        headers may also be present.

        Returns
        -------
        string
            A JSON string mapping header names to a JSON representation of the
            ItemUsedEvent message.
        """

        return json.dumps(self.toDict())
