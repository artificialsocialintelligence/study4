# -*- coding: utf-8 -*-
"""
.. module:: score_update
   :platform: Linux, Windows, OSX
   :synopsis: Message class encapsulating Score Update messages

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a class encapsulating Score Update messages.
"""


import json

import ciso8601

from ..message_exceptions import (
    MalformedMessageCreationException, 
    MissingMessageArgumentException, 
    ImmutableAttributeException
)
from ..base_message import BaseMessage

class ScoreUpdate(BaseMessage):
    """
    A class encapsulating Score Update messages.

    Attributes
    ----------
    playerScores : Map[str,int]
        Current score of each player
    teamScore : int
        Current team score
    """

    def __init__(self, **kwargs):
        """
        """

        BaseMessage.__init__(self, **kwargs)

        self._playerScores = kwargs.get('playerScores', {})
        self._teamScore = kwargs.get('teamScore', -1)


    def __str__(self):
        """
        String representation of the message.

        Returns
        -------
        string
            Class name of the message
        """

        return self.__class__.__name__


    @property
    def playerScores(self):
        return self._playerScores

    @property
    def teamScore(self):
        return self._teamScore
    
    

    def toDict(self):
        """
        Generates a dictionary representation of the message.  Message
        information is contained in a dictionary under the key "data".
        Additional named headers may also be present.

        Returns
        -------
        dict
            A dictionary representation of the Score Update message.
        """

        jsonDict = BaseMessage.toDict(self)

        # Check to see if a "data" is in the dictionary, and add if not
        # Note that headers should have been added in jsonDict, as well as
        # common message data.
        if not "data" in jsonDict:
            jsonDict["data"] = {}

        # Add the message data
        jsonDict["data"]["playerScores"] = self.playerScores
        jsonDict["data"]["teamScore"] = self.teamScore

        return jsonDict


    def toJson(self):
        """
        Generates a JSON representation of the message.  Message information is
        contained in a JSON object under the key "data".  Additional named
        headers may also be present.

        Returns
        -------
        string
            A JSON string mapping header names to a JSON representation of the
            Score Update message.
        """

        return json.dumps(self.toDict())
