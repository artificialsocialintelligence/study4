# -*- coding: utf-8 -*-
"""
.. module:: mission_level_progress
   :platform: Linux, Windows, OSX
   :synopsis: Message class encapsulating Mission Level Progress messages

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a class encapsulating Mission Level Progress messages.
"""


import json

import ciso8601

from ..message_exceptions import (
    MalformedMessageCreationException, 
    MissingMessageArgumentException, 
    ImmutableAttributeException
)
from ..base_message import BaseMessage

class MissionLevelProgress(BaseMessage):
    """
    A class encapsulating MissionLevelProgress messages.

    Attributes
    ----------
    level_unlocked : int
        The integer associated with the unlocked level / region
    """

    def __init__(self, **kwargs):
        """
        """

        BaseMessage.__init__(self, **kwargs)

        self._level_unlocked = kwargs.get["level_unlocked"]


    def __str__(self):
        """
        String representation of the message.

        Returns
        -------
        string
            Class name of the message
        """

        return self.__class__.__name__


    @property
    def level_unlocked(self):
        return self._level_unlocked
    

    def toDict(self):
        """
        Generates a dictionary representation of the message.  Message
        information is contained in a dictionary under the key "data".
        Additional named headers may also be present.

        Returns
        -------
        dict
            A dictionary representation of the MissionLevelProgress message.
        """

        jsonDict = BaseMessage.toDict(self)

        # Check to see if a "data" is in the dictionary, and add if not
        # Note that headers should have been added in jsonDict, as well as
        # common message data.
        if not "data" in jsonDict:
            jsonDict["data"] = {}

        # Add the message data
        jsonDict["data"]["level_unlocked"] = self.level_unlocked

        return jsonDict


    def toJson(self):
        """
        Generates a JSON representation of the message.  Message information is
        contained in a JSON object under the key "data".  Additional named
        headers may also be present.

        Returns
        -------
        string
            A JSON string mapping header names to a JSON representation of the
            MissionLevelProgress message.
        """

        return json.dumps(self.toDict())
