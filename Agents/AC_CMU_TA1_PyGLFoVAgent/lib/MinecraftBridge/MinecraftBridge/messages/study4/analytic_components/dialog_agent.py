# -*- coding: utf-8 -*-
"""
.. module:: dialog_agent
   :platform: Linux, Windows, OSX
   :synopsis: Message class encapsulating Dialog Agent messages

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a class encapsulating Study 4 Dialog Agent messages.
"""

import json

from ...message_exceptions import (
    MalformedMessageCreationException, 
    MissingMessageArgumentException, 
    ImmutableAttributeException
)
from ...base_message import BaseMessage


class DialogAgent(BaseMessage):
    """
    A class encapsulating Study 4 Dialog Agent messages.

    Attributes
    ----------
    text : str
        Original text
    corrected_text : str
        Corrected text (spellchecked?)
    participant_id : str
        ID of the participant who wrote the message
    utterance_id : str
        ID of the utterance
    extractions : List[]
        List of extracted rules
    compact_extractions : List[]
        List of compact extractions
    """


    def __init__(self, **kwargs):
        """
        """

        BaseMessage.__init__(self, **kwargs)

        self._text = kwargs.get('text', '')
        self._corrected_text = kwargs.get('corrected_text', '')
        self._participant_id = kwargs.get('participant_id', None)
        self._utterance_id = kwargs.get('utterance_id', None)

        self._extractions = []
        self._compact_extractions = []

    def __str__(self):
        return self.__class__.__name__

    def add_extraction(self, extraction):
        self._extractions.append(extraction)

    def add_compact_extraction(self, extraction):
        self._compact_extractions.append(extraction)

    @property
    def text(self):
        return self._text

    @property
    def corrected_text(self):
        return self._corrected_text

    @property
    def participant_id(self):
        return self._participant_id

    @property
    def utterance_id(self):
        return self._utterance_id

    @property
    def extractions(self):
        return self._extractions

    @property
    def compact_extractions(self):
        return self._compact_extractions
    
    
     
    
    


    def __str__(self):
        """
        String representation of the message.

        Returns
        -------
        string
            Class name of the message (i.e., 'DialogAgent')
        """

        return self.__class__.__name__


    def toDict(self):
        """
        Generates a dictionary representation of the message.  Message
        information is contained in a dictionary under the key "data".
        Additional named headers may also be present.

        Returns
        -------
        dict
            A dictionary representation of the ItemUsedEvent.
        """

        jsonDict = BaseMessage.toDict(self)

        # Check to see if a "data" is in the dictionary, and add if not
        # Note that headers should have been added in jsonDict, as well as
        # common message data.
        if not "data" in jsonDict:
            jsonDict["data"] = {}

        # Add the message data
        jsonDict['data']['text'] = self.text
        jsonDict['data']['corrected_text'] = self.corrected_text
        jsonDict['data']['participant_id'] = self.participant_id
        jsonDict['data']['utterance_id'] = self.utterance_id
        jsonDict['data']['extractions'] = self.extractions
        jsonDict['data']['compact_extractions'] = self.compact_extractions


        return jsonDict


    def toJson(self):
        """
        Generates a JSON representation of the message.  Message information is
        contained in a JSON object under the key "data".  Additional named
        headers may also be present.

        Returns
        -------
        string
            A JSON string mapping header names to a JSON representation of the
            ItemUsedEvent message.
        """

        return json.dumps(self.toDict())
