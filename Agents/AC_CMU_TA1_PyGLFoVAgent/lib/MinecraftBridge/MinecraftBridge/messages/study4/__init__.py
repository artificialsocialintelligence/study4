# -*- coding: utf-8 -*-
"""
.. module:: messages.study4
   :platform: Linux, Windows, OSX
   :synopsis: Module defining Study 4 messages

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of message classes used by MinecraftBridge.  Classes in this module
are related to General Purpose message sets defined for Study 4.
"""
