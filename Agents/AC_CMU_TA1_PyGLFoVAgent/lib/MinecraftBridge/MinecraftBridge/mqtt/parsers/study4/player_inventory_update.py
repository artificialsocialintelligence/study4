# -*- coding: utf-8 -*-
"""
.. module:: mission_level_progress
   :platform: Linux, Windows, OSX
   :synopsis: Parser for PlayerInventoryUpdate messages

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a parser for PlayerInventoryUpdate messages
"""

from ....messages import PlayerInventoryUpdate
from ..message_types import MessageType, MessageSubtype
from ..bus_header import BusHeaderParser
from ..message_header import MessageHeaderParser
from ..parser_exceptions import MissingHeaderException

import MinecraftElements

import logging


class PlayerInventoryUpdateParser:
    """
    A class for parsing PlayerInventoryUpdate messages from MQTT bus.

    MQTT Message Fields
    -------------------


    MQTT Blockage Fields
    --------------------

    """

    topic = "player/inventory/update"
    MessageClass = PlayerInventoryUpdate

    msg_type = MessageType.event
    msg_subtype = MessageSubtype.Event_PlayerInventoryUpdate
    alternatives = []

    # Class-level logger
    logger = logging.getLogger("PlayerInventoryUpdateParser")


    @classmethod
    def parse(cls, json_message):
        """
        Convert the a Python dictionary format of the message to a MissionStage
        instance.

        Arguments
        ---------
        json_message : dictionary
            Dictionary representation of the message received from the MQTT bus
        """

        # Make sure that there's a "header" and "msg" field in the message
        if not "header" in json_message.keys() or not "msg" in json_message.keys():
            raise MissingHeaderException(json_message)

        # Parse the header and message header
        busHeader = BusHeaderParser.parse(json_message["header"])
        messageHeader = MessageHeaderParser.parse(json_message["msg"])

        # Check to see if this parser can handle this message type, if not, 
        # then return None
        if busHeader.message_type not in [MessageType.event, MessageType.simulator_event]:
            print("Not Message Type event")
            return None
        if messageHeader.sub_type not in [MessageSubtype.Event_PlayerInventoryUpdate, MessageSubtype.Event_InventoryUpdate]:
            print("Bad message subtype")
            return None

        # Parse the data
        data = json_message["data"]

        message = PlayerInventoryUpdate(**data)
        message.addHeader("header", busHeader)
        message.addHeader("msg", messageHeader)

        return message