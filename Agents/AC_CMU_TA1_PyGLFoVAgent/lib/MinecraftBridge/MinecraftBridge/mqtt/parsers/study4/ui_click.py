# -*- coding: utf-8 -*-
"""
.. module:: ui_click
   :platform: Linux, Windows, OSX
   :synopsis: Parser for UI Click messages

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a parser for UI Click messages
"""

from ....messages import UI_Click
from ..message_types import MessageType, MessageSubtype
from ..bus_header import BusHeaderParser
from ..message_header import MessageHeaderParser
from ..parser_exceptions import MissingHeaderException

import MinecraftElements

import logging


class UI_ClickParser:
    """
    A class for parsing UI Click messages from MQTT bus.

    MQTT Message Fields
    -------------------


    MQTT Blockage Fields
    --------------------

    """

    topic = "ui/click"
    MessageClass = UI_Click

    msg_type = MessageType.simulator_event
    msg_subtype = MessageSubtype.Event_UIClick
    alternatives = []

    # Class-level logger
    logger = logging.getLogger("UI_ClickParser")


    @classmethod
    def parse(cls, json_message):
        """
        Convert the a Python dictionary format of the message to a BlockageList
        instance.

        Arguments
        ---------
        json_message : dictionary
            Dictionary representation of the message received from the MQTT bus
        """

        # Make sure that there's a "header" and "msg" field in the message
        if not "header" in json_message.keys() or not "msg" in json_message.keys():
            raise MissingHeaderException(json_message)

        # Parse the header and message header
        busHeader = BusHeaderParser.parse(json_message["header"])
        messageHeader = MessageHeaderParser.parse(json_message["msg"])

        # Check to see if this parser can handle this message type, if not, 
        # then return None
        if busHeader.message_type != MessageType.simulator_event:
            return None
        if messageHeader.sub_type != MessageSubtype.Event_UIClick:
            return None

        # Parse the data
        data = json_message["data"]

        participant_id = data["participant_id"]
        element_id = data["element_id"]
        parent_id = data["parent_id"]
        type_ = data["type"]
        x = data["x"]
        y = data["y"]
        meta_action = data["additional_info"]["meta_action"]
        call_sign_code = data["additional_info"]["call_sign_code"]


        message = UI_Click(participant_id=participant_id,
                           element_id=element_id,
                           parent_id=parent_id,
                           type=type_,
                           x=x,
                           y=y,
                           meta_action=meta_action,
                           call_sign_code=call_sign_code,
                           additional_info=data['additional_info'])
        message.addHeader("header", busHeader)
        message.addHeader("msg", messageHeader)

        return message