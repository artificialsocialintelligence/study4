from .callback_interface import CallbackInterface
from .scheduler import SchedulerInterface

# Alias
Scheduler = SchedulerInterface

__all__ = ["CallbackInterface", "SchedulerInterface", "Scheduler"]