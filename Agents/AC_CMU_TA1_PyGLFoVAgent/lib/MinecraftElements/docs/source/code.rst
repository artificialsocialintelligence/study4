Minecraft Elements API
======================

.. automodule:: MinecraftElements


Enumerations
------------

Enumeration-specific documentation and class definitions are provided for each
enumeration in the module.

Block
*****
.. automodule:: MinecraftElements.blocks

Color
*****
.. automodule:: MinecraftElements.colors

Entity
******
.. automodule:: MinecraftElements.entities

Facing
******
.. automodule:: MinecraftElements.facing

Flower
******
.. automodule:: MinecraftElements.flowers

Half
****
.. automodule:: MinecraftElements.halfs

Hinge
*****
.. automodule:: MinecraftElements.hinge

Item
****
.. automodule:: MinecraftElements.items

MonsterEgg
**********
.. automodule:: MinecraftElements.monster_eggs

Shape
*****
.. automodule:: MinecraftElements.shapes

Stone
*****
.. automodule:: MinecraftElements.stones

Wood
****
.. automodule:: MinecraftElements.woods


Class Definitions
~~~~~~~~~~~~~~~~~

.. autoclass:: MinecraftElements.blocks.Block
.. autoclass:: MinecraftElements.colors.Color
.. autoclass:: MinecraftElements.entities.Entity
.. autoclass:: MinecraftElements.facing.Facing
.. autoclass:: MinecraftElements.flowers.Flower
.. autoclass:: MinecraftElements.halfs.Half
.. autoclass:: MinecraftElements.hinge.Hinge
.. autoclass:: MinecraftElements.items.Item
.. autoclass:: MinecraftElements.monster_eggs.MonsterEgg
.. autoclass:: MinecraftElements.shapes.Shape
.. autoclass:: MinecraftElements.stones.Stone
.. autoclass:: MinecraftElements.woods.Wood
