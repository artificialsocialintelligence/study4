"""
fovWorker.py

Class definition of worker threads for performing FoV calculations

Author: Dana Hughes
email: danahugh@andrew.cmu.edu
"""

import enum

import time
import threading

import pygl_fov

import endpoints

from study4.context import Context as WorldModelContext
from dynamics_model import DynamicsModel
from study4 import (
    CreatedBlockListUpdate,
    CreatedBlockSingleUpdate,
    RemovedBlockListUpdate,
    RemovedBlockSingleUpdate,
    BlockChangedUpdate,
    MissionStageChangedUpdate,
    DoorUpdate,
    LeverUpdate
)

from dispatchers import Queue
from dispatchers import LatestPlayerStateDispatcher
from dispatchers import IgnoreMultiplePlayerStateDispatcher

from MinecraftBridge.messages import (
    PlayerState, 
    EnvironmentCreatedList,
    EnvironmentCreatedSingle,
    EnvironmentRemovedList,
    EnvironmentRemovedSingle,
    MissionStageTransition,
    MissionStage,
    FoV_MapMetadata, 
    FoVProfile,
    BusHeader, 
    MessageHeader
)
from MinecraftBridge.mqtt.parsers import MessageType, MessageSubtype
from MinecraftBridge.utils import Loggable

import MinecraftElements

from participant import Participant

import numpy as np

from world_model import WorldModel
from dynamics_model import DynamicsModel

__author__ = 'danahugh'


WORLD_MODEL_UPDATES = [
    CreatedBlockListUpdate,
    CreatedBlockSingleUpdate,
    RemovedBlockListUpdate,
    RemovedBlockSingleUpdate,
    BlockChangedUpdate,
    MissionStageChangedUpdate,
    DoorUpdate,
    LeverUpdate
]

class FoVWorker(threading.Thread,Loggable):
    """
    A worker for performing FoV calculations on a separate thread for a single
    trial.

    Attributes
    ----------
    worker_key : tuple of strings
        Key associated with this worker, consisting of
        (experiment_id, trial_id, replay_id)
    state : FoVWorker.State
        State that the worker is currently in
    """


    class State(enum.Enum):
        """
        Indicates the state of an FoVWorker thread.  States are indicated as::

        * INITIALIZING - Worker is currently initializing, and is not ready
                         to be run
        * READY -        Worker is ready to be run, thread has yet to be started
        * RUNNING -      Worker thread is running
        * STOPPING -     Worker is not accepting messages, and is processing
                         remaining messages
        * STOPPED -      Worker has completed execution, and is ready to be 
                         joined
        """

        INITIALIZING = "INITIALIZING"
        READY = "READY"
        RUNNING = "RUNNING"
        STOPPING = "STOPPING"
        STOPPED = "STOPPED"


    def __init__(self, parent, config, key=None, map_path=None, **kwargs):
        """
        Create a new FoVWorker.

        Arguments
        ---------
        parent : PyGLFoVAgent
            Agent that created and is managing this worker
        key : tuple 
            Key this worker is referred to by
        map_path : stirng
            Path to JSON file containing map block information
        """

        # Initialize thread-related aspect
        threading.Thread.__init__(self)

        # Create the FoV worker's name
        self.__string_name = '[FoVWorker]'

        self.config = config
        self.parent = parent
        self.key = key
        self.map_path = map_path

        self.state = FoVWorker.State.INITIALIZING


        # Message Queue and related locks for handling the fact that messages
        # may be pushed to the Queue from one thread while consumed by another
        self.message_lock = threading.Lock()


        # Determine which message dispatcher is needed
        # NOTE: For submission to TA3, hard coded to drop stale player state
        #       messages
###        self.messageQueue = Queue()
###        self.messageQueue = LatestPlayerStateDispatcher()
        self.messageQueue = IgnoreMultiplePlayerStateDispatcher()

        self.messages_to_cache = kwargs.get("messages_to_cache", [ EnvironmentCreatedList, EnvironmentRemovedList, EnvironmentCreatedSingle, EnvironmentRemovedSingle, MissionStageTransition ])
        self.message_cache = []

        # pygl related items:
        #   world contains the set of block feeders containing block instances
        #   player_block_store contains a dummy block for each participant
        #   player_vertex_store contains a composite vertex store for rendering players
        #   vertex_store contains a composite vertex store for rendering
        #   perspective is the viewport
        #   fov is the FoV wrapper
        # NOTE: OpenGL components need to be created in the execution thread 
        #       where it will be used in order to ensure context is available

        self.perspective = None
        self.fov = None
        self.color_map = None

        # Participant information -- a pair of dictionaries mapping player 
        # names to Participant instances and lists of endpoints
        self.participants = {}
        self.endpoints = {}

        # Set up the WorldModel and Dynamics
        world_context = WorldModelContext()
        self.world_model = WorldModel(world_context, self.participants)
        self.dynamics_model = DynamicsModel(self.world_model)

        for UpdateClass in WORLD_MODEL_UPDATES:
            self.dynamics_model.add_update(UpdateClass(self.world_model))


        # Endpoint factories will be used to create endpoints as participants
        # are generated
        self.endpoint_factories = []
        self.buildEndpointFactories()


        # Everything is initialized, and the worker is ready to be started.
        self.state = FoVWorker.State.READY

        # Lookup map for how to process each message type
        self.messageProcessors = { PlayerState:          self.processPlayerState,
                                 }

        for update in self.dynamics_model.updates:
            self.messageProcessors[update.MessageClass()] = update

        # Keep track of how long processing player states takes, and now much
        # time is spent not processing player states
        self.process_player_state_times = []
        self.non_render_times = []
        self.last_render_end_time = None


    def __str__(self):
        """
        String representation of the FoV worker)
        """

        return self.__string_name


    def buildEndpointFactories(self):
        """
        Build and add the factories defined by the config file, and add them to
        the worker.
        """

        # TODO: Change this to a DI-type framework, by dynamically importing
        #       the classes from the config file

        for EndpointClass, arguments in self.parent.config["endpoints"].items():

            self.logger.info("%s:  Creating %s Factory", self, EndpointClass)

            # MatplotVisualizer
            if EndpointClass == "MatplotVisualizer":
                colormap_path = arguments.get("colormap_path", None)
                factory = endpoints.factories[EndpointClass](self, 
                                                             colormap_path=colormap_path, 
                                                             fov_agent=self.parent)#.publisher)
                self.endpoint_factories.append(factory)


            if EndpointClass == "BlockSummaryMessageEndpoint":
                blocks_to_summarize = arguments.get("blocks_to_summarize", None)
                timestamp_delay = arguments.get("timestamp_delay", -1)
                scaling_factor = arguments.get("scaling_factor", 1)
                factory = endpoints.factories[EndpointClass](self,
                                                             blocks_to_summarize=blocks_to_summarize,
                                                             fov_agent=self.parent,#.publisher,
                                                             timestamp_delay=timestamp_delay,
                                                             scaling_factor=scaling_factor)
                self.endpoint_factories.append(factory)


            if EndpointClass == "BlockLocationListMessageEndpoint":
                timestamp_delay = arguments.get("timestamp_delay", -1)
                factory = endpoints.factories[EndpointClass](self,
                                                             fov_agent=self.parent,#.publisher,
                                                             timestamp_delay = timestamp_delay)
                self.endpoint_factories.append(factory)


    def addEndpointFactory(self, factory):
        """
        Add a factory to generate endpoints as particpants are generated
        """

        self.logger.info("%s:  Adding endpoint factory: %s", self, factory)

        self.endpoint_factories.append(factory)


    def initializeFoV(self):
        """
        Initialize all the FoV components.  This needs to be called after the
        thread has started.
        """

        self.logger.info("%s:  Creating pygl_fov objects", self)

        # Needed components for the FoV instance: a common color map instance
        # for converting block id to color and back, a composite block feeder
        # for storing chunks of blocks, and a composite vertex store for
        # storing corresponding vertices of each block.
        self.logger.info("%s:      Creating Color Mapper", self)
        self.color_map = pygl_fov.BlockColorMapper()

        self.world_model.initialize()

        # Create an instance of Perspective, which will be the OpenGL context
        # rendered to.
        self.logger.info("%s:      Creating Perspective", self)
        self.perspective = pygl_fov.Perspective(position=(0,0,0),
                                                orientation=(0,0,0),
                                                window_size=(self.config["window_size"]["width"],
                                                             self.config["window_size"]["height"]))

        # Finally, create the FoV instance
        self.logger.info("%s:      Creating FoV", self)
        self.fov = pygl_fov.FOV(self.perspective, self.world_model.world_vertex_store, False, color_map=self.color_map)

        backend_info = self.perspective.context.getBackendInfo()

        self.logger.info("%s:", self)
        self.logger.info("%s:  OpenGL Context and Content Created", self)
        self.logger.info("%s:    Backend:      %s", self, backend_info['backend'])
        self.logger.info("%s:    Vendor:       %s", self, backend_info['vendor'])
        self.logger.info("%s:    Renderer:     %s", self, backend_info['renderer'])
        self.logger.info("%s:    Version:      %s", self, backend_info['version'])
        self.logger.info("%s:    SL Version:   %s", self, backend_info['sl_version'])
        self.logger.info("%s:", self)


    def createParticipant(self, participant_id, **kwargs):
        """
        Create a participant and associated endpoints, and add to the store
        of participants
        """

        # Check if the participant already exists in the store, and ignore if
        # so.
        if participant_id in self.participants.keys():
            self.logger.warning("%s:  Attempted to add participant with existing name: %s", self, participant_id)
            return


        position = kwargs.get("position", (0,0,0))
        orientation = kwargs.get("orientation", (0,0,0))
        window_size = kwargs.get("window_size", (self.config["window_size"]["width"],
                                                 self.config["window_size"]["height"]))

        participant = Participant(participant_id, position=position, 
                                                  orientation=orientation, 
                                                  window_size=window_size)

        self.world_model.addEntity(participant.block, participant.vertices)
        self.world_model.context.add_block_attributes(participant.block, {"participant_id": participant_id})

        self.logger.info("%s:  Participant Block ID: %d", self, participant.block.id)
        self.logger.info("%s:  Participant Block Color: %s", self, str(participant.vertices.color))
       

        # Create a list of endpoints for the participant using the list of
        # endpoint factories
        endpoints = [factory(participant) for factory in self.endpoint_factories]

        self.participants[participant_id] = participant
        self.endpoints[participant_id] = endpoints

        # Provide information to the logger
        self.logger.info("%s:  Added new participant to the store: %s", self, self.participants[participant_id])
        self.logger.info("%s:    Participant Name: %s", self, self.participants[participant_id].name)
        self.logger.info("%s:    Participant Position: %s", self, str(self.participants[participant_id].position))
        self.logger.info("%s:    Participant Orientation: %s", self, str(self.participants[participant_id].orientation))
        self.logger.info("%s:    Participant Window Size: %s", self, str(self.participants[participant_id].window_size))
        self.logger.info("%s:    Endpoints:", self)
        for endpoint in self.endpoints[participant_id]:
            self.logger.info("%s:      %s", self, endpoint)


    ###
    ### PROCESSING SPECIFIC MESSAGES ###
    ###


    def setPlayerPosition(self, message):
        """
        Process a PlayerState by setting the position of the participant, but 
        not rendering the scene

        Args:
            message - a player state
        """

        self.logger.debug("%s:  Processing %s message", self, message)
        
        # The participant may not have been created, so ignore the message is not
        if not message.participant_id in self.participants:
            self.logger.warning("%s:  Participant not in participants: %s", self, message.participant_id)
            return

        with self.world_model.feeder_lock:
            # Grab the relevant participant
            participant = self.participants[message.participant_id]
            participant.set_pose(message.position, message.orientation + (0,))


    def processPlayerState(self, message):
        """
        Method for processing Player State

        Args:
            message - a player state
        """

        self.logger.debug("%s:  Processing %s message", self, message)
        
        # The participant may not have been created, so ignore the message is not
        if not message.participant_id in self.participants:
            self.logger.warning("%s:  Participant not in participants: %s", self, message.participant_id)
            return

        start_time = time.time()

        with self.world_model.feeder_lock:
            # Grab the relevant participant
            participant = self.participants[message.participant_id]

            participant.set_pose(message.position, message.orientation + (0,))
            self.perspective.set_pose(message.position, message.orientation + (0,))

            # Only render if not in shop
            if self.world_model.context.mission_stage in { MissionStage.FieldStage, 
                                                           MissionStage.ReconStage }:

                # Create the pixel to block ID map -- do not render vertices of the
                # participant's avatar
                pixelMap = self.fov.calculatePixelToBlockIdMap(ignore=[participant.vertices])

                # Send the calcualted pixel map to all of the participant's
                # endpoints
                for endpoint in self.endpoints[message.participant_id]:
                    endpoint(pixelMap, player_state_message=message)

        end_time = time.time()

        self.process_player_state_times.append(end_time - start_time)

        if self.last_render_end_time is not None:
            self.non_render_times.append(start_time - self.last_render_end_time)

        self.last_render_end_time = end_time



    def consume(self, message):
        """
        Consume the message, updating the correspnding FoV component
        """

        self.logger.debug("%s:  Consuming %s message", self, message)        

        # message shouldn't be None, but check just in case
        if message is None:
            self.logger.warning("%s:  Consume received None for argument", self)
            return

        # Get a processor for the message, if it exists
        processor = self.messageProcessors.get(message.__class__, None)

        # Process the message
        if processor is not None:
            processor(message)


    def run(self):
        """
        Start the FoVWorker thread
        """

        self.logger.info("%s:  run()", self)

        # OpenGL components need to be created in the execution thread where 
        # it will be used
        self.initializeFoV()

        self.logger.info("%s:  PARTICIPANTS IN PARENT:", self)
        for participant in self.parent.participants:
            self.logger.info("%s:    %s", self, participant)

        # Create participants
        # HACK:  Participants will be overwritten if provided the same position.
        #        Start the participants at an offset of 0.5 on each access, and
        #        increment one axis
        _x, _y, _z = (0.5, 0.5, 0.5)
        for participant in self.parent.participants:
            self.logger.info("%s:  Creating a new participant: %s", self, participant.participant_id)
            self.createParticipant(participant.participant_id,
                                   position=(_x,_y,_z),
                                   orientation=(0,0,0))
            _z += 1


        self.logger.info("%s:      Loading Minecraft World Data", self)

        if self.map_path is None:
            self.logger.error("%s:      No path provided to Mission map.  Cannot load blocks of base map.", self)
            return
        
        # Load the block feeder from the provided json path, and add it to the
        # FoV instance
        self.world_model.addBlocksFromJson(self.map_path)



        # Pass the initial victim and blockage lists
        self.logger.info("%s:  Processing Cached Messages", self)
        for message in self.message_cache:
            print(message)
            self.consume(message)

        # Wait until the worker is ready -- yield so that another process
        # can get some execution time in
        while self.state != FoVWorker.State.READY:
            time.sleep(0)        

        self.logger.info("%s:  Starting FoV Worker Thread", self)

        self.state = FoVWorker.State.RUNNING

        while self.state == FoVWorker.State.RUNNING:

            # Inner loop ensures that the message queue gets to complete
            # after stop is called
            while len(self.messageQueue) > 0:
                with self.message_lock:
                    message = self.messageQueue.next()

                if message is not None:
                    self.consume(message)

                # Yield the thread
                time.sleep(0)


    def stop(self):
        """
        Indicate the thread should stop
        """

        # No need to do anything if already stopping or stopped
        if self.state in [FoVWorker.State.STOPPING, FoVWorker.State.STOPPED]:
            return

        # Change the state to STOPPING, to indicate that there could still
        # be some messages in the queue to process.
        self.state = FoVWorker.State.STOPPING
        self.logger.info("%s:  Stopping FoV Worker Thread", self)
        self.last_render_end_time = None



    def kill(self):
        """
        Force a quick stop to the thread, deleting the remaining contents of
        the messageQueue
        """

        self.stop()
        self.messageQueue.empty()


    def addMessage(self, message):
        """
        Add a message to the message queue to be processed
        """

        # Don't queue any messages if this worker is done
        if self.state in [FoVWorker.State.STOPPING, FoVWorker.State.STOPPED]:
            return

        # Queue any received messages while running
        if self.state == FoVWorker.State.RUNNING:
            with self.message_lock:
                self.messageQueue.add(message)
        else:
            if message.__class__ in self.messages_to_cache:
                self.message_cache.append(message)


    def get_profile_stats(self):
        """
        Return the profiling statistics for PlayerState processing times.

        Returns
        -------
        dictionary
            "counts": Number of times PlayerState was processed
            "average_time": Average time to process PlayerState messages
            "stdev_time": Standard deviation of time to process PlayerState messages
            "min_time": Minimum time to process PlayerState messages
            "max_time": Maximum time to process PlayerState messages
        """

        # Calculate the statistics, or provide dummy values if no method calls
        # have been made yet
        if len(self.process_player_state_times) == 0:
            average_time = 0.0
            stdev_time = 0.0
            min_time = 0.0
            max_time = 0.0
        else:
            average_time = np.mean(self.process_player_state_times)
            stdev_time = np.std(self.process_player_state_times)
            min_time = min(self.process_player_state_times)
            max_time = max(self.process_player_state_times)

        if len(self.non_render_times) == 0:
            non_render_average_time = 0.0
            non_render_stdev_time = 0.0
            non_render_min_time = 0.0
            non_render_max_time = 0.0
        else:
            non_render_average_time = np.mean(self.non_render_times)
            non_render_stdev_time = np.std(self.non_render_times)
            non_render_min_time = min(self.non_render_times)
            non_render_max_time = max(self.non_render_times)

        return { "counts": len(self.process_player_state_times),
                 "average_time": average_time,
                 "stdev_time": stdev_time,
                 "min_time": min_time,
                 "max_time": max_time,
                 "non_render_average_time": non_render_average_time,
                 "non_render_stdev_time": non_render_stdev_time,
                 "non_render_min_time": non_render_min_time,
                 "non_render_max_time": non_render_max_time
               }

    def reset_profile_stats(self):
        """
        """

        self.process_player_state_times.clear()
        self.non_render_times.clear()
