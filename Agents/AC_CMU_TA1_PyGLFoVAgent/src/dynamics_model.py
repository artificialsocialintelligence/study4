from MinecraftBridge.utils import Loggable

class DynamicsModel(Loggable):
	"""
	The DynamicsModel class defines how messages should be processed to affect
	the linked WorldModel.  The DynamicsModel is composed at run-time by a set
	of callable Update objects, which are defined for each study / world.
	"""

	def __init__(self, world_model):
		"""
		Arguments
		---------
		world_model : WorldModel
			World model whose state is updated by the dynamics
		"""

		self.world_model = world_model
		self.updates = []


	def add_update(self, update):
		"""
		Arguments
		---------
		update : DynamicsUpdate
			Update method to add
		"""

		self.updates.append(update)
