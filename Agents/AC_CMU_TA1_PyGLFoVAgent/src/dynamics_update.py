import abc

from MinecraftBridge.utils import Loggable

class DynamicsUpdate(abc.ABC, Loggable):
	"""
	A DynamicsUpdate class defines an object that subscribes to a message and
	performs an update on a WorldModel provided at construction.
	"""

	def __init__(self, world_model):
		"""
		Arguments
		---------
		world_model : WorldModel
			World model updated by this object
		"""

		self.world_model = world_model


	def __str__(self):
		"""
		String representation of the update
		"""

		return self.__class__.__name__


	@property
	@abc.abstractmethod
	def MessageClass(self):
		"""
		Message type this Update responds to.  This needs to be defined for 
		each subclass of the update
		"""

		raise NotImplementedError


	@abc.abstractmethod
	def __call__(self, message):
		"""
		Callback when a message of the MessageClass is received.

		Arguments
		---------
		message : MinecraftBridge.message
			Received message
		"""

		pass
