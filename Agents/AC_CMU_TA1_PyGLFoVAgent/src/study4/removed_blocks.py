"""
"""

import MinecraftElements

import pygl_fov

from MinecraftBridge.messages import (
    EnvironmentRemovedList,
    EnvironmentRemovedSingle
)

from dynamics_update import DynamicsUpdate

class RemovedBlockListUpdate(DynamicsUpdate):
    """
    Definition of what happens when an EnvironmentRemovedList is received
    """

    def __init__(self, world_model):
        """
        """

        super().__init__(world_model)


    def __str__(self):
        """
        """

        return self.__class__.__name__


    def MessageClass(self):
        """
        """

        return EnvironmentRemovedList


    def __call__(self, message):
        """
        """

        self.logger.debug("%s:  Processing %s message", self, message)

        # Add created blocks into the block feeder
        with self.world_model.feeder_lock:

            self.logger.debug("%s:    Number of blocks prior to processing: %s", self, len(self.world_model.world))

            for environment_block in message.objects:

                location = environment_block.location

                # Check to see if a block exists there
                if not self.world_model.containsBlockAt(location):
                    self.logger.error("%s:  Attempting to remove non-existant block at %s", self, location)
                    return

                # Hide the block
                self.world_model.hide(self.world_model.getBlockAt(location))

            self.logger.debug("%s:    Number of blocks after processing: %d", self, len(self.world_model.world))



class RemovedBlockSingleUpdate(DynamicsUpdate):
    """
    Definition of what happens when an EnvironmentRemovedSingle is received
    """

    def __init__(self, world_model):
        """
        """

        super().__init__(world_model)


    def __str__(self):
        """
        """

        return self.__class__.__name__


    def MessageClass(self):
        """
        """

        return EnvironmentRemovedSingle


    def __call__(self, message):
        """
        """

        self.logger.debug("%s:  Processing %s message", self, message)

        # Add created blocks into the block feeder
        with self.world_model.feeder_lock:
            self.logger.debug("%s:    Number of blocks prior to processing: %s", self, len(self.world_model.world))

            # Location of the block to remove
            location = message.object.location

            # Check to see if the block exists
            if not self.world_model.containsBlockAt(location):
                self.logger.error("%s:  Attempting to remove non-existant block as %s", self, location)
                self.logger.error("%s:  Message Contents: %s", self, message.toJson())
                return

            # Hide the block
            self.world_model.hide(self.world_model.getBlockAt(location))

            self.logger.debug("%s:    Number of blocks after processing: %d", self, len(self.world_model.world))