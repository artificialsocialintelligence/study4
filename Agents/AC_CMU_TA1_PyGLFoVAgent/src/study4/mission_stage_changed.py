"""
"""

import MinecraftElements

import pygl_fov

from MinecraftBridge.messages import MissionStageTransition

from dynamics_update import DynamicsUpdate

class MissionStageChangedUpdate(DynamicsUpdate):
    """
    Definition of what happens when an Mission Stage Transition is received
    """

    def __init__(self, world_model):
        """
        """

        super().__init__(world_model)


    def __str__(self):
        """
        """

        return self.__class__.__name__


    def MessageClass(self):
        """
        """

        return MissionStageTransition


    def __call__(self, message):
        """
        """

        self.logger.debug("%s:  Processing %s message", self, message)

        # Simply set the mission stage in the context to that of the MissionStageTransition message
        self.world_model.context.mission_stage = message.mission_stage