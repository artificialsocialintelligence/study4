"""
block_summary.py

The BlockSummaryMessageEndpoint defines a wrapper for the pygl_fov 
FilteredBlockListSummary endpoint, by converting the list of block summaries to
a message, as well as defining a Factory to create the endpoint.
"""

###import logging
from MinecraftBridge.utils import Loggable

import pygl_fov.endpoints

from MinecraftBridge.messages import FoVSummary, FoVDelta, FoVBlock, BusHeader, MessageHeader
from MinecraftBridge.mqtt.parsers import MessageType, MessageSubtype

import MinecraftElements

import numpy as np

import datetime
#from dateutil.parser import isoparse


class BlockSummaryMessageEndpoint(Loggable):
	"""
	A BlockSummaryMessage endpoint calcualtes the pixel statistics of blocks of
	interest using the pygl_fov FilteredBlockSummary endpoint, and creates a
	message to put on the bus with the resulting summary contents.
	"""


	class Factory(Loggable):
		"""
		A Factory for creating BlockSummaryMessageEndpoint instances.
		"""

		def __init__(self, worker, **kwargs):
			"""
			Create a BlockSummaryMessageEndpoint Factory

			Args:
				worker - instance of the FoVWorker that uses this factory to
				         create endpoints for discovered participants.
			"""

			if not "scaling_factor" in kwargs:
				self.logger.warning("%s: No Scaling Factor!", self)

			self.worker = worker
			self.block_list = kwargs.get("blocks_to_summarize", None)
			self.fov_agent = kwargs.get("fov_agent", None)
			self.timestamp_delay = kwargs.get("timestamp_delay", -1)
			self.scaling_factor = kwargs.get("scaling_factor", 1)


			# If no blocks were provided to summarize, assume that all block
			# types are to be summarized
			if self.block_list is None:
				self.block_list = set(MinecraftElements.Block)
			else:
				# Convert the string representation of blocks to
				# MinecraftElements.Block instances
				self.block_list = set([MinecraftElements.Block[block] for block in self.block_list])


		def __call__(self, participant):
			"""
			Create a BlockSummaryMessageEndpoint instance for the provided
			participant.

			Args:
				participant - participant whose FoV is used to provide block ID
				              maps in the generated instance.
			"""

			if self.worker is None:
				self.logger.error("%s:  Unable to create instance of BlockSummaryMessageEndpoint.  Provided worker handle is None.", self)
				return lambda: None

			if self.fov_agent is None:
				self.logger.error("%s:  Unable to create instance of BlockSummaryMessageEndpoint.  FoV agent / message bus not provided", self)


			# Create the 
			block_list_summary = pygl_fov.endpoints.FilteredBlockListSummary(self.worker.world_model.world,
				                                                             self.block_list)

			return BlockSummaryMessageEndpoint(self.fov_agent, block_list_summary, self.worker.world_model,
				                               timestamp_delay=self.timestamp_delay,
				                               scaling_factor=self.scaling_factor)



	def __init__(self, message_bus, block_summary_endpoint, world_model, **kwargs):
		"""
		Create a new endpoint, connected to the supplied bridge.

		The optional timestamp_delay argument will timestamp the bus and message
		header by the provided delay amount (in seconds).  If the delay is None
		or negative, then the message headers will be timestamped using the 
		current time.

		Args:
			message_bus - handle to the bridge to the message bus to publish to
			block_summary_endpoint - instance of a FilteredBlockListSummary

		Keyword Arguments:
			timestamp_delay - amount of time to advance the timestamp of the 
			                  message from the received PlayerState message.
			scaling_factor  - amount to scale the pixel count and bounding box
			                  values, based on the difference of actual viewport
			                  dimentions from "standard" viewport dimensions
			message_source  - string name for the message header source
			message_type    - instance of MinecraftBridge.messages.MessageType
			message_subtype - instance of MinecraftBridge.messages.MessageSubtype
			testbed_version - version number of the testbed
		"""

		# String representation of the instance
		self.__string_name = "[BlockSummaryMessageEndpoint]"

		# Store the timestamp_delay, converting to None if negative
		self.timestamp_delay = kwargs.get("timestamp_delay", None)
		if self.timestamp_delay is not None and self.timestamp_delay < 0:
			self.timestamp_delay = None
		self.scaling_factor = kwargs.get("scaling_factor", 1)
		if self.scaling_factor <= 0:
			self.scaling_factor = 1

		# Store keyword arguments, or default values in not provided
		self.message_source = kwargs.get("message_source", "PyGL_FoV_Agent")
		self.message_type = kwargs.get("message_type", MessageType.observation)
		self.message_subtype = kwargs.get("message_subtype", MessageSubtype.FoV)
		self.testbed_version = kwargs.get("testbed_version", 0.5)
		self.timestamp_delay = kwargs.get("timestamp_delay", -1)

		self.world_model = world_model


		# Store the base endpoint and message bus
		self.block_summary_endpoint = block_summary_endpoint
		self.message_bus = message_bus

		# Store the locations of blocks in the previous observation
		self.publish_delta_blocks = kwargs.get("publish_delta_blocks", True)
		self.previous_block_locations = []
		self.previous_blocks = []


	def __str__(self):
		"""
		String representation of the instance
		"""

		return self.__string_name


	def create_headers(self, player_state_message, message_subtype):
		"""
		Create the bus and message headers using the contents of the player
		state message
		"""

		if player_state_message is None:
			self.logger.error("%s:  Attempting to create message headers with 'None' PlayerState message", self)
			return

		# Get the experiment_id, trial_id, and replay_id (if it exists), and
		# timestamps from the player_state_message
		experiment_id = player_state_message.headers["msg"].experiment_id
		trial_id = player_state_message.headers["msg"].trial_id
		replay_id = player_state_message.headers["msg"].replay_id


		# Calculate the timestamps for the bus and message headers
		if self.timestamp_delay is not None and self.timestamp_delay > 0:
			bus_timestamp = player_state_message.headers["header"].timestamp
			msg_timestamp = player_state_message.headers["msg"].timestamp

			# Parse the timestamps, and advance by the number of seconds
			bus_timestamp = bus_timestamp + datetime.timedelta(seconds=self.timestamp_delay)
			msg_timestamp = msg_timestamp + datetime.timedelta(seconds=self.timestamp_delay)

		else:
			bus_timestamp = datetime.datetime.utcnow()
			msg_timestamp = datetime.datetime.utcnow()


		# Create the bus and message headers
		bus_header = BusHeader(self.message_type, 
			                   timestamp=bus_timestamp,
			                   version=self.testbed_version)

		msg_header = MessageHeader(message_subtype,
			                       experiment_id,
			                       trial_id,
			                       self.message_source,
			                       timestamp=msg_timestamp,
			                       version=self.testbed_version,
			                       replay_id=replay_id)

		return bus_header, msg_header


	def __call__(self, pixelMap, **kwargs):
		"""
		Calculate the pixel statistics of blocks of intetest, construct a
		message and send to the bus
		"""

		self.logger.debug("%s: Generating Block Summary Message", self)

		# Get the original player state message to get needed information,
		# e.g., observation_number
		player_state_message=kwargs.get("player_state_message", None)

		if player_state_message is None:
			self.logger.error("%s:  Unable to create BlockSummary message -- PlayerState message not provided.", self)
			return

		# Create the bus and message headers
		busHeader, msgHeader = self.create_headers(player_state_message, self.message_subtype)

		# Calculate the block summary
		blockSummary = self.block_summary_endpoint(pixelMap)

		# Get the player's name and the observation number for the message data
		participant_id = player_state_message.participant_id
		observation_number = player_state_message.observation_number

		# Create the FoVSummary message
		message = FoVSummary(participant_id=participant_id,
			                 observationNumber=observation_number)
		message.addHeader("header", busHeader)
		message.addHeader("msg", msgHeader)

		# Maintain blocks for next timestep, and which entered / leaved
		current_blocks = []
		current_block_locations = []
		entered_blocks = []
		left_blocks = []

		# Add the blocks that have been summarized
		for summary in blockSummary:

			block = FoVBlock(summary['block'].block_type,
				             summary['block'].location,
				             int(summary['pixel_count'] * (self.scaling_factor**2)),
				             { "x": [int(summary['bounding_box'][0] * self.scaling_factor),
				                     int(summary['bounding_box'][1] * self.scaling_factor)],
				               "y": [int(summary['bounding_box'][2] * self.scaling_factor),
				                     int(summary['bounding_box'][3] * self.scaling_factor)]

				             }
				             )

			for name in self.world_model.context.get_block_attribute_names(summary['block']):
				value = self.world_model.context.get_block_attribute(summary['block'], name)
				block.add_attribute(name, value)

			block.finalize()

			message.addBlock(block)

		self.message_bus.publish(message)

		# If the flag is set, determine which blocks have entered / left the
		# participant's FoV and publish
		if self.publish_delta_blocks:

			current_block_locations = [block.location for block in message.blocks]

			entered_blocks = [block for block in message.blocks if block.location not in self.previous_block_locations]

			left_blocks = [block for block in self.previous_blocks if block.location not in current_block_locations]

			self.previous_blocks = message.blocks
			self.previous_block_locations = current_block_locations

			# Only publish if a block has entered or left the FoV
			if len(entered_blocks) > 0 or len(left_blocks) > 0:
				delta_message = FoVDelta(participant_id=participant_id,
					                     observationNumber=observation_number,
					                     entered_blocks=entered_blocks,
					                     left_blocks=left_blocks)
				delta_busHeader, delta_msgHeader = self.create_headers(player_state_message, MessageSubtype.FoV_Delta)
				delta_message.addHeader("header", delta_busHeader)
				delta_message.addHeader("msg", delta_msgHeader)

				self.message_bus.publish(delta_message)






