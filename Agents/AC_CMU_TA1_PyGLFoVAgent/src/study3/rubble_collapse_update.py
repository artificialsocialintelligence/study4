"""
"""

import MinecraftElements

from MinecraftBridge.messages import RubbleCollapse

from dynamics_update import DynamicsUpdate

class RubbleCollapseUpdate(DynamicsUpdate):
    """
    Definition of what happens when a
    """

    def __init__(self, world_model):
        """
        """

        super().__init__(world_model)


    def __str__(self):
        """
        """

        return self.__class__.__name__


    def MessageClass(self):
        """
        """

        return RubbleCollapse


    def __call__(self, message):
        """
        """

        print("RUBBLE COLLAPSE")

        self.logger.debug("%s:  Processing %s message", self, message)

        # Sort the x, y, and z values of the two end points, to make sure that
        # the lower values are indeed less than the upper values
        x_min = min(message.fromBlock_x, message.toBlock_x)
        x_max = max(message.fromBlock_x, message.toBlock_x)
        y_min = min(message.fromBlock_y, message.toBlock_y)
        y_max = max(message.fromBlock_y, message.toBlock_y)
        z_min = min(message.fromBlock_z, message.toBlock_z)
        z_max = max(message.fromBlock_z, message.toBlock_z)

        # Get all the locations in the rectangular region
        rubble_locations = [(x,y,z) for x in range(x_min, x_max+1)
                                    for y in range(y_min, y_max+1)
                                    for z in range(z_min, z_max+1)]

        # Go through the locations, and add blocks where applicable
        blocks = []

        for location in rubble_locations:
            # Was there a block here?
            if self.containsBlockAt(location):
                block_to_remove = self.getBlockAt(location)

                # Is the block a marker block or sign
                if block_to_remove.block_type in MinecraftElements.Block.markers():
                    self.logger.debug("%s:    Ignoring Block %s", self, block_to_remove)
                elif block_to_remove.block_type == MinecraftElements.Block.wall_sign:
                    self.logger.debug("%s:    Ignoring Block %s", self, block_to_remove)
                else:
                    blocks.append(pygl_fov.Block(location, MinecraftElements.Block.gravel))
                    self.world_model.hide(block_to_remove)

            # No block, go ahead and add the rubble
            else:
                blocks.append(pygl_fov.Block(location, MinecraftElements.Block.gravel))

        self.logger.debug("%s:  Added %d rubble blocks due to collapse", self, len(blocks))

        self.world_model.addBlocks(blocks)