"""
"""

import MinecraftElements

from MinecraftBridge.messages import VictimPlaced

from dynamics_update import DynamicsUpdate

class VictimPlacedUpdate(DynamicsUpdate):
    """
    Definition of what happens when a
    """

    def __init__(self, world_model):
        """
        """

        super().__init__(world_model)


    def __str__(self):
        """
        """

        return self.__class__.__name__


    def MessageClass(self):
        """
        """

        return VictimPlaced


    def __call__(self, message):
        """
        """

        self.logger.debug("%s:  Processing %s message", self, message)

        # Who is placing the victim
        participant = self.world_model.participants.get(message.participant_id, None)

        # Check to see if the participant exists; if not, raise a warning
        if participant is None:
            self.logger.warning("%s:  Participant not in participant store: %s", self, message.participant_id)
            return

        # Make sure the participant has a victim block
        if participant.victim_block is None:
            self.logger.warning("%s:    Participant %s not carrying a victim block", self, participant)
            return

        # Unhide the victim block, and change the block's location
        if self.world_model.isHidden(participant.victim_block):
            self.world_model.unhide(participant.victim_block, message.location)
        else:
            self.logger.warning("%s:    Victim block %s not present in hidden block store", self, participant.victim_block)
            # TODO: Create a victim block and add it to the store

        # Make sure the participant is no longer holding a block
        participant.victim_block = None