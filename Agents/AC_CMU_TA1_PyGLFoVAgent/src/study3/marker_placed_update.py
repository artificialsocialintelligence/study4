"""
"""

import MinecraftElements

import pygl_fov

from MinecraftBridge.messages import MarkerPlacedEvent

from dynamics_update import DynamicsUpdate

class MarkerPlacedUpdate(DynamicsUpdate):
    """
    Definition of what happens when a
    """

    def __init__(self, world_model):
        """
        """

        super().__init__(world_model)


    def __str__(self):
        """
        """

        return self.__class__.__name__


    def MessageClass(self):
        """
        """

        return MarkerPlacedEvent


    def __call__(self, message):
        """
        """

        self.logger.debug("%s:  Processing %s message", self, message)

        # Get the location and type of marker placed
        location = message.location
        marker_type = message.marker_type
        owner = message.participant_id

        # Create the marker block
        marker_block = pygl_fov.Block(location, 
                                      MinecraftElements.Block.marker_block,
                                      marker_type = marker_type,
                                      playername = owner)

        # Check to see if a block exists where the marker is being placed.  If
        # it's a marker block, no need to store it for future retrieval.
        if self.world_model.containsBlockAt(location):
            self.logger.debug("%s:    Placing marker block %s at occupied location: %s", self, marker_block, str(location))
            if self.world_model.getBlockAt(location).block_type is not MinecraftElements.Block.marker_block:
                self.world_model.context.marker_block_replacements[location] = self.world_model.getBlockAt(location)
            self.world_model.hide(self.world_model.getBlockAt(location))


        # Add the block to the world.  Vertices for the block will be created
        # automatically and loaded into OpenGL
        self.world_model.addBlocks([marker_block])
