"""
"""

import MinecraftElements

from MinecraftBridge.messages import PerturbationRubbleLocations

import pygl_fov

from dynamics_update import DynamicsUpdate

class RubblePerturbationUpdate(DynamicsUpdate):
    """
    Definition of what happens when a
    """

    def __init__(self, world_model):
        """
        """

        super().__init__(world_model)


    def __str__(self):
        """
        """

        return self.__class__.__name__


    def MessageClass(self):
        """
        """

        return PerturbationRubbleLocations


    def __call__(self, message):
        """
        """

        self.logger.debug("%s:  Processing %s message", self, message)

        
        # Add the blockages to the block feeder
        with self.world_model.feeder_lock:
            self.logger.debug("%s:      Number of blocks prior to processing: %d", self, len(self.world_model.world))

            # Start a new block feeder to store blockages in
            blockage_blocks = []

            for blockage in message.blockages:
                block = pygl_fov.Block(blockage.location, 
                                       block_type=blockage.block_type)

                self.logger.debug("%s:  Blockage: %s", self, block)

                # If the 'blockage' is air, remove the block at that location
                # from the feeder
                if block.block_type == MinecraftElements.Block.air:

                    # Hide the block -- this is to avoid the need to
                    # recalculate the VBO, as would need to be done if the block
                    # was removed.
                    self.world_model.hide(self.world_model.getBlockAt(block.location))

                    # If the configuration is such to render a wireframe for
                    # these openings, then set the block_type to a
                    # ProxyBlock.opening and add to the feeder
                    if self.config["include_openings"]:
                        block.block_type = MinecraftElements.Block.perturbation_opening

                        self.logger.debug("%s:        Adding opening block at location %s", self, block.location)
                        blockage_blocks.append(block)                       

                else:
                    self.logger.debug("%s:        Adding block of type %s at location %s", self, block.block_type, block.location)

                    # See if we need to remove a block first
                    if self.world_model.containsBlockAt(block.location):
                        removed_block = self.world_model.getBlockAt(block.location)

                        # Hide the block to remove
                        self.world_model.hide(removed_block)

                    blockage_blocks.append(block)

            # Add the blockage feeder to the world, also creating a vertex
            # store in the process
            self.world_model.addBlocks(blockage_blocks)

            self.logger.debug("%s:      Number of blocks after processing: %d", self, len(self.world_model.world))