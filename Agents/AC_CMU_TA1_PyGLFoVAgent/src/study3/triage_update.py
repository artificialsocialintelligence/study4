"""
"""

import MinecraftElements

from MinecraftBridge.messages import TriageEvent

from dynamics_update import DynamicsUpdate

print("TRIAGE UPDATE IMPORTED")

class TriageUpdate(DynamicsUpdate):
    """
    Definition of what happens when a triage event occurs
    """

    def __init__(self, world_model):
        """
        """

        super().__init__(world_model)

        print("TRIAGE UPDATE CREATE")


    def __str__(self):
        """
        """

        return self.__class__.__name__


    def MessageClass(self):
        """
        """

        print("TRIAGE EVENT MESSAGE CLASS")

        return TriageEvent


    def __call__(self, message):
        """
        """

    def processTriageEvent(self, message):
        """
        Change the block type at the provided location to "victim_saved" to
        indicate the victim was triaged
        """

        print("TRIAGE!!!")

        self.logger.debug("%s:  Processing %s message", self, message)

        with self.world_model.feeder_lock:

            # Double check -- event type should be TriageState.SUCCESS
            if message.triage_state != TriageEvent.TriageState.SUCCESSFUL:
                self.logger.debug("%s:    Received non-success triage event message", self)
                return

            # Get the block at the location
            victim_block = self.world_model.getBlockAt(message.victim_location)

            print("TRIAGE EVENT")
            print("  Victim Block: ", victim_block)
            print("  Block Type: ", victim_block.block_type)

            if victim_block is not None:
                # Make sure that the block is indeed a victim type
                if victim_block.block_type in [MinecraftElements.Block.block_victim_1,
                                               MinecraftElements.Block.block_victim_2,
                                               MinecraftElements.Block.block_victim_1b,
                                               MinecraftElements.Block.block_victim_proximity]:
                    # Convert the block to a saved victim
                    victim_block.block_type = MinecraftElements.Block.block_victim_saved

                else:
                    self.logger.warning("%s:    Received %s message; block type %s at location %s not an untriaged victim type.", self, message, victim_block.block_type, victim_block.location)
            else:
                self.logger.warning("%s:    Received %s message; no block at location %s.", self, message, message.victim_location)

            print("  New Block Type: ", victim_block.block_type)