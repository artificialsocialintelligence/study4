"""
"""

import MinecraftElements

from MinecraftBridge.messages import VictimList

import pygl_fov

from dynamics_update import DynamicsUpdate

class VictimListUpdate(DynamicsUpdate):
	"""
	Definition of what happens when a victim list is received
	"""

	def __init__(self, world_model):
		"""
		"""

		super().__init__(world_model)


	def __str__(self):
		"""
		"""

		return self.__class__.__name__


	def MessageClass(self):
		"""
		"""

		return VictimList


	def __call__(self, message):
		"""
		"""

		self.logger.debug("%s:  Processing %s message", self, message)

		# Add the victims in the list to the block feeder
		with self.world_model.feeder_lock:
			self.logger.debug("%s:      Number of blocks prior to processing: %d", self, len(self.world_model.world))

			# Start a new block feeder to store victim blocks in
			victim_blocks = []

			for victim in message.victims:
				self.logger.debug("%s:    Creating Victim Block of type %s at location %s", self, victim.block_type, str(victim.location))

				# Hide any block this victim is re-occupying
				if self.world_model.containsBlockAt(victim.location):
					self.world_model.hide(self.world_model.getBlockAt(victim.location))

				block = pygl_fov.Block(victim.location, block_type=victim.block_type,
									   victim_id = victim.unique_id)
				victim_blocks.append(block)

			# Add the victim feeder to the world, also creating a VBO store
			self.world_model.addBlocks(victim_blocks)

			self.logger.debug("%s:      Number of blocks after processing: %d", self, len(self.world_model.world))