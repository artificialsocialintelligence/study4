﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MQTTnet;
using MQTTnet.Client;
using MQTTnet.Extensions.ManagedClient;
using MQTTnet.Packets;
using MQTTnet.Server;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json.Nodes;
using System.Threading;
using System.Threading.Tasks;

namespace ScalingMetricService.Services
{
    public class MQTTService : IMQTTService
    {
        private ILogger Log { get; set; }
        private IManagedMqttClient Client { get; set; }

        public Boolean Connected = false;

        private string ClientID { get; set; }
        private int MQTTPort { get; set; }
        private string MQTTIP { get; set; }
        private string InstancesStateTopic { get; set; }
        private Timer Timer { get; set; }
        private int MonitoringFileLogInterval { get; set; }
        private string InstancesJSON { get; set; }

        public MQTTService(ILoggerFactory loggerFactory, IConfiguration config)
        {
            Log = loggerFactory.CreateLogger<MQTTService>();

            var factory = new MqttFactory();
            Client = factory.CreateManagedMqttClient();
            ClientID = config.GetSection("Mqtt")["clientID"];
            MQTTIP = config.GetSection("Mqtt")["host"];
            MQTTPort = Int32.Parse(config.GetSection("Mqtt")["port"]);
            InstancesJSON = null;

            InstancesStateTopic = config.GetSection("Mqtt")["instancesStateTopic"];

            Log.LogInformation("MQTTService: MQTT Client Initializing");

            try
            {
                this.Connect().Wait();

            }
            catch (Exception e)
            {
                Log.LogError("MQTTService: There was an error establishing the MQTT connection on the Client Side");
                Log.LogError(null, e);
            }

            MonitoringFileLogInterval = config.GetValue<int>("MonitoringFileLogInterval");

            // Start Heart Beat Task
            Timer = new Timer(MonitoringFileLogTask, null, TimeSpan.Zero,
                TimeSpan.FromSeconds(MonitoringFileLogInterval));
        }

        public async Task Connect()
        {

            ManagedMqttClientOptions options = new ManagedMqttClientOptionsBuilder()
                .WithAutoReconnectDelay(TimeSpan.FromSeconds(5))
                .WithClientOptions(new MqttClientOptionsBuilder()
                    .WithClientId(ClientID)
                    .WithTcpServer(MQTTIP, MQTTPort)
                    .WithCleanSession()
                    .Build())
                .Build();

            await Client.StartAsync(options);

            await Client.SubscribeAsync(new List<MqttTopicFilter> { new MqttTopicFilterBuilder().WithTopic(InstancesStateTopic).Build() });

            // Set up handlers
            Client.ConnectedAsync += MQTTClient_ConnectedAsync;


            Client.DisconnectedAsync += MQTTClient_DisconnectedAsync;


            Client.ApplicationMessageReceivedAsync += MQTTClient_ApplicationMessageReceivedAsync;

            Log.LogInformation("MQTTService: MQTT Client Running");
        }

        Task MQTTClient_ConnectedAsync(MqttClientConnectedEventArgs arg)
        {
            Log.LogInformation("MQTTService: Connected to MQTT Broker @ " + MQTTIP + ":" + MQTTPort + "!");
            Connected = true;
            return Task.CompletedTask;
        }

        Task MQTTClient_DisconnectedAsync(MqttClientDisconnectedEventArgs arg)
        {
            Log.LogInformation("MQTTService: Attempting to Connect to " + MQTTIP);
            Log.LogInformation("MQTTService: Disconnected from MQTT Broker!");
            Connected = false;
            return Task.CompletedTask;
        }

        Task MQTTClient_ApplicationMessageReceivedAsync(MqttApplicationMessageReceivedEventArgs arg)
        {      
            // Handle External Monitoring Service Messages
            if (arg.ApplicationMessage.Topic == InstancesStateTopic)
            {
                try
                {
                    String jsonString = Encoding.UTF8.GetString(arg.ApplicationMessage.PayloadSegment);
                    var jsonObject = JObject.Parse(jsonString);
                    InstancesJSON = jsonObject.ToString(Formatting.None);
                }
                catch (Exception ex)
                {
                    Log.LogError("Error parsing json message");
                }
            }
            return Task.CompletedTask;
        }

        public void MonitoringFileLogTask(object? state)
        {
            if (!String.IsNullOrEmpty(InstancesJSON))
            {
                Log.LogInformation(InstancesJSON);
            }
        }
    }
}
