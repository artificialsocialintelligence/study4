﻿using DebounceThrottle;
using Google.Api.Gax;
using Google.Cloud.Compute.V1;
using ScalingMetricService.Models;

namespace ScalingMetricService.Services
{
    public class InstanceService : IInstanceService
    {
        private readonly ILogger<InstanceService> _logger;

        public InstanceService(ILogger<InstanceService> logger, IConfiguration configuration)
        {
            _logger = logger;
        }

        public bool AddInstanceToGroup(string projectId, Instance instanceInfo, string instanceGroup, string zone)
        {
            try
            {
                // Create client
                InstanceGroupsClient instanceGroupsClient = InstanceGroupsClient.Create();

                var instancesRequest = new InstanceGroupsAddInstancesRequest();
                var instanceReference = new InstanceReference()
                {
                    Instance = instanceInfo.SelfLink
                };
                instancesRequest.Instances.Add(instanceReference);


                AddInstancesInstanceGroupRequest request = new AddInstancesInstanceGroupRequest()
                {
                    Project = projectId,
                    Zone = zone,
                    InstanceGroup = instanceGroup,
                    InstanceGroupsAddInstancesRequestResource = instancesRequest
                };
                instanceGroupsClient.AddInstances(request);

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);

                return false;
            }
        }

        public Instance? GetInstanceInfo(string projectId, string instanceName, string zone)
        {
            try
            {
                // Create client
                InstancesClient instanceClient = InstancesClient.Create();

                Instance instanceInfo = instanceClient.Get(projectId, zone, instanceName);
                return instanceInfo;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);

                return null;
            }
        }

        private static Random random = new Random();

        private static string RandomString(int length)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public string GenerateInstanceName(string instanceGroup)
        {
            return $"{instanceGroup}-{RandomString(6)}";
        }

        public bool StartNewInstance(string projectId, string name, string instanceTemplate, string zone)
        {
            try
            {
                // Create client
                InstancesClient instanceClient = InstancesClient.Create();

                // Create request
                InsertInstanceRequest insertInstanceRequest = new InsertInstanceRequest()
                {
                    Project = projectId,
                    Zone = zone,
                    InstanceResource = new Instance()
                    {
                        Name = name
                    },
                    SourceInstanceTemplate = $"global/instanceTemplates/{instanceTemplate}",
                };

                _logger.LogInformation($"InstanceService : Crreating Instance {name}");

                var result = instanceClient.Insert(insertInstanceRequest);

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);

                return false;
            }
        }

        public int GetInstanceGroupSize(string projectId, string zone, string instanceGroup)
        {
            List<string> instanceNames = GetInstanceGroupNames(projectId, zone, instanceGroup);

            if (instanceNames == null)
            {
                return 0;
            }

            return instanceNames.Count();
        }

        public List<string> GetInstanceGroupNames(string projectId, string zone, string instanceGroup)
        {
            List<string> result = new List<string>();

            try
            {
                // Create client.
                InstanceGroupsClient instanceGroupsClient = InstanceGroupsClient.Create();

                // Initialize request argument(s).
                InstanceGroupsListInstancesRequest instanceGroupsListInstancesRequest = new InstanceGroupsListInstancesRequest();
                PagedEnumerable<InstanceGroupsListInstances, InstanceWithNamedPorts> pagedInstances = instanceGroupsClient.ListInstances(projectId, zone, instanceGroup, instanceGroupsListInstancesRequest);
                foreach (var instance in pagedInstances)
                {
                    int pos = instance.Instance.LastIndexOf("/") + 1;
                    result.Add(instance.Instance.Substring(pos));
                }
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);

                return result;
            }
        }

        public bool DeleteInstance(string projectId, Instance instance, string zone)
        {
            try
            {
                // Create client
                InstancesClient instanceClient = InstancesClient.Create();

                // Create request
                DeleteInstanceRequest deleteInstanceRequest = new DeleteInstanceRequest()
                {
                    Project = projectId,
                    Zone = zone,
                    Instance = instance.Name                  
                };

                _logger.LogInformation($"InstanceService : Deleting Instance {instance.Name}");

                var result = instanceClient.Delete(deleteInstanceRequest);

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);

                return false;
            }

        }
    }
}
