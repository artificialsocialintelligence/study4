﻿
using Google.Api.Gax.ResourceNames;
using Google.Api;
using Google.Cloud.Monitoring.V3;
using Google.Protobuf.WellKnownTypes;
using Newtonsoft.Json.Linq;
using Microsoft.Extensions.Configuration;
using Google.Cloud.Compute.V1;
using ScalingMetricService.Models;
using DebounceThrottle;
using Google.Api.Gax;
using System.IO;

namespace ScalingMetricService.Services
{
    public class InstanceCreationTimer : IHostedService, IDisposable
    {
        private readonly ILogger<InstanceCreationTimer> _logger;
        private IConfiguration Configuration { get; set; }
        private ScalingMetricConfiguration ScalingMetricConfiguration { get; set; }
        private InstanceService InstanceService { get; set; }
        private Timer? JobTimer { get; set; }
        static public bool IsEnabled { get; set; } = false;
        static private bool InstanceCreationRunning { get; set; }
        static public int CurrentMetricValue { get; set; }
        static private DebounceDispatcher DebounceDispatcher { get; set; }
        static private Dictionary<string, InstanceState> InstanceStateDict { get; set; }

        public InstanceCreationTimer(ILogger<InstanceCreationTimer> logger, IConfiguration configuration, InstanceService instanceService)
        {
            _logger = logger;
            InstanceService = instanceService;
            JobTimer = null;
            InstanceCreationRunning = false;
            ScalingMetricConfiguration = new ScalingMetricConfiguration();
            configuration.GetSection("ScalingMetric").Bind(ScalingMetricConfiguration);
            CurrentMetricValue = 0;
            InstanceStateDict = new Dictionary<string, InstanceState>();

            DebounceDispatcher = new DebounceDispatcher(ScalingMetricConfiguration.InstanceDebounceTime * 1000);
        }

        public Task StartAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("InstanceCreationTimer: Timed Hosted Service running.");

            JobTimer = new Timer(InstanceCreationTimerAsync, null, TimeSpan.Zero, TimeSpan.FromSeconds(ScalingMetricConfiguration.InstanceCreationDelay));

            return Task.CompletedTask;
        }

        private void InstanceCreationTimerAsync(object? state)
        {
            if (!IsEnabled)
            {
                return;
            }
            try
            {
                if (InstanceCreationRunning)
                {
                    return;
                }

                InstanceCreationRunning = true;

                //_logger.LogInformation("InstanceCreationTimer : Started");

                // Get internal count of instances
                // Get the current number of instances in the instance group
                int numInstances = InstanceService.GetInstanceGroupSize(ScalingMetricConfiguration.ProjectId, ScalingMetricConfiguration.Zone, ScalingMetricConfiguration.InstanceGroup);

                // Add in the number of instances already starting
                numInstances += InstanceStateDict.Keys.Count();
                _logger.LogInformation($"InstanceCreationTimer Debounce: Current number of instances = {numInstances}, Current MetricValue = {CurrentMetricValue}, MetricValue = {ScalingMetricTimer.MetricValue}");

                // Check Scaling metric for changes
                if (CurrentMetricValue != ScalingMetricTimer.MetricValue)
                {
                    CurrentMetricValue = ScalingMetricTimer.MetricValue;

                    DebounceDispatcher.Debounce(() =>
                    {
                        _logger.LogInformation($"InstanceCreationTimer Debounce1 : Current MetricValue = {CurrentMetricValue}, MetricValue = {ScalingMetricTimer.MetricValue}");

                        CurrentMetricValue = ScalingMetricTimer.MetricValue;

                        _logger.LogInformation($"InstanceCreationTimer Debounce2 : Current MetricValue = {CurrentMetricValue}, MetricValue = {ScalingMetricTimer.MetricValue}");

                        _logger.LogInformation($"InstanceCreationTimer : Current Metric Value changed to: {CurrentMetricValue}");

                        // Start the requested number of instances if needed
                        if ((ScalingMetricTimer.MetricValue - (numInstances * ScalingMetricConfiguration.MetricValuePerInstance)) >= ScalingMetricConfiguration.MetricValuePerInstance) {
                            // Start a new instance
                            string instanceName = InstanceService.GenerateInstanceName(ScalingMetricConfiguration.InstanceGroup);
                            _logger.LogInformation($"InstanceCreationTimer : Creating instance: {instanceName}");

                            if (InstanceService.StartNewInstance(ScalingMetricConfiguration.ProjectId, instanceName, ScalingMetricConfiguration.InstanceTemplate, ScalingMetricConfiguration.Zone))
                            {
                                InstanceStateDict.Add(instanceName, InstanceState.Starting);

                                // Add one at a time
                                CurrentMetricValue = ((numInstances + 1) * ScalingMetricConfiguration.MetricValuePerInstance);
                            }
                        }
                    });
                }
                else
                {
                    CurrentMetricValue = (numInstances * ScalingMetricConfiguration.MetricValuePerInstance);
                }

                // Check Instance States
                foreach (var instanceName in InstanceStateDict.Keys.ToList())
                {
                    Instance? instanceInfo = InstanceService.GetInstanceInfo(ScalingMetricConfiguration.ProjectId, instanceName, ScalingMetricConfiguration.Zone);

                    if (instanceInfo != null)
                    {
                        if (InstanceStateDict[instanceName] == InstanceState.Starting)
                        {
                            if (instanceInfo.Status == "RUNNING")
                            {
                                // Add to Group
                                if (InstanceService.AddInstanceToGroup(ScalingMetricConfiguration.ProjectId, instanceInfo, ScalingMetricConfiguration.InstanceGroup, ScalingMetricConfiguration.Zone))
                                {
                                    InstanceStateDict[instanceName] = InstanceState.Grouping;
                                }
                            }
                        }
                        else if (InstanceStateDict[instanceName] == InstanceState.Grouping)
                        {
                            // Check if now in instance group
                            List<string> groupInstanceNames = InstanceService.GetInstanceGroupNames(ScalingMetricConfiguration.ProjectId, ScalingMetricConfiguration.Zone, ScalingMetricConfiguration.InstanceGroup);
                            if (groupInstanceNames.Contains(instanceName))
                            {
                                InstanceStateDict.Remove(instanceName);
                            }
                        }
                    }
                }

                ShowInternalInstanceTable();

                //_logger.LogInformation("InstanceCreationTimer : Finished");

                InstanceCreationRunning = false;
            }
            catch (Exception ex)
            {
                var message = $"Message : {ex.Message}. Inner Message : {ex.InnerException?.Message}";
                _logger.LogCritical(message);

                InstanceCreationRunning = false;
            }
        }

        private void ShowInternalInstanceTable()
        {
            if (InstanceStateDict.Keys.Count <= 0)
            {
                return;
            }

            _logger.LogInformation("Internal Instance Table");
            _logger.LogInformation("=======================");

            foreach (var instanceName in InstanceStateDict.Keys.ToList())
            {
                _logger.LogInformation($"{instanceName}\t\t{InstanceStateDict[instanceName].ToString()}");
            }
        }


        public Task StopAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("InstanceCreationTimer: Timed Hosted Service is stopping.");

            JobTimer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            JobTimer?.Dispose();
        }

    }
}

