using ScalingMetricService.Models;
using ScalingMetricService.Services;

namespace ScalingMetricService
{
    class Program
    {
        static void Main(string[] args)
        {
            ScalingMetricConfiguration scalingMetricConfiguration = new ScalingMetricConfiguration();

            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddControllers();
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();
            builder.Services.AddHostedService<ScalingMetricTimer>();
            builder.Services.AddHostedService<InstanceCreationTimer>();
            builder.Services.AddSingleton<InstanceService>();
            builder.Services.AddSingleton<IMQTTService, MQTTService>();

            var app = builder.Build();

            var loggerFactory = app.Services.GetService<ILoggerFactory>();
            if (loggerFactory != null)
            {
                loggerFactory.AddFile(builder.Configuration.GetSection("Logging").GetSection("File"));
            }

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();

            app.MapControllers();

            builder.Configuration.GetSection("ScalingMetric").Bind(scalingMetricConfiguration);
            Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", scalingMetricConfiguration.GoogleCredentialsPath);

            // Instantiate services
            app.Services.GetService<IMQTTService>();

            ScalingMetricTimer.IsEnabled = true;
            InstanceCreationTimer.IsEnabled = true;

            app.Run();
        }
    }
}
