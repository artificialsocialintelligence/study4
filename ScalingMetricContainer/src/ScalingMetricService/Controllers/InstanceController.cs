﻿using Google.Api;
using Google.Api.Gax.ResourceNames;
using Google.Cloud.Compute.V1;
using Google.Cloud.Monitoring.V3;
using Google.Protobuf.WellKnownTypes;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using ScalingMetricService.Models;
using ScalingMetricService.Services;
using System.Net;

namespace ScalingMetricService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class InstanceController : ControllerBase
    {
        private readonly ILogger<InstanceController> _logger;
        private IConfiguration Configuration { get; set; }
        private InstanceService InstanceService { get; set; }
        private ScalingMetricConfiguration ScalingMetricConfiguration { get; set; }
        private IMQTTService MQTTService { get; set; }

        public InstanceController(ILogger<InstanceController> logger, IConfiguration configuration, InstanceService instanceService, IMQTTService mqttService)
        {
            _logger = logger;
            InstanceService = instanceService;
            MQTTService = mqttService;

            Configuration = configuration;
            ScalingMetricConfiguration = new ScalingMetricConfiguration();
            configuration.GetSection("ScalingMetric").Bind(ScalingMetricConfiguration);
        }

        [Produces(typeof(string))]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(string))]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [HttpGet("GetInternalIP")]
        public IActionResult Get([FromQuery] string ip)
        {
            _logger.LogInformation($"GetInternalIP: Request for internal ip address of: {ip}");

            List<string> instanceNames = InstanceService.GetInstanceGroupNames(ScalingMetricConfiguration.ProjectId, ScalingMetricConfiguration.Zone, ScalingMetricConfiguration.InstanceGroup);

            if (instanceNames != null)
            {
                foreach (string instanceName in instanceNames)
                {
                    Instance? instance = InstanceService.GetInstanceInfo(ScalingMetricConfiguration.ProjectId, instanceName, ScalingMetricConfiguration.Zone);

                    if (instance != null)
                    {
                        NetworkInterface? network = instance.NetworkInterfaces.Where(i => i.AccessConfigs.Where(a => a.NatIP == ip).FirstOrDefault() != null).FirstOrDefault();

                        if (network != null && network.HasNetworkIP)
                        {
                            // Return instance info
                            return Ok(network.NetworkIP);
                        }
                    }
                }
            }
            return NotFound();
        }

        [HttpDelete()]
        public IActionResult Deletet([FromQuery] string ip)
        {
            _logger.LogInformation($"Instance Delete: Request to remove instance: {ip}");

            // Log instance state to file
            MQTTService.MonitoringFileLogTask(null);

            List<string> instanceNames = InstanceService.GetInstanceGroupNames(ScalingMetricConfiguration.ProjectId, ScalingMetricConfiguration.Zone, ScalingMetricConfiguration.InstanceGroup);

            if (instanceNames != null)
            {
                foreach (string instanceName in instanceNames)
                {
                    Instance? instance = InstanceService.GetInstanceInfo(ScalingMetricConfiguration.ProjectId, instanceName, ScalingMetricConfiguration.Zone);

                    if (instance != null)
                    {
                        NetworkInterface? network = instance.NetworkInterfaces.Where(i => i.AccessConfigs.Where(a => a.NatIP == ip).FirstOrDefault() != null).FirstOrDefault();

                        if (network != null)
                        {
                            // Delete instance
                            InstanceService.DeleteInstance(ScalingMetricConfiguration.ProjectId, instance, ScalingMetricConfiguration.Zone);
                        }
                    }
                }
            }
            return Ok();
        }

    }
}