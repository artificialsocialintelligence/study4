﻿
using AsistControl.DTO;
using AsistControl.Interfaces;
using MQTTnet;
using System.Text.Json;
using System.Text;
using System.Text.Json.Nodes;
using Microsoft.AspNetCore.Mvc;
using System;
using AsistControl.DTO.DBModels;

namespace AsistControl
{
    public static class MQTTProcessor
    {   
        public static int restartCount = 0;

        public static long enterShopTimestamp = -1000;

        async public static void process(IConfiguration config, MQTT_Client mqtt, MqttApplicationMessageReceivedEventArgs eventArgs, IExperiment experiment){

            NonAPIExperimentControl nonAPIExperimentControl = new NonAPIExperimentControl(config, mqtt, experiment);

            try
            {
                switch (eventArgs.ApplicationMessage.Topic)
                {

                    case "control/request/getTrialInfo":

                        try
                        {

                            Console.WriteLine(" MqttProcessor getTrialInfo ... ");

                            Console.WriteLine(" PROCESSING getTrialInfo request ...");

                            TrialInfo trialInfoObject = new TrialInfo(experiment);

                            string trialInfoString = trialInfoObject.convertToJsonString();

                            mqtt.publish(trialInfoString, "control/response/getTrialInfo");

                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.StackTrace);
                        }

                        break;

                    case "control/status/trigger_trial_stop":

                        Console.WriteLine(" MqttProcessor trigger Trial Stop ... ");

                        
                        string objString = Encoding.UTF8.GetString(eventArgs.ApplicationMessage.Payload, 0, eventArgs.ApplicationMessage.Payload.Length);

                        Console.WriteLine(objString);

                        var jsonEl = JsonSerializer.Deserialize<JsonElement>(objString);

                        string reason = jsonEl.GetProperty("reason").GetString();

                        Console.WriteLine(reason);

                        bool continue_with_team = jsonEl.GetProperty("continue_with_team").GetBoolean();

                        Console.WriteLine("Continue With Team : " + continue_with_team);

                        string endCond = null ;
                       
                        if (reason.CompareTo("DISSOLVE_TEAM_MINECRAFT_DISCONNECT") == 0)
                        {
                            endCond = Experiment.TrialEndCondition.ERROR_MINECRAFT_DISCONNECT.ToString();
                        }
                        else if (reason.CompareTo("DISSOLVE_TEAM_CLIENTMAP_DISCONNECT") == 0)
                        {
                            endCond = Experiment.TrialEndCondition.ERROR_CLIENTMAP_DISCONNECT.ToString();
                        }
                        else if (reason.CompareTo("DISSOLVE_TEAM_PLAYER_LEFT") == 0)
                        {
                            endCond = Experiment.TrialEndCondition.ERROR_PLAYER_LEFT.ToString();
                        }                        

                        if (endCond != null)
                        {

                            experiment.trialDTO.data.trial_end_condition = endCond;
                            experiment.trialSummaryDB.TrialEndCondition = endCond;

                            Console.WriteLine("experiment.trialDTO.data.trial_end_condition : " + experiment.trialDTO.data.trial_end_condition);
                            Console.WriteLine("experiment.trialSummaryDB.TrialEndCondition : " + experiment.trialSummaryDB.TrialEndCondition);
                            Console.WriteLine("End Condition : " + endCond);

                        }

                        await stopTrialAndMinecraftAndUpdateDB(config, mqtt, eventArgs, experiment, nonAPIExperimentControl);

                        if (continue_with_team) {

                            // Start a New Trial
                            Console.WriteLine("Running nonAPIExperimentControl.startTrial Task");
                            await nonAPIExperimentControl.startTrial(experiment.playerArrayToStartTrial);

                            // Start Minecraft Server
                            Console.WriteLine("Running nonAPIExperimentControl.startMission Task");
                            nonAPIExperimentControl.startMission(experiment.missionDTO);
                        }
                        
                        break;

                    case "status/minecraft/block_loading":
                        Console.WriteLine(" MqttProcessor block loading ... ");

                        string blockLoadingString = Encoding.UTF8.GetString(eventArgs.ApplicationMessage.Payload, 0, eventArgs.ApplicationMessage.Payload.Length);

                        var blockLoadingObject = JsonSerializer.Deserialize<JsonElement>(blockLoadingString);

                        bool error = blockLoadingObject.GetProperty("error").GetBoolean();

                        if (error)
                        {
                            if (restartCount <= 5)
                            {
                                restartCount++;

                                Console.WriteLine("There was a loading error in the Minecraft Server");

                                Console.WriteLine(" MINECRAFT SERVER HAS STOPPED WITH AN ERROR, ATTEMPTNG TO RESTART. RESTART COUNT : " + restartCount.ToString() + "/5 ");

                                // Stop the Server and throw out the map                            

                                await nonAPIExperimentControl.StopServer();

                                // Start Mission with saved DTO
                                nonAPIExperimentControl.startMission(experiment.missionDTO);
                            }
                            else
                            {

                                Console.WriteLine("There was a loading error in the Minecraft Server");

                                Console.WriteLine(" MINECRAFT SERVER HAS FAILED TO LOAD 5 TIMES ... SOMETHING IS WRONG. ");

                                nonAPIExperimentControl.StopServer();

                            }
                        }

                        break;

                    case "control/bounce":
                        Console.WriteLine(" MqttProcessor control/bounce ... ");
                        Console.WriteLine("--------------------------------------Initiating Mock Bounce-----------------------------------------------------------");
                        string args = "";
                        string agent = experiment.experimentDTO.data.condition;
                        string overrideAgent = Environment.GetEnvironmentVariable("asi");

                        if (overrideAgent == null || (overrideAgent.CompareTo(String.Empty) == 0))
                        {
                            args += "ra";
                        }
                        else {
                            // SET ARGS THAT WOULD NORMALLY BE PASSED TO TESTBED UP
                            if (String.Compare(agent, "ASI_DOLL_TA1_RITA") == 0)
                            {
                                args += "a";
                            }
                            else if (String.Compare(agent, "ASI_CMU_TA1_ATLAS") == 0)
                            {
                                args += "r";
                            }
                            else if (String.Compare(agent, "NO_ADVISOR") == 0)
                            {
                                args += "ra";
                            }
                            Console.WriteLine("Bouncing Agent : " + agent);
                        }

                        nonAPIExperimentControl.MockBounce(args);

                        Console.WriteLine("--------------------------------------Mock Bounce Complete-----------------------------------------------------------");

                        mqtt.publish("{}", "control/bounce/complete");
                        break;

                    case "status/minecraft/stopped":

                        Console.WriteLine(" MqttProcessor status/minecraft/stopped ... ");

                        string serverStopped = Encoding.UTF8.GetString(eventArgs.ApplicationMessage.Payload, 0, eventArgs.ApplicationMessage.Payload.Length);

                        var serverStoppedObject = JsonSerializer.Deserialize<JsonElement>(serverStopped);

                        bool crash = serverStoppedObject.GetProperty("crash").GetBoolean();

                        if (crash)
                        {
                            // We should wait for export/auto to end before we do this ... that is why it increments wrong in the export fileneame
                            // This happens before export process grabs the

                            Console.WriteLine("Minecraft Server crashed unexpectedly. Attempting to restart the Server.");

                            Console.WriteLine("Running stopTrialAndMinecraftAndUpdateDB Task with invalid trial flag");
                            await stopTrialAndMinecraftAndUpdateDB(config, mqtt, eventArgs, experiment, nonAPIExperimentControl);

                            // Start a New Trial
                            Console.WriteLine("Running nonAPIExperimentControl.startTrial Task");
                            await nonAPIExperimentControl.startTrial(experiment.playerArrayToStartTrial);

                            // Start Minecraft Server
                            Console.WriteLine("Running nonAPIExperimentControl.startMission Task");
                            nonAPIExperimentControl.startMission(experiment.missionDTO);
                        }
                        else
                        {
                            Console.WriteLine("Server stopped in a non-crash way. Everything is OK.");
                        }
                        break;

                    // AGGRAGATING TRIAL DATA HERE
                    case "observations/events/mission":

                        Console.WriteLine(" MqttProcessor Mission State ... ");
                        string missionString = Encoding.UTF8.GetString(eventArgs.ApplicationMessage.Payload, 0, eventArgs.ApplicationMessage.Payload.Length);

                        JsonObject missionObject = JsonSerializer.Deserialize<JsonObject>(missionString);

                        string missionStateOutcome = (string)missionObject!["data"]!["state_change_outcome"]!;

                        string endCondition = "MISSION_START_NOT_SET";
                        /*
                         
                        MISSION_RUNNING_OK,
		                MISSION_STOP_TIMER_END,
		                MISSION_STOP_ALL_PLAYERS_FROZEN, 
		                MISSION_STOP_ALL_BOMBS_REMOVED, 
		                MISSION_STOP_MINECRAFT_DISCONNECT,
		                MISSION_STOP_SERVER_CRASHED,
		                MISSION_STOP_CLIENTMAP_DISCONNECT,
		                MISSION_STOP_PLAYER_LEFT
                        TEAM_DISSOLVE_PROCEED_TIMEOUT
                         
                         */

                        if (missionStateOutcome.CompareTo("MISSION_RUNNING_OK") == 0)
                        {
                            endCondition = Experiment.TrialEndCondition.NOT_SET.ToString();
                        }
                        else if (missionStateOutcome.CompareTo("MISSION_STOP_TIMER_END") == 0)
                        {
                            endCondition = Experiment.TrialEndCondition.OK_TIMER_END.ToString();
                        }
                        else if (missionStateOutcome.CompareTo("MISSION_STOP_ALL_PLAYERS_FROZEN") == 0)
                        {
                            endCondition = Experiment.TrialEndCondition.OK_ALL_PLAYERS_FROZEN.ToString();
                        }
                        else if (missionStateOutcome.CompareTo("MISSION_STOP_ALL_BOMBS_REMOVED") == 0)
                        {
                            endCondition = Experiment.TrialEndCondition.OK_ALL_BOMBS_REMOVED.ToString();
                        }
                        else if (missionStateOutcome.CompareTo("MISSION_STOP_MINECRAFT_DISCONNECT") == 0)
                        {
                            endCondition = Experiment.TrialEndCondition.ERROR_MINECRAFT_DISCONNECT.ToString();
                        }
                        else if (missionStateOutcome.CompareTo("MISSION_STOP_CLIENTMAP_DISCONNECT") == 0)
                        {
                            endCondition = Experiment.TrialEndCondition.ERROR_CLIENTMAP_DISCONNECT.ToString();
                        }
                        else if (missionStateOutcome.CompareTo("MISSION_STOP_PLAYER_LEFT") == 0)
                        {
                            endCondition = Experiment.TrialEndCondition.ERROR_PLAYER_LEFT.ToString();
                        }
                        else if (missionStateOutcome.CompareTo("MISSION_STOP_SERVER_CRASHED") == 0)
                        {
                            endCondition = Experiment.TrialEndCondition.ERROR_MINECRAFT_CRASH.ToString();
                        }
                        else if (missionStateOutcome.CompareTo("DISSOLVE_TEAM_PROCEED_TIMEOUT") == 0)
                        {
                            endCondition = Experiment.TrialEndCondition.ERROR_PROCEED_TIMEOUT.ToString();
                        }
                        else
                        {

                            Console.WriteLine(" MISSION STATE OUTCOME WEIRD STATE : " + missionStateOutcome);
                                                       
                        }

                        experiment.trialDTO.data.trial_end_condition = endCondition;
                        experiment.trialSummaryDB.LastActiveMissionTime = (string)missionObject!["data"]!["mission_timer"]!;
                        experiment.trialSummaryDB.MissionEndCondition = missionStateOutcome;
                        experiment.trialSummaryDB.TrialEndCondition = endCondition;

                        Console.WriteLine("experiment.trialDTO.data.trial_end_condition : " + experiment.trialDTO.data.trial_end_condition);
                        Console.WriteLine("experiment.trialSummaryDB.TrialEndCondition : " + experiment.trialSummaryDB.TrialEndCondition);
                        Console.WriteLine("End Condition : " + endCondition);

                        break;

                    case "observations/events/stage_transition":

                        Console.WriteLine(" MqttProcessor StageTransition ... ");

                        string stageTransitionString = Encoding.UTF8.GetString(eventArgs.ApplicationMessage.Payload, 0, eventArgs.ApplicationMessage.Payload.Length);

                        JsonObject stageTransitionObject = JsonSerializer.Deserialize<JsonObject>(stageTransitionString);

                        string missionStageString = (string)stageTransitionObject!["data"]!["mission_stage"]!;

                        long globalElapsedMilli = (long)stageTransitionObject!["data"]!["elapsed_milliseconds"]!;

                        if (missionStageString.ToLower().CompareTo("shop_stage") == 0)
                        {
                            experiment.trialSummaryDB.NumStoreVisits++;
                            enterShopTimestamp = globalElapsedMilli!;
                        }
                        else
                        {
                            // includes recon
                            experiment.trialSummaryDB.NumFieldVisits++;

                            if (enterShopTimestamp > 0)
                            {

                                long deltaTime = globalElapsedMilli - enterShopTimestamp;

                                experiment.trialSummaryDB.TotalStoreTime += deltaTime;

                                enterShopTimestamp = -1000;

                                Console.WriteLine("TotalStoreTime : " + experiment.trialSummaryDB.TotalStoreTime);

                            }
                        }

                        break;

                    case "groundtruth/environment/created/list":

                        Console.WriteLine(" MqttProcessor Environment Created List ... ");

                        string messageString = Encoding.UTF8.GetString(eventArgs.ApplicationMessage.Payload, 0, eventArgs.ApplicationMessage.Payload.Length);

                        JsonObject messageObject = JsonSerializer.Deserialize<JsonObject>(messageString);

                        JsonArray array = messageObject!["data"]!["list"]!.AsArray();
                        
                        experiment.trialSummaryDB.BombsTotal = array.Count;

                        Console.WriteLine("BombsTotal : " + experiment.trialSummaryDB.BombsTotal);

                        break;

                    case "ui/click":

                        Console.WriteLine(" MqttProcessor UI Click ... ");

                        string clickString = Encoding.UTF8.GetString(eventArgs.ApplicationMessage.Payload, 0, eventArgs.ApplicationMessage.Payload.Length);                                                

                        JsonObject clickObject = JsonSerializer.Deserialize<JsonObject>(clickString);                        

                        string metaActionString = (string)clickObject!["data"]!["additional_info"]["meta_action"]!;                        

                        string actorPid = (string)clickObject!["data"]!["participant_id"]!;                        

                        if (metaActionString != null && actorPid != null)
                        {                            

                            if (metaActionString.ToLower().CompareTo("planning_flag_placed") == 0)
                            {                                

                                experiment.trialSummaryDB.FlagsPlaced =
                                    JsonDocument.Parse(
                                        JsonSerializer.Serialize(
                                            aggregateActionOverTeamObject(
                                                experiment.trialSummaryDB.FlagsPlaced,
                                                actorPid,
                                                1
                                            )
                                        )
                                    );
                                
                            }
                            else if (metaActionString.ToLower().Contains("propose_tool_purchase"))
                            {

                                int addAmount = (int)clickObject!["data"]!["additional_info"]["cost"]!;
                                
                                if (metaActionString.Contains("+")) { addAmount*= 1; }
                                else if (metaActionString.Contains("-")) { addAmount*= -1; }
                                experiment.trialSummaryDB.BudgetExpended =
                                    JsonDocument.Parse(
                                        JsonSerializer.Serialize(
                                            aggregateActionOverTeamObject(
                                                experiment.trialSummaryDB.BudgetExpended,
                                                actorPid,
                                                addAmount
                                            )
                                        )
                                    );
                            };

                        } 
                        break;
                    case "environment/removed/single":

                        Console.WriteLine(" Environment Created Single ... ");

                        string envRemovedSingleString = Encoding.UTF8.GetString(eventArgs.ApplicationMessage.Payload, 0, eventArgs.ApplicationMessage.Payload.Length);

                        JsonObject envRemovedSingleObject = JsonSerializer.Deserialize<JsonObject>(envRemovedSingleString);

                        string removersPid = (string)envRemovedSingleObject!["data"]!["triggering_entity"]!;

                        string blockType = (string)envRemovedSingleObject!["data"]!["obj"]!["type"]!;

                        Console.WriteLine(blockType);
                        if (removersPid != null && blockType != null) {

                            if (blockType.ToLower().Contains("block_fire_custom"))
                            {
                                Console.WriteLine(" removing fire ...  ");
                                if (!removersPid.ToLower().Contains("server")) {

                                    JsonObject updatedObject = aggregateActionOverTeamObject(
                                            experiment.trialSummaryDB.FiresExtinguished,
                                            removersPid,
                                            1
                                        );
                                    experiment.trialSummaryDB.FiresExtinguished =
                                        JsonDocument.Parse(
                                            JsonSerializer.Serialize(
                                                updatedObject
                                            )
                                        );
                                }
                            }
                        }  

                        break;

                    case "environment/created/single":

                        Console.WriteLine(" Environment Created Single ... ");

                        string envCreatedSingleString = Encoding.UTF8.GetString(eventArgs.ApplicationMessage.Payload, 0, eventArgs.ApplicationMessage.Payload.Length);

                        JsonObject envCreatedSingleObject = JsonSerializer.Deserialize<JsonObject>(envCreatedSingleString);

                        string creatingPid = (string)envCreatedSingleObject!["data"]!["triggering_entity"]!;

                        // START BEACONS
                        string beaconType = (string)envCreatedSingleObject!["data"]!["obj"]!["type"]!;

                        Console.WriteLine(creatingPid + " : " + beaconType);

                        if (creatingPid != null && beaconType != null)
                        {
                            if (beaconType.ToLower().Contains("hazard"))
                            {
                                JsonObject updatedObject = aggregateActionOverTeamObject(
                                            experiment.trialSummaryDB.CommBeaconsPlaced,
                                            creatingPid,
                                            1
                                        );
                                experiment.trialSummaryDB.CommBeaconsPlaced =
                                JsonDocument.Parse(
                                    JsonSerializer.Serialize(
                                        updatedObject
                                    )
                                );

                                Console.WriteLine("teamActionObject After increment:");
                                Console.WriteLine(JsonSerializer.Serialize(updatedObject));
                                Console.WriteLine(JsonSerializer.Serialize(experiment.trialSummaryDB));

                            }
                            else if (beaconType.ToLower().Contains("bomb"))
                            {
                                JsonObject updatedObject = aggregateActionOverTeamObject(
                                            experiment.trialSummaryDB.BombBeaconsPlaced,
                                            creatingPid,
                                            1
                                        );
                                experiment.trialSummaryDB.BombBeaconsPlaced =
                                JsonDocument.Parse(
                                    JsonSerializer.Serialize(
                                        updatedObject
                                    )
                                );

                                Console.WriteLine("teamActionObject After increment:");
                                Console.WriteLine(JsonSerializer.Serialize(updatedObject));
                                
                                Console.WriteLine(JsonSerializer.Serialize(experiment.trialSummaryDB));
                                
                            }

                            Console.WriteLine(JsonSerializer.Serialize(experiment.trialSummaryDB));
                        }
                        // END BEACONS


                        break;

                    case "communication/chat":

                        Console.WriteLine(" Communication Chat Received ... ");

                        string chatModelString = Encoding.UTF8.GetString(eventArgs.ApplicationMessage.Payload, 0, eventArgs.ApplicationMessage.Payload.Length);
                        string senderPid = JsonSerializer.Deserialize<JsonObject>(chatModelString)["data"]["sender_id"].GetValue<string>();

                        JsonObject chatObject = aggregateActionOverTeamObject(
                                            experiment.trialSummaryDB.TextChatsSent,
                                            senderPid,
                                            1
                                        );
                        experiment.trialSummaryDB.TextChatsSent =
                        JsonDocument.Parse(
                            JsonSerializer.Serialize(
                                chatObject
                            )
                        );

                        Console.WriteLine("teamActionObject After increment:");
                        Console.WriteLine(JsonSerializer.Serialize(chatObject));
                        Console.WriteLine(JsonSerializer.Serialize(experiment.trialSummaryDB));

                        break;

                    case "player/survey/response":

                        Console.WriteLine(" Player Survey Response Received ... ");

                        string surveyModelString = Encoding.UTF8.GetString(eventArgs.ApplicationMessage.Payload, 0, eventArgs.ApplicationMessage.Payload.Length);
                        string surveyPid = JsonSerializer.Deserialize<JsonObject>(surveyModelString)["data"]["participant_id"].GetValue<string>();

                        JsonObject surveyObject = aggregateActionOverTeamObject(
                                            experiment.trialSummaryDB.NumCompletePostSurveys,
                                            surveyPid,
                                            1
                                        );
                        experiment.trialSummaryDB.NumCompletePostSurveys =
                        JsonDocument.Parse(
                            JsonSerializer.Serialize(
                                surveyObject
                            )
                        );

                        Console.WriteLine("teamActionObject After increment:");
                        Console.WriteLine(JsonSerializer.Serialize(surveyObject));
                        Console.WriteLine(JsonSerializer.Serialize(experiment.trialSummaryDB));

                        break;
                    case "score/change":

                        Console.WriteLine(" MqttProcessor Score Change ... ");

                        string scoreChangeString = Encoding.UTF8.GetString(eventArgs.ApplicationMessage.Payload, 0, eventArgs.ApplicationMessage.Payload.Length);
                        experiment.trialSummaryDB.TeamScore = JsonSerializer.Deserialize<JsonObject>(scoreChangeString)["data"]["teamScore"].GetValue<Int32>();
                        break;

                    // TRACKS BOMBS DEFUSED OR EXPLODED
                    case "player/state/change":

                        Console.WriteLine(" MqttProcessor Player State Change ... ");

                        string playerStateChangeString = Encoding.UTF8.GetString(eventArgs.ApplicationMessage.Payload, 0, eventArgs.ApplicationMessage.Payload.Length);

                        JsonObject playerStateChangeObject = JsonSerializer.Deserialize<JsonObject>(playerStateChangeString);

                        string playerPid = (string)playerStateChangeObject!["data"]!["participant_id"]!;

                        JsonObject changedAttributes = playerStateChangeObject!["data"]!["changedAttributes"].AsObject();

                        if (changedAttributes != null && changedAttributes.ContainsKey("is_frozen")) {

                            JsonArray frozenDeltaArray = playerStateChangeObject!["data"]!["changedAttributes"]!["is_frozen"]!.AsArray();

                            // Calculate Teammates Rescued
                            if (frozenDeltaArray != null && frozenDeltaArray.Count == 2)
                            {
                                Console.WriteLine(" Frozen State Change.");
                                bool oldState = bool.Parse((string)frozenDeltaArray[0]!);
                                bool newState = bool.Parse((string)frozenDeltaArray[1]!);

                                // teammate rescued
                                if (newState)
                                {
                                    experiment.trialSummaryDB.TimesFrozen =
                                        JsonDocument.Parse(
                                            JsonSerializer.Serialize(
                                                aggregateActionOverTeamObject(
                                                    experiment.trialSummaryDB.TimesFrozen,
                                                    playerPid,
                                                    1
                                                )
                                            )
                                        );
                                }
                                else
                                {
                                    string rescuerPid = (string)playerStateChangeObject!["data"]!["source_id"]!;
                                    experiment.trialSummaryDB.TeammatesRescued =
                                        JsonDocument.Parse(
                                            JsonSerializer.Serialize(
                                                aggregateActionOverTeamObject(
                                                    experiment.trialSummaryDB.TeammatesRescued,
                                                    rescuerPid,
                                                    1
                                                )
                                            )
                                        );
                                }
                            }

                        }
                        if (changedAttributes != null && changedAttributes.ContainsKey("health"))
                        {                            
                            JsonArray healthDeltaArray = playerStateChangeObject!["data"]!["changedAttributes"]!["health"]!.AsArray();

                            // Calculate DamgeTaken per player for TrialRecord Aggregation
                            if (healthDeltaArray != null && healthDeltaArray.Count == 2)
                            {
                                Console.WriteLine(" Health State Change.");
                                float deltaHealth = float.Parse((string)healthDeltaArray[0]) - float.Parse((string)healthDeltaArray[1]);

                                if (deltaHealth != float.NaN && deltaHealth > 0)
                                {
                                    experiment.trialSummaryDB.DamageTaken =
                                        JsonDocument.Parse(
                                            JsonSerializer.Serialize(
                                                aggregateActionOverTeamObject(
                                                    experiment.trialSummaryDB.DamageTaken,
                                                    playerPid,
                                                    deltaHealth
                                                )
                                            )
                                        );
                                    
                                }
                            }
                        }                        

                        break;

                    // TRACKS BOMBS DEFUSED OR EXPLODED
                    case "object/state/change":

                        Console.WriteLine(" MqttProcessor Object State Change ... ");

                        string objectStateChangeString = Encoding.UTF8.GetString(eventArgs.ApplicationMessage.Payload, 0, eventArgs.ApplicationMessage.Payload.Length);

                        JsonObject objectStateChangeObject = JsonSerializer.Deserialize<JsonObject>(objectStateChangeString);
                        
                        string outcome = (string)objectStateChangeObject!["data"]!["currAttributes"]!["outcome"]!;
                        
                        string pid = (string)objectStateChangeObject!["data"]!["triggering_entity"]!;
                        
                        string bombType = (string)objectStateChangeObject!["data"]!["type"]!;
                        
                        if (!pid.ToLower().Contains("server"))
                        {
                            JsonObject bombSummaryPlayer = createBombSummaryPlayerObject(experiment, pid); 

                            JsonObject bombsExploded = JsonSerializer.Deserialize<JsonObject>(JsonSerializer.Serialize(experiment.trialSummaryDB.BombsExploded));
                            if (bombsExploded == null)
                            {
                                bombsExploded = new JsonObject()
                                {
                                    ["EXPLODE_TOOL_MISMATCH"] = 0,
                                    ["EXPLODE_TIME_LIMIT"] = 0,
                                    ["EXPLODE_CHAINED_ERROR"] = 0,
                                    ["EXPLODE_FIRE"] = 0,
                                    ["TOTAL_EXPLODED"] = 0
                                };
                                Console.WriteLine("5");
                            }

                            if (outcome.CompareTo("TRIGGERED_ADVANCE_SEQ") == 0)
                            {
                                
                                JsonArray phaseDeltaList = objectStateChangeObject!["data"]!["changedAttributes"]!["sequence"]!.AsArray();
                                
                                string color = phaseDeltaList[0].GetValue<string>()[0].ToString();                                
                                
                                if (bombType.ToLower().Contains("standard"))
                                {
                                    
                                    bombSummaryPlayer[pid]["PHASES_DEFUSED"]["STANDARD"][color] = bombSummaryPlayer[pid]["PHASES_DEFUSED"]["STANDARD"][color].GetValue<int>() + 1;
                                }
                                else if (bombType.ToLower().Contains("chained"))
                                {
                                    
                                    bombSummaryPlayer[pid]["PHASES_DEFUSED"]["CHAINED"][color] = bombSummaryPlayer[pid]["PHASES_DEFUSED"]["CHAINED"][color].GetValue<int>() + 1;
                                }
                                else if (bombType.ToLower().Contains("fire"))
                                {
                                    bombSummaryPlayer[pid]["PHASES_DEFUSED"]["FIRE"][color] = bombSummaryPlayer[pid]["PHASES_DEFUSED"]["CHAINED"][color].GetValue<int>() + 1;
                                }
                            }
                            else if (outcome.CompareTo("DEFUSED") == 0)
                            {                                
                                incrementBombTypeBombSummary(bombSummaryPlayer, "BOMBS_DEFUSED", bombType, pid);

                            }
                            else if (outcome.CompareTo("EXPLODE_TOOL_MISMATCH") == 0)
                            {                                
                                incrementBombTypeBombSummary(bombSummaryPlayer, "BOMBS_EXPLODED", bombType, pid);

                                int explodedToolMismatch = (int)bombsExploded["EXPLODE_TOOL_MISMATCH"];
                                bombsExploded["EXPLODE_TOOL_MISMATCH"] = explodedToolMismatch + 1;
                                int total = (int)bombsExploded["TOTAL_EXPLODED"];
                                bombsExploded["TOTAL_EXPLODED"] = total + 1;


                            }
                            else if (outcome.CompareTo("EXPLODE_TIME_LIMIT") == 0)
                            {                                
                                bool timeLimitExists = bombsExploded!.ContainsKey("EXPLODE_TIME_LIMIT");
                                if (!timeLimitExists)
                                {
                                    bombsExploded.Add("EXPLODE_TIME_LIMIT", 0);
                                };
                                int explodeTimeLimit = (int)bombsExploded["EXPLODE_TIME_LIMIT"];
                                bombsExploded["EXPLODE_TIME_LIMIT"] = explodeTimeLimit + 1;
                                int total = (int)bombsExploded["TOTAL_EXPLODED"];
                                bombsExploded["TOTAL_EXPLODED"] = total + 1;
                            }
                            else if (outcome.CompareTo("EXPLODE_FIRE") == 0)
                            {
                                
                                bool fireExists = bombsExploded!.ContainsKey("EXPLODE_FIRE");
                                if (!fireExists)
                                {
                                    bombsExploded.Add("EXPLODE_FIRE", 0);
                                };
                                int explodeFire = (int)bombsExploded["EXPLODE_FIRE"];
                                bombsExploded["EXPLODE_FIRE"] = explodeFire + 1;
                                int total = (int)bombsExploded["TOTAL_EXPLODED"];
                                bombsExploded["TOTAL_EXPLODED"] = total + 1;
                            }
                            else if (outcome.CompareTo("EXPLODE_CHAINED_ERROR") == 0)
                            {
                                
                                incrementBombTypeBombSummary(bombSummaryPlayer, "BOMBS_EXPLODED", bombType, pid);

                                bool chainedErrorExists = bombsExploded!.ContainsKey("EXPLODE_CHAINED_ERROR");
                                if (!chainedErrorExists)
                                {
                                    bombsExploded.Add("EXPLODE_CHAINED_ERROR", 0);
                                };
                                int explodeChainedError = (int)bombsExploded["EXPLODE_CHAINED_ERROR"];
                                bombsExploded["EXPLODE_CHAINED_ERROR"] = explodeChainedError + 1;
                                int total = (int)bombsExploded["TOTAL_EXPLODED"];
                                bombsExploded["TOTAL_EXPLODED"] = total + 1;
                            }

                            if (outcome != null)
                            { 
                                experiment.trialSummaryDB.BombsExploded = JsonDocument.Parse(JsonSerializer.Serialize(bombsExploded));
                                experiment.trialSummaryDB.BombSummaryPlayer = JsonDocument.Parse(JsonSerializer.Serialize(bombSummaryPlayer));
                                Console.WriteLine(JsonSerializer.Serialize(experiment.trialSummaryDB));
                            }
                        }                       
                   
                        break;

                    default:

                        break;
                }
            }catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        
        }
        async public static Task<IActionResult> stopTrialAndMinecraftAndUpdateDB(

            IConfiguration config, MQTT_Client mqtt, MqttApplicationMessageReceivedEventArgs eventArgs,
            IExperiment experiment, NonAPIExperimentControl nonAPIExperimentControl)
        {
            // UPDATE DB
            string trialUpdateReturned = await NonAPIExperimentControl.updateOrCreateTrialRecord(experiment, false);
            Console.WriteLine(trialUpdateReturned);

            try
            {
                // Serialize the returned Trial Summary and package it as the data part of standard asist json message, then publush below
                mqtt.publish(new TrialSummaryMQTT(experiment, trialUpdateReturned).convertToJsonString(), "control/trial/summary");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            string teamUpdateReturned = await NonAPIExperimentControl.updateOrCreateTeamRecord(experiment);
            Console.WriteLine(teamUpdateReturned);

            // TRIAL STOP
            TrialDTO dto = experiment.trialDTO;

            string timestamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.ffff") + 'Z';

            dto.header.timestamp = timestamp;
            dto.msg.timestamp = timestamp;
            dto.msg.sub_type = "stop";            

            TrialDTO copy = dto.DeepCopy();
            // clear out playernames
            copy.data.client_info.ForEach(client => {
                client.playername = null;
            });

            experiment.trialDTO = dto;

            Console.WriteLine("Stopping Trial with id :  " + experiment.trial_id);
            Console.WriteLine("Stopping Trial with End Condition :  " + experiment.trialDTO.data.trial_end_condition);
            //publish to message bus
            try
            {
                mqtt.publish(copy.convertToJsonString().ToString(), "trial");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            
            Console.WriteLine(" Stop Trial by MQTT received ... PRE container kill async task");
            
            await nonAPIExperimentControl.StopServer();
           
            Console.WriteLine(" Stop Trial by MQTT received ... POST container async task");

            


           

            return new OkObjectResult(dto);

        }

        public static JsonObject aggregateActionOverTeamObject(JsonDocument teamObject, string pid, int actionIntDelta) {

            JsonObject teamActionObject = JsonSerializer.Deserialize<JsonObject>(JsonSerializer.Serialize(teamObject));

            if (teamActionObject == null)               
            {
                Console.WriteLine("Team Action Object was null.");
                teamActionObject = new JsonObject()
                {
                    ["Team"] = 0
                };
            }
            bool found = teamActionObject.ContainsKey(pid);
            if (found)
            {
                Console.WriteLine("Pid Found in Team Action Object.");
                
                teamActionObject[pid] = (int)teamActionObject[pid] + actionIntDelta;
            }
            else
            {
                Console.WriteLine("Pid Not Found in Team Action Object. Creating an entry.");
                teamActionObject.Add(pid, actionIntDelta);
            }
            teamActionObject["Team"] = (int)teamActionObject["Team"] + actionIntDelta;

                       
            return teamActionObject;
        }


        public static JsonObject aggregateActionOverTeamObject(JsonDocument teamObject, string pid, float actionFloatDelta)
        {

            JsonObject teamActionObject = JsonSerializer.Deserialize<JsonObject>(JsonSerializer.Serialize(teamObject));
                      
            Console.WriteLine(JsonSerializer.Serialize(teamActionObject));
            
            if (teamActionObject == null)
            {
                teamActionObject = new JsonObject()
                {
                    ["Team"] = 0f
                };
            }
            bool found = teamActionObject.ContainsKey(pid);
            if (found)
            {                
                teamActionObject[pid] = (float)teamActionObject[pid] + actionFloatDelta;
            }
            else
            {
                teamActionObject.Add(pid, actionFloatDelta);
            }
            teamActionObject["Team"] = (float)teamActionObject["Team"] + actionFloatDelta;

            return teamActionObject;

        }

        public static JsonObject createBombSummaryPlayerObject(IExperiment experiment, string pid)
        {
            JsonObject bombSummaryObject = JsonSerializer.Deserialize<JsonObject>(JsonSerializer.Serialize(experiment.trialSummaryDB.BombSummaryPlayer) );
            
            if (bombSummaryObject == null)
            {
                bombSummaryObject = new JsonObject()
                {
                    [pid] = new JsonObject()
                    {
                        ["PHASES_DEFUSED"] = new JsonObject()
                        {
                            ["STANDARD"] = new JsonObject()
                            {
                                ["R"] = 0,
                                ["G"] = 0,
                                ["B"] = 0
                            },
                            ["CHAINED"] = new JsonObject()
                            {
                                ["R"] = 0,
                                ["G"] = 0,
                                ["B"] = 0
                            },
                            ["FIRE"] = new JsonObject()
                            {
                                ["R"] = 0,
                                ["G"] = 0,
                                ["B"] = 0
                            }
                        },
                        ["BOMBS_DEFUSED"] = new JsonObject()
                        {
                            ["STANDARD"] = 0,
                            ["CHAINED"] = 0,
                            ["FIRE"] = 0
                        },
                        ["BOMBS_EXPLODED"] = new JsonObject()
                        {
                            ["STANDARD"] = 0,
                            ["CHAINED"] = 0,
                            ["FIRE"] = 0
                        }
                    }
                };
            }
            
            else
            {
                if (bombSummaryObject.ContainsKey(pid)){ 
                
                }
                else {

                    bombSummaryObject[pid] = new JsonObject()
                    {
                        ["PHASES_DEFUSED"] = new JsonObject()
                        {
                            ["STANDARD"] = new JsonObject()
                            {
                                ["R"] = 0,
                                ["G"] = 0,
                                ["B"] = 0
                            },
                            ["CHAINED"] = new JsonObject()
                            {
                                ["R"] = 0,
                                ["G"] = 0,
                                ["B"] = 0
                            },
                            ["FIRE"] = new JsonObject()
                            {
                                ["R"] = 0,
                                ["G"] = 0,
                                ["B"] = 0
                            }
                        },                        
                        ["BOMBS_DEFUSED"] = new JsonObject()
                        {
                            ["STANDARD"] = 0,
                            ["CHAINED"] = 0,
                            ["FIRE"] = 0
                        },
                        ["BOMBS_EXPLODED"] = new JsonObject()
                        {
                            ["STANDARD"] = 0,
                            ["CHAINED"] = 0,
                            ["FIRE"] = 0
                        }
                    };
                }
            
            }
            

            experiment.trialSummaryDB.BombSummaryPlayer = JsonDocument.Parse(JsonSerializer.Serialize(bombSummaryObject));

            return JsonSerializer.Deserialize<JsonObject>(JsonSerializer.Serialize(experiment.trialSummaryDB.BombSummaryPlayer)); 

        }

        public static JsonObject incrementBombTypeBombSummary( JsonObject bombSummaryPlayer, string attributeKey, string bombType, string pid){
           
            if(bombType.ToLower().Contains("standard"))
            {
                bombSummaryPlayer[pid][attributeKey]["STANDARD"] = bombSummaryPlayer[pid][attributeKey]["STANDARD"].GetValue<int>() + 1;
            }
            else if (bombType.ToLower().Contains("chained"))
            {
                bombSummaryPlayer[pid][attributeKey]["CHAINED"] = bombSummaryPlayer[pid][attributeKey]["CHAINED"].GetValue<int>() + 1;
            }
            else if (bombType.ToLower().Contains("fire"))
            {
                bombSummaryPlayer[pid][attributeKey]["FIRE"] = bombSummaryPlayer[pid][attributeKey]["FIRE"].GetValue<int>() + 1;
            }

            return bombSummaryPlayer;
        }

    }
}
