﻿using AsistControl.Interfaces;
using AsistControl.DTO;
using AsistControl.DTO.DBModels;
using System.Text.Json.Nodes;
using System.Text.Json;
using Microsoft.AspNetCore.Mvc;

namespace AsistControl
{
    public class NonAPIExperimentControl
    {
        private IConfiguration _config;
        private IMQTT_Client _client;        
        private IExperiment _experiment;
        
        

        public NonAPIExperimentControl(IConfiguration config, IMQTT_Client client, IExperiment experiment) {

            _client = client;                        
            _experiment = experiment;
            _config = config;
            
        }

        public async Task<IActionResult> StopServer() {
            

            // Bring down Minecraft Server

            // STOP Current Minecraft server instance
            string stopCommand = "docker stop minecraft-server0";
            Console.WriteLine("Executing Bash Command : " + stopCommand);
            //BashHelper.Bash(stopCommand);

            // UPDATE SERVER.PROPERTIES
            string server_properties_text = File.ReadAllText("minecraft_data/server.properties");
            Console.Write(server_properties_text);
            string[] lines = server_properties_text.Split('\n');
            string current_map = "";

            foreach (string line in lines)
            {

                // if the string starts with levelname, return the altered string with the last "newline" character removed
                if (line.StartsWith("level-name="))
                {
                    current_map = line;
                    break;
                }
            }

            string deleteMapCommand = "rm -r minecraft_data/" + current_map.Substring(11) + "/";
            //BashHelper.Bash(deleteMapCommand);            

            string copyMapCommand = "(cd CLEAN_MAPS && find " + current_map.Substring(11) + "/ -type f -exec install -Dm 777 \"{}\" \"../minecraft_data/{}\" \\;)";
            //string copyMapCommand = "cp -r CLEAN_MAPS/" + current_map.Substring(11) + "/ minecraft_data/" + current_map.Substring(11) + "/";

            //BashHelper.Bash(copyMapCommand);

            BashHelper.Bash(stopCommand + " && sleep 3 &&" + deleteMapCommand + " && sleep 1 && " + copyMapCommand);
            Console.WriteLine(" Waiting 10 seconds after stop command to allow Minecraft to Stop gracefully and then starting new Trial ");
            await Task.Delay(10000);
            return new OkObjectResult( new JsonElement() );
        }

        public void MockBounce(string args)
        {
            string? admin_stack_ip = Environment.GetEnvironmentVariable("admin_stack_ip");
            string? instance_ip = Environment.GetEnvironmentVariable("instance_ip");
            string? db_api_token = Environment.GetEnvironmentVariable("DB_API_TOKEN");

            string command =
                "ADMIN_STACK_IP=" + admin_stack_ip
                + " && export ADMIN_STACK_IP"
                + " && INSTANCE_IP=" + instance_ip
                + " && export INSTANCE_IP"
                + " && DB_API_TOKEN=" + db_api_token
                + " && export DB_API_TOKEN"
                + "&& ./agent_bounce.sh -" + args;

            BashHelper.Bash(command);

        }
       
        public async Task<IActionResult> startTrial(JsonArray obj)
        {
            // WE NOW HAVE TO CREATE THIS TRIAL DTO USING PARTICIPANT INFORMATION WE OBTAIN FROM THE TEAM FORMATION
            // PROCESS 

            Console.WriteLine(JsonSerializer.Serialize(obj));

            // TeamInfoDTO takes a json array of selected participants
            // if we get the new team stats from the db or somewhere else between trials, this teaminfodto can be updated then
            _experiment.teamInfoDTO = new TeamInfoDTO(obj);

            string teamRecordString = await getTeamRecordFromDBasTeamDB(_experiment);
            if (teamRecordString != null)
            {
                Console.WriteLine($"{teamRecordString}");
                _experiment.teamInfoDTO.NumTrials = _experiment.teamDB.TeamPlays.Count();                
                Console.WriteLine(_experiment.teamDB.TeamPlays.Count());
            }
            else
            {
                Console.WriteLine("TeamId Not found in DB. Assuming this is a new team.");
            }

            TrialDTO dto = new TrialDTO(_experiment.teamInfoDTO, _experiment.experimentDTO);
            dto.data.testbed_version = _config.GetSection("About")["system_version"];
            dto.data.map_name = "Dragon_1.1_3D";

            // THIS WILL CYCLE THROUGH EACH POSSIBILITY UNTIL ALL ARE EXHAUSTED AND THEN REPOPULATE
            dto.data.map_block_filename = randomlySelectMapLayout(_config,_experiment);

            List<string> agents = new List<string>();
            try
            {
                string agent = Environment.GetEnvironmentVariable("asi");
                if (agent != null && (agent.CompareTo(String.Empty) != 0))
                {
                    Console.WriteLine("using Env Var Agent override : " + agent);
                    agents.Add(agent);
                }
                else
                {
                    agents.Add(_experiment.experimentDTO.data.condition);

                }
                Console.WriteLine("Agent : " + agent);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }

            dto.data.intervention_agents = agents;

            Console.WriteLine("Starting Trial with :  " + dto.convertToJsonString().ToString());

            _experiment.experiment_id = dto.msg.experiment_id;
            _experiment.trial_id = dto.msg.trial_id;

            // store dto in experiment singleton
            _experiment.trialDTO = dto;
            // make a copy with its own reference
            TrialDTO copy = dto.DeepCopy();
            // clear out playernames
            copy.data.client_info.ForEach(client => {
                client.playername = null;
            });

            Console.WriteLine("Stored Experiment Trial DTO :  " + _experiment.trialDTO.convertToJsonString().ToString());

            //publish to message bus
            try
            {
                _client.publish(copy.convertToJsonString().ToString(), "trial");

                // This is now necessary to give the ClientMap TrialInfo separately from the trial start message
                TrialInfo trialInfoObject = new TrialInfo(_experiment);
                _client.publish(trialInfoObject.convertToJsonString(), "control/response/getTrialInfo");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            // GET AND PUBLISH AGGREGATED PLAYER DATA FOR EACH PLAYER            
            string? ip = Environment.GetEnvironmentVariable("admin_stack_ip");
            string? port = Environment.GetEnvironmentVariable("admin_stack_port");
            if (port == null)
            {
                port = "443";
            }

            Console.WriteLine("ADMIN_STACK_IP : " + ip);
            Console.WriteLine("ADMIN_STACK_PORT : " + port);

            if (ip.CompareTo("localhost") == 0)
            {
                // this only works for Windows
                ip = "host.docker.internal";
            }

            string aggregatePlayerString = await NonAPIExperimentControl.grabAggregatePlayerData(_experiment, obj, _client);
            //Console.WriteLine(aggregatePlayerString);

            string trialUpdateReturned = await NonAPIExperimentControl.updateOrCreateTrialRecord(_experiment,true);
            Console.WriteLine(trialUpdateReturned);
            string teamUpdateReturned = await NonAPIExperimentControl.updateOrCreateTeamRecord(_experiment);
            Console.WriteLine(teamUpdateReturned);
            
            return new OkObjectResult(dto);
        }
        
        public void startMission(MissionDTO dto)
        {
            string mapname = dto.MapName;
            // set mission name so minecraft-server can grab it on starting
            _experiment.mission_name = dto.MissionName; // this will say Falcon_Easy or whatever
            _experiment.missionDTO = dto;           

            // UPDATE SERVER.PROPERTIES
            string server_properties_text = System.IO.File.ReadAllText("minecraft_data/server.properties");
            //Console.Write(server_properties_text);
            string[] lines = server_properties_text.Split('\n');
            string current_map = "";

            foreach (string line in lines)
            {

                // if the string starts with levelname, return the altered string with the last "newline" character removed
                if (line.StartsWith("level-name="))
                {
                    current_map = line;
                    break;
                }
            }

            string match_string = current_map;
            string replace_string = "level-name=" + mapname;

            Console.WriteLine("Match String : " + match_string + " --- Replace With : " + replace_string);

            string replaceMissionCommand = "sed -i 's/" + match_string + "/" + replace_string + "/g' minecraft_data/server.properties";
            Console.WriteLine("Running Bash command : " + replaceMissionCommand);           

            // Start Minecraft Server instance with new server properties.
            string startCommand = "docker start minecraft-server0";
            //BashHelper.Bash(startCommand);  

            // '&&' MEANS DO NOT PROCESS FOLLOWING COMMAND UNTIL PREVIOUS RETURNS SUCCESSFUL
            // FULL COMMAND BUILT FROM COMMANDS ABOVE
            // Console.WriteLine("Running Bash command : " + deleteMapCommand);
            BashHelper.Bash(replaceMissionCommand);
            Console.WriteLine("Running Bash command : " + replaceMissionCommand);
            BashHelper.Bash(startCommand);
            Console.WriteLine("Executing Bash Command : " + startCommand);

        }

        private string randomlySelectMapLayout(IConfiguration _config, IExperiment _experiment)
        {
            string layout = null;

            string file = Environment.GetEnvironmentVariable("map_blocks_file");

            Console.WriteLine(_config.GetSection("About")["system_version"]);
            Console.WriteLine(JsonSerializer.Serialize(_config.GetSection("MapBlockLayouts").Get<List<string>>()));
            if (_experiment.MapBlocksLayouts == null || _experiment.MapBlocksLayouts.Count == 0)
            {
                _experiment.MapBlocksLayouts = _config.GetSection("MapBlockLayouts").Get<List<string>>();
            }

            if (file != null && (file.CompareTo(String.Empty) != 0))
            {
                Console.WriteLine("using Env Var Layout override : " + layout);
                layout = file;
            }
            else
            {

                int index = new Random().Next(0, _experiment.MapBlocksLayouts.Count);
                layout = _experiment.MapBlocksLayouts.ElementAt(index);
                _experiment.MapBlocksLayouts.RemoveAt(index);

                if (_experiment.MapBlocksLayouts.Count == 0)
                {
                    _experiment.MapBlocksLayouts = _config.GetSection("MapBlockLayouts").Get<List<string>>();
                }

            }


            Console.WriteLine("Layout Selected : " + layout);
            Console.WriteLine(JsonSerializer.Serialize(_experiment.MapBlocksLayouts));
            return layout;
        }

        async public static Task<string> grabAggregatePlayerData(IExperiment _experiment, JsonArray obj, IMQTT_Client _client)
        {
            string? ip = Environment.GetEnvironmentVariable("admin_stack_ip");
            string? port = Environment.GetEnvironmentVariable("admin_stack_port");
            string? bearerToken = Environment.GetEnvironmentVariable("DB_API_TOKEN")!;
            string outString = " Gathe Player Aggregate Data Not Successful ... outString not overwritten.";
            using (var handler = new HttpClientHandler())
            {
                handler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true;
                
                for (int i = 0; i < obj.Count; i++)
                {
                    JsonObject o = JsonSerializer.Deserialize<JsonObject>(obj.ElementAt(i))!;
                    string getPlayerDataAddress = $"https://{ip}:{port}/SSOService/user/GetPlayerStatsForAgentsById/{(string)o["participant_id"]}";
                    Console.WriteLine("Attempting to retreive player info for player/data/aggregated message");
                    Console.WriteLine(getPlayerDataAddress);
                    HttpClient httpClient = new HttpClient(handler);
                    httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer "+ bearerToken);
                    using (var response = await httpClient.GetAsync(getPlayerDataAddress))
                    {
                        try
                        {
                            outString = await response.Content.ReadAsStringAsync();
                            //Console.WriteLine("Retrieved Below message string");
                            //Console.WriteLine(messageString);

                            var options = new JsonSerializerOptions
                            {
                                PropertyNameCaseInsensitive = true
                            };
                            UserMessageBusDTO userData = JsonSerializer.Deserialize<UserMessageBusDTO>(outString, options);
                            PlayerAggDataDTO aggDataDto = new PlayerAggDataDTO();
                            aggDataDto.header.message_type = "metadata";

                            aggDataDto.msg.experiment_id = _experiment.experiment_id;
                            aggDataDto.msg.trial_id = _experiment.trial_id;

                            aggDataDto.data = userData;

                            _client.publish(aggDataDto.convertToJsonString(), "player/data/aggregated");
                        }
                        catch (HttpRequestException e)
                        {
                            Console.WriteLine("\nException Caught!");
                            Console.WriteLine("Message :{0} ", e.Message);
                        }
                    }
                }
            }

            return "Player Aggregate Data successfuly distributed on message bus.";
        }

        async public static Task<string> updateOrCreateTeamRecord( IExperiment _experiment)
        {
            string? ip = Environment.GetEnvironmentVariable("admin_stack_ip");
            string? port = Environment.GetEnvironmentVariable("admin_stack_port");
            string? bearerToken = Environment.GetEnvironmentVariable("DB_API_TOKEN")!;
            string returnedString = " Unsuccessful ... string not overwritten";
            using (var handler = new HttpClientHandler())
            {
                handler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true;

                TeamDB teamDB = new TeamDB();
                teamDB.TeamId = _experiment.trialDTO.data.team_id;
                teamDB.TeamPlays = new List<string>() { _experiment.trialDTO.msg.trial_id };
                teamDB.Members = _experiment.trialSummaryDB.Members;
                teamDB.TeamScore = _experiment.trialSummaryDB.TeamScore;

                string createTeamRecordAddress = $"https://{ip}:{port}/SSOService/team/updateStatsByTeamId/{teamDB.TeamId}";
                Console.WriteLine("Attempting to Create a Team record");
                Console.WriteLine(createTeamRecordAddress);
                HttpClient httpClient = new HttpClient(handler);
                httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + bearerToken);
                using (var response = await httpClient.PutAsJsonAsync(createTeamRecordAddress, teamDB))
                {
                    returnedString = await response.Content.ReadAsStringAsync();                    
                }
            }
            
            return returnedString;
        }

        async public static Task<string> updateOrCreateTrialRecord(IExperiment _experiment, bool create)
        {
            TrialSummaryDB trialSummary = null;
            if (create)
            {
                trialSummary= new TrialSummaryDB();
                trialSummary.TrialId = _experiment.trial_id;
                trialSummary.TrialName = _experiment.trialDTO.data.name;
                trialSummary.MissionVariant = _experiment.trialDTO.data.map_block_filename;
                trialSummary.ExperimentId = _experiment.experiment_id;
                trialSummary.ExperimentName = _experiment.trialDTO.data.experiment_name;
                trialSummary.Members = _experiment.trialDTO.data.subjects!;
                trialSummary.StartTimestamp = _experiment.trialDTO.header.timestamp;
                trialSummary.TeamId = _experiment.teamInfoDTO.TeamId;
                trialSummary.ASICondition = _experiment.experimentDTO.data.condition;

                // CREATE A TRIAL RECORD IN THE PARTICIPANT_DB
                _experiment.trialSummaryDB = trialSummary;
            }
             

            // UPDATE A TRIAL RECORD IN THE PARTICIPANT_DB
            string ip = Environment.GetEnvironmentVariable("admin_stack_ip")!;
            string port = Environment.GetEnvironmentVariable("admin_stack_port")!;
            string? bearerToken = Environment.GetEnvironmentVariable("DB_API_TOKEN")!;
            Console.WriteLine("ADMIN_STACK_IP : " + ip);            
            
            string updateTrialAddress = $"https://{ip}:{port}/SSOService/trial/UpdateTrialRecord/{_experiment.trialSummaryDB.TrialId}";
            Console.WriteLine("Attempting to Update a Trial record");
            Console.WriteLine(updateTrialAddress);
            string outString = "Trial Record Update Unsuccessful - string not overwirtten.";
            using (var handler = new HttpClientHandler())
            {
                handler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true;

                JsonSerializerOptions options = new JsonSerializerOptions();
                options.PropertyNameCaseInsensitive = true;
                HttpClient httpClient = new HttpClient(handler);
                httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " +bearerToken);
                using (HttpResponseMessage response = await httpClient.PutAsJsonAsync(updateTrialAddress, _experiment.trialSummaryDB, options))
                {
                    TrialSummaryDB? responseObj = await response.Content.ReadFromJsonAsync<TrialSummaryDB>();
                    outString = JsonSerializer.Serialize(responseObj);
                }            
            }

            return outString;
        }

        async public static Task<string> getTeamRecordFromDBasTeamDB(IExperiment _experiment)
        {
            // UPDATE A TRIAL RECORD IN THE PARTICIPANT_DB
            string ip = Environment.GetEnvironmentVariable("admin_stack_ip")!;
            string port = Environment.GetEnvironmentVariable("admin_stack_port")!;
            string? bearerToken = Environment.GetEnvironmentVariable("DB_API_TOKEN")!;
            Console.WriteLine("ADMIN_STACK_IP : " + ip);           
            
            string getTeamRecordAddress = $"https://{ip}:{port}/SSOService/team/GetTeamById/"+_experiment.teamInfoDTO.TeamId;
            Console.WriteLine(getTeamRecordAddress);
            string outString = null;
            using (var handler = new HttpClientHandler())
            {
                handler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true;

                JsonSerializerOptions options = new JsonSerializerOptions();
                options.PropertyNameCaseInsensitive = true;

                HttpClient httpClient = new HttpClient(handler);
                httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + bearerToken);
                using (var response = await httpClient.GetAsync(getTeamRecordAddress))
                {
                    _experiment.teamDB = await response.Content.ReadFromJsonAsync<TeamDB>();
                    outString = JsonSerializer.Serialize(_experiment.teamDB);
                }
            }

            return outString;
        }

    }


}
