﻿using AsistControl.DTO;
using AsistControl.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using System.Text.Json.Nodes;

namespace AsistControl.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamAssemblyController : ControllerBase
    {
        private IMQTT_Client _client;
        private IConfiguration _config;
        private IExperiment _experiment;


        public TeamAssemblyController(IMQTT_Client client, IConfiguration config, IExperiment experiment)
        {

            _client = client;
            _config = config;
            _experiment = experiment;
        }

        [HttpPost("[action]")]
        
        public IActionResult updateWaitingList(WaitingRoomDTO dto)
        {
            // this should update everytime a player joins the waiting room, leaves the waiting room, or a trial ends
            Console.WriteLine("Waiting Room List :");
                        
            dto.waitingParticipants!.ForEach(p => Console.WriteLine(p.name + " , " + p.participant_id + " , " + p.time_waiting_milli));
            
            return Ok(dto);

        }

    }
}
