﻿using System.Text.Json;


namespace AsistControl.DTO
{
    public class RollcallDTO
    {
        public CommonHeaderDTO header = new CommonHeaderDTO();
        public RollcallMessageDTO msg = new RollcallMessageDTO();
        public RollcallDataDTO data = new RollcallDataDTO();

        public string convertToJsonString()
        {

            return JsonSerializer.Serialize(this);
        }
    }

    public class RollcallMessageDTO
    {
        public string experiment_id = Guid.Empty.ToString();
        public string trial_id = Guid.Empty.ToString();
        public string timestamp = "NOT SET";
        public string source = "NOT SET";
        public string sub_type = "NOT SET";
        public string version = "NOT SET";
    }

    public class RollcallDataDTO
    {
        public string rollcall_id = "NOT SET";
    }
}
