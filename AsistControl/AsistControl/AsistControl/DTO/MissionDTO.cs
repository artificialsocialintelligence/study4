using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AsistControl.DTO
{
    public class MissionDTO
    {
        public string MissionName { get; set; } = "NOT SET";
        
        public string MapName {get;set; } = "NOT SET";

        public string MapBlockFilename {get;set; } = "NOT SET";

        public string MapInfoFilename {get;set; } = "NOT SET";

        public List<string> ObserverInfo { get; set; } = new List<string>();

    }   
}