﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AsistControl.DTO
{
    public class IdDTO
    {
        public string trial_id { get; set; } = "Not Set";
        public string experiment_id { get; set; } = "Not Set";
    }
}
