﻿using System.Text.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AsistControl.DTO
{
    public class ControlDTO
    {
        public CommonHeaderDTO header = new CommonHeaderDTO();
        public AgentMessageDTO msg = new AgentMessageDTO();

         public string convertToJsonString(){

            return JsonSerializer.Serialize(this);
        }
    }

    public class AgentMessageDTO
    { 
        public string trial_id = "NOT SET";
        public string command = "NOT SET";
        public string experiment_name = "NOT SET";        
    }
}
