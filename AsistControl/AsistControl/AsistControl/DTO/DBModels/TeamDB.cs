﻿namespace AsistControl.DTO.DBModels
{
    public class TeamDB
    {
        public string TeamId { get; set; } = "NOT_SET";
        public List<string> TeamPlays { get; set; } = new List<string>();
        public List<string> Members { get; set; } = new List<string>();

        public int TeamScore { get; set; } = 0;
    }
}
