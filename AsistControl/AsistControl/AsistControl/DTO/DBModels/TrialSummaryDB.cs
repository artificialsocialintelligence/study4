﻿using AsistControl.Interfaces;
using System.Text.Json;

namespace AsistControl.DTO.DBModels
{
    public class TrialSummaryDB
    {

        public string ExperimentId { get; set; } = "NOT_SET";
        public string ExperimentName { get; set; } = "NOT_SET";
        public string TrialId { get; set; } = "NOT_SET";
        public string TrialName { get; set; } = "NOT_SET";
        public string MissionVariant { get; set; } = "NOT_SET";
        public string StartTimestamp { get; set; } = "NOT_SET";
        public string TeamId { get; set; } = "NOT_SET";
        public List<string> Members { get; set; } = new List<string>();
        public string ASICondition { get; set; } = "NOT_SET";
        public int TeamScore { get; set; } = 0;       
        public JsonDocument? BombSummaryPlayer { get; set; }        
        public int BombsTotal { get; set; } = 0;
        public JsonDocument? BombsExploded { get; set; }
        public JsonDocument? DamageTaken { get; set; }
        public JsonDocument? FiresExtinguished { get; set; }
        public JsonDocument? BombBeaconsPlaced { get; set; }
        public JsonDocument? CommBeaconsPlaced { get; set; }
        public JsonDocument? TimesFrozen { get; set; }
        public JsonDocument? FlagsPlaced { get; set; }
        public JsonDocument? TeammatesRescued { get; set; } 
        public JsonDocument? BudgetExpended { get; set; }
        public JsonDocument? TextChatsSent { get; set; }
        public string LastActiveMissionTime { get; set; } = "NOT_SET";
        public int NumStoreVisits { get; set; } = 0;
        public int NumFieldVisits { get; set; } = 0;
        public JsonDocument? NumCompletePostSurveys { get; set; }
        public long TotalStoreTime { get; set; } = 0L;
        public string TrialEndCondition { get; set; } = "NOT_SET";
        public string MissionEndCondition { get; set; } = "NOT_SET";
        public string convertToJsonString() {
            return JsonSerializer.Serialize(this);
        }

    }

    public class TrialSummaryMQTT {

        public CommonHeaderDTO header { get; set; } = new CommonHeaderDTO();
        public CommonMessageDTO msg { get; set; } = new CommonMessageDTO();
        public TrialSummaryDB data { get; set; }

        public TrialSummaryMQTT( IExperiment experiment, string trialSummaryString) {

            header.message_type = "control";
            msg.experiment_id = experiment.trialDTO.msg.experiment_id;
            msg.trial_id = experiment.trialDTO.msg.trial_id;
            msg.sub_type = "Event:TrialSummary";
            msg.version = "0.0.0";
            data = JsonSerializer.Deserialize<TrialSummaryDB>(trialSummaryString);
        }

        public string convertToJsonString()
        {

            return JsonSerializer.Serialize(this);

        }

    }
}
