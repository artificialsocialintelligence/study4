﻿using System.Text.Json;

namespace AsistControl.DTO
{
    public class AgentControlDTO
    {
               
        public string? agent_id { get; set; }
        public string? command { get; set; }

        public string convertToJsonString(){

            return JsonSerializer.Serialize(this);
        }
    }

}
