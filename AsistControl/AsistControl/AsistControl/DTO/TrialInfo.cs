﻿using AsistControl.Interfaces;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Nodes;

namespace AsistControl.DTO
{
    public class TrialInfo { 
        
        public string experiment_id {  get; set; } = " NOT SET ";
        public string trial_id { get; set; } = " NOT SET ";
        public string mission_name { get; set; } = " NOT SET ";
        public string map_name { get; set; } = " NOT SET ";
        public string map_block_filename { get; set; } = " NOT SET ";
        public string map_info_filename { get; set; } = " NOT SET ";

        public List<string> observers = new List<string>();

        public Dictionary<string, string> callSigns = new Dictionary<string, string>();

        public Dictionary<string, string> participant_ids = new Dictionary<string, string>();

        public List<string> active_agents = new List<string>();


        public TrialInfo(IExperiment experiment)
        {
            experiment_id = experiment.trialDTO.msg.experiment_id;
            trial_id = experiment.trialDTO.msg.trial_id;
            mission_name = experiment.trialDTO.data.name;
            map_name = experiment.trialDTO.data.map_name;
            map_block_filename = experiment.trialDTO.data.map_block_filename;
            //map_info_filename = experiment.trialDTO.data.map_info_filename;
            observers = experiment.trialDTO.data.observers;
            experiment.trialDTO.data.client_info.ForEach( (ClientInfoDTO c) => {
                callSigns.Add(c.playername, c.callsign);
                participant_ids.Add(c.playername, c.participant_id);
            });
            active_agents = experiment.trialDTO.data.intervention_agents;

        }



        public string convertToJsonString()
        {
            JsonObject trialInfoObject = new JsonObject()
            {
                ["experiment_id"] = experiment_id,
                ["trial_id"] = trial_id,
                ["mission_name"] = mission_name,
                ["map_name"] = map_name,
                ["map_block_filename"] = map_block_filename,
                ["map_info_filename"] = map_info_filename,
            };

            if (observers != null)
            {
                JsonArray observers = new JsonArray();
                foreach (string ob_name in observers)
                {

                    observers.Add(ob_name);
                }

                trialInfoObject["observer_info"] = observers;
            }
            if (callSigns != null)
            {
                JsonObject csObject = new JsonObject();
                
                foreach ( (string name,string value) in callSigns)
                {

                    csObject.Add(name, value);
                    
                }

                trialInfoObject["callsigns"] = csObject;
                
            }
            if (participant_ids != null)
            {
                JsonObject pidObject = new JsonObject();


                foreach ((string name, string value) in participant_ids)
                {

                    pidObject.Add(name, value);
                }
                
                trialInfoObject["participant_ids"] = pidObject;
            }
            
            if (active_agents != null)
            {
                JsonArray agents = new JsonArray();

                foreach (String name in active_agents)
                {
                    agents.Add(name);
                }
                trialInfoObject["active_agents"] = agents;

            }

            return trialInfoObject.ToJsonString();

        }
    }
}
