﻿namespace AsistControl.DTO
{
    public class UserMessageBusDTO
    {
        public string? ParticipantId { get; set; }
        public int? ParticipationCount { get; set; }
        public SurveysDTO? PreTrialSurveys { get; set; } = null;
        public SurveysDTO? PostTrialSurveys { get; set; } = null;
        public SurveysDTO? OptionalSurveys { get; set; } = null;
        public List<string> TeamsList { get; set; } = new List<string>();

    }
}
