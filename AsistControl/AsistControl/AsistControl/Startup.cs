﻿using AsistControl.Interfaces;


namespace AsistControl
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public IServiceCollection Services { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddSingleton<IMQTT_Client, MQTT_Client>();
            services.AddSingleton<IExperiment, Experiment>();
            services.AddLogging();

            this.Services = services;

           
           
            // SWAGGER
            //services.AddSwaggerGen(c =>
            //{
            //    c.SwaggerDoc("v1", new OpenApiInfo { Title = "AsistControl Api", Version = "v1" });
            //    //c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, $"{Assembly.GetExecutingAssembly().GetName().Name}.xml"));
            //});

            // SignalR
            //services.AddSignalR();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IHostApplicationLifetime lifetime)
        {
            lifetime.ApplicationStarted.Register(OnAppStarted);

            lifetime.ApplicationStopping.Register(OnAppStopping);

            lifetime.ApplicationStopped.Register(OnAppStopped);

            string swaggerEndpoint = "v1/swagger.json";

            string? ip = Environment.GetEnvironmentVariable("admin_stack_ip");
            string? port = Environment.GetEnvironmentVariable("admin_stack_port");

            string basePath = "https://localhost:"+ port + "/AsistControl";

            if (env.IsDevelopment())
            {
                basePath = "http://localhost:5002";
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                //app.UseHttpsRedirection();
            }

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            //app.UseSwagger(c =>
            //{
            //    c.PreSerializeFilters.Add((swaggerDoc, httpReq) =>
            //    {
            //        swaggerDoc.Servers = new List<OpenApiServer> { new OpenApiServer { Url = basePath } };
            //    });
            //}
            //);

            //// Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            //// specifying the Swagger JSON endpoint.
            //app.UseSwaggerUI(c =>
            //{
            //    c.SwaggerEndpoint(swaggerEndpoint, "MalmoControl API v1");

            //});

            // CORS middleware
            app.UseCors(builder => builder
              .AllowAnyHeader()
              .AllowAnyMethod()
              .SetIsOriginAllowed((host) => true)
              .AllowCredentials()
            );

            
        }

        private void OnAppStopped()
        {
            throw new NotImplementedException();
        }

        private void OnAppStopping()
        {
            throw new NotImplementedException();
        }

        private void OnAppStarted()
        {
            var mqtt = Services.BuildServiceProvider().GetService<IMQTT_Client>();
            var creds = Configuration.GetSection("RegistryCredentials");
            string u = creds.GetValue<string>("username");
            string p = creds.GetValue<string>("password");
            try
            {
                Console.WriteLine("Attempting to log into docker registry ...");
                BashHelper.Bash("docker login -u " + u + " -p " + p + " https://gitlab.asist.aptima.com:5050");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }

        }
    }
}

