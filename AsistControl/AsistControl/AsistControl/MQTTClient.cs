﻿using AsistControl.Interfaces;
using Microsoft.Extensions.Logging;
using MQTTnet;
using MQTTnet.Client;
using MQTTnet.Client.Options;
using System.Reflection;

namespace AsistControl
{
    public class MQTT_Client : IMQTT_Client
    {
        private int MasterPort;
        private string MasterIP;
        private string ClientID;        
        private readonly IConfiguration _config;
        public IExperiment _experiment;
        public bool connected = false;
        public IMqttClient client;
        public ILogger<MQTT_Client> _logger;

        public MQTT_Client(IConfiguration config, IExperiment experiment, ILogger<MQTT_Client> logger){            
            
            _config = config;
            _logger = logger;
            
            var factory = new MqttFactory();    
            client = factory.CreateMqttClient();
            
            MasterIP = _config.GetSection("Mqtt")["host"];
            MasterPort = Int32.Parse(config.GetSection("Mqtt")["port"]);            
            ClientID = _config.GetSection("Mqtt")["clientID"];            
            _experiment = experiment;            
            _logger.LogInformation("MQTT Client Initializing");
            //Console.WriteLine();
            Setup();

            try {
                connect().Wait();

            } catch (Exception e) {

                Console.WriteLine("There was an error establishing the MQTT connection on the Client Side");
                Console.WriteLine(e.StackTrace);
            }
        }

         public async Task connect(){

            var options = new MqttClientOptionsBuilder()               
                .WithClientId("AsistControl")
                .WithTcpServer(MasterIP,MasterPort)                   
                .WithCleanSession()                    
                .Build();
            
            await client.ConnectAsync(options, CancellationToken.None);

            Console.WriteLine("MQTT Client Running");           

            await client.SubscribeAsync(new MqttTopicFilterBuilder().WithTopic("status/#").Build());                        
            await client.SubscribeAsync(new MqttTopicFilterBuilder().WithTopic("control/#").Build());
            await client.SubscribeAsync(new MqttTopicFilterBuilder().WithTopic("agent/+/versioninfo").Build());
            await client.SubscribeAsync(new MqttTopicFilterBuilder().WithTopic("agent/control/rollcall/response").Build());            
            await client.SubscribeAsync(new MqttTopicFilterBuilder().WithTopic("observations/events/mission").Build());
            await client.SubscribeAsync(new MqttTopicFilterBuilder().WithTopic("observations/events/stage_transition").Build());
            await client.SubscribeAsync(new MqttTopicFilterBuilder().WithTopic("object/state/change").Build());
            await client.SubscribeAsync(new MqttTopicFilterBuilder().WithTopic("player/state/change").Build());
            await client.SubscribeAsync(new MqttTopicFilterBuilder().WithTopic("score/change").Build());
            await client.SubscribeAsync(new MqttTopicFilterBuilder().WithTopic("ui/click").Build());
            await client.SubscribeAsync(new MqttTopicFilterBuilder().WithTopic("environment/created/single").Build());
            await client.SubscribeAsync(new MqttTopicFilterBuilder().WithTopic("environment/removed/single").Build());
            await client.SubscribeAsync(new MqttTopicFilterBuilder().WithTopic("groundtruth/environment/created/list").Build());
            await client.SubscribeAsync(new MqttTopicFilterBuilder().WithTopic("communication/chat").Build());
            await client.SubscribeAsync(new MqttTopicFilterBuilder().WithTopic("player/survey/response").Build());
            // await client.SubscribeAsync(new TopicFilterBuilder().WithTopic("malmo/Initialized").Build());
        }

        public async void publish(string messageText, string topic){ 
            
            var message = new MqttApplicationMessageBuilder()
                .WithTopic(topic)
                .WithPayload(messageText)
                .WithQualityOfServiceLevel(MQTTnet.Protocol.MqttQualityOfServiceLevel.ExactlyOnce)
                //.WithRetainFlag()
                .Build();

            await client.PublishAsync(message);
        }

         public void Setup(){

          client.UseApplicationMessageReceivedHandler( e => 
            {
                // COMMENT BELOW WHEN NOT DEBUGGING
                // Console.WriteLine("");
                // Console.WriteLine("### RECEIVED APPLICATION MESSAGE ###");
                // Console.WriteLine($"+ Topic = {e.ApplicationMessage.Topic}");
                // Console.WriteLine($"+ Payload = {Encoding.UTF8.GetString(e.ApplicationMessage.Payload)}");
                // Console.WriteLine($"+ QoS = {e.ApplicationMessage.QualityOfServiceLevel}");
                // Console.WriteLine($"+ Retain = {e.ApplicationMessage.Retain}");
                // Console.WriteLine();

                MQTTProcessor.process(_config, this, e, _experiment);

            });
            
            client.UseDisconnectedHandler(e=>{
                Console.WriteLine("\nDisconnected from MQTT Broker!");
                Console.WriteLine("Attempting to Connect to " + MasterIP);                
                connected = false;
            });

            client.UseConnectedHandler(c=>{
                Console.WriteLine("\nConnected to MQTT Broker @ "+MasterIP+":"+MasterPort+"!");
                connected = true;
            });
        }

        public string lastAgentMessage { get; set; } = " NOT SET "; 

        public string lastExperimentMessage {get;set; } = " NOT SET ";  

        public string lastTrialMessage {get;set;} = " NOT SET ";
    }
}
