﻿
using AsistControl.DTO;
using AsistControl.DTO.DBModels;
using System.Text.Json.Nodes;

namespace AsistControl.Interfaces
{
    public interface IExperiment
    {
        JsonArray playerArrayToStartTrial { get; set; }
        string experiment_id {get;set;}
        string trial_id {get;set;}
        string mission_name {get;set;}
        MissionDTO missionDTO { get;set; }
        ExperimentDTO experimentDTO {get;set;}
        TrialDTO trialDTO {get;set;}
        TeamInfoDTO teamInfoDTO { get; set; }
        TrialSummaryDB trialSummaryDB { get; set; }

        TeamDB teamDB { get; set; }
        List<string> MapBlocksLayouts { get; set; } 

    }
}
