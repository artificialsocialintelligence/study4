Name "AsistClientInstaller"
OutFile "AsistClientInstaller.exe"
RequestExecutionLevel user
SilentInstall silent

Section
InitPluginsDir
SetOutPath "$pluginsdir\AsistClientInstaller\Install"

File /r build\windows\runner\Release\*.*

ExecWait '"$pluginsdir\AsistClientInstaller\Install\client_install_bundle.exe"'

SetOutPath $exedir ;Change current dir so $temp and $pluginsdir is not locked by our open handle
SectionEnd


