class ProgressBarData {
  String label1;
  double percentage1;
  String label2;
  double percentage2;

  ProgressBarData(this.label1, this.percentage1, this.label2, this.percentage2);
}
