class ProgressButtonData {
  String buttonText;
  bool enabled;
  bool hadError;

  ProgressButtonData(this.buttonText, this.enabled, this.hadError);
}
