import 'dart:io';

import 'package:client_install_bundle/main.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

import 'services/progressbar_service.dart';
import 'services/progressbutton_service.dart';
import 'services/progresslog_service.dart';

class WizardPage2 extends StatelessWidget {
  const WizardPage2({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'ASIST Minecraft Installer'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: AppBody(),
    );
  }
}

class AppBody extends StatelessWidget {
  AppBody({Key? key}) : super(key: key);
  final ScrollController _scrollController = ScrollController();
  bool _needScroll = false;

  _scrollToEnd() async {
    if (_needScroll) {
      _needScroll = false;
      _scrollController.animateTo(_scrollController.position.maxScrollExtent,
          duration: const Duration(milliseconds: 400), curve: Curves.ease);
    }
  }

  @override
  Widget build(BuildContext context) {
    final ButtonStyle style =
        ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20));
    final ButtonStyle errorStyle = ElevatedButton.styleFrom(
        textStyle: const TextStyle(fontSize: 20), primary: Colors.red);
    final progressLogService = getIt.get<ProgressLogService>();
    final progressBarService = getIt.get<ProgressBarService>();
    final progressButtonService = getIt.get<ProgressButtonService>();

    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(children: const [
                      SizedBox(
                        height: 20,
                        child: null,
                      ),
                    ]),
                    const SizedBox(height: 10),
                    StreamBuilder(
                        stream: progressBarService.stream$,
                        builder: (BuildContext context, AsyncSnapshot snap) {
                          if (snap.data == null) {
                            return const Text('');
                          } else {
                            return Text('${snap.data.label1}');
                          }
                        }),
                    SizedBox(
                      width: 600,
                      child: StreamBuilder(
                          stream: progressBarService.stream$,
                          builder: (BuildContext context, AsyncSnapshot snap) {
                            if (snap.data == null) {
                              return const LinearProgressIndicator(
                                minHeight: 10,
                                value: 0,
                              );
                            } else {
                              return LinearProgressIndicator(
                                minHeight: 10,
                                value: snap.data.percentage1,
                              );
                            }
                          }),
                    ),
                    const SizedBox(height: 10),
                    StreamBuilder(
                        stream: progressBarService.stream$,
                        builder: (BuildContext context, AsyncSnapshot snap) {
                          if (snap.data == null) {
                            return Text('');
                          } else {
                            return Text('${snap.data.label2}');
                          }
                        }),
                    SizedBox(
                      width: 600,
                      child: StreamBuilder(
                          stream: progressBarService.stream$,
                          builder: (BuildContext context, AsyncSnapshot snap) {
                            if (snap.data == null) {
                              return const LinearProgressIndicator(
                                minHeight: 10,
                                value: 0,
                              );
                            } else {
                              return LinearProgressIndicator(
                                minHeight: 10,
                                value: snap.data.percentage2,
                              );
                            }
                          }),
                    ),
                    const SizedBox(height: 10),
                    const Text('''Progress Log:'''),
                    Row(
                      children: [
                        Expanded(
                          child: Container(
                            height: 300,
                            decoration: BoxDecoration(
                                border: Border.all(
                              color: Colors.black,
                            )),
                            child: SingleChildScrollView(
                              controller: _scrollController,
                              child: StreamBuilder(
                                  stream: progressLogService.stream$,
                                  builder: (BuildContext context,
                                      AsyncSnapshot snap) {
                                    _needScroll = true;
                                    SchedulerBinding.instance
                                        .addPostFrameCallback(
                                            (_) => _scrollToEnd());
                                    return Text('${snap.data}');
                                  }),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ]),
            ), //Container
            Align(
              alignment: Alignment.bottomRight,
              child: StreamBuilder(
                  stream: progressButtonService.stream$,
                  builder: (BuildContext context, AsyncSnapshot snap) {
                    return ElevatedButton(
                        onPressed: (snap.data == null || !snap.data.enabled)
                            ? null
                            : () {
                                // Navigator.of(context).push(
                                //   MaterialPageRoute(
                                //     builder: (context) => WizardPage2(),
                                //   ),
                                // );
                                if (kDebugMode) {
                                  print('Exiting');
                                }

                                if (snap.data == null || snap.data.hadError) {
                                  exit(-1);
                                } else {
                                  exit(0);
                                }
                              },
                        style: snap.data == null || snap.data.hadError
                            ? errorStyle
                            : style,
                        child: Text(snap.data == null
                            ? ''
                            : '${snap.data.buttonText}'));
                  }),
            ),
          ],
        ));
  }
}

class CustomPageRoute extends MaterialPageRoute {
  CustomPageRoute({builder}) : super(builder: builder);

  @override
  Duration get transitionDuration => const Duration(milliseconds: 0);
}
