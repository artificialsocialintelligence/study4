import 'package:rxdart/rxdart.dart';

import '../models/progress_button_data.dart';

// Data Model
class ProgressButtonService {
  final BehaviorSubject<ProgressButtonData> _progressButtonData =
      BehaviorSubject.seeded(ProgressButtonData('Installing', false, false));

  Stream get stream$ => _progressButtonData.stream;
  ProgressButtonData get current => _progressButtonData.value;

  setProgressButtonData(ProgressButtonData progressButtonData) {
    _progressButtonData.add(progressButtonData);
  }
}
