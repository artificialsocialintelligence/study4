import 'package:rxdart/rxdart.dart';

// Data Model
class ProgressLogService {
  final BehaviorSubject<String> _log = BehaviorSubject.seeded("");

  Stream get stream$ => _log.stream;
  String get current => _log.value;

  addLine(String line) {
    _log.add(current + line);
  }
}
