$java_installation_minecraft_dir = $env:USERPROFILE + "\\Minecraft Launcher\\runtime\\jre-legacy\\windows-x64\\jre-legacy\\bin\\java.exe"
$java_installation_system_dir = $env:USERPROFILE + $configs.minecraft_installation_dir

Try {
    $java_executable_path = Get-Command $java_installation_minecraft_dir -ErrorAction Stop | Select-Object Path 
    if($java_executable_path ){
        Write-Host ("Java executable found at : " + $java_executable_path)
    }
}
Catch {
    Write-Warning ( "Looks like you don't have Java installed! This experiment requires Minecraft Java Version!" )
    Exit
}