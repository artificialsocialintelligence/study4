# WINDOWS VERSION

# This Script will check if you have Minecraft and Java installed
# If so, it will proceed to make a copy of your minecraft directory to keep it safe, and keep that copy until
# experiment session is over.
# It will then install Minecraft Forge, as well as the Mod and and libraries the mod requires in the correct spots in your
# Minecraft Installation.
# You can restore your original Minecraft Installation after completion.

# read in CONFIGS
Write-Warning ( "THI IS THE LARGE INSTALLATION BUNDLE THAT PACKAGES REQUIRED LIBRARIES IN WITH THE CLIENT BUNDLE." )
Write-Host ("Reading Installation Configs For LARGE INSTALLATION BUNDLE")
$configs = Get-Content "./installation_config.json" | ConvertFrom-Json
Write-Host( $configs )

# TO CHECK

# Minecraft
$minecraft_installation_dir_backup = $env:USERPROFILE + $configs.minecraft_installation_dir_backup
$minecraft_installation_dir = $env:USERPROFILE + $configs.minecraft_installation_dir
$libraries_installation_path = $env:USERPROFILE + $configs.libraries_installation_path
$mod_installation_path = $env:USERPROFILE + $configs.mod_installation_path
$mod_location = $configs.mod_location
$forge_installer_path = $configs.forge_installer_path


if(Test-Path -Path $minecraft_installation_dir){
    
    Write-Host ("Minecraft installation found at : " + $minecraft_installation_dir)
    Write-Host (" Press Enter to Back Up your precious Minecraft Files!")
    Read-Host

    if(Test-Path -Path $minecraft_installation_dir_backup){
        Write-Warning ( "You already have a backup directory ... skipping backup." )
    }
    else{
        #Copy-Item -Path $minecraft_installation_dir -Destination $minecraft_installation_dir_backup -Recurse
        $sourcePath = $minecraft_installation_dir
        $destinationPath = $minecraft_installation_dir_backup
        $files = Get-ChildItem -Path $sourcePath -Recurse
        $filecount = $files.count
        $i=0
        Foreach ($file in $files) {
            $i++
            Write-Progress -activity "Moving files..." -status "($i of $filecount) $file" -percentcomplete (($i/$filecount)*100)
        
            # Determine the absolute path of this object's parent container.  This is stored as a different attribute on file and folder objects so we use an if block to cater for both
            if ($file.psiscontainer) {$sourcefilecontainer = $file.parent} else {$sourcefilecontainer = $file.directory}
        
            # Calculate the path of the parent folder relative to the source folder
            $relativepath = $sourcefilecontainer.fullname.SubString($sourcepath.length)
        
            # Copy the object to the appropriate folder within the destination folder
            copy-Item $file.fullname ($destinationPath + $relativepath)
        }
    }
    

}
else{
    
    Write-Warning ( "Looks like Minecraft is not installed in the default folder." )
    Write-Warning ( "Go ahead and change the installation path in the config.json file to the location of your minecraft installation")
}


# Java
Try {
    $java_executable_path = Get-Command java -ErrorAction Stop | Select-Object Path 
    if($java_executable_path ){
        Write-Host ("Java executable found at : " + $java_executable_path)
    }
}
Catch {
    Write-Warning ( "Looks like you don't have Java installed! This experiment requires Minecraft Java Version!" )
    Exit
}

# LIBS
Write-Warning ( "Updating Libraries")

if(Test-Path -Path $libraries_installation_path){
    Write-Host ("Minecraft libraries found at : " + $libraries_installation_path)
    Remove-Item -r -fo $libraries_installation_path  
}
else{
    Write-Host ("Minecraft libraries not found at : " + $libraries_installation_path)
    Write-Host ("Creating the libraries folder.")
    mdir $libraries_installation_path
}

Copy-Item -Path ".\libraries" -Destination $minecraft_installation_dir -Recurse

# Forge
Write-Warning ( "Setting up Minecraft Forge Profile and Libraries" )
Write-Warning ( "If you are an ASIST participant, select 'Install client' in the Forge installation popup window."  )
java -jar $forge_installer_path

# MOD
Write-Warning ( "Copying asistmod to mod directory")

if(Test-Path -Path $mod_installation_path){
    Write-Host ("Mod folder found at : " + $mod_installation_path)
    Remove-Item -r -fo $mod_installation_path 
}
else{
    Write-Warning ( "Looks like you dont have a mod folder, lets make one." )
    mkdir $mod_installation_path
}

Copy-Item $mod_location $mod_installation_path

Write-Warning ( "ASIST CLIENT BUNDLE successfully installed! Please proceed by entering your username and provided password into the Login page." )

Write-Host( "Press any key to continue")

Read-Host 