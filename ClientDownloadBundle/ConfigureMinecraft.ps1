# WINDOWS VERSION

# This Script will check if you have Minecraft and Java installed
# If so, it will proceed to make a copy of your minecraft directory to keep it safe, and keep that copy until
# experiment session is over.
# It will then install Minecraft Forge, as well as the Mod and and libraries the mod requires in the correct spots in your
# Minecraft Installation.
# You can restore your original Minecraft Installation after completion.

# TO CHECK

# Minecraft
$minecraft_installation_dir = $env:USERPROFILE + "\AppData\Roaming\.minecraft"
$libraries_installation_path = $env:USERPROFILE + "\AppData\Roaming\.minecraft\libraries"
$mod_installation_path = $env:USERPROFILE + "\AppData\Roaming\.minecraft\mods"


if(Test-Path -Path $minecraft_installation_dir){
    Write-Host ("Minecraft installation found at : " + $minecraft_installation_dir)
}
else{
    Write-Warning ( "Looks like Minecraft is not installed in the deafult folder." )
    Write-Warning ( "Go ahead and change the instalation path in the config.json file to the location of your minecraft installation")
}

# Java
Try {
    $java_executable_path = Get-Command java -ErrorAction Stop | Select-Object Path 
    if($java_executable_path ){
        Write-Host ("Java executable found at : " + $java_executable_path)
    }
}
Catch {
    Write-Warning ( "Looks like you don't have Java installed! This experiment requires Minecraft Java Version!" )
    Exit
}


Write-Warning ( "Updating Libraries")

if(Test-Path -Path $libraries_installation_path){
    Write-Host ("Minecraft libraries found at : " + $libraries_installation_path)
    rm -r -fo $libraries_installation_path  
}
else{
    Write-Host ("Minecraft libraries not found at : " + $libraries_installation_path)
    Write-Host ("Creating the libraries folder.")
    mdir $libraries_installation_path
}

Copy-Item -Path ".\libraries" -Destination $minecraft_installation_dir -Recurse

# Forge
Write-Warning ( "Setting up Minecraft Forge Profile and Libraries" )
Write-Warning ( "If you are an ASIST participant, select 'Install client' in the Forge installation popup window."  )
java -jar ".\forge-1.11.2-13.20.1.2588-installer_NO_CHECKSUMS.jar"


Write-Warning ( "Copying asistmod to mod directory")
if(Test-Path -Path $mod_installation_path){
    Write-Host ("Mod folder found at : " + $mod_installation_path)
}
else{
    Write-Warning ( "Looks like you dont have a mod folder, lets make one." )
    mkdir $mod_installation_path
}

cp ".\asistmod-2.0.9.jar" $mod_installation_path
