## TOPIC

testbed_status

## Message Fields

| Field Name                       | Type               | Description                                                                  |
| -------------------------------- | ------------------ | ---------------------------------------------------------------------------- |
| header                           | object             | From Common Message Format section                                           |
| data.id                          | string             | IP address of testbed                                                        |
| data.status                      | string enum        | The status of the testbed - if one or more services are in an error state    |
| data.services                    | array[Service]     | Array of Service objects representing the state of a testbed service         |
| data.participants                | array[Participant] | Array of Participant objects with name and ID of participants                |
| data.error_log                   | array[string]      | Log of error messages produced by testbed services (?)                       |
| data.trial_exports               | array[ExportLog]   | Export history for this testbed - team ID, trial ID, export succeeded/failed |
| data.trial.participants_joined   | boolean            | True when players have joined the game                                       |
| data.trial.surveys_finished      | boolean            | True when surveys are finished                                               |
| data.trial.mission_started       | boolean            | True when mission has begun                                                  |
| data.trial.mission_finished      | boolean            | True after mission ends                                                      |
| data.trial.trial_export_started  | boolean            | True after trial has begun exporting                                         |
| data.trial.trial_export_finished | boolean            | True after trial export has finished                                         |
| data.mission_timer               | string             | Current time in mission                                                      |

## Service Fields

| Field Name | Type        | Description                                            |
| ---------- | ----------- | ------------------------------------------------------ |
| name       | string      | Human-readable name of service                         |
| status     | string enum | The status of the service ['Healthy', 'Warn', 'Error'] |

## Participant Fields

| Field Name | Type   | Description       |
| ---------- | ------ | ----------------- |
| name       | string | Player's username |
| id         | string | Player's ID       |
| team       | string | Player's team     |

## ExportLog Fields

| Field Name     | Type    | Description                     |
| -------------- | ------- | ------------------------------- |
| team_id        | string  | ID of team playing in the trial |
| trial_id       | string  | ID of trial                     |
| export_success | boolean | Whether or not export succeeded |


## Example
{
  "header": {
    "timestamp": "2019-12-26T12:47:23.1234Z",
    "message_type": "testbed_status",
    "version": "1.1"
  },
  "data": {
    "status": "Warn",
    "services": [
        { "name": "Minecraft", "status": "Warn" }
    ],
    "participants": [
        { "name": "Henry", "id": "123", "team": "Miners" }
    ],
    "error_log": [
        "No heartbeat received from Minecraft"
    ],
    "trial_exports": [],
    "trial": {
      "participants_joined": false,
      "surveys_finished": true,
      "mission_started": true,
      "mission_finished": true,
      "trial_export_started": true,
      "trial_export_finished": true
    }
  }
}