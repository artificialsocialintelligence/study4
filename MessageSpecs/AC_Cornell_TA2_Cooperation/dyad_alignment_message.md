# Msg type

This data message subtype, agent, contains data from Cornell's AC component.

It includes players goals via a list for each player that indicates goals updated retroactively.
Possible goals are as follows:
    Recon : indicates that the team is in RECON_STAGE
    Team : The player prioritizes collaboration by making a request to another player 
        (despite having the tools defuse the bomb sequence on their own)
        or responding to another player's request
    Ind : The player prioritizes defusing bombs on their own. 
        This goal indicates the player chooses to defuse multi-step bombs on their own
    * Note: Defusing a one-sequence bomb will not change the player's last goal.
    Hazard : The player is concerned with extinguishing hazard, and or dealing with frozen players (**not yet implemented)
    Shop: The player has voted to go to the shop

We also publish the alignment of goals among players and team and publish them at the end of each stage or trial.
It indicates what portion during the stage or trial did each dyad or team align on the same goal.


## TOPIC

agent/ac/dyad_alignment

## Message Fields

#### Messages published within trial
| Field Name | Type | Description
| --- | --- | --- |
| header | object | From Common_Header Format section
| msg | object | From Common_Message Format section
| data.Alpha_goal | string | Updating Alpha's goal (retroactively or proactively)
| data.Bravo_goal | string | Updating Bravo's goal (retroactively or proactively)
| data.Delta_goal | string | Updating Delta's goal (retroactively or proactively)

#### Messages published at stage transition
| Field Name | Type | Description
| --- | --- | --- |
| header | object | From Common_Header Format section
| msg | object | From Common_Message Format section
| data.stage_start | int | sec at which the given stage started
| data.stage_end | int | sec at which the given stage ended
| data.alignment_alpha_bravo_stage | float | Alpha and Bravo's alignment during stage
| data.alignment_bravo_delta_stage | float | Delta and Bravo's alignment during stage
| data.alignment_alpha_delta_stage | float | Alpha and Delta's alignment during stage
| data.alignment_alpha_bravo_delta_stage | float | Team's alignment during stage

#### Messages published at final
| Field Name | Type | Description
| --- | --- | --- |
| header | object | From Common_Header Format section
| msg | object | From Common_Message Format section
| data.stage_start | int | sec at which the given stage started
| data.stage_end | int | sec at which the given stage ended
| data.alignment_alpha_bravo_stage | float | Alpha and Bravo's alignment during stage
| data.alignment_bravo_delta_stage | float | Delta and Bravo's alignment during stage
| data.alignment_alpha_delta_stage | float | Alpha and Delta's alignment during stage
| data.alignment_alpha_bravo_delta_stage | float | Team's alignment during stage
| data.alignment_alpha_bravo_final | float | Alpha and Bravo's alignment during trial
| data.alignment_bravo_delta_final | float | Delta and Bravo's alignment during trial
| data.alignment_alpha_delta_final | float | Alpha and Delta's alignment during trial
| data.alignment_alpha_bravo_delta_final | float | Team's alignment during trial

## Message Example(s)

```json
{
  "header": {
    "timestamp": "2021-03-16T22:14:53.393Z", 
    "version": "1.1", 
    "message_type": "agent"
  }, 
  "msg": {
    "sub_type": "AC:Dyad_alignment", 
    "timestamp": "2021-03-16T22:14:53.393Z", 
    "experiment_id": "a0d638c4-0411-46b1-b88a-66992f14d64b", 
    "trial_id": "9c78662f-0bd4-4269-b402-fa1e8bc6c36a", 
    "version": "1.0", 
    "source": "Cornell_TA2"
  }, 
  "data": {
    {'Alpha_goal': 'Ind', 
    'Bravo_goal': 'Team', 
    'Delta_goal': 'Team'}
  } 
}
```


```json
{
  "header": {
    "timestamp": "2021-03-16T22:14:53.393Z", 
    "version": "1.1", 
    "message_type": "agent"
  }, 
  "msg": {
    "sub_type": "AC:Dyad_alignment", 
    "timestamp": "2021-03-16T22:14:53.393Z", 
    "experiment_id": "a0d638c4-0411-46b1-b88a-66992f14d64b", 
    "trial_id": "9c78662f-0bd4-4269-b402-fa1e8bc6c36a", 
    "version": "1.0", 
    "source": "Cornell_TA2"
  }, 
  "data": {
    {'stage_start': 442, 
    'stage_end': 713, 
    'alignment_alpha_bravo_stage': 0.7785977859778598, 
    'alignment_bravo_delta_stage': 0.996309963099631, 
    'alignment_alpha_delta_stage': 0.7822878228782287, 'alignment_alpha_bravo_delta_stage': 0.7785977859778598} 
}
```


```json
{
  "header": {
    "timestamp": "2021-03-16T22:14:53.393Z", 
    "version": "1.1", 
    "message_type": "agent"
  }, 
  "msg": {
    "sub_type": "AC:Dyad_alignment", 
    "timestamp": "2021-03-16T22:14:53.393Z", 
    "experiment_id": "a0d638c4-0411-46b1-b88a-66992f14d64b", 
    "trial_id": "9c78662f-0bd4-4269-b402-fa1e8bc6c36a", 
    "version": "1.0", 
    "source": "Cornell_TA2"
  }, 
  "data": {
    {'stage_start': 713, 
    'stage_end': 752, 
    'alignment_alpha_bravo_stage': 1.0, 
    'alignment_bravo_delta_stage': 1.0, 
    'alignment_alpha_delta_stage': 1.0, 
    'alignment_alpha_bravo_delta_stage': 1.0, 
    'alignment_alpha_bravo_trial': 0.8297872340425532, 
    'alignment_bravo_delta_trial': 0.9627659574468085, 
    'alignment_alpha_delta_trial': 0.8351063829787234, 'alignment_alpha_bravo_delta_trial': 0.8151595744680851}
}
```
## CHANGE HISTORY

VERSION | DATE | DETAILS

