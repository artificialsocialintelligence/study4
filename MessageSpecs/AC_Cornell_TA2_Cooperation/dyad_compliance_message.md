# Msg type 
This data message subtype, agent, contains data from Cornell's AC component.

We track any request and response by players in the game. 

Request:
    Requests are made by players when they place a bomb beacon in the environment,
    or a beacon indicating the type of wire cutter they need.
    Multiple requests can be made for one bomb
    Request message will be published each time a new request is made
Response:
    Responses are published each time a new response is detected on an existing request.
    There may be multiple responses to the same request.
    Responder and requester may be the same.
    A response might not have the same type of tool the requester was expecting
    But if a request is fulfilled, meaning the there was already an intended response to a given request, then no further responses can be made on the same request.
    *For hazard beacons needing wirecutters, we assume it is associated with the bomb that is nearest to the beacon.

We also publish the how many request-response pairs were made between players after each stage and trial. 
These numbers are cumulative (unlike 'per stage' messages like goal alignment AC)
 of goals among players and team and publish them at the end of each stage or trial.


## TOPIC

agent/ac/dyad_compliance

## Message Fields

#### Messages published within trial 
##### request
| Field Name | Type | Description
| --- | --- | --- |
| header | object | From Common_Header Format section
| msg | object | From Common_Message Format section
| data.bomb_id | string | Bomb ID associated with the request
| data.requester | string | 
| data.request_stage | string | 
| data.remaining_sequence | string | 
| data.requested_tool | string | 
| data.request_time | int | 
| data.request_stage | string | 



#### Messages published within trial 
##### response
| Field Name | Type | Description
| --- | --- | --- |
| header | object | From Common_Header Format section
| msg | object | From Common_Message Format section
| data.bomb_id | string | Bomb ID associated with the request
| data.requester | string | 
| data.request_stage | string | 
| data.remaining_sequence | string | 
| data.requested_tool | string | 
| data.request_time | int | 
| data.responder | string | 
| data.response | dict | 
| data.respond_time | int | 
| data.respond_stage | string | 
| data.response_tool | string | 
| data.response_time_duration | int | 


#### Messages published at stage transition and final
| Field Name | Type | Description
| --- | --- | --- |
| header | object | From Common_Header Format section
| msg | object | From Common_Message Format section
| data.complied_dyad_raw | dict | raw number of request-response pairs by each dyad
| data.complied_dyad_raw_balance | dict | number of request-response pairs by each dyad divided by the number of all request-response pairs until time. 
| data.dyad_compliance_by_requester_reqs | dict | number of request-response pairs by each dyad divided by the number of all requests made by the requester in the dyad
| data.res_player_compliance_by_all_reqs | dict | number of requests fulfilled by player divided by the number of all requests made until time.

## Message Example(s)

```json
{
  "header": {
    "timestamp": "2021-03-16T22:14:53.393Z", 
    "version": "1.1", 
    "message_type": "agent"
  }, 
  "msg": {
    "sub_type": "AC:Dyad_compliance", 
    "timestamp": "2021-03-16T22:14:53.393Z", 
    "experiment_id": "a0d638c4-0411-46b1-b88a-66992f14d64b", 
    "trial_id": "9c78662f-0bd4-4269-b402-fa1e8bc6c36a", 
    "version": "1.0", 
    "source": "Cornell_TA2"
  }, 
  "data": 
    {'bomb_id': 'BOMB9', 
    'requester': 'Delta', 
    'request_stage': 'FIELD_STAGEF2S1', 
    'remaining_sequence': '[G]', 
    'requested_tool': 'G', 
    'request_time': 188}
}
```


```json
{
  "header": {
    "timestamp": "2021-03-16T22:14:53.393Z", 
    "version": "1.1", 
    "message_type": "agent"
  }, 
  "msg": {
    "sub_type": "AC:Dyad_compliance", 
    "timestamp": "2021-03-16T22:14:53.393Z", 
    "experiment_id": "a0d638c4-0411-46b1-b88a-66992f14d64b", 
    "trial_id": "9c78662f-0bd4-4269-b402-fa1e8bc6c36a", 
    "version": "1.0", 
    "source": "Cornell_TA2"
  }, 
  "data": 
    {'bomb_id': 'BOMB3', 
    'requester': 'Alpha', 
    'request_stage': 'RECON_STAGEF1S0', 
    'remaining_sequence': '[R]', 
    'requested_tool': 'R', 
    'request_time': 7, 
    'responder': 'Alpha', 
    response': {'sequence_index': ['0', '1'], 'sequence': ['R', ''], 'outcome': ['INACTIVE', 'TRIGGERED_ADVANCE_SEQ']}, '
    respond_time': 187, 
    'respond_stage': 'FIELD_STAGEF2S1', 
    'response_tool': 'R', 
    'response_time_duration': 180}
}
```


```json
{
  "header": {
    "timestamp": "2021-03-16T22:14:53.393Z", 
    "version": "1.1", 
    "message_type": "agent"
  }, 
  "msg": {
    "sub_type": "AC:Dyad_compliance", 
    "timestamp": "2021-03-16T22:14:53.393Z", 
    "experiment_id": "a0d638c4-0411-46b1-b88a-66992f14d64b", 
    "trial_id": "9c78662f-0bd4-4269-b402-fa1e8bc6c36a", 
    "version": "1.0", 
    "source": "Cornell_TA2"
  }, 
  "data": 
  { 'complied_dyad_raw': 
      {'AA': 5, 'DA': 6, 'BB': 1, 'AB': 4, 'AD': 1},
    'complied_dyad_raw_balance': 
      {'AA': 0.29411764705882354, 'DA': 0.35294117647058826, 'BB': 0.058823529411764705, 'AB': 0.23529411764705882, 'AD': 0.058823529411764705}, 
    'dyad_compliance_by_requester_reqs': 
      {'AA': 0.35714285714285715, 'DA': 0.46153846153846156, 'BB': 0.2, 'AB': 0.2857142857142857, 'AD': 0.07142857142857142}, 
    'res_player_compliance_by_all_reqs': 
      {'A': 0.34375, 'B': 0.15625, 'D': 0.03125}}
}
```





## CHANGE HISTORY

VERSION | DATE | DETAILS
