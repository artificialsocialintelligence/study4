## TOPIC

item/used

## Message Fields

| Field Name | Type | Description
| --- | --- | ---|
| header | object | From Common Header Format |  
| msg | object | From Common Message Format |  
| data.mission_timer | string | the mission time of the event |
| data.elapsed_milliseconds | number | the number of elapsed milliseconds since mission start |
| data.item_id | string | The unqiue id of the item that changed state |
| data.item_name | string | The registered name of the item in synthetic environment |
| data.input_mode | string | Which user input key was pressed to use this item |
| data.target_x | integer | the x location of the object |
| data.target_y | integer | the y location of the object |
| data.target_z | integer | the z location of the object |

## Message Example

```json
{  
  "header": {
    "timestamp": "2023-01-10T01:26:50.077Z",
    "message_type": "simulator_event",
    "version": "1.2"
  },
  "msg": {
    "experiment_id": "946d4567-ab65-cfe7-b208-426305dc1234",
    "trial_id": "85ed4567-ab65-cfe7-b208-426305dc1234",
    "source": "MINECRAFT_SERVER",
    "sub_type": "Event:ItemUsed",
    "version": "0.0.0"
  },
  "data": {
    "mission_timer": "0 : 6",
    "elapsed_milliseconds": 33505,
    "participant_id": "p100",
    "item_id": "ITEM_2",
    "item_name": "WIRECUTTERS_BLUE",
    "input_mode": "LEFT_MOUSE",
    "target_x": 28,
    "target_y": 52,
    "target_z": 107
  }
}

```

    ## CHANGE HISTORY

VERSION | DATE | DETAILS
| --- | --- | --- |
1.0.0 | 03/22/2023 | Edited item_name enums to reflect true enum names
0.0.0 | 01/09/2022 | Initial MessageSpec created