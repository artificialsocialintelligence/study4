## TOPIC

environment/created/single

## Message Fields

| Field Name | Type | Description
| --- | --- | ---|
| header | object | From Common Header Format section
| msg | object | From the Common Event Message Format section 
| data.mission_timer | string | the mission time of the event
| data.elapsed_milliseconds | number | the number of elapsed milliseconds since mission start
| data | An Object State | Object state representing whichever object is being instantiated into the level as custom protocol - no including UI and standard game startup


## Message Example

```json
"header": {

    "message_type": "simulator_event",
    "version": "1.2",
    "timestamp": "2022-12-05T21:53:31.566Z"

},
"msg": {

    "trial_id": "20046ec2-919e-4828-9c7f-82bae4f2fafb",
    "experiment_id": "7fc30a4a-a0cd-4294-ab3d-e043b15cf0ae",
    "sub_type": "Event:EnvironmentCreatedSingle",
    "source": "MINECRAFT_SERVER",
    "version": "1.0.0"
 },
"data": { 
        
    
    "mission_timer":"8 : 36",
    "elapsed_milliseconds": 7000,
    "triggering_entity": "P000011",
    "obj":{           
        "elapsed_milliseconds": 7000,
        "mission_timer":"8 : 36",
        "x": 28,
        "y": 52,
        "z": 107,
        "id": "ITEMSTACK0_ITEM10",
        "type": "block_beacon_bomb",
        "currAttributes": {
            "uses_remaining": "0",
            "linked_message": "Alpha needs a Red Wirecutter"
        }
    }       
}
```

## CHANGE HISTORY

VERSION | DATE | DETAILS |
| --- | --- | --- |
| 1.0.2 | 03/28/2023 | Added "fuse_start_minute" to "bomb_state" schema |
| 1.0.1 | 02/14/2023 | Added nullable "linked_message" key to "currAttributes" of beacon_state definition |
| 1.0.0 | 02/10/2023 | "triggeringEntity" key changed to "triggering_entity" for uniform json casing
| 0.0.0 | 02/03/2023 | Initial MessageSpec created |
