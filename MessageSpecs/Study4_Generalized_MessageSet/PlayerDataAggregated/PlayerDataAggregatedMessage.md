## TOPIC

player/data/aggregated


## Message Fields

| Field Name | Type | Description
| --- | --- | ---|
| ParticipantId | string | The pid of the player taking the survey
| ParticipationCount | number | The total number of trials completed
| PreTrialSurveys | object | Holds the Pre-Trial survey object representing all participant survey responses to date
| PostTrialSurveys | object | Holds the Post-Trial survey object representing all participant survey responses to date
| TeamsList | string[] | Hold a list of all the teams this participant has been part of to date

## Message Example

```json
{ 
  "header": {
    "message_type": "metadata",
    "version": "0.6",
    "timestamp": "2023-04-18T01:58:11.2994Z"
  },
  "msg": {
    "trial_id": "d04bbc36-ca7e-457c-90c9-f74471f649e5",
    "replay_parent_type": null,
    "sub_type": "Event:PlayerDataAggregated",
    "experiment_id": "8aac65b2-23e2-483b-b2a4-90839c1aa286",
    "replay_id": null,
    "source": "simulator",
    "replay_parent_id": null,
    "version": "0.0.0",
    "timestamp": "2023-04-18T01:58:11.2994Z"
  },
  "data": {
    "ParticipantId": "P000006",
    "OptionalSurveys": null,
    "PostTrialSurveys": null,
    "TeamsList": [],
    "PreTrialSurveys": {
      "participant_id": "P000006",
      "surveys": [
        {
          "survey_id": "DEMO",
          "data": [
            {
              "image_asset": null,
              "response": "23",
              "input_type": "TEXT",
              "text": "What is your age in years?",
              "qid": "1",
              "labels": null
            },
            {
              "image_asset": null,
              "response": "2",
              "input_type": "MULTI_CHOICE",
              "text": "What sex were you assigned at birth, on your original birth certificate?",
              "qid": "2",
              "labels": [
                "Male (1)",
                "Female (2)",
                "Don't know (3)",
                "Prefer not to answer (4)"
              ]
            },
            {
              "image_asset": null,
              "response": "5",
              "input_type": "MULTI_CHOICE",
              "text": "What is your English proficiency level?",
              "qid": "3",
              "labels": [
                "Elementary proficiency (1)",
                "Limited working proficiency (2)",
                "Professional working proficiency (3)",
                "Full professional proficiency (4)",
                "Native or bilingual proficiency (5)"
              ]
            },
            {
              "image_asset": null,
              "response": "4",
              "input_type": "MULTI_CHOICE",
              "text": "The highest education level you have achieved (or equivalent):",
              "qid": "4",
              "labels": [
                "12th grade (1)",
                "Some college/currently enrolled (2)",
                "Some college/not enrolled (3)",
                "Graduated from college (4)",
                "Some training in a master's program and/or graduated from a master's program (5)",
                "Some training in a doctoral program and/or graduated from a doctoral program (6)",
                "Professional degree (MD, JD) (7)",
                "Other level (please specify) (8)"
              ]
            },
            {
              "image_asset": null,
              "response": "1",
              "input_type": "MULTI_CHOICE",
              "text": "Ethnicity",
              "qid": "5",
              "labels": [
                "White/Caucasian (1)",
                "African American (2)",
                "Hispanic or Latino (3)",
                "Asian (4)",
                "Native American (5)",
                "Pacific Islander (6)",
                "Middle Eastern (7)",
                "Other (please specify) (8)"
              ]
            }
          ],
          "completedTimestamp": "2023-04-18T01:05:59.3948Z",
          "complete": true,
          "survey_name": "Demographic Questions"
        },
        {
          "survey_id": "DEMO",
          "data": [
            {
              "image_asset": null,
              "response": "23",
              "input_type": "TEXT",
              "text": "What is your age in years?",
              "qid": "1",
              "labels": null
            },
            {
              "image_asset": null,
              "response": "2",
              "input_type": "MULTI_CHOICE",
              "text": "What sex were you assigned at birth, on your original birth certificate?",
              "qid": "2",
              "labels": [
                "Male (1)",
                "Female (2)",
                "Don't know (3)",
                "Prefer not to answer (4)"
              ]
            },
            {
              "image_asset": null,
              "response": "5",
              "input_type": "MULTI_CHOICE",
              "text": "What is your English proficiency level?",
              "qid": "3",
              "labels": [
                "Elementary proficiency (1)",
                "Limited working proficiency (2)",
                "Professional working proficiency (3)",
                "Full professional proficiency (4)",
                "Native or bilingual proficiency (5)"
              ]
            },
            {
              "image_asset": null,
              "response": "4",
              "input_type": "MULTI_CHOICE",
              "text": "The highest education level you have achieved (or equivalent):",
              "qid": "4",
              "labels": [
                "12th grade (1)",
                "Some college/currently enrolled (2)",
                "Some college/not enrolled (3)",
                "Graduated from college (4)",
                "Some training in a master's program and/or graduated from a master's program (5)",
                "Some training in a doctoral program and/or graduated from a doctoral program (6)",
                "Professional degree (MD, JD) (7)",
                "Other level (please specify) (8)"
              ]
            },
            {
              "image_asset": null,
              "response": "1",
              "input_type": "MULTI_CHOICE",
              "text": "Ethnicity",
              "qid": "5",
              "labels": [
                "White/Caucasian (1)",
                "African American (2)",
                "Hispanic or Latino (3)",
                "Asian (4)",
                "Native American (5)",
                "Pacific Islander (6)",
                "Middle Eastern (7)",
                "Other (please specify) (8)"
              ]
            }
          ],
          "completedTimestamp": "2023-04-18T01:23:59.1117Z",
          "complete": true,
          "survey_name": "Demographic Questions"
        }
      ]
    },
    "UnsuccessfulBombDisposals": 0,
    "SuccessfulBombDisposals": 0,
    "ParticipationCount": 0
  }
}

```

## CHANGE HISTORY

| VERSION | DATE | DETAILS |
| --- | --- | ---|
| 0.0.0 | 04/19/2023 | Initial Schema Creation |
