## TOPIC

player/state/change

## Message Fields

| Field Name | Type | Description
| --- | --- | ---|
| header | object | From Common Header Format |  
| msg | object | From Common Message Format |  
| data.mission_timer | string | the mission time of the event |
| data.elapsed_milliseconds | number | the number of elapsed milliseconds since mission start |
| data.source_type | string | the type of the entity that affected the change |
| data.source_id | string | the source id of the entity that affected the change |
| data.player_x | integer | the x location of the player |
| data.player_y | integer | the y location of the player |
| data.player_z | integer | the z location of the player |
| data.participant_id | string | the participant id of the player |
| data.source_x | integer | the x location of the source of the change |
| data.source_y | integer | the y location of the source of the change |
| data.source_z | integer | the z location of the source of the change |
| data.changedAttributes | object | Map<String,String[]> representing any changed attributes in their previous and current values |
| data.currentAttributes | object | Map<String,String> representing the current attribute state of the player |

## Message Example

```json
{
    "header": {
        "timestamp": "2019-12-26T12:47:23.1234Z",
        "message_type": "simulator_event",
        "version": "0.4"
	},
    "msg": { 
        "experiment_id": "523e4567-e89b-12d3-a456-426655440000",
        "trial_id": "123e4567-e89b-12d3-a456-426655440000",	
        "source": "minecraft_server",
        "sub_type": "Event:PlayerStateChange",
        "version": "1.0.0"
    },
    "data": {
        "mission_timer":"8 : 36",
        "elapsed_milliseconds": 15113,
        "source_type": "bomb_block_standard",	
        "source_id": "BOMB25",
        "participant_id":"P0000111",	
        "player_x": -2185,
        "player_y":28,
        "player_z":198,
        "source_x": -2187,
        "source_y":28,
        "source_z":198,
        "changedAttributes":{
            "is_frozen":["false","true"],                      
        },
        "currAttributes":{
            "is_frozen":"true",
            "ppe_equipped":"false",
            "health":"0.6", 
        }	
	}
}

```

## CHANGE HISTORY

VERSION | DATE | DETAILS
| --- | --- | --- |
| 0.0.0 | 02/26/2022 | Initial MessageSpec created