## TOPIC

communication/chat

## USAGE
Fired when participants chat in the Shop

## Message Fields

| Field Name | Type | Description
| --- | --- | ---|
| header | object | From Common Header Format section |
| msg | object | From the Common Event Message Format section |
| data.mission_timer | string | the mission time of the event |
| data.elapsed_milliseconds | number | the number of elapsed milliseconds since mission start |
| data.message_id | string | The unique id of this message |
| data.sender_id | string | The id of the object sending the message |
| data.sender_x | int (nullable) | The x position of the object sending the message - can be null |
| data.sender_y | int (nullable) | The y position of the object sending the message - can be null |
| data.sender_z | int (nullable) | The z position of the object sending the message - can be null |
| data.recipients | string[] | An array of ids of the recipients of the message |
| data.message | string | The raw form of the message |
| data.environment | string | an string representing the environment in which the chat took place
| data.additional_info | json object (nullable) | additional info that may be of use when processing the object

## Message Example

```json

{
  "header": {
    "timestamp": "2023-02-10T22:26:37.364Z",
    "message_type": "simulator_event",
    "version": "1.2"
  },
  "msg": {
    "experiment_id": "946d4567-ab65-cfe7-b208-426305dc1234",
    "trial_id": "85ed4567-ab65-cfe7-b208-426305dc1234",
    "source": "MINECRAFT_SERVER",
    "sub_type": "Event:CommunicationEnvironment",
    "version": "1.0.0"
  },
  "data": {
    "mission_timer": "0 : 10",
    "elapsed_milliseconds": 49453,
    "message_id":"COMM_CHAT0",
    "environment":"SHOP_GUI",
    "sender_id": "P000008",
    "recipients": [
      "P000009",
      "P000010"
    ],
    "message": "Hello team!",
    "sender_x": 26,
    "sender_y": 52,
    "sender_z": 108,
    "additional_info":null
  }
}
```

## CHANGE HISTORY

| VERSION | DATE | DETAILS |
| --- | --- | --- |
| 1.0.0 | 02/15/2022 | Added message_id, environment, and addition_info keys as well as nullable sender_x,y,z as these chats occur in the Shop from the same location every time |
| 0.0.0 | 02/15/2022 | Initial Spec Created |

