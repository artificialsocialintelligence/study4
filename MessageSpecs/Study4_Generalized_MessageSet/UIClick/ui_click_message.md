## TOPIC

ui/click

## Message Fields

| Field Name | Type | Description
| --- | --- | ---|
| header | object | From Common Header Format section
| msg | object | From the Common Event Message Format section 
| data.participant_id | string | the unique id of the participant in the study
| data.element_id | string | the unique id of the element clicked in the UI
| data.parent_id | string | the unique id of the parent of the element clicked in the UI
| data.type | enum | the type of element clicked
| data.x | number | the x location of the click
| data.y | number | the y location of the click
| data.additional_info | json object | Additional information about the click event
| data.additional_info.meta_action | enum | A description of the action that the click signifies
| data.additional_info.call_sign_code | number | The index of the callsign (from the array of callsigns) who did the clicking


## Message Example

```json

{
    "header":{
        "timestamp": "2019-12-26T14:05:02.3412Z",
        "message_type": "simulator_event",
        "version": "1.2"
    },
    "msg":{
        "experiment_id":"946d4567-ab65-cfe7-b208-426305dc1234",
        "trial_id":"123e4567-e89b-12d3-a456-426655440000",
        "source":"simulator",
        "sub_type": "Event:UIClick",
        "version":"0.0.2"
    },
    "data":{      
        "participant_id":"p100",
        "element_id": "test_id",
        "parent_id":"StoreInteractionContainer",
        "type": "BUTTON",
        "x": 0,
        "y": 0,
        "additional_info":{
            "meta_action":"VOTE_LEAVE_STORE",        
            "call_sign_code":0
        }
    }
}

```

    ## CHANGE HISTORY

VERSION | DATE | DETAILS
| --- | --- | --- |
0.0.2 | 03/29/2023 | Added models and example schemas for flagUpdate and beaconUpdate - used for responding and communication
0.0.1 | 03/15/2023 | Added actual model examples for all current implimented UI Click events
0.0.0 | 01/01/2023 | Initial MessageSpec created