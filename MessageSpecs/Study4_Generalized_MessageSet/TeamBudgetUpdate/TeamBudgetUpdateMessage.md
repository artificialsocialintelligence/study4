## TOPIC

team/budget/update

## Message Fields

| Field Name                | Type        | Description                                            |
| ------------------------- | ----------- | ------------------------------------------------------ |
| header                    | object      | From Common Header Format section                      |
| msg                       | object      | From the Common Event Message Format section           |
| data.mission_timer        | string      | the mission time of the event                          |
| data.elapsed_milliseconds | number      | the number of elapsed milliseconds since mission start |
| data.team_budget          | number      | The integer value of the current team budget           |

## Message Example

```json
{
  "header": {
    "timestamp": "2019-12-26T12:47:23.1234Z",
    "message_type": "event",
    "version": "0.5"
  },
  "msg": {
    "experiment_id": "563e4567-e89b-12d3-a456-426655440000",
    "trial_id": "123e4567-e89b-12d3-a456-426655440000",
    "timestamp": "2019-12-26T14:05:02.1412Z",
    "source": "minecraft_server",
    "sub_type": "Event:TeamBudgetUpdate",
    "version": "0.5"
  },
  "data": {
    "mission_timer": "0 : 0",
    "elapsed_milliseconds": 1,
    "team_budget": 50
  }
}
```
## CHANGE HISTORY

VERSION | DATE | DETAILS
| --- | --- | --- |
| 0.0.0 | 03/22/2023 | Initial Message Spec created