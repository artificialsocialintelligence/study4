## TOPIC

score/change

## Message Fields

| Field Name | Type | Description
| --- | --- | ---|
| header | object | From Common Message Format section
| msg | object | From the event Header Format section 
| data.mission_timer | string | the mission time of the event
| data.elapsed_milliseconds | number | the number of elapsed milliseconds since mission start
| data.playerScores | object | The scoreboard represented as a Map object with pids as keys and player scores as integer values
| data.teamScore | number | The aggregated teamscore


## Message Example

```json
{
	"header": {
		"timestamp": "2019-12-26T12:47:23.1234Z",
		"message_type": "event",
		"version": "0.4"
	},
	"msg": { 
    	"trial_id": "123e4567-e89b-12d3-a456-426655440000",
		"timestamp": "2019-12-26T14:05:02.1412Z",
		"source": "simulator",
		"sub_type": "Event:ScoreUpdate",
		"version": "0.4"
	},
	"data": {
		"mission_timer":"8 : 36",
		"elapsed_milliseconds": 15113,
		"playerScores": {
	        "P000007": 10,
			"P000010": 10,
			"P000003": 10,			
	    },
		"teamScore":30	
	}	
}

```

VERSION | DATE | DETAILS
| --- | --- | --- |
| 0.0.0 | 03/01/2022 | Initial MessageSpec created