## TOPIC

item/state/change

## Message Fields

| Field Name | Type | Description
| --- | --- | ---|
| header | object | From Common Header Format |  
| msg | object | From Common Message Format |  
| data.mission_timer | string | the mission time of the event |
| data.elapsed_milliseconds | number | the number of elapsed milliseconds since mission start |
| data.item_id | string | The unqiue id of the item that changed state |
| data.item_name | string | The registered name of the item in synthetic environment |
| data.owner | string | The unique participant id of the owner of this item |
| data.changedAttributes | object | Map<String,String[]> representing any changed attributes in their previous and current values |
| data.currentAttributes | object | Map<String,String> representing the current attribute state of the objecy |

## Message Example

```json
{
  "header": {
    "timestamp": "2023-01-10T01:26:52.679Z",
    "message_type": "simulator_event",
    "version": "1.2"
  },
  "msg": {
    "experiment_id": "946d4567-ab65-cfe7-b208-426305dc1234",
    "trial_id": "85ed4567-ab65-cfe7-b208-426305dc1234",
    "source": "MINECRAFT_SERVER",
    "sub_type": "Event:ItemStateChange",
    "version": "1.0.1"
  },
  "data": {
    "mission_timer": "0 : 9",
    "elapsed_milliseconds": 36107,
    "item_id": "ITEM_2",
    "item_name": "WIRECUTTERS_BLUE",
    "owner": "p100",
    "changedAttributes": {
      "uses_remaining": [
        "1",
        "0"
      ]
    },
    "currAttributes": {
      "uses_remaining": "0"
    }
  }
}

```

## CHANGE HISTORY

VERSION | DATE | DETAILS
| --- | --- | --- |
1.0.1 | 03/22/2023 | Added optional fields in item_state object to capture basic changes to beacons - ied bomb_id set for bomb beacon and message set for hazard beacon
1.0.0 | 03/22/2023 | Edited item_name enums to reflect true enum names
0.0.0 | 01/09/2022 | Initial MessageSpec created