# groundtruth subtype : measures Message Format
The measures published at the end of a trial.  

## TOPICS

agent/measures

## Message Fields

| Field Name | Type | Description|
 --- | --- | ---
| id | string | The id of the location
| x | number | the x coord 
| z | number | the z coord 

## Message Example

```json
{
    "x" : -2175,
    "z" : 0
}