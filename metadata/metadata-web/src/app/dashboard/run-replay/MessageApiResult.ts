export interface MessageApiResult {
  result: string;
  message: string;
  data: {};
}
