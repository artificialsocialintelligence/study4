export interface IgnoreListItem {
  message_type: string;
  sub_type: string;
}
