import { Experiment } from '../experiment/experiment';

export interface ClientInfo {
  callsign: string;
  participant_id: string;
}
