import { Experiment } from '../experiment/experiment';
import { ClientInfo } from './client-info';

export interface Trial {
  id: number;
  trial_id: string;
  name: string;
  date: string;
  experimenter: string;
  trial_number: string;
  group_number: string;
  study_number: string;
  condition: string;
  subjects: string[];
  notes: string[];
  testbed_version: string;
  map_name: string;
  map_block_filename: string;
  client_info: ClientInfo[];
  experiment: Experiment;
  team_id: string;
  intervention_agents: string[];
  observers: string[];
}
