package metadata.app.service;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintWriter;
import java.text.MessageFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.ExecutorService;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import io.reactivex.Observable;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.inject.Singleton;
import jakarta.validation.constraints.NotNull;

import metadata.app.model.*;
import metadata.app.publisher.ReplayMessagePublisher;
import org.apache.lucene.search.TotalHits;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.ClearScrollRequest;
import org.elasticsearch.action.search.ClearScrollResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.settings.Settings;
//import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.core.TimeValue;
//import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.Scroll;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.micronaut.context.annotation.Property;
import io.micronaut.core.annotation.NonNull;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.MediaType;
import io.micronaut.http.exceptions.HttpStatusException;
import io.micronaut.http.server.types.files.StreamedFile;
import io.micronaut.http.server.types.files.SystemFile;
import io.vertx.core.json.JsonArray;
import io.vertx.reactivex.pgclient.PgPool;
import io.vertx.reactivex.sqlclient.Row;
import io.vertx.reactivex.sqlclient.RowIterator;
import io.vertx.reactivex.sqlclient.RowSet;
import io.vertx.reactivex.sqlclient.Tuple;
import metadata.app.publisher.TrialCreatedPublisher;

@Singleton
public class DefaultTrialService implements TrialService {
    private static final Logger logger = LoggerFactory.getLogger(DefaultTrialService.class);
    private ObjectMapper objectMapper = new ObjectMapper();
    // final BeanContext context = BeanContext.run();
    @Inject
    private PgPool client;
    @Inject
    private RestHighLevelClient elasticsearchClient;

    @Named("io")
    @Inject
    ExecutorService executorService;

    private long currentMessageCount = 0;
    private long totalMessageCount = 0;

    private final DefaultExperimentService defaultExperimentService;
    private final TrialCreatedPublisher trialCreatedClient;

    private final ReplayMessagePublisher replayMessagePublisher;

    private final String HEADER_METADATA_FILE_SUFIX = ".metadata";

    @Property(name = "asist.testbedVersion")
    private String TESTBED_VERSION;

    @Inject
    public DefaultTrialService(DefaultExperimentService defaultExperimentService,
                               TrialCreatedPublisher trialCreatedClient,
                               ReplayMessagePublisher replayMessagePublisher) {
        this.defaultExperimentService = defaultExperimentService;
        this.trialCreatedClient = trialCreatedClient;
        this.replayMessagePublisher = replayMessagePublisher;

//		client = context.getBean(PgPool.class);
//		PgPoolOptions options = new PgPoolOptions().setPort(5432).setHost("localhost").setDatabase("postgres").setUser("postgres").setPassword("example").setMaxSize(5);
//		client = PgClient.pool(options);
    }

    @Override
    public Trial createTrial(Trial trial) {
        // Do we want to do an ON CONFLICT DO UPDATE here to overwrite trial that might have been added via import. For now, yes.
        String sqlQuery = "INSERT INTO trials (\r\n"
                + "  trial_id, name, date, experimenter, \r\n"
                + "  subjects, trial_number, group_number, \r\n"
                + "  study_number, condition, notes, testbed_version, \r\n"
                + "  map_name, map_block_filename, client_info, team_id, intervention_agents, observers, \r\n"
                + "  experiment_id_experiments\r\n"
                + ") \r\n"
                + "VALUES \r\n"
                + "  (\r\n"
                + "    $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, \r\n"
                + "    $11, $12, $13, $14, $15, $16, $17, $18\r\n"
                + "  ) ON CONFLICT (trial_id) DO \r\n"
                + "UPDATE \r\n"
                + "SET \r\n"
                + "  name = EXCLUDED.name, \r\n"
                + "  date = EXCLUDED.date, \r\n"
                + "  experimenter = EXCLUDED.experimenter, \r\n"
                + "  subjects = EXCLUDED.subjects, \r\n"
                + "  trial_number = EXCLUDED.trial_number, \r\n"
                + "  group_number = EXCLUDED.group_number, \r\n"
                + "  study_number = EXCLUDED.study_number, \r\n"
                + "  condition = EXCLUDED.condition, \r\n"
                + "  notes = EXCLUDED.notes, \r\n"
                + "  testbed_version = EXCLUDED.testbed_version, \r\n"
                + "  map_name = EXCLUDED.map_name, \r\n"
                + "  map_block_filename = EXCLUDED.map_block_filename, \r\n"
                + "  client_info = EXCLUDED.client_info, \r\n"
                + "  team_id = EXCLUDED.team_id, \r\n"
                + "  intervention_agents = EXCLUDED.intervention_agents, \r\n"
                + "  experiment_id_experiments = EXCLUDED.experiment_id_experiments, \r\n"
                + "  observers = EXCLUDED.observers RETURNING (id);";
        Tuple elements = Tuple.tuple();
        elements.addUUID(UUID.fromString(trial.getTrialId()));
        elements.addString(trial.getName());
        elements.addLocalDateTime(LocalDateTime.ofInstant(Instant.parse(trial.getDate()), ZoneOffset.UTC));
        elements.addString(trial.getExperimenter());
        elements.addArrayOfString(trial.getSubjects().toArray(new String[0]));
        elements.addString(trial.getTrialNumber());
        elements.addString(trial.getGroupNumber());
        elements.addString(trial.getStudyNumber());
        elements.addString(trial.getCondition());
        elements.addArrayOfString(trial.getNotes().toArray(new String[0]));
        elements.addString(trial.getTestbedVersion());
        elements.addString(trial.getMapName());
        elements.addString(trial.getMapBlockFilename());
        String clientInfo = "[]";
        try {
            clientInfo = objectMapper.writeValueAsString(trial.getClientInfo() != null ? trial.getClientInfo() : new ArrayList<ClientInfoItem>());
        } catch (JsonProcessingException e) {
            logger.error(e.getMessage());
            return null;
        }
        elements.addValue(io.vertx.core.json.Json.decodeValue(clientInfo));
		elements.addString(trial.getTeamId());
		elements.addArrayOfString(trial.getInterventionAgents().toArray(new String[0]));
		elements.addArrayOfString(trial.getObservers().toArray(new String[0]));
        elements.addUUID(UUID.fromString(trial.getExperiment().getExperimentId()));
        try {
            RowSet<Row> rowSet = client.preparedQuery(sqlQuery).rxExecute(elements).blockingGet();
            RowIterator<Row> rowIterator = rowSet.iterator();
            if (rowIterator.hasNext()) {
                logger.info("1 row(s) affected.");
                Row row = rowIterator.next();
                Long _id = row.getLong("id");
                logger.info(MessageFormat.format("id returned: {0}.", _id));
                trial.setId(_id);
            }
            if (trial.getId() > 0) {
                trialCreatedClient.send(objectMapper.writeValueAsBytes(trial)).subscribe(() -> {
                    // handle completion
                }, throwable -> {
                    // handle error
                });
                return trial;
            } else {
                logger.error(MessageFormat.format("Trial id: {0} from database was invalid!", trial.getId()));
                return null;
            }
        } catch (RuntimeException e) {
            logger.error(e.getMessage());
            return null;
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            logger.error(e.getMessage());
            return null;
        }
    }

    @Override
    public List<Trial> readTrials() {
        List<Trial> trials = new LinkedList<>();
        try {
            RowSet<Row> rowSet = client.preparedQuery("SELECT * from trials ORDER BY id ASC").rxExecute().blockingGet();
            RowIterator<Row> rowIterator = rowSet.iterator();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                JsonArray jsonArray = (JsonArray) row.getValue("client_info");
                if (row.getUUID("experiment_id_experiments") == null) {
                    logger.error(MessageFormat.format("Trial [{0}] has no experiment assigned to it!",
                            row.getUUID("trial_id").toString()));
                }
                Experiment experiment = defaultExperimentService.readExperimentUUID(row.getUUID("experiment_id_experiments") == null ? null : row.getUUID("experiment_id_experiments").toString());
                trials.add(new Trial(row.getInteger("id"), row.getUUID("trial_id").toString(), row.getString("name"),
                        row.getLocalDateTime("date").toInstant(ZoneOffset.UTC).toString(),
                        row.getString("experimenter"), Arrays.asList(row.getArrayOfStrings("subjects")),
                        row.getString("trial_number"), row.getString("group_number"), row.getString("study_number"),
                        row.getString("condition"), Arrays.asList(row.getArrayOfStrings("notes")),
                        row.getString("testbed_version"), experiment,
                        row.getString("map_name"),
                        row.getString("map_block_filename"),
                        jsonArray != null ? objectMapper.readValue(jsonArray.encode(), new TypeReference<List<ClientInfoItem>>() {
                        }) : new ArrayList<ClientInfoItem>(),
						row.getString("team_id"),
						Arrays.asList(row.getArrayOfStrings("intervention_agents")),
						Arrays.asList(row.getArrayOfStrings("observers"))
                ));
            }
        } catch (RuntimeException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            return null;
        } catch (JsonProcessingException e) {
			logger.error(e.getMessage());
            e.printStackTrace();
        }
        return trials;
    }

    @Override
    public Trial readTrial(long id) {
        List<Trial> trials = new LinkedList<>();
        try {
            RowSet<Row> rowSet = client.preparedQuery("SELECT * FROM trials WHERE id = $1").rxExecute(Tuple.of((int) id)).blockingGet();
            RowIterator<Row> rowIterator = rowSet.iterator();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                JsonArray jsonArray = (JsonArray) row.getValue("client_info");
                if (row.getUUID("experiment_id_experiments") == null) {
                    logger.error(MessageFormat.format("Trial [{0}] has no experiment assigned to it!", row.getUUID("trial_id").toString()));
                }
                Experiment experiment = defaultExperimentService.readExperimentUUID(row.getUUID("experiment_id_experiments") == null ? null : row.getUUID("experiment_id_experiments").toString());
                trials.add(new Trial(row.getInteger("id"), row.getUUID("trial_id").toString(), row.getString("name"),
                        row.getLocalDateTime("date").toInstant(ZoneOffset.UTC).toString(),
                        row.getString("experimenter"), Arrays.asList(row.getArrayOfStrings("subjects")),
                        row.getString("trial_number"), row.getString("group_number"), row.getString("study_number"),
                        row.getString("condition"), Arrays.asList(row.getArrayOfStrings("notes")),
                        row.getString("testbed_version"), experiment,
                        row.getString("map_name"),
                        row.getString("map_block_filename"),
                        jsonArray != null ? objectMapper.readValue(jsonArray.encode(), new TypeReference<List<ClientInfoItem>>() {
                        }) : new ArrayList<ClientInfoItem>(),
						row.getString("team_id"),
						Arrays.asList(row.getArrayOfStrings("intervention_agents")),
						Arrays.asList(row.getArrayOfStrings("observers"))
                ));
            }
        } catch (RuntimeException e) {
            logger.error(e.getMessage());
            return null;
        } catch (JsonProcessingException e) {
			logger.error(e.getMessage());
            e.printStackTrace();
        }
        if (trials.isEmpty()) {
            logger.info(MessageFormat.format("No trial id: {0} found!", id));
            return null;
        }
        return trials.iterator().next();
    }

    @Override
    public Trial readTrialUUID(String trialId) {
        List<Trial> trials = new LinkedList<>();
        try {
            RowSet<Row> rowSet = client.preparedQuery("SELECT * FROM trials WHERE trial_id = $1").rxExecute(Tuple.of(UUID.fromString(trialId))).blockingGet();
            RowIterator<Row> rowIterator = rowSet.iterator();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                JsonArray jsonArray = (JsonArray) row.getValue("client_info");
                if (row.getUUID("experiment_id_experiments") == null) {
                    logger.error(MessageFormat.format("Trial [{0}] has no experiment assigned to it!",
                            row.getUUID("trial_id").toString()));
                }
                Experiment experiment = defaultExperimentService
                        .readExperimentUUID(row.getUUID("experiment_id_experiments") == null ? null
                                : row.getUUID("experiment_id_experiments").toString());
                trials.add(new Trial(row.getInteger("id"), row.getUUID("trial_id").toString(), row.getString("name"),
                        row.getLocalDateTime("date").toInstant(ZoneOffset.UTC).toString(),
                        row.getString("experimenter"), Arrays.asList(row.getArrayOfStrings("subjects")),
                        row.getString("trial_number"), row.getString("group_number"), row.getString("study_number"),
                        row.getString("condition"), Arrays.asList(row.getArrayOfStrings("notes")),
                        row.getString("testbed_version"), experiment,
                        row.getString("map_name"),
                        row.getString("map_block_filename"),
                        jsonArray != null ? objectMapper.readValue(jsonArray.encode(), new TypeReference<List<ClientInfoItem>>() {
                        }) : new ArrayList<ClientInfoItem>(),
						row.getString("team_id"),
						Arrays.asList(row.getArrayOfStrings("intervention_agents")),
						Arrays.asList(row.getArrayOfStrings("observers"))
                ));
            }
        } catch (RuntimeException e) {
            logger.error(e.getMessage());
            return null;
        } catch (JsonProcessingException e) {
			logger.error(e.getMessage());
            e.printStackTrace();
        }
        if (trials.isEmpty()) {
            logger.info(MessageFormat.format("No trial uuid: {0} found!", trialId));
            return null;
        }
        return trials.iterator().next();
    }

    @Override
    public Trial updateTrial(Trial trial) {
        String sqlQuery = "UPDATE trials SET trial_id = $1, name = $2, date = $3, experimenter = $4, subjects = $5, trial_number = $6, group_number = $7, study_number = $8, condition = $9, notes = $10, testbed_version = $11, map_name = $12, map_block_filename = $13, client_info = $14, team_id = $15, intervention_agents = $16 , observers = $17 , experiment_id_experiments = $18  WHERE id = $19 RETURNING (id)";
        Tuple elements = Tuple.tuple();
        elements.addUUID(UUID.fromString(trial.getTrialId()));
        elements.addString(trial.getName());
        elements.addLocalDateTime(LocalDateTime.ofInstant(Instant.parse(trial.getDate()), ZoneOffset.UTC));
        elements.addString(trial.getExperimenter());
        elements.addArrayOfString(trial.getSubjects().toArray(new String[0]));
        elements.addString(trial.getTrialNumber());
        elements.addString(trial.getGroupNumber());
        elements.addString(trial.getStudyNumber());
        elements.addString(trial.getCondition());
        elements.addArrayOfString(trial.getNotes().toArray(new String[0]));
        elements.addString(trial.getTestbedVersion());
        elements.addString(trial.getMapName());
        elements.addString(trial.getMapBlockFilename());
        String clientInfo = "[]";
        try {
            clientInfo = objectMapper.writeValueAsString(trial.getClientInfo() != null ? trial.getClientInfo() : new ArrayList<ClientInfoItem>());
        } catch (JsonProcessingException e) {
            logger.error(e.getMessage());
            return null;
        }
        elements.addValue(io.vertx.core.json.Json.decodeValue(clientInfo));
        elements.addString(trial.getTeamId());
        elements.addArrayOfString(trial.getInterventionAgents().toArray(new String[0]));
        elements.addArrayOfString(trial.getObservers().toArray(new String[0]));
        elements.addUUID(UUID.fromString(trial.getExperiment().getExperimentId()));
        elements.addInteger((int) trial.getId());
        try {
            RowSet<Row> rowSet = client.preparedQuery(sqlQuery).rxExecute(elements).blockingGet();
            RowIterator<Row> rowIterator = rowSet.iterator();
            if (rowIterator.hasNext()) {
                logger.info("1 row(s) affected.");
                Row row = rowIterator.next();
                Long _id = row.getLong("id");
                logger.info(MessageFormat.format("id returned: {0}.", _id));
                if (trial.getId() == _id) {
                    return trial;
                } else {
                    logger.error(MessageFormat.format("id returned: {0} does not match trial id.", trial.getId()));
                    return null;
                }
            } else {
                logger.info("No trials found!");
                return null;
            }
        } catch (RuntimeException e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    @Override
    public boolean deleteTrial(long id) {
        try {
            RowSet<Row> rowSet = client.preparedQuery("DELETE FROM trials WHERE id = $1").rxExecute(Tuple.of((int) id))
                    .blockingGet();
            if (rowSet.rowCount() > 0) {
                logger.info(MessageFormat.format("{0} row(s) affected.", rowSet.rowCount()));
                return true;
            } else {
                logger.error(MessageFormat.format("Trial with id: {0} was not deleted!", id));
                return false;
            }

        } catch (RuntimeException e) {
            logger.error(e.getMessage());
            return false;
        }
    }

    @Override
    @NonNull
    public SystemFile exportFile(@NonNull @NotNull String trialId, @NonNull @NotNull String index) {
        logger.info(MessageFormat.format("Exporting temporary file using trial [{0}] from index {1}.", trialId, index));
        try {
            Trial trial = this.readTrialUUID(trialId);
            if (trial == null) {
                logger.error(MessageFormat.format("Export aborted! No trial found with id: [{0}].", trialId));
                return null;
            }
//			String HEADER_METADATA_FILE_PREFIX = MessageFormat.format(
//					"TrialMessages_CondBtwn-{0}_CondWin-{1}-StaticMap_Trial-{2}_Team-na_Member-{3}_Vers-{4}",
//					trial.getCondition(), trial.getExperiment().getMission(), trial.getTrialNumber(),
//					String.join("-", trial.getSubjects()), trial.getTestbedVersion());
            String HEADER_METADATA_FILE_PREFIX = MessageFormat.format(
                    "{0}",
                    trial.getTrialId());
            String HEADER_METADATA_FILENAME = HEADER_METADATA_FILE_PREFIX + HEADER_METADATA_FILE_SUFIX;
            File file = File.createTempFile(HEADER_METADATA_FILE_PREFIX, HEADER_METADATA_FILE_SUFIX);
            logger.info(MessageFormat.format("File name: {0}.", HEADER_METADATA_FILENAME));
            PrintWriter printWriter = new PrintWriter(new FileWriter(file));

            // Add metadata json file header.
            MessageTrialExport messageTrialExport = MessageTrialExport.generate(trial, index, "STARTED", 0);
            printWriter.println(objectMapper.writeValueAsString(messageTrialExport));

            final Scroll scroll = new Scroll(TimeValue.timeValueMinutes(30L));
            SearchRequest searchRequest = new SearchRequest(index).scroll(scroll);

            BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();

            boolQueryBuilder.must(QueryBuilders.matchQuery("msg.trial_id.keyword", trialId));
            boolQueryBuilder.mustNot(QueryBuilders.existsQuery("msg.replay_id"));

            SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
            searchSourceBuilder.size(1000);
            searchSourceBuilder.fetchSource(null, "message");
            searchSourceBuilder.sort("@timestamp", SortOrder.ASC);
            searchSourceBuilder.query(boolQueryBuilder);

            searchRequest.source(searchSourceBuilder);

            SearchResponse searchResponse = elasticsearchClient.search(searchRequest, RequestOptions.DEFAULT);

            String scrollId = searchResponse.getScrollId();

            TotalHits totalHits = searchResponse.getHits().getTotalHits();
            totalMessageCount = totalHits.value;
            SearchHit[] searchHits = searchResponse.getHits().getHits();

            List<Instant> lastInstant = new ArrayList<Instant>();
            lastInstant.add(null);
            // List<Integer> messageSourceCount = new ArrayList<Integer>();
            // messageSourceCount.add(0);
            // List<Integer> messageDestinationCount = new ArrayList<Integer>();
            // messageDestinationCount.add(1);
            currentMessageCount = 0;
            while (searchHits != null && searchHits.length > 0) {
                // messageSourceCount.set(0, messageSourceCount.get(0) + searchHits.length);
                // Arrays.stream(searchHits).forEach(searchHit -> {
                for (SearchHit searchHit : searchHits) {
                    currentMessageCount = currentMessageCount + 1;
                    String source = searchHit.getSourceAsString();
                    printWriter.println(source);
                }
                SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
                scrollRequest.scroll(scroll);
                searchResponse = elasticsearchClient.scroll(scrollRequest, RequestOptions.DEFAULT);
                scrollId = searchResponse.getScrollId();
                searchHits = searchResponse.getHits().getHits();
            }

            ClearScrollRequest clearScrollRequest = new ClearScrollRequest();
            clearScrollRequest.addScrollId(scrollId);
            ClearScrollResponse clearScrollResponse = elasticsearchClient.clearScroll(clearScrollRequest, RequestOptions.DEFAULT);

            printWriter.close();

            logger.info(MessageFormat.format("Exported {0} documents successfully.", currentMessageCount));
            return new SystemFile(file).attach(HEADER_METADATA_FILENAME);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        throw new HttpStatusException(HttpStatus.SERVICE_UNAVAILABLE, "error exporting trial");
    }

    @Override
    @NonNull
    public StreamedFile exportStreamed(@NonNull @NotNull String trialId, @NonNull @NotNull String index) {
        logger.info(MessageFormat.format("Exporting streamed file using trial [{0}] from index {1}.", trialId, index));
        PipedInputStream pipedInputStream = new PipedInputStream();
        PipedOutputStream pipedOutputStream = new PipedOutputStream();
        Trial trial = this.readTrialUUID(trialId);
        if (trial == null) {
            logger.error(MessageFormat.format("Export aborted! No trial found with id: [{0}].", trialId));
            return null;
        }
        String HEADER_METADATA_FILE_PREFIX = MessageFormat.format(
                "TrialMessages_CondBtwn-{0}_CondWin-{1}-StaticMap_Trial-{2}_Team-na_Member-{3}_Vers-{4}",
                trial.getCondition(), trial.getExperiment().getMission(), trial.getTrialNumber(),
                String.join("-", trial.getSubjects()), trial.getTestbedVersion());
        String HEADER_METADATA_FILENAME = HEADER_METADATA_FILE_PREFIX + HEADER_METADATA_FILE_SUFIX;
        logger.info(MessageFormat.format("File name: {0}.", HEADER_METADATA_FILENAME));
        executorService.execute(() -> {
            try {
                pipedOutputStream.connect(pipedInputStream);

                PrintWriter printWriter = new PrintWriter(pipedOutputStream);

                // Add metadata json file header.
                MessageTrialExport messageTrialExport = MessageTrialExport.generate(trial, index, "STARTED", 0);
                printWriter.println(objectMapper.writeValueAsString(messageTrialExport));

                final Scroll scroll = new Scroll(TimeValue.timeValueMinutes(30L));
                SearchRequest searchRequest = new SearchRequest(index).scroll(scroll);

                BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();

                boolQueryBuilder.must(QueryBuilders.matchQuery("msg.trial_id.keyword", trialId));
                boolQueryBuilder.mustNot(QueryBuilders.existsQuery("msg.replay_id"));

                SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
                searchSourceBuilder.size(1000);
                searchSourceBuilder.fetchSource(null, "message");
                searchSourceBuilder.sort("@timestamp", SortOrder.ASC);
                searchSourceBuilder.query(boolQueryBuilder);

                searchRequest.source(searchSourceBuilder);

                SearchResponse searchResponse = elasticsearchClient.search(searchRequest, RequestOptions.DEFAULT);

                String scrollId = searchResponse.getScrollId();

                TotalHits totalHits = searchResponse.getHits().getTotalHits();
                totalMessageCount = totalHits.value;
                SearchHit[] searchHits = searchResponse.getHits().getHits();

                List<Instant> lastInstant = new ArrayList<Instant>();
                lastInstant.add(null);
                // List<Integer> messageSourceCount = new ArrayList<Integer>();
                // messageSourceCount.add(0);
                // List<Integer> messageDestinationCount = new ArrayList<Integer>();
                // messageDestinationCount.add(1);
                currentMessageCount = 0;
                while (searchHits != null && searchHits.length > 0) {
                    // messageSourceCount.set(0, messageSourceCount.get(0) + searchHits.length);
                    // Arrays.stream(searchHits).forEach(searchHit -> {
                    for (SearchHit searchHit : searchHits) {
                        currentMessageCount = currentMessageCount + 1;
                        String source = searchHit.getSourceAsString();
                        printWriter.println(source);
                    }
                    SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
                    scrollRequest.scroll(scroll);
                    searchResponse = elasticsearchClient.scroll(scrollRequest, RequestOptions.DEFAULT);
                    scrollId = searchResponse.getScrollId();
                    searchHits = searchResponse.getHits().getHits();
                }

                ClearScrollRequest clearScrollRequest = new ClearScrollRequest();
                clearScrollRequest.addScrollId(scrollId);
                ClearScrollResponse clearScrollResponse = elasticsearchClient.clearScroll(clearScrollRequest, RequestOptions.DEFAULT);

                printWriter.close();
                pipedOutputStream.flush();
                pipedOutputStream.close();
                logger.info(MessageFormat.format("Exported {0} documents successfully.", currentMessageCount));
            } catch (IOException e) {
                logger.error(e.getMessage());
            }

        });
        return new StreamedFile(pipedInputStream, MediaType.TEXT_PLAIN_TYPE).attach(HEADER_METADATA_FILENAME);
    }

    @Override
    public MessageApiResult importFile(@NonNull @NotNull byte[] bytes, @NonNull @NotNull String filename, @NonNull @NotNull String index, @NonNull @NotNull boolean createIndex) {
        logger.info(MessageFormat.format("Importing {0} into index {1} [creating: {2}].", filename, index, createIndex));
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        try {
            // Read in the file
            inputStream = new ByteArrayInputStream(bytes);
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            // Create the index
            if (createIndex) {
                logger.info(MessageFormat.format("Creating index {0}.", index));
                GetIndexRequest getIndexRequest = new GetIndexRequest(index);
                boolean exists = elasticsearchClient.indices().exists(getIndexRequest, RequestOptions.DEFAULT);
                if (!exists) {
                    CreateIndexRequest createIndexRequest = new CreateIndexRequest(index);
                    createIndexRequest.settings(Settings.builder()
                            .put("index.lifecycle.name", "logstash-policy")
                            .put("index.lifecycle.rollover_alias", "logstash")
                            .put("index.refresh_interval", "5s")
                    );
                    createIndexRequest.mapping("{\r\n"
                                    + "  \"dynamic\": false,\r\n"
                                    + "  \"properties\": {\r\n"
                                    + "    \"header\": {\r\n"
                                    + "      \"properties\": {\r\n"
                                    + "        \"message_type\": {\r\n"
                                    + "          \"type\": \"text\",\r\n"
                                    + "          \"norms\": false,\r\n"
                                    + "          \"fields\": {\r\n"
                                    + "            \"keyword\": {\r\n"
                                    + "              \"type\": \"keyword\",\r\n"
                                    + "              \"ignore_above\": 256\r\n"
                                    + "            }\r\n"
                                    + "          }\r\n"
                                    + "        }\r\n"
                                    + "      }\r\n"
                                    + "    },\r\n"
                                    + "    \"msg\": {\r\n"
                                    + "      \"properties\": {\r\n"
                                    + "        \"trial_id\": {\r\n"
                                    + "          \"type\": \"text\",\r\n"
                                    + "          \"norms\": false,\r\n"
                                    + "          \"fields\": {\r\n"
                                    + "            \"keyword\": {\r\n"
                                    + "              \"type\": \"keyword\",\r\n"
                                    + "              \"ignore_above\": 256\r\n"
                                    + "            }\r\n"
                                    + "          }\r\n"
                                    + "        },\r\n"
                                    + "        \"experiment_id\": {\r\n"
                                    + "          \"type\": \"text\",\r\n"
                                    + "          \"norms\": false,\r\n"
                                    + "          \"fields\": {\r\n"
                                    + "            \"keyword\": {\r\n"
                                    + "              \"type\": \"keyword\",\r\n"
                                    + "              \"ignore_above\": 256\r\n"
                                    + "            }\r\n"
                                    + "          }\r\n"
                                    + "        },\r\n"
                                    + "        \"replay_id\": {\r\n"
                                    + "          \"type\": \"text\",\r\n"
                                    + "          \"norms\": false,\r\n"
                                    + "          \"fields\": {\r\n"
                                    + "            \"keyword\": {\r\n"
                                    + "              \"type\": \"keyword\",\r\n"
                                    + "              \"ignore_above\": 256\r\n"
                                    + "            }\r\n"
                                    + "          }\r\n"
                                    + "        },\r\n"
                                    + "        \"replay_parent_id\": {\r\n"
                                    + "          \"type\": \"text\",\r\n"
                                    + "          \"norms\": false,\r\n"
                                    + "          \"fields\": {\r\n"
                                    + "            \"keyword\": {\r\n"
                                    + "              \"type\": \"keyword\",\r\n"
                                    + "              \"ignore_above\": 256\r\n"
                                    + "            }\r\n"
                                    + "          }\r\n"
                                    + "        },\r\n"
                                    + "        \"replay_parent_type\": {\r\n"
                                    + "          \"type\": \"text\",\r\n"
                                    + "          \"norms\": false,\r\n"
                                    + "          \"fields\": {\r\n"
                                    + "            \"keyword\": {\r\n"
                                    + "              \"type\": \"keyword\",\r\n"
                                    + "              \"ignore_above\": 256\r\n"
                                    + "            }\r\n"
                                    + "          }\r\n"
                                    + "        },\r\n"
                                    + "        \"source\": {\r\n"
                                    + "          \"type\": \"text\",\r\n"
                                    + "          \"norms\": false,\r\n"
                                    + "          \"fields\": {\r\n"
                                    + "            \"keyword\": {\r\n"
                                    + "              \"type\": \"keyword\",\r\n"
                                    + "              \"ignore_above\": 256\r\n"
                                    + "            }\r\n"
                                    + "          }\r\n"
                                    + "        },\r\n"
                                    + "        \"sub_type\": {\r\n"
                                    + "          \"type\": \"text\",\r\n"
                                    + "          \"norms\": false,\r\n"
                                    + "          \"fields\": {\r\n"
                                    + "            \"keyword\": {\r\n"
                                    + "              \"type\": \"keyword\",\r\n"
                                    + "              \"ignore_above\": 256\r\n"
                                    + "            }\r\n"
                                    + "          }\r\n"
                                    + "        },\r\n"
                                    + "        \"timestamp\": {\r\n"
                                    + "          \"type\": \"date\"\r\n"
                                    + "        },\r\n"
                                    + "        \"name\": {\r\n"
                                    + "          \"type\": \"text\",\r\n"
                                    + "          \"norms\": false,\r\n"
                                    + "          \"fields\": {\r\n"
                                    + "            \"keyword\": {\r\n"
                                    + "              \"type\": \"keyword\",\r\n"
                                    + "              \"ignore_above\": 256\r\n"
                                    + "            }\r\n"
                                    + "          }\r\n"
                                    + "        },\r\n"
                                    + "        \"testbed_version\": {\r\n"
                                    + "          \"type\": \"text\",\r\n"
                                    + "          \"norms\": false,\r\n"
                                    + "          \"fields\": {\r\n"
                                    + "            \"keyword\": {\r\n"
                                    + "              \"type\": \"keyword\",\r\n"
                                    + "              \"ignore_above\": 256\r\n"
                                    + "            }\r\n"
                                    + "          }\r\n"
                                    + "        }\r\n"
                                    + "      }\r\n"
                                    + "    },\r\n"
                                    + "    \"topic\": {\r\n"
                                    + "      \"type\": \"text\",\r\n"
                                    + "      \"norms\": false,\r\n"
                                    + "      \"fields\": {\r\n"
                                    + "        \"keyword\": {\r\n"
                                    + "          \"type\": \"keyword\",\r\n"
                                    + "          \"ignore_above\": 256\r\n"
                                    + "        }\r\n"
                                    + "      }\r\n"
                                    + "    },\r\n"
                                    + "    \"@timestamp\": {\r\n"
                                    + "      \"type\": \"date\"\r\n"
                                    + "    },\r\n"
                                    + "    \"@version\": {\r\n"
                                    + "      \"type\": \"keyword\"\r\n"
                                    + "    },\r\n"
                                    + "    \"error\": {\r\n"
                                    + "      \"properties\": {\r\n"
                                    + "        \"reason\": {\r\n"
                                    + "          \"type\": \"text\",\r\n"
                                    + "          \"norms\": false,\r\n"
                                    + "          \"fields\": {\r\n"
                                    + "            \"keyword\": {\r\n"
                                    + "              \"type\": \"keyword\",\r\n"
                                    + "              \"ignore_above\": 256\r\n"
                                    + "            }\r\n"
                                    + "          }\r\n"
                                    + "        },\r\n"
                                    + "        \"plugin_id\": {\r\n"
                                    + "          \"type\": \"text\",\r\n"
                                    + "          \"norms\": false,\r\n"
                                    + "          \"fields\": {\r\n"
                                    + "            \"keyword\": {\r\n"
                                    + "              \"type\": \"keyword\",\r\n"
                                    + "              \"ignore_above\": 256\r\n"
                                    + "            }\r\n"
                                    + "          }\r\n"
                                    + "        },\r\n"
                                    + "        \"plugin_type\": {\r\n"
                                    + "          \"type\": \"text\",\r\n"
                                    + "          \"norms\": false,\r\n"
                                    + "          \"fields\": {\r\n"
                                    + "            \"keyword\": {\r\n"
                                    + "              \"type\": \"keyword\",\r\n"
                                    + "              \"ignore_above\": 256\r\n"
                                    + "            }\r\n"
                                    + "          }\r\n"
                                    + "        }\r\n"
                                    + "      }\r\n"
                                    + "    }\r\n"
                                    + "  }\r\n"
                                    + "}",
                            XContentType.JSON);
                    CreateIndexResponse createIndexResponse = elasticsearchClient.indices().create(createIndexRequest, RequestOptions.DEFAULT);
                    boolean acknowledged = createIndexResponse.isAcknowledged();
                    logger.info(MessageFormat.format("Create index response acknowledged: {0}.", acknowledged));
                } else {
                    logger.info(MessageFormat.format("Index [{0}] could not be created because it already exists!.", index));
                }
            }

            // Prepare bulk insert by adding json documents from the file to the request.
            BulkRequest bulkRequest = new BulkRequest();
            int line = 0;
            while (bufferedReader.ready()) {
                String json = bufferedReader.readLine();
                // If there is a header then process it.
                if (line == 0) {
                    MessageTrialExport messageTrialExport = objectMapper.readValue(json, MessageTrialExport.class);

                    // Check to see if the trialId is already present in elasticsearch.
                    String trialId = messageTrialExport.getMsg().getTrialId();
                    // Check to see if index already has a document with this trial id.
                    SearchRequest searchRequest = new SearchRequest(index);

                    MatchQueryBuilder matchQueryBuilder = QueryBuilders.matchQuery("msg.trial_id.keyword", trialId);

                    SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
                    searchSourceBuilder.size(0);
                    searchSourceBuilder.fetchSource(null, "message");
                    searchSourceBuilder.query(matchQueryBuilder);

                    searchRequest.source(searchSourceBuilder);

                    SearchResponse searchResponse = elasticsearchClient.search(searchRequest, RequestOptions.DEFAULT);

                    TotalHits totalHits = searchResponse.getHits().getTotalHits();
                    totalMessageCount = totalHits.value;

                    if (totalMessageCount > 0) {
                        return new MessageApiResult("failure", MessageFormat.format("Imported aborted! Trial: [{0}] already exists in index: [{1}]", trialId, index), new HashMap<String, String>());
                    }

                    // Create experiment
                    Experiment experiment = new Experiment(
                            -1,
                            messageTrialExport.getMsg().getExperimentId(),
                            messageTrialExport.getData().getMetadata().getTrial().getExperimentName(),
                            messageTrialExport.getData().getMetadata().getTrial().getExperimentDate(),
                            messageTrialExport.getData().getMetadata().getTrial().getExperimentAuthor(),
                            messageTrialExport.getData().getMetadata().getTrial().getExperimentMission(),
                            messageTrialExport.getData().getMetadata().getTrial().getCondition()
                    );
                    logger.info(MessageFormat.format("Creating experiment [{0}] found in header.", messageTrialExport.getMsg().getExperimentId()));
                    defaultExperimentService.createExperiment(experiment);
                    // Create trial
                    logger.info(MessageFormat.format("Creating trial [{0}] found in header.", messageTrialExport.getMsg().getTrialId()));
                    createTrial(new Trial(
                            -1,
                            messageTrialExport.getMsg().getTrialId(),
                            messageTrialExport.getData().getMetadata().getTrial().getName(),
                            messageTrialExport.getData().getMetadata().getTrial().getDate(),
                            messageTrialExport.getData().getMetadata().getTrial().getExperimenter(),
                            messageTrialExport.getData().getMetadata().getTrial().getSubjects(),
                            messageTrialExport.getData().getMetadata().getTrial().getTrialNumber(),
                            messageTrialExport.getData().getMetadata().getTrial().getGroupNumber(),
                            messageTrialExport.getData().getMetadata().getTrial().getStudyNumber(),
                            messageTrialExport.getData().getMetadata().getTrial().getCondition(),
                            messageTrialExport.getData().getMetadata().getTrial().getNotes(),
                            messageTrialExport.getData().getMetadata().getTrial().getTestbedVersion(),
                            experiment,
                            messageTrialExport.getData().getMetadata().getTrial().getMapName(),
                            messageTrialExport.getData().getMetadata().getTrial().getMapBlockFilename(),
                            messageTrialExport.getData().getMetadata().getTrial().getClientInfo(),
                            messageTrialExport.getData().getMetadata().getTrial().getTeamId(),
                            messageTrialExport.getData().getMetadata().getTrial().getInterventionAgents(),
                            messageTrialExport.getData().getMetadata().getTrial().getObservers()
                    ));
                } else {
                    IndexRequest indexRequest = new IndexRequest(index).source(json, XContentType.JSON);
                    bulkRequest.add(indexRequest);
                }
                line++;
            }
            logger.info(MessageFormat.format("Bulk indexing [{0}] documents found in file [total estimated size: {1} MB ].", bulkRequest.requests().size(), bulkRequest.estimatedSizeInBytes() / (1024 * 1024)));
            BulkResponse bulkResponse = elasticsearchClient.bulk(bulkRequest, RequestOptions.DEFAULT);
            int errorCount = 0;
            BulkItemResponse[] bulkItemResponse = bulkResponse.getItems();
            if (bulkResponse.hasFailures()) {
                for (int i = 0; i < bulkItemResponse.length; i++) {
                    if (bulkItemResponse[i].isFailed()) {
                        errorCount++;
                        BulkItemResponse.Failure failure = bulkItemResponse[i].getFailure();
                        logger.error(failure.getMessage());
                    }

                }
            }
            logger.info(MessageFormat.format("Imported {0} out of {1} documents with {2} errors.", bulkItemResponse.length - errorCount, bulkItemResponse.length, errorCount));
            return new MessageApiResult("success", MessageFormat.format("Imported {0} out of {1} documents with {2} errors.", bulkItemResponse.length - errorCount, bulkItemResponse.length, errorCount), new HashMap<String, String>());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            logger.error(e.getMessage());
            return new MessageApiResult("failure", e.getMessage(), new HashMap<String, String>());
        }
    }

    public void simulateTrial(@NonNull byte[] bytes, @NonNull @NotNull String filename) {
        try {
            InputStream inputStream = new ByteArrayInputStream(bytes);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            List<Instant> lastInstant = new ArrayList<Instant>();
            lastInstant.add(null);

            int line = 0;
            while (bufferedReader.ready()) {
                String json = bufferedReader.readLine();
                JsonNode sourceMap = objectMapper.readTree(json);
                if (sourceMap.has("@timestamp")) {
                    String timestamp = sourceMap.get("@timestamp").asText();
                    String topic = sourceMap.get("topic").asText();
                    Map<String, Object> message = new HashMap<String, Object>();
                    String strHeader = objectMapper.writeValueAsString(sourceMap.get("header"));
                    Header header = objectMapper.readValue(strHeader, Header.class);
                    message.put("header", header);
                    String strMsg = objectMapper.writeValueAsString(sourceMap.get("msg"));
                    Msg msg = objectMapper.readValue(strMsg, Msg.class);
                    msg.setReplayId(null);
                    msg.setReplayParentId(null);
                    msg.setReplayParentType(null);
                    message.put("msg", msg);
                    message.put("data", sourceMap.get("data"));

                    Instant instant = Instant.parse(timestamp);
                    long delay = ChronoUnit.MILLIS.between(lastInstant.get(0) != null ? lastInstant.get(0) : instant, instant);
                    if (delay > 15000)
                        delay = 15000;
                    lastInstant.set(0, instant);
//                currentReplayThread = Thread.currentThread();
                    if (delay < 0) {
                        delay = 0;
                    }
                    Thread.sleep(delay);

                    replayMessagePublisher.send(objectMapper.writeValueAsBytes(message), topic, 2)
                            .subscribe(() -> {
                                // handle completion
                                logger.trace(MessageFormat.format("{0} [{1}] : {2}", timestamp, topic,
                                        objectMapper.writeValueAsString(message)));
                                // messageDestinationCount.set(0, messageDestinationCount.get(0) + 1);
                            }, throwable -> {
                                logger.error(throwable.getMessage());
                            });
                } else {
                    logger.trace("Skipping message.");
                }
                line++;
            }
            bufferedReader.close();
//            inputStream.close();;
        } catch (IOException | InterruptedException e) {
            logger.error(e.getMessage());
        }
    }


        public boolean existElasticsearch(String id, String index) {
        // Check to see if the trialId is already present in elasticsearch.
        long totalMessageCount = 0;
        // Check to see if index already has a document with this trial id.
        SearchRequest searchRequest = new SearchRequest(index);

        MatchQueryBuilder matchQueryBuilder = QueryBuilders.matchQuery("msg.trial_id.keyword", id);

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.size(0);
        searchSourceBuilder.fetchSource(null, "message");
        searchSourceBuilder.query(matchQueryBuilder);

        searchRequest.source(searchSourceBuilder);

        try {
            SearchResponse searchResponse;
            searchResponse = elasticsearchClient.search(searchRequest, RequestOptions.DEFAULT);

            TotalHits totalHits = searchResponse.getHits().getTotalHits();
            totalMessageCount = totalHits.value;

        } catch (IOException e) {
            logger.error(e.getMessage());
        }


        if (totalMessageCount > 0) {
            return true;
        } else {
            return false;
        }
    }
}
