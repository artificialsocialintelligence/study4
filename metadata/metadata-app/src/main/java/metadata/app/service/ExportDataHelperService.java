package metadata.app.service;

import metadata.app.model.TrialStopOptions;

public interface ExportDataHelperService {
    boolean autoExport(TrialStopOptions trialStopEventOptions);
}
