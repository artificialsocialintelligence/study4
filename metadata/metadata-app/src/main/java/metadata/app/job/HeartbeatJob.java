package metadata.app.job;

import java.text.MessageFormat;

import metadata.app.model.*;
import org.joda.time.Instant;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.protobuf.Message;

import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.scheduling.annotation.Scheduled;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import metadata.app.client.HeartbeatClient;
import metadata.app.publisher.HeartbeatPublisher;
import metadata.app.service.DefaultTrialService;

@Singleton
public class HeartbeatJob {
	private static final Logger logger = LoggerFactory.getLogger(HeartbeatJob.class);
	private ObjectMapper objectMapper = new ObjectMapper();
	
	@Inject
	private HeartbeatPublisher heartbeatPublisher;
	
	@Inject
    HeartbeatClient heartbeatClient;
	
	@Scheduled(fixedDelay = "${asist.heartbeatInitialDelay}", initialDelay = "${asist.heartbeatInterval}") 
    void execute() {
		String health = heartbeatClient.health();
		try {
			JsonNode healthObj = objectMapper.readTree(health);
			String status = healthObj.get("status").textValue();
			Header header = new Header(Instant.now().toString(),"status", "0.1");
			Msg msg = new Msg("heartbeat", "metadata-app", null, null, "0.3", null, null, null);
			DataHeartbeat dataHeartbeat = new DataHeartbeat(HeartbeatStateType.ok);
			MessageHeartbeat messageHeartbeat = new MessageHeartbeat(header, msg, dataHeartbeat);
			if (status.equals("UP")) {
				logger.trace(MessageFormat.format("Heartbeat: {0}", objectMapper.writeValueAsString(messageHeartbeat)));
				heartbeatPublisher.send(objectMapper.writeValueAsBytes(messageHeartbeat)).subscribe(() -> {
					// handle completion
				}, throwable -> {
					// handle error
				});
			}

		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}
}
