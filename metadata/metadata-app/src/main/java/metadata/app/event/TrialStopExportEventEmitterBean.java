package metadata.app.event;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;

import io.micronaut.context.event.ApplicationEventPublisher;
import metadata.app.model.MessageTrial;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Singleton
public class TrialStopExportEventEmitterBean {
    private static final Logger logger = LoggerFactory.getLogger(TrialStopExportEventEmitterBean.class);
    @Inject
    ApplicationEventPublisher<TrialStopExportEvent> eventPublisher;

    public void publishTrialStopExportEvent(MessageTrial msgTrial) {
        try {
            eventPublisher.publishEvent(new TrialStopExportEvent(msgTrial));
        } catch(Exception e) {
            logger.info(e.getMessage());
        }
    }

}
