package metadata.app.event;

import java.nio.file.Path;

public class TrialStopUploadEvent {

	private final Path path;
	private final String id;

	public TrialStopUploadEvent(Path path, String id) {
		this.path = path;
		this.id = id;
	}

    public Path getPath() {
        return path;
    }

	public String getId() {
		return id;
	}

}
