package metadata.app.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AgentLog {

	private String name;
	private String folder;
	private String wildcardDirFilter;

	@JsonCreator
	public AgentLog(
			@JsonProperty("name") String messageType,
			@JsonProperty("folder") String subType,
			@JsonProperty("wildcard_dir_filter") String comparison
			) {
		this.name = messageType;
        this.folder = subType;
        this.wildcardDirFilter = comparison;
    }

	@JsonProperty("name")
	public String getName() {
		return name;
	}
	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}
	@JsonProperty("folder")
	public String getFolder() {
		return folder;
	}
	@JsonProperty("folder")
	public void setFolder(String folder) {
		this.folder = folder;
	}
	@JsonProperty("wildcard_dir_filter")
	public String getWildcardDirFilter() {
		return wildcardDirFilter;
	}
	@JsonProperty("wildcard_dir_filter")
	public void setWildcardDirFilter(String wildcardDirFilter) {
		this.wildcardDirFilter = wildcardDirFilter;
	}
}
