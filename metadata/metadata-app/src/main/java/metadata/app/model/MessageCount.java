package metadata.app.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class MessageCount {

	private String messageType;
	private String subType;
	private int count;
	
	@JsonCreator
	public MessageCount(
			@JsonProperty("message_type") String messageType,
			@JsonProperty("sub_type") String subType,
			@JsonProperty("count") int count
			) {
		this.messageType = messageType;
        this.subType = subType;
        this.count = count;   
    }

	@JsonProperty("message_type")
	public String getMessageType() {
		return messageType;
	}
	@JsonProperty("message_type")
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	@JsonProperty("sub_type")
	public String getSubType() {
		return subType;
	}
	@JsonProperty("sub_type")
	public void setSubType(String subType) {
		this.subType = subType;
	}
	@JsonProperty("count")
	public int getCount() {
		return count;
	}
	@JsonProperty("comparison")
	public void setCoount(int count) {
		this.count = count;
	}	
}