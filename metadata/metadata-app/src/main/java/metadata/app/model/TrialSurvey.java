package metadata.app.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class TrialSurvey {
    @JsonProperty(value="survey_id")
    private String surveyId;
    @JsonProperty(value="completedTimestamp")
    private String completedTimestamp;
    @JsonProperty(value="complete")
    private String complete;
    @JsonProperty(value="survey_name")
    private String surveyName;
    @JsonProperty(value="data")
    private List<TrialSurveyData> data;

    public String getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(String surveyId) {
        this.surveyId = surveyId;
    }

    public String getCompletedTimestamp() {
        return completedTimestamp;
    }

    public void setCompletedTimestamp(String completedTimestamp) {
        this.completedTimestamp = completedTimestamp;
    }

    public String getComplete() {
        return complete;
    }

    public void setComplete(String complete) {
        this.complete = complete;
    }

    public String getSurveyName() {
        return surveyName;
    }

    public void setSurveyName(String survey_name) {
        this.surveyName = survey_name;
    }

    public List<TrialSurveyData> getData() {
        return data;
    }

    public void setData(List<TrialSurveyData> data) {
        this.data = data;
    }
}
