package metadata.app.model;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;
import io.micronaut.core.annotation.Introspected;

import java.text.MessageFormat;

@Introspected
public class MeasuresIndividual {
    @CsvBindByName(column = "PLAYER_ID")
    @CsvBindByPosition(position = 0)
    private final String playerId;

    @CsvBindByName(column = "DEMO-1")
    @CsvBindByPosition(position = 1)
    private String demo_1;

    @CsvBindByName(column = "DEMO-2")
    @CsvBindByPosition(position = 2)
    private String demo_2;

    @CsvBindByName(column = "DEMO-3")
    @CsvBindByPosition(position = 3)
    private String demo_3;

    @CsvBindByName(column = "DEMO-4")
    @CsvBindByPosition(position = 4)
    private String demo_4;

    @CsvBindByName(column = "DEMO-5")
    @CsvBindByPosition(position = 5)
    private String demo_5;

    @CsvBindByName(column = "DEMO-6")
    @CsvBindByPosition(position = 6)
    private String demo_6;

    @CsvBindByName(column = "DEMO-7")
    @CsvBindByPosition(position = 7)
    private String demo_7;

    @CsvBindByName(column = "PSY_COL-1")
    @CsvBindByPosition(position = 8)
    private String psyCol_1;

    @CsvBindByName(column = "PSY_COL-2")
    @CsvBindByPosition(position = 9)
    private String psyCol_2;

    @CsvBindByName(column = "PSY_COL-3")
    @CsvBindByPosition(position = 10)
    private String psyCol_3;

    @CsvBindByName(column = "PSY_COL-4")
    @CsvBindByPosition(position = 11)
    private String psyCol_4;

    @CsvBindByName(column = "PSY_COL-5")
    @CsvBindByPosition(position = 12)
    private String psyCol_5;

    @CsvBindByName(column = "PSY_COL-6")
    @CsvBindByPosition(position = 13)
    private String psyCol_6;

    @CsvBindByName(column = "PSY_COL-7")
    @CsvBindByPosition(position = 14)
    private String psyCol_7;

    @CsvBindByName(column = "PSY_COL-8")
    @CsvBindByPosition(position = 15)
    private String psyCol_8;

    @CsvBindByName(column = "PSY_COL-9")
    @CsvBindByPosition(position = 16)
    private String psyCol_9;

    @CsvBindByName(column = "PSY_COL-10")
    @CsvBindByPosition(position = 17)
    private String psyCol_10;

    @CsvBindByName(column = "PSY_COL-11")
    @CsvBindByPosition(position = 18)
    private String psyCol_11;

    @CsvBindByName(column = "PSY_COL-12")
    @CsvBindByPosition(position = 19)
    private String psyCol_12;

    @CsvBindByName(column = "PSY_COL-13")
    @CsvBindByPosition(position = 20)
    private String psyCol_13;

    @CsvBindByName(column = "PSY_COL-14")
    @CsvBindByPosition(position = 21)
    private String psyCol_14;

    @CsvBindByName(column = "PSY_COL-15")
    @CsvBindByPosition(position = 22)
    private String psyCol_15;

    @CsvBindByName(column = "MC_PROF_15-1")
    @CsvBindByPosition(position = 23)
    private String mcProf15_1;

    @CsvBindByName(column = "MC_PROF_15-2")
    @CsvBindByPosition(position = 24)
    private String mcProf15_2;

    @CsvBindByName(column = "MC_PROF_15-3")
    @CsvBindByPosition(position = 25)
    private String mcProf15_3;

    @CsvBindByName(column = "MC_PROF_15-4")
    @CsvBindByPosition(position = 26)
    private String mcProf15_4;

    @CsvBindByName(column = "MC_PROF_15-5")
    @CsvBindByPosition(position = 27)
    private String mcProf15_5;

    @CsvBindByName(column = "MC_PROF_15-6")
    @CsvBindByPosition(position = 28)
    private String mcProf15_6;

    @CsvBindByName(column = "MC_PROF_15-7")
    @CsvBindByPosition(position = 29)
    private String mcProf15_7;

    @CsvBindByName(column = "MC_PROF_15-8")
    @CsvBindByPosition(position = 30)
    private String mcProf15_8;

    @CsvBindByName(column = "MC_PROF_15-9")
    @CsvBindByPosition(position = 31)
    private String mcProf15_9;

    @CsvBindByName(column = "MC_PROF_15-10")
    @CsvBindByPosition(position = 32)
    private String mcProf15_10;

    @CsvBindByName(column = "MC_PROF_15-11")
    @CsvBindByPosition(position = 33)
    private String mcProf15_11;

    @CsvBindByName(column = "MC_PROF_15-12")
    @CsvBindByPosition(position = 34)
    private String mcProf15_12;

    @CsvBindByName(column = "MC_PROF_15-13")
    @CsvBindByPosition(position = 35)
    private String mcProf15_13;

    @CsvBindByName(column = "MC_PROF_15-14")
    @CsvBindByPosition(position = 36)
    private String mcProf15_14;

    @CsvBindByName(column = "MC_PROF_15-15")
    @CsvBindByPosition(position = 37)
    private String mcProf15_15;

    @CsvBindByName(column = "SOC_DOM-1")
    @CsvBindByPosition(position = 38)
    private String socDom_1;

    @CsvBindByName(column = "SOC_DOM-2")
    @CsvBindByPosition(position = 39)
    private String socDom_2;

    @CsvBindByName(column = "SOC_DOM-3")
    @CsvBindByPosition(position = 40)
    private String socDom_3;

    @CsvBindByName(column = "SOC_DOM-4")
    @CsvBindByPosition(position = 41)
    private String socDom_4;

    @CsvBindByName(column = "SOC_DOM-5")
    @CsvBindByPosition(position = 42)
    private String socDom_5;

    @CsvBindByName(column = "SOC_DOM-6")
    @CsvBindByPosition(position = 43)
    private String socDom_6;

    @CsvBindByName(column = "SOC_DOM-7")
    @CsvBindByPosition(position = 44)
    private String socDom_7;

    @CsvBindByName(column = "SOC_DOM-8")
    @CsvBindByPosition(position = 45)
    private String socDom_8;

    @CsvBindByName(column = "SOC_DOM-9")
    @CsvBindByPosition(position = 46)
    private String socDom_9;

    @CsvBindByName(column = "SOC_DOM-10")
    @CsvBindByPosition(position = 47)
    private String socDom_10;

    @CsvBindByName(column = "SOC_DOM-11")
    @CsvBindByPosition(position = 48)
    private String socDom_11;

    @CsvBindByName(column = "SOC_DOM-12")
    @CsvBindByPosition(position = 49)
    private String socDom_12;

    @CsvBindByName(column = "SOC_DOM-13")
    @CsvBindByPosition(position = 50)
    private String socDom_13;

    @CsvBindByName(column = "SOC_DOM-14")
    @CsvBindByPosition(position = 51)
    private String socDom_14;

    @CsvBindByName(column = "SOC_DOM-15")
    @CsvBindByPosition(position = 52)
    private String socDom_15;

    @CsvBindByName(column = "RMIE_short-1")
    @CsvBindByPosition(position = 53)
    private String rmieShort_1;

    @CsvBindByName(column = "RMIE_short-2")
    @CsvBindByPosition(position = 54)
    private String rmieShort_2;

    @CsvBindByName(column = "RMIE_short-3")
    @CsvBindByPosition(position = 55)
    private String rmieShort_3;

    @CsvBindByName(column = "RMIE_short-4")
    @CsvBindByPosition(position = 56)
    private String rmieShort_4;

    @CsvBindByName(column = "RMIE_short-5")
    @CsvBindByPosition(position = 57)
    private String rmieShort_5;

    @CsvBindByName(column = "RMIE_short-6")
    @CsvBindByPosition(position = 58)
    private String rmieShort_6;

    @CsvBindByName(column = "RMIE_short-7")
    @CsvBindByPosition(position = 59)
    private String rmieShort_7;

    @CsvBindByName(column = "RMIE_short-8")
    @CsvBindByPosition(position = 60)
    private String rmieShort_8;

    @CsvBindByName(column = "RMIE_short-9")
    @CsvBindByPosition(position = 61)
    private String rmieShort_9;

    @CsvBindByName(column = "RMIE_short-10")
    @CsvBindByPosition(position = 62)
    private String rmieShort_10;

    @CsvBindByName(column = "SBSOD-1")
    @CsvBindByPosition(position = 63)
    private String sbsod_1;

    @CsvBindByName(column = "SBSOD-2")
    @CsvBindByPosition(position = 64)
    private String sbsod_2;

    @CsvBindByName(column = "SBSOD-3")
    @CsvBindByPosition(position = 65)
    private String sbsod_3;

    @CsvBindByName(column = "SBSOD-4")
    @CsvBindByPosition(position = 66)
    private String sbsod_4;

    @CsvBindByName(column = "SBSOD-5")
    @CsvBindByPosition(position = 67)
    private String sbsod_5;

    @CsvBindByName(column = "SBSOD-6")
    @CsvBindByPosition(position = 68)
    private String sbsod_6;

    @CsvBindByName(column = "SBSOD-7")
    @CsvBindByPosition(position = 69)
    private String sbsod_7;

    @CsvBindByName(column = "SBSOD-8")
    @CsvBindByPosition(position = 70)
    private String sbsod_8;

    @CsvBindByName(column = "SBSOD-9")
    @CsvBindByPosition(position = 71)
    private String sbsod_9;

    @CsvBindByName(column = "SBSOD-10")
    @CsvBindByPosition(position = 72)
    private String sbsod_10;

    @CsvBindByName(column = "SBSOD-11")
    @CsvBindByPosition(position = 73)
    private String sbsod_11;

    @CsvBindByName(column = "SBSOD-12")
    @CsvBindByPosition(position = 74)
    private String sbsod_12;

    @CsvBindByName(column = "SBSOD-13")
    @CsvBindByPosition(position = 75)
    private String sbsod_13;

    @CsvBindByName(column = "SBSOD-14")
    @CsvBindByPosition(position = 76)
    private String sbsod_14;

    @CsvBindByName(column = "SBSOD-15")
    @CsvBindByPosition(position = 77)
    private String sbsod_15;

    @CsvBindByName(column = "PRSS-1")
    @CsvBindByPosition(position = 78)
    private String prss_1;

    @CsvBindByName(column = "PRSS-2")
    @CsvBindByPosition(position = 79)
    private String prss_2;

    @CsvBindByName(column = "PRSS-3")
    @CsvBindByPosition(position = 80)
    private String prss_3;

    @CsvBindByName(column = "PRSS-4")
    @CsvBindByPosition(position = 81)
    private String prss_4;

    @CsvBindByName(column = "PRSS-5")
    @CsvBindByPosition(position = 82)
    private String prss_5;

    @CsvBindByName(column = "PRSS-6")
    @CsvBindByPosition(position = 83)
    private String prss_6;

    @CsvBindByName(column = "PRSS-7")
    @CsvBindByPosition(position = 84)
    private String prss_7;

    @CsvBindByName(column = "PRSS-8")
    @CsvBindByPosition(position = 85)
    private String prss_8;

    @CsvBindByName(column = "PRSS-9")
    @CsvBindByPosition(position = 86)
    private String prss_9;

    @CsvBindByName(column = "SATIS-1")
    @CsvBindByPosition(position = 87)
    private String satis_1;

    @CsvBindByName(column = "SATIS-2")
    @CsvBindByPosition(position = 88)
    private String satis_2;

    @CsvBindByName(column = "SATIS-3")
    @CsvBindByPosition(position = 89)
    private String satis_3;

    @CsvBindByName(column = "SATIS-4")
    @CsvBindByPosition(position = 90)
    private String satis_4;

    @CsvBindByName(column = "SATIS-5")
    @CsvBindByPosition(position = 91)
    private String satis_5;

    @CsvBindByName(column = "SELF_EFF-1")
    @CsvBindByPosition(position = 92)
    private String selfEff_1;

    @CsvBindByName(column = "SELF_EFF-2")
    @CsvBindByPosition(position = 93)
    private String selfEff_2;

    @CsvBindByName(column = "SELF_EFF-3")
    @CsvBindByPosition(position = 94)
    private String selfEff_3;

    @CsvBindByName(column = "SELF_EFF-4")
    @CsvBindByPosition(position = 95)
    private String selfEff_4;

    @CsvBindByName(column = "SELF_EFF-5")
    @CsvBindByPosition(position = 96)
    private String selfEff_5;

    @CsvBindByName(column = "SELF_EFF-6")
    @CsvBindByPosition(position = 97)
    private String selfEff_6;

    @CsvBindByName(column = "SELF_EFF-7")
    @CsvBindByPosition(position = 98)
    private String selfEff_7;

    @CsvBindByName(column = "SELF_EFF-8")
    @CsvBindByPosition(position = 99)
    private String selfEff_8;

    @CsvBindByName(column = "EVAL-1")
    @CsvBindByPosition(position = 100)
    private String eval_1;

    @CsvBindByName(column = "EVAL-2")
    @CsvBindByPosition(position = 101)
    private String eval_2;

    @CsvBindByName(column = "EVAL-3")
    @CsvBindByPosition(position = 102)
    private String eval_3;

    @CsvBindByName(column = "EVAL-4")
    @CsvBindByPosition(position = 103)
    private String eval_4;

    @CsvBindByName(column = "EVAL-5")
    @CsvBindByPosition(position = 104)
    private String eval_5;

    @CsvBindByName(column = "EVAL-6")
    @CsvBindByPosition(position = 105)
    private String eval_6;

    @CsvBindByName(column = "TEAM_FAMIL-1")
    @CsvBindByPosition(position = 106)
    private String teamFamil_1;

    @CsvBindByName(column = "TEAM_FAMIL-2")
    @CsvBindByPosition(position = 107)
    private String teamFamil_2;

    @CsvBindByName(column = "TEAM_FAMIL-3")
    @CsvBindByPosition(position = 108)
    private String teamFamil_3;

    public MeasuresIndividual(String playerId) {
        this.playerId = playerId;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof MeasuresIndividual p)) {
            return false;
        }
        return p.playerId.equals(this.playerId);
    }

    public String getPlayerId() {
        return playerId;
    }

//    public void setPlayerId(String playerId) {
//        this.playerId = playerId;
//    }

    public String getDemo_1() {
        return demo_1;
    }

    public void setDemo_1(String demo_1) {
        this.demo_1 = demo_1;
    }

    public String getDemo_2() {
        return demo_2;
    }

    public void setDemo_2(String demo_2) {
        this.demo_2 = demo_2;
    }

    public String getDemo_3() {
        return demo_3;
    }

    public void setDemo_3(String demo_3) {
        this.demo_3 = demo_3;
    }

    public String getDemo_4() {
        return demo_4;
    }

    public void setDemo_4(String demo_4) {
        this.demo_4 = demo_4;
    }

    public String getDemo_5() {
        return demo_5;
    }

    public void setDemo_5(String demo_5) {
        this.demo_5 = demo_5;
    }

    public String getDemo_6() {
        return demo_6;
    }

    public void setDemo_6(String demo_6) {
        this.demo_6 = demo_6;
    }

    public String getDemo_7() {
        return demo_7;
    }

    public void setDemo_7(String demo_7) {
        this.demo_7 = demo_7;
    }

    public String getPsyCol_1() {
        return psyCol_1;
    }

    public void setPsyCol_1(String psyCol_1) {
        this.psyCol_1 = psyCol_1;
    }

    public String getPsyCol_2() {
        return psyCol_2;
    }

    public void setPsyCol_2(String psyCol_2) {
        this.psyCol_2 = psyCol_2;
    }

    public String getPsyCol_3() {
        return psyCol_3;
    }

    public void setPsyCol_3(String psyCol_3) {
        this.psyCol_3 = psyCol_3;
    }

    public String getPsyCol_4() {
        return psyCol_4;
    }

    public void setPsyCol_4(String psyCol_4) {
        this.psyCol_4 = psyCol_4;
    }

    public String getPsyCol_5() {
        return psyCol_5;
    }

    public void setPsyCol_5(String psyCol_5) {
        this.psyCol_5 = psyCol_5;
    }

    public String getPsyCol_6() {
        return psyCol_6;
    }

    public void setPsyCol_6(String psyCol_6) {
        this.psyCol_6 = psyCol_6;
    }

    public String getPsyCol_7() {
        return psyCol_7;
    }

    public void setPsyCol_7(String psyCol_7) {
        this.psyCol_7 = psyCol_7;
    }

    public String getPsyCol_8() {
        return psyCol_8;
    }

    public void setPsyCol_8(String psyCol_8) {
        this.psyCol_8 = psyCol_8;
    }

    public String getPsyCol_9() {
        return psyCol_9;
    }

    public void setPsyCol_9(String psyCol_9) {
        this.psyCol_9 = psyCol_9;
    }

    public String getPsyCol_10() {
        return psyCol_10;
    }

    public void setPsyCol_10(String psyCol_10) {
        this.psyCol_10 = psyCol_10;
    }

    public String getPsyCol_11() {
        return psyCol_11;
    }

    public void setPsyCol_11(String psyCol_11) {
        this.psyCol_11 = psyCol_11;
    }

    public String getPsyCol_12() {
        return psyCol_12;
    }

    public void setPsyCol_12(String psyCol_12) {
        this.psyCol_12 = psyCol_12;
    }

    public String getPsyCol_13() {
        return psyCol_13;
    }

    public void setPsyCol_13(String psyCol_13) {
        this.psyCol_13 = psyCol_13;
    }

    public String getPsyCol_14() {
        return psyCol_14;
    }

    public void setPsyCol_14(String psyCol_14) {
        this.psyCol_14 = psyCol_14;
    }

    public String getPsyCol_15() {
        return psyCol_15;
    }

    public void setPsyCol_15(String psyCol_15) {
        this.psyCol_15 = psyCol_15;
    }

    public String getMcProf15_1() {
        return mcProf15_1;
    }

    public void setMcProf15_1(String mcProf15_1) {
        this.mcProf15_1 = mcProf15_1;
    }

    public String getMcProf15_2() {
        return mcProf15_2;
    }

    public void setMcProf15_2(String mcProf15_2) {
        this.mcProf15_2 = mcProf15_2;
    }

    public String getMcProf15_3() {
        return mcProf15_3;
    }

    public void setMcProf15_3(String mcProf15_3) {
        this.mcProf15_3 = mcProf15_3;
    }

    public String getMcProf15_4() {
        return mcProf15_4;
    }

    public void setMcProf15_4(String mcProf15_4) {
        this.mcProf15_4 = mcProf15_4;
    }

    public String getMcProf15_5() {
        return mcProf15_5;
    }

    public void setMcProf15_5(String mcProf15_5) {
        this.mcProf15_5 = mcProf15_5;
    }

    public String getMcProf15_6() {
        return mcProf15_6;
    }

    public void setMcProf15_6(String mcProf15_6) {
        this.mcProf15_6 = mcProf15_6;
    }

    public String getMcProf15_7() {
        return mcProf15_7;
    }

    public void setMcProf15_7(String mcProf15_7) {
        this.mcProf15_7 = mcProf15_7;
    }

    public String getMcProf15_8() {
        return mcProf15_8;
    }

    public void setMcProf15_8(String mcProf15_8) {
        this.mcProf15_8 = mcProf15_8;
    }

    public String getMcProf15_9() {
        return mcProf15_9;
    }

    public void setMcProf15_9(String mcProf15_9) {
        this.mcProf15_9 = mcProf15_9;
    }

    public String getMcProf15_10() {
        return mcProf15_10;
    }

    public void setMcProf15_10(String mcProf15_10) {
        this.mcProf15_10 = mcProf15_10;
    }

    public String getMcProf15_11() {
        return mcProf15_11;
    }

    public void setMcProf15_11(String mcProf15_11) {
        this.mcProf15_11 = mcProf15_11;
    }

    public String getMcProf15_12() {
        return mcProf15_12;
    }

    public void setMcProf15_12(String mcProf15_12) {
        this.mcProf15_12 = mcProf15_12;
    }

    public String getMcProf15_13() {
        return mcProf15_13;
    }

    public void setMcProf15_13(String mcProf15_13) {
        this.mcProf15_13 = mcProf15_13;
    }

    public String getMcProf15_14() {
        return mcProf15_14;
    }

    public void setMcProf15_14(String mcProf15_14) {
        this.mcProf15_14 = mcProf15_14;
    }

    public String getMcProf15_15() {
        return mcProf15_15;
    }

    public void setMcProf15_15(String mcProf15_15) {
        this.mcProf15_15 = mcProf15_15;
    }

    public String getSocDom_1() {
        return socDom_1;
    }

    public void setSocDom_1(String socDom_1) {
        this.socDom_1 = socDom_1;
    }

    public String getSocDom_2() {
        return socDom_2;
    }

    public void setSocDom_2(String socDom_2) {
        this.socDom_2 = socDom_2;
    }

    public String getSocDom_3() {
        return socDom_3;
    }

    public void setSocDom_3(String socDom_3) {
        this.socDom_3 = socDom_3;
    }

    public String getSocDom_4() {
        return socDom_4;
    }

    public void setSocDom_4(String socDom_4) {
        this.socDom_4 = socDom_4;
    }

    public String getSocDom_5() {
        return socDom_5;
    }

    public void setSocDom_5(String socDom_5) {
        this.socDom_5 = socDom_5;
    }

    public String getSocDom_6() {
        return socDom_6;
    }

    public void setSocDom_6(String socDom_6) {
        this.socDom_6 = socDom_6;
    }

    public String getSocDom_7() {
        return socDom_7;
    }

    public void setSocDom_7(String socDom_7) {
        this.socDom_7 = socDom_7;
    }

    public String getSocDom_8() {
        return socDom_8;
    }

    public void setSocDom_8(String socDom_8) {
        this.socDom_8 = socDom_8;
    }

    public String getSocDom_9() {
        return socDom_9;
    }

    public void setSocDom_9(String socDom_9) {
        this.socDom_9 = socDom_9;
    }

    public String getSocDom_10() {
        return socDom_10;
    }

    public void setSocDom_10(String socDom_10) {
        this.socDom_10 = socDom_10;
    }

    public String getSocDom_11() {
        return socDom_11;
    }

    public void setSocDom_11(String socDom_11) {
        this.socDom_11 = socDom_11;
    }

    public String getSocDom_12() {
        return socDom_12;
    }

    public void setSocDom_12(String socDom_12) {
        this.socDom_12 = socDom_12;
    }

    public String getSocDom_13() {
        return socDom_13;
    }

    public void setSocDom_13(String socDom_13) {
        this.socDom_13 = socDom_13;
    }

    public String getSocDom_14() {
        return socDom_14;
    }

    public void setSocDom_14(String socDom_14) {
        this.socDom_14 = socDom_14;
    }

    public String getSocDom_15() {
        return socDom_15;
    }

    public void setSocDom_15(String socDom_15) {
        this.socDom_15 = socDom_15;
    }

    public String getRmieShort_1() {
        return rmieShort_1;
    }

    public void setRmieShort_1(String rmieShort_1) {
        this.rmieShort_1 = rmieShort_1;
    }

    public String getRmieShort_2() {
        return rmieShort_2;
    }

    public void setRmieShort_2(String rmieShort_2) {
        this.rmieShort_2 = rmieShort_2;
    }

    public String getRmieShort_3() {
        return rmieShort_3;
    }

    public void setRmieShort_3(String rmieShort_3) {
        this.rmieShort_3 = rmieShort_3;
    }

    public String getRmieShort_4() {
        return rmieShort_4;
    }

    public void setRmieShort_4(String rmieShort_4) {
        this.rmieShort_4 = rmieShort_4;
    }

    public String getRmieShort_5() {
        return rmieShort_5;
    }

    public void setRmieShort_5(String rmieShort_5) {
        this.rmieShort_5 = rmieShort_5;
    }

    public String getRmieShort_6() {
        return rmieShort_6;
    }

    public void setRmieShort_6(String rmieShort_6) {
        this.rmieShort_6 = rmieShort_6;
    }

    public String getRmieShort_7() {
        return rmieShort_7;
    }

    public void setRmieShort_7(String rmieShort_7) {
        this.rmieShort_7 = rmieShort_7;
    }

    public String getRmieShort_8() {
        return rmieShort_8;
    }

    public void setRmieShort_8(String rmieShort_8) {
        this.rmieShort_8 = rmieShort_8;
    }

    public String getRmieShort_9() {
        return rmieShort_9;
    }

    public void setRmieShort_9(String rmieShort_9) {
        this.rmieShort_9 = rmieShort_9;
    }

    public String getRmieShort_10() {
        return rmieShort_10;
    }

    public void setRmieShort_10(String rmieShort_10) {
        this.rmieShort_10 = rmieShort_10;
    }

    public String getSbsod_1() {
        return sbsod_1;
    }

    public void setSbsod_1(String sbsod_1) {
        this.sbsod_1 = sbsod_1;
    }

    public String getSbsod_2() {
        return sbsod_2;
    }

    public void setSbsod_2(String sbsod_2) {
        this.sbsod_2 = sbsod_2;
    }

    public String getSbsod_3() {
        return sbsod_3;
    }

    public void setSbsod_3(String sbsod_3) {
        this.sbsod_3 = sbsod_3;
    }

    public String getSbsod_4() {
        return sbsod_4;
    }

    public void setSbsod_4(String sbsod_4) {
        this.sbsod_4 = sbsod_4;
    }

    public String getSbsod_5() {
        return sbsod_5;
    }

    public void setSbsod_5(String sbsod_5) {
        this.sbsod_5 = sbsod_5;
    }

    public String getSbsod_6() {
        return sbsod_6;
    }

    public void setSbsod_6(String sbsod_6) {
        this.sbsod_6 = sbsod_6;
    }

    public String getSbsod_7() {
        return sbsod_7;
    }

    public void setSbsod_7(String sbsod_7) {
        this.sbsod_7 = sbsod_7;
    }

    public String getSbsod_8() {
        return sbsod_8;
    }

    public void setSbsod_8(String sbsod_8) {
        this.sbsod_8 = sbsod_8;
    }

    public String getSbsod_9() {
        return sbsod_9;
    }

    public void setSbsod_9(String sbsod_9) {
        this.sbsod_9 = sbsod_9;
    }

    public String getSbsod_10() {
        return sbsod_10;
    }

    public void setSbsod_10(String sbsod_10) {
        this.sbsod_10 = sbsod_10;
    }

    public String getSbsod_11() {
        return sbsod_11;
    }

    public void setSbsod_11(String sbsod_11) {
        this.sbsod_11 = sbsod_11;
    }

    public String getSbsod_12() {
        return sbsod_12;
    }

    public void setSbsod_12(String sbsod_12) {
        this.sbsod_12 = sbsod_12;
    }

    public String getSbsod_13() {
        return sbsod_13;
    }

    public void setSbsod_13(String sbsod_13) {
        this.sbsod_13 = sbsod_13;
    }

    public String getSbsod_14() {
        return sbsod_14;
    }

    public void setSbsod_14(String sbsod_14) {
        this.sbsod_14 = sbsod_14;
    }

    public String getSbsod_15() {
        return sbsod_15;
    }

    public void setSbsod_15(String sbsod_15) {
        this.sbsod_15 = sbsod_15;
    }

    public String getPrss_1() {
        return prss_1;
    }

    public void setPrss_1(String prss_1) {
        this.prss_1 = prss_1;
    }

    public String getPrss_2() {
        return prss_2;
    }

    public void setPrss_2(String prss_2) {
        this.prss_2 = prss_2;
    }

    public String getPrss_3() {
        return prss_3;
    }

    public void setPrss_3(String prss_3) {
        this.prss_3 = prss_3;
    }

    public String getPrss_4() {
        return prss_4;
    }

    public void setPrss_4(String prss_4) {
        this.prss_4 = prss_4;
    }

    public String getPrss_5() {
        return prss_5;
    }

    public void setPrss_5(String prss_5) {
        this.prss_5 = prss_5;
    }

    public String getPrss_6() {
        return prss_6;
    }

    public void setPrss_6(String prss_6) {
        this.prss_6 = prss_6;
    }

    public String getPrss_7() {
        return prss_7;
    }

    public void setPrss_7(String prss_7) {
        this.prss_7 = prss_7;
    }

    public String getPrss_8() {
        return prss_8;
    }

    public void setPrss_8(String prss_8) {
        this.prss_8 = prss_8;
    }

    public String getPrss_9() {
        return prss_9;
    }

    public void setPrss_9(String prss_9) {
        this.prss_9 = prss_9;
    }

    public String getSatis_1() {
        return satis_1;
    }

    public void setSatis_1(String satis_1) {
        this.satis_1 = satis_1;
    }

    public String getSatis_2() {
        return satis_2;
    }

    public void setSatis_2(String satis_2) {
        this.satis_2 = satis_2;
    }

    public String getSatis_3() {
        return satis_3;
    }

    public void setSatis_3(String satis_3) {
        this.satis_3 = satis_3;
    }

    public String getSatis_4() {
        return satis_4;
    }

    public void setSatis_4(String satis_4) {
        this.satis_4 = satis_4;
    }

    public String getSatis_5() {
        return satis_5;
    }

    public void setSatis_5(String satis_5) {
        this.satis_5 = satis_5;
    }

    public String getSelfEff_1() {
        return selfEff_1;
    }

    public void setSelfEff_1(String selfEff_1) {
        this.selfEff_1 = selfEff_1;
    }

    public String getSelfEff_2() {
        return selfEff_2;
    }

    public void setSelfEff_2(String selfEff_2) {
        this.selfEff_2 = selfEff_2;
    }

    public String getSelfEff_3() {
        return selfEff_3;
    }

    public void setSelfEff_3(String selfEff_3) {
        this.selfEff_3 = selfEff_3;
    }

    public String getSelfEff_4() {
        return selfEff_4;
    }

    public void setSelfEff_4(String selfEff_4) {
        this.selfEff_4 = selfEff_4;
    }

    public String getSelfEff_5() {
        return selfEff_5;
    }

    public void setSelfEff_5(String selfEff_5) {
        this.selfEff_5 = selfEff_5;
    }

    public String getSelfEff_6() {
        return selfEff_6;
    }

    public void setSelfEff_6(String selfEff_6) {
        this.selfEff_6 = selfEff_6;
    }

    public String getSelfEff_7() {
        return selfEff_7;
    }

    public void setSelfEff_7(String selfEff_7) {
        this.selfEff_7 = selfEff_7;
    }

    public String getSelfEff_8() {
        return selfEff_8;
    }

    public void setSelfEff_8(String selfEff_8) {
        this.selfEff_8 = selfEff_8;
    }

    public String getEval_1() {
        return eval_1;
    }

    public void setEval_1(String eval_1) {
        this.eval_1 = eval_1;
    }

    public String getEval_2() {
        return eval_2;
    }

    public void setEval_2(String eval_2) {
        this.eval_2 = eval_2;
    }

    public String getEval_3() {
        return eval_3;
    }

    public void setEval_3(String eval_3) {
        this.eval_3 = eval_3;
    }

    public String getEval_4() {
        return eval_4;
    }

    public void setEval_4(String eval_4) {
        this.eval_4 = eval_4;
    }

    public String getEval_5() {
        return eval_5;
    }

    public void setEval_5(String eval_5) {
        this.eval_5 = eval_5;
    }

    public String getEval_6() {
        return eval_6;
    }

    public void setEval_6(String eval_6) {
        this.eval_6 = eval_6;
    }

    public String getTeamFamil_1() {
        return teamFamil_1;
    }

    public void setTeamFamil_1(String teamFamil_1) {
        this.teamFamil_1 = teamFamil_1;
    }

    public String getTeamFamil_2() {
        return teamFamil_2;
    }

    public void setTeamFamil_2(String teamFamil_2) {
        this.teamFamil_2 = teamFamil_2;
    }

    public String getTeamFamil_3() {
        return teamFamil_3;
    }

    public void setTeamFamil_3(String teamFamil_3) {
        this.teamFamil_3 = teamFamil_3;
    }

    public void setPreTrialSurveys(TrialSurveys preTrialSurveys) {
        preTrialSurveys.getSurveys().forEach(survey -> {
            String surveyId = survey.getSurveyId();
            survey.getData().forEach(data -> {
                String qId = data.getQId();
                String response = data.getResponse();
                String columnId = MessageFormat.format("{0}_{1}", surveyId, qId);
                if (qId != null) {
                    switch (surveyId) {
                        case "DEMO" -> demoResponse(qId, response);
                        case "PSY_COL" -> psyColResponse(qId, response);
                        case "MC_PROF_15" -> mcProf15Response(qId, response);
                        case "SOC_DOM" -> socDomResponse(qId, response);
                        case "RMIE_short" -> rmieShortResponse(qId, response);
                        case "SBSOD" -> sbsodResponse(qId, response);
                    }
                }
            });
        });
    }

    private void demoResponse(String qId, String response) {
        switch (qId) {
            case "1" -> setDemo_1(response);
            case "2" -> setDemo_2(response);
            case "3" -> setDemo_3(response);
            case "4" -> setDemo_4(response);
            case "5" -> setDemo_5(response);
            case "6" -> setDemo_6(response);
            case "7" -> setDemo_7(response);
        }
    }

    private void psyColResponse(String qId, String response) {
        switch (qId) {
            case "1" -> setPsyCol_1(response);
            case "2" -> setPsyCol_2(response);
            case "3" -> setPsyCol_3(response);
            case "4" -> setPsyCol_4(response);
            case "5" -> setPsyCol_5(response);
            case "6" -> setPsyCol_6(response);
            case "7" -> setPsyCol_7(response);
            case "8" -> setPsyCol_8(response);
            case "9" -> setPsyCol_9(response);
            case "10" -> setPsyCol_10(response);
            case "11" -> setPsyCol_11(response);
            case "12" -> setPsyCol_12(response);
            case "13" -> setPsyCol_13(response);
            case "14" -> setPsyCol_14(response);
            case "15" -> setPsyCol_15(response);
        }
    }

    private void mcProf15Response(String qId, String response) {
        switch (qId) {
            case "1" -> setMcProf15_1(response);
            case "2" -> setMcProf15_2(response);
            case "3" -> setMcProf15_3(response);
            case "4" -> setMcProf15_4(response);
            case "5" -> setMcProf15_5(response);
            case "6" -> setMcProf15_6(response);
            case "7" -> setMcProf15_7(response);
            case "8" -> setMcProf15_8(response);
            case "9" -> setMcProf15_9(response);
            case "10" -> setMcProf15_10(response);
            case "11" -> setMcProf15_11(response);
            case "12" -> setMcProf15_12(response);
            case "13" -> setMcProf15_13(response);
            case "14" -> setMcProf15_14(response);
            case "15" -> setMcProf15_15(response);
        }
    }

    private void socDomResponse(String qId, String response) {
        switch (qId) {
            case "1" -> setSocDom_1(response);
            case "2" -> setSocDom_2(response);
            case "3" -> setSocDom_3(response);
            case "4" -> setSocDom_4(response);
            case "5" -> setSocDom_5(response);
            case "6" -> setSocDom_6(response);
            case "7" -> setSocDom_7(response);
            case "8" -> setSocDom_8(response);
            case "9" -> setSocDom_9(response);
            case "10" -> setSocDom_10(response);
            case "11" -> setSocDom_11(response);
            case "12" -> setSocDom_12(response);
            case "13" -> setSocDom_13(response);
            case "14" -> setSocDom_14(response);
            case "15" -> setSocDom_15(response);
        }
    }

    private void rmieShortResponse(String qId, String response) {
        switch (qId) {
            case "1" -> setRmieShort_1(response);
            case "2" -> setRmieShort_2(response);
            case "3" -> setRmieShort_3(response);
            case "4" -> setRmieShort_4(response);
            case "5" -> setRmieShort_5(response);
            case "6" -> setRmieShort_6(response);
            case "7" -> setRmieShort_7(response);
            case "8" -> setRmieShort_8(response);
            case "9" -> setRmieShort_9(response);
            case "10" -> setRmieShort_10(response);
        }
    }

    private void sbsodResponse(String qId, String response) {
        switch (qId) {
            case "1" -> setSbsod_1(response);
            case "2" -> setSbsod_2(response);
            case "3" -> setSbsod_3(response);
            case "4" -> setSbsod_4(response);
            case "5" -> setSbsod_5(response);
            case "6" -> setSbsod_6(response);
            case "7" -> setSbsod_7(response);
            case "8" -> setSbsod_8(response);
            case "9" -> setSbsod_9(response);
            case "10" -> setSbsod_10(response);
            case "11" -> setSbsod_11(response);
            case "12" -> setSbsod_12(response);
            case "13" -> setSbsod_13(response);
            case "14" -> setSbsod_14(response);
            case "15" -> setSbsod_15(response);
        }
    }

    public void setPostTrialSurveys(TrialSurveys postTrialSurveys) {
        postTrialSurveys.getSurveys().forEach(survey -> {
            String surveyId = survey.getSurveyId();
            survey.getData().forEach(data -> {
                String qId = data.getQId();
                String response = data.getResponse();
                String columnId = MessageFormat.format("{0}_{1}", surveyId, qId);
                if (qId != null) {
                    switch (surveyId) {
                        case "PRSS" -> prssResponse(qId, response);
                        case "SATIS" -> satisResponse(qId, response);
                        case "SELF_EFF" -> selfEffResponse(qId, response);
                        case "EVAL" -> evalResponse(qId, response);
                        case "TEAM_FAMIL" -> teamFamilResponse(qId, response);
                    }
                }
            });
        });
    }

    private void prssResponse(String qId, String response) {
        switch (qId) {
            case "1" -> setPrss_1(response);
            case "2" -> setPrss_2(response);
            case "3" -> setPrss_3(response);
            case "4" -> setPrss_4(response);
            case "5" -> setPrss_5(response);
            case "6" -> setPrss_6(response);
            case "7" -> setPrss_7(response);
            case "8" -> setPrss_8(response);
            case "9" -> setPrss_9(response);
        }
    }

    private void satisResponse(String qId, String response) {
        switch (qId) {
            case "1" -> setSatis_1(response);
            case "2" -> setSatis_2(response);
            case "3" -> setSatis_3(response);
            case "4" -> setSatis_4(response);
            case "5" -> setSatis_5(response);
        }
    }

    private void selfEffResponse(String qId, String response) {
        switch (qId) {
            case "1" -> setSelfEff_1(response);
            case "2" -> setSelfEff_2(response);
            case "3" -> setSelfEff_3(response);
            case "4" -> setSelfEff_4(response);
            case "5" -> setSelfEff_5(response);
            case "6" -> setSelfEff_6(response);
            case "7" -> setSelfEff_7(response);
            case "8" -> setSelfEff_8(response);
        }
    }

    private void evalResponse(String qId, String response) {
        switch (qId) {
            case "1" -> setEval_1(response);
            case "2" -> setEval_2(response);
            case "3" -> setEval_3(response);
            case "4" -> setEval_4(response);
            case "5" -> setEval_5(response);
            case "6" -> setEval_6(response);
        }
    }

    private void teamFamilResponse(String qId, String response) {
        switch (qId) {
            case "1" -> setTeamFamil_1(response);
            case "2" -> setTeamFamil_2(response);
            case "3" -> setTeamFamil_3(response);
        }
    }
}
