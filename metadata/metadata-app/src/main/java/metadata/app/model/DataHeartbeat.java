package metadata.app.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DataHeartbeat {
	private HeartbeatStateType state;

	@JsonCreator
	public DataHeartbeat(
			@JsonProperty("state") HeartbeatStateType state
			) {
		this.state = state;
	}
	
	public DataHeartbeat() {
		// TODO Auto-generated constructor stub
	}
	
	@JsonProperty("state")
	public HeartbeatStateType getState() {
		return state;
	}
	@JsonProperty("state")
	public void setState(HeartbeatStateType state) {
		this.state = state;
	}
}
