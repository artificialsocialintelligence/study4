package metadata.app.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ExplodedBomb {
    @JsonProperty(value="EXPLODE_CHAINED_ERROR")
    private int explodeChainedError;
    @JsonProperty(value="EXPLODE_FIRE")
    private int explodeFire;
    @JsonProperty(value="EXPLODE_TIME_LIMIT")
    private int explodeTimeLimit;
    @JsonProperty(value="EXPLODE_TOOL_MISMATCH")
    private int explodeToolMismatch;
    @JsonProperty(value="TOTAL_EXPLODED")
    private int totalExploded;

    public ExplodedBomb() {
        this.explodeChainedError = 0;
        this.explodeFire = 0;
        this.explodeTimeLimit = 0;
        this.explodeToolMismatch = 0;
        this.totalExploded = 0;
    }

    public int getExplodeChainedError() {
        return explodeChainedError;
    }

    public void setExplodeChainedError(int explodeChainedError) {
        this.explodeChainedError = explodeChainedError;
    }

    public int getExplodeFire() {
        return explodeFire;
    }

    public void setExplodeFire(int explodeFire) {
        this.explodeFire = explodeFire;
    }

    public int getExplodeTimeLimit() {
        return explodeTimeLimit;
    }

    public void setExplodeTimeLimit(int explodeTimeLimit) {
        this.explodeTimeLimit = explodeTimeLimit;
    }

    public int getExplodeToolMismatch() {
        return explodeToolMismatch;
    }

    public void setExplodeToolMismatch(int explodeToolMismatch) {
        this.explodeToolMismatch = explodeToolMismatch;
    }

    public int getTotalExploded() {
        return totalExploded;
    }

    public void setTotalExploded(int totalExploded) {
        this.totalExploded = totalExploded;
    }
}
