package metadata.app.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DataAutoExport {
	private AutoExportStateType state;

	@JsonCreator
	public DataAutoExport(
			@JsonProperty("state") AutoExportStateType state
			) {
		this.state = state;
	}

	public DataAutoExport() {
		// TODO Auto-generated constructor stub
	}
	
	@JsonProperty("state")
	public AutoExportStateType getState() {
		return state;
	}
	@JsonProperty("state")
	public void setState(AutoExportStateType state) {
		this.state = state;
	}
}
