package metadata.app.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class TrialSurveys {
    @JsonProperty(value="participant_id")
    private String participantId;
    @JsonProperty(value="surveys")
    private List<TrialSurvey> surveys;

    public String getParticipantId() {
        return participantId;
    }

    public void setParticipantId(String participantId) {
        this.participantId = participantId;
    }

    public List<TrialSurvey> getSurveys() {
        return surveys;
    }

    public void setSurveys(List<TrialSurvey> surveys) {
        this.surveys = surveys;
    }
}
