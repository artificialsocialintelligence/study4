package metadata.app.model;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

import io.micronaut.core.annotation.Introspected;

@Introspected
public class MeasuresIntervention {

	@CsvBindByName(column = "InterventionId")
	@CsvBindByPosition(position = 0)
	private String interventionId;
	@CsvBindByName(column = "Content")
	@CsvBindByPosition(position = 1)
	private String content;
	@CsvBindByName(column = "Agent")
	@CsvBindByPosition(position = 2)
	private String agent;
	@CsvBindByName(column = "Timestamp")
	@CsvBindByPosition(position = 3)
	private String timestamp;
	@CsvBindByPosition(position = 4)
	@CsvBindByName(column = "TrialId")
	private String trialId;
	@CsvBindByName(column = "TeamId")
	@CsvBindByPosition(position = 5)
	private String teamId;
	@CsvBindByName(column = "PlayerId")
	@CsvBindByPosition(position = 6)
	private String playerId;
//	@CsvBindByName(column = "InterventionType-1")
//	@CsvBindByPosition(position = 7)
//	private String interventionType1;
//	@CsvBindByName(column = "InterventionType-2")
//	@CsvBindByPosition(position = 8)
//	private String interventionType2;
//	@CsvBindByName(column = "TeamProcessCategory")
//	@CsvBindByPosition(position = 9)
//	private String teamProcessCategory;
//	@CsvBindByName(column = "TeamProcessSubcategory")
//	@CsvBindByPosition(position = 10)
//	private String teamProcessSubcategory;
//	@CsvBindByName(column = "PerformanceBefore")
//	@CsvBindByPosition(position = 11)
//	private String performanceBefore;
//	@CsvBindByName(column = "PerformanceAfter")
//	@CsvBindByPosition(position = 12)
//	private String performanceAfter;

	public String getInterventionId() {
		return interventionId;
	}

	public void setInterventionId(String interventionId) {
		this.interventionId = interventionId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}


	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getTrialId() {
		return trialId;
	}

	public void setTrialId(String trialId) {
		this.trialId = trialId;
	}

	public String getTeamId() {
		return teamId;
	}

	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}

	public String getPlayerId() {
		return playerId;
	}

	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}

//	public String getInterventionType1() {
//		return interventionType1;
//	}
//
//	public void setInterventionType1(String interventionType1) {
//		this.interventionType1 = interventionType1;
//	}
//
//	public String getInterventionType2() {
//		return interventionType2;
//	}
//
//	public void setInterventionType2(String interventionType2) {
//		this.interventionType2 = interventionType2;
//	}
//
//	public String getTeamProcessCategory() {
//		return teamProcessCategory;
//	}
//
//	public void setTeamProcessCategory(String teamProcessCategory) {
//		this.teamProcessCategory = teamProcessCategory;
//	}
//
//	public String getTeamProcessSubcategory() {
//		return teamProcessSubcategory;
//	}
//
//	public void setTeamProcessSubcategory(String teamProcessSubcategory) {
//		this.teamProcessSubcategory = teamProcessSubcategory;
//	}
//
//	public String getPerformanceBefore() {
//		return performanceBefore;
//	}
//
//	public void setPerformanceBefore(String performanceBefore) {
//		this.performanceBefore = performanceBefore;
//	}
//
//	public String getPerformanceAfter() {
//		return performanceAfter;
//	}
//
//	public void setPerformanceAfter(String performanceAfter) {
//		this.performanceAfter = performanceAfter;
//	}

}
