package metadata.app.model;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;
import io.micronaut.core.annotation.Introspected;

@Introspected
public class MeasuresChat {
	@CsvBindByName(column = "SENDER_ID")
	@CsvBindByPosition(position = 0)
	private String senderId;
	@CsvBindByName(column = "TEAM_ID")
	@CsvBindByPosition(position = 1)
	private String teamId;
	@CsvBindByName(column = "RECIPIENTS")
	@CsvBindByPosition(position = 2)
	private String recipients;
	@CsvBindByName(column = "TRIAL_ID")
	@CsvBindByPosition(position = 3)
	private String trialId;
	@CsvBindByPosition(position = 4)
	@CsvBindByName(column = "TIMESTAMP")
	private String timestamp;
	@CsvBindByName(column = "MESSAGE_CONTENT")
	@CsvBindByPosition(position = 5)
	private String messageContent;
	@CsvBindByName(column = "MESSAGE_ID")
	@CsvBindByPosition(position = 6)
	private String messageId;
	@CsvBindByName(column = "ENVIRONMENT")
	@CsvBindByPosition(position = 7)
	private String environment;

	public String getSenderId() {
		return senderId;
	}

	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}

	public String getTeamId() {
		return teamId;
	}

	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}

	public String getRecipients() {
		return recipients;
	}

	public void setRecipients(String recipients) {
		this.recipients = recipients;
	}

	public String getTrialId() {
		return trialId;
	}

	public void setTrialId(String trialId) {
		this.trialId = trialId;
	}


	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}


	public String getMessageContent() {
		return messageContent;
	}

	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}

	public String getMessageId() { return messageId; }

	public void setMessageId(String messageId) { this.messageId = messageId; }

	public String getEnvironment() { return environment; }

	public void setEnvironment(String environment) { this.environment = environment; }
}
