package metadata.app.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.core.annotation.Introspected;

import java.nio.file.Path;
import java.util.Map;

@Introspected
public class TrialStopOptions {
    @JsonProperty(value="METADATA_APP_TRIAL_STOP_EXPORT")
    private boolean trialStopExport;
    @JsonProperty(value="METADATA_APP_TRIAL_STOP_VALIDATION")
    private boolean trialStopValidation;
    @JsonProperty(value="METADATA_APP_TRIAL_STOP_MEASURES")
    private boolean trialStopMeasures;
    @JsonProperty(value="METADATA_APP_TRIAL_STOP_AGENT_TESTS")
    private boolean trialStopAgentTests;
    @JsonProperty(value="METADATA_APP_TRIAL_STOP_DOCKER_LOGS")
    private boolean trialStopDockerLogs;
    @JsonProperty(value="METADATA_APP_TRIAL_STOP_AGENT_LOGS")
    private boolean trialStopAgentLogs;
    @JsonProperty(value="METADATA_APP_TRIAL_STOP_UPLOAD")
    private boolean trialStopUpload;
    @JsonProperty(value="TOKEN")
    private String token;
    @JsonProperty(value="SOURCE_FOLDER")
    private String sourceFolder;
    @JsonProperty(value="REGENERATE_METADATA_FILE_USING_ELASTICSEARCH")
    private boolean regenerate;
    @JsonIgnore
    private Map<String, Integer> tenure;
    @JsonIgnore
    private Map<String, Integer> teamTenure;
    @JsonIgnore
    private int correctedTeamScore;
    @JsonIgnore
    private Path path;
    @JsonIgnore
    private SummaryTrial summaryTrial;

    @JsonIgnore
    private String timestamp;

    public TrialStopOptions() {
        // TODO Auto-generated constructor stub
    }

    @JsonProperty(value="METADATA_APP_TRIAL_STOP_EXPORT")
    public boolean isTrialStopExport() { return trialStopExport; }
    @JsonProperty(value="METADATA_APP_TRIAL_STOP_EXPORT")
    public void setTrialStopExport(boolean trialStopExport) { this.trialStopExport = trialStopExport; }

    @JsonProperty(value="METADATA_APP_TRIAL_STOP_VALIDATION")
    public boolean isTrialStopValidation() {
        return trialStopValidation;
    }
    @JsonProperty(value="METADATA_APP_TRIAL_STOP_VALIDATION")
    public void setTrialStopValidation(boolean trialStopValidation) {
        this.trialStopValidation = trialStopValidation;
    }

    @JsonProperty(value="METADATA_APP_TRIAL_STOP_MEASURES")
    public boolean isTrialStopMeasures() {
        return trialStopMeasures;
    }
    @JsonProperty(value="METADATA_APP_TRIAL_STOP_MEASURES")
    public void setTrialStopMeasures(boolean trialStopMeasures) {
        this.trialStopMeasures = trialStopMeasures;
    }

    @JsonProperty(value="METADATA_APP_TRIAL_STOP_AGENT_TESTS")
    public boolean isTrialStopAgentTests() {
        return trialStopAgentTests;
    }
    @JsonProperty(value="METADATA_APP_TRIAL_STOP_AGENT_TESTS")
    public void setTrialStopAgentTests(boolean trialStopAgentTests) {
        this.trialStopAgentTests = trialStopAgentTests;
    }

    @JsonProperty(value="METADATA_APP_TRIAL_STOP_DOCKER_LOGS")
    public boolean isTrialStopDockerLogs() {
        return trialStopDockerLogs;
    }
    @JsonProperty(value="METADATA_APP_TRIAL_STOP_DOCKER_LOGS")
    public void setTrialStopDockerLogs(boolean trialStopDockerLogs) {
        this.trialStopDockerLogs = trialStopDockerLogs;
    }

    @JsonProperty(value="METADATA_APP_TRIAL_STOP_AGENT_LOGS")
    public boolean isTrialStopAgentLogs() { return trialStopAgentLogs; }
    @JsonProperty(value="METADATA_APP_TRIAL_STOP_AGENT_LOGS")
    public void setTrialStopAgentLogs(boolean trialStopAgentLogs) { this.trialStopAgentLogs = trialStopAgentLogs; }

    @JsonProperty(value="METADATA_APP_TRIAL_STOP_UPLOAD")
    public boolean isTrialStopUpload() {
        return trialStopUpload;
    }
    @JsonProperty(value="METADATA_APP_TRIAL_STOP_UPLOAD")
    public void setTrialStopUpload(boolean trialStopUpload) {
        this.trialStopUpload = trialStopUpload;
    }

    @JsonProperty(value="TOKEN")
    public String getToken() { return token; }
    @JsonProperty(value="TOKEN")
    public void setToken(String token) { this.token = token; }

    @JsonProperty(value="SOURCE_FOLDER")
    public String getSourceFolder() { return sourceFolder; }
    @JsonProperty(value="SOURCE_FOLDER")
    public void setSourceFolder(String sourceFolder) { this.sourceFolder = sourceFolder; }

    @JsonProperty(value="REGENERATE_METADATA_FILE_USING_ELASTICSEARCH")
    public boolean getRegenerate() { return regenerate; }
    @JsonProperty(value="REGENERATE_METADATA_FILE_USING_ELASTICSEARCH")
    public void setRegenerate(boolean regenerate) { this.regenerate = regenerate; }

    @JsonIgnore
    public Map<String, Integer> getTenure() { return tenure; }
    @JsonIgnore
    public void setTenure(Map<String, Integer> tenure) { this.tenure = tenure; }

    @JsonIgnore
    public Map<String, Integer> getTeamTenure() { return teamTenure; }
    @JsonIgnore
    public void setTeamTenure(Map<String, Integer> teamTenure) { this.teamTenure = teamTenure; }

    @JsonIgnore
    public int getCorrectedTeamScore() { return correctedTeamScore; }
    @JsonIgnore
    public void setCorrectedTeamScore(int correctedTeamScore) { this.correctedTeamScore = correctedTeamScore; }

    @JsonIgnore
    public Path getPath() { return path; }
    @JsonIgnore
    public void setPath(Path path) { this.path = path; }

    @JsonIgnore
    public SummaryTrial getSummaryTrial() { return summaryTrial; }
    @JsonIgnore
    public void setSummaryTrial(SummaryTrial summaryTrial) { this.summaryTrial = summaryTrial; }

    @JsonIgnore
    public String getTimestamp() {
        return timestamp;
    }
    @JsonIgnore
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
