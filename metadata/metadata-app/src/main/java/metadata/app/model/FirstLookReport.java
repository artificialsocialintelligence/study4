package metadata.app.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class FirstLookReport {

	private String trialId;
	private String date;
	private FirstLookFile firstLookFile;
	private List<FirstLookTest> firstLookTests;
	
	@JsonCreator
	public FirstLookReport(
			@JsonProperty("trial_id") String trialId,
			@JsonProperty("date") String date,
			@JsonProperty("file") FirstLookFile firstLookFile,
			@JsonProperty("tests") List<FirstLookTest> firstLookTests
			) {
		this.trialId = trialId;
        this.date = date;
        this.firstLookFile = firstLookFile;   
        this.firstLookTests = firstLookTests;   
    }

	@JsonProperty("trial_id")
	public String getTrialId() {
		return trialId;
	}
	@JsonProperty("trial_id")
	public void setTrialId(String trialId) {
		this.trialId = trialId;
	}
	@JsonProperty("date")
	public String getDate() {
		return date;
	}
	@JsonProperty("date")
	public void setDate(String date) {
		this.date = date;
	}
	@JsonProperty("file")
	public FirstLookFile getFirstLookFile() {
		return firstLookFile;
	}
	@JsonProperty("file")
	public void setFirstLookFile(FirstLookFile firstLookFile) {
		this.firstLookFile = firstLookFile;
	}
	@JsonProperty("tests")
	public List<FirstLookTest> getFirstLookTests() {
		return firstLookTests;
	}
	@JsonProperty("tests")
	public void setFirstLookTests(List<FirstLookTest> firstLookTests) {
		this.firstLookTests = firstLookTests;
	}
}