package metadata.app.model;

import com.opencsv.bean.CsvBindAndSplitByName;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;
import io.micronaut.core.annotation.Introspected;

import java.util.List;

@Introspected
public class MeasuresTrial {
	// Summary
	@CsvBindByName(column = "StartTimestamp")
	@CsvBindByPosition(position = 0)
	private String startTimestamp;
	@CsvBindByName(column = "TrialId")
	@CsvBindByPosition(position = 1)
	private String trialId;
	@CsvBindByName(column = "TeamId")
	@CsvBindByPosition(position = 2)
	private String teamId;
	@CsvBindByName(column = "MissionEndCondition")
	@CsvBindByPosition(position = 3)
	private String missionEndCondition;
	@CsvBindByName(column = "TrialEndCondition")
	@CsvBindByPosition(position = 4)
	private String trialEndCondition;
	@CsvBindByName(column = "M1_TeamScore")
	@CsvBindByPosition(position = 5)
	private int teamScore;

	// MEASURES
	@CsvBindByName(column = "M2_SYNC")
	@CsvBindByPosition(position = 6)
	private String asiM2;
	@CsvBindByName(column = "M3_ERROR")
	@CsvBindByPosition(position = 7)
	private String asiM3;
	@CsvBindByName(column = "M3.2_ERROR2")
	@CsvBindByPosition(position = 8)
	private String asiM3_2;
	@CsvBindByName(column = "M4_ADAPT")
	@CsvBindByPosition(position = 9)
	private String asiM4;
	@CsvBindByName(column = "M5_COMMS")
	@CsvBindByPosition(position = 10)
	private String asiM5;
	@CsvBindByName(column = "M8_INTERVENTION-COUNT")
	@CsvBindByPosition(position = 11)
	private String asiM8;
	@CsvBindByName(column = "M14_RISK")
	@CsvBindByPosition(position = 12)
	private String asiM14;
	@CsvBindByName(column = "M16_TEAM-PLANNING")
	@CsvBindByPosition(position = 13)
	private String asiM16;
	@CsvBindByName(column = "M18_RECON-COMMS")
	@CsvBindByPosition(position = 14)
	private String asiM18;

	// END MEASURES

	@CsvBindByName(column = "CorrectedTeamScore")
	@CsvBindByPosition(position = 15)
	private int correctedTeamScore;
	@CsvBindByName(column = "NumStoreVisits")
	@CsvBindByPosition(position = 16)
	private int numStoreVisits;
	@CsvBindByName(column = "TotalStoreTime")
	@CsvBindByPosition(position = 17)
	private int totalStoreTime;
	@CsvBindByName(column = "ASICondition")
	@CsvBindByPosition(position = 18)
	private String asiCondition;
	@CsvBindByName(column = "ExperimentName")
	@CsvBindByPosition(position = 19)
	private String experimentName;
	@CsvBindByName(column = "LastActiveMissionTime")
	@CsvBindByPosition(position = 20)
	private String lastActiveMissionTime;
	@CsvBindByName(column = "TrialName")
	@CsvBindByPosition(position = 21)
	private String trialName;
	@CsvBindByName(column = "MissionVariant")
	@CsvBindByPosition(position = 22)
	private String missionVariant;
	@CsvBindByName(column = "NumFieldVisits")
	@CsvBindByPosition(position = 23)
	private int numFieldVisits;
	@CsvBindByName(column = "ExperimentId")
	@CsvBindByPosition(position = 24)
	private String experimentId;
	@CsvBindByName(column = "BombsTotal")
	@CsvBindByPosition(position = 25)
	private int bombsTotal;
	@CsvBindByName(column = "Player1")
	@CsvBindByPosition(position = 26)
	private String player1;
	@CsvBindByName(column = "Player2")
	@CsvBindByPosition(position = 27)
	private String player2;
	@CsvBindByName(column = "Player3")
	@CsvBindByPosition(position = 28)
	private String player3;
	@CsvBindByName(column = "Player1Tenure")
	@CsvBindByPosition(position = 29)
	private int player1Tenure;
	@CsvBindByName(column = "Player2Tenure")
	@CsvBindByPosition(position = 30)
	private int player2Tenure;
	@CsvBindByName(column = "Player3Tenure")
	@CsvBindByPosition(position = 31)
	private int player3Tenure;
	@CsvBindByName(column = "TeamTenure")
	@CsvBindByPosition(position = 32)
	private int teamTenure;
	@CsvBindByName(column = "TeammatesRescued.Player1")
	@CsvBindByPosition(position = 33)
	private int teammatesRescuedP1;
	@CsvBindByName(column = "TeammatesRescued.Player2")
	@CsvBindByPosition(position = 34)
	private int teammatesRescuedP2;
	@CsvBindByName(column = "TeammatesRescued.Player3")
	@CsvBindByPosition(position = 35)
	private int teammatesRescuedP3;
	@CsvBindByName(column = "TeammatesRescued.Team")
	@CsvBindByPosition(position = 36)
	private int teammatesRescuedTeam;
	@CsvBindByName(column = "TimesFrozen.Player1")
	@CsvBindByPosition(position = 37)
	private int timesFrozenP1;
	@CsvBindByName(column = "TimesFrozen.Player2")
	@CsvBindByPosition(position = 38)
	private int timesFrozenP2;
	@CsvBindByName(column = "TimesFrozen.Player3")
	@CsvBindByPosition(position = 39)
	private int timesFrozenP3;
	@CsvBindByName(column = "TimesFrozen.Team")
	@CsvBindByPosition(position = 40)
	private int timesFrozenTeam;
	@CsvBindByName(column = "TextChatsSent.Player1")
	@CsvBindByPosition(position = 41)
	private int textChatsSentP1;
	@CsvBindByName(column = "TextChatsSent.Player2")
	@CsvBindByPosition(position = 42)
	private int textChatsSentP2;
	@CsvBindByName(column = "TextChatsSent.Player3")
	@CsvBindByPosition(position = 43)
	private int textChatsSentP3;
	@CsvBindByName(column = "TextChatsSent.Team")
	@CsvBindByPosition(position = 44)
	private int textChatsSentTeam;
	@CsvBindByName(column = "FlagsPlaced.Player1")
	@CsvBindByPosition(position = 45)
	private int flagsPlacedP1;
	@CsvBindByName(column = "FlagsPlaced.Player2")
	@CsvBindByPosition(position = 46)
	private int flagsPlacedP2;
	@CsvBindByName(column = "FlagsPlaced.Player3")
	@CsvBindByPosition(position = 47)
	private int flagsPlacedP3;
	@CsvBindByName(column = "FlagsPlaced.Team")
	@CsvBindByPosition(position = 48)
	private int flagsPlacedTeam;
	@CsvBindByName(column = "FiresExtinguished.Player1")
	@CsvBindByPosition(position = 49)
	private int firesExtinguishedP1;
	@CsvBindByName(column = "FiresExtinguished.Player2")
	@CsvBindByPosition(position = 50)
	private int firesExtinguishedP2;
	@CsvBindByName(column = "FiresExtinguished.Player3")
	@CsvBindByPosition(position = 51)
	private int firesExtinguishedP3;
	@CsvBindByName(column = "FiresExtinguished.Team")
	@CsvBindByPosition(position = 52)
	private int firesExtinguishedTeam;
	@CsvBindByName(column = "DamageTaken.Player1")
	@CsvBindByPosition(position = 53)
	private int damageTakenP1;
	@CsvBindByName(column = "DamageTaken.Player2")
	@CsvBindByPosition(position = 54)
	private int damageTakenP2;
	@CsvBindByName(column = "DamageTaken.Player3")
	@CsvBindByPosition(position = 55)
	private int damageTakenP3;
	@CsvBindByName(column = "DamageTaken.Team")
	@CsvBindByPosition(position = 56)
	private int damageTakenTeam;
	@CsvBindByName(column = "NumCompletePostSurveys.Player1")
	@CsvBindByPosition(position = 57)
	private int numCompletePostSurveysP1;
	@CsvBindByName(column = "NumCompletePostSurveys.Player2")
	@CsvBindByPosition(position = 58)
	private int numCompletePostSurveysP2;
	@CsvBindByName(column = "NumCompletePostSurveys.Player3")
	@CsvBindByPosition(position = 59)
	private int numCompletePostSurveysP3;
	@CsvBindByName(column = "NumCompletePostSurveys.Team")
	@CsvBindByPosition(position = 60)
	private int numCompletePostSurveysTeam;
	@CsvBindByName(column = "BombBeaconsPlaced.Player1")
	@CsvBindByPosition(position = 61)
	private int bombBeaconsPlacedP1;
	@CsvBindByName(column = "BombBeaconsPlaced.Player2")
	@CsvBindByPosition(position = 62)
	private int bombBeaconsPlacedP2;
	@CsvBindByName(column = "BombBeaconsPlaced.Player3")
	@CsvBindByPosition(position = 63)
	private int bombBeaconsPlacedP3;
	@CsvBindByName(column = "BombBeaconsPlaced.Team")
	@CsvBindByPosition(position = 64)
	private int bombBeaconsPlacedTeam;
	@CsvBindByName(column = "BudgetExpended.Player1")
	@CsvBindByPosition(position = 65)
	private int budgetExpendedP1;
	@CsvBindByName(column = "BudgetExpended.Player2")
	@CsvBindByPosition(position = 66)
	private int budgetExpendedP2;
	@CsvBindByName(column = "BudgetExpended.Player3")
	@CsvBindByPosition(position = 67)
	private int budgetExpendedP3;
	@CsvBindByName(column = "BudgetExpended.Team")
	@CsvBindByPosition(position = 68)
	private int budgetExpendedTeam;
	@CsvBindByName(column = "CommBeaconsPlaced.Player1")
	@CsvBindByPosition(position = 69)
	private int commBeaconsPlacedP1;
	@CsvBindByName(column = "CommBeaconsPlaced.Player2")
	@CsvBindByPosition(position = 70)
	private int commBeaconsPlacedP2;
	@CsvBindByName(column = "CommBeaconsPlaced.Player3")
	@CsvBindByPosition(position = 71)
	private int commBeaconsPlacedP3;
	@CsvBindByName(column = "CommBeaconsPlaced.Team")
	@CsvBindByPosition(position = 72)
	private int commBeaconsPlacedTeam;
	@CsvBindByName(column = "BombsExploded.EXPLODE_CHAINED_ERROR")
	@CsvBindByPosition(position = 73)
	private int bombsExplodedChainedError;
	@CsvBindByName(column = "BombsExploded.EXPLODE_FIRE")
	@CsvBindByPosition(position = 74)
	private int bombsExplodedFire;
	@CsvBindByName(column = "BombsExploded.EXPLODE_TIME_LIMIT")
	@CsvBindByPosition(position = 75)
	private int bombsExplodedTimeLimit;
	@CsvBindByName(column = "BombsExploded.EXPLODE_TOOL_MISMATCH")
	@CsvBindByPosition(position = 76)
	private int bombsExplodedToolMismatch;
	@CsvBindByName(column = "BombsExploded.TOTAL_EXPLODED")
	@CsvBindByPosition(position = 77)
	private int bombsExplodedTotal;
	@CsvBindByName(column = "BombSummary.Player1.BOMBS_EXPLODED.CHAINED")
	@CsvBindByPosition(position = 78)
	private int bombsSummaryP1BombsExplodedChained;
	@CsvBindByName(column = "BombSummary.Player1.BOMBS_EXPLODED.FIRE")
	@CsvBindByPosition(position = 79)
	private int bombsSummaryP1BombsExplodedFire;
	@CsvBindByName(column = "BombSummary.Player1.BOMBS_EXPLODED.STANDARD")
	@CsvBindByPosition(position = 80)
	private int bombsSummaryP1BombsExplodedStandard;
	@CsvBindByName(column = "BombSummary.Player1.PHASES_DEFUSED.CHAINED.B")
	@CsvBindByPosition(position = 81)
	private int bombsSummaryP1PhasesDefusedChainedB;
	@CsvBindByName(column = "BombSummary.Player1.PHASES_DEFUSED.CHAINED.R")
	@CsvBindByPosition(position = 82)
	private int bombsSummaryP1PhasesDefusedChainedR;
	@CsvBindByName(column = "BombSummary.Player1.PHASES_DEFUSED.CHAINED.G")
	@CsvBindByPosition(position = 83)
	private int bombsSummaryP1PhasesDefusedChainedG;
	@CsvBindByName(column = "BombSummary.Player1.PHASES_DEFUSED.FIRE.B")
	@CsvBindByPosition(position = 84)
	private int bombsSummaryP1PhasesDefusedFireB;
	@CsvBindByName(column = "BombSummary.Player1.PHASES_DEFUSED.FIRE.R")
	@CsvBindByPosition(position = 85)
	private int bombsSummaryP1PhasesDefusedFireR;
	@CsvBindByName(column = "BombSummary.Player1.PHASES_DEFUSED.FIRE.G")
	@CsvBindByPosition(position = 86)
	private int bombsSummaryP1PhasesDefusedFireG;
	@CsvBindByName(column = "BombSummary.Player1.PHASES_DEFUSED.STANDARD.B")
	@CsvBindByPosition(position = 87)
	private int bombsSummaryP1PhasesDefusedStandardB;
	@CsvBindByName(column = "BombSummary.Player1.PHASES_DEFUSED.STANDARD.R")
	@CsvBindByPosition(position = 88)
	private int bombsSummaryP1PhasesDefusedStandardR;
	@CsvBindByName(column = "BombSummary.Player1.PHASES_DEFUSED.STANDARD.G")
	@CsvBindByPosition(position = 89)
	private int bombsSummaryP1PhasesDefusedStandardG;
	@CsvBindByName(column = "BombSummary.Player1.BOMBS_DEFUSED.CHAINED")
	@CsvBindByPosition(position = 90)
	private int bombsSummaryP1BombsDefusedChained;
	@CsvBindByName(column = "BombSummary.Player1.BOMBS_DEFUSED.FIRE")
	@CsvBindByPosition(position = 91)
	private int bombsSummaryP1BombsDefusedFire;
	@CsvBindByName(column = "BombSummary.Player1.BOMBS_DEFUSED.STANDARD")
	@CsvBindByPosition(position = 92)
	private int bombsSummaryP1BombsDefusedStandard;
	@CsvBindByName(column = "BombSummary.Player2.BOMBS_EXPLODED.CHAINED")
	@CsvBindByPosition(position = 93)
	private int bombsSummaryP2BombsExplodedChained;
	@CsvBindByName(column = "BombSummary.Player2.BOMBS_EXPLODED.FIRE")
	@CsvBindByPosition(position = 94)
	private int bombsSummaryP2BombsExplodedFire;
	@CsvBindByName(column = "BombSummary.Player2.BOMBS_EXPLODED.STANDARD")
	@CsvBindByPosition(position = 95)
	private int bombsSummaryP2BombsExplodedStandard;
	@CsvBindByName(column = "BombSummary.Player2.PHASES_DEFUSED.CHAINED.B")
	@CsvBindByPosition(position = 96)
	private int bombsSummaryP2PhasesDefusedChainedB;
	@CsvBindByName(column = "BombSummary.Player2.PHASES_DEFUSED.CHAINED.R")
	@CsvBindByPosition(position = 97)
	private int bombsSummaryP2PhasesDefusedChainedR;
	@CsvBindByName(column = "BombSummary.Player2.PHASES_DEFUSED.CHAINED.G")
	@CsvBindByPosition(position = 98)
	private int bombsSummaryP2PhasesDefusedChainedG;
	@CsvBindByName(column = "BombSummary.Player2.PHASES_DEFUSED.FIRE.B")
	@CsvBindByPosition(position = 99)
	private int bombsSummaryP2PhasesDefusedFireB;
	@CsvBindByName(column = "BombSummary.Player2.PHASES_DEFUSED.FIRE.R")
	@CsvBindByPosition(position = 100)
	private int bombsSummaryP2PhasesDefusedFireR;
	@CsvBindByName(column = "BombSummary.Player2.PHASES_DEFUSED.FIRE.G")
	@CsvBindByPosition(position = 101)
	private int bombsSummaryP2PhasesDefusedFireG;
	@CsvBindByName(column = "BombSummary.Player2.PHASES_DEFUSED.STANDARD.B")
	@CsvBindByPosition(position = 102)
	private int bombsSummaryP2PhasesDefusedStandardB;
	@CsvBindByName(column = "BombSummary.Player2.PHASES_DEFUSED.STANDARD.R")
	@CsvBindByPosition(position = 103)
	private int bombsSummaryP2PhasesDefusedStandardR;
	@CsvBindByName(column = "BombSummary.Player2.PHASES_DEFUSED.STANDARD.G")
	@CsvBindByPosition(position = 104)
	private int bombsSummaryP2PhasesDefusedStandardG;
	@CsvBindByName(column = "BombSummary.Player2.BOMBS_DEFUSED.CHAINED")
	@CsvBindByPosition(position = 105)
	private int bombsSummaryP2BombsDefusedChained;
	@CsvBindByName(column = "BombSummary.Player2.BOMBS_DEFUSED.FIRE")
	@CsvBindByPosition(position = 106)
	private int bombsSummaryP2BombsDefusedFire;
	@CsvBindByName(column = "BombSummary.Player2.BOMBS_DEFUSED.STANDARD")
	@CsvBindByPosition(position = 107)
	private int bombsSummaryP2BombsDefusedStandard;
	@CsvBindByName(column = "BombSummary.Player3.BOMBS_EXPLODED.CHAINED")
	@CsvBindByPosition(position = 108)
	private int bombsSummaryP3BombsExplodedChained;
	@CsvBindByName(column = "BombSummary.Player3.BOMBS_EXPLODED.FIRE")
	@CsvBindByPosition(position = 109)
	private int bombsSummaryP3BombsExplodedFire;
	@CsvBindByName(column = "BombSummary.Player3.BOMBS_EXPLODED.STANDARD")
	@CsvBindByPosition(position = 110)
	private int bombsSummaryP3BombsExplodedStandard;
	@CsvBindByName(column = "BombSummary.Player3.PHASES_DEFUSED.CHAINED.B")
	@CsvBindByPosition(position = 111)
	private int bombsSummaryP3PhasesDefusedChainedB;
	@CsvBindByName(column = "BombSummary.Player3.PHASES_DEFUSED.CHAINED.R")
	@CsvBindByPosition(position = 112)
	private int bombsSummaryP3PhasesDefusedChainedR;
	@CsvBindByName(column = "BombSummary.Player3.PHASES_DEFUSED.CHAINED.G")
	@CsvBindByPosition(position = 113)
	private int bombsSummaryP3PhasesDefusedChainedG;
	@CsvBindByName(column = "BombSummary.Player3.PHASES_DEFUSED.FIRE.B")
	@CsvBindByPosition(position = 114)
	private int bombsSummaryP3PhasesDefusedFireB;
	@CsvBindByName(column = "BombSummary.Player3.PHASES_DEFUSED.FIRE.R")
	@CsvBindByPosition(position = 115)
	private int bombsSummaryP3PhasesDefusedFireR;
	@CsvBindByName(column = "BombSummary.Player3.PHASES_DEFUSED.FIRE.G")
	@CsvBindByPosition(position = 116)
	private int bombsSummaryP3PhasesDefusedFireG;
	@CsvBindByName(column = "BombSummary.Player3.PHASES_DEFUSED.STANDARD.B")
	@CsvBindByPosition(position = 117)
	private int bombsSummaryP3PhasesDefusedStandardB;
	@CsvBindByName(column = "BombSummary.Player3.PHASES_DEFUSED.STANDARD.R")
	@CsvBindByPosition(position = 118)
	private int bombsSummaryP3PhasesDefusedStandardR;
	@CsvBindByName(column = "BombSummary.Player3.PHASES_DEFUSED.STANDARD.G")
	@CsvBindByPosition(position = 119)
	private int bombsSummaryP3PhasesDefusedStandardG;
	@CsvBindByName(column = "BombSummary.Player3.BOMBS_DEFUSED.CHAINED")
	@CsvBindByPosition(position = 120)
	private int bombsSummaryP3BombsDefusedChained;
	@CsvBindByName(column = "BombSummary.Player3.BOMBS_DEFUSED.FIRE")
	@CsvBindByPosition(position = 121)
	private int bombsSummaryP3BombsDefusedFire;
	@CsvBindByName(column = "BombSummary.Player3.BOMBS_DEFUSED.STANDARD")
	@CsvBindByPosition(position = 122)
	private int bombsSummaryP3BombsDefusedStandard;
	@CsvBindByName(column = "AutoJump.Player1")
	@CsvBindByPosition(position = 123)
	private int autoJumpP1;
	@CsvBindByName(column = "AutoJump.Player2")
	@CsvBindByPosition(position = 124)
	private int autoJumpP2;
	@CsvBindByName(column = "AutoJump.Player3")
	@CsvBindByPosition(position = 125)
	private int autoJumpP3;
	@CsvBindByName(column = "PreReconPurchase.Player1")
	@CsvBindByPosition(position = 126)
	private int preReconPurchaseP1;
	@CsvBindByName(column = "PreReconPurchase.Player2")
	@CsvBindByPosition(position = 127)
	private int preReconPurchaseP2;
	@CsvBindByName(column = "PreReconPurchase.Player3")
	@CsvBindByPosition(position = 128)
	private int preReconPurchaseP3;
	@CsvBindByName(column = "WirecutterInventory.Player1")
	@CsvBindByPosition(position = 129)
	private int wirecutterInventoryP1;
	@CsvBindByName(column = "WirecutterInventory.Player2")
	@CsvBindByPosition(position = 130)
	private int wirecutterInventoryP2;
	@CsvBindByName(column = "WirecutterInventory.Player3")
	@CsvBindByPosition(position = 131)
	private int wirecutterInventoryP3;
	@CsvBindByName(column = "ThreePhaseBombs.Player1")
	@CsvBindByPosition(position = 132)
	private int threePhaseBombsP1;
	@CsvBindByName(column = "ThreePhaseBombs.Player2")
	@CsvBindByPosition(position = 133)
	private int threePhaseBombsP2;
	@CsvBindByName(column = "ThreePhaseBombs.Player3")
	@CsvBindByPosition(position = 134)
	private int threePhaseBombsP3;
//	@CsvBindByName(column = "SoloDisposers")
	@CsvBindByName(column = "SoloDisposers")
	@CsvBindByPosition(position = 135)
	private String soloDisposers;
	@CsvBindByName(column = "AverageWaitTimeSeconds")
	@CsvBindByPosition(position = 136)
	private long averageWaitTime;

	public String getAsiM2() {
		return asiM2;
	}

	public void setAsiM2(String asiM2) {
		this.asiM2 = asiM2;
	}

	public String getAsiM3() {
		return asiM3;
	}

	public void setAsiM3(String asiM3) {
		this.asiM3 = asiM3;
	}

	public String getAsiM3_2() {
		return asiM3_2;
	}

	public void setAsiM3_2(String asiM3_2) {
		this.asiM3_2 = asiM3_2;
	}

	public String getAsiM4() {
		return asiM4;
	}

	public void setAsiM4(String asiM4) {
		this.asiM4 = asiM4;
	}

	public String getAsiM5() {
		return asiM5;
	}

	public void setAsiM5(String asiM5) {
		this.asiM5 = asiM5;
	}

	public String getAsiM8() {
		return asiM8;
	}

	public void setAsiM8(String asiM8) {
		this.asiM8 = asiM8;
	}

	public String getAsiM14() {
		return asiM14;
	}

	public void setAsiM14(String asiM14) {
		this.asiM14 = asiM14;
	}

	public String getAsiM16() {
		return asiM16;
	}

	public void setAsiM16(String asiM16) {
		this.asiM16 = asiM16;
	}

	public String getAsiM18() {
		return asiM18;
	}

	public void setAsiM18(String asiM18) {
		this.asiM18 = asiM18;
	}

	public String getStartTimestamp() {
		return startTimestamp;
	}

	public void setStartTimestamp(String startTimestamp) {
		this.startTimestamp = startTimestamp;
	}

	public int getNumStoreVisits() {
		return numStoreVisits;
	}

	public void setNumStoreVisits(int numStoreVisits) {
		this.numStoreVisits = numStoreVisits;
	}

	public int getTotalStoreTime() {
		return totalStoreTime;
	}

	public void setTotalStoreTime(int totalStoreTime) {
		this.totalStoreTime = totalStoreTime;
	}

	public String getAsiCondition() {
		return asiCondition;
	}

	public void setAsiCondition(String asiCondition) {
		this.asiCondition = asiCondition;
	}

	public String getExperimentName() {
		return experimentName;
	}

	public void setExperimentName(String experimentName) {
		this.experimentName = experimentName;
	}

	public String getLastActiveMissionTime() {
		return lastActiveMissionTime;
	}

	public void setLastActiveMissionTime(String lastActiveMissionTime) {
		this.lastActiveMissionTime = lastActiveMissionTime;
	}

	public int getTeamScore() {
		return teamScore;
	}

	public void setTeamScore(int teamScore) {
		this.teamScore = teamScore;
	}

	public String getMissionEndCondition() {
		return missionEndCondition;
	}

	public void setMissionEndCondition(String missionEndCondition) {
		this.missionEndCondition = missionEndCondition;
	}

	public String getTrialEndCondition() {
		return trialEndCondition;
	}

	public void setTrialEndCondition(String trialEndCondition) {
		this.trialEndCondition = trialEndCondition;
	}

	public String getTrialName() {
		return trialName;
	}

	public void setTrialName(String trialName) {
		this.trialName = trialName;
	}

	public String getMissionVariant() {
		return missionVariant;
	}

	public void setMissionVariant(String missionVariant) {
		this.missionVariant = missionVariant;
	}

	public String getTeamId() {
		return teamId;
	}

	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}

	public int getNumFieldVisits() {
		return numFieldVisits;
	}

	public void setNumFieldVisits(int numFieldVisits) {
		this.numFieldVisits = numFieldVisits;
	}

	public String getExperimentId() {
		return experimentId;
	}

	public void setExperimentId(String experimentId) {
		this.experimentId = experimentId;
	}

	public String getTrialId() {
		return trialId;
	}

	public void setTrialId(String trialId) {
		this.trialId = trialId;
	}

	public int getBombsTotal() {
		return bombsTotal;
	}

	public void setBombsTotal(int bombsTotal) {
		this.bombsTotal = bombsTotal;
	}

	public String getPlayer1() {
		return player1;
	}

	public void setPlayer1(String player1) {
		this.player1 = player1;
	}

	public String getPlayer2() {
		return player2;
	}

	public void setPlayer2(String player2) {
		this.player2 = player2;
	}

	public String getPlayer3() {
		return player3;
	}

	public void setPlayer3(String player3) {
		this.player3 = player3;
	}

	public int getTeammatesRescuedP1() {
		return teammatesRescuedP1;
	}

	public void setTeammatesRescuedP1(int teammatesRescuedP1) {
		this.teammatesRescuedP1 = teammatesRescuedP1;
	}

	public int getTeammatesRescuedP2() {
		return teammatesRescuedP2;
	}

	public void setTeammatesRescuedP2(int teammatesRescuedP2) {
		this.teammatesRescuedP2 = teammatesRescuedP2;
	}

	public int getTeammatesRescuedP3() {
		return teammatesRescuedP3;
	}

	public void setTeammatesRescuedP3(int teammatesRescuedP3) {
		this.teammatesRescuedP3 = teammatesRescuedP3;
	}

	public int getTeammatesRescuedTeam() {
		return teammatesRescuedTeam;
	}

	public void setTeammatesRescuedTeam(int teammatesRescuedTeam) {
		this.teammatesRescuedTeam = teammatesRescuedTeam;
	}

	public int getTimesFrozenP1() {
		return timesFrozenP1;
	}

	public void setTimesFrozenP1(int timesFrozenP1) {
		this.timesFrozenP1 = timesFrozenP1;
	}

	public int getTimesFrozenP2() {
		return timesFrozenP2;
	}

	public void setTimesFrozenP2(int timesFrozenP2) {
		this.timesFrozenP2 = timesFrozenP2;
	}

	public int getTimesFrozenP3() {
		return timesFrozenP3;
	}

	public void setTimesFrozenP3(int timesFrozenP3) {
		this.timesFrozenP3 = timesFrozenP3;
	}

	public int getTimesFrozenTeam() {
		return timesFrozenTeam;
	}

	public void setTimesFrozenTeam(int timesFrozenTeam) {
		this.timesFrozenTeam = timesFrozenTeam;
	}

	public int getTextChatsSentP1() {
		return textChatsSentP1;
	}

	public void setTextChatsSentP1(int textChatsSentP1) {
		this.textChatsSentP1 = textChatsSentP1;
	}

	public int getTextChatsSentP2() {
		return textChatsSentP2;
	}

	public void setTextChatsSentP2(int textChatsSentP2) {
		this.textChatsSentP2 = textChatsSentP2;
	}

	public int getTextChatsSentP3() {
		return textChatsSentP3;
	}

	public void setTextChatsSentP3(int textChatsSentP3) {
		this.textChatsSentP3 = textChatsSentP3;
	}

	public int getTextChatsSentTeam() {
		return textChatsSentTeam;
	}

	public void setTextChatsSentTeam(int textChatsSentTeam) {
		this.textChatsSentTeam = textChatsSentTeam;
	}

	public int getFlagsPlacedP1() {
		return flagsPlacedP1;
	}

	public void setFlagsPlacedP1(int flagsPlacedP1) {
		this.flagsPlacedP1 = flagsPlacedP1;
	}

	public int getFlagsPlacedP2() {
		return flagsPlacedP2;
	}

	public void setFlagsPlacedP2(int flagsPlacedP2) {
		this.flagsPlacedP2 = flagsPlacedP2;
	}

	public int getFlagsPlacedP3() {
		return flagsPlacedP3;
	}

	public void setFlagsPlacedP3(int flagsPlacedP3) {
		this.flagsPlacedP3 = flagsPlacedP3;
	}

	public int getFlagsPlacedTeam() {
		return flagsPlacedTeam;
	}

	public void setFlagsPlacedTeam(int flagsPlacedTeam) {
		this.flagsPlacedTeam = flagsPlacedTeam;
	}

	public int getFiresExtinguishedP1() {
		return firesExtinguishedP1;
	}

	public void setFiresExtinguishedP1(int firesExtinguishedP1) {
		this.firesExtinguishedP1 = firesExtinguishedP1;
	}

	public int getFiresExtinguishedP2() {
		return firesExtinguishedP2;
	}

	public void setFiresExtinguishedP2(int firesExtinguishedP2) {
		this.firesExtinguishedP2 = firesExtinguishedP2;
	}

	public int getFiresExtinguishedP3() {
		return firesExtinguishedP3;
	}

	public void setFiresExtinguishedP3(int firesExtinguishedP3) {
		this.firesExtinguishedP3 = firesExtinguishedP3;
	}

	public int getFiresExtinguishedTeam() {
		return firesExtinguishedTeam;
	}

	public void setFiresExtinguishedTeam(int firesExtinguishedTeam) {
		this.firesExtinguishedTeam = firesExtinguishedTeam;
	}

	public int getDamageTakenP1() {
		return damageTakenP1;
	}

	public void setDamageTakenP1(int damageTakenP1) {
		this.damageTakenP1 = damageTakenP1;
	}

	public int getDamageTakenP2() {
		return damageTakenP2;
	}

	public void setDamageTakenP2(int damageTakenP2) {
		this.damageTakenP2 = damageTakenP2;
	}

	public int getDamageTakenP3() {
		return damageTakenP3;
	}

	public void setDamageTakenP3(int damageTakenP3) {
		this.damageTakenP3 = damageTakenP3;
	}

	public int getDamageTakenTeam() {
		return damageTakenTeam;
	}

	public void setDamageTakenTeam(int damageTakenTeam) {
		this.damageTakenTeam = damageTakenTeam;
	}

	public int getNumCompletePostSurveysP1() {
		return numCompletePostSurveysP1;
	}

	public void setNumCompletePostSurveysP1(int numCompletePostSurveysP1) {
        this.numCompletePostSurveysP1 = Math.min(numCompletePostSurveysP1, 1);
	}

	public int getNumCompletePostSurveysP2() {
		return numCompletePostSurveysP2;
	}

	public void setNumCompletePostSurveysP2(int numCompletePostSurveysP2) {
		this.numCompletePostSurveysP2 = Math.min(numCompletePostSurveysP2, 1);
	}

	public int getNumCompletePostSurveysP3() {
		return numCompletePostSurveysP3;
	}

	public void setNumCompletePostSurveysP3(int numCompletePostSurveysP3) {
		this.numCompletePostSurveysP3 = Math.min(numCompletePostSurveysP3, 1);
	}

	public int getNumCompletePostSurveysTeam() {
		return numCompletePostSurveysTeam;
	}

	public void setNumCompletePostSurveysTeam(int numCompletePostSurveysTeam) {
		this.numCompletePostSurveysTeam = numCompletePostSurveysTeam;
	}

	public int getBombBeaconsPlacedP1() {
		return bombBeaconsPlacedP1;
	}

	public void setBombBeaconsPlacedP1(int bombBeaconsPlacedP1) {
		this.bombBeaconsPlacedP1 = bombBeaconsPlacedP1;
	}

	public int getBombBeaconsPlacedP2() {
		return bombBeaconsPlacedP2;
	}

	public void setBombBeaconsPlacedP2(int bombBeaconsPlacedP2) {
		this.bombBeaconsPlacedP2 = bombBeaconsPlacedP2;
	}

	public int getBombBeaconsPlacedP3() {
		return bombBeaconsPlacedP3;
	}

	public void setBombBeaconsPlacedP3(int bombBeaconsPlacedP3) {
		this.bombBeaconsPlacedP3 = bombBeaconsPlacedP3;
	}

	public int getBombBeaconsPlacedTeam() {
		return bombBeaconsPlacedTeam;
	}

	public void setBombBeaconsPlacedTeam(int bombBeaconsPlacedTeam) {
		this.bombBeaconsPlacedTeam = bombBeaconsPlacedTeam;
	}

	public int getBudgetExpendedP1() {
		return budgetExpendedP1;
	}

	public void setBudgetExpendedP1(int budgetExpendedP1) {
		this.budgetExpendedP1 = budgetExpendedP1;
	}

	public int getBudgetExpendedP2() {
		return budgetExpendedP2;
	}

	public void setBudgetExpendedP2(int budgetExpendedP2) {
		this.budgetExpendedP2 = budgetExpendedP2;
	}

	public int getBudgetExpendedP3() {
		return budgetExpendedP3;
	}

	public void setBudgetExpendedP3(int budgetExpendedP3) {
		this.budgetExpendedP3 = budgetExpendedP3;
	}

	public int getBudgetExpendedTeam() {
		return budgetExpendedTeam;
	}

	public void setBudgetExpendedTeam(int budgetExpendedTeam) {
		this.budgetExpendedTeam = budgetExpendedTeam;
	}

	public int getCommBeaconsPlacedP1() {
		return commBeaconsPlacedP1;
	}

	public void setCommBeaconsPlacedP1(int commBeaconsPlacedP1) {
		this.commBeaconsPlacedP1 = commBeaconsPlacedP1;
	}

	public int getCommBeaconsPlacedP2() {
		return commBeaconsPlacedP2;
	}

	public void setCommBeaconsPlacedP2(int commBeaconsPlacedP2) {
		this.commBeaconsPlacedP2 = commBeaconsPlacedP2;
	}

	public int getCommBeaconsPlacedP3() {
		return commBeaconsPlacedP3;
	}

	public void setCommBeaconsPlacedP3(int commBeaconsPlacedP3) {
		this.commBeaconsPlacedP3 = commBeaconsPlacedP3;
	}

	public int getCommBeaconsPlacedTeam() {
		return commBeaconsPlacedTeam;
	}

	public void setCommBeaconsPlacedTeam(int commBeaconsPlacedTeam) {
		this.commBeaconsPlacedTeam = commBeaconsPlacedTeam;
	}

	public int getBombsExplodedChainedError() {
		return bombsExplodedChainedError;
	}

	public void setBombsExplodedChainedError(int bombsExplodedChainedError) {
		this.bombsExplodedChainedError = bombsExplodedChainedError;
	}

	public int getBombsExplodedFire() {
		return bombsExplodedFire;
	}

	public void setBombsExplodedFire(int bombsExplodedFire) {
		this.bombsExplodedFire = bombsExplodedFire;
	}

	public int getBombsExplodedTimeLimit() {
		return bombsExplodedTimeLimit;
	}

	public void setBombsExplodedTimeLimit(int bombsExplodedTimeLimit) {
		this.bombsExplodedTimeLimit = bombsExplodedTimeLimit;
	}

	public int getBombsExplodedToolMismatch() {
		return bombsExplodedToolMismatch;
	}

	public void setBombsExplodedToolMismatch(int bombsExplodedToolMismatch) {
		this.bombsExplodedToolMismatch = bombsExplodedToolMismatch;
	}

	public int getBombsExplodedTotal() {
		return bombsExplodedTotal;
	}

	public void setBombsExplodedTotal(int bombsExplodedTotal) {
		this.bombsExplodedTotal = bombsExplodedTotal;
	}

	public int getBombsSummaryP1BombsExplodedChained() {
		return bombsSummaryP1BombsExplodedChained;
	}

	public void setBombsSummaryP1BombsExplodedChained(int bombsSummaryP1BombsExplodedChained) {
		this.bombsSummaryP1BombsExplodedChained = bombsSummaryP1BombsExplodedChained;
	}

	public int getBombsSummaryP1BombsExplodedFire() {
		return bombsSummaryP1BombsExplodedFire;
	}

	public void setBombsSummaryP1BombsExplodedFire(int bombsSummaryP1BombsExplodedFire) {
		this.bombsSummaryP1BombsExplodedFire = bombsSummaryP1BombsExplodedFire;
	}

	public int getBombsSummaryP1BombsExplodedStandard() {
		return bombsSummaryP1BombsExplodedStandard;
	}

	public void setBombsSummaryP1BombsExplodedStandard(int bombsSummaryP1BombsExplodedStandard) {
		this.bombsSummaryP1BombsExplodedStandard = bombsSummaryP1BombsExplodedStandard;
	}

	public int getBombsSummaryP1PhasesDefusedChainedB() {
		return bombsSummaryP1PhasesDefusedChainedB;
	}

	public void setBombsSummaryP1PhasesDefusedChainedB(int bombsSummaryP1PhasesDefusedChainedB) {
		this.bombsSummaryP1PhasesDefusedChainedB = bombsSummaryP1PhasesDefusedChainedB;
	}

	public int getBombsSummaryP1PhasesDefusedChainedR() {
		return bombsSummaryP1PhasesDefusedChainedR;
	}

	public void setBombsSummaryP1PhasesDefusedChainedR(int bombsSummaryP1PhasesDefusedChainedR) {
		this.bombsSummaryP1PhasesDefusedChainedR = bombsSummaryP1PhasesDefusedChainedR;
	}

	public int getBombsSummaryP1PhasesDefusedChainedG() {
		return bombsSummaryP1PhasesDefusedChainedG;
	}

	public void setBombsSummaryP1PhasesDefusedChainedG(int bombsSummaryP1PhasesDefusedChainedG) {
		this.bombsSummaryP1PhasesDefusedChainedG = bombsSummaryP1PhasesDefusedChainedG;
	}

	public int getBombsSummaryP1PhasesDefusedFireB() {
		return bombsSummaryP1PhasesDefusedFireB;
	}

	public void setBombsSummaryP1PhasesDefusedFireB(int bombsSummaryP1PhasesDefusedFireB) {
		this.bombsSummaryP1PhasesDefusedFireB = bombsSummaryP1PhasesDefusedFireB;
	}

	public int getBombsSummaryP1PhasesDefusedFireR() {
		return bombsSummaryP1PhasesDefusedFireR;
	}

	public void setBombsSummaryP1PhasesDefusedFireR(int bombsSummaryP1PhasesDefusedFireR) {
		this.bombsSummaryP1PhasesDefusedFireR = bombsSummaryP1PhasesDefusedFireR;
	}

	public int getBombsSummaryP1PhasesDefusedFireG() {
		return bombsSummaryP1PhasesDefusedFireG;
	}

	public void setBombsSummaryP1PhasesDefusedFireG(int bombsSummaryP1PhasesDefusedFireG) {
		this.bombsSummaryP1PhasesDefusedFireG = bombsSummaryP1PhasesDefusedFireG;
	}

	public int getBombsSummaryP1PhasesDefusedStandardB() {
		return bombsSummaryP1PhasesDefusedStandardB;
	}

	public void setBombsSummaryP1PhasesDefusedStandardB(int bombsSummaryP1PhasesDefusedStandardB) {
		this.bombsSummaryP1PhasesDefusedStandardB = bombsSummaryP1PhasesDefusedStandardB;
	}

	public int getBombsSummaryP1PhasesDefusedStandardR() {
		return bombsSummaryP1PhasesDefusedStandardR;
	}

	public void setBombsSummaryP1PhasesDefusedStandardR(int bombsSummaryP1PhasesDefusedStandardR) {
		this.bombsSummaryP1PhasesDefusedStandardR = bombsSummaryP1PhasesDefusedStandardR;
	}

	public int getBombsSummaryP1PhasesDefusedStandardG() {
		return bombsSummaryP1PhasesDefusedStandardG;
	}

	public void setBombsSummaryP1PhasesDefusedStandardG(int bombsSummaryP1PhasesDefusedStandardG) {
		this.bombsSummaryP1PhasesDefusedStandardG = bombsSummaryP1PhasesDefusedStandardG;
	}

	public int getBombsSummaryP1BombsDefusedChained() {
		return bombsSummaryP1BombsDefusedChained;
	}

	public void setBombsSummaryP1BombsDefusedChained(int bombsSummaryP1BombsDefusedChained) {
		this.bombsSummaryP1BombsDefusedChained = bombsSummaryP1BombsDefusedChained;
	}

	public int getBombsSummaryP1BombsDefusedFire() {
		return bombsSummaryP1BombsDefusedFire;
	}

	public void setBombsSummaryP1BombsDefusedFire(int bombsSummaryP1BombsDefusedFire) {
		this.bombsSummaryP1BombsDefusedFire = bombsSummaryP1BombsDefusedFire;
	}

	public int getBombsSummaryP1BombsDefusedStandard() {
		return bombsSummaryP1BombsDefusedStandard;
	}

	public void setBombsSummaryP1BombsDefusedStandard(int bombsSummaryP1BombsDefusedStandard) {
		this.bombsSummaryP1BombsDefusedStandard = bombsSummaryP1BombsDefusedStandard;
	}

	public int getBombsSummaryP2BombsExplodedChained() {
		return bombsSummaryP2BombsExplodedChained;
	}

	public void setBombsSummaryP2BombsExplodedChained(int bombsSummaryP2BombsExplodedChained) {
		this.bombsSummaryP2BombsExplodedChained = bombsSummaryP2BombsExplodedChained;
	}

	public int getBombsSummaryP2BombsExplodedFire() {
		return bombsSummaryP2BombsExplodedFire;
	}

	public void setBombsSummaryP2BombsExplodedFire(int bombsSummaryP2BombsExplodedFire) {
		this.bombsSummaryP2BombsExplodedFire = bombsSummaryP2BombsExplodedFire;
	}

	public int getBombsSummaryP2BombsExplodedStandard() {
		return bombsSummaryP2BombsExplodedStandard;
	}

	public void setBombsSummaryP2BombsExplodedStandard(int bombsSummaryP2BombsExplodedStandard) {
		this.bombsSummaryP2BombsExplodedStandard = bombsSummaryP2BombsExplodedStandard;
	}

	public int getBombsSummaryP2PhasesDefusedChainedB() {
		return bombsSummaryP2PhasesDefusedChainedB;
	}

	public void setBombsSummaryP2PhasesDefusedChainedB(int bombsSummaryP2PhasesDefusedChainedB) {
		this.bombsSummaryP2PhasesDefusedChainedB = bombsSummaryP2PhasesDefusedChainedB;
	}

	public int getBombsSummaryP2PhasesDefusedChainedR() {
		return bombsSummaryP2PhasesDefusedChainedR;
	}

	public void setBombsSummaryP2PhasesDefusedChainedR(int bombsSummaryP2PhasesDefusedChainedR) {
		this.bombsSummaryP2PhasesDefusedChainedR = bombsSummaryP2PhasesDefusedChainedR;
	}

	public int getBombsSummaryP2PhasesDefusedChainedG() {
		return bombsSummaryP2PhasesDefusedChainedG;
	}

	public void setBombsSummaryP2PhasesDefusedChainedG(int bombsSummaryP2PhasesDefusedChainedG) {
		this.bombsSummaryP2PhasesDefusedChainedG = bombsSummaryP2PhasesDefusedChainedG;
	}

	public int getBombsSummaryP2PhasesDefusedFireB() {
		return bombsSummaryP2PhasesDefusedFireB;
	}

	public void setBombsSummaryP2PhasesDefusedFireB(int bombsSummaryP2PhasesDefusedFireB) {
		this.bombsSummaryP2PhasesDefusedFireB = bombsSummaryP2PhasesDefusedFireB;
	}

	public int getBombsSummaryP2PhasesDefusedFireR() {
		return bombsSummaryP2PhasesDefusedFireR;
	}

	public void setBombsSummaryP2PhasesDefusedFireR(int bombsSummaryP2PhasesDefusedFireR) {
		this.bombsSummaryP2PhasesDefusedFireR = bombsSummaryP2PhasesDefusedFireR;
	}

	public int getBombsSummaryP2PhasesDefusedFireG() {
		return bombsSummaryP2PhasesDefusedFireG;
	}

	public void setBombsSummaryP2PhasesDefusedFireG(int bombsSummaryP2PhasesDefusedFireG) {
		this.bombsSummaryP2PhasesDefusedFireG = bombsSummaryP2PhasesDefusedFireG;
	}

	public int getBombsSummaryP2PhasesDefusedStandardB() {
		return bombsSummaryP2PhasesDefusedStandardB;
	}

	public void setBombsSummaryP2PhasesDefusedStandardB(int bombsSummaryP2PhasesDefusedStandardB) {
		this.bombsSummaryP2PhasesDefusedStandardB = bombsSummaryP2PhasesDefusedStandardB;
	}

	public int getBombsSummaryP2PhasesDefusedStandardR() {
		return bombsSummaryP2PhasesDefusedStandardR;
	}

	public void setBombsSummaryP2PhasesDefusedStandardR(int bombsSummaryP2PhasesDefusedStandardR) {
		this.bombsSummaryP2PhasesDefusedStandardR = bombsSummaryP2PhasesDefusedStandardR;
	}

	public int getBombsSummaryP2PhasesDefusedStandardG() {
		return bombsSummaryP2PhasesDefusedStandardG;
	}

	public void setBombsSummaryP2PhasesDefusedStandardG(int bombsSummaryP2PhasesDefusedStandardG) {
		this.bombsSummaryP2PhasesDefusedStandardG = bombsSummaryP2PhasesDefusedStandardG;
	}

	public int getBombsSummaryP2BombsDefusedChained() {
		return bombsSummaryP2BombsDefusedChained;
	}

	public void setBombsSummaryP2BombsDefusedChained(int bombsSummaryP2BombsDefusedChained) {
		this.bombsSummaryP2BombsDefusedChained = bombsSummaryP2BombsDefusedChained;
	}

	public int getBombsSummaryP2BombsDefusedFire() {
		return bombsSummaryP2BombsDefusedFire;
	}

	public void setBombsSummaryP2BombsDefusedFire(int bombsSummaryP2BombsDefusedFire) {
		this.bombsSummaryP2BombsDefusedFire = bombsSummaryP2BombsDefusedFire;
	}

	public int getBombsSummaryP2BombsDefusedStandard() {
		return bombsSummaryP2BombsDefusedStandard;
	}

	public void setBombsSummaryP2BombsDefusedStandard(int bombsSummaryP2BombsDefusedStandard) {
		this.bombsSummaryP2BombsDefusedStandard = bombsSummaryP2BombsDefusedStandard;
	}

	public int getBombsSummaryP3BombsExplodedChained() {
		return bombsSummaryP3BombsExplodedChained;
	}

	public void setBombsSummaryP3BombsExplodedChained(int bombsSummaryP3BombsExplodedChained) {
		this.bombsSummaryP3BombsExplodedChained = bombsSummaryP3BombsExplodedChained;
	}

	public int getBombsSummaryP3BombsExplodedFire() {
		return bombsSummaryP3BombsExplodedFire;
	}

	public void setBombsSummaryP3BombsExplodedFire(int bombsSummaryP3BombsExplodedFire) {
		this.bombsSummaryP3BombsExplodedFire = bombsSummaryP3BombsExplodedFire;
	}

	public int getBombsSummaryP3BombsExplodedStandard() {
		return bombsSummaryP3BombsExplodedStandard;
	}

	public void setBombsSummaryP3BombsExplodedStandard(int bombsSummaryP3BombsExplodedStandard) {
		this.bombsSummaryP3BombsExplodedStandard = bombsSummaryP3BombsExplodedStandard;
	}

	public int getBombsSummaryP3PhasesDefusedChainedB() {
		return bombsSummaryP3PhasesDefusedChainedB;
	}

	public void setBombsSummaryP3PhasesDefusedChainedB(int bombsSummaryP3PhasesDefusedChainedB) {
		this.bombsSummaryP3PhasesDefusedChainedB = bombsSummaryP3PhasesDefusedChainedB;
	}

	public int getBombsSummaryP3PhasesDefusedChainedR() {
		return bombsSummaryP3PhasesDefusedChainedR;
	}

	public void setBombsSummaryP3PhasesDefusedChainedR(int bombsSummaryP3PhasesDefusedChainedR) {
		this.bombsSummaryP3PhasesDefusedChainedR = bombsSummaryP3PhasesDefusedChainedR;
	}

	public int getBombsSummaryP3PhasesDefusedChainedG() {
		return bombsSummaryP3PhasesDefusedChainedG;
	}

	public void setBombsSummaryP3PhasesDefusedChainedG(int bombsSummaryP3PhasesDefusedChainedG) {
		this.bombsSummaryP3PhasesDefusedChainedG = bombsSummaryP3PhasesDefusedChainedG;
	}

	public int getBombsSummaryP3PhasesDefusedFireB() {
		return bombsSummaryP3PhasesDefusedFireB;
	}

	public void setBombsSummaryP3PhasesDefusedFireB(int bombsSummaryP3PhasesDefusedFireB) {
		this.bombsSummaryP3PhasesDefusedFireB = bombsSummaryP3PhasesDefusedFireB;
	}

	public int getBombsSummaryP3PhasesDefusedFireR() {
		return bombsSummaryP3PhasesDefusedFireR;
	}

	public void setBombsSummaryP3PhasesDefusedFireR(int bombsSummaryP3PhasesDefusedFireR) {
		this.bombsSummaryP3PhasesDefusedFireR = bombsSummaryP3PhasesDefusedFireR;
	}

	public int getBombsSummaryP3PhasesDefusedFireG() {
		return bombsSummaryP3PhasesDefusedFireG;
	}

	public void setBombsSummaryP3PhasesDefusedFireG(int bombsSummaryP3PhasesDefusedFireG) {
		this.bombsSummaryP3PhasesDefusedFireG = bombsSummaryP3PhasesDefusedFireG;
	}

	public int getBombsSummaryP3PhasesDefusedStandardB() {
		return bombsSummaryP3PhasesDefusedStandardB;
	}

	public void setBombsSummaryP3PhasesDefusedStandardB(int bombsSummaryP3PhasesDefusedStandardB) {
		this.bombsSummaryP3PhasesDefusedStandardB = bombsSummaryP3PhasesDefusedStandardB;
	}

	public int getBombsSummaryP3PhasesDefusedStandardR() {
		return bombsSummaryP3PhasesDefusedStandardR;
	}

	public void setBombsSummaryP3PhasesDefusedStandardR(int bombsSummaryP3PhasesDefusedStandardR) {
		this.bombsSummaryP3PhasesDefusedStandardR = bombsSummaryP3PhasesDefusedStandardR;
	}

	public int getBombsSummaryP3PhasesDefusedStandardG() {
		return bombsSummaryP3PhasesDefusedStandardG;
	}

	public void setBombsSummaryP3PhasesDefusedStandardG(int bombsSummaryP3PhasesDefusedStandardG) {
		this.bombsSummaryP3PhasesDefusedStandardG = bombsSummaryP3PhasesDefusedStandardG;
	}

	public int getBombsSummaryP3BombsDefusedChained() {
		return bombsSummaryP3BombsDefusedChained;
	}

	public void setBombsSummaryP3BombsDefusedChained(int bombsSummaryP3BombsDefusedChained) {
		this.bombsSummaryP3BombsDefusedChained = bombsSummaryP3BombsDefusedChained;
	}

	public int getBombsSummaryP3BombsDefusedFire() {
		return bombsSummaryP3BombsDefusedFire;
	}

	public void setBombsSummaryP3BombsDefusedFire(int bombsSummaryP3BombsDefusedFire) {
		this.bombsSummaryP3BombsDefusedFire = bombsSummaryP3BombsDefusedFire;
	}

	public int getBombsSummaryP3BombsDefusedStandard() {
		return bombsSummaryP3BombsDefusedStandard;
	}

	public void setBombsSummaryP3BombsDefusedStandard(int bombsSummaryP3BombsDefusedStandard) {
		this.bombsSummaryP3BombsDefusedStandard = bombsSummaryP3BombsDefusedStandard;
	}

	public int getCorrectedTeamScore() {
		return correctedTeamScore;
	}

	public void setCorrectedTeamScore(int correctedTeamScore) {
		this.correctedTeamScore = correctedTeamScore;
	}

	public int getPlayer1Tenure() {
		return player1Tenure;
	}

	public void setPlayer1Tenure(int player1Tenure) {
		this.player1Tenure = player1Tenure;
	}

	public int getPlayer2Tenure() {
		return player2Tenure;
	}

	public void setPlayer2Tenure(int player2Tenure) {
		this.player2Tenure = player2Tenure;
	}

	public int getPlayer3Tenure() {
		return player3Tenure;
	}

	public void setPlayer3Tenure(int player3Tenure) {
		this.player3Tenure = player3Tenure;
	}

	public int getTeamTenure() {
		return teamTenure;
	}

	public void setTeamTenure(int teamTenure) {
		this.teamTenure = teamTenure;
	}

	public int getAutoJumpP1() {
		return autoJumpP1;
	}

	public void setAutoJumpP1(int autoJumpP1) {
		this.autoJumpP1 = autoJumpP1;
	}

	public int getAutoJumpP2() {
		return autoJumpP2;
	}

	public void setAutoJumpP2(int autoJumpP2) {
		this.autoJumpP2 = autoJumpP2;
	}

	public int getAutoJumpP3() {
		return autoJumpP3;
	}

	public void setAutoJumpP3(int autoJumpP3) {
		this.autoJumpP3 = autoJumpP3;
	}

	public int getPreReconPurchaseP1() {
		return preReconPurchaseP1;
	}

	public void setPreReconPurchaseP1(int preReconPurchaseP1) {
		this.preReconPurchaseP1 = preReconPurchaseP1;
	}

	public int getPreReconPurchaseP2() {
		return preReconPurchaseP2;
	}

	public void setPreReconPurchaseP2(int preReconPurchaseP2) {
		this.preReconPurchaseP2 = preReconPurchaseP2;
	}

	public int getPreReconPurchaseP3() {
		return preReconPurchaseP3;
	}

	public void setPreReconPurchaseP3(int preReconPurchaseP3) {
		this.preReconPurchaseP3 = preReconPurchaseP3;
	}

	public int getWirecutterInventoryP1() {
		return wirecutterInventoryP1;
	}

	public void setWirecutterInventoryP1(int wirecutterInventoryP1) {
		this.wirecutterInventoryP1 = wirecutterInventoryP1;
	}

	public int getWirecutterInventoryP2() {
		return wirecutterInventoryP2;
	}

	public void setWirecutterInventoryP2(int wirecutterInventoryP2) {
		this.wirecutterInventoryP2 = wirecutterInventoryP2;
	}

	public int getWirecutterInventoryP3() {
		return wirecutterInventoryP3;
	}

	public void setWirecutterInventoryP3(int wirecutterInventoryP3) {
		this.wirecutterInventoryP3 = wirecutterInventoryP3;
	}

	public int getThreePhaseBombsP1() {
		return threePhaseBombsP1;
	}

	public void setThreePhaseBombsP1(int threePhaseBombsP1) {
		this.threePhaseBombsP1 = threePhaseBombsP1;
	}

	public int getThreePhaseBombsP2() {
		return threePhaseBombsP2;
	}

	public void setThreePhaseBombsP2(int threePhaseBombsP2) {
		this.threePhaseBombsP2 = threePhaseBombsP2;
	}

	public int getThreePhaseBombsP3() {
		return threePhaseBombsP3;
	}

	public void setThreePhaseBombsP3(int threePhaseBombsP3) {
		this.threePhaseBombsP3 = threePhaseBombsP3;
	}

	public String getSoloDisposers() {
		return soloDisposers;
	}

	public void setSoloDisposers(String soloDisposers) {
		this.soloDisposers = soloDisposers;
	}

	public long getAverageWaitTime() {
		return averageWaitTime;
	}

	public void setAverageWaitTime(long averageWaitTime) {
		this.averageWaitTime = averageWaitTime;
	}

	public void setMeasures(String measureId, String measureValue) {
		switch (measureId) {
//			case "ASI-M1":
//				this.setAsiM1(measureValue);
//				break;
			case "ASI-M2":
				this.setAsiM2(measureValue);
				break;
			case "ASI-M3":
				this.setAsiM3(measureValue);
				break;
			case "ASI-M3.2":
				this.setAsiM3_2(measureValue);
				break;
			case "ASI-M4":
				this.setAsiM4(measureValue);
				break;
			case "ASI-M5":
				this.setAsiM5(measureValue);
				break;
			case "ASI-M8":
				this.setAsiM8(measureValue);
				break;
			case "ASI-M14":
				this.setAsiM14(measureValue);
				break;
//			case "ASI-M15":
//				this.setAsiM15(measureValue);
//				break;
			case "ASI-M16":
				this.setAsiM16(measureValue);
				break;
			case "ASI-M17":
//				this.setAsiM17(measureValue);
				break;
			case "ASI-M18":
				this.setAsiM18(measureValue);
				break;
//			case "ASI-M19":
//				this.setAsiM19(measureValue);
//				break;
//			case "TeamProc":
//				this.setTeamProc(measureValue);
//				break;
//			case "TeamPerfSat":
//				this.setTeamPerfSat(measureValue);
//				break;
//			case "TeammateSat":
//				this.setTeammateSat(measureValue);
//				break;
//			case "ASI-M6":
//				this.setAsiM6(measureValue);
//				break;
//			case "ASI-M7":
//				this.setAsiM7(measureValue);
//				break;
//			case "EmergeL":
//				this.setEmergeL(measureValue);
//				break;
//			case "MemFam":
//				this.setMemFam(measureValue);
//				break;
		}
	}

	public void setSummary(SummaryTrial updatedSummaryTrial) {
		this.startTimestamp = updatedSummaryTrial.getStartTimestamp();
		this.numStoreVisits = updatedSummaryTrial.getNumStoreVisits();
		this.totalStoreTime = updatedSummaryTrial.getTotalStoreTime();
		this.asiCondition = updatedSummaryTrial.getAsiCondition();
		this.experimentName = updatedSummaryTrial.getExperimentName();
		this.lastActiveMissionTime = updatedSummaryTrial.getLastActiveMissionTime();
		this.teamScore = updatedSummaryTrial.getTeamScore();
		this.missionEndCondition = updatedSummaryTrial.getMissionEndCondition();
		this.trialEndCondition = updatedSummaryTrial.getTrialEndCondition();
		this.trialName = updatedSummaryTrial.getTrialName();
		this.missionVariant = updatedSummaryTrial.getMissionVariant();
		this.teamId = updatedSummaryTrial.getTeamId();
		this.numFieldVisits = updatedSummaryTrial.getNumFieldVisits();
		this.experimentId = updatedSummaryTrial.getExperimentId();
		this.trialId = updatedSummaryTrial.getTrialId();
		this.bombsTotal = updatedSummaryTrial.getBombsTotal();
		this.player1 = updatedSummaryTrial.getMembers().size() >= 1 ? updatedSummaryTrial.getMembers().get(0) : "";
		this.player2 = updatedSummaryTrial.getMembers().size() >= 2 ? updatedSummaryTrial.getMembers().get(1) : "";
		this.player3 = updatedSummaryTrial.getMembers().size() == 3 ? updatedSummaryTrial.getMembers().get(2) : "";
		if (updatedSummaryTrial.getTeammatesRescued() != null) {
			this.teammatesRescuedP1 = updatedSummaryTrial.getTeammatesRescued().getOrDefault(this.player1, 0);
			this.teammatesRescuedP2 = updatedSummaryTrial.getTeammatesRescued().getOrDefault(this.player2, 0);
			this.teammatesRescuedP3 = updatedSummaryTrial.getTeammatesRescued().getOrDefault(this.player3, 0);
			this.teammatesRescuedTeam = updatedSummaryTrial.getTeammatesRescued().getOrDefault("Team", 0);
		}
		if (updatedSummaryTrial.getTimesFrozen() != null) {
			this.timesFrozenP1 = updatedSummaryTrial.getTimesFrozen().getOrDefault(this.player1, 0);
			this.timesFrozenP2 = updatedSummaryTrial.getTimesFrozen().getOrDefault(this.player2, 0);
			this.timesFrozenP3 = updatedSummaryTrial.getTimesFrozen().getOrDefault(this.player3, 0);
			this.timesFrozenTeam = updatedSummaryTrial.getTimesFrozen().getOrDefault("Team", 0);
		}
		if (updatedSummaryTrial.getTextChatsSent() != null) {
			this.textChatsSentP1 = updatedSummaryTrial.getTextChatsSent().getOrDefault(this.player1, 0);
			this.textChatsSentP2 = updatedSummaryTrial.getTextChatsSent().getOrDefault(this.player2, 0);
			this.textChatsSentP3 = updatedSummaryTrial.getTextChatsSent().getOrDefault(this.player3, 0);
			this.textChatsSentTeam = updatedSummaryTrial.getTextChatsSent().getOrDefault("Team", 0);
		}
		if (updatedSummaryTrial.getFlagsPlaced() != null) {
			this.flagsPlacedP1 = updatedSummaryTrial.getFlagsPlaced().getOrDefault(this.player1, 0);
			this.flagsPlacedP2 = updatedSummaryTrial.getFlagsPlaced().getOrDefault(this.player2, 0);
			this.flagsPlacedP3 = updatedSummaryTrial.getFlagsPlaced().getOrDefault(this.player3, 0);
			this.flagsPlacedTeam = updatedSummaryTrial.getFlagsPlaced().getOrDefault("Team", 0);
		}
		if (updatedSummaryTrial.getFiresExtinguished() != null) {
			this.firesExtinguishedP1 = updatedSummaryTrial.getFiresExtinguished().getOrDefault(this.player1, 0);
			this.firesExtinguishedP2 = updatedSummaryTrial.getFiresExtinguished().getOrDefault(this.player2, 0);
			this.firesExtinguishedP3 = updatedSummaryTrial.getFiresExtinguished().getOrDefault(this.player3, 0);
			this.firesExtinguishedTeam = updatedSummaryTrial.getFiresExtinguished().getOrDefault("Team", 0);
		}
		if (updatedSummaryTrial.getDamageTaken() != null) {
			this.damageTakenP1 = updatedSummaryTrial.getDamageTaken().getOrDefault(this.player1, 0);
			this.damageTakenP2 = updatedSummaryTrial.getDamageTaken().getOrDefault(this.player2, 0);
			this.damageTakenP3 = updatedSummaryTrial.getDamageTaken().getOrDefault(this.player3, 0);
			this.damageTakenTeam = updatedSummaryTrial.getDamageTaken().getOrDefault("Team", 0);
		}
		if (updatedSummaryTrial.getNumCompletePostSurveys() != null) {
			this.setNumCompletePostSurveysP1(updatedSummaryTrial.getNumCompletePostSurveys().getOrDefault(this.player1, 0));
			this.setNumCompletePostSurveysP2(updatedSummaryTrial.getNumCompletePostSurveys().getOrDefault(this.player2, 0));
			this.setNumCompletePostSurveysP3(updatedSummaryTrial.getNumCompletePostSurveys().getOrDefault(this.player3, 0));
//			int numCompletePostSurveysTeam = updatedSummaryTrial.getNumCompletePostSurveys().getOrDefault("Team", 0);
			this.setNumCompletePostSurveysTeam(this.getNumCompletePostSurveysP1() + this.getNumCompletePostSurveysP2() + this.getNumCompletePostSurveysP3());
		}
		if (updatedSummaryTrial.getBombBeaconsPlaced() != null) {
			this.bombBeaconsPlacedP1 = updatedSummaryTrial.getBombBeaconsPlaced().getOrDefault(this.player1, 0);
			this.bombBeaconsPlacedP2 = updatedSummaryTrial.getBombBeaconsPlaced().getOrDefault(this.player2, 0);
			this.bombBeaconsPlacedP3 = updatedSummaryTrial.getBombBeaconsPlaced().getOrDefault(this.player3, 0);
			this.bombBeaconsPlacedTeam = updatedSummaryTrial.getBombBeaconsPlaced().getOrDefault("Team", 0);
		}
		if (updatedSummaryTrial.getBudgetExpended() != null) {
			this.budgetExpendedP1 = updatedSummaryTrial.getBudgetExpended().getOrDefault(this.player1, 0);
			this.budgetExpendedP2 = updatedSummaryTrial.getBudgetExpended().getOrDefault(this.player2, 0);
			this.budgetExpendedP3 = updatedSummaryTrial.getBudgetExpended().getOrDefault(this.player3, 0);
			this.budgetExpendedTeam = updatedSummaryTrial.getBudgetExpended().getOrDefault("Team", 0);
		}
		if (updatedSummaryTrial.getCommBeaconsPlaced() != null) {
			this.commBeaconsPlacedP1 = updatedSummaryTrial.getCommBeaconsPlaced().getOrDefault(this.player1, 0);
			this.commBeaconsPlacedP2 = updatedSummaryTrial.getCommBeaconsPlaced().getOrDefault(this.player2, 0);
			this.commBeaconsPlacedP3 = updatedSummaryTrial.getCommBeaconsPlaced().getOrDefault(this.player3, 0);
			this.commBeaconsPlacedTeam = updatedSummaryTrial.getCommBeaconsPlaced().getOrDefault("Team", 0);
		}

		this.bombsExplodedChainedError = updatedSummaryTrial.getBombsExploded() != null ? updatedSummaryTrial.getBombsExploded().getExplodeChainedError() : 0;
		this.bombsExplodedChainedError = updatedSummaryTrial.getBombsExploded() != null ? updatedSummaryTrial.getBombsExploded().getExplodeFire() : 0;
		this.bombsExplodedChainedError = updatedSummaryTrial.getBombsExploded() != null ? updatedSummaryTrial.getBombsExploded().getExplodeTimeLimit() : 0;
		this.bombsExplodedChainedError = updatedSummaryTrial.getBombsExploded() != null ? updatedSummaryTrial.getBombsExploded().getExplodeToolMismatch() : 0;
		this.bombsExplodedChainedError = updatedSummaryTrial.getBombsExploded() != null ? updatedSummaryTrial.getBombsExploded().getTotalExploded() : 0;

		if (updatedSummaryTrial.getBombSummaryPlayer() != null) {
			this.bombsSummaryP1BombsExplodedChained = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player1, new SummaryBomb()).getBombsExploded().getChanined();
			this.bombsSummaryP1BombsExplodedFire = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player1, new SummaryBomb()).getBombsExploded().getFire();
			this.bombsSummaryP1BombsExplodedStandard = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player1, new SummaryBomb()).getBombsExploded().getStandard();
			this.bombsSummaryP1PhasesDefusedChainedB = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player1, new SummaryBomb()).getPhasesDefused().getChained().getB();
			this.bombsSummaryP1PhasesDefusedChainedR = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player1, new SummaryBomb()).getPhasesDefused().getChained().getR();
			this.bombsSummaryP1PhasesDefusedChainedG = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player1, new SummaryBomb()).getPhasesDefused().getChained().getG();
			this.bombsSummaryP1PhasesDefusedFireB = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player1, new SummaryBomb()).getPhasesDefused().getFire().getB();
			this.bombsSummaryP1PhasesDefusedFireR = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player1, new SummaryBomb()).getPhasesDefused().getFire().getR();
			this.bombsSummaryP1PhasesDefusedFireG = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player1, new SummaryBomb()).getPhasesDefused().getFire().getG();
			this.bombsSummaryP1PhasesDefusedStandardB = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player1, new SummaryBomb()).getPhasesDefused().getStandard().getB();
			this.bombsSummaryP1PhasesDefusedStandardR = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player1, new SummaryBomb()).getPhasesDefused().getStandard().getR();
			this.bombsSummaryP1PhasesDefusedStandardG = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player1, new SummaryBomb()).getPhasesDefused().getStandard().getG();
			this.bombsSummaryP1BombsDefusedChained = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player1, new SummaryBomb()).getBombsDefused().getChanined();
			this.bombsSummaryP1BombsDefusedFire = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player1, new SummaryBomb()).getBombsDefused().getFire();
			this.bombsSummaryP1BombsDefusedStandard = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player1, new SummaryBomb()).getBombsDefused().getStandard();
			this.bombsSummaryP2BombsExplodedChained = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player2, new SummaryBomb()).getBombsExploded().getChanined();
			this.bombsSummaryP2BombsExplodedFire = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player2, new SummaryBomb()).getBombsExploded().getFire();
			this.bombsSummaryP2BombsExplodedStandard = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player2, new SummaryBomb()).getBombsExploded().getStandard();
			this.bombsSummaryP2PhasesDefusedChainedB = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player2, new SummaryBomb()).getPhasesDefused().getChained().getB();
			this.bombsSummaryP2PhasesDefusedChainedR = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player2, new SummaryBomb()).getPhasesDefused().getChained().getR();
			this.bombsSummaryP2PhasesDefusedChainedG = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player2, new SummaryBomb()).getPhasesDefused().getChained().getG();
			this.bombsSummaryP2PhasesDefusedFireB = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player2, new SummaryBomb()).getPhasesDefused().getFire().getB();
			this.bombsSummaryP2PhasesDefusedFireR = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player2, new SummaryBomb()).getPhasesDefused().getFire().getR();
			this.bombsSummaryP2PhasesDefusedFireG = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player2, new SummaryBomb()).getPhasesDefused().getFire().getG();
			this.bombsSummaryP2PhasesDefusedStandardB = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player2, new SummaryBomb()).getPhasesDefused().getStandard().getB();
			this.bombsSummaryP2PhasesDefusedStandardR = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player2, new SummaryBomb()).getPhasesDefused().getStandard().getR();
			this.bombsSummaryP2PhasesDefusedStandardG = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player2, new SummaryBomb()).getPhasesDefused().getStandard().getG();
			this.bombsSummaryP2BombsDefusedChained = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player2, new SummaryBomb()).getBombsDefused().getChanined();
			this.bombsSummaryP2BombsDefusedFire = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player2, new SummaryBomb()).getBombsDefused().getFire();
			this.bombsSummaryP2BombsDefusedStandard = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player2, new SummaryBomb()).getBombsDefused().getStandard();
			this.bombsSummaryP3BombsExplodedChained = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player3, new SummaryBomb()).getBombsExploded().getChanined();
			this.bombsSummaryP3BombsExplodedFire = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player3, new SummaryBomb()).getBombsExploded().getFire();
			this.bombsSummaryP3BombsExplodedStandard = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player3, new SummaryBomb()).getBombsExploded().getStandard();
			this.bombsSummaryP3PhasesDefusedChainedB = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player3, new SummaryBomb()).getPhasesDefused().getChained().getB();
			this.bombsSummaryP3PhasesDefusedChainedR = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player3, new SummaryBomb()).getPhasesDefused().getChained().getR();
			this.bombsSummaryP3PhasesDefusedChainedG = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player3, new SummaryBomb()).getPhasesDefused().getChained().getG();
			this.bombsSummaryP3PhasesDefusedFireB = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player3, new SummaryBomb()).getPhasesDefused().getFire().getB();
			this.bombsSummaryP3PhasesDefusedFireR = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player3, new SummaryBomb()).getPhasesDefused().getFire().getR();
			this.bombsSummaryP3PhasesDefusedFireG = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player3, new SummaryBomb()).getPhasesDefused().getFire().getG();
			this.bombsSummaryP3PhasesDefusedStandardB = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player3, new SummaryBomb()).getPhasesDefused().getStandard().getB();
			this.bombsSummaryP3PhasesDefusedStandardR = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player3, new SummaryBomb()).getPhasesDefused().getStandard().getR();
			this.bombsSummaryP3PhasesDefusedStandardG = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player3, new SummaryBomb()).getPhasesDefused().getStandard().getG();
			this.bombsSummaryP3BombsDefusedChained = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player3, new SummaryBomb()).getBombsDefused().getChanined();
			this.bombsSummaryP3BombsDefusedFire = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player3, new SummaryBomb()).getBombsDefused().getFire();
			this.bombsSummaryP3BombsDefusedStandard = updatedSummaryTrial.getBombSummaryPlayer().getOrDefault(this.player3, new SummaryBomb()).getBombsDefused().getStandard();
		}
	}

	public void addAutoJump(String playerId) {
		if (this.player1.equals(playerId)) {
			this.autoJumpP1 += 1;
		} else if(this.player2.equals(playerId)) {
			this.autoJumpP2 += 1;
		} else if(this.player3.equals(playerId)) {
			this.autoJumpP3 += 1;
		}
	}

	public void addPreReconPurchase(String playerId) {
		if (this.player1.equals(playerId)) {
			this.preReconPurchaseP1 += 1;
		} else if(this.player2.equals(playerId)) {
			this.preReconPurchaseP2 += 1;
		} else if(this.player3.equals(playerId)) {
			this.preReconPurchaseP3 += 1;
		}
	}

	public void addWirecutterInventory(String playerId) {
		if (this.player1.equals(playerId)) {
			this.wirecutterInventoryP1 += 1;
		} else if(this.player2.equals(playerId)) {
			this.wirecutterInventoryP2 += 1;
		} else if(this.player3.equals(playerId)) {
			this.wirecutterInventoryP3 += 1;
		}
	}

	public void addThreePhaseBombs(String playerId) {
		if (this.player1.equals(playerId)) {
			this.threePhaseBombsP1 += 1;
		} else if(this.player2.equals(playerId)) {
			this.threePhaseBombsP2 += 1;
		} else if(this.player3.equals(playerId)) {
			this.threePhaseBombsP3 += 1;
		}
	}
}
