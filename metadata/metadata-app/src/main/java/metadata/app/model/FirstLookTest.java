package metadata.app.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class FirstLookTest {

	private boolean result;
	private Integer count;
	private String summary;
	private FirstLook firstLook;
	
	@JsonCreator
	public FirstLookTest(
			@JsonProperty("result") boolean result,
			@JsonProperty("count") Integer count,
			@JsonProperty("summary") String summary,
			@JsonProperty("item") FirstLook firstLook
			) {
		this.result = result;
		this.count = count != null ? count : 0;
        this.summary = summary;
        this.firstLook = firstLook;   
    }

	@JsonProperty("result")
	public boolean getResult() {
		return result;
	}
	@JsonProperty("result")
	public void setResult(boolean result) {
		this.result = result;
	}
	@JsonProperty("count")
	public Integer getCount() {
		return count;
	}
	@JsonProperty("count")
	public void setCount(Integer count) {
		this.count = count;
	}
	@JsonProperty("summary")
	public String getSummary() {
		return summary;
	}
	@JsonProperty("summary")
	public void setSummary(String summary) {
		this.summary = summary;
	}
	@JsonProperty("item")
	public FirstLook getFirstLook() {
		return firstLook;
	}
	@JsonProperty("item")
	public void setFirstLook(FirstLook firstLook) {
		this.firstLook = firstLook;
	}	
}