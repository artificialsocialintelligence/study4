package metadata.app.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class MessageHeartbeat {
	private Header header;
	private Msg msg;
	private DataHeartbeat data;

	@JsonCreator
	public MessageHeartbeat(
			@JsonProperty("header") Header header,
			@JsonProperty("msg") Msg msg,
			@JsonProperty("data") DataHeartbeat data
			) {
		this.header = header;
		this.msg = msg;
		this.data = data;
	}

	public MessageHeartbeat() {
		// TODO Auto-generated constructor stub
	}

	@JsonProperty("header")
	public Header getHeader() {
		return header;
	}
	@JsonProperty("header")
	public void setHeader(Header header) {
		this.header = header;
	}
	
	@JsonProperty("msg")
	public Msg getMsg() {
		return msg;
	}
	@JsonProperty("msg")
	public void setMsg(Msg msg) {
		this.msg = msg;
	}

	@JsonProperty("data")
	public DataHeartbeat getData() {
		return data;
	}
	@JsonProperty("data")
	public void setData(DataHeartbeat data) {
		this.data = data;
	}
}