package metadata.app.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DataTrialExport {
	private String index;
	private String state;
	private int records;
	private DataTrialMetadata metadata;

	@JsonCreator
	public DataTrialExport(
			@JsonProperty("index") String index,
			@JsonProperty("state") String state,
			@JsonProperty("records") int records,
			@JsonProperty("metadata") DataTrialMetadata metadata
			) {
		this.index = index;
		this.state = state;
		this.records = records;
		this.metadata = metadata;
	}

	public DataTrialExport() {
		// TODO Auto-generated constructor stub
	}

	@JsonProperty("index")
	public String getIndex() {
		return index;
	}
	@JsonProperty("index")
	public void setIndex(String index) {
		this.index = index;
	}

	@JsonProperty("state")
	public String getState() {
		return state;
	}
	@JsonProperty("state")
	public void setState(String state) {
		this.state = state;
	}

	@JsonProperty("records")
	public int getRecords() {
		return records;
	}
	@JsonProperty("records")
	public void setRecords(int records) {
		this.records = records;
	}

	@JsonProperty("metadata")
	public DataTrialMetadata getMetadata() {
		return metadata;
	}
	@JsonProperty("metadata")
	public void setMetadata(DataTrialMetadata metadata) {
		this.metadata = metadata;
	}
}
