package metadata.app.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class TrialSurveyData {
    @JsonProperty(value = "image_asset")
    private String imageAsset;
    @JsonProperty(value = "response")
    private String response;
    @JsonProperty(value = "input_type")
    private String inputType;
    @JsonProperty(value = "text")
    private String text;
    @JsonProperty(value = "qid")
    private String qId;
    @JsonProperty(value = "labels")
    private List<String> labels;

    public String getImageAsset() {
        return imageAsset;
    }

    public void setImageAsset(String imageAsset) {
        this.imageAsset = imageAsset;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getInputType() {
        return inputType;
    }

    public void setInputType(String inputType) {
        this.inputType = inputType;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getQId() {
        return qId;
    }

    public void setQId(String qId) {
        this.qId = qId;
    }

    public List<String> getLabels() {
        return labels;
    }

    public void setLabels(List<String> labels) {
        this.labels = labels;
    }
}
