package metadata.app;

import ch.qos.logback.classic.Level;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import io.micronaut.context.ApplicationContext;
import io.micronaut.runtime.Micronaut;
import metadata.app.appender.MqttAppender;

public class Application {

	private static String OS = System.getProperty("os.name").toLowerCase();
    public static boolean IS_WINDOWS = (OS.indexOf("win") >= 0);
    public static boolean IS_MAC = (OS.indexOf("mac") >= 0);
    public static boolean IS_UNIX = (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0);
    public static boolean IS_SOLARIS = (OS.indexOf("sunos") >= 0);

    public static void main(String[] args) {
    	ApplicationContext context = Micronaut.run(Application.class);

    	LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
    	MqttAppender mqttAppender = context.getBean(MqttAppender.class); //new MqttAppender();
    	mqttAppender.setContext(loggerContext);

//    	MetadataLogClient metadataLogClient = context.getBean(MetadataLogClient.class);
//    	mqttAppender.setMetadataLogClient(metadataLogClient);

    	mqttAppender.start();

    	Logger logbackLogger = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
//		logbackLogger.setLevel(Level.DEBUG);
    	logbackLogger.addAppender((Appender<ILoggingEvent>)mqttAppender);
    	logbackLogger.setAdditive(true);
    }
}
