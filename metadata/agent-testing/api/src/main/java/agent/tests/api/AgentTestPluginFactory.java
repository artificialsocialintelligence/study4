package agent.tests.api;

import org.pf4j.DefaultPluginFactory;
import org.pf4j.Plugin;
import org.pf4j.PluginWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Constructor;

class AgentTestPluginFactory extends DefaultPluginFactory {

    private static final Logger log = LoggerFactory.getLogger(AgentTestPluginFactory.class);

    @Override
    protected Plugin createInstance(Class<?> pluginClass, PluginWrapper pluginWrapper) {
        PluginContext context = new PluginContext(pluginWrapper.getRuntimeMode());
        try {
            Constructor<?> constructor = pluginClass.getConstructor(PluginContext.class);
            return (Plugin) constructor.newInstance(context);
        } catch (Exception e) {
//            log.error(e.getMessage(), e);
            log.error(e.getMessage());
        }

        return null;
    }

}
