package agent.tests.api;

import java.util.ArrayList;
import java.util.List;

public class AgentResultHelper {

    private final List<AgentTestResultRow> agentResults;

    public AgentResultHelper() {
        this.agentResults = new ArrayList<>();
    }

    public void addResult(String name, boolean result, String data, String predicate) {
        this.agentResults.add(new AgentTestResultRow(name, result, data, predicate));
    }

    public List<AgentTestResultRow> getAgentTestResultRows() {
        return agentResults;
    }
}
