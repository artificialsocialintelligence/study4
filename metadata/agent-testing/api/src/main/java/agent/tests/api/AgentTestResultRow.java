package agent.tests.api;

//import io.micronaut.core.annotation.Introspected;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

//@Introspected
public class AgentTestResultRow {

	@CsvBindByPosition(position = 0)
	@CsvBindByName(column = "Name")
	private String name;
	@CsvBindByName(column = "Result")
	@CsvBindByPosition(position = 1)
	private boolean result;
	@CsvBindByName(column = "Data")
	@CsvBindByPosition(position = 2)
	private String data;
	@CsvBindByName(column = "Predicate")
	@CsvBindByPosition(position = 3)
	private String predicate;

	public AgentTestResultRow(String name, boolean result, String data, String predicate) {
		this.name = name;
		this.result = result;
		this.data = data;
		this.predicate = predicate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean getResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getPredicate() {
		return predicate;
	}

	public void setPredicate(String predicate) {
		this.predicate = predicate;
	}

	@Override
	public String toString() {
		return "{" +
				"\"name\": " + this.getName() + "," +
				"\"result\": " + this.getResult() + "," +
				"\"data\": " + this.getData() + "," +
				"\"predicate\": " + this.getPredicate() +
				"}";
	}
}
