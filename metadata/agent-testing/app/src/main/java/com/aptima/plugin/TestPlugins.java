package com.aptima.plugin;

import agent.tests.api.AgentTest;
import agent.tests.api.AgentTestPluginManager;
import agent.tests.api.AgentTestResultRow;
import agent.tests.api.CustomMappingStrategy;
import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import dnl.utils.text.table.TextTable;
import dnl.utils.text.table.csv.CsvTableModel;
import org.apache.commons.cli.*;
import org.apache.commons.lang3.StringUtils;
import org.pf4j.PluginManager;
import org.pf4j.PluginWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class TestPlugins {

    private static final Logger log = LoggerFactory.getLogger(TestPlugins.class);

    public static void main(String[] args) {
        // create the command line parser
        HelpFormatter formatter = new HelpFormatter();
        CommandLineParser parser = new DefaultParser();
        Options options = new Options();
        options.addOption("p", "plugins", true,"folder for the plugin jar files");
        options.addOption("e", "enable", true,"comma delimited list of plugins to enable for testing");

        // create the plugin manager
        // the path should point to the metadata plugins folder where the built plugins are stored.
        // make sure to run 'mvn clean package' on agent-testing to create plugin jars prior to running this test
        try {
            // parse the command line arguments
            CommandLine commandLine = parser.parse(options, args);
            if (!commandLine.hasOption("plugins")) {
                // print the value of block-size
                log.error("plugins command line option is required!");
                formatter.printHelp("test plugins", options);
                System.exit(1);
            }
            String metadataFilePath = commandLine.getOptionValue("plugins");
            if (!commandLine.hasOption("enable")) {
                // print the value of block-size
                log.error("at least 1 plugin needs to be enabled!");
                formatter.printHelp("test plugins", options);
                System.exit(1);
            }
            String enabledPluginsStr = commandLine.getOptionValue("enable");
            String[] enabledPlugins = enabledPluginsStr.split(",");

            long startTime = System.nanoTime();

            Path pluginPath = Paths.get(System.getProperty("user.dir"), "../metadata-app/plugins");
            log.info("Plugin path: {}", pluginPath);
            PluginManager pluginManager = new AgentTestPluginManager(pluginPath);

            // load the plugins
            pluginManager.loadPlugins();

            // enable only this plugin for testing.
            boolean enableAllPlugins = false;
            if (enabledPlugins.length == 1) {
                if (enabledPlugins[0].equalsIgnoreCase("all")) {
                    enableAllPlugins = true;
                }
            }
            if (!enableAllPlugins) {
                List<String> enablePluginIds = Arrays.asList(enabledPlugins);
                pluginManager.getPlugins().forEach(plugin -> {
                    if (enablePluginIds.contains(plugin.getPluginId())) {
                        pluginManager.enablePlugin(plugin.getPluginId());
                    } else {
                        pluginManager.disablePlugin(plugin.getPluginId());
                    }
                });
            }

            // start (active/resolved) the plugins
            pluginManager.startPlugins();

            List<AgentTest> agentTests = pluginManager.getExtensions(AgentTest.class);

            Path path = Path.of(metadataFilePath);
            File file = path.toFile();
            log.info("Using metadata file {}", path.toString());
            int lineCount = 0;
            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    for (AgentTest agentTest : agentTests) {
                        agentTest.evaluate(line);
                    }
                    lineCount++;
                }
            } catch (IOException e) {
                log.error(e.getMessage());
            }
            long endTime = System.nanoTime();
            long duration = (endTime - startTime);
            double d = duration / (double) TimeUnit.SECONDS.toNanos(1);
            log.info("Finished agent testing for [{}] messages in [{}] seconds.", lineCount, d);
            List<AgentTestResultRow> rows = new ArrayList<>();
            for (AgentTest agentTest : agentTests) {
                List<AgentTestResultRow> results = agentTest.getResults();
                rows.addAll(results);
            }
            // stop the plugins
            pluginManager.stopPlugins();

            // Dump CSV to console
            CustomMappingStrategy<AgentTestResultRow> beanStrategyMeasuresTrial = new CustomMappingStrategy<>();
            beanStrategyMeasuresTrial.setType(AgentTestResultRow.class);

            StringWriter stringWriter = new StringWriter();
            CSVWriter csvWriterMeasuresTrial = new CSVWriter(stringWriter);

            StatefulBeanToCsv<AgentTestResultRow> beanToCsvTrialMeasures = new StatefulBeanToCsvBuilder<AgentTestResultRow>(csvWriterMeasuresTrial)
                    .withMappingStrategy(beanStrategyMeasuresTrial)
                    .build();
            beanToCsvTrialMeasures.write(rows);
            log.info("");
            CsvTableModel csvTableModel = new CsvTableModel(stringWriter.toString());
            TextTable tt = new TextTable(csvTableModel);
            tt.printTable();
        } catch (Exception e) {
            log.error(e.getMessage());
        }

    }

}
