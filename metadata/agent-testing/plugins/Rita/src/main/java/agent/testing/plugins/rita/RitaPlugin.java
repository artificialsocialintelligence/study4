package agent.testing.plugins.rita;

import agent.tests.api.*;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;
import org.pf4j.Extension;

import java.text.MessageFormat;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * Location Monitor plugin.
 *
 * @author Aptima, Inc.
 */
public class RitaPlugin extends AgentPlugin {

    public RitaPlugin(PluginContext context) {
        super(context);
    }

    @Override
    public void start() {
        log.debug("{} started", getClass().getSimpleName() );
    }

    @Override
    public void stop() {
        log.debug("{} started", getClass().getSimpleName() );
    }

    @Extension
    public static class CognitiveTest extends AgentResultHelper implements AgentTest {
        Configuration conf = Configuration.defaultConfiguration().addOptions(Option.SUPPRESS_EXCEPTIONS);
        boolean isTrainingMission = true;
        int num_chat_messages = 0;
        @Override
        public String getAgentName() {
            return "ASI_DOLL_TA1_Rita";
        }

        @Override
        public void evaluate(String line) {
            DocumentContext jsonContext = JsonPath.using(conf).parse(line);
            String topic = jsonContext.read("$.topic");
            String message_type = jsonContext.read("$.header.message_type");
            String sub_type = jsonContext.read("$.msg.sub_type");
            String experiment_mission = jsonContext.read("$.data.experiment_mission");
            if (Stream.of(topic, message_type, sub_type, experiment_mission).noneMatch(Objects::isNull)) {
                if (topic.equalsIgnoreCase("trial")
                        && message_type.equalsIgnoreCase("trial")
                        && sub_type.equalsIgnoreCase("start")) {
                    if (!experiment_mission.contains("training")) {
                        isTrainingMission = false;
                    }
                }
            }
            if (!isTrainingMission) {
                if (topic != null) {
                    if (topic.equalsIgnoreCase("agent/intervention/asi_doll_ta1_rita/chat")) {
                        num_chat_messages += 1;
                    }
                }
            }
        }

        @Override
        public List<AgentTestResultRow> getResults() {
            boolean result1 = false;
            if (!isTrainingMission) {
                if (num_chat_messages >= 5) {
                    result1 = true;
                }
            } else {
                result1 = true;
            }

            this.addResult(MessageFormat.format("{0}_{1}", this.getAgentName(), this.getAgentTestResultRows().size()), result1, MessageFormat.format("{0} : {1}","# chats in mission", num_chat_messages), "message >= 5 at the end of a non training mission" );

            return this.getAgentTestResultRows();
        }
    }

}
