package agent.testing.plugins.dialog;

import agent.tests.api.*;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;
import org.pf4j.Extension;

import java.text.MessageFormat;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * Location Monitor plugin.
 *
 * @author Aptima, Inc.
 */
public class DialogPlugin extends AgentPlugin {

    public DialogPlugin(PluginContext context) {
        super(context);
    }

    @Override
    public void start() {
        log.debug("{} started", getClass().getSimpleName() );
    }

    @Override
    public void stop() {
        log.debug("{} started", getClass().getSimpleName() );
    }

    @Extension
    public static class FlockingTest extends AgentResultHelper implements AgentTest {
        Configuration conf = Configuration.defaultConfiguration().addOptions(Option.SUPPRESS_EXCEPTIONS);
        int num_messages = 0;
        int trial_start = 0;
        int trial_stop = 0;
        int rollcall_request = 0;
        int chat = 0;
        int asr = 0;
        int dialog = 0;
        int heartbeats = 0;
        int version_info = 0;
        int rollcall_response = 0;
        @Override
        public String getAgentName() {
            return "Dialog";
        }

        @Override
        public void evaluate(String line) {
            DocumentContext jsonContext = JsonPath.using(conf).parse(line);
            String topic = jsonContext.read("$.topic");
            String message_type = jsonContext.read("$.header.message_type");
            String source = jsonContext.read("$.msg.source");
            String sub_type = jsonContext.read("$.msg.sub_type");
            // Record valid single-line JSON messages
            if (Stream.of(topic, message_type, source, sub_type).noneMatch(Objects::isNull)) {
                num_messages += 1;
                // subscribed Trial message
                if (topic.equalsIgnoreCase("trial")
                        && message_type.equalsIgnoreCase("trial")) {
                    if (sub_type.equalsIgnoreCase("start")) {
                        trial_start += 1;
                    } else if (sub_type.equalsIgnoreCase("stop")) {
                        trial_stop += 1;
                    }
                }
                // subscribed Rollcall Request message
                else if (topic.equalsIgnoreCase("agent/control/rollcall/request")
                        && message_type.equalsIgnoreCase("agent")
                        && sub_type.equalsIgnoreCase("rollcall:request")) {
                    rollcall_request += 1;
                }
                // subscribed Chat message
                else if (topic.equalsIgnoreCase("minecraft/chat")
                        && message_type.equalsIgnoreCase("chat")
                        && sub_type.equalsIgnoreCase("Event:Chat")) {
                    chat += 1;
                }
                // subscribed ASR message
                else if (topic.equalsIgnoreCase("agent/asr/final")
                        && message_type.equalsIgnoreCase("observation")
                        && sub_type.equalsIgnoreCase("asr:transcription")) {
                    asr += 1;
                }
                // published Dialog Agent message
                else if (topic.equalsIgnoreCase("agent/dialog")
                        && message_type.equalsIgnoreCase("event")
                        && sub_type.equalsIgnoreCase("Event:dialogue_event")
                        && source.equalsIgnoreCase("uaz_dialog_agent")) {
                    dialog += 1;
                }
                // published Heartbeat message
                else if (topic.equalsIgnoreCase("agent/uaz_dialog_agent/heartbeats")
                        && message_type.equalsIgnoreCase("status")
                        && sub_type.equalsIgnoreCase("heartbeat")
                        && source.equalsIgnoreCase("uaz_dialog_agent")) {
                    heartbeats += 1;
                }
                // published Version Info message
                else if (topic.equalsIgnoreCase("agent/tomcat_textAnalyzer/versioninfo")
                        && message_type.equalsIgnoreCase("agent")
                        && sub_type.equalsIgnoreCase("versioninfo")
                        && source.equalsIgnoreCase("uaz_dialog_agent")) {
                    version_info += 1;
                }
                // published Rollcall Response message
                else if (topic.equalsIgnoreCase("agent/control/rollcall/response")
                        && message_type.equalsIgnoreCase("agent")
                        && sub_type.equalsIgnoreCase("rollcall:response")
                        && source.equalsIgnoreCase("uaz_dialog_agent")) {
                    rollcall_response += 1;
                }
            }
        }

        @Override
        public List<AgentTestResultRow> getResults() {
            // TEST 0:  The number of dialog messages must equal the number
            //          of chat and final ASR messages
            this.addResult(MessageFormat.format("{0}_{1}", this.getAgentName(),
                            this.getAgentTestResultRows().size()),
                    dialog == asr + chat,
                    MessageFormat.format("{0} : {1}","# dialog", dialog),
                    "# dialog == asr + chat" );
            // TEST 1:  The number of version_info messages must equal the number
            //          of trial_start messages
            this.addResult(MessageFormat.format("{0}_{1}", this.getAgentName(),
                            this.getAgentTestResultRows().size()),
                    version_info == trial_start,
                    MessageFormat.format("{0} : {1}","# version_info", version_info),
                    "# version_info == trial_start" );
            // TEST 2:  The number of rollcall_response messages must equal the
            //          rollcall_request messages
            this.addResult(MessageFormat.format("{0}_{1}", this.getAgentName(),
                            this.getAgentTestResultRows().size()),
                    rollcall_response == rollcall_request,
                    MessageFormat.format("{0} : {1}","# rollcall_response", rollcall_response),
                    "# rollcall_response == rollcall_response" );
            // TEST 3:  The number of heartbeat messages must be greater than zero
            this.addResult(MessageFormat.format("{0}_{1}", this.getAgentName(),
                            this.getAgentTestResultRows().size()),
                    heartbeats > 0,
                    MessageFormat.format("{0} : {1}","# heartbeats", heartbeats),
                    "# heartbeats > 0" );
            // TEST 4:  The number of messages must be greater than zero
            // a degenerate case, only include if it fails.
            if (num_messages <= 0) {
                this.addResult(MessageFormat.format("{0}_{1}", this.getAgentName(),
                                this.getAgentTestResultRows().size()),
                        num_messages > 0,
                        MessageFormat.format("{0} : {1}", "# num_messages", num_messages),
                        "# num_messages > 0");
            }
            return this.getAgentTestResultRows();
        }
    }

}
