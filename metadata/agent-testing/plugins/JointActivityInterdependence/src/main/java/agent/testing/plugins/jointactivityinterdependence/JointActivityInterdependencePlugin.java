package agent.testing.plugins.jointactivityinterdependence;

import agent.tests.api.*;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;
import org.pf4j.Extension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.List;

/**
 * Location Monitor plugin.
 *
 * @author Aptima, Inc.
 */
public class JointActivityInterdependencePlugin extends AgentPlugin {

    private static final Logger log = LoggerFactory.getLogger(JointActivityInterdependencePlugin.class);
    public JointActivityInterdependencePlugin(PluginContext context) {
        super(context);
    }

    @Override
    public void start() {
        log.debug("{} started", getClass().getSimpleName() );
    }

    @Override
    public void stop() {
        log.debug("{} started", getClass().getSimpleName());
    }

    @Extension
    public static class LocationMonitorTest extends AgentResultHelper implements AgentTest {
        Configuration conf = Configuration.defaultConfiguration().addOptions(Option.SUPPRESS_EXCEPTIONS);
        int num_jag_messages = 0;
        @Override
        public String getAgentName() {
            return "AC_IHMC_TA2_Joint-Activity-Interdependence";
        }

        @Override
        public void evaluate(String line) {
            DocumentContext jsonContext = JsonPath.using(conf).parse(line);
            String topic = jsonContext.read("$.topic");
            if (topic != null) {
                if (topic.equalsIgnoreCase("observations/events/player/jag")) {
                    num_jag_messages += 1;
                }
            }
        }

        @Override
        public List<AgentTestResultRow> getResults() {
            boolean result1 = (num_jag_messages >= 2000);

            this.addResult(MessageFormat.format("{0}_{1}", this.getAgentName(), this.getAgentTestResultRows().size()), result1, MessageFormat.format("{0} : {1}","# jag messages", num_jag_messages), "# messages >= 2000" );

            return this.getAgentTestResultRows();
        }
    }
}
