package agent.testing.plugins.locationmonitor;

import agent.tests.api.*;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;
import org.pf4j.Extension;

import java.text.MessageFormat;
import java.util.List;

/**
 * Location Monitor plugin.
 *
 * @author Aptima, Inc.
 */
public class LocationMonitorPlugin extends AgentPlugin {

    public LocationMonitorPlugin(PluginContext context) {
        super(context);
    }

    @Override
    public void start() {
        log.debug("{} started", getClass().getSimpleName() );
    }

    @Override
    public void stop() {
        log.debug("{} started", getClass().getSimpleName() );
    }

    @Extension
    public static class LocationMonitorTest extends AgentResultHelper implements AgentTest {
        Configuration conf = Configuration.defaultConfiguration().addOptions(Option.SUPPRESS_EXCEPTIONS);
        int num_map_init_messages = 0;
        int num_loc_messages = 0;
        @Override
        public String getAgentName() {
            return "AC_IHMC_TA2_Location-Monitor";
        }

        @Override
        public void evaluate(String line) {
            DocumentContext jsonContext = JsonPath.using(conf).parse(line);
            String topic = jsonContext.read("$.topic");
            if (topic != null) {
                if (topic.equalsIgnoreCase("ground_truth/semantic_map/initialized")) {
                    num_map_init_messages += 1;
                } else if(topic.equalsIgnoreCase("observations/events/player/location")) {
                    num_loc_messages += 1;
                }
            }
        }

        @Override
        public List<AgentTestResultRow> getResults() {
            boolean result1 = (num_map_init_messages == 1);
            boolean result2 = (num_loc_messages >= 500);

            this.addResult(MessageFormat.format("{0}_{1}", this.getAgentName(), this.getAgentTestResultRows().size()), result1, MessageFormat.format("{0} : {1}","# num sem_map_ini messages", num_map_init_messages), "# messages == 1" );
            this.addResult(MessageFormat.format("{0}_{1}", this.getAgentName(), this.getAgentTestResultRows().size()), result2, MessageFormat.format("{0} : {1}","# num player loc messages", num_loc_messages), "# loc messages >= 500");

            return this.getAgentTestResultRows();
        }
    }

}
