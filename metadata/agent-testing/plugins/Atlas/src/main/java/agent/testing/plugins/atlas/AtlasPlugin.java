package agent.testing.plugins.atlas;

import agent.tests.api.*;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;
import org.pf4j.Extension;

import java.text.MessageFormat;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * Location Monitor plugin.
 *
 * @author Aptima, Inc.
 */
public class AtlasPlugin extends AgentPlugin {

    public AtlasPlugin(PluginContext context) {
        super(context);
    }

    @Override
    public void start() {
        log.debug("{} started", getClass().getSimpleName() );
    }

    @Override
    public void stop() {
        log.debug("{} started", getClass().getSimpleName() );
    }

    @Extension
    public static class FlockingTest extends AgentResultHelper implements AgentTest {
        Configuration conf = Configuration.defaultConfiguration().addOptions(Option.SUPPRESS_EXCEPTIONS);
        boolean isTrainingMission = true;
        boolean mission_running = false;
        int elapsed_milliseconds_global = 0;
        int num_chat_messages = 0;
        int num_chat_messages_two_and_a_half = 0;
        int two_and_a_half_min_in_milliseconds = (1000*60*2) + (30*1000);
        @Override
        public String getAgentName() {
            return "ASI_CMU_TA1_ATLAS";
        }

        @Override
        public void evaluate(String line) {
            DocumentContext jsonContext = JsonPath.using(conf).parse(line);
            String topic = jsonContext.read("$.topic");
            String message_type = jsonContext.read("$.header.message_type");
            String sub_type = jsonContext.read("$.msg.sub_type");
            String experiment_mission = jsonContext.read("$.data.experiment_mission");
            if (Stream.of(topic, message_type, sub_type, experiment_mission).noneMatch(Objects::isNull)) {
                if (topic.equalsIgnoreCase("trial")
                        && message_type.equalsIgnoreCase("trial")
                        && sub_type.equalsIgnoreCase("start")) {
                    if (!experiment_mission.contains("training")) {
                        isTrainingMission = false;
                    }
                }
            }
            if (!isTrainingMission) {
                if (topic != null) {
                    if (topic.equalsIgnoreCase("observations/events/mission")) {
                        String missionState = jsonContext.read("$.data.mission_state");
                        if (missionState != null) {
                            mission_running = missionState.equalsIgnoreCase("start");
                        }
                    } else if (mission_running) {
                        if (topic.equalsIgnoreCase("observations/state")) {
                            elapsed_milliseconds_global = jsonContext.read("$.data.elapsed_milliseconds_global");
                        } else if (topic.equalsIgnoreCase("agent/intervention/asi_cmu_ta1_atlas/chat")) {
                            if (elapsed_milliseconds_global <= two_and_a_half_min_in_milliseconds) {
                                num_chat_messages_two_and_a_half += 1;
                            }
                            num_chat_messages += 1;
                        }
                    }
                }
            }
        }

        @Override
        public List<AgentTestResultRow> getResults() {
            boolean result1 = false;
            if (!isTrainingMission) {
                if (num_chat_messages >= 0) {
                    result1 = true;
                }
            } else {
                result1 = true;
            }

            boolean result2 = false;
            if (!isTrainingMission) {
                if (num_chat_messages_two_and_a_half > 0) {
                    result2 = true;
                }
            }
            else {
                result2 = true;
            }

            this.addResult(MessageFormat.format("{0}_{1}", this.getAgentName(), this.getAgentTestResultRows().size()), result1, MessageFormat.format("{0} : {1}","# chats in mission", num_chat_messages), "messages > 0 if trial != Training" );
            this.addResult(MessageFormat.format("{0}_{1}", this.getAgentName(), this.getAgentTestResultRows().size()), result2, MessageFormat.format("{0} : {1}","# chats within 2.5", num_chat_messages_two_and_a_half), "# messages > 0 within 2.5 minutes of mission start if trial != Training" );

            return this.getAgentTestResultRows();
        }
    }

}
