package com.asist.asistmod.MissionCommon.BlockPlacement;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.MissionCommon.Utilities.AttributeManagement.AttributeSetCreator;
import com.asist.asistmod.MissionCommon.Utilities.AttributeManagement.AttributedObjectManager;
import com.asist.asistmod.MissionSpecific.MissionA.BombManagement.BombBookkeeper;
import com.asist.asistmod.MissionSpecific.MissionA.RegionManagement.RegionManager;
import com.asist.asistmod.datamodels.EnvironmentCreated.List.EnvironmentCreatedListModel;
import com.asist.asistmod.datamodels.MapBlock.MapBlockModel;
import com.asist.asistmod.datamodels.ObjectStateChange.ObjectStateChangeData;
import com.asist.asistmod.mqtt.InternalMqttClient;
import com.asist.asistmod.tile_entity.BombBlockTileEntity;

import net.minecraft.block.state.IBlockState;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.WorldServer;
//import net.minecraftforge.fml.common.FMLLog;
import net.minecraftforge.fml.common.registry.ForgeRegistries;


public class MapBlockManager {
	
	public static enum MapBlockMode{MISSION_START,PERTURBATION}
	
	public static boolean publishedEnvironmentCreatedListOnce = false;


	public static void addBlocksFromFile(String blockFilename, WorldServer world, MapBlockMode mode) {		
		        
		Iterator<MapBlockModel> csvUserIterator = MapBlockManager.readAllDataAtOnce(blockFilename);
        
		//System.out.println("Adding MapBlocks from " + blockFilename);
        

        List<ObjectStateChangeData> environmentCreatedList = new ArrayList<ObjectStateChangeData>();
        while (csvUserIterator.hasNext()) {
        	

        	
        	MapBlockModel mapBlockModel = csvUserIterator.next();
        	 
        	String msg = "Location: " + mapBlockModel.getLocationXYZ() + "    BlockType = " + mapBlockModel.getBlockType() +
        			 "    FeatureType = " + mapBlockModel.getFeatureType() + "    Sequence = " + mapBlockModel.getSequence() +
					"    BombID = " + mapBlockModel.getBombID() +  "    ChainedID = " + mapBlockModel.getChainedID() +
					"    FuseStartTime = " + mapBlockModel.getFuseStartTime();
        	
        	//System.out.println(msg); 
            
            String[] coords = mapBlockModel.getLocationXYZ().split(" ");
            int x = Integer.parseInt(coords[0]);
            int y = Integer.parseInt(coords[1]);
            int z = Integer.parseInt(coords[2]);
            String blockType = mapBlockModel.getBlockType();            
            IBlockState block;            
          
            if (coords.length == 3) {
            	try {
	            	//System.out.println("Block ID :" + mapBlockModel.getBombID());
	            	
	            	// FOR ASISTMOD CUSTOM BLOCKS
	            	if( blockType.contains("block_bomb") ) {
	            		String sequence = mapBlockModel.getSequence();
	            		block = ForgeRegistries.BLOCKS.getValue(new ResourceLocation("asistmod", mapBlockModel.getBlockType())).getDefaultState();
	            		BlockPos blockPos = new BlockPos(x, y, z);
	            		
	            		try {
	            			world.setBlockState(blockPos, block);
	            			BombBlockTileEntity te = (BombBlockTileEntity)world.getTileEntity(blockPos);
	            			if(te != null) {
	            				String fuseStartTimeString = mapBlockModel.getFuseStartTime();
	            				// SET TILE ENTITY
	            				te.setDefuseSequence(sequence);
	            				String id = te.setBombId(mapBlockModel.getBombID());
	            				te.setFuseDuration(AsistMod.modSettings.bomb_timer_seconds);
								te.setFuseStartTime( fuseStartTimeString ) ;
								
								te.setChainedId(mapBlockModel.getChainedID());
								
								// TODO: ADD FUSE_DURATION ATTRIBUTE
								// SET CUSTOM ATTRIBUTES
								AttributedObjectManager.initAttributedObject( id, AttributeSetCreator.createBombAttributeSet("false",sequence,"0","INACTIVE", fuseStartTimeString) );
								
								// build data model for environment created list
								ObjectStateChangeData objectStateChangeData = new ObjectStateChangeData(false);
								
								objectStateChangeData.id = id;
								objectStateChangeData.type = blockType;
								objectStateChangeData.x = x;
								objectStateChangeData.y = y;
								objectStateChangeData.z = z;
								objectStateChangeData.currAttributes = AttributedObjectManager.getObjectAttributes(objectStateChangeData.id);
								
								environmentCreatedList.add(objectStateChangeData);

								BombBookkeeper.addBombToTotal(id, blockPos);
								RegionManager.addBombToRegion(id, blockPos);

								System.out.println( "Added " + objectStateChangeData.id );

	            			}
	            		}	            		
	            		catch(Exception e) {
	            			//System.out.println("Problem placing block : \n" );
	            			e.printStackTrace();
	            		}
	            	}
	            	
	            	// FOR MINECRAFT BLOCKS
	            	else {            		
	            		block = ForgeRegistries.BLOCKS.getValue(new ResourceLocation("minecraft", mapBlockModel.getBlockType())).getDefaultState();            		
	            		BlockPos blockPos = new BlockPos(Integer.parseInt(coords[0]), Integer.parseInt(coords[1]), Integer.parseInt(coords[2]));
	            		world.setBlockState(blockPos, block);
	            		
	            		// ADD TO ENVIRONMENT CREATED LIST
	            		
	            	}	
	    			
				}catch(Exception e){
					//System.out.println("MAPBLOCKS ERROR @ #"+ mapBlockModel.getBombID());
					e.printStackTrace();
				}           	
            	
        	}
        }

        if( !publishedEnvironmentCreatedListOnce) {
        	
        	EnvironmentCreatedListModel envCreatedMessage= new EnvironmentCreatedListModel("SERVER",environmentCreatedList);		
        	
        	InternalMqttClient.publish(envCreatedMessage.toJsonString(), "groundtruth/"+envCreatedMessage.getTopic());  
        	
        	publishedEnvironmentCreatedListOnce = true;
        	
        }
		
    	
    	// Sets total bombs per region
    	RegionManager.setRegionTotals();

	}

    public static Iterator<MapBlockModel> readAllDataAtOnce(String file) 
    { 
    	List<MapBlockModel> modelList = new ArrayList<MapBlockModel>();
        try { 
        	
        	//String[] properties = null;
            BufferedReader br = new BufferedReader(new FileReader(file)); 
            int index = 0;             
            String line = br.readLine();
            while( line != null) {
            	////System.out.println( line );
            	String[] split = line.split(",");
            	//System.out.println( Arrays.toString(split) ) ;
            	if(index == 0) {
            		//properties = split;
            	}
            	else {
            		MapBlockModel model = new MapBlockModel();            		
        			model.setLocationXYZ(split[0]);
        			model.setBlockType(split[1]);
        			model.setFeatureType(split[2]);
        			model.setSequence(split[3]);
					model.setBombID(split[4]);
					model.setFuseStartTime(split[5]);
					model.setChainedID(split[6]);
					
        			modelList.add(model);
            		
            	}            	
            	line = br.readLine();
            	index++;
            }
            br.close();
        } 
        catch (Exception e) { 
        	//System.out.println("--MAPBLOCKS ERROR - #0");
            e.printStackTrace(); 
            //return null;
        }
        return modelList.iterator();
    }

}
