package com.asist.asistmod.datamodels.EnvironmentRemoved.Single;

import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimer;
import com.asist.asistmod.datamodels.ObjectStateChange.ObjectStateChangeData;

public class EnvironmentRemovedSingleData {
	
	public String mission_timer = MissionTimer.getMissionTimeString();
	public long elapsed_milliseconds = MissionTimer.getElapsedMillisecondsGlobal();
	public String triggering_entity = "NOT_SET";
	public ObjectStateChangeData obj = new ObjectStateChangeData(true);;

}
