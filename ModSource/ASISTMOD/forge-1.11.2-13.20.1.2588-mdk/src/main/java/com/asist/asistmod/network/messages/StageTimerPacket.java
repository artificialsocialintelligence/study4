package com.asist.asistmod.network.messages;


import com.asist.asistmod.network.MessagePacket;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;

public class StageTimerPacket extends MessagePacket<StageTimerPacket> {
	
	int minutes=0;
	int seconds=0;
	
	public StageTimerPacket() {}
	
	public StageTimerPacket(int minutes, int seconds) {
		
		this.minutes = minutes;
		this.seconds = seconds;
		
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		// TODO Auto-generated method stub
		this.minutes = buf.readInt();
		this.seconds = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		// TODO Auto-generated method stub
		buf.writeInt(minutes);
		buf.writeInt(seconds);
	}

	@Override
	public void handleClientSide(StageTimerPacket message, EntityPlayer player) {
		// TODO Auto-generated method stub

		
			//RenderOverlayEventHandler.onFieldTimeChange(message.minutes,message.seconds);
		
		
		
	}

	@Override
	public void handleServerSide(StageTimerPacket message, EntityPlayer player) {
		// TODO Auto-generated method stub
		
	}

}
