package com.asist.asistmod.tile_entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.MissionCommon.Utilities.AttributeManagement.AttributedObjectManager;
import com.asist.asistmod.MissionCommon.Utilities.AttributeManagement.AttributedObjectType;
import com.asist.asistmod.MissionSpecific.MissionA.BombManagement.BombBookkeeper;
import com.asist.asistmod.MissionSpecific.MissionA.BombManagement.Explosions.ExplosionManager;
import com.asist.asistmod.MissionSpecific.MissionA.BombRemovedBroadcasting.BombRemovedListener;
import com.asist.asistmod.MissionSpecific.MissionA.BombRemovedBroadcasting.BombRemovedBroadcaster;
import com.asist.asistmod.MissionSpecific.MissionA.FireManagement.FireManager;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.PlayerManager;
import com.asist.asistmod.MissionSpecific.MissionA.RegionManagement.RegionManager;
import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimer;
import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimerListener;
import com.asist.asistmod.block.bombs.BlockBombBase;
import com.asist.asistmod.block.bombs.BlockBombFire;
import com.asist.asistmod.block.bombs.BlockBombStandard;
import com.asist.asistmod.datamodels.CommunicationEnvironment.CommunicationEnvironmentModel;
import com.asist.asistmod.datamodels.EnvironmentRemoved.Single.EnvironmentRemovedSingleModel;
import com.asist.asistmod.datamodels.ObjectStateChange.ObjectStateChangeModel;
import com.asist.asistmod.mqtt.InternalMqttClient;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ITickable;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

public class BombBlockTileEntity extends TileEntity implements ITickable, MissionTimerListener, BombRemovedListener {


	char[] defuseSequence = {};
	int defuseIndex = 0;
	String id = "NOT SET";
	String chainedId = "NONE";
	private boolean countingDown = false;

	private boolean begunCountingDown = false;
	public boolean isPerturbationTrigger = false;

	private int fuseDuration = 15;
	private int fuseRemaining = 15;
	private int fuseStartTime = -1;
	
	
	private IBlockState blockState = null;

	List<String> contributingParticipants = new ArrayList<>();

	@Override
	public void onBombRemoved(String bomb_id) {
		if(this.chainedId.equals(bomb_id))
		{
			chainedId = "NONE";
		}
	}


	private enum flashSpeedEnum {
		FLASH_OFF,
		FLASH_SLOW,
		FLASH_MEDIUM,
		FLASH_FAST
	}
	
	private flashSpeedEnum current_flash_speed = flashSpeedEnum.FLASH_OFF;
	
	private int ticks = 0;
	
	ConcurrentHashMap<String,String> attributes = new ConcurrentHashMap<String,String>();


	public BombBlockTileEntity() {
		
		MissionTimer.subscribeToTimerTicks(this);
		
		BombRemovedBroadcaster.subscribeToOnRemoved(this);
		
	}

	public void addParticipantContribution(String participant_id)
	{
		if(!contributingParticipants.contains(participant_id))
		{
			contributingParticipants.add(participant_id);
		}
	}

	public List<String> getContributingParticipants()
	{
		return contributingParticipants;
	}
	
	public void setAsPerturbationTrigger() {
		this.isPerturbationTrigger = true;
		this.writeToNBT( this.createNBTTagCompound() );
	}
	
	


	// Called once per tick ( 20 tps )
	@Override
	public void update(){
		
		if(!world.isRemote) {
			
			if (countingDown) {
				
				ticks++;
				
				if( (ticks%12 == 6)  && (this.current_flash_speed == flashSpeedEnum.FLASH_SLOW) ) {
					//AsistMod.server.commandManager.executeCommand(AsistMod.server, "playsound block.stone_button.click_off master @p ~ ~ ~ 0.5 1 0.2");
					AsistMod.playSound(this.world,"block.stone_button.click_off",this.pos);
				}
				else if( (ticks%6 == 3)  && (this.current_flash_speed == flashSpeedEnum.FLASH_MEDIUM) ) {
					//AsistMod.server.commandManager.executeCommand(AsistMod.server, "playsound block.stone_button.click_off master @p ~ ~ ~ 0.5 1 0.2");
					AsistMod.playSound(this.world,"block.stone_button.click_off",this.pos);
				}
				else if( (ticks%4 == 2)  && (this.current_flash_speed == flashSpeedEnum.FLASH_FAST) ) {
					//AsistMod.server.commandManager.executeCommand(AsistMod.server, "playsound block.stone_button.click_off master @p ~ ~ ~ 0.5 1 0.2");
					AsistMod.playSound(this.world,"block.stone_button.click_off",this.pos);
				}
				
				flashRed();
				
			}
		}
	}
	
	
	
	public void outputSequenceToPlayer(String playerName)
	{
		char[] seq = getDefuseSequence();
		
		// +4 because that's how many additional fields aside from seq there are
		@SuppressWarnings("unchecked")		
		Map<String,String>[] tellrawArray= new HashMap[seq.length + 4];
		
		for(int i =0; i< tellrawArray.length; i++) {
			tellrawArray[i] = new HashMap<String,String>();
			if(i==0) {
				tellrawArray[i].put("text","ID : " + id);
			}
			else if (i==1) {

				tellrawArray[i].put("text","\nCHAINED TO : " + chainedId);
			}
			else if (i==2) {
				tellrawArray[i].put("text","\nFUSE_START_MINUTE : " + fuseStartTime);
			}
			else if (i==3) {
				tellrawArray[i].put("text","\nSEQUENCE : ");
			}			
			else {
				tellrawArray[i].put("text",Character.toString(seq[i-4]));				
				
				if(seq[i-4]=='R'){
					////System.out.println("Matching Red.");
					tellrawArray[i].put("color","red");
				}
				else if(seq[i-4]=='G') {
					////System.out.println("Matching Green.");
					tellrawArray[i].put("color","green");
				}
				else if(seq[i-4]=='B') {
					////System.out.println("Matching Blue.");
					tellrawArray[i].put("color","blue");				
				}
				
			}
		}
				
	
		String tellrawArrayString = AsistMod.gson.toJson(tellrawArray);
		
		String stringSeq = Arrays.toString(seq);
		
		String pid = PlayerManager.getPlayerByName(playerName).getParticipantId();
		
		String bombId = id;
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("BOMB_ID: ");
		sb.append(bombId);
		sb.append("\n");
		sb.append("CHAINED_ID: ");
		sb.append(chainedId);
		sb.append("\n");
		sb.append("FUSE_START_MINUTE: ");
		sb.append(fuseStartTime);
		sb.append("\n");
		sb.append("SEQUENCE: ");
		sb.append(stringSeq);
		
		String message = sb.toString();

		//System.out.println("PlayerInteractionEvent Right Click : " + message);

		//{"text":"Herobrine joined the game","color":"yellow"}
		///tellraw @p [{"text":"hi","color":"red"},{"text":"\ndude","color":"blue"}]		

		AsistMod.server.commandManager.executeCommand(AsistMod.server, "tellraw " + playerName +" "+ tellrawArrayString);
		
		Map<String,String> additionalInfo = new HashMap<String,String>();
		additionalInfo.put("bomb_id", bombId );
		additionalInfo.put("chained_id", chainedId);
		additionalInfo.put("remaining_sequence", stringSeq);
		additionalInfo.put("fuse_start_minute", Integer.toString(fuseStartTime));

		CommunicationEnvironmentModel comEnvModel = new CommunicationEnvironmentModel(
				bombId,
				this.getBlockType().getRegistryName().toString().split(":")[1],
				new String[]{pid},
				message,
				this.getPos().getX(),
				this.getPos().getY(),
				this.getPos().getZ(),
				additionalInfo);
		InternalMqttClient.publish(comEnvModel.toJsonString(), comEnvModel.getTopic());
		
	}
	
	public void flashRed() {		
				
		if(this.blockState == null ) {
			this.blockState = this.world.getBlockState(this.pos);
		}		
		
		if ( ( fuseRemaining <= fuseDuration * (3.0f/3.0f) && fuseRemaining > fuseDuration * (2.0f/3.0f)) ) {
			////System.out.println("Setting Slow Flash 0");
			if ( current_flash_speed != flashSpeedEnum.FLASH_SLOW ) {
				
				////System.out.println("Setting Slow Flash 1");
				
				this.world.setBlockState(this.pos, blockState.withProperty(BlockBombStandard.EXPLODE, 1));			
				
				current_flash_speed = flashSpeedEnum.FLASH_SLOW;	
			}
			
		}
		
		else if ( ( fuseRemaining <= fuseDuration * (2.0f/3.0f) && fuseRemaining > fuseDuration * (1.0f/3.0f) ) ) {
			////System.out.println("Setting Medium Flash 0");
			if( current_flash_speed != flashSpeedEnum.FLASH_MEDIUM ) {
				
				////System.out.println("Setting Medium Flash 1");
				
				this.world.setBlockState(this.pos, blockState.withProperty(BlockBombStandard.EXPLODE, 2));				
				
				current_flash_speed = flashSpeedEnum.FLASH_MEDIUM;	
				
			}		
					
		}
		
		else if ( ( fuseRemaining <= fuseDuration * (1.0f/3.0f) &&  fuseRemaining > 0f ) ) {
			////System.out.println("Setting Fast Flash 0");
			if( current_flash_speed != flashSpeedEnum.FLASH_FAST ) {
				
				////System.out.println("Setting Fast Flash 1");
				
				this.world.setBlockState(this.pos, blockState.withProperty(BlockBombStandard.EXPLODE, 3));			
				
				current_flash_speed = flashSpeedEnum.FLASH_FAST;
			}						
		}		
	}
	
	public void setFuseStartTime(String timeInMinutes)
	{
		if( timeInMinutes == null || timeInMinutes.isEmpty() ) {
			this.fuseStartTime = -1;
		}
		else {			
			this.fuseStartTime = Integer.parseInt(timeInMinutes);			
		}
				
	}	
	
	public void setFuseDuration(int durationInSeconds)
	{
		this.fuseDuration = durationInSeconds;
		fuseRemaining = this.fuseDuration;
		this.ticks = 0;		
	}

	public int getFuseDuration()
	{
		return this.fuseDuration;
	}

	public int getFuseRemaining()
	{
		return this.fuseRemaining;
	}
	
	public int getFuseStartTime()
	{
		return this.fuseStartTime;
	}



	public  void resetFuse()
	{		
		fuseRemaining = this.fuseDuration;
		this.beginCountDown();
		this.ticks = 0;
	}

	public void beginCountDown()
	{
		countingDown = true;

	}

	
	public void pauseCountDown()
	{
		countingDown = false;
	}

	public void playSound(double x, double y, double z, int state)
	{
		switch (state)
		{
			case 1:
				AsistMod.server.commandManager.executeCommand(AsistMod.server, "playsound entity.tnt.primed master @p ~ ~ ~ 1 0.2 1");
				break;
			case 2:
				AsistMod.server.commandManager.executeCommand(AsistMod.server, "playsound entity.tnt.primed master @p ~ ~ ~ 1 0.9 1");
				break;
			case 3:
				AsistMod.server.commandManager.executeCommand(AsistMod.server, "playsound entity.tnt.primed master @p ~ ~ ~ 1 2.0 1");
				break;

		}
	}

	@Override
	public boolean shouldRefresh(World world, BlockPos pos, IBlockState oldState, IBlockState newSate)
	{
		return (oldState.getBlock() != newSate.getBlock());
	}

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound compound) {

		//System.out.println("Writing NBT");

		return super.writeToNBT(compound);
	}

	public NBTTagCompound createNBTTagCompound() {

		NBTTagCompound compound = new NBTTagCompound();
		StringBuilder sb = new StringBuilder("");
		for(char c : defuseSequence) {
			sb.append(c);
		}
		compound.setString("defuseSequence", sb.toString());
		compound.setInteger("defuseIndex", this.defuseIndex );		
		compound.setString("id", this.id );
		compound.setInteger("fuseStartTime", this.fuseStartTime);
		compound.setInteger("fuseDuration", this.fuseDuration);
		compound.setInteger("fuseRemaining", this.fuseRemaining);
		compound.setBoolean("countingDown", this.countingDown);
		compound.setBoolean("begunCountingDown", this.begunCountingDown);
		compound.setBoolean("isPerturbationTrigger", this.isPerturbationTrigger);


		NBTTagList tagList = new NBTTagList();
		for(int i = 0; i < contributingParticipants.size(); i++)
		{
			String s = contributingParticipants.get(i);
			if(s != null)
			{
				NBTTagCompound tag = new NBTTagCompound();
				tag.setString("" + i, s);
				tagList.appendTag(tag);
			}
		}
		compound.setTag("contributingParticipants", tagList);


		return compound;
	}

	@Override
	public void readFromNBT(NBTTagCompound compound) {
		
		super.readFromNBT(compound);
		
		String defuseSequenceString = compound.getString("defuseSequence");

		char[] array = new char[defuseSequenceString.length()];
		for(int i = 0 ; i<defuseSequenceString.length(); i++) {
			array[i]=defuseSequenceString.charAt(i);
		}
		this.defuseSequence = array;

		this.defuseIndex = compound.getInteger("defuseIndex");	
		this.id = compound.getString("id");
		this.fuseStartTime = compound.getInteger("fuseStartTime");
		this.fuseDuration = compound.getInteger("fuseDuration");
		this.fuseRemaining = compound.getInteger("fuseRemaining");
		this.countingDown = compound.getBoolean("countingDown");
		this.begunCountingDown = compound.getBoolean("begunCountingDown");
		this.isPerturbationTrigger = compound.getBoolean("isPerturbationTrigger");

		NBTTagList tagList = compound.getTagList("contributingParticipants", Constants.NBT.TAG_COMPOUND);
		for(int i = 0; i < tagList.tagCount(); i++)
		{
			NBTTagCompound tag = tagList.getCompoundTagAt(i);
			String s = tag.getString("" + i);
			contributingParticipants.add(i, s);

		}

	}

	public boolean compareBlockPos( BlockPos pos) {
		return ( pos.getX() == this.pos.getX() ) && ( pos.getY() == this.pos.getY() ) && ( pos.getZ() == this.pos.getZ() );
	}

	public void setDefuseSequence(char[] sequence) {
		
		this.defuseSequence = sequence;

		this.writeToNBT( this.createNBTTagCompound() );
	}

	public void setDefuseSequence(String sequence) {
		char[] array = new char[sequence.length()];
		for(int i = 0 ; i<sequence.length(); i++) {
			array[i]=sequence.charAt(i);
		}
		this.defuseSequence = array;

		this.writeToNBT( this.createNBTTagCompound() );
	}

	public char[] getDefuseSequence() {
		char[] newSeq = Arrays.copyOfRange(defuseSequence,defuseIndex,defuseSequence.length);
		return newSeq;
	}

	public String getChainedId()
	{
		return this.chainedId;
	}

	public char[] advanceSequence() {

		char[] newSeq = {};

		if(this.defuseIndex < this.defuseSequence.length-1) {

			this.defuseIndex++;
			newSeq = Arrays.copyOfRange(this.defuseSequence,this.defuseIndex,this.defuseSequence.length);
			
			// Sets or resets the fuse, because this is not the first defusal and not the last			
			this.resetFuse();

		}
		else if (this.defuseIndex == this.defuseSequence.length-1) {

			// IF defuseIndex is greater than the defuseSequence length we can assume bomb is defused
			this.defuseIndex++;

			// GIVE REWARD HERE

			//System.out.println("Bomb Defused : Awarding Points");

			IBlockState blockState = ForgeRegistries.BLOCKS.getValue(new ResourceLocation("minecraft", "air")).getDefaultState();

			world.setBlockState(pos, blockState);

		}

		this.writeToNBT( this.createNBTTagCompound() );
		return newSeq;
	}

	public String setBombId(String id) {
		this.id = AttributedObjectType.BOMB.getPrefix() + id;
		
		return this.id;
	}

	public void setChainedId(String id)
	{
		if( id.toLowerCase().contentEquals("none")) {
			this.chainedId = id;
		}
		else{
			this.chainedId = AttributedObjectType.BOMB.getPrefix() + id;
		}
	}

 
	public String getBombId() {
		
		
		return this.id;
	}
	
	public int getDefuseIndex() {
		
		
		return this.defuseIndex;
	}
	
	public boolean isCountingDown() {
		
		
		return this.countingDown;
	}

	@Override
	public void onMissionTimeChange(int m, int s) {
		
		try {			

			//on mission time change if we haven't started already, begin counting down.
			if( !this.countingDown ) {
				
				if( m == this.fuseStartTime && s == 0)
				{
					beginCountDown();
					StringBuilder sb = new StringBuilder();
					for(char c : this.getDefuseSequence()) {
						sb.append(c);
					}
					Map<String,String> newAttr = new HashMap<String,String>();
					newAttr.put("sequence", sb.toString() );
					newAttr.put("sequence_index", Integer.toString(this.getDefuseIndex()) );
					newAttr.put("active", "true");
					newAttr.put("outcome", BlockBombBase.BombOutcomeEnum.TRIGGERED.name());
					newAttr.put("fuse_start_time", String.valueOf(this.getFuseStartTime()));
					
					Map<String, String[]> changedAttributes = AttributedObjectManager.updateObjectAttributes(this.getBombId(), newAttr);
					
					// Build Message
					ObjectStateChangeModel objectStateChangeModel = new ObjectStateChangeModel(
							this.getBombId(),
							this.getBlockType().getRegistryName().toString().split(":")[1],
							"SERVER",
							this.pos.getX(),
							this.pos.getY(),
							this.pos.getZ(),
							changedAttributes,
							AttributedObjectManager.getObjectAttributes(this.getBombId())
					);
					
					InternalMqttClient.publish(objectStateChangeModel.toJsonString(), objectStateChangeModel.getTopic());
				}			
			}
			else {
				
				if(this.fuseRemaining <= 0 ) {					
					
					Block block = this.blockState.getBlock();
					String type = block.getRegistryName().toString().split(":")[1];
					//remove any beacon thats on top
					BlockBombBase.removeAssociatedBeacon(world, this.getPos(), this.blockType,"SERVER");
					ExplosionManager.createExplosion(this.id, type, world, this.getPos().getX(), this.getPos().getY(), this.getPos().getZ());			
					
					this.pauseCountDown();
					MissionTimer.unsubscribeToTimerTicks(this);
					
					// START UPDATE ATTRIBUTES AND SEND OBJECT_STATE_CHANGE MESSAGE
					StringBuilder sb = new StringBuilder();
					for(char c : getDefuseSequence()) {
						sb.append(c);
					}
					Map<String,String> newAttr = new HashMap<String,String>();
					newAttr.put("sequence", sb.toString() );
					newAttr.put("sequence_index", Integer.toString(getDefuseIndex()) );
					newAttr.put("active", "false");
					newAttr.put("outcome", BlockBombBase.BombOutcomeEnum.EXPLODE_TIME_LIMIT.name());
					newAttr.put("fuse_duration", String.valueOf(getFuseDuration()));
					
					Map<String, String[]> changedAttributes = AttributedObjectManager.updateObjectAttributes(this.id, newAttr);
					
					// Build Message
					ObjectStateChangeModel objectStateChangeModel = new ObjectStateChangeModel(
							this.id,
							type,
							"SERVER",
							pos.getX(),
							pos.getY(),
							pos.getZ(),
							changedAttributes,
							AttributedObjectManager.getObjectAttributes(getBombId())
					);
					
					InternalMqttClient.publish(objectStateChangeModel.toJsonString(), objectStateChangeModel.getTopic());
					// END UPDATE ATTRIBUTES AND SEND OBJECT_STATE_CHANGE MESSAGE
					
					// ENVIRONMENT REMOVED SINGLE
					EnvironmentRemovedSingleModel envRemovedModel = new EnvironmentRemovedSingleModel("SERVER",objectStateChangeModel.data);
					InternalMqttClient.publish(envRemovedModel.toJsonString(), envRemovedModel.getTopic());
					
					//Bookkeeping
					BombBookkeeper.addBombToExploded(this.id);
					RegionManager.removeBombFromRegion(this.id, pos);
					
					// BOMB REMOVED BROADCASTER
					BombRemovedBroadcaster.bombRemoved(id);					
					if( this.isPerturbationTrigger ) {
						
						FireManager.fireSourceBombsArray.remove(id);
						FireManager.placeFire(world, pos, id, false);
						
					}
					else {
						
						if( block instanceof BlockBombFire) {
							FireManager.placeFire(world, pos, id, false);
						}
						
					}					
				}
				else {
					//System.out.println("Mission Timer : " + m + " : " + s);
					this.fuseRemaining--;
					//System.out.println("Active Bomb tick from Bomb # " + this.id + " : " + this.fuseRemaining);
				}
							
			}
			
		}
		catch(Exception e) {
			
			e.printStackTrace();
			
		}

	}
	
}
