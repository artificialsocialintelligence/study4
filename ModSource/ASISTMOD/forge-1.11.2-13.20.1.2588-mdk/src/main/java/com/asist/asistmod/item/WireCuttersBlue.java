package com.asist.asistmod.item;

import net.minecraft.item.ItemPickaxe;

public class WireCuttersBlue extends ItemPickaxe {
		
	public WireCuttersBlue(String name, ToolMaterial material) {
		super(material);
	    this.setUnlocalizedName(name);
	    this.setMaxDamage(2);
	    
	}
}
