package com.asist.asistmod.item;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.MissionCommon.Utilities.AttributeManagement.AttributedObjectManager;
import com.asist.asistmod.MissionSpecific.MissionA.BombManagement.BombBookkeeper;
import com.asist.asistmod.MissionSpecific.MissionA.ItemManagement.ItemManager;
import com.asist.asistmod.MissionSpecific.MissionA.ItemManagement.ItemType;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.PlayerManager;
import com.asist.asistmod.datamodels.ItemStateChange.ItemStateChangeModel;
import com.asist.asistmod.datamodels.ItemUsed.ItemUsedModel;
import com.asist.asistmod.datamodels.ToolUsed.ToolUsedModel;
import com.asist.asistmod.mqtt.InternalMqttClient;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BombSensor extends ItemPickaxe
{
    protected String name;

    public BombSensor(String name, ToolMaterial material)
    {
    	super(material);
	    this.setUnlocalizedName(name);
	    this.setMaxDamage(2);
        this.setMaxStackSize(64);
    }


    public static double getDistance(BlockPos block1, BlockPos block2)
    {
        return Math.sqrt(
                Math.pow(block2.getX() - block1.getX(), 2) +                
                Math.pow(block2.getZ() - block1.getZ(), 2)
        );
    }
    
    /**
     * Called when the equipped item is right clicked.
     */
    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn)
    {
    	
    	ItemStack itemStack = playerIn.getHeldItemMainhand();
		if(!worldIn.isRemote) {    		
        	
        	//Get nearest bomb position
        	
        	BlockPos playerPos = playerIn.getPosition();
        	
        	String id = "NOT_SET";
        	double closestDist = 200.0d;
        	BlockPos closestPos = null;
        	
        	Set<String> keys = BombBookkeeper.getbombsRemainingMap().keySet();
        	Iterator<String> iterator = keys.iterator();
        	
        	while(iterator.hasNext()) {
        		String idToCheck = iterator.next();
        		BlockPos posToCheck = BombBookkeeper.getbombsRemainingMap().get(idToCheck);
        		double dist = getDistance(playerPos,posToCheck);
        		if( dist<closestDist) {
        			id = idToCheck;
        			closestDist = dist;
        			closestPos = posToCheck;
        		}
        	}
        	
        	//System.out.println(" BOMB_SENSOR:  NEAREST BOMB : "+ id + " : " + closestPos.toString() );
        	
        	AsistMod.server.commandManager.executeCommand(AsistMod.server,
                     "particle angryVillager " + closestPos.getX() + " " + (closestPos.getY()+1) + " " + closestPos.getZ() + " 0.1 50 0.1 .1 1000 force");
        	
        	String name = playerIn.getName();
        	String pid = PlayerManager.getPlayerByName(name).getParticipantId();
        	Block block = worldIn.getBlockState(closestPos).getBlock();
        	publishItemUsedEvents(name,pid,closestPos,block,itemStack);
        	
        	itemStack.shrink(1);
    		playerIn.renderBrokenItemStack(itemStack);
    	}
    	
    	
        return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, itemStack );
    }
    
 public void publishItemUsedEvents(String playerName, String pid, BlockPos pos, Block block, ItemStack itemStack) {
    	
    	String registryName = this.getRegistryName().toString();
    	ItemType heldModItem = ItemManager.getItemTypeFromRegistryName(registryName);
    	String itemName = heldModItem.getName();
		String itemStackId = itemStack.getTagCompound().getTag("item_stack_id").toString();
		////System.out.println(itemStackId);
		itemStackId = itemStackId.replaceAll("\"", "");
		////System.out.println(itemStackId);
		String itemId = "ITEM"+Integer.toString(itemStack.getCount());
		String idForMessage = itemStackId+'_'+itemId;
		//System.out.println("ITEM ID FOR MESSAGE : "+idForMessage);
    	
		// TOOL USED MESSAGE
    	ToolUsedModel toolUsedModel = new ToolUsedModel();
		toolUsedModel.msg.experiment_id = InternalMqttClient.currentTrialInfo.experiment_id;
		toolUsedModel.msg.trial_id = InternalMqttClient.currentTrialInfo.trial_id;
		//toolUsedModel.data.playername = playerName;
		toolUsedModel.data.participant_id = pid;
		toolUsedModel.data.tool_type = itemName;
		toolUsedModel.data.durability = 0;
		toolUsedModel.data.target_block_x = pos.getX();
		toolUsedModel.data.target_block_y = pos.getY();
		toolUsedModel.data.target_block_z = pos.getZ();
		toolUsedModel.data.target_block_type = block.getRegistryName().toString().split(":")[1];
		InternalMqttClient.publish(toolUsedModel.toJsonString(), "observations/events/player/tool_used",playerName);
		
		// ITEM USED MESSAGE
		ItemUsedModel itemUsedModel = new ItemUsedModel();
		itemUsedModel.data.participant_id = pid;						
		itemUsedModel.data.item_id = idForMessage;
		itemUsedModel.data.item_name = itemName;
		itemUsedModel.data.input_mode = "RIGHT_MOUSE";
		itemUsedModel.data.target_x = pos.getX();
		itemUsedModel.data.target_y = pos.getY();
		itemUsedModel.data.target_z = pos.getZ();		
		InternalMqttClient.publish(itemUsedModel.toJsonString(), itemUsedModel.getTopic(), playerName);
		
		
		// ITEM STATE CHANGE MESSAGE
		Map<String,String> currAttr = AttributedObjectManager.getItemAttributes(itemStackId,itemId);
		int usesRemaining = Integer.parseInt(currAttr.get("uses_remaining"));
		usesRemaining--;
		Map<String,String> newAttr = new HashMap<String,String>();
		newAttr.put("uses_remaining", Integer.toString(usesRemaining) );
		Map<String, String[]> changedAttributes = AttributedObjectManager.updateItemAttributes(itemStackId, itemId, newAttr);
		// for all single use items
		ItemStateChangeModel itemStateChangeModel = new ItemStateChangeModel(idForMessage,itemName,pid,changedAttributes,currAttr);
		InternalMqttClient.publish(itemStateChangeModel.toJsonString(), itemStateChangeModel.getTopic(), playerName);
		
    }

}