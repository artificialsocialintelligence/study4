package com.asist.asistmod.MissionSpecific.MissionA.UtilityStructures;

import net.minecraft.util.math.BlockPos;

public class Position {

	private String commandLineOut;
	
	private int posX;
	private int posY;
	private int posZ;
	
	public BlockPos blockPos;
	
	public Position(int x, int y, int z) {
		this.posX = x;
		this.posY = y;
		this.posZ = z;
		
		blockPos = new BlockPos(x, y, z);
		commandLineOut = this.posX + " " + this.posY + " " + this.posZ;
	}
	
	public Position(BlockPos pos) {
		this.posX = pos.getX();
		this.posY = pos.getY();
		this.posZ = pos.getZ();	
		
		blockPos = pos;
		commandLineOut = this.posX + " " + this.posY + " " + this.posZ;
	}
	
		
	public String getString() {
		return commandLineOut;
	}
	
	public int getX() {
		return this.posX;
	}
	
	public int getY() {
		return this.posY;
	}
	
	public int getZ() {
		return this.posZ;
	}
}
