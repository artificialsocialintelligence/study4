package com.asist.asistmod.eventHandlers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.MissionSpecific.MissionA.BombManagement.BombBookkeeper;
import com.asist.asistmod.MissionSpecific.MissionA.MissionManagement.CallSigns;
import com.asist.asistmod.MissionSpecific.MissionA.MissionManagement.MissionManager;
import com.asist.asistmod.MissionSpecific.MissionA.MissionManagement.MissionManager.MissionStage;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.Player;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.PlayerDisconnectEventHandler;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.PlayerManager;
import com.asist.asistmod.MissionSpecific.MissionA.ScoreboardManagement.ScoreboardManager;
import com.asist.asistmod.MissionSpecific.MissionA.ShopManagement.ServerSideShopManager;
import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimer;
import com.asist.asistmod.mqtt.InternalMqttClient;
import com.asist.asistmod.network.NetworkHandler;
import com.asist.asistmod.network.messages.BombCountUpdatePacket;
import com.asist.asistmod.network.messages.GameOverPacket;
import com.asist.asistmod.network.messages.MissionStageTransitionPacket;
import com.asist.asistmod.network.messages.TeamScoreUpdatePacket;
import com.asist.asistmod.network.messages.shop.AssignCallSignPacket;
import com.asist.asistmod.network.messages.shop.UpdateVoteCountPacket;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;
import org.lwjgl.Sys;

public class PlayerJoinEventHandler {
	
	MinecraftServer server;
	String worldName;
	
	public PlayerJoinEventHandler( MinecraftServer server) {
		this.server = server;
		this.worldName = server.worlds[0].getWorldInfo().getWorldName();
	}


	
	
	@SubscribeEvent
	public void onPlayerJoin(PlayerLoggedInEvent event) {
			
		EntityPlayer player = event.player;
		String name = player.getName();
		
		// AUTHORIZE PLAYERS
		if( AsistMod.modSettings.authorizePlayers) {
			
			if( InternalMqttClient.authorizedPlayers.contains(name)) {
				
				playerSetup(player, name);
			}
			else {	
				
				//System.out.println("Unauthorized player " + name + " is being kicked from the server.");			
				server.commandManager.executeCommand(server, "kick " + name);
			}			
		}		
		// END AUTHORIZE PLAYERS
		else {
					
			server.commandManager.executeCommand(server, "op " + name);
			
			//playerSetup(player, name);
			
		}
		
	}
	
	public void playerSetup(EntityPlayer player, String name){
		
		// Players need to be OPPED to use Starting Switch, is removed after teleportation		
		
		server.commandManager.executeCommand(server, "scoreboard teams join asist " + name);
		
		boolean isObserver = AsistMod.isObserver(name);		
		
		if(!isObserver) {
			
			boolean playerExists = false;
			
			Iterator<Player> iterator = PlayerManager.players.iterator();
			Player asistPlayer = null;
			
			while ( iterator.hasNext()) {
				Player p = iterator.next();
				if ( p.getName().contentEquals(name) ) {
					playerExists = true;
					asistPlayer = p;
				}
			};				
			if(playerExists) {				
				asistPlayer.player = player;
				final Player playerForThread = asistPlayer;
				Thread thread = new Thread() {			
					public void run() {										
							try {
								// wait one second for client connection to stabilize
								Thread.sleep(1000);	
								NetworkHandler.sendToClient(new AssignCallSignPacket(playerForThread.callSignCode,CallSigns.masterCallSignArray), playerForThread.getEntityPlayer());
								NetworkHandler.sendToClient(new UpdateVoteCountPacket(ServerSideShopManager.votesToGoToShop), playerForThread.getEntityPlayer());
								NetworkHandler.sendToClient(new TeamScoreUpdatePacket(ScoreboardManager.teamScore), playerForThread.getEntityPlayer());								
								NetworkHandler.sendToClient(new GameOverPacket( "" ), playerForThread.getEntityPlayer() );
								NetworkHandler.sendToClient(new BombCountUpdatePacket(BombBookkeeper.bombsDefused,BombBookkeeper.bombsExploded,BombBookkeeper.bombsTotal-(BombBookkeeper.bombsDefused+BombBookkeeper.bombsExploded)), playerForThread.getEntityPlayer());
							} 
							catch (InterruptedException e) {
								e.printStackTrace();
							}							
						}
					};			
				thread.start();
				
			}
			else {
				asistPlayer = PlayerManager.createPlayerManagerRecord(player, name);			
				
			}
			
			MissionStage stage = MissionManager.stage;
			if(stage == null) {
				MissionManager.teleportCallsignToShop(asistPlayer.callSignCode, false);				
			}
			else if(stage.compareTo(MissionStage.RECON_STAGE)==0) {
				MissionManager.teleportCallsignToRecon(asistPlayer.callSignCode);				
			}
			else if(stage.compareTo(MissionStage.SHOP_STAGE)==0) {
				MissionManager.teleportCallsignToShop(asistPlayer.callSignCode, false);
			}
			else if(stage.compareTo(MissionStage.FIELD_STAGE)==0) {
				MissionManager.teleportCallsignToField(asistPlayer.callSignCode);
			}
			
			NetworkHandler.sendToClient(new MissionStageTransitionPacket(stage), player);
			
			if(MissionManager.missionStarted) {			
				
			}
			else {				
				MissionManager.shouldMissionStart();				
			}
			
			
		}		
	}	
}
