package com.asist.asistmod.datamodels.CommunicationEnvironment;

import java.util.Map;

import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimer;

public class CommunicationEnvironmentData {
	public String mission_timer = MissionTimer.getMissionTimeString();
	public long elapsed_milliseconds = MissionTimer.getElapsedMillisecondsGlobal();
	public String message_id;
	public String sender_id;
	public String sender_type;
	public String[] recipients;
	public String message;
	public int sender_x;
	public int sender_y;
	public int sender_z;
	public Map<String, String> additional_info;
}
