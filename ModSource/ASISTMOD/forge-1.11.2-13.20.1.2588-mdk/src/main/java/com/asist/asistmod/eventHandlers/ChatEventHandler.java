package com.asist.asistmod.eventHandlers;

import java.util.List;

import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.Player;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.PlayerManager;
import com.asist.asistmod.datamodels.Chat.ChatModel;
import com.asist.asistmod.mqtt.InternalMqttClient;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.event.ServerChatEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class ChatEventHandler {
	
	public ChatEventHandler() {}
	
	@SubscribeEvent	
	public void onChat(ServerChatEvent event){
		
		Player player = PlayerManager.getPlayerByName(event.getPlayer().getName());
		
		if( player != null) {
			
			//System.out.println("Cancelling a player chat.");
			event.setCanceled(true);
		}
		else {
			
			// Get player list
			List<EntityPlayerMP> playersArray = event.getPlayer().getServer().getPlayerList().getPlayers();
			// create mutable sb object
			StringBuilder nameString = new StringBuilder("");
			// append names with comma delimiter
			playersArray.forEach( (p)->{
				
				nameString.append(p.getName() + ',');
				
			});
			
			ChatModel chatMessage = new ChatModel();		
			
			//header
			chatMessage.header.message_type = "chat";
			
			//msg
			chatMessage.msg.experiment_id = InternalMqttClient.currentTrialInfo.experiment_id;
			chatMessage.msg.trial_id = InternalMqttClient.currentTrialInfo.trial_id;
			
			
			chatMessage.data.sender = event.getUsername();
			
			//msg.data
			// change this to pid's now
			chatMessage.data.addressees = nameString.toString().split(",");
			chatMessage.data.text = event.getMessage();
			
			InternalMqttClient.publish(chatMessage.toJsonString(), "minecraft/chat", event.getUsername() );
		}
	}
}
