package com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement;

import java.time.Clock;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.MissionSpecific.MissionA.MissionManagement.MissionManager;
import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimer;
import com.asist.asistmod.mqtt.InternalMqttClient;

import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;

public class PlayerDisconnectEventHandler{

    //Map of players and their disconnect time
    static long defaultTimeout = 180000;
    static long sleepInterval = 10000;
    
    //static Map<EntityPlayer, Float> currentDisconnectedPlayers = null;
    static Thread thread;
    static boolean isInitialized = false;
    static int windDownRemaining = 10;
    static boolean inWindDown = false;
    public static String disconnectedPlayerName="NOT_SET";
    
    public PlayerDisconnectEventHandler()
    {        
        isInitialized = true;
        
        thread = new Thread() {			
			public void run() {				
				while (isInitialized) {					
					try { 
						
						Thread.sleep( PlayerDisconnectEventHandler.sleepInterval); 
					} 
					catch (InterruptedException e) {
						e.printStackTrace();
					}
					if(PlayerDisconnectEventHandler.inWindDown) {
						windDownRemaining --;
						AsistMod.server.commandManager.executeCommand(AsistMod.server, "tellraw @a { \"text\": \"Removing Players in "+ windDownRemaining + "\", \"color\": \"yellow\"}");
						if( windDownRemaining == 0) {
							PlayerManager.players.forEach( (p) ->{
								AsistMod.server.commandManager.executeCommand(AsistMod.server, "kick " + p.playerName);
							});
							isInitialized = false;
							MissionTimer.isInitialized = false;
							AsistMod.executing = false;							
						}
					}
					else {
						// CHECK DISCONNECT
						PlayerDisconnectEventHandler.checkDisconnect();
					}
				}				
			}			
		};
		thread.start();
    }

    @SubscribeEvent
    public void onPlayerDisconnect(PlayerEvent.PlayerLoggedOutEvent event) {
        if(!event.player.world.isRemote) {
        	Player player = PlayerManager.getPlayerByName( event.player.getName() );
        	if(player != null) {
        		player.disconnectedTimestampMilli = Clock.systemDefaultZone().millis();
        		//System.out.println(player.disconnectedTimestampMilli);
        	}
        }
    }

    @SubscribeEvent
    public void onPlayerJoin(PlayerEvent.PlayerLoggedInEvent event) {

    	 if(!event.player.world.isRemote) {
         	Player player = PlayerManager.getPlayerByName( event.player.getName() );
         	if(player != null) {
         		player.disconnectedTimestampMilli = 0;
         	}
    	 }
    }

    public static void checkDisconnect() {
    	for (int i = 0; i< PlayerManager.players.size(); i++ ) {
    		Player player = PlayerManager.players.get(i);
    		if( player.disconnectedTimestampMilli != 0 ) { 
    			long currentTime = Clock.systemDefaultZone().millis();
    			System.out.println( player.callSign + " disconnected for " + (currentTime - player.disconnectedTimestampMilli) + " milliseconds."  );
    			if ( currentTime - player.disconnectedTimestampMilli >= PlayerDisconnectEventHandler.defaultTimeout) {
    				PlayerDisconnectEventHandler.triggerDisconnect( player.callSign, player.playerName);
    			}
    		}
    		
    	}
    }

    public static void triggerDisconnect( String callSign, String name )
    {       
        String message = callSign + " exceeded maximum disconnected time of "+ (defaultTimeout/1000L) +" seconds. This Trial will now end.";
        disconnectedPlayerName = name;
        AsistMod.server.commandManager.executeCommand(AsistMod.server, "tellraw @a { \"text\": \""+ message + "\", \"color\": \"yellow\"}");
        MissionManager.endMission(MissionManager.StateChangeOutcome.MISSION_STOP_MINECRAFT_DISCONNECT,false);        
        InternalMqttClient.publish("{\"name\":\""+PlayerDisconnectEventHandler.disconnectedPlayerName+"\"}", "status/minecraft/player_timeout");
        // WE HANDLE THE WINDDOWN HERE INSTEAD OF MISSION TIMER BECASUE OF A THREAD SYNCHRONIZATION ISSUE WHERE MISSION TIMER THREAD JUST WAITS FOR THIS THREAD
        // TO COMPLETE AND I DON'T KNOW HOW TO MAKE IT STOP ... WORKS WELL ENOUGH LIKE THIS FOR NOW
        PlayerDisconnectEventHandler.sleepInterval = 1000;
        PlayerDisconnectEventHandler.inWindDown = true;
    }    
}
