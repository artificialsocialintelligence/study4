package com.asist.asistmod.tile_entity;

import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimer;
import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimerListener;

import net.minecraft.block.state.IBlockState;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BeaconTileEntity extends TileEntity implements ITickable, MissionTimerListener {
	
	String id = "NOT SET";

	//private IBlockState blockState = null;
	
	//private int ticks = 0;
	
	//ConcurrentHashMap<String,String> attributes = new ConcurrentHashMap<String,String>();

	public BeaconTileEntity() {
		
		MissionTimer.subscribeToTimerTicks(this);
	}


	// Called once per tick ( 20 tps )
	@Override
	public void update(){
		
		if(!world.isRemote) {
			
		}
	}
	
	
	
	@Override
	public boolean shouldRefresh(World world, BlockPos pos, IBlockState oldState, IBlockState newSate)
	{
		return (oldState.getBlock() != newSate.getBlock());
	}

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound compound) {

		//System.out.println("Writing NBT");

		return super.writeToNBT(compound);
	}

	public NBTTagCompound createNBTTagCompound() {

		NBTTagCompound compound = new NBTTagCompound();
		//StringBuilder sb = new StringBuilder("");
		//for(char c : defuseSequence) {
		//	sb.append(c);
		//}
		//compound.setString("defuseSequence", sb.toString());
		//compound.setInteger("defuseIndex", this.defuseIndex );		
		compound.setString("id", this.id );
		//compound.setInteger("fuseDuration", this.fuseDuration);
		//compound.setInteger("fuse", this.fuseRemaining);
		//compound.setBoolean("countingDown", this.countingDown);

		return compound;
	}

	@Override
	public void readFromNBT(NBTTagCompound compound) {

		//System.out.println("Reading NBT");
		super.readFromNBT(compound);

		//String defuseSequenceString = compound.getString("defuseSequence");

		//char[] array = new char[defuseSequenceString.length()];
		//for(int i = 0 ; i<defuseSequenceString.length(); i++) {
			//array[i]=defuseSequenceString.charAt(i);
		//}
		//this.defuseSequence = array;

		//this.defuseIndex = compound.getInteger("defuseIndex");	
		this.id = compound.getString("id");
		//this.fuseDuration = compound.getInteger("fuseDuration");
		//this.fuseRemaining = compound.getInteger("fuseRemaining");
		//this.countingDown = compound.getBoolean("countingDown");

	}

	public boolean compareBlockPos( BlockPos pos) {
		return ( pos.getX() == this.pos.getX() ) && ( pos.getY() == this.pos.getY() ) && ( pos.getZ() == this.pos.getZ() );
	}

	public String setBeaconId(String s) {
		this.id = s;
		return this.id;
	}

 
	public String getBeaconId() {
		return this.id;
	}
	
	

	@Override
	public void onMissionTimeChange(int m, int s) {
		

	}
	
	
	


}
