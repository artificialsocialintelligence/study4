package com.asist.asistmod.eventHandlers;

import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.PlayerManager;

import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class LivingDeathEventHandler {
	
	@SubscribeEvent
	public void onLivingDeathEvent ( LivingDeathEvent event) {
		event.setCanceled(true);
		
		String name = event.getEntityLiving().getName();
		//System.out.println("Canceling Death Event#1");
		
		PlayerManager.players.forEach((p)->{
			if (p.playerName.contentEquals(name)) {
				
				// set player frozen
				
				//System.out.println("Canceling Death Event#2");
				// cancel event
				
				
			};
		});
		
	}

}
