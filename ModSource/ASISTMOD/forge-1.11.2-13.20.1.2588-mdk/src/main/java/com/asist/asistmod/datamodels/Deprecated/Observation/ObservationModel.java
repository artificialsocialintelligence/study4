package com.asist.asistmod.datamodels.Deprecated.Observation;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.datamodels.Header.HeaderModel;

public class ObservationModel{	 
	
	public HeaderModel header = new HeaderModel();
	
	public ObservationMessageModel msg = new ObservationMessageModel();
	
	public ObservationData data = new ObservationData();
	
	public ObservationModel() {
		header.message_type = "observation";
	}
	
	public String getTopic() { 		
		
		return "observations/state"; 
		
	}
	
	public String toJsonString() { 		
		
		return AsistMod.gson.toJson(this); 
		
	}

}
