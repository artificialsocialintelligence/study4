package com.asist.asistmod.network.messages.shop;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.MissionCommon.Utilities.AttributeManagement.AttributedObjectManager;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.Player;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.PlayerManager;
import com.asist.asistmod.datamodels.CommunicationEnvironment.CommunicationEnvironmentModel;
import com.asist.asistmod.datamodels.ItemStateChange.ItemStateChangeModel;
import com.asist.asistmod.mqtt.InternalMqttClient;
import com.asist.asistmod.network.MessagePacket;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;

public class BeaconChatPacket extends MessagePacket<BeaconChatPacket> {
	
	int callSignLength = 0;
	String callSign="";
	int textLength = 0;
	String text="";
	String beaconId;
	int beaconIdLength;
	
	public BeaconChatPacket() {
		
	}
	
	public BeaconChatPacket(String callSign, String text, String beaconId) {
		this.callSign = callSign;
		this.callSignLength = callSign.length();
		this.text = text;
		this.textLength = text.length();
		this.beaconId = beaconId;
		this.beaconIdLength = beaconId.length();
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		// TODO Auto-generated method stub
		
		callSignLength=buf.readInt();
		StringBuilder sb = new StringBuilder();
		for(int i=0;i<callSignLength;i++) {
			sb.append(buf.readChar());
		}
		callSign = sb.toString();
		textLength = buf.readInt();
		sb = new StringBuilder();
		for(int i=0;i<textLength;i++) {
			sb.append(buf.readChar());
		}
		text = sb.toString();
		sb.setLength(0);
		this.beaconIdLength = buf.readInt();
		for(int i=0;i<beaconIdLength;i++) {
			sb.append(buf.readChar());
		}
		this.beaconId = sb.toString();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		// TODO Auto-generated method stub
		
		buf.writeInt(callSignLength);
		for(int i=0;i<callSignLength;i++) {
			buf.writeChar(callSign.toCharArray()[i]);
		}
		buf.writeInt(textLength);
		for(int i=0;i<textLength;i++) {
			buf.writeChar(text.toCharArray()[i]);
		}
		buf.writeInt(beaconIdLength);
		for(int i=0;i<beaconIdLength;i++) {
			buf.writeChar(beaconId.charAt(i));
		}
		
	}

	@Override
	public void handleClientSide(BeaconChatPacket message, EntityPlayer player) {
		// TODO Auto-generated method stub
		
		// GuiShopTemplate.processIncomingChatMessage( message.callSign,message.text);
		
	}

	@Override
	public void handleServerSide(BeaconChatPacket message, EntityPlayer player) {
		
		//System.out.println("SERVERSIDE BEACON CHAT PACKET");
		
		String callSign = message.callSign.toUpperCase();
		
		AsistMod.server.commandManager.executeCommand(AsistMod.server, "tellraw @a { \"text\": \"" + callSign +" : "+message.text +"\", \"color\": \"yellow\"}");
						
		String beaconId = message.beaconId;    	
    	
    	Player p= PlayerManager.getPlayerByName(player.getName());
		
		Iterator<Player> iterator = PlayerManager.players.iterator();
		String[] pids = new String[PlayerManager.players.size()];
		int i = 0;
		
		while(iterator.hasNext()) {
			pids[i] = iterator.next().participant_id;
			i++;
		}		
		
		Map<String,String> additionalInfo = new HashMap<String,String>();
		additionalInfo.put("text", message.text);
		
		// communicates to all players
		CommunicationEnvironmentModel comEnvModel = new CommunicationEnvironmentModel(
				beaconId,
				p.lastBeaconType,
				pids,
				message.text,
				p.lastBeaconPosition.getX(),
				p.lastBeaconPosition.getY(),
				p.lastBeaconPosition.getZ(),
				additionalInfo);
		InternalMqttClient.publish(comEnvModel.toJsonString(), comEnvModel.getTopic());
		
		
		// item/state/change
		String participant_id = p.participant_id;
		String itemName = p.lastBeaconType;
		String itemStackId = beaconId.split("_")[0];    	
    	String itemId = beaconId.split("_")[1]; 
		
		Map<String,String> currAttr = AttributedObjectManager.getItemAttributes(itemStackId,itemId);
		int usesRemaining = Integer.parseInt(currAttr.get("uses_remaining"));
		////System.out.println("PlayerInteractionEvent:uses_remaining_pre " +usesRemaining );
		usesRemaining--;
		////System.out.println("PlayerInteractionEvent:uses_remaining_pre " +usesRemaining );
		Map<String,String> newAttr = new HashMap<String,String>();
		newAttr.put("uses_remaining", Integer.toString(usesRemaining) );
		newAttr.put("message", message.text );
		Map<String, String[]> changedAttributes = AttributedObjectManager.updateItemAttributes(itemStackId, itemId, newAttr);
		// for all single use items
		ItemStateChangeModel itemStateChangeModel = new ItemStateChangeModel(beaconId,itemName,participant_id,changedAttributes,currAttr);
		InternalMqttClient.publish(itemStateChangeModel.toJsonString(), itemStateChangeModel.getTopic(), p.playerName);
		
		
	}

}
