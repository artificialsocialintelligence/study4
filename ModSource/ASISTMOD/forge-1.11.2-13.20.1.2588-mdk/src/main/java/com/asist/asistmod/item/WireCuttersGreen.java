package com.asist.asistmod.item;

import net.minecraft.item.ItemPickaxe;

public class WireCuttersGreen extends ItemPickaxe {
		
	public WireCuttersGreen(String name, ToolMaterial material) {
		super(material);
	    this.setUnlocalizedName(name);
	    this.setMaxDamage(2);
	    
	}
}
