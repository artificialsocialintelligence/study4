package com.asist.asistmod.datamodels.ModSettings;

import java.util.Map;

public class ModSettings_v3 {
	
	public boolean clientSideMapBuilder;
	
	public String clientSideMapBuilderMapBlockFile;
	
	public int max_num_transitions_to_field = 3;
	
	public MinSec missionLength = new MinSec(3,0);
	
	public MinSec time_in_recon = new MinSec(2,0);
	
	public MinSec time_in_field = new MinSec(1,0);
	
	public MinSec time_in_shop = new MinSec(1,0);
	
	public MinSec fire_perturbation_time_threshold = new MinSec(3,0);
	
	public int team_budget = 0;
	
	public int team_budget_add_on_unlock = 0;
	
	public Map<String,Integer> tool_cost = null;
	
	public boolean useRoomDefinitionFile;
	
	public String roomDefinitionFile;
	
	public String mqtthost = null;
	
	public int observationInterval;
	
	public boolean removePlayersOnMissionEnd;
	
	public boolean authorizePlayers = true;
	
	public String[] observerNames = {};
	
	public PerturbationDef[] blackout_perturbation = null;
	
	public PerturbationDef[] rubble_perturbation = null;
	
	public Map<String,Integer> map_boundaries = null;
	public Map<String,Integer>[] sector_boundaries = null;
	public Map<String,Integer>[] sector_gates = null;
	
	public int bomb_timer_seconds = 15;
	
	public int fire_spread_interval_seconds = 10;
	
	public int markers_on_start = 5;
	
}