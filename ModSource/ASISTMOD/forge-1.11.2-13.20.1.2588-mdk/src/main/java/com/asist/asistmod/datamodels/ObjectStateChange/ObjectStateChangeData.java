package com.asist.asistmod.datamodels.ObjectStateChange;

import java.util.Map;

import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimer;

public class ObjectStateChangeData {	

	
	public String mission_timer;
	public long elapsed_milliseconds;	
	public String id = "NOT_SET";
	public String type = "NOT_SET";
	public String triggering_entity;
	public int x = 0;
	public int y = 0;
	public int z = 0;
	public Map< String, String[]> changedAttributes = null; 
	public Map<String, String> currAttributes = null;	
	
	// SOMETIMES YOU WANT THE TIMESTAMP AND ELAPSED MILLISECONDS FROM THIS (env created single) AND SOMETIMES YOU DONT (env created list)
	// TRUE WILL PUT IT IN, FALSE WILL NOT
	public ObjectStateChangeData(Boolean timestamp) {
		
		if( timestamp) {
			mission_timer = MissionTimer.getMissionTimeString();
			elapsed_milliseconds = MissionTimer.getElapsedMillisecondsGlobal();	
		}
		
		
	}
	
	
}
