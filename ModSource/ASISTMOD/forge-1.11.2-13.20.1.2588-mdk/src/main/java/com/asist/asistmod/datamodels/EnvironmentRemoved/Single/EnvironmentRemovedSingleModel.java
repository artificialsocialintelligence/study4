package com.asist.asistmod.datamodels.EnvironmentRemoved.Single;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.datamodels.Header.HeaderModel;
import com.asist.asistmod.datamodels.Msg.MsgModel;
import com.asist.asistmod.datamodels.ObjectStateChange.ObjectStateChangeData;
import com.asist.asistmod.mqtt.MessageTopic;

public class EnvironmentRemovedSingleModel {
	
	public HeaderModel header = new HeaderModel();
		
	public MsgModel msg = new MsgModel(getMessageTopicEnum().getEventName(),getMessageTopicEnum().getVersion());
	
	public EnvironmentRemovedSingleData data = new EnvironmentRemovedSingleData();		
		
	public EnvironmentRemovedSingleModel( String triggeringEntity, ObjectStateChangeData obj) {
		
		data.triggering_entity = triggeringEntity;
		data.obj = obj;
				
	}	
	
	public MessageTopic getMessageTopicEnum() { 		
		
		return MessageTopic.ENVIRONMENT_REMOVED_SINGLE; 
		
	}
	
	public String getTopic() {
		return getMessageTopicEnum().getTopic();
	}
	
	public String toJsonString() { 
		
		return AsistMod.gson.toJson(this);		
	}

}
