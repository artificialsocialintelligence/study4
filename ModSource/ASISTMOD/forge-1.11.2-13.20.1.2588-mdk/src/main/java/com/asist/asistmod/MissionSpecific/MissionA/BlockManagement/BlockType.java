package com.asist.asistmod.MissionSpecific.MissionA.BlockManagement;

import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

public enum BlockType {
	
	BOMB_STANDARD	("bomb_standard", "bomb","asistmod:block_bomb_standard", 
						ForgeRegistries.BLOCKS.getValue(new ResourceLocation("asistmod", "block_bomb_standard")).getDefaultState()),	

	NULL			("air", "air","minecraft:air", Blocks.AIR.getDefaultState());
		
	private final String name;
	private final String typeName;
	private final IBlockState blockState;
	private final String registryName;
	
	BlockType(String name, String typeName, String registryName, IBlockState blockState) {
		this.name = name;
		this.typeName = typeName;
		this.blockState = blockState;
		this.registryName = registryName;
	}
		
	public String getName() {
		return this.name;
	}
	
	public String getTypeName() {
		return this.typeName;
	}
	
	public String getRegistryName() {
		return this.registryName;
	}
		
	public IBlockState getDefaultState() {
		return this.blockState;
	}
}
