package com.asist.asistmod.datamodels.Deprecated.ProximityBlockInteractionMessage;

import com.asist.asistmod.datamodels.Header.HeaderModel;
import com.google.gson.Gson;

public class ProximityBlockInteractionModel {
	
	public HeaderModel header = new HeaderModel();
		
	public ProximityBlockInteractionMessageModel msg = new ProximityBlockInteractionMessageModel();	
	
	public ProximityBlockInteractionDataModel data = new ProximityBlockInteractionDataModel();
		
	public ProximityBlockInteractionModel() {		
		
		header.message_type = "event";		
	}
	
	
	public String toJsonString() { 
		
		Gson gson = new Gson();
		return gson.toJson(this);		
	}

}
