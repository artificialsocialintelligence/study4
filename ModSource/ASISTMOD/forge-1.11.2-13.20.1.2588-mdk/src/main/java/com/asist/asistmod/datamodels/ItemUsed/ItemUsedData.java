package com.asist.asistmod.datamodels.ItemUsed;

import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimer;

public class ItemUsedData {
	
	public String mission_timer = MissionTimer.getMissionTimeString();
	public long elapsed_milliseconds = MissionTimer.getElapsedMillisecondsGlobal();	
	public String participant_id = "Not Set";
	public String item_id = null;
	public String item_name = null;
	public String input_mode = null;
	public int target_x = 0;
	public int target_y = 0;
	public int target_z = 0;	
}
