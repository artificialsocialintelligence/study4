package com.asist.asistmod.eventHandlers;


import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.MissionCommon.Utilities.AttributeManagement.AttributedObjectManager;
import com.asist.asistmod.MissionSpecific.MissionA.MissionManagement.MissionManager;
import com.asist.asistmod.MissionSpecific.MissionA.MissionManagement.MissionManager.StateChangeOutcome;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.Player;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.PlayerManager;
import com.asist.asistmod.MissionSpecific.MissionA.ScoreboardManagement.ScoreboardManager;
import com.asist.asistmod.datamodels.PlayerStateChange.PlayerStateChangeModel;
import com.asist.asistmod.mqtt.InternalMqttClient;

import net.minecraft.util.DamageSource;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class LivingHurtEventHandler {

	public LivingHurtEventHandler() {
		// TODO Auto-generated constructor stub
	}
	
	@SubscribeEvent	
	public void onHurt(LivingHurtEvent event) {
		
		
		if(!event.getEntityLiving().world.isRemote) {
						
			//System.out.println("----------------LivingHurtEvent----------------");
			
			////System.out.println( " DAMAGE ENTITY SOURCE :" + event.getSource().getEntity().getName() );	
			Player player = PlayerManager.getPlayerByName(event.getEntity().getName());
			boolean frozen = player.isFrozen;
			
			// DAMAGE OCCURS AFTER THIS EVENT IS COMPLETE -SO WHEN WE CHECK REMAINING HEALTH _ ITS THE HEALTH PRIOR TO THE APPLIED DAMAGE
			// This is why we don't pass that number directly to the mqtt message
			float remainingHealth = player.getEntityPlayer().getHealth();
			String damageType = event.getSource().damageType;			
			//System.out.println(damageType);			
			
			if( damageType.contentEquals("fall") || 
				damageType.contentEquals("onFire") || 
				frozen || 
				(remainingHealth <= 1.0f) 
			) {
				//System.out.println("Cancelling falling || Frozen || onFire || <=1f health");				
				
				event.setCanceled(true);
				// this is here to catch if we are already at 1.0f health and not yet frozen - which happens with fire
				if(remainingHealth <= 1.0f && !frozen) {
					publishHealthStateChange(player, remainingHealth);
					freezeLogic( event, player, remainingHealth);
				}				
				
			}
			else if( damageType.contentEquals("inFire") && !frozen ) {
				
				player.lastDamagedById = "FIRE";
				player.lastDamagedByType = "fire";
				player.lastDamagedByPosition = event.getEntity().getPosition();
				if( remainingHealth - 1f > 1f) {
					publishHealthStateChange(player, remainingHealth-1f);
				}				
				else {					
					publishHealthStateChange(player, 1f);
					freezeLogic( event, player, 1f);
				}
        		ScoreboardManager.decreasePlayerScore(player.participant_id, 1);
        		
				
			}
			else if( damageType.contentEquals("generic") && !frozen ) {
				
				if( remainingHealth - 10f > 1f) {
					publishHealthStateChange(player, remainingHealth-10f);
					ScoreboardManager.decreasePlayerScore(player.participant_id, 10);
				}
				
				else{					
					publishHealthStateChange(player, 1f);
					ScoreboardManager.decreasePlayerScore(player.participant_id, 10);
					freezeLogic( event, player, 1f);
				}
				
			}
			
			//System.out.println("----------------LivingHurtEvent----------------");
		}
		
	}
	
	public void freezeLogic(LivingHurtEvent event, Player player, float remainingHealth) {
		
		if( remainingHealth <= 1.0f && !player.isFrozen) {
			//System.out.println("Freezing Player");
			player.setPlayerFrozen(true, player.getParticipantId(), event.getEntity().getPosition(), 
					player.lastDamagedById, player.lastDamagedByType, player.lastDamagedByPosition);
			AsistMod.server.commandManager.executeCommand(AsistMod.server, "effect "+ player.getName() +" slowness 99999 10");
			AsistMod.server.commandManager.executeCommand(AsistMod.server, "/tell "+player.getName()+" You are now frozen until a "
					+ "teammate comes and rescues you!");			
		}		
		checkAllPlayersFrozen();
	}
	
	public void checkAllPlayersFrozen() {
		CopyOnWriteArrayList<Player> players = PlayerManager.players;
		int numFrozenPlayers = 0;
		for(int i=0; i<players.size(); i++) {
			if(players.get(i).isFrozen) {
				numFrozenPlayers++;
			}
		}
		if(numFrozenPlayers == players.size()) {
			
			MissionManager.endMission(StateChangeOutcome.MISSION_STOP_ALL_PLAYERS_FROZEN,false);			
			
			// SEND THIS TO EVERYONE
			//String message = "YOUR WHOLE TEAM HAS BEEN FROZEN! THE GAME IS OVER! YOU WILL SOON BE KICKED FROM THE SERVER! PLEASE TRY AGAIN!";
		}
	}
	
	public void publishHealthStateChange(Player player, float remainingHealth) {
		
		Map<String,String> oldAttributes = new ConcurrentHashMap<String,String>(player.attributes);
		
		player.attributes.put("health", Float.toString(remainingHealth));
		
		Map<String,String[]>changedAttributes = AttributedObjectManager.returnAttributeDiff(oldAttributes, player.attributes);
		
		PlayerStateChangeModel model = new PlayerStateChangeModel(
				player.getParticipantId(),
				player.getEntityPlayer().getPosition(),
				player.lastDamagedById,
				player.lastDamagedByType,
				player.lastDamagedByPosition,
				changedAttributes,
				player.attributes);
		
		InternalMqttClient.publish(model.toJsonString(), model.getTopic());
		
		
	}

}
