package com.asist.asistmod.MissionCommon.InterventionManagement.ChatInterventions;

import java.util.List;

import com.asist.asistmod.datamodels.ModSettings.MinSec;

public class TimeAndChat {
	
	MinSec timeTrigger;
	String content;
	List<?> receivers;
	
	
	public TimeAndChat(MinSec t, String c, List<?> r) {
		
		this.timeTrigger = t;
		this.content = c;
		this.receivers = r;
		
	}

}
