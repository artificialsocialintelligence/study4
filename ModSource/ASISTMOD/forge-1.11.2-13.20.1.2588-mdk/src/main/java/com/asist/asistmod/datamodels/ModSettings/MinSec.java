package com.asist.asistmod.datamodels.ModSettings;

public class MinSec {
	
	public int minute;
	public int second;
	
	public MinSec(int m, int s) {
		minute = m;
		second = s;
	}

}
