package com.asist.asistmod.item;

import java.util.ArrayList;
import java.util.List;

import com.asist.asistmod.AsistMod;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.init.Blocks;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemPotion;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.fml.common.registry.GameRegistry;


public class _Items {
	
	public static final List<Item> items = new ArrayList<Item>();
	
	//change to high durability
	public static final int wireCuttersDurability = 5000;
	
	public static final ToolMaterial wireCuttersMaterial = 
			EnumHelper.addToolMaterial("custom_material", 0, wireCuttersDurability, 0.0f, 0, 0);

	public static ItemArmor.ArmorMaterial bombppeMaterial = EnumHelper.addArmorMaterial("bombppe",
			AsistMod.MODID + ":items/bombppe", 33, new int[] {0, 0, 0, 0},
			9, SoundEvents.ITEM_ARMOR_EQUIP_DIAMOND, 2.0f);


	public static Item BUCKET;

	public static Item WATER_BUCKET;

	public static Item WIRECUTTERS_RED;

	public static Item PPE_HEAD;
	public static Item PPE_CHEST;
	public static Item PPE_LEGS;
	public static Item PPE_FEET;

	public static Item FIRE_EXTINGUISHER;

	public static ItemPotion WATER_BALL;


	public static final void commonPreInit() {
		
		//ITEMS
		WIRECUTTERS_RED = registerItem(new WireCuttersRed("item_wirecutters_red", wireCuttersMaterial), "item_wirecutters_red");
		registerItem(new WireCuttersGreen("item_wirecutters_green", wireCuttersMaterial), "item_wirecutters_green");
		registerItem(new WireCuttersBlue("item_wirecutters_blue", wireCuttersMaterial), "item_wirecutters_blue");

		Item item = (new ItemModBucket(Blocks.AIR)).setUnlocalizedName("item_bucket").setMaxStackSize(16);
		BUCKET = registerItem(item, "item_bucket");
		WATER_BUCKET = registerItem((new ItemModBucket(Blocks.FLOWING_WATER)).setUnlocalizedName("item_bucket_water").setContainerItem(item), "item_bucket_water");

		//ARMOR
		PPE_HEAD = registerItem(new ItemBombPPE(bombppeMaterial, 1, EntityEquipmentSlot.HEAD, "item_bombppe_helmet"),
				"item_bombppe_helmet");
		PPE_CHEST = registerItem(new ItemBombPPE(bombppeMaterial, 1, EntityEquipmentSlot.CHEST, "item_bombppe_chestplate"),
				"item_bombppe_chestplate");
		PPE_LEGS = registerItem(new ItemBombPPE(bombppeMaterial, 2, EntityEquipmentSlot.LEGS, "item_bombppe_leggings"),
				"item_bombppe_leggings");
		PPE_FEET = registerItem(new ItemBombPPE(bombppeMaterial, 2, EntityEquipmentSlot.FEET, "item_bombppe_boots"),
				"item_bombppe_boots");

		//OTHER ITEMS
		registerItem(new BombSensor("item_bomb_sensor",wireCuttersMaterial), "item_bomb_sensor");
		registerItem(new BombDisposer("item_bomb_disposer", wireCuttersMaterial), "item_bomb_disposer");
		FIRE_EXTINGUISHER = registerItem(new FireExtinguisher("item_fire_extinguisher"), "item_fire_extinguisher");
		registerItem(new ItemFlintSteel(), "item_flint_steel");

	}
	
	public static final void clientPostInit() {
		for(Item item : items) {
			registerRender(item);
		}
	}

	private static final void registerRender(Item item) {
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, new ModelResourceLocation(AsistMod.MODID + ":" + item.getRegistryName().getResourcePath(), "inventory"));
	}

	public static final Item registerItem(Item item, String name) {
		item.setUnlocalizedName(name);
		GameRegistry.register(item, new ResourceLocation(AsistMod.MODID, name));
		items.add(item);
		return item;
	}
}
