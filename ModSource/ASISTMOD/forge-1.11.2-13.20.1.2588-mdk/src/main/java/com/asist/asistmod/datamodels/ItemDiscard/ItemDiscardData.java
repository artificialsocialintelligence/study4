package com.asist.asistmod.datamodels.ItemDiscard;

import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimer;

public class ItemDiscardData {
	
	public String mission_timer = MissionTimer.getMissionTimeString();
	public long elapsed_milliseconds = MissionTimer.getElapsedMillisecondsGlobal();	
	public String participant_id = "NOT_SET";
	public String item_name = null;
	public int amount_discarded;

}
