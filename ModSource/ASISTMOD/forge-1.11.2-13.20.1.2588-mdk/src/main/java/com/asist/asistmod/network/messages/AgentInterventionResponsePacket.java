package com.asist.asistmod.network.messages;

import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.PlayerManager;
import com.asist.asistmod.datamodels.AgentInterventionResponse.AgentInterventionResponseModel;
import com.asist.asistmod.mqtt.InternalMqttClient;
import com.asist.asistmod.network.MessagePacket;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;

public class AgentInterventionResponsePacket extends MessagePacket<AgentInterventionResponsePacket> {
	
	
	public String interventionId;
	public int interventionIdLength;
	public String agentId;
	public int agentIdLength;
	public int responseIndex;


	
	public AgentInterventionResponsePacket() {}
	
	public AgentInterventionResponsePacket( AgentInterventionChatPacket agentInterventionChatPacket, int respIndex) {
		
		interventionId = agentInterventionChatPacket.interventionId;
		interventionIdLength = interventionId.length();
		agentId = agentInterventionChatPacket.agentId;
		agentIdLength = agentId.length();
		responseIndex = respIndex;
	}

	// NEEDS TO BE IN SAME ORDER AS toBytes
	@Override
	public void fromBytes(ByteBuf buf) {
		interventionIdLength = buf.readInt();
		StringBuilder sb = new StringBuilder();
		for(int i=0; i<interventionIdLength; i++) {
			sb.append(buf.readChar());
		}
		interventionId = sb.toString();
		sb.setLength(0);
		agentIdLength = buf.readInt();		
		for(int i=0; i<agentIdLength; i++) {
			sb.append(buf.readChar());
		}
		agentId = sb.toString();
		sb.setLength(0);
		
		responseIndex = buf.readInt();
	}

	// NEEDS TO BE IN SAME ORDER AS fromBytes
	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(interventionIdLength);		
		for(int i=0;i<interventionIdLength;i++) {
			buf.writeChar(interventionId.charAt(i));
		}
		buf.writeInt(agentIdLength);		
		for(int i=0;i<agentIdLength;i++) {
			buf.writeChar(agentId.charAt(i));
		}
		buf.writeInt(responseIndex);	
	}

	@Override
	public void handleClientSide(AgentInterventionResponsePacket message, EntityPlayer player) {
		
	
		
	}

	@Override
	public void handleServerSide(AgentInterventionResponsePacket message, EntityPlayer player) {
		
		// HANDLED HERE
		String pid = PlayerManager.getPlayerByName(player.getName()).getParticipantId();
		
		// SEND MQTT MESSAGE
		AgentInterventionResponseModel messageModel = new AgentInterventionResponseModel(pid,message.interventionId, message.agentId,message.responseIndex);
		InternalMqttClient.publish(messageModel.toJsonString(), messageModel.getTopic(message.agentId));			
	}

}
