package com.asist.asistmod.mqtt;

public enum MessageTopic {
	
	AGENT_INTERVENTION_RESPONSE			("agent/intervention/+/response","0.0.0","Event:InterventionResponse"),
	PLAYER_INVENTORY_UPDATE				("player/inventory/update","0.0.0","Event:InventoryUpdate"),
	PLAYER_STATE 						("player/state","0.0.0","Event:PlayerState"),
	PLAYER_STATE_CHANGE 				("player/state/change","0.0.0","Event:PlayerStateChange"),
	ENVIRONMENT_CREATED_SINGLE			("environment/created/single","0.0.0","Event:EnvironmentCreatedSingle"),
	ENVIRONMENT_CREATED_LIST			("environment/created/list","0.0.0","Event:EnvironmentCreatedList"),
	ENVIRONMENT_REMOVED_SINGLE			("environment/removed/single","0.0.0","Event:EnvironmentRemovedSingle"),
	ENVIRONMENT_REMOVED_LIST			("environment/removed/list","0.0.0","Event:EnvironmentRemovedList"),
	OBJECT_STATE_CHANGE					("object/state/change","0.0.0","Event:ObjectStateChange"),
	ITEM_USED							("item/used","0.0.0","Event:ItemUsed"),
	ITEM_STATE_CHANGE					("item/state/change","0.0.0","Event:ItemStateChange"),
	ITEM_DISCARD						("item/discard","0.0.0","Event:ItemDiscard"),
	ITEM_ACQUIRED						("item/acquired","0.0.0","Event:ItemAcquired"),
	COMMUNICATION_CHAT					("communication/chat","1.0.0","Event:CommunicationChat"),
	COMMUNICATION_ENVIRONMENT			("communication/environment","1.0.0","Event:CommunicationEnvironment"),
	COMMUNICATION_AGENT					("communication/agent/$agent_name/chat","0.0.0","Event:CommunicationAgentChat"),
	MISSION_STATE						("observations/events/mission","1.0.0","Event:MissionState"),
	MISSION_LEVEL_PROGRESS				("mission/level/progress","0.0.0","Event:MissionLevelProgress"),
	MISSION_STAGE_TRANSITION			("observations/events/stage_transition","1.0.0","Event:MissionStageTransition"),
	MISSION_PLAYER_JOIN					("mission/player/join","0.0.0","Event:MissionPlayerJoin"),
	MISSION_PLAYER_LEFT					("mission/player/left","0.0.0","Event:MissionPlayerLeft"),
	MISSION_PERTURBATION_START			("mission/perturbation/start","0.0.0","Event:MissionPerturbationStart"),
	TEAM_BUDGET_UPDATE					("team/budget/update","0.0.0","Event:TeamBudgetUpdate"),
	UI_OPEN								("ui/open","0.0.0","Event:UIOpen"),
	UI_CLOSE							("ui/close","0.0.0","Event:UIClose"),
	UI_CLICK							("ui/click","0.0.0","Event:UIClick"),
	SCORE_CHANGE						("score/change","0.0.0","Event:ScoreChange"),
	ACHEIVEMENT_EARNED					("achievement/earned","0.0.0","Event:AcheivementEarned"),
	SCHEMA_SCENARIO						("schema/scenario","0.0.0","Schema:ScenarioDef"),
	
	// NOT PUBLISHED TO BUS (PII OR OTHER UNNECESSARY INFO
	CONTROL_REQUEST_GETTRIALINFO		("control/request/getTrialInfo","0.0.0","Control:GetTrialInfo"),
	STATUS_MINECRAFT_LOADING			("status/minecraft/loading","0.0.0","Status:MinecraftLoading"),
	STATUS_SERVER_STOPPED				("status/server/stopped","0.0.0","Status:ServerStopped")
	;	
	
	private final String topic;
	private final String version;
	private final String eventName;
	
	
	MessageTopic(String topic, String version, String eventName) {		
		this.topic = topic;		
		this.version = version;
		this.eventName = eventName;
	}
	
	public String getTopic() {		
		return this.topic;
	}
	
	public String getTopicWithAgent(String agentId) {	
		if(this.eventName.contentEquals("Event:InterventionResponse")) {
			return "agent/intervention/"+agentId+"/response";
		}
		else {
			//System.out.println("You've incorrectly used an agent topic function with an event that does not use an agent in it's topic string");
		}
		return this.topic;
	}
	
	public String getVersion() {
		return this.version;
	}
	
	public String getEventName() {
		return this.eventName;
	}
	
	
}
