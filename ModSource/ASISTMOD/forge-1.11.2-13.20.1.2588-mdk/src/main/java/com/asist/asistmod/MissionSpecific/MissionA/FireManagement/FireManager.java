package com.asist.asistmod.MissionSpecific.MissionA.FireManagement;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.MissionCommon.Utilities.AttributeManagement.AttributedObjectManager;
import com.asist.asistmod.MissionSpecific.MissionA.BombManagement.BombBookkeeper;
import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimer;
import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimerListener;
import com.asist.asistmod.block.beacons.BlockBeaconBasic;
import com.asist.asistmod.block.bombs.BlockBombBase;
import com.asist.asistmod.datamodels.EnvironmentCreated.Single.EnvironmentCreatedSingleModel;
import com.asist.asistmod.datamodels.EnvironmentRemoved.Single.EnvironmentRemovedSingleModel;
import com.asist.asistmod.datamodels.ModSettings.MinSec;
import com.asist.asistmod.datamodels.ObjectStateChange.ObjectStateChangeData;
import com.asist.asistmod.datamodels.Perturbation.PerturbationModel;
import com.asist.asistmod.mqtt.InternalMqttClient;
import com.asist.asistmod.tile_entity.BeaconTileEntity;
import com.asist.asistmod.tile_entity.BombBlockTileEntity;
import com.asist.asistmod.tile_entity.FireCustomTileEntity;

import net.minecraft.block.Block;
import net.minecraft.block.BlockAir;
import net.minecraft.block.BlockFire;
import net.minecraft.block.BlockLiquid;
import net.minecraft.block.BlockSlab;
import net.minecraft.block.BlockStairs;
import net.minecraft.block.state.IBlockState;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

public class FireManager implements MissionTimerListener {

	public static ConcurrentHashMap<BlockPos, String> fieldFire = new ConcurrentHashMap<BlockPos, String>();
	
	public static ConcurrentLinkedQueue<QueuedFire> fireQueue = new ConcurrentLinkedQueue<QueuedFire>();
	
	public static FirePlacingThread firePlacingThread;
	
	public static int fireId = 0;
	
	public static List<String> fireSourceBombsArray = new ArrayList<String>();
	
	private static MinSec firPerturbationTimeThreshold;
	
	public static boolean perturbationHappened = false;

	
	private static final List<String> fireCanDestroy = new ArrayList<>(
		Arrays.asList(
			"minecraft:wooden_door",
			"minecraft:grass",
			"minecraft:tallgrass",
			"minecraft:leaves",
			"minecraft:vine",
			"minecraft:log",
			"minecraft:air",
			"minecraft:nether_brick_fence",
			"minecraft:dark_oak_fence",
			"minecraft:jungle_fence",
			"minecraft:double_plant",
			"asistmod:block_bomb_fire",
			"asistmod:block_bomb_chained",
			"asistmod:block_bomb_standard",
			"asistmod:block_beacon_bomb",
			"asistmod:block_beacon_hazard"
				
		)
	);
	
	

	//private static FireManager instance;
	
	public FireManager() {
		MissionTimer.subscribeToTimerTicks(this);	
		firePlacingThread = new FirePlacingThread();
	}
	
	public static void queueFire(World world, BlockPos pos, String parentFireId, boolean fixFireStateAfterBeacon) {
		
		if( fieldFire.size()<=500 ) {
			Block block = world.getBlockState(pos).getBlock();
			Block blockBelow = world.getBlockState(pos.down()).getBlock();
			
			if (  !(blockBelow instanceof BlockAir) 
					&& !(blockBelow instanceof BlockLiquid) 
					&& !(blockBelow instanceof BlockSlab)
					&& !(blockBelow instanceof BlockStairs)
					&& !(block instanceof BlockFire)
				) {
			
				if( fireCanDestroy.contains(block.getRegistryName().toString()) ) {
					
					FireManager.fireQueue.add( new QueuedFire(world, pos, parentFireId, fixFireStateAfterBeacon) );
				}				
			}
		}
		
	}
	 /** Place fire into the world.  Any fire that is created through the spreading of this fire
	 * block will also be added to fieldFire but this is done through the fire tile entity itself, not this class
	 * */
	public static void placeFire(World world, BlockPos pos, String parentFireId, boolean fixFireStateAfterBeacon) {

		Block block = world.getBlockState(pos).getBlock();
					
		if(fixFireStateAfterBeacon) {
			removeBeaconWithFire(world,pos, block, parentFireId);
		}		 
		else if( block instanceof BlockBombBase) {			
			BombBlockTileEntity bte = (BombBlockTileEntity) world.getTileEntity(pos);
			//do we remove this tile entity?
			((BlockBombBase)block).destroyBomb(world, pos, "SERVER", bte.getBombId(), BlockBombBase.BombOutcomeEnum.EXPLODE_FIRE);
		}
		else if( block instanceof BlockBeaconBasic) {			
			////System.out.println("Fire spreading and breaking beacon");
			removeBeaconWithFire(world,pos, block, parentFireId);
		}		
		
		IBlockState fireBlockState = ForgeRegistries.BLOCKS.getValue(new ResourceLocation("asistmod:block_fire_custom")).getDefaultState();
		
		try {
			world.setBlockState(pos, fireBlockState,3);			
			FireCustomTileEntity te = (FireCustomTileEntity)world.getTileEntity(pos);
			if(te != null) { 
				fieldFire.put(pos, te.id);
				if(!world.isRemote) {
    				
    				// Data Models - we are not calling fire and attributed object because it only has one state
        			ObjectStateChangeData objStateData = new ObjectStateChangeData(false);
        			objStateData.id = te.id;
        			objStateData.type = "block_fire_custom";    			
        			objStateData.x = pos.getX();
        			objStateData.y = pos.getY();
        			objStateData.z = pos.getZ();
        			EnvironmentCreatedSingleModel message = new EnvironmentCreatedSingleModel(parentFireId, objStateData);
        			message.publish();    				
    			}
			}
		}	            		
		catch(Exception e) {
			//System.out.println("Problem placing fire : \n" );
			e.printStackTrace();
		}
    	
	}
	
	/** Removes fire from the world and the fieldFire hashmap. Checks to see if there is fire at the given location before removing */
	public static void removeFire(World world, int x, int y, int z) {
		BlockPos pos = new BlockPos(x,y,z);		
		world.setBlockToAir(pos);
		world.removeTileEntity(pos);
		fieldFire.remove(pos);
		
	}
	
	/** Removes fire from the world and the fieldFire hashmap. Checks to see if there is fire at the given location before removing */
	public static void removeFire(World world, BlockPos pos) {
		//Block blockBelow = world.getBlockState(pos.down()).getBlock();
		world.setBlockToAir(pos);
		world.removeTileEntity(pos);
		//world.setBlockState(pos, blockBelow.getDefaultState().withProperty(BlockFireCustom.AGE, 15));
		fieldFire.remove(pos);		
	}
	
	public static void printFireIds(){
		String[] values = new String[fieldFire.values().size()];
		fieldFire.values().toArray(values);
		//System.out.println(Arrays.toString(values));
	}
	
	public static void selectRandomFireSources(int numSources) {
		
		System.out.println("-------------------SETTING PERTURBATION BOMB-----------------------");
		
		World world = AsistMod.server.getEntityWorld();
		firPerturbationTimeThreshold = AsistMod.modSettings.fire_perturbation_time_threshold;
		String[] bombIds = new String[BombBookkeeper.bombsRemainingMap.keySet().size()];
		BombBookkeeper.bombsRemainingMap.keySet().toArray(bombIds);
		int size = bombIds.length;
		Random rand = new Random();
		
		for( int i=0; i<numSources; i++) {
			
			int randInt = rand.nextInt(size);
			//System.out.println("randInt : " + randInt);
			String bombId = bombIds[randInt];			
			BlockPos pos = BombBookkeeper.bombsRemainingMap.get(bombId);
			BombBlockTileEntity te = (BombBlockTileEntity)world.getTileEntity(pos);
			te.setAsPerturbationTrigger();
			fireSourceBombsArray.add(bombId);
		}		
		
		System.out.println(Arrays.toString(fireSourceBombsArray.toArray()));
		System.out.println("-------------------END SETTING PERTURBATION BOMB-----------------------");
		
	}
	
	public static void removeBeaconWithFire(World world, BlockPos pos, Block block, String pid) {
		
		IBlockState blockState = world.getBlockState(pos);
		
		////System.out.println("Fire spreading and breaking beacon.");
		////System.out.println("Removing a beacon @ " + pos);		
		if( blockState.getBlock() instanceof BlockBeaconBasic ) {
			
			String beaconType = world.getBlockState(pos).getBlock().getRegistryName().toString().split(":")[1];
			BeaconTileEntity te = (BeaconTileEntity)world.getTileEntity(pos);
			String id = te.getBeaconId();			
			
			IBlockState air = ForgeRegistries.BLOCKS.getValue(new ResourceLocation("minecraft", "air")).getDefaultState();		
			world.setBlockState(pos, air,3);
			world.removeTileEntity(pos);
			
			// Split out ids for querying
			String stackid = id.split("_")[0];
			String itemid = id.split("_")[1];
			
			// EnvironmentRemovedSingle
			ObjectStateChangeData data = new ObjectStateChangeData(false);
        	data.id = id;
    		data.type = beaconType;
    		data.x = pos.getX();
    		data.y = pos.getY();
    		data.z = pos.getZ();
    		data.currAttributes = AttributedObjectManager.getItemAttributes(stackid, itemid);
			EnvironmentRemovedSingleModel envRemovedModel = new EnvironmentRemovedSingleModel(pid,data);
			InternalMqttClient.publish(envRemovedModel.toJsonString(), envRemovedModel.getTopic());
		}
		else {
			////System.out.println("No associated beacon!");
		}
		
	}

	// this method is not static so we can take advantage of the MissionTimerListener Interface 
	// we instantiate a generic (nameless) form of this class in the MissionTimerListener subscriber list in MissionTime class
	
	@Override
	public void onMissionTimeChange(int min, int sec) {
		
		
		// TODO Auto-generated method stub
		int m = FireManager.firPerturbationTimeThreshold.minute;
		int s = FireManager.firPerturbationTimeThreshold.second;
		
		
		if (m == min && s == sec) {
			MinecraftServer server = AsistMod.server;
			World world = server.getEntityWorld();
			
			for(int i=0; i< FireManager.fireSourceBombsArray.size(); i++) {
				String id = FireManager.fireSourceBombsArray.get(i);
				
				if( id != null ) {
					
					BlockPos pos = BombBookkeeper.bombsRemainingMap.get(id);
					
					if ( pos != null ) {
						
						try {
							Block block = world.getBlockState(pos).getBlock();
							
							if (block instanceof BlockBombBase) {
								
								BlockBombBase bomb = (BlockBombBase)block;
								
								bomb.destroyBomb(world, pos, "SERVER", id, BlockBombBase.BombOutcomeEnum.PERTURBATION_FIRE_TRIGGER);
								FireManager.fireSourceBombsArray.remove(id);
								placeFire(world,pos,"SERVER",false);
								AsistMod.server.commandManager.executeCommand(AsistMod.server, "tellraw @a { \"text\": \"" + " A fire has started somewhere in the vicinity of "+ id + "\", \"color\": \"yellow\"}");
								PerturbationModel perturbationModel = new PerturbationModel("FIRE",pos.getX(),pos.getY(),pos.getZ(),id);
								perturbationModel.publish();
								
							}							
							
						}catch(Exception e) {
							e.printStackTrace();
						}
						
					}					
				}				
			}
			// Maybe never unsubscribe because you may want to light more fires using timing later
			//MissionTimer.unsubscribeToTimerTicks(this);
		}		
	}	
}
