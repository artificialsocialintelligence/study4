package com.asist.asistmod.MissionSpecific.MissionA.BombManagement.Explosions;

import com.asist.asistmod.AsistMod;

import net.minecraft.block.state.IBlockState;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

public class ExplosionManager {
	
	public static Explosion createExplosion( String bombId, String bombType, World world , int x, int y, int z) {		
		float explosionSizeModifier = 6F;
		// custom explosion
		CustomExplosion explosion = new CustomExplosion(bombId, bombType, world, null, x, y, z, explosionSizeModifier, true, true);
		BlockPos pos = new BlockPos(x,y,z);
        if (net.minecraftforge.event.ForgeEventFactory.onExplosionStart(world, explosion)) return explosion;
        // BLOCK AND PLAYER DAMAGE
        explosion.doExplosionA();        
        // KNOCKBACK, DROPS, PARTICLE EFFECTS
        explosion.doExplosionB(true);
        IBlockState blockState = ForgeRegistries.BLOCKS.getValue(new ResourceLocation("minecraft", "air")).getDefaultState();		
		world.setBlockState(pos, blockState);
		AsistMod.server.commandManager.executeCommand(AsistMod.server, "particle explode "+x+" "+y+" "+z+" 1 1 1 1 200");
		world.removeTileEntity(pos);
		return explosion;
	}
} 
