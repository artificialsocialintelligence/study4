package com.asist.asistmod.MissionCommon.InterventionManagement.ChatInterventions;

public class LongAndChat {
	
	long longTime;
	String content;
	String receiver;
	
	public LongAndChat(long t, String c, String receiver) {
		this.longTime = t;
		this.content = c;
		this.receiver = receiver;
	}
}
