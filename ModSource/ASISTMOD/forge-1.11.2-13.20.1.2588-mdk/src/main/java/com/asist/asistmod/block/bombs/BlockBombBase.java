
package com.asist.asistmod.block.bombs;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.MissionCommon.Utilities.AttributeManagement.AttributedObjectManager;
import com.asist.asistmod.MissionSpecific.MissionA.BombManagement.BombBookkeeper;
import com.asist.asistmod.MissionSpecific.MissionA.BombManagement.Explosions.ExplosionManager;
import com.asist.asistmod.MissionSpecific.MissionA.BombRemovedBroadcasting.BombRemovedBroadcaster;
import com.asist.asistmod.MissionSpecific.MissionA.FireManagement.FireManager;
import com.asist.asistmod.MissionSpecific.MissionA.GuiManagement.ASISTCreativeTabs.ASISTModTabs;
import com.asist.asistmod.MissionSpecific.MissionA.ItemManagement.ItemManager;
import com.asist.asistmod.MissionSpecific.MissionA.ItemManagement.ItemType;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.Player;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.PlayerManager;
import com.asist.asistmod.MissionSpecific.MissionA.RegionManagement.RegionManager;
import com.asist.asistmod.MissionSpecific.MissionA.ScoreboardManagement.ScoreboardManager;
import com.asist.asistmod.MissionSpecific.MissionA.ShopManagement.ServerSideShopManager;
import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimer;
import com.asist.asistmod.block.beacons.BlockBeaconBasic;
import com.asist.asistmod.datamodels.EnvironmentRemoved.Single.EnvironmentRemovedSingleModel;
import com.asist.asistmod.datamodels.ObjectStateChange.ObjectStateChangeData;
import com.asist.asistmod.datamodels.ObjectStateChange.ObjectStateChangeModel;
import com.asist.asistmod.datamodels.Perturbation.PerturbationModel;
import com.asist.asistmod.mqtt.InternalMqttClient;
import com.asist.asistmod.tile_entity.BeaconTileEntity;
import com.asist.asistmod.tile_entity.BombBlockTileEntity;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyInteger;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockBombBase extends BlockContainer {


	public static final PropertyInteger EXPLODE = PropertyInteger.create("explode", 0, 3);
	
	public static enum BombOutcomeEnum{
		TRIGGERED,
		TRIGGERED_ADVANCE_SEQ,
		DEFUSED,
		DEFUSED_DISPOSER,
		EXPLODE_TOOL_MISMATCH,
		EXPLODE_CHAINED_ERROR,
		EXPLODE_TIME_LIMIT,
		EXPLODE_FIRE,
		PERTURBATION_FIRE_TRIGGER};
	
	public BlockBombBase() {

		super(Material.WOOD);
		this.setDefaultState(this.blockState.getBaseState().withProperty(EXPLODE, 0));
		this.setTickRandomly(true);
		this.setCreativeTab(ASISTModTabs.ASISTBlocks);

	} 

	// MAKES THE RENDERER WORK, OTHERWISE BLOCK IS INVISIBLE
	@Override
	public EnumBlockRenderType getRenderType(IBlockState state) {
		return EnumBlockRenderType.MODEL;
	}

	// DONT DROP ANY ITEMS
	@Override public int quantityDropped(Random par1Random){ return 0;}

	@Override
	public void onBlockClicked(World world, BlockPos pos, EntityPlayer player) {

		if(!world.isRemote) {
			
			BombBlockTileEntity te = (BombBlockTileEntity)world.getTileEntity(pos);
			Player p = PlayerManager.getPlayerByEntityPlayer(player);
			String pid = p.getParticipantId();			
			String id = te.getBombId();
			
			if(te.isPerturbationTrigger) {
				destroyBomb(world, pos, pid, id, BlockBombBase.BombOutcomeEnum.PERTURBATION_FIRE_TRIGGER);
				FireManager.fireSourceBombsArray.remove(id);
				FireManager.placeFire(world,pos,pid,false);
				//FireManager.perturbationHappened = true;				
				PerturbationModel perturbationModel = new PerturbationModel("FIRE",pos.getX(),pos.getY(),pos.getZ(),id);
				perturbationModel.publish();				
				System.out.println("Perturbation Bomb Exploded before 5 minutes ...");
				return;
			}

			char[] seq = te.getDefuseSequence();

			ItemStack itemStack = player.getHeldItemMainhand();

			ItemType itemType = ItemManager.getItemTypeFromRegistryName( itemStack.getItem().getRegistryName().toString());

			char effectiveOnColor = ItemManager.wirecutterEffectiveOnColor(itemType);  

			if ( seq.length > 0) {
				if(seq[0]==effectiveOnColor) {
					
					char[] advSeq = te.advanceSequence();					
					te.addParticipantContribution(pid);
					//System.out.println( "Increasing Score!");
					ScoreboardManager.increasePlayerScore(pid, ScoreboardManager.PlayerHelpedBombValue);

					// START UPDATE ATTRIBUTES AND SEND OBJECT_STATE_CHANGE MESSAGE
					StringBuilder sb = new StringBuilder();
					for(char c : advSeq) {
						sb.append(c);
					}
					Map<String,String> newAttr = new HashMap<String,String>();
					newAttr.put("sequence", sb.toString() );
					newAttr.put("sequence_index", Integer.toString(te.getDefuseIndex()) );
					newAttr.put("active", Boolean.toString(te.isCountingDown()));
					newAttr.put("outcome", BombOutcomeEnum.TRIGGERED_ADVANCE_SEQ.name());
					newAttr.put("fuse_duration", String.valueOf(te.getFuseDuration()));
					
					Map<String, String[]> changedAttributes = AttributedObjectManager.updateObjectAttributes(id, newAttr);
					
					// Build Message
					ObjectStateChangeModel objectStateChangeModel = new ObjectStateChangeModel(
							id,
							this.getRegistryName().toString().split(":")[1],
							pid,
							pos.getX(),
							pos.getY(),
							pos.getZ(),
							changedAttributes,
							AttributedObjectManager.getObjectAttributes(id)
					);
					
					InternalMqttClient.publish(objectStateChangeModel.toJsonString(), objectStateChangeModel.getTopic());
					// END UPDATE ATTRIBUTES AND SEND OBJECT_STATE_CHANGE MESSAGE
					
					//Add to budget
					ServerSideShopManager.addToTeamBudget(1);
					if(advSeq.length == 0)
					{
						
						te.pauseCountDown();
						
						MissionTimer.unsubscribeToTimerTicks(te);
						
						//Bookkeeping
						BombBookkeeper.addBombToDefused(id);
						
						//RegionManager.removeBombFromRegion(id, pos);
						BombRemovedBroadcaster.bombRemoved(id);
						
						//remove any beacon thats on top
						removeAssociatedBeacon(world,pos, this,pid);
						
						world.removeTileEntity(pos);
						

						newAttr.put("outcome", BombOutcomeEnum.DEFUSED.name());
						newAttr.put("active", "false");
						changedAttributes = AttributedObjectManager.updateObjectAttributes(id, newAttr);
						// Build Message
						objectStateChangeModel = new ObjectStateChangeModel(
								id,
								this.getRegistryName().toString().split(":")[1],
								pid,
								pos.getX(),
								pos.getY(),
								pos.getZ(),
								changedAttributes,
								AttributedObjectManager.getObjectAttributes(id)
						);

						InternalMqttClient.publish(objectStateChangeModel.toJsonString(), objectStateChangeModel.getTopic());
						
						// EnvironmentRemovedSingle
						EnvironmentRemovedSingleModel envRemovedModel = new EnvironmentRemovedSingleModel(p.participant_id,objectStateChangeModel.data);
						InternalMqttClient.publish(envRemovedModel.toJsonString(), envRemovedModel.getTopic());
					
					}
				}
				else {
					destroyBomb(world, pos, pid, id,BombOutcomeEnum.EXPLODE_TOOL_MISMATCH);
				}
				
				// REDUCE THE STACK SIZE	
				itemStack.setItemDamage(0);
				player.renderBrokenItemStack(itemStack);				
				itemStack.shrink(1);
			}
		}
	}


	@SideOnly(Side.SERVER)
	public void destroyBomb(World world, BlockPos pos, String pid, String bombId, BombOutcomeEnum explodeReason) {
		
		System.out.println("Destroy Bomb ID " + bombId);
		
		//Bookkeeping
		BombBookkeeper.addBombToExploded(bombId);
		
		//RegionManager.removeBombFromRegion(bombId, pos);
		BombRemovedBroadcaster.bombRemoved(bombId);
		
		
		//probably need to know if we destroyed from tool mismatch or from chained failure	
		BombBlockTileEntity te = (BombBlockTileEntity)world.getTileEntity(pos);
		
		String registryName = this.getRegistryName().toString().split(":")[1];
		
		//remove any beacon thats on top
		removeAssociatedBeacon(world, pos,this, pid);
		
		ExplosionManager.createExplosion(bombId, this.getRegistryName().toString().split(":")[1], world, pos.getX(), pos.getY(), pos.getZ());
		
		te.pauseCountDown();
		
		MissionTimer.unsubscribeToTimerTicks(te);
		

		world.removeTileEntity(pos);
		

		// START UPDATE ATTRIBUTES AND SEND OBJECT_STATE_CHANGE MESSAGE
		StringBuilder sb = new StringBuilder();
		for(char c : te.getDefuseSequence()) {
			sb.append(c);
		}
		Map<String,String> newAttr = new HashMap<String,String>();
		newAttr.put("sequence", sb.toString() );
		newAttr.put("sequence_index", Integer.toString(te.getDefuseIndex()) );
		newAttr.put("active", "false");		
		newAttr.put("outcome", explodeReason.name());
		newAttr.put("fuse_duration", String.valueOf(te.getFuseDuration()));

		Map<String, String[]> changedAttributes = AttributedObjectManager.updateObjectAttributes(bombId, newAttr);
		
		

		// Build Message
		ObjectStateChangeModel objectStateChangeModel = new ObjectStateChangeModel(
				bombId,
				registryName,
				pid,
				pos.getX(),
				pos.getY(),
				pos.getZ(),
				changedAttributes,
				AttributedObjectManager.getObjectAttributes(bombId)
		);
		

		// END UPDATE ATTRIBUTES AND SEND OBJECT_STATE_CHANGE MESSAGE
		InternalMqttClient.publish(objectStateChangeModel.toJsonString(), objectStateChangeModel.getTopic());
		

		//ENVIRONMENT REMOVED MESSAGE

		EnvironmentRemovedSingleModel envRemovedModel = new EnvironmentRemovedSingleModel(pid,objectStateChangeModel.data);
		
		InternalMqttClient.publish(envRemovedModel.toJsonString(), envRemovedModel.getTopic());
		

		
	}


	@Override 
	public void onBlockDestroyedByPlayer(World world, BlockPos pos, IBlockState blockState) {
		world.removeTileEntity(pos);
	}

	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta ) {

		BombBlockTileEntity tileEntity = new BombBlockTileEntity();

		return tileEntity;
	}

	@Override public void updateTick(World worldIn, BlockPos pos, IBlockState state, Random random) {
		////System.out.println("Normal Tick Update?" );
	}

	@Override public void randomTick(World worldIn, BlockPos pos, IBlockState state, Random random) {
		////System.out.println("Random Tick Update! : " + pos.toString() );
	}

	@Override public int tickRate(World worldIn) {
		return 1;
	}

	/**
	 * Return whether this block can drop from an explosion.
	 */
	public boolean canDropFromExplosion(Explosion explosionIn)
	{
		return false;
	}

	/**
	 * Convert the given metadata into a BlockState for this Block
	 */
	public IBlockState getStateFromMeta(int meta)
	{
		return this.getDefaultState().withProperty(EXPLODE, meta);
	}

	/**
	 * Convert the BlockState into the correct metadata value
	 */
	public int getMetaFromState(IBlockState state)
	{
		return ((int)state.getValue(EXPLODE));
	}

	//This NEEDS to be here when creating a property like this.
	protected BlockStateContainer createBlockState()
	{
		return new BlockStateContainer(this, new IProperty[] {EXPLODE});
	}
	
	public static void removeAssociatedBeacon(World world, BlockPos pos, Block block, String pid) {
		
		IBlockState blockState = world.getBlockState(pos.up());
		
		//System.out.println("Removing a beacon @ " + pos.up().toString());
		
		if( blockState.getBlock() instanceof BlockBeaconBasic ) {
			
			String beaconType = world.getBlockState(pos.up()).getBlock().getRegistryName().toString().split(":")[1];
			BeaconTileEntity te = (BeaconTileEntity)world.getTileEntity(pos.up());
			String id = te.getBeaconId();
			//System.out.println("Found beacon on top! : " + id);
			
			IBlockState air = ForgeRegistries.BLOCKS.getValue(new ResourceLocation("minecraft", "air")).getDefaultState();		
			world.setBlockState(pos.up(), air,3);
			world.removeTileEntity(pos.up());
			
			// Split out ids for querying
			String stackid = id.split("_")[0];
			String itemid = id.split("_")[1];
			
			// EnvironmentRemovedSingle
			ObjectStateChangeData data = new ObjectStateChangeData(false);
        	data.id = id;
    		data.type = beaconType;
    		data.x = pos.up().getX();
    		data.y = pos.up().getY();
    		data.z = pos.up().getZ();
    		data.currAttributes = AttributedObjectManager.getItemAttributes(stackid, itemid);
			EnvironmentRemovedSingleModel envRemovedModel = new EnvironmentRemovedSingleModel(pid,data);
			InternalMqttClient.publish(envRemovedModel.toJsonString(), envRemovedModel.getTopic());
		}
		else {
			//System.out.println("No associated beacon!");
		}
		
	}


}
