package com.asist.asistmod.datamodels.Scoreboard;

import java.util.concurrent.ConcurrentHashMap;

import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimer;

public class ScoreboardData {
	
	public String mission_timer = MissionTimer.getMissionTimeString();
    public long elapsed_milliseconds = MissionTimer.getElapsedMillisecondsGlobal();
    public ConcurrentHashMap<String, Integer> playerScores = null;
    public Integer teamScore = null;
}
