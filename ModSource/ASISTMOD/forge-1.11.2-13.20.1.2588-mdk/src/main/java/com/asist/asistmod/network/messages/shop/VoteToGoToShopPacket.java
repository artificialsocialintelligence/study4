package com.asist.asistmod.network.messages.shop;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.MissionSpecific.MissionA.MissionManagement.MissionManager;
import com.asist.asistmod.MissionSpecific.MissionA.MissionManagement.MissionManager.MissionStage;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.Player;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.PlayerManager;
import com.asist.asistmod.MissionSpecific.MissionA.ShopManagement.ServerSideShopManager;
import com.asist.asistmod.datamodels.CommunicationEnvironment.CommunicationEnvironmentModel;
import com.asist.asistmod.mqtt.InternalMqttClient;
import com.asist.asistmod.network.MessagePacket;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;

public class VoteToGoToShopPacket extends MessagePacket<VoteToGoToShopPacket> {
	
	int callSignLength = 0;
	String callSign="";
	
	
	public VoteToGoToShopPacket() {
		
	}
	
	 /** Constructor for server-side
	 * @param displayShop  */
	public VoteToGoToShopPacket(String callSign) {
		this.callSign = callSign;
		this.callSignLength = callSign.length();		
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		callSignLength=buf.readInt();
		StringBuilder sb = new StringBuilder();
		for(int i=0;i<callSignLength;i++) {
			sb.append(buf.readChar());
		}
		callSign = sb.toString();
		
	}
	
	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(callSignLength);
		for(int i=0;i<callSignLength;i++) {
			buf.writeChar(callSign.toCharArray()[i]);
		}
		
	}

	@Override
	public void handleClientSide(VoteToGoToShopPacket message, EntityPlayer player) {
		//if(AsistMod.side == Side.CLIENT) {
			
		//}
		
	}

	@Override
	public void handleServerSide(VoteToGoToShopPacket message, EntityPlayer player) {
			
		String name = player.getName();
		Player p = PlayerManager.getPlayerByName(name);
		//System.out.println("Processing Vote To Go To Shop Packet From : " + message.callSign);
		String m = null;
		String[] receivers = null;
		if( p.isFrozen ) {
			
			m = "You cannot vote to go to the store if you are frozen! Have another player help you first!";
			AsistMod.server.commandManager.executeCommand(AsistMod.server, "/tell "+name+" "+m);
			receivers = new String[]{p.participant_id};
		}
		
		else if( MissionManager.stage == MissionStage.RECON_STAGE ) {
			
			m = "Please complete this short recon tutorial phase. You will be able to go to the Shop at will in the non tutorial stages!";
			AsistMod.server.commandManager.executeCommand(AsistMod.server, "/tell "+name+" "+m);
			receivers = new String[]{p.participant_id};
			
		}
		else if( MissionManager.stage == MissionStage.SHOP_STAGE ) {
			
			m = "You're already in the Shop silly!";
			AsistMod.server.commandManager.executeCommand(AsistMod.server, "/tell "+name+" "+m);
			receivers = new String[]{p.participant_id};
			
		}
		else if (MissionManager.stage == MissionStage.FIELD_STAGE) {
			m = p.callSign + " voted to go to the Shop!";
			AsistMod.server.commandManager.executeCommand(AsistMod.server, "/tell @a "+m);
			ServerSideShopManager.setPlayerReadyToProceedToShop(message.callSign);
			
			receivers = new String[PlayerManager.players.size()];
			for(int i =0; i<PlayerManager.players.size(); i++) {
				receivers[i] = PlayerManager.players.get(i).participant_id;
			}
		}
		
		if( receivers != null ) {
			
			CommunicationEnvironmentModel comEnvModel = new CommunicationEnvironmentModel(
					"SERVER", 
					"SERVER",
					receivers,
					m,
					0,
					0,
					0,
					null
			);
			InternalMqttClient.publish(comEnvModel.toJsonString(), comEnvModel.getTopic());			
		}
	}
}
