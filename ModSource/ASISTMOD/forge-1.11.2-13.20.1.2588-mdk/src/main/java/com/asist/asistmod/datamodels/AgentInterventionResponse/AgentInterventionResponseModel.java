package com.asist.asistmod.datamodels.AgentInterventionResponse;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.datamodels.Header.HeaderModel;
import com.asist.asistmod.datamodels.Msg.MsgModel;
import com.asist.asistmod.mqtt.MessageTopic;

public class AgentInterventionResponseModel {
	
	
	
	public HeaderModel header = new HeaderModel();
	
	public MsgModel msg = new MsgModel(getMessageTopicEnum().getEventName(),getMessageTopicEnum().getVersion());
	
	public AgentInterventionResponseData data = new AgentInterventionResponseData();
	
	public AgentInterventionResponseModel(String pid, String intervId, String agentId, int respIndex) {	
		
		data.participant_id = pid;
		data.intervention_id = intervId;
		data.agent_id = agentId;
		data.response_index = respIndex;
	}
	
	public MessageTopic getMessageTopicEnum() { 		
		
		return MessageTopic.AGENT_INTERVENTION_RESPONSE; 
		
	}
	
	public String getTopic(String agentId) { 		
		
		return getMessageTopicEnum().getTopicWithAgent(agentId); 
		
	}
	
	public String toJsonString() { 		
		
		return AsistMod.gson.toJson(this); 
		
	}

}
