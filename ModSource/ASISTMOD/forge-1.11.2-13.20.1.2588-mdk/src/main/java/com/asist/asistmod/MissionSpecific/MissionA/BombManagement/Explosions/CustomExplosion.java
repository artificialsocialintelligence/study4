package com.asist.asistmod.MissionSpecific.MissionA.BombManagement.Explosions;

import java.util.List;
import java.util.Map;
import java.util.Random;

import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.Player;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.PlayerManager;
import com.asist.asistmod.MissionSpecific.MissionA.ScoreboardManagement.ScoreboardManager;
import com.asist.asistmod.block._Blocks;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.enchantment.EnchantmentProtection;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;

public class CustomExplosion extends Explosion {

	/** whether or not the explosion sets fire to blocks around it */
    private final boolean isFlaming;
    /** whether or not this explosion spawns smoke particles */
    private final boolean isSmoking;
    private final Random explosionRNG;
    private final World world;
    private final double explosionX;
    private final double explosionY;
    private final double explosionZ;
    private final Entity exploder;
    private final float explosionSize;
    /** A list of ChunkPositions of blocks affected by this explosion */
    private final List<BlockPos> affectedBlockPositions;
    /** Maps players to the knockback vector applied by the explosion, to send to the client */
    private final Map<EntityPlayer, Vec3d> playerKnockbackMap;
    private final Vec3d position;
    private String bombId;
    private String bombType;
    
	
	public CustomExplosion(String bombId, String bombType, World worldIn, Entity entityIn, double x, double y, double z, float size, boolean flaming,
			boolean smoking) { 
			
		super(worldIn, entityIn, x, y, z, size, flaming, smoking);
		this.explosionRNG = new Random();
        this.affectedBlockPositions = Lists.<BlockPos>newArrayList();
        this.playerKnockbackMap = Maps.<EntityPlayer, Vec3d>newHashMap();
        this.world = worldIn;
        this.exploder = entityIn;
        this.explosionSize = size;
        this.explosionX = x;
        this.explosionY = y;
        this.explosionZ = z;
        this.isFlaming = flaming;
        this.isSmoking = smoking;
        this.position = new Vec3d(explosionX, explosionY, explosionZ);
        
        this.bombId = bombId;
        this.bombType =bombType;
		
	}
	
	
	@Override
	public void doExplosionA() {
		//System.out.println("Custom Explosion A");
		
        //this.affectedBlockPositions.addAll(set);
		//float f3 = this.explosionSize * 2.0F;
        //int k1 = MathHelper.floor(this.explosionX - (double)f3 - 1.0D);
        //int l1 = MathHelper.floor(this.explosionX + (double)f3 + 1.0D);
        //int i2 = MathHelper.floor(this.explosionY - (double)f3 - 1.0D);
        //int i1 = MathHelper.floor(this.explosionY + (double)f3 + 1.0D);
        //int j2 = MathHelper.floor(this.explosionZ - (double)f3 - 1.0D);
        //int j1 = MathHelper.floor(this.explosionZ + (double)f3 + 1.0D);
        
        float f3 = this.explosionSize;
        int k1 = MathHelper.floor(this.explosionX - (double)f3);
        int l1 = MathHelper.floor(this.explosionX + (double)f3);
        int i2 = MathHelper.floor(this.explosionY - (double)f3);
        int i1 = MathHelper.floor(this.explosionY + (double)f3);
        int j2 = MathHelper.floor(this.explosionZ - (double)f3);
        int j1 = MathHelper.floor(this.explosionZ + (double)f3);
        
        // this gives us a visual representation of the explosion radius
        // this.world.setBlockState(new BlockPos(k1,this.explosionY,this.explosionZ), ForgeRegistries.BLOCKS.getValue(new ResourceLocation("minecraft", "glowstone")).getDefaultState());
        
        List<Entity> list = this.world.getEntitiesWithinAABBExcludingEntity(this.exploder, new AxisAlignedBB((double)k1, (double)i2, (double)j2, (double)l1, (double)i1, (double)j1));
        net.minecraftforge.event.ForgeEventFactory.onExplosionDetonate(this.world, this, list, f3);
        Vec3d vec3d = new Vec3d(this.explosionX, this.explosionY, this.explosionZ);

        for (int k2 = 0; k2 < list.size(); ++k2)
        {
            Entity entity = (Entity)list.get(k2);
            
            //System.out.println(entity.getName());

            if (!entity.isImmuneToExplosions())
            {
                double d12 = entity.getDistance(this.explosionX, this.explosionY, this.explosionZ) / (double)f3;

                if (d12 <= 1.0D)
                {
                    double d5 = entity.posX - this.explosionX;
                    double d7 = entity.posY + (double)entity.getEyeHeight() - this.explosionY;
                    double d9 = entity.posZ - this.explosionZ;
                    double d13 = (double)MathHelper.sqrt(d5 * d5 + d7 * d7 + d9 * d9);

                    if (d13 != 0.0D)
                    {
                        d5 = d5 / d13;
                        d7 = d7 / d13;
                        d9 = d9 / d13;
                        double d14 = (double)this.world.getBlockDensity(vec3d, entity.getEntityBoundingBox());
                        double d10 = (1.0D - d12) * d14;
                      
                        
                        double d11 = d10;

                        if (entity instanceof EntityLivingBase)
                        {
                            d11 = EnchantmentProtection.getBlastDamageReduction((EntityLivingBase)entity, d10);
                        }

                        entity.motionX += d5 * d11;
                        entity.motionY += d7 * d11;
                        entity.motionZ += d9 * d11;

                        if (entity instanceof EntityPlayer)
                        {
                            EntityPlayer entityplayer = (EntityPlayer)entity;

                            if (!entityplayer.isSpectator() && (!entityplayer.isCreative() || !entityplayer.capabilities.isFlying))
                            {
                            	BlockPos pos = new BlockPos(this.explosionX,this.explosionY,this.explosionZ);
                            	Player player = PlayerManager.getPlayerByName(entityplayer.getName());
                            	player.lastDamagedById = bombId;
                        		player.lastDamagedByType = bombType;
                        		player.lastDamagedByPosition = pos;
                                // -------------------------------------------------------------------------
                                
                            	if ( player.armorIsEquipped ) {
                            		//System.out.println(player.getParticipantId() + " : Armor blocked the explosion!");
                            		player.removePlayerArmor();
                            		player.setArmorEquipped(false,player.getParticipantId(),entity.getPosition(),this.bombId,this.bombType,	pos );
                            	}
                            	else {
                            		// THIS IS WHERE WE SET THE DAMAGE QUANTITY DEALT ON EXPLOSION AS A FLOAT
                                    // WE USE GENERIC AS DAMAGE SOURCE BECAUSE IT IS UNIFORM AND CONTROLLABLE
                            		float damage = 10f;
                            		float remainingHealth = player.getEntityPlayer().getHealth();
                            		if( remainingHealth <= damage ) { 
                            			damage = remainingHealth - 1f;
                            		}
                            		entity.attackEntityFrom(DamageSource.GENERIC, damage);                            		
                            	}                                
                                // -------------------------------------------------------------------------
                                
                                this.playerKnockbackMap.put(entityplayer, new Vec3d(d5 * d10, d7 * d10, d9 * d10));
                            }
                        }
                    }
                }
            }
        }
	}
	
	public void doExplosionB(boolean spawnParticles) {
		//System.out.println("Custom Explosion B");
		 this.world.playSound((EntityPlayer)null, this.explosionX, this.explosionY, this.explosionZ, SoundEvents.ENTITY_GENERIC_EXPLODE, SoundCategory.BLOCKS, 4.0F, (1.0F + (this.world.rand.nextFloat() - this.world.rand.nextFloat()) * 0.2F) * 0.7F);

	        if (this.explosionSize >= 2.0F && this.isSmoking)
	        {
	            this.world.spawnParticle(EnumParticleTypes.EXPLOSION_HUGE, this.explosionX, this.explosionY, this.explosionZ, 1.0D, 0.0D, 0.0D, new int[0]);
	        }
	        else
	        {
	            this.world.spawnParticle(EnumParticleTypes.EXPLOSION_LARGE, this.explosionX, this.explosionY, this.explosionZ, 1.0D, 0.0D, 0.0D, new int[0]);
	        }

	        if (this.isSmoking)
	        {
	            for (BlockPos blockpos : this.affectedBlockPositions)
	            {
	                IBlockState iblockstate = this.world.getBlockState(blockpos);
	                Block block = iblockstate.getBlock();

	                if (spawnParticles)
	                {
	                    double d0 = (double)((float)blockpos.getX() + this.world.rand.nextFloat());
	                    double d1 = (double)((float)blockpos.getY() + this.world.rand.nextFloat());
	                    double d2 = (double)((float)blockpos.getZ() + this.world.rand.nextFloat());
	                    double d3 = d0 - this.explosionX;
	                    double d4 = d1 - this.explosionY;
	                    double d5 = d2 - this.explosionZ;
	                    double d6 = (double)MathHelper.sqrt(d3 * d3 + d4 * d4 + d5 * d5);
	                    d3 = d3 / d6;
	                    d4 = d4 / d6;
	                    d5 = d5 / d6;
	                    double d7 = 0.5D / (d6 / (double)this.explosionSize + 0.1D);
	                    d7 = d7 * (double)(this.world.rand.nextFloat() * this.world.rand.nextFloat() + 0.3F);
	                    d3 = d3 * d7;
	                    d4 = d4 * d7;
	                    d5 = d5 * d7;
	                    this.world.spawnParticle(EnumParticleTypes.EXPLOSION_NORMAL, (d0 + this.explosionX) / 2.0D, (d1 + this.explosionY) / 2.0D, (d2 + this.explosionZ) / 2.0D, d3, d4, d5, new int[0]);
	                    this.world.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, d0, d1, d2, d3, d4, d5, new int[0]);
	                }

	                if (iblockstate.getMaterial() != Material.AIR)
	                {
	                    if (block.canDropFromExplosion(this))
	                    {
	                        block.dropBlockAsItemWithChance(this.world, blockpos, this.world.getBlockState(blockpos), 1.0F / this.explosionSize, 0);
	                    }

	                    block.onBlockExploded(this.world, blockpos, this);
	                }
	            }
	        }

	        if (this.isFlaming)
	        {
	            for (BlockPos blockpos1 : this.getAffectedBlockPositions())
	            {
	                if (this.world.getBlockState(blockpos1).getMaterial() == Material.AIR && this.world.getBlockState(blockpos1.down()).isFullBlock() && this.explosionRNG.nextInt(3) == 0)
	                {
	                    this.world.setBlockState(blockpos1, _Blocks.blockFireCustom.getDefaultState());
	                }
	            }
	        }
	}

}
