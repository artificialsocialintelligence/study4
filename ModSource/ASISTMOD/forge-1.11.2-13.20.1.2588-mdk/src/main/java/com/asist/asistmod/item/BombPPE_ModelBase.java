package com.asist.asistmod.item;

// Made with Blockbench 4.5.2
// Exported for Minecraft version 1.7 - 1.12
// Paste this class into your mod and generate all required imports
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.model.ModelBox;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;


public class BombPPE_ModelBase extends ModelBiped {

	private final ModelRenderer armorHead;
	private final ModelRenderer armorBody;
	private final ModelRenderer armorRightArm;
	private final ModelRenderer armorLeftArm;
	private final ModelRenderer armorLeftLeg;
	private final ModelRenderer armorLeftBoot;
	private final ModelRenderer armorRightLeg;
	private final ModelRenderer armorRightBoot;

	public BombPPE_ModelBase() {
		textureWidth = 128;
		textureHeight = 128;

		armorHead = new ModelRenderer(this);
		armorHead.setRotationPoint(0.0F, 0.0F, 0.0F);
		bipedHead.addChild(armorHead);
		armorHead.cubeList.add(new ModelBox(armorHead, 83, 103, -5.0F, -8.0F, -5.0F, 10, 9, 10, 0.0F, false));

		armorBody = new ModelRenderer(this);
		armorBody.setRotationPoint(0.0F, 0.0F, 0.0F);
		bipedBody.addChild(armorBody);
		armorBody.cubeList.add(new ModelBox(armorBody, 84, 68, -5.0F, -2.0F, -6.0F, 3, 4, 1, 0.0F, false));
		armorBody.cubeList.add(new ModelBox(armorBody, 64, 18, -5.0F, 1.0F, -5.0F, 10, 1, 3, 0.0F, false));
		armorBody.cubeList.add(new ModelBox(armorBody, 59, 77, -6.0F, -4.0F, -5.0F, 1, 6, 3, 0.0F, false));
		armorBody.cubeList.add(new ModelBox(armorBody, 39, 77, -6.0F, -5.0F, -2.0F, 1, 5, 4, 0.0F, false));
		armorBody.cubeList.add(new ModelBox(armorBody, 50, 77, -6.0F, -5.0F, 2.0F, 1, 6, 3, 0.0F, false));
		armorBody.cubeList.add(new ModelBox(armorBody, 31, 0, -5.0F, -5.0F, 5.0F, 10, 6, 1, 0.0F, false));
		armorBody.cubeList.add(new ModelBox(armorBody, 56, 13, -5.0F, 1.0F, 2.0F, 10, 1, 3, 0.0F, false));
		armorBody.cubeList.add(new ModelBox(armorBody, 83, 63, -2.0F, -1.0F, -6.0F, 4, 3, 1, 0.0F, false));
		armorBody.cubeList.add(new ModelBox(armorBody, 56, 0, -4.0F, 2.0F, 2.0F, 8, 10, 2, 0.0F, false));
		armorBody.cubeList.add(new ModelBox(armorBody, 0, 54, -4.0F, 2.0F, -4.0F, 8, 10, 2, 0.0F, false));
		armorBody.cubeList.add(new ModelBox(armorBody, 28, 77, 5.0F, -5.0F, -2.0F, 1, 5, 4, 0.0F, false));
		armorBody.cubeList.add(new ModelBox(armorBody, 0, 84, 2.0F, -2.0F, -6.0F, 3, 4, 1, 0.0F, false));
		armorBody.cubeList.add(new ModelBox(armorBody, 11, 67, 5.0F, -5.0F, 2.0F, 1, 6, 3, 0.0F, false));
		armorBody.cubeList.add(new ModelBox(armorBody, 29, 87, 5.0F, -4.0F, -5.0F, 1, 6, 3, 0.0F, false));

		armorRightArm = new ModelRenderer(this);
		armorRightArm.setRotationPoint(0.0F, 0.0F, 0.0F);
		bipedRightArm.addChild(armorRightArm);
		//                                 ModelBox( renderer,            U,            V,             X_1,             X_2,             Y_1,         Y_2,         Z_1,         Z_2,           delta,          mirror)
		armorRightArm.cubeList.add(new ModelBox(armorRightArm, 72, 56, -4.0F, -2.0F, -2.0F, 1, 12, 4, 0.0F, false));
		armorRightArm.cubeList.add(new ModelBox(armorRightArm, 77, 0,  -4.0F, 0.0F, 2.0F, 5, 10, 1, 0.0F, false));
		armorRightArm.cubeList.add(new ModelBox(armorRightArm, 11, 77, -4.0F, -3.0F, -2.0F, 4, 1, 4, 0.0F, false));
		armorRightArm.cubeList.add(new ModelBox(armorRightArm, 83, 58, -4.0F, -3.0F, 2.0F, 4, 3, 1, 0.0F, false));
		armorRightArm.cubeList.add(new ModelBox(armorRightArm, 83, 12, -4.0F, -3.0F, -3.0F, 4, 3, 1, 0.0F, false));
		armorRightArm.cubeList.add(new ModelBox(armorRightArm, 72, 73, -4.0F, 0.0F, -3.0F, 5, 10, 1, 0.0F, false));

		armorLeftArm = new ModelRenderer(this);
		armorLeftArm.setRotationPoint(0.0F, 0.0F, 0.0F);
		bipedLeftArm.addChild(armorLeftArm);
		armorLeftArm.cubeList.add(new ModelBox(armorLeftArm, 73, 35, -1.0F, 0.0F, -3.0F, 5, 10, 1, 0.0F, false));
		armorLeftArm.cubeList.add(new ModelBox(armorLeftArm, 10, 83, 0.0F, -3.0F, -3.0F, 4, 3, 1, 0.0F, false));
		armorLeftArm.cubeList.add(new ModelBox(armorLeftArm, 79, 53, 0.0F, -3.0F, 2.0F, 4, 3, 1, 0.0F, false));
		armorLeftArm.cubeList.add(new ModelBox(armorLeftArm, 0, 67, 3.0F, -2.0F, -2.0F, 1, 12, 4, 0.0F, false));
		armorLeftArm.cubeList.add(new ModelBox(armorLeftArm, 73, 23, -1.0F, 0.0F, 2.0F, 5, 10, 1, 0.0F, false));
		armorLeftArm.cubeList.add(new ModelBox(armorLeftArm, 75, 47, 0.0F, -3.0F, -2.0F, 4, 1, 4, 0.0F, false));

		armorLeftLeg = new ModelRenderer(this);
		armorLeftLeg.setRotationPoint(0.0F, 0.0F, 0.0F);
		bipedLeftLeg.addChild(armorLeftLeg);
		armorLeftLeg.cubeList.add(new ModelBox(armorLeftLeg, 0, 37, -3.0F, -2.0F, -3.0F, 6, 10, 6, 0.0F, false));

		armorLeftBoot = new ModelRenderer(this);
		armorLeftBoot.setRotationPoint(0.0F, 0.0F, 0.0F);
		bipedLeftLeg.addChild(armorLeftBoot);
		armorLeftBoot.cubeList.add(new ModelBox(armorLeftBoot, 25, 29, -3.0F, 7.0F, -4.0F, 7, 5, 8, 0.0F, false));

		armorRightLeg = new ModelRenderer(this);
		armorRightLeg.setRotationPoint(0.0F, 0.0F, 0.0F);
		bipedRightLeg.addChild(armorRightLeg);
		armorRightLeg.cubeList.add(new ModelBox(armorRightLeg, 25, 43, -3.0F, -2.0F, -3.0F, 6, 10, 6, 0.0F, false));

		armorRightBoot = new ModelRenderer(this);
		armorRightBoot.setRotationPoint(0.0F, 0.0F, 0.0F);
		bipedRightLeg.addChild(armorRightBoot);
		armorRightBoot.cubeList.add(new ModelBox(armorRightBoot, 33, 12, -4.0F, 7.0F, -4.0F, 7, 5, 8, 0.0F, false));
	}


	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
		super.render(entity, f, f1, f2, f3, f4, f5);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}

