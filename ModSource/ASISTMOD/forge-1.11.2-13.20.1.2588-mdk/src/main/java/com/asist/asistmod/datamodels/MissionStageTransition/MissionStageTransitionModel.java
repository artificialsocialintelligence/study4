package com.asist.asistmod.datamodels.MissionStageTransition;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.datamodels.Header.HeaderModel;
import com.asist.asistmod.datamodels.Msg.MsgModel;
import com.asist.asistmod.mqtt.MessageTopic;

public class MissionStageTransitionModel{
	
    public HeaderModel header = new HeaderModel();
	
    public MsgModel msg = new MsgModel(getMessageTopicEnum().getEventName(),getMessageTopicEnum().getVersion());
	
	public MissionStageTransitionDataModel data = new MissionStageTransitionDataModel();
	
	
	public MissionStageTransitionModel() {
		
		header.message_type = "event";
		
	}
	
public MessageTopic getMessageTopicEnum() { 		
		
		return MessageTopic.MISSION_STAGE_TRANSITION; 
		
	}
	
	public String getTopic() {
		return getMessageTopicEnum().getTopic();
	}
	
	
	public String toJsonString() { 
		
		return AsistMod.gson.toJson(this); 
		
	}
}
