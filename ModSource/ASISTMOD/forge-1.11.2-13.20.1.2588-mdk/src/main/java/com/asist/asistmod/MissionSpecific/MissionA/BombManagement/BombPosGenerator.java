package com.asist.asistmod.MissionSpecific.MissionA.BombManagement;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import net.minecraft.util.math.BlockPos;

public class BombPosGenerator {
	
	 /** Using a csv file of possible spawn locations for bombs, this randomly selects numOfLocations elements from the csv
	 * file and places them in an array to be returned. 
	 * @param numOfLocations - Size of the array/number of positions for the bombs to spawn
	 * @return - An array of randomly generated bomb positions */
	public static BlockPos[] generateBombPositions(int numOfLocations) {
		BlockPos[] bombSpawns = new BlockPos[numOfLocations];
		ArrayList<BlockPos> possibleSpawns = new ArrayList<BlockPos>();
		
		//Read file of possible locations
		try {
			FileReader fr = new FileReader("./mods/BombSpawnLocations.csv");
			BufferedReader br = new BufferedReader(fr);
			
			String line = "";
			
			//Add each location to possibleSpawns
			try {
				while ((line = br.readLine()) != null) {
					String[] xyz = line.split(",");
					BlockPos possibleBombSpawn = new BlockPos(Integer.parseInt(xyz[0]), Integer.parseInt(xyz[1]), Integer.parseInt(xyz[2]));
					possibleSpawns.add(possibleBombSpawn);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Random rng = new Random();
		//Following line creates a list of numOfLocations number of random integers selected from the interval (0, possibleSpawns.size()) 
		List<Integer> randomLocations = rng.ints(0, possibleSpawns.size()).distinct().limit(numOfLocations).boxed().collect(Collectors.toList());
		
		//Randomly grab locations from possibleSpawns to place in bombSpawns 
		for (int i = 0; i < numOfLocations; i++) {
			bombSpawns[i] = possibleSpawns.get(randomLocations.get(i));
		}
		
		return bombSpawns;
	}
}
