package com.asist.asistmod.datamodels.PlayerStateChange;

import java.util.Map;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.datamodels.Header.HeaderModel;
import com.asist.asistmod.datamodels.Msg.MsgModel;
import com.asist.asistmod.mqtt.MessageTopic;

import net.minecraft.util.math.BlockPos;

public class PlayerStateChangeModel {
	
	
	HeaderModel header = new HeaderModel();
	MsgModel msg = new MsgModel(getMessageTopicEnum().getEventName(),getMessageTopicEnum().getVersion());
	public PlayerStateChangeData data = new PlayerStateChangeData();
	
	public PlayerStateChangeModel(String pid, BlockPos playerPos, String sourceId, String sourceType, 
			BlockPos sourcePos, Map<String,String[]> changedAtt, Map<String, String> currAtt  ) {
		
		data.participant_id = pid;	
		data.player_x = playerPos.getX();
		data.player_y = playerPos.getY();
		data.player_z = playerPos.getZ();
		data.source_id = sourceId;
		data.source_type = sourceType;
		data.source_x = sourcePos.getX();
		data.source_y = sourcePos.getY();
		data.source_z = sourcePos.getZ();
		data.changedAttributes = changedAtt; 
		data.currAttributes = currAtt;
	}
	
	public MessageTopic getMessageTopicEnum() { 		
		
		return MessageTopic.PLAYER_STATE_CHANGE; 
		
	}
	
	public String getTopic() { 		
		
		return getMessageTopicEnum().getTopic(); 
		
	}
	
	public String toJsonString() { 		
		
		return AsistMod.gson.toJson(this); 
		
	}

}
