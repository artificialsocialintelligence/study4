package com.asist.asistmod.MissionCommon.InterventionManagement.ChatInterventions;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.Player;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.PlayerManager;
import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimer;
import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimerListener;
import com.asist.asistmod.datamodels.AgentChatIntervention.AgentChatInterventionModel;
import com.asist.asistmod.mqtt.InternalMqttClient;
import com.asist.asistmod.network.NetworkHandler;
import com.asist.asistmod.network.messages.AgentInterventionChatPacket;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;

public class ChatInterventionManager {
	
	static int id = 0;

	static String interventionTextColor = "yellow";
	
	static ConcurrentHashMap<Integer,LongAndChat> startIntervention =  new ConcurrentHashMap<Integer,LongAndChat>();
	static ConcurrentHashMap<Integer,AgentChatInterventionModel> agentInterventions =  new ConcurrentHashMap<Integer,AgentChatInterventionModel>();

	
	public static void addChatIntervention ( AgentChatInterventionModel model) {	
					
	
		// CONVERT FROM PID TO NAME
		List<String> pids = model.data.receivers;
		
		pids.forEach(pid ->{
			
			String playerName = InternalMqttClient.pid_to_name(pid);
			
			startIntervention.put(id, new LongAndChat(model.data.start,model.data.content,playerName) );
			agentInterventions.put(id, model );
			
			//System.out.println("Entered Chat Intervention ID: " + id);
			
			id++;
			
		});
		
				
		
	}



	
	
	public static void onMissionTimeChange(int m, int s, long elapsedMilliseconds) {

		try {

			for( ConcurrentHashMap.Entry<Integer,LongAndChat> entry : startIntervention.entrySet() ) {

				//System.out.println("Checking Chat ID : " + entry.getKey() );

				LongAndChat e = entry.getValue();

				// less than zero means fire immediately
				if( (e.longTime < 0) || (e.longTime <= elapsedMilliseconds)  ) {


					System.out.println("Triggering chat inervention for " + e.receiver +" @ "+m+" : "+s);
					
					e.content = "AI Advisor : " + e.content;
					
					String parsedMessage = parseInterventionMessage(e.content);

					String interventionMessage = buildASIMinecraftMessage(parsedMessage);


					//MissionTimer.server.commandManager.executeCommand(MissionTimer.server,
					//		"tellraw " + e.receiver + interventionMessage);

					startIntervention.remove( entry.getKey() );



					Player p = PlayerManager.getPlayerByName(e.receiver);

					EntityPlayer ep = p.getEntityPlayer();

					BlockPos pos = ep.getPosition();

					// play sound only for receiving player at their location
					//AsistMod.server.commandManager.executeCommand(AsistMod.server, "playsound minecraft:block.note.pling voice "+e.receiver+" " + pos.getX() + " " + pos.getY() + " " + pos.getZ() );

					//ep.playSound(SoundEvents.BLOCK_NOTE_PLING,1.0f, 1.0f);
					//ep.getEntityWorld().playSound(ep, (double)ep.getPosition().getX(), (double)ep.getPosition().getY(), (double)ep.getPosition().getZ(), SoundEvents.BLOCK_NOTE_PLING, SoundCategory.NEUTRAL, 1.0f, 1.0f);
				}
			}
		}
		catch (Exception e){

			System.out.println(" Tried to trigger a chat intervention, but something went wrong - make sure the receivers field is an array of participant id's, not names.");

			e.printStackTrace();

		}

		try {

			for( ConcurrentHashMap.Entry<Integer,AgentChatInterventionModel> entry : agentInterventions.entrySet() ) {

				AgentChatInterventionModel e = entry.getValue();
				e.data.content = "AI Advisor : " + e.data.content;
				// less than zero means fire immediately
				if( e.data.start<0 || (e.data.start <= elapsedMilliseconds)  ) {
					e.data.receivers.forEach(pid -> {
						EntityPlayer player = PlayerManager.getPlayerByPid(pid).getEntityPlayer();
						NetworkHandler.sendToClient( new AgentInterventionChatPacket(e), player );
						BlockPos pos = player.getPosition();
						// play sound only for receiving player at their location
						//AsistMod.server.commandManager.executeCommand(AsistMod.server, "playsound minecraft:block.note.pling voice "+player.getName()+" " + pos.getX() + " " + pos.getY() + " " + pos.getZ() );
					});
					agentInterventions.remove( entry.getKey() );
				}
			}

		}catch (Exception e){

			e.printStackTrace();

		}
		
	}

	public static String buildASIMinecraftMessage(String parsedMessage)
	{

		String[] interventionMessageList = parsedMessage.split("\n");

		StringBuilder sb = new StringBuilder();
		//sb.append("[");
		sb.append(" [\"\", ");
		int count = interventionMessageList.length;
		for(String message: interventionMessageList)
		{
			count--;
			sb.append("{ \"text\": \"");
			if(count == interventionMessageList.length - 1) {
				sb.append("ADVISOR: ");
			}
			sb.append(message);
			sb.append("\", \"color\": \"" + interventionTextColor + "\"}");
			if(count > 0)
			{
				sb.append(", ");
				sb.append("\"\\n\"");
				sb.append(", ");
			}
			System.out.println(count);
		}
		sb.append("]");

		return sb.toString();
	}

	public static String parseInterventionMessage(String input) {
		int characterMax = 35;
		return parseInterventionMessage(input, characterMax);
	}

	public static String parseInterventionMessage(String input, int characterMax) {

		if (input == null || input.length() <= characterMax) {
			return input;
		}

		StringBuilder result = new StringBuilder();
		String[] splitInput = input.split(" ");

		//Take each word in the message, add its length to the running character total with the space
		//if at any point it goes over the characterMax then insert a new line, reset the character total and continue
		int runningCharacterTotal = 0;
		for (String word: splitInput) {

			//Add additional 1 for the space at the end
			runningCharacterTotal += word.length() + 1;

			if(runningCharacterTotal >= characterMax)
			{
				result.append("\n");
				runningCharacterTotal = 0;
			}

			result.append(word);
			result.append(" ");
		}

		return result.toString();
	}


}
