package com.asist.asistmod.MissionSpecific.MissionA.BlockManagement;

import net.minecraft.block.state.IBlockState;

public class ModBlocks {	
	
	static BlockType bombBlocks[] = {	BlockType.BOMB_STANDARD };

	

	public static BlockType getEnum(IBlockState block) {
		for(BlockType type : BlockType.values()) {
			if(block == type.getDefaultState()) { return type; }
		}
		return BlockType.NULL;
	}
		
	
	public static BlockType getEnum(String blockName) {
		for(BlockType type : BlockType.values()) {
			if(blockName == type.getName()) { return type; }
		}
		return BlockType.NULL;
	}
	
	public static BlockType getEnumFromRegistryName(String registryName) {
		for(BlockType type : BlockType.values()) {
			if( registryName.contentEquals( type.getRegistryName() ) ) { return type; }
		}
		return BlockType.NULL;
	}	

	
	public static boolean isBombBlock(BlockType modBlock) {
		for(BlockType type: bombBlocks) {
			if(modBlock.equals(type)) { return true; }
		}
		return false;
	}

}
