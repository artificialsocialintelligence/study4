package com.asist.asistmod.MissionSpecific.MissionA.BombManagement;

import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.asist.asistmod.MissionSpecific.MissionA.MissionManagement.MissionManager;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.PlayerManager;
import com.asist.asistmod.network.NetworkHandler;
import com.asist.asistmod.network.messages.BombCountUpdatePacket;

import net.minecraft.util.math.BlockPos;


public class BombBookkeeper {
	
	
	public static int bombsDefused = 0;
	public static int bombsExploded = 0;	
	public static int bombsTotal = 0;

	public static ConcurrentHashMap<String, BlockPos> bombsDefusedMap = new ConcurrentHashMap<String, BlockPos>();

	public static ConcurrentHashMap<String, BlockPos> bombsExplodedMap = new ConcurrentHashMap<String, BlockPos>();	

	public static ConcurrentHashMap<String, BlockPos> bombsRemainingMap = new ConcurrentHashMap<String, BlockPos>();


	public static void updateAllPlayers() {
		bombsDefused = bombsDefusedMap.size();
		bombsExploded = bombsExplodedMap.size();
		PlayerManager.players.forEach( player -> {
			
			NetworkHandler.sendToClient(new BombCountUpdatePacket(bombsDefused,bombsExploded,bombsTotal-(bombsDefused+bombsExploded)), player.getEntityPlayer());
		});
	}

	public static void updateAllPlayers(String bomb_id, BlockPos blockPos){
		bombsDefused = bombsDefusedMap.size();
		bombsExploded = bombsExplodedMap.size();
		PlayerManager.players.forEach( player -> {
			NetworkHandler.sendToClient(
					new BombCountUpdatePacket(bombsDefused,bombsExploded,bombsTotal-(bombsDefused+bombsExploded),
					bomb_id, blockPos.getX(), blockPos.getY(), blockPos.getZ()), player.getEntityPlayer());
		});
	}
	
	/*
	public static void incrementDefusedBombs() {
		bombsDefused++;
		updateAllPlayers();
	}
	*/
	
	/*
	public static void incrementExplodedBombs() {
		bombsExploded++;
		updateAllPlayers();
	}
	*/
	
	

	public static void addBombToTotal(String bomb_id, BlockPos position)
	{

		bombsRemainingMap.put(bomb_id, position);

		bombsTotal++;
	}

	public static void addBombToExploded(String bomb_id)
	{
		try {	
			
			BlockPos pos =  bombsRemainingMap.get(bomb_id);
			
			bombsExplodedMap.put(bomb_id, pos);
			
			bombsRemainingMap.remove(bomb_id);
			
			updateAllPlayers();
			
			printBombsRemaining();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		if(bombsRemainingMap.size() == 0 ) {
			MissionManager.endMission( MissionManager.StateChangeOutcome.MISSION_STOP_ALL_BOMBS_REMOVED,false);
		}
	}

	public static void addBombToDefused(String bomb_id)
	{
		
		
		BlockPos pos = bombsRemainingMap.get(bomb_id);
		
		bombsDefusedMap.put(bomb_id, pos );
		
		bombsRemainingMap.remove(bomb_id);
		
		updateAllPlayers();
				
		printBombsRemaining();
		
		
		if(bombsRemainingMap.size() == 0 ) {
			MissionManager.endMission( MissionManager.StateChangeOutcome.MISSION_STOP_ALL_BOMBS_REMOVED,false);
		}

		//bombsDefused++;updateAllPlayers(bomb_id, bombsRemainingMap.get(bomb_id));
	}
	
	public static void correctGhostBomb(String bomb_id)
	{	
		
		
		bombsRemainingMap.remove(bomb_id);
		
		
		
		printBombsRemaining();		
		
		if(bombsRemainingMap.size() == 0 ) {
			MissionManager.endMission( MissionManager.StateChangeOutcome.MISSION_STOP_ALL_BOMBS_REMOVED,false);
		}
	}

	public static Map<String, BlockPos> getBombsDefusedMap()
	{
		return bombsDefusedMap;
	}

	public static Map<String, BlockPos> getBombsExplodedMap()
	{
		return  bombsExplodedMap;
	}

	public static Map<String, BlockPos> getbombsRemainingMap()
	{
		return bombsRemainingMap;
	}
	
	public static void printBombsRemaining() {		
		
		System.out.println("----- BOMBS REMAINING START ------");
		
		Set<String> keys = bombsRemainingMap.keySet();
		
		System.out.println(Arrays.toString(keys.toArray()));
		
		System.out.println("\n----- BOMBS REMAINING END : "+keys.size()+" ------");	}

}
