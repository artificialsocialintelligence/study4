package com.asist.asistmod.MissionSpecific.MissionA.GuiManagement.ContextCommunication;

import java.io.IOException;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;

public class ContextCommunicator extends GuiScreen {

	//final ResourceLocation bg_texture = new ResourceLocation(AsistMod.MODID,"textures/gui/context_menu_bg.png");
	final int bg_texture_width = 256;
	final int bg_texture_height = 62;
	//final int buttonWidth = 30;
	int centerX = 0;
	int centerY = 0;
	Minecraft minecraft;
	
	GuiButton[] buttons = new GuiButton[8];
	
	String[] buttonText = {
			"I need a Red Wirecutter!",
			"I need a Green Wirecutter!",
			"I need a Blue Wirecutter!",
			"This is a Fire Bomb!",
			"This area is clear!",
			"There's fire over here!",
			"Help I can't move!",
			"Oops! Ignore this beacon."
			};
	
	
	public int callSignCode = 0;
	
	public String[] callSigns = null;
	
	public String beaconId = "NOT_SET";
	
	private boolean initMouse = true;
	
	//IBlockState contextBlock;
	
	public ContextCommunicator( int callSignCode, String[] callSigns, String beaconId ) {
		
		//this.contextBlock = blockState;
		this.minecraft = Minecraft.getMinecraft();
		this.callSignCode = callSignCode;
		this.callSigns = callSigns;
		this.beaconId = beaconId;
	}
	
	@Override
	public void drawScreen( int mouseX, int mouseY, float partialTicks) {
		
		try {
			if(initMouse) {
	    		//Mouse.setGrabbed(false);
	    		initMouse = true;
			}
			
			//Minecraft.getMinecraft().renderEngine.bindTexture(bg_texture);
			centerX = (width/2);
			centerY = (height/2);
			int fontheight = this.fontRendererObj.FONT_HEIGHT;
			//drawAtPosition(beaconId, .4f, .1f, "f4C430");
			int buttonWidth = (int)(width/2.25);
			
			int currentButtonTop = 0;
			for(int i=0; i<buttons.length; i++) {
				
				buttons[i].xPosition = centerX-(buttonWidth/2); 
				buttons[i].yPosition = currentButtonTop;
				buttons[i].width = buttonWidth;
				buttons[i].height = (int)(fontheight * 2);
				buttons[i].drawButton(minecraft, mouseX, mouseY);
				currentButtonTop+=height/buttons.length;
			}			
			super.drawScreen(mouseX, mouseY, partialTicks);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void initGui() {
		
		centerX = (width/2) ;
		centerY = (height/2);
		
		int fontheight = this.fontRendererObj.FONT_HEIGHT;		
		int buttonWidth = (int)(width/2.75);		
		int currentButtonTop = 0;
		for(int i=0; i<buttons.length; i++) {
			String text = buttonText[i];
			buttons[i] = this.addButton(new GuiButton(i, centerX-(buttonWidth/2), currentButtonTop,buttonWidth,(int)(fontheight *1.75), text));
			currentButtonTop+=height/buttons.length;
		}		

		super.initGui();
	}
	
	
	
	@Override
	public void actionPerformed( GuiButton button) throws IOException {
		
			
		//ContextCommunicatorUtilities.processButtonClick(button.id);
		
		//super.actionPerformed(button);
		
		//sentOnce = false;
	}
	
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException{
		
		if (mouseButton == 0)
        {
            for (int i = 0; i < this.buttonList.size(); ++i)
            {
                GuiButton guibutton = (GuiButton)this.buttonList.get(i);

                if (guibutton.mousePressed(this.mc, mouseX, mouseY))
                {
                    //net.minecraftforge.client.event.GuiScreenEvent.ActionPerformedEvent.Pre event = new net.minecraftforge.client.event.GuiScreenEvent.ActionPerformedEvent.Pre(this, guibutton, this.buttonList);
                    //if (net.minecraftforge.common.MinecraftForge.EVENT_BUS.post(event))
                    //    break;
                    //guibutton = event.getButton();
                    //this.selectedButton = guibutton;
                    guibutton.playPressSound(this.mc.getSoundHandler());
                    
                    //System.out.println("Processing button Click");
                    ContextCommunicatorUtilities.processButtonClick(guibutton.id, beaconId);                    
                    
                    //if (this.equals(this.mc.currentScreen))
                    //    net.minecraftforge.common.MinecraftForge.EVENT_BUS.post(new net.minecraftforge.client.event.GuiScreenEvent.ActionPerformedEvent.Post(this, event.getButton(), this.buttonList));
                }
            }
        }
		
	}
	
	@Override
	public boolean doesGuiPauseGame( )  {
		return false;
	}
	
	@Override
	 protected void keyTyped(char typedChar, int keyCode) throws IOException
    {
	
	   //System.out.println(typedChar);
	   //System.out.println(keyCode);
	   
	   // If they press spacebar, set the keycode to the escape keyCode and close the gui
	   if(typedChar == ' ') {
		   
		   this.mc.displayGuiScreen((GuiScreen)null);

           if (this.mc.currentScreen == null)
           {
               this.mc.setIngameFocus();
           }
	   }       
      
    }
	
	public void drawAtPosition(String value, float percentFromLeft, float percentFromTop, String colorCode) {
		
		//drawString(mc.fontRendererObj, value, (int)( widthScaledBackTo1 - ( widthScaledBackTo1 * xFloatPercentFromRight ) ), (int)( heightScaledBackTo1 - (heightScaledBackTo1*yFloatPercentFromTop) ), Integer.parseInt(colorCode, 16));
		drawString(mc.fontRendererObj, value, (int)(width*percentFromLeft), (int)(height*percentFromTop), Integer.parseInt(colorCode, 16));
	}
	
	
	
		
	

}
