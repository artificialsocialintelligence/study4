package com.asist.asistmod.block.bombs;

import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.Player;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.PlayerManager;
import com.asist.asistmod.tile_entity.BombBlockTileEntity;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockBombChained extends BlockBombBase {


    public BlockBombChained() {
        this.setHardness(10f);
        this.setTickRandomly(true);

    }

    @Override
    public void onBlockClicked(World world, BlockPos pos, EntityPlayer player)
    {
        if(!world.isRemote)
        {
        	BombBlockTileEntity te = (BombBlockTileEntity)world.getTileEntity(pos);            
            
            if( te.getChainedId().contentEquals("NONE") ){
            	
            	super.onBlockClicked(world, pos, player);
            	
            }
            else {
            	
                Player p = PlayerManager.getPlayerByName(player.getName());
                
                String pid = p.getParticipantId();                

                String id = te.getBombId();

                this.destroyBomb(world, pos, pid, id, BlockBombBase.BombOutcomeEnum.EXPLODE_CHAINED_ERROR);
            }
        }
    }
}
