package com.asist.asistmod.MissionSpecific.MissionA.MissionManagement;

import java.time.Clock;
import java.util.Set;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.MissionSpecific.MissionA.BombManagement.BombBookkeeper;
import com.asist.asistmod.MissionSpecific.MissionA.BombManagement.GhostBombCorrectorThread;
import com.asist.asistmod.MissionSpecific.MissionA.FireManagement.FireManager;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.Player;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.PlayerManager;
import com.asist.asistmod.MissionSpecific.MissionA.ScoreboardManagement.ScoreboardManager;
import com.asist.asistmod.MissionSpecific.MissionA.ShopManagement.ServerSideShopManager;
import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimer;
import com.asist.asistmod.MissionSpecific.MissionA.UtilityStructures.Position;
import com.asist.asistmod.datamodels.MissionStageTransition.MissionStageTransitionModel;
import com.asist.asistmod.datamodels.MissionState.MissionStateModel;
import com.asist.asistmod.mqtt.InternalMqttClient;
import com.asist.asistmod.network.NetworkHandler;
import com.asist.asistmod.network.messages.BombCountUpdatePacket;
import com.asist.asistmod.network.messages.GameOverPacket;
import com.asist.asistmod.network.messages.MissionStageTransitionPacket;
import com.asist.asistmod.network.messages.TeamScoreUpdatePacket;
import com.asist.asistmod.network.messages.shop.AssignCallSignPacket;
import com.asist.asistmod.network.messages.shop.TeamBudgetUpdatePacket;
import com.asist.asistmod.network.messages.shop.UpdateVoteCountPacket;

import net.minecraft.scoreboard.ScorePlayerTeam;
import net.minecraft.scoreboard.Scoreboard;
import net.minecraft.scoreboard.Team.EnumVisible;
import net.minecraft.util.text.TextFormatting;

public class MissionManager {
	
	public static boolean missionStarted = false;
	public static boolean missionStoppedSuccessfully = false;
	
	public static enum MissionStage{RECON_STAGE,SHOP_STAGE,FIELD_STAGE};
	public static enum StateChangeOutcome{
		MISSION_RUNNING_OK,
		MISSION_STOP_TIMER_END,
		MISSION_STOP_ALL_PLAYERS_FROZEN, 
		MISSION_STOP_ALL_BOMBS_REMOVED, 
		MISSION_STOP_MINECRAFT_DISCONNECT,
		MISSION_STOP_SERVER_CRASHED,
		MISSION_STOP_CLIENTMAP_DISCONNECT,
		MISSION_STOP_PLAYER_LEFT
	};
	
	public static MissionStage stage = null;
	
	public static int transitionsToShop = 0;
	public static int transitionsToField = 0;
	
	public static final Position SHOP_0 = new Position (-44,51,4); 
	public static final Position SHOP_1 = new Position (-45,51,6);
	public static final Position SHOP_2 = new Position (-44,51,8);
	
	public static Position FIELD_0 = new Position (26, 52, 149);
	public static Position FIELD_1 = new Position (24, 52, 149);
	public static Position FIELD_2 = new Position (22, 52, 149);
	
	public static Position RECON_0 = new Position (26, 52, 149);
	public static Position RECON_1 = new Position (24, 52, 149);
	public static Position RECON_2 = new Position (22, 52, 149);
	

	public static void  shouldMissionStart() {
		
		// The number of players is entirely determined by the incoming
		// callsign maps assignments. If there is 1 1 is required, if there is 2, 2's required ... etc.
		
		Set<String> namesFromTrialInfo = AsistMod.currentTrialInfo.callsigns.keySet();
		
		boolean shouldStart = true;
		for(String name : namesFromTrialInfo) {
			if( PlayerManager.getPlayerByName(name) == null){				
				shouldStart = false;
				break;
			};			
		}
		
		//System.out.println("Should Start is : " + shouldStart );
		
		if( shouldStart ) {
			StartMission();
			GhostBombCorrectorThread.startNewThread( AsistMod.instance.server.getEntityWorld() );
		}
	}	
	
	public static void StartMission() {		
		
		missionStarted = true;
		
		MissionTimer.init();	
		
		if(stage == null || (stage.compareTo(MissionStage.SHOP_STAGE)==0) ) {
			MissionTimer.pause();
		}
		
		setDisplayNamesViaCustomScoreboard();
		
		// This would make the game a lot harder ... but more fire means more crashes.
		FireManager.selectRandomFireSources(1);				
		ServerSideShopManager.teamBudget = AsistMod.modSettings.team_budget;
		// TODO: THIS MissionStageTransitionPacket is sent to the client EVERY TIME the stage transitions from RECON STAGE to SHOP_STAGE_ to FIELD_STAGE and vice verse
		// You can use this packet as the hook to launch the GUI clientside if you want ... or you can make one specifically for that purpose
		PlayerManager.players.forEach(player ->{			
			NetworkHandler.sendToClient(new AssignCallSignPacket(player.callSignCode,CallSigns.masterCallSignArray), player.getEntityPlayer());
			NetworkHandler.sendToClient(new TeamBudgetUpdatePacket(ServerSideShopManager.teamBudget), player.getEntityPlayer());			
			NetworkHandler.sendToClient(new UpdateVoteCountPacket(ServerSideShopManager.votesToGoToShop), player.getEntityPlayer());
			NetworkHandler.sendToClient(new TeamScoreUpdatePacket(ScoreboardManager.teamScore), player.getEntityPlayer());								
			NetworkHandler.sendToClient(new BombCountUpdatePacket(BombBookkeeper.bombsDefused,BombBookkeeper.bombsExploded,BombBookkeeper.bombsTotal-(BombBookkeeper.bombsDefused+BombBookkeeper.bombsExploded)), player.getEntityPlayer());
			NetworkHandler.sendToClient(new GameOverPacket( "" ), player.getEntityPlayer() );
			AsistMod.server.commandManager.executeCommand(AsistMod.server, "deop " + player.playerName);
		});				
		
		//transitionToShop();
		BombBookkeeper.updateAllPlayers();
		
		// this allows the player to start with tools if there is some standard set at startup
		ServerSideShopManager.giveAllPlayersAllPurchasedTools();
		
		MissionStateModel model = new MissionStateModel(AsistMod.currentTrialInfo.mission_name,"Start",StateChangeOutcome.MISSION_RUNNING_OK);
		model.publish();
		
		
		
		Thread startMissionThread = new Thread() {
			
			public void run(){
				try {
					System.out.println("Startin Mission in 30 seconds ...");
					Thread.sleep(30000);
					MissionManager.transitionToRecon();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}	
			}
		};
		
		startMissionThread.start();
		
		

	}
	
	public static void teleportCallsignToShop( int callSignCode, boolean saveFieldPos){
		BombBookkeeper.updateAllPlayers();
		Player p = PlayerManager.getPlayerByCallsign(CallSigns.masterCallSignArray[callSignCode]);
		if( p != null) {
			if( callSignCode == 0) {				
				if(saveFieldPos) { FIELD_0 = new Position(p.getEntityPlayer().getPosition());}
				
				p.teleport(SHOP_0, AsistMod.server);
			}
			else if( callSignCode == 1) {
				if(saveFieldPos) { FIELD_1 = new Position(p.getEntityPlayer().getPosition());}
				
				p.teleport(SHOP_1, AsistMod.server);
			}
			else if( callSignCode == 2) {
				if(saveFieldPos) { FIELD_2 = new Position(p.getEntityPlayer().getPosition());}				
				p.teleport(SHOP_2, AsistMod.server);
			}
		}		
	}
	
	public static void teleportCallsignToField( int callSignCode){
		BombBookkeeper.updateAllPlayers();
		Player p = PlayerManager.getPlayerByCallsign(CallSigns.masterCallSignArray[callSignCode]);
		if( p != null) {
			if( callSignCode == 0) {			
				p.teleport(FIELD_0, AsistMod.server);
			}
			else if( callSignCode == 1) {
				p.teleport(FIELD_1, AsistMod.server);
			}
			else if( callSignCode == 2) {
				p.teleport(FIELD_2, AsistMod.server);
			}
		}		
	}
	
	public static void teleportCallsignToRecon( int callSignCode){
		BombBookkeeper.updateAllPlayers();
		Player p = PlayerManager.getPlayerByCallsign(CallSigns.masterCallSignArray[callSignCode]);
		if( p != null) {
			if( callSignCode == 0) {			
				p.teleport(RECON_0, AsistMod.server);
			}
			else if( callSignCode == 1) {
				p.teleport(RECON_1, AsistMod.server);
			}
			else if( callSignCode == 2) {
				p.teleport(RECON_2, AsistMod.server);
			}
		}		
	}
	
	public static void teleportCallsignsToShop(){
		int i = 0;
		for( String cs : CallSigns.masterCallSignArray) {
			teleportCallsignToShop(i,true);
			i++;
		}
	}
	
	public static void teleportCallsignsToField(){
		int i = 0;
		for( String cs : CallSigns.masterCallSignArray) {
			teleportCallsignToField(i);
			i++;
		}
	}
	
	public static void teleportCallsignsToRecon(){
		int i = 0;
		for( String cs : CallSigns.masterCallSignArray) {
			teleportCallsignToRecon(i);
			i++;
		}
	}
	
	
	
	public static void transitionToShop(){
		// UPDATE GUI ON ALL CLIENTS WITH THE NEW TIME
		stage = MissionStage.SHOP_STAGE;
		ServerSideShopManager.publishInventoryState();
		
		teleportCallsignsToShop();
		PlayerManager.players.forEach(player ->{
			NetworkHandler.sendToClient(new MissionStageTransitionPacket(stage), player.getEntityPlayer());
			
		});		
		transitionsToShop++;
		//MissionTimer.startStageTimer();
		MissionStageTransitionModel st_model = new MissionStageTransitionModel();
		st_model.data.team_budget = ServerSideShopManager.teamBudget;
		InternalMqttClient.publish(st_model.toJsonString(), "observations/events/stage_transition");		
		MissionTimer.pause();		
	}
	
	public static void transitionToField() {
		stage = MissionStage.FIELD_STAGE;
		teleportCallsignsToField();		
		PlayerManager.players.forEach(player ->{
			NetworkHandler.sendToClient(new MissionStageTransitionPacket(stage), player.getEntityPlayer());
		});
		transitionsToField++;
		MissionStageTransitionModel st_model = new MissionStageTransitionModel();
		InternalMqttClient.publish(st_model.toJsonString(), "observations/events/stage_transition");
		MissionTimer.unPause();
		ServerSideShopManager.publishInventoryState();
	}
	
	public static void transitionToRecon() {
		stage = MissionStage.RECON_STAGE;
		teleportCallsignsToRecon();
		PlayerManager.players.forEach(player ->{
			NetworkHandler.sendToClient(new MissionStageTransitionPacket(stage), player.getEntityPlayer());
		});
		transitionsToField++;		
		MissionStageTransitionModel st_model = new MissionStageTransitionModel();
		InternalMqttClient.publish(st_model.toJsonString(), "observations/events/stage_transition");
		MissionTimer.unPause();
		ServerSideShopManager.publishInventoryState();
		ServerSideShopManager.resetPlayerReadyToProceedMaps();
	}
	
	public static void setDisplayNamesViaCustomScoreboard() {
		
		//CustomScoreboard scoreboard = new CustomScoreboard(AsistMod.server.getEntityWorld().getScoreboard());
		
		Scoreboard scoreboard = AsistMod.server.getEntityWorld().getScoreboard();
		
		ScorePlayerTeam scorePlayerTeam = scoreboard.getTeam("Alpha");
		if(scorePlayerTeam != null){
			scoreboard.removeTeam(scorePlayerTeam);
			scoreboard.broadcastTeamRemove(scorePlayerTeam);
		}
		
		scorePlayerTeam = scoreboard.getTeam("Bravo");
		if(scorePlayerTeam != null){
			scoreboard.removeTeam(scorePlayerTeam);
			scoreboard.broadcastTeamRemove(scorePlayerTeam);
		}
		
		scorePlayerTeam = scoreboard.getTeam("Delta");
		if(scorePlayerTeam != null){
			scoreboard.removeTeam(scorePlayerTeam);
			scoreboard.broadcastTeamRemove(scorePlayerTeam);
		}
		
		
		PlayerManager.players.forEach(player ->{
			ScorePlayerTeam  spt = scoreboard.createTeam(player.callSign);
			spt.setNamePrefix(player.callSign+":");
			//spt.setNameSuffix("---- : ----" + player.callSign);
			scoreboard.addPlayerToTeam(player.getName(),player.getCallSign());			 
			spt.setNameTagVisibility(EnumVisible.ALWAYS);
			//scoreboard.broadcastTeamCreated(spt);
			//scoreboard.broadcastTeamInfoUpdate(spt);			
		});
		
		scoreboard.getTeams().forEach( (t) ->{
			//System.out.println( t.getTeamName());
			//System.out.print( t.getRegisteredName() + " -> | " );
			t.getMembershipCollection().forEach( m ->{
				//System.out.println(ScorePlayerTeam.formatPlayerName(t, m));
			});
		});		
	}
	
	public static void endMission( StateChangeOutcome stateChangeOutcome, boolean crash) {
		
		teleportCallsignsToShop();
		System.out.println("Running Mission End Procedure");
		MissionStateModel model = new MissionStateModel(AsistMod.currentTrialInfo.mission_name,"Stop",stateChangeOutcome);
		System.out.println("Running Mission End Procedure1");
		model.publish();
		System.out.println("Running Mission End Procedure2");
		MissionTimer.isInitialized = false;
		missionStoppedSuccessfully = !crash;
		PlayerManager.players.forEach(player ->{			
			NetworkHandler.sendToClient(new GameOverPacket(stateChangeOutcome.toString()), player.getEntityPlayer());
		});		
		
		Thread stopThread = new Thread() {			
			
			public void run() {				
				
				long elapsedMilliseconds = 0;
				long startTime = Clock.systemDefaultZone().millis();
				// WAIT 5 SECONDS TO VERIFY BLOCK LOADING CRASH DID NOT OCCUR
				while (elapsedMilliseconds <= 10000 ) {							
					try { 
						Thread.sleep(1000); 
					} 
					catch (InterruptedException e) {
						e.printStackTrace();
					}						
					elapsedMilliseconds = Clock.systemDefaultZone().millis() - startTime;
					System.out.println("Stopping Server in : " + (10000 - elapsedMilliseconds) + " milliseconds.");
				}
				AsistMod.server.commandManager.executeCommand(AsistMod.server, "stop");
			}			
		}; // END THREAD DEFINITION
		
		stopThread.start();
		
		/*
		FireManager.firePlacingThread.joinThread();		
		if( !MissionTimer.inWindDown) {
			//System.out.println("Triggering endMission function.");
			//System.out.println("StateChangeOutcome: " + stateChangeOutcome.name());
			
			MissionTimer.startWindDown();			
		}
		*/		
	}
	
	
}
