package com.asist.asistmod.item;


import javax.annotation.Nullable;

import com.asist.asistmod.MissionSpecific.MissionA.GuiManagement.ASISTCreativeTabs.ASISTModTabs;

import net.minecraft.block.Block;
import net.minecraft.block.BlockLiquid;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.stats.StatList;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.RayTraceResult.Type;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.event.ForgeEventFactory;
import net.minecraftforge.fluids.capability.wrappers.FluidBucketWrapper;

public class ItemModBucket extends Item {
    private final Block containedBlock;

    public ItemModBucket(Block block) {
        this.maxStackSize = 1;
        this.containedBlock = block;
        this.setCreativeTab(ASISTModTabs.ASISTItems);
    }

    public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer player, EnumHand hand) {
        boolean flag = this.containedBlock == Blocks.AIR;
        ItemStack itemstack = player.getHeldItem(hand);
        RayTraceResult raytraceresult = this.rayTrace(worldIn, player, flag);
        ActionResult<ItemStack> ret = ForgeEventFactory.onBucketUse(player, worldIn, itemstack, raytraceresult);
        if (ret != null) {
            return ret;
        } else if (raytraceresult == null) {
            return new ActionResult(EnumActionResult.PASS, itemstack);
        } else if (raytraceresult.typeOfHit != Type.BLOCK) {
            return new ActionResult(EnumActionResult.PASS, itemstack);
        } else {
            BlockPos blockpos = raytraceresult.getBlockPos();
            if (!worldIn.isBlockModifiable(player, blockpos)) {
                return new ActionResult(EnumActionResult.FAIL, itemstack);
            } else if (flag) {
                if (!player.canPlayerEdit(blockpos.offset(raytraceresult.sideHit), raytraceresult.sideHit, itemstack)) {
                    return new ActionResult(EnumActionResult.FAIL, itemstack);
                } else {
                    IBlockState iblockstate = worldIn.getBlockState(blockpos);
                    Material material = iblockstate.getMaterial();
                    if (material == Material.WATER && (Integer)iblockstate.getValue(BlockLiquid.LEVEL) == 0) {
                        worldIn.setBlockState(blockpos, Blocks.AIR.getDefaultState(), 11);
                        player.addStat(StatList.getObjectUseStats(this));
                        player.playSound(SoundEvents.ITEM_BUCKET_FILL, 1.0F, 1.0F);
                        return new ActionResult(EnumActionResult.SUCCESS, this.fillBucket(itemstack, player, _Items.WATER_BUCKET));
                    }
                        /*
                     else if (material == Material.LAVA && (Integer)iblockstate.getValue(BlockLiquid.LEVEL) == 0) {
                        player.playSound(SoundEvents.ITEM_BUCKET_FILL_LAVA, 1.0F, 1.0F);
                        worldIn.setBlockState(blockpos, Blocks.AIR.getDefaultState(), 11);
                        player.addStat(StatList.getObjectUseStats(this));
                        return new ActionResult(EnumActionResult.SUCCESS, this.fillBucket(itemstack, player, Items.LAVA_BUCKET));
                    }
                    */else {

                        return new ActionResult(EnumActionResult.FAIL, itemstack);
                    }
                }
            } else {
                boolean flag1 = worldIn.getBlockState(blockpos).getBlock().isReplaceable(worldIn, blockpos);
                BlockPos blockpos1 = flag1 && raytraceresult.sideHit == EnumFacing.UP ? blockpos : blockpos.offset(raytraceresult.sideHit);
                if (!player.canPlayerEdit(blockpos1, raytraceresult.sideHit, itemstack)) {
                    return new ActionResult(EnumActionResult.FAIL, itemstack);
                } else if (this.tryPlaceContainedLiquid(player, worldIn, blockpos1)) {
                    player.addStat(StatList.getObjectUseStats(this));
                    return !player.capabilities.isCreativeMode ? new ActionResult(EnumActionResult.SUCCESS, new ItemStack(_Items.BUCKET)) : new ActionResult(EnumActionResult.SUCCESS, itemstack);
                } else {
                    return new ActionResult(EnumActionResult.FAIL, itemstack);
                }
            }
        }
    }

    private ItemStack fillBucket(ItemStack itemStack, EntityPlayer player, Item item) {
        if (player.capabilities.isCreativeMode) {
            return itemStack;
        } else {
            itemStack.shrink(1);
            if (itemStack.isEmpty()) {
                return new ItemStack(item);
            } else {
                if (!player.inventory.addItemStackToInventory(new ItemStack(item))) {
                    player.dropItem(new ItemStack(item), false);
                }

                return itemStack;
            }
        }
    }

    public boolean tryPlaceContainedLiquid(@Nullable EntityPlayer player, World world, BlockPos pos) {
        if (this.containedBlock == Blocks.AIR) {
            return false;
        } else {
            IBlockState iblockstate = world.getBlockState(pos);
            Material material = iblockstate.getMaterial();
            boolean flag = !material.isSolid();
            boolean flag1 = iblockstate.getBlock().isReplaceable(world, pos);
            if (!world.isAirBlock(pos) && !flag && !flag1) {
                return false;
            } else {
                if (world.provider.doesWaterVaporize() && this.containedBlock == Blocks.FLOWING_WATER) {
                    int l = pos.getX();
                    int i = pos.getY();
                    int j = pos.getZ();
                    world.playSound(player, pos, SoundEvents.BLOCK_FIRE_EXTINGUISH, SoundCategory.BLOCKS, 0.5F, 2.6F + (world.rand.nextFloat() - world.rand.nextFloat()) * 0.8F);

                    for(int k = 0; k < 8; ++k) {
                        world.spawnParticle(EnumParticleTypes.SMOKE_LARGE, (double)l + Math.random(), (double)i + Math.random(), (double)j + Math.random(), 0.0, 0.0, 0.0, new int[0]);
                    }
                } else {
                    if (!world.isRemote && (flag || flag1) && !material.isLiquid()) {
                        world.destroyBlock(pos, true);
                    }

                    SoundEvent soundevent = this.containedBlock == Blocks.FLOWING_LAVA ? SoundEvents.ITEM_BUCKET_EMPTY_LAVA : SoundEvents.ITEM_BUCKET_EMPTY;
                    world.playSound(player, pos, soundevent, SoundCategory.BLOCKS, 1.0F, 1.0F);
                    world.setBlockState(pos, this.containedBlock.getDefaultState(), 11);
                }

                return true;
            }
        }
    }

    public ICapabilityProvider initCapabilities(ItemStack p_initCapabilities_1_, @Nullable NBTTagCompound p_initCapabilities_2_) {
        return (ICapabilityProvider)(this.getClass() == ItemModBucket.class ? new FluidBucketWrapper(p_initCapabilities_1_) : super.initCapabilities(p_initCapabilities_1_, p_initCapabilities_2_));
    }
}
