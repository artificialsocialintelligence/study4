package com.asist.asistmod.datamodels.CommunicationEnvironment;

import java.util.Map;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.datamodels.Header.HeaderModel;
import com.asist.asistmod.datamodels.Msg.MsgModel;
import com.asist.asistmod.mqtt.MessageTopic;

public class CommunicationEnvironmentModel {
	
	public static int message_id = 0;
	
	public HeaderModel header = new HeaderModel();
	
	public MsgModel msg = new MsgModel(getMessageTopicEnum().getEventName(),getMessageTopicEnum().getVersion());
	
	public CommunicationEnvironmentData data = new CommunicationEnvironmentData();		
		
	public CommunicationEnvironmentModel( String senderId, String senderType, String[] recipients, String message,int x,int y, int z, Map<String, String> addInfo ) {		
		
				this.data.message_id = "COMM_ENV"+(message_id++);
				this.data.sender_id = senderId;
				this.data.sender_type = senderType;
				this.data.recipients = recipients;
				this.data.message = message;
				this.data.sender_x = x;
				this.data.sender_y = y;
				this.data.sender_z = z;
				this.data.additional_info = addInfo;
	}	
	
	public String toJsonString() { 
		
		return AsistMod.gson.toJson(this);		
	}
	
	public MessageTopic getMessageTopicEnum() {
		return MessageTopic.COMMUNICATION_ENVIRONMENT;
	}
	
	public String getTopic() {
		return getMessageTopicEnum().getTopic();
	}
	
	public String getEventName() {
		return getMessageTopicEnum().getEventName();
	}
	
	public String getVersion() {
		return getMessageTopicEnum().getVersion();
	}


}
