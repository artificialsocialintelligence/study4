package com.asist.asistmod.datamodels.MissionLevelProgress;

import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimer;

public class MissionLevelProgressDataModel {
	
	public String mission_timer = MissionTimer.getMissionTimeString();
	public long elapsed_milliseconds = MissionTimer.getElapsedMillisecondsGlobal();
	public int level_unlocked;
			
}
