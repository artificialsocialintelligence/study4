package com.asist.asistmod.MissionSpecific.MissionA.ItemManagement;

public enum ItemInteractionMode {	
	
		LEFT_MOUSE,
		RIGHT_MOUSE,
		LEFT_RIGHT_MOUSE,
		NONE

}
