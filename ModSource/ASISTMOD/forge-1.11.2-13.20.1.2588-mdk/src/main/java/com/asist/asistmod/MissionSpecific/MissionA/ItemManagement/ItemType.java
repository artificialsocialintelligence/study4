package com.asist.asistmod.MissionSpecific.MissionA.ItemManagement;

import java.util.Map;
import com.asist.asistmod.MissionCommon.Utilities.AttributeManagement.AttributeSetCreator;
import com.asist.asistmod.MissionCommon.Utilities.AttributeManagement.AttributedObjectManager;
import com.asist.asistmod.MissionCommon.Utilities.AttributeManagement.AttributedObjectType;

import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.ForgeRegistries;



public enum ItemType {

	WIRECUTTERS_RED			(0, "WIRECUTTERS_RED", ItemInteractionMode.LEFT_RIGHT_MOUSE,
								new ItemStack(ForgeRegistries.ITEMS.getValue(new ResourceLocation("asistmod","item_wirecutters_red"))),
								"asistmod:item_wirecutters_red", 1),
	
	WIRECUTTERS_GREEN		(1, "WIRECUTTERS_GREEN", ItemInteractionMode.LEFT_RIGHT_MOUSE,
								new ItemStack(ForgeRegistries.ITEMS.getValue(new ResourceLocation("asistmod","item_wirecutters_green"))),
								"asistmod:item_wirecutters_green", 1),
	
	WIRECUTTERS_BLUE		(2, "WIRECUTTERS_BLUE", ItemInteractionMode.LEFT_RIGHT_MOUSE,
								new ItemStack(ForgeRegistries.ITEMS.getValue(new ResourceLocation("asistmod","item_wirecutters_blue"))),
								"asistmod:item_wirecutters_blue", 1),
	
	BOMB_BEACON				(3, "BOMB_BEACON", ItemInteractionMode.LEFT_RIGHT_MOUSE,
								new ItemStack(ForgeRegistries.ITEMS.getValue(new ResourceLocation("asistmod","block_beacon_bomb"))),
								"asistmod:block_beacon_bomb", 1),
	
	HAZARD_BEACON			(4, "HAZARD_BEACON", ItemInteractionMode.LEFT_RIGHT_MOUSE,
								new ItemStack(ForgeRegistries.ITEMS.getValue(new ResourceLocation("asistmod","block_beacon_hazard"))),
								"asistmod:block_beacon_hazard", 1),
	
	BOMB_SENSOR				(5, "BOMB_SENSOR", ItemInteractionMode.RIGHT_MOUSE,
								new ItemStack(ForgeRegistries.ITEMS.getValue(new ResourceLocation("asistmod","item_bomb_sensor"))),
								"asistmod:item_bomb_sensor", 1),
	
	FIRE_EXTINGUISHER		(6, "FIRE_EXTINGUISHER", ItemInteractionMode.RIGHT_MOUSE,
								new ItemStack(ForgeRegistries.ITEMS.getValue(new ResourceLocation("asistmod","item_fire_extinguisher"))),
								"asistmod:item_fire_extinguisher", 1),

	BOMB_DISPOSER			(7, "BOMB_DISPOSER", ItemInteractionMode.RIGHT_MOUSE,
								new ItemStack(ForgeRegistries.ITEMS.getValue(new ResourceLocation("asistmod","item_bomb_disposer"))),
								"asistmod:item_bomb_disposer", 1),
	
	BOMB_PPE				(8, "BOMB_PPE", ItemInteractionMode.RIGHT_MOUSE,
								new ItemStack(ForgeRegistries.ITEMS.getValue(new ResourceLocation("asistmod","item_bombppe_helmet"))),
								"asistmod:item_bombppe_helmet", 1),	
	
	NULL					(9, "NULL", ItemInteractionMode.NONE, new ItemStack(Blocks.AIR), " minecraft:air")
	;
	
	private final int code;
	private final String name;
	public final ItemInteractionMode interactionMode;
	private final ItemStack itemStack;
	private final String registryName;
	private final int commandDefaultQuantity;

	private final int commandDefaultData;

	private final String commandAdditionalComponents;

	//For the commandText it is split into; modname:itemName (int)quantity (int)data
	// and is used during the give command. More information can be found here
	// https://minecraft.fandom.com/wiki/Commands/give
	// give <player: target> <itemName: Item> [amount: int] [data: int] [components: json]


	ItemType(int code, String name,  ItemInteractionMode mode, ItemStack item, String commandItemName) {
		this.code = code;
		this.name = name;
		this.interactionMode = mode;
		this.itemStack = item;
		this.registryName = commandItemName;
		this.commandDefaultQuantity = 1;
		this.commandDefaultData = 0;
		this.commandAdditionalComponents = "";
	}

	ItemType(int code, String name,  ItemInteractionMode mode, ItemStack item, String commandItemName, int defaultQuantity) {
		this.code = code;
		this.name = name;
		this.interactionMode = mode;
		this.itemStack = item;
		this.registryName = commandItemName;
		this.commandDefaultQuantity = defaultQuantity;
		this.commandDefaultData = 0;
		this.commandAdditionalComponents = "";
	}

	ItemType(int code, String name,  ItemInteractionMode mode, ItemStack item, String commandItemName, int defaultQuantity , int defaultData) {
		this.code = code;
		this.name = name;
		this.interactionMode = mode;
		this.itemStack = item;
		this.registryName = commandItemName;
		this.commandDefaultQuantity = defaultQuantity;
		this.commandDefaultData = defaultData;
		this.commandAdditionalComponents = "";
	}

	ItemType(int code, String name,  ItemInteractionMode mode, ItemStack item, String commandItemName,
			 int defaultQuantity , int defaultData, String[] blocksThisItemCanDestroy) {
		this.code = code;
		this.name = name;
		this.interactionMode = mode;
		this.itemStack = item;
		this.registryName = commandItemName;
		this.commandDefaultQuantity = defaultQuantity;
		this.commandDefaultData = defaultData;


		StringBuilder temp = new StringBuilder("{CanDestroy:["); //{CanDestroy:[

		// "minecraft:stone","minecraft:stonebrick"
		for(int i = 0; i < blocksThisItemCanDestroy.length; i++)
		{
			temp.append(blocksThisItemCanDestroy[i]);
			if(i != blocksThisItemCanDestroy.length - 1)
			{
				temp.append(",");
			}
		}

		temp.append("]}"); //]}

		this.commandAdditionalComponents = temp.toString();
	}


	public String getRegistryName() {
		return this.registryName;
	}
	
	public int getSlotIndex() {
		return this.code;
	}

	public static String MakeItemCommandText(ItemType item)
	{
		return item.registryName + " " + item.commandDefaultQuantity + " " + item.commandDefaultData;
	}

	public static String MakeItemCommandText(ItemType item, int quantity, int data)
	{
		return item.registryName + " " + quantity + " " + data;
	}

	public static String MakeItemCommandText(ItemType item, int quantity)
	{
		return item.registryName + " " + quantity + " " + item.commandDefaultData;
	}

	public static String MakeItemCommandText(ItemType item, int quantity, int data, String[] blocksThisItemCanDestroy)
	{
		StringBuilder temp = new StringBuilder("{CanDestroy:["); //{CanDestroy:[

		// "minecraft:stone","minecraft:stonebrick"
		for(int i = 0; i < blocksThisItemCanDestroy.length; i++)
		{
			temp.append(blocksThisItemCanDestroy[i]);
			if(i != blocksThisItemCanDestroy.length - 1)
			{
				temp.append(",");
			}
		}

		temp.append("]}"); //]}


		return item.registryName + " " + quantity + " " + data + " " + temp;
	}
	
	public int getItemCode() {
		return this.code;
	}
	
	public String getName() {
		return this.name;
	}
	
	public ItemStack getItemStack( ) {
		

		return this.itemStack;
	}


	public ItemStack getItemStackCopyWithNBTAndAmount(String name, int amount) {
		
		String lowerCaseName = name.toLowerCase();
		NBTTagCompound nbt = this.itemStack.hasTagCompound() ? this.itemStack.getTagCompound() : new NBTTagCompound();
		String itemStackId = "ITEMSTACK"+ AttributedObjectManager.attributedItemStacks.size();
		AttributedObjectManager.initAttributedItemStack(itemStackId );
		nbt.setTag("item_stack_id",new NBTTagString(itemStackId));
				
		if( lowerCaseName.contains("wirecutter") || lowerCaseName.contains("disposer") ) {
			NBTTagList canDestroyTag = new NBTTagList();
			canDestroyTag.appendTag(new NBTTagString("asistmod:block_bomb_standard"));
			canDestroyTag.appendTag(new NBTTagString("asistmod:block_bomb_fire"));
			canDestroyTag.appendTag(new NBTTagString("asistmod:block_bomb_chained"));
			nbt.setTag("CanDestroy",canDestroyTag);			
			
			////System.out.println("ModItemsNBTAttempt : " + nbt.getTag("CanDestroy").toString());			
		}
		else if(lowerCaseName.contains("beacon")) {
			if(lowerCaseName.contains("bomb")) {
				NBTTagList canPlaceOnTag = new NBTTagList();
				
				canPlaceOnTag.appendTag(new NBTTagString("asistmod:block_bomb_standard"));
				canPlaceOnTag.appendTag(new NBTTagString("asistmod:block_bomb_fire"));
				canPlaceOnTag.appendTag(new NBTTagString("asistmod:block_bomb_chained"));
				nbt.setTag("CanPlaceOn",canPlaceOnTag);
				NBTTagList canDestroyTag = new NBTTagList();
				canDestroyTag.appendTag(new NBTTagString("asistmod:block_beacon_rally"));
				canDestroyTag.appendTag(new NBTTagString("asistmod:block_beacon_hazard"));
				canDestroyTag.appendTag(new NBTTagString("asistmod:block_beacon_bomb"));
				nbt.setTag("CanDestroy",canDestroyTag);
			}
			else {
				
				NBTTagList canPlaceOnTag = new NBTTagList();
				
				canPlaceOnTag.appendTag(new NBTTagString("minecraft:dirt"));
				canPlaceOnTag.appendTag(new NBTTagString("minecraft:grass"));
				canPlaceOnTag.appendTag(new NBTTagString("minecraft:stone"));
				canPlaceOnTag.appendTag(new NBTTagString("minecraft:stonebrick"));
				canPlaceOnTag.appendTag(new NBTTagString("minecraft:sand"));
				canPlaceOnTag.appendTag(new NBTTagString("minecraft:log"));
				canPlaceOnTag.appendTag(new NBTTagString("minecraft:end_bricks"));
				canPlaceOnTag.appendTag(new NBTTagString("minecraft:quartz_block"));
				canPlaceOnTag.appendTag(new NBTTagString("minecraft:grass_path"));				
				
				nbt.setTag("CanPlaceOn",canPlaceOnTag);
				NBTTagList canDestroyTag = new NBTTagList();
				canDestroyTag.appendTag(new NBTTagString("asistmod:block_beacon_rally"));
				canDestroyTag.appendTag(new NBTTagString("asistmod:block_beacon_hazard"));
				canDestroyTag.appendTag(new NBTTagString("asistmod:block_beacon_bomb"));
				nbt.setTag("CanDestroy",canDestroyTag);
				
			}
			////System.out.println("ModItemsNBTAttempt : " + nbt.getTag("CanPlaceOn").toString());
		}
		else if(lowerCaseName.contains("extinguisher"))
		{
			NBTTagList canDestroyTag = new NBTTagList();
			canDestroyTag.appendTag(new NBTTagString("minecraft:grass"));
			canDestroyTag.appendTag(new NBTTagString("minecraft:stone"));
			canDestroyTag.appendTag(new NBTTagString("minecraft:sand"));
			canDestroyTag.appendTag(new NBTTagString("minecraft:logs"));
			canDestroyTag.appendTag(new NBTTagString("minecraft:end_bricks"));
			canDestroyTag.appendTag(new NBTTagString("minecraft:quartz_block"));
			canDestroyTag.appendTag(new NBTTagString("minecraft:grass_path"));
			nbt.setTag("CanDestroy", canDestroyTag);
		}
		
		this.itemStack.setTagCompound(nbt);
		this.itemStack.setCount(amount);
		
		for(int i=1; i<=amount; i++) {
			
			String itemId = AttributedObjectType.GENERIC_ITEM.getPrefix()+ i;
			Map<String, String> attributeSet;
			if(lowerCaseName.compareTo("bomb_beacon")==0) {
				attributeSet = AttributeSetCreator.createBombBeaconAttributeSet("", 1);
			}
			else if(lowerCaseName.compareTo("hazard_beacon")==0) {
				attributeSet = AttributeSetCreator.createHazardBeaconAttributeSet("", 1);
			}
			else {
				attributeSet = AttributeSetCreator.createGenericItemAttributeSet(1);
			}			
			AttributedObjectManager.initAttributedItem(itemStackId,itemId, attributeSet);
			
		}		
		
		return this.itemStack.copy();
	}
	

	
	public Item getItem() {
		return getItemStack().getItem();
	}
	
	public String makeCommandText(int quantity) {
		return " "+MakeItemCommandText(this, quantity);
	}

	public String makeCommandText(int quantity, int data) {
		return " "+MakeItemCommandText(this, quantity, data);
	}

	public String makeCommandText(int quantity, int data, String[] blocksThisItemCanDestroy) {
		return " "+MakeItemCommandText(this, quantity, data, blocksThisItemCanDestroy);
	}

	public String getCommandText() {
		return " "+MakeItemCommandText(this, this.commandDefaultQuantity);
	}
	
	public boolean notNull() {
		return !this.name.equals("null");
	}
	
	public static ItemType getItemBySlotIndex(int slotIndex) {
		
		for (ItemType itemType : ItemType.values()) {
	          if (itemType.code == slotIndex) 
	        	  return itemType;
	    }
		
		return ItemType.NULL;
	}
	
	public static ItemType getItemTypeByRegistryName(String registryName) {		
		for (ItemType itemType : ItemType.values()) {			
	          if (itemType.registryName.contentEquals(registryName)) 
	        	  return itemType;
	    }
		//System.out.println("Itemtype "+ registryName +  " not matched with any known item, returning ItemType.NULL");
		return ItemType.NULL;
	}
	
	public static ItemType getItemTypeByEnumName(String enumName) {		
		for (ItemType itemType : ItemType.values()) {			
	          if (itemType.getName().contentEquals(enumName)) 
	        	  return itemType;
	    }
		//System.out.println("Itemtype " + enumName + " not matched with any known item, returning ItemType.NULL");
		return ItemType.NULL;
	}
	
	public static String generateItemIdForTopOfCurrentHeldStack(ItemStack stack) {
		
		String itemStackId = stack.getTagCompound().getTag("item_stack_id").toString();
		////System.out.println(itemStackId);
		itemStackId = itemStackId.replaceAll("\"", "");
		////System.out.println(itemStackId);
		String itemId = AttributedObjectType.GENERIC_ITEM.getPrefix()+Integer.toString(stack.getCount());
		String idForMessage = itemStackId+'_'+itemId;
		//System.out.println("ITEM ID FOR MESSAGE : "+idForMessage);
		return idForMessage;
	}
}
