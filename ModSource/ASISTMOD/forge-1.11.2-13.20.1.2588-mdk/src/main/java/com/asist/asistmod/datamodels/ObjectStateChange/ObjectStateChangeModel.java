package com.asist.asistmod.datamodels.ObjectStateChange;

import java.util.Map;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.datamodels.Header.HeaderModel;
import com.asist.asistmod.datamodels.Msg.MsgModel;
import com.asist.asistmod.mqtt.MessageTopic;

public class ObjectStateChangeModel {	
	
	public HeaderModel header = new HeaderModel();
		
	public MsgModel msg = new MsgModel(getMessageTopicEnum().getEventName(),getMessageTopicEnum().getVersion());
	
	public ObjectStateChangeData data = new ObjectStateChangeData(true);
		
		
	public ObjectStateChangeModel( String id, String type, String triggeringEntity, int x, int y, int z, Map<String,String[]> changedAttributes, Map<String,String> currAttributes) {
		
		data.id = id;
		data.type = type;
		data.triggering_entity = triggeringEntity;
		data.x = x;
		data.y = y;
		data.z = z;		
		data.changedAttributes = changedAttributes;
		data.currAttributes = currAttributes;
				
	}	
	
	public String toJsonString() { 
		
		return AsistMod.gson.toJson(this);		
	}
	
	public MessageTopic getMessageTopicEnum() {
		return MessageTopic.OBJECT_STATE_CHANGE;
	}
	
	public String getTopic() {
		return getMessageTopicEnum().getTopic();
	}
	
	public String getEventName() {
		return getMessageTopicEnum().getEventName();
	}
	
	public String getVersion() {
		return getMessageTopicEnum().getVersion();
	}

}
