package com.asist.asistmod.datamodels.MissionState;

import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimer;

public class MissionStateDataModel {
	
	public String mission_timer = MissionTimer.getMissionTimeString();
	public long elapsed_milliseconds = MissionTimer.getElapsedMillisecondsGlobal();
	public String mission;
	public String mission_state;
	public String state_change_outcome;
		
}
