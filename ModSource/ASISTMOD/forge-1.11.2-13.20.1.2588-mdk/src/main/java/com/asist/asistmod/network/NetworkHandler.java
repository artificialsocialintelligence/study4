package com.asist.asistmod.network;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.network.messages.*;
import com.asist.asistmod.network.messages.shop.*;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;

public class NetworkHandler {
	
	private static SimpleNetworkWrapper INSTANCE;
	

	 
	public static void init(  ) {		
		
		INSTANCE = NetworkRegistry.INSTANCE.newSimpleChannel(AsistMod.MODID);	
		
		INSTANCE.registerMessage(MissionTimerPacket.class, MissionTimerPacket.class, 1, Side.CLIENT);	
		
		INSTANCE.registerMessage(MissionStageTransitionPacket.class, MissionStageTransitionPacket.class, 2, Side.CLIENT);
		
		INSTANCE.registerMessage(BombCountUpdatePacket.class, BombCountUpdatePacket.class, 3, Side.CLIENT);		
		//SHOP MESSAGES		
		INSTANCE.registerMessage(DisplayShopPacket.class, DisplayShopPacket.class, 4, Side.CLIENT);
		
		// CLIENT REQUESTS TO PROCEED
		INSTANCE.registerMessage(VoteToGoToShopPacket.class, VoteToGoToShopPacket.class, 5, Side.SERVER);
		// SERVER ALLOWS TO PROCEED
		INSTANCE.registerMessage(UpdateVoteCountPacket.class, UpdateVoteCountPacket.class, 6, Side.CLIENT);
		
		INSTANCE.registerMessage(TeamBudgetUpdatePacket.class, TeamBudgetUpdatePacket.class, 9, Side.CLIENT);
		
		INSTANCE.registerMessage(OpenBeaconOptionsPacket.class, OpenBeaconOptionsPacket.class, 12, Side.CLIENT);
		
		INSTANCE.registerMessage(BeaconChatPacket.class, BeaconChatPacket.class, 13, Side.SERVER);
		
		INSTANCE.registerMessage(AssignCallSignPacket.class,AssignCallSignPacket.class, 14, Side.CLIENT);	
		
		INSTANCE.registerMessage(AgentInterventionChatPacket.class,AgentInterventionChatPacket.class, 15, Side.CLIENT);
		
		INSTANCE.registerMessage(AgentInterventionResponsePacket.class,AgentInterventionResponsePacket.class, 16, Side.SERVER);

		INSTANCE.registerMessage(TeamScoreUpdatePacket.class, TeamScoreUpdatePacket.class, 17, Side.CLIENT);
		
		INSTANCE.registerMessage(GameOverPacket.class, GameOverPacket.class, 18, Side.CLIENT);

	}	
	
	public static void sendToServer(IMessage message) {
		INSTANCE.sendToServer(message);		
	}
	
	public static void sendToClient(IMessage message, EntityPlayer player) {		
		
		if (player instanceof EntityPlayerMP) {
			INSTANCE.sendTo(message, (EntityPlayerMP) player);
		}
	}
}
