package com.asist.asistmod.datamodels.PlayerState;

import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimer;

public class PlayerStateData{
	
	//String playername = null; --> PII
	public String mission_timer = MissionTimer.getMissionTimeString();
	public long elapsed_milliseconds_global = MissionTimer.getElapsedMillisecondsGlobal();
	public long elapsed_milliseconds_stage = MissionTimer.getElapsedMillisecondsStage();
	public int obs_id = 0;			
	public String participant_id = "Not Set";	
	public Float yaw = null;
	public Double x = null;
	public Double y = null;
	public Double z = null;
	public Float pitch = null;			
	public Double motion_x= null;
	public Double motion_y = null;
	public Double motion_z = null;
	
	public PlayerStateData() {
		
	}
	
	
	
}
