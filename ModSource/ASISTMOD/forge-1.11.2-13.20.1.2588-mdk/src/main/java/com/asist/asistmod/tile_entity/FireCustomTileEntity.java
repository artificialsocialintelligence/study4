package com.asist.asistmod.tile_entity;

import java.util.Random;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.MissionSpecific.MissionA.FireManagement.FireManager;
import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimer;
import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimerListener;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class FireCustomTileEntity extends TileEntity implements ITickable, MissionTimerListener {

	private int timer = 0;
	public boolean hasSpreadOnce = false;
	
	private int spreadTime = 0; //Time (in seconds) to spread fire	
	public String id = "NOT_SET";
	
	public FireCustomTileEntity() {		
					
		this.subscribeToTimerTicks();		
		this.id = "FIRE" + (FireManager.fireId++);
		System.out.println(this.id + " instantiated. " );		
		
		// another way to check if you're on the client
		if( AsistMod.modSettings != null ) {
			this.spreadTime = AsistMod.modSettings.fire_spread_interval_seconds;
		}	
		
		
	}	
	
	public void subscribeToTimerTicks() {
		MissionTimer.subscribeToTimerTicks(this);	
	}
	
	public void unsubscribeToTimerTicks() {
		MissionTimer.unsubscribeToTimerTicks(this);	
	}
	
	@Override
	public void onMissionTimeChange(int m, int s) {
		//Spread fire when the mission is running
		timer++;
		
		if( (spreadTime>0) && (timer>=spreadTime) && !this.hasSpreadOnce) {
			timer = 0; //Reset timer
			spread(world);
			this.hasSpreadOnce = true;
			this.unsubscribeToTimerTicks();
			
		}
		
		////System.out.println("FireCustomTimerTick");
	}

	@Override	
	 /** The following code block is used to test the fire on the client side rather than the server.
	  *  Uncomment out the code block to use the fire on the client side. */
	public void update() {

	}
	
	
	 /** Set spread time
	 * @param spreadTimeInSeconds - how long it takes for fire to spread. ie: spreadTime = 5 means that it will take 5 seconds for the fire to spread
	 * and place new fire around itself */
	public void setSpreadTime(int spreadTimeInSeconds) {
		this.spreadTime = spreadTimeInSeconds;
		timer = 0;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	/** Checks for valid spawning spaces and places fire blocks where possible. Does not reset the timer! */
	public void spread(World world) {
		
		BlockPos pos = this.getPos();
		
		////System.out.println(this.id + " spreading from " + pos.toString() );
		
		FireManager.queueFire(world, pos.north(),this.id,false);
		FireManager.queueFire(world, pos.south(),this.id,false);
		FireManager.queueFire(world, pos.east(),this.id,false);
		FireManager.queueFire(world, pos.west(),this.id,false);		
		
	}
	
	

	public int getRandomIntInRange(int min, int max) {
		Random random = new Random();
		return random.ints(min, max)
				.findFirst()
				.getAsInt();
	}

}
