package com.asist.asistmod.MissionSpecific.MissionA.Timer;

import java.lang.Thread.State;
import java.time.Clock;
import java.util.concurrent.CopyOnWriteArrayList;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.MissionCommon.InterventionManagement.ChatInterventions.ChatInterventionManager;
import com.asist.asistmod.MissionSpecific.MissionA.FireManagement.FireManager;
import com.asist.asistmod.MissionSpecific.MissionA.MissionManagement.MissionManager;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.PlayerManager;
import com.asist.asistmod.datamodels.ModSettings.MinSec;
import com.asist.asistmod.datamodels.ModSettings.ModSettings_v3;
import com.asist.asistmod.mqtt.InternalMqttClient;
import com.asist.asistmod.network.NetworkHandler;
import com.asist.asistmod.network.messages.MissionTimerPacket;

import net.minecraft.server.MinecraftServer;

public class MissionTimer {
	
	private static ModSettings_v3 modSettings;
	
	public static int secondCount;
	public static int minuteCount;
	
	public static CopyOnWriteArrayList<MissionTimerListener> listeners = new CopyOnWriteArrayList<MissionTimerListener>();
	
	public static int stageSecondCount;
	public static int stageMinuteCount;
	
	// THIS IS THE VERY FIRST TIMESTAMP THE TIMER PRODUCES FOR THE TRIAL
	private static long startTimeMillisecondsGlobal = 0;
	
	// THIS IS THE VERY FIRST TIMESTAMP THE TIMER PRODUCES FOR EACH RESPECTIVE STAGE
	private static long startTimeMillisecondsStage = 0;
	
	// THIS TRACKS WHAT OUT CURRENT SECOND IS IN MILLISECONDS SO WE CAN CHECK IF WE SHOULD TICK, will always be start_time + (1000*s)
	private static long lastTickTimestamp = 0;
	
	public static boolean inWindDown = false;
	
	// MILLISECONDS OF SLEEP BETWEEN EACH TIMER LOOP
	private static int timerSleepInterval;
	
	private static MinSec time_in_recon;
	//private static MinSec time_in_shop;
	
	public static boolean reconComplete = false;
	public static boolean isInitialized = false;
	public static boolean isPaused = false;
		
	public static MinecraftServer server;
	
	public static Thread thread;	
	
	public static void init() {
		
		server = AsistMod.server;
		modSettings = AsistMod.modSettings;
		
		//time_in_field = modSettings.time_in_field;
		//time_in_shop = modSettings.time_in_shop;
		
		//String missionName = InternalMqttClient.currentTrialInfo.mission_name.toLowerCase();		
					
		minuteCount = 0;
		secondCount = 0;
		
		timerSleepInterval = 1000;
		
		// start time
		startTimeMillisecondsGlobal = Clock.systemDefaultZone().millis();
		lastTickTimestamp = startTimeMillisecondsGlobal;

		
		isInitialized = true;
		
		listeners.add(new FireManager());
		
		// timer thread
		
		startNewThread();
	}
	
	public static void startNewThread() {
		thread = new Thread() {			
			public void run() {				
				while (isInitialized) {					
					try { 
						Thread.sleep(timerSleepInterval); 
					} 
					catch (InterruptedException e) {
						e.printStackTrace();
					}					
					timerLoop();
				}
			}			
		};
		thread.start();
	}
	public static void timerLoop() {		
		
		if(isInitialized ) {
			
			long currentTime = Clock.systemDefaultZone().millis();
			
			long deltaTime = currentTime - lastTickTimestamp;			
			
			// ONE TICK
			if( deltaTime >= 1000 ) {
				
				lastTickTimestamp += 1000;
				mainTimerLoop();				
			
			}// END ONE TICK
		}		
	}
		
	private static void mainTimerLoop() {
		////System.out.println("Timer Tick " + getMissionTimeString() + " : isPaused : " + isPaused );
		
		if( !isPaused ) {
			
			if(secondCount == 59) {
				secondCount = 0;
				minuteCount ++ ;					
			}
			else {			
				secondCount ++;
			}		
			
			MissionTimer.listeners.forEach( (listener) ->{
				listener.onMissionTimeChange(minuteCount, secondCount);
			} );
		
			// UPDATE GUI ON ALL CLIENTS WITH THE NEW TIME
			server.getPlayerList().getPlayers().forEach(player ->{
				NetworkHandler.sendToClient(new MissionTimerPacket(minuteCount,secondCount), player);
			});
			
			// UPDATE CHAT INTERVENTION SUBSCRIBER
			ChatInterventionManager.onMissionTimeChange(minuteCount, secondCount, getElapsedMillisecondsGlobal() );		
			
			
			if (modSettings.missionLength.minute == minuteCount && modSettings.missionLength.second == secondCount) {
				
				MissionManager.endMission( MissionManager.StateChangeOutcome.MISSION_STOP_TIMER_END,false);
			}
			
			checkReconOver();
			
			checkIfInWindDown();
			// ENDUPDATE SUBSCRIBERS AND PLAYERS
		}
	}
	
	public static void checkReconOver() {
		
		if ( !reconComplete ) {
			
			if (modSettings.time_in_recon.minute == minuteCount && modSettings.time_in_recon.second == secondCount) {
				//System.out.println("Recon complete. Transitioning Players back to the Shop."); 
				MissionManager.transitionToShop();
				reconComplete = true;
			}
			
		}		
	}
	
	/*
	public static void startReconTimer() {
		
		stageMinuteCount = 0;
		stageSecondCount = 0;
				
		startTimeMillisecondsStage = Clock.systemDefaultZone().millis();
	}
	
	private static void reconTimerLoop() {		
			
		if( !isPaused ) {
			if(stageSecondCount == 59) {
				stageSecondCount = 0;
				stageMinuteCount ++ ;					
			}
			else {			
				stageSecondCount ++;
			}
		}		 
	}
	*/
	
	public static void pause() {
		MissionTimer.isPaused = true;
	}
	public static void unPause() {
		MissionTimer.isPaused = false;
	}
	
	public static String getMissionTimeString() {
		
		if(isInitialized) {
			return minuteCount + " : " + secondCount;
		}
		else return "Mission Timer not initialized.";
	}
	
	public static long getElapsedMillisecondsGlobal() {
		
		if (isInitialized) {
			
			return Clock.systemDefaultZone().millis() - startTimeMillisecondsGlobal;
		}
		
		return -1;
	}
	
	public static long getElapsedMillisecondsStage() {
		
		if (isInitialized) {
			
			return Clock.systemDefaultZone().millis() - startTimeMillisecondsStage;
		}
		
		return -1;
	}
	
	public static void startWindDown() {
		
		if( MissionTimer.isPaused) {
			MissionTimer.isPaused = false;
		}
		if(MissionTimer.isInitialized = false) {
			MissionTimer.isInitialized = true;
		}
		//System.out.println("In WindDown ...");
		//System.out.println(MissionTimer.thread.getState());		
		inWindDown = true;
		minuteCount = 0;
		secondCount = 0;
		
	}
	
	public static void checkIfInWindDown() {
			
		if( inWindDown) {
			
			server.commandManager.executeCommand(server, "tell @a Removing all participants in " + (5-secondCount) + " seconds.");
			
			if(minuteCount == 0 && secondCount == 5) {				
				
				PlayerManager.players.forEach( (p) ->{
					server.commandManager.executeCommand(server, "kick " + p.playerName);
				});
				//server.getPlayerList().removeAllPlayers();
				
				isInitialized = false;
				
				AsistMod.executing = false;				
				
				try {
					thread.join();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}				
			}			
		}		
	}
	
	public  static void subscribeToTimerTicks( MissionTimerListener listener) {
		MissionTimer.listeners.add(listener);
	}
	
	public  static void unsubscribeToTimerTicks( MissionTimerListener listener) {
		MissionTimer.listeners.remove(listener);
	}
}
