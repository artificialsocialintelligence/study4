package com.asist.asistmod.datamodels.Deprecated.RubblePlaced;

import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimer;
import com.asist.asistmod.MissionSpecific.MissionA.UtilityStructures.PositionRange;

public class RubblePlacedDataModel {
	
	public String mission_timer = MissionTimer.getMissionTimeString();public long elapsed_milliseconds = MissionTimer.getElapsedMillisecondsGlobal();
	public int from_x = 0;
	public int from_y = 0;
	public int from_z = 0;
	public int to_x = 0;
	public int to_y = 0;
	public int to_z = 0;
	
	public void AddValues( PositionRange range ) {
		from_x = range.from.getX();
		from_y = range.from.getY();
		from_z = range.from.getZ();
		to_x = range.to.getX();
		to_y = range.to.getY();
		to_z = range.to.getZ();
	}
		
}
