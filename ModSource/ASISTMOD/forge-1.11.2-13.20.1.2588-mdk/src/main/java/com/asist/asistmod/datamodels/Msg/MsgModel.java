package com.asist.asistmod.datamodels.Msg;

import com.asist.asistmod.AsistMod;

public class MsgModel {
	
	public String experiment_id = AsistMod.currentTrialInfo.experiment_id;
	public String trial_id = AsistMod.currentTrialInfo.trial_id;	
	public String source = "MINECRAFT_SERVER";
	public String sub_type = "NOT SET";
	public String version = "NOT SET";
	
	public MsgModel(String sub_type, String version) {
		this.sub_type = sub_type;
		this.version = version;
	}

}
