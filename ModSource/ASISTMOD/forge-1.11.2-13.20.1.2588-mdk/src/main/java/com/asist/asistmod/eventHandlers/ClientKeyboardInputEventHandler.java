package com.asist.asistmod.eventHandlers;

import com.asist.asistmod.MissionSpecific.MissionA.GuiManagement.MainOverlay.RenderOverlayEventHandler;
import com.asist.asistmod.network.NetworkHandler;
import com.asist.asistmod.network.messages.shop.VoteToGoToShopPacket;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.client.settings.GameSettings.Options;
import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent.KeyInputEvent;

public class ClientKeyboardInputEventHandler {
	
	Minecraft minecraft;
	GameSettings gs;
	EntityPlayerSP player;
	int code;
	
	public static boolean freeze = true;
	//public static boolean isGuiOpen = false;
	
	
	@SubscribeEvent 
	public void onKeyboardInput (KeyInputEvent event){		
		
		// INITIALIZATION
		if (player == null ) {			
			if( minecraft == null) {				
				minecraft = Minecraft.getMinecraft();				
			}
			
			if( gs == null) {				
				
				gs = minecraft.gameSettings;			
				
				if(gs.autoJump == true) {					
					gs.setOptionValue(Options.AUTO_JUMP, 0);
				}			
			}			
			player = minecraft.player;			
			code = gs.keyBindJump.getKeyCode();		
		
		}		
		
		if( !player.isCreative() ) {
			
			
			if(gs.autoJump == true) {				
				gs.setOptionValue(Options.AUTO_JUMP, 0);
			}
			// CANCEL JUMP
			if( gs.keyBindJump.isPressed() ) {			
				
				if(!player.isCreative()) {				
					
					KeyBinding.setKeyBindState(code, false);
					
					// RAYTRACE AND OPEN CONTEXT SPECIFIC GUI
					try {
						
						//RayTraceResult rayTrace = minecraft.objectMouseOver;
						//IBlockState blockState  = minecraft.world.getBlockState(rayTrace.getBlockPos());
						//minecraft.displayGuiScreen(new ContextCommunicator( blockState  ));
						//isGuiOpen = true;
						// minecraft.displayGuiScreen(new GuiShopTemplate(0));
												
					}
					catch(Exception e){
						e.printStackTrace();
					}					
					
				}				
			}
			else if( gs.keyBindTogglePerspective.isPressed() ) {			
										
				KeyBinding.setKeyBindState(gs.keyBindTogglePerspective.getKeyCode(), false);				
							
			}
			else if( gs.keyBindInventory.isPressed() ) {			
				
				KeyBinding.setKeyBindState(gs.keyBindInventory.getKeyCode(), false);
				NetworkHandler.sendToServer( new VoteToGoToShopPacket(RenderOverlayEventHandler.callSign) );
							
			}
			else if (gs.keyBindSwapHands.isPressed() ) {
				
				KeyBinding.setKeyBindState(gs.keyBindSwapHands.getKeyCode(), false);
				
			}
			else if (gs.keyBindDrop.isPressed() ) {
				
				KeyBinding.setKeyBindState(gs.keyBindDrop.getKeyCode(), false);
								
			}
			//else if (gs.)
			// DISABLE PLAYER WALKING IN THE SHOP
			if( freeze ) {
				// //System.out.println( OrionManager.stage == MissionStage.SHOP_STAGE );
				if ( gs.keyBindForward.isPressed() ) {					
					KeyBinding.setKeyBindState(gs.keyBindForward.getKeyCode(), false);
				}
				else if ( gs.keyBindBack.isPressed() ) {
					KeyBinding.setKeyBindState(gs.keyBindBack.getKeyCode(), false);
					
				}
				else if ( gs.keyBindLeft.isPressed() ) {
					KeyBinding.setKeyBindState(gs.keyBindLeft.getKeyCode(), false);
					
				}
				else if ( gs.keyBindRight.isPressed() ) {
					KeyBinding.setKeyBindState(gs.keyBindRight.getKeyCode(), false);					
				}
			}			
		}
		return;	
	}
}
