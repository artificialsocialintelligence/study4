package com.asist.asistmod.network.messages;

import com.asist.asistmod.MissionSpecific.MissionA.GuiManagement.MainOverlay.RenderOverlayEventHandler;
import com.asist.asistmod.MissionSpecific.MissionA.MissionManagement.MissionManager;
import com.asist.asistmod.MissionSpecific.MissionA.MissionManagement.MissionManager.MissionStage;
import com.asist.asistmod.eventHandlers.ClientKeyboardInputEventHandler;
import com.asist.asistmod.network.MessagePacket;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;

public class MissionStageTransitionPacket extends MessagePacket<MissionStageTransitionPacket> {
	
	MissionStage stage;

	
	public MissionStageTransitionPacket()  {
				
	}

	public MissionStageTransitionPacket( MissionStage stage)  {
		this.stage = stage;

	}

	@Override
	public void fromBytes(ByteBuf buf) {
		
		// 0 - SHOP_STAGE, 1 - FIELD STAGE		
		int stageCode = buf.readInt();
		if( stageCode == 0) { this.stage = MissionStage.SHOP_STAGE;}
		else if( stageCode == 1) { this.stage = MissionStage.FIELD_STAGE;}	
		else if( stageCode == 2) { this.stage = MissionStage.RECON_STAGE;}
		else if( stageCode == 3) { this.stage = null;}
	}

	@Override
	public void toBytes(ByteBuf buf) {
		
		// 0 - SHOP_STAGE, 1 - FIELD STAGE		
		int stageCode = 0;
		if( this.stage == MissionStage.SHOP_STAGE) { stageCode=0;}
		else if( this.stage == MissionStage.FIELD_STAGE) {stageCode=1;}
		else if( this.stage == MissionStage.RECON_STAGE) {stageCode=2; }
		else if( this.stage == null) {stageCode=3; }
		buf.writeInt(stageCode);

	}

	@Override
	public void handleClientSide(MissionStageTransitionPacket message, EntityPlayer player) {
		// TODO Auto-generated method stub
		
		//System.out.println("Handle MissionStageTransitionPacket ClientSide : " + message.stage);
		RenderOverlayEventHandler.stage = message.stage;
		if( message.stage == MissionStage.SHOP_STAGE) {
			ClientKeyboardInputEventHandler.freeze = true;			
		}
		else {
			ClientKeyboardInputEventHandler.freeze = false;			
		}
		MissionManager.stage = message.stage;
	}

	@Override
	public void handleServerSide(MissionStageTransitionPacket message, EntityPlayer player) {
		// TODO Auto-generated method stub
		
	}

}
