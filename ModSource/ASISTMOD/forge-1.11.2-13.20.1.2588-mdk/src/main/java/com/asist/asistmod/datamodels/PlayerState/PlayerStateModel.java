package com.asist.asistmod.datamodels.PlayerState;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.datamodels.Header.HeaderModel;
import com.asist.asistmod.datamodels.Msg.MsgModel;
import com.asist.asistmod.mqtt.MessageTopic;

public class PlayerStateModel {
	
	
	
	public HeaderModel header = new HeaderModel();
	
	public MsgModel msg = new MsgModel(getEventName(),getVersion());
	
	public PlayerStateData data = new PlayerStateData();
	
	public PlayerStateModel() {	
		
	}	
	
	public String toJsonString() { 		
		
		return AsistMod.gson.toJson(this); 
		
	}
	
	public MessageTopic getMessageTopicEnum() {
		return MessageTopic.PLAYER_STATE;
	}
	
	public String getTopic() {
		return getMessageTopicEnum().getTopic();
	}
	
	public String getEventName() {
		return getMessageTopicEnum().getEventName();
	}
	
	public String getVersion() {
		return getMessageTopicEnum().getVersion();
	}

	
}
