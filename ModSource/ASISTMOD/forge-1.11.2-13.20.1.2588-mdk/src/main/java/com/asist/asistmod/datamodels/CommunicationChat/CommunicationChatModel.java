package com.asist.asistmod.datamodels.CommunicationChat;

import java.util.Map;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.datamodels.Header.HeaderModel;
import com.asist.asistmod.datamodels.Msg.MsgModel;
import com.asist.asistmod.datamodels.ObjectStateChange.ObjectStateChangeData;
import com.asist.asistmod.mqtt.MessageTopic;

public class CommunicationChatModel {
	
	public HeaderModel header = new HeaderModel();
	
	public MsgModel msg = new MsgModel(getMessageTopicEnum().getEventName(),getMessageTopicEnum().getVersion());
	
	public CommunicationChatData data = new CommunicationChatData();		
		
	public CommunicationChatModel( ) {		
		
				
	}	
	
	public String toJsonString() { 
		
		return AsistMod.gson.toJson(this);		
	}
	
	public MessageTopic getMessageTopicEnum() {
		return MessageTopic.COMMUNICATION_CHAT;
	}
	
	public String getTopic() {
		return getMessageTopicEnum().getTopic();
	}
	
	public String getEventName() {
		return getMessageTopicEnum().getEventName();
	}
	
	public String getVersion() {
		return getMessageTopicEnum().getVersion();
	}


}
