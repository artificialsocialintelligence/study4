package com.asist.asistmod.MissionSpecific.MissionA.GuiManagement.ShopGui;

import java.io.IOException;
import java.util.concurrent.CopyOnWriteArrayList;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.GLContext;

import com.asist.asistmod.AsistMod;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;

public class GuiShopTemplate extends GuiScreen {
	
	public static enum PageType {
		
		PAGE_0,
		PAGE_1,
		PAGE_2
		
	}
	
	public static String thisPlayersCallsign = "NOT SET";
	
	public static String[] otherCallSigns = {"NOT_SET"};
	
	
	
	
	final ResourceLocation bg_texture = new ResourceLocation(AsistMod.MODID,"textures/gui/gui_background.png");	
	
	
	final int bg_texture_width = 256;
	final int bg_texture_height = 256;
	

	final int buttonWidth = 30;
	int centerX = 0;
	int centerY = 0;
	
	Minecraft minecraft;
	
	int actualScaledWidth;
	int actualScaledHeight;			
	
	int widthScaledBackTo1;
	int heightScaledBackTo1;
	

	
	
	// NORMAL BUTTONS

	
	GuiButton leaveShopButton;
	
	
	public static int pageTypeIndex;
	
	boolean plus_minus_buttons_initialized=false;
	
	public static GuiShopTemplate guiRef;
	
	public static int teamBudget = 0;
	
	// STATIC MEMBERS	
	public static CopyOnWriteArrayList<String[]> chatList = new CopyOnWriteArrayList<String[]>();
	
	public GuiShopTemplate( int i) {
		
		
		this.minecraft = Minecraft.getMinecraft();
		pageTypeIndex = i;		
		guiRef = this;

		
		
	}
	
	public static void incrementPageIndex() {
		pageTypeIndex++;
		guiRef.initGui();
		guiRef.onResize(Minecraft.getMinecraft(),guiRef.width, guiRef.height);
	}
	
	// USE THE SCREEN WIDTH/HEIGHT AND THE INDIVIDUAL WIDTH HEIGHT OF THE ELEMENT ITSELF TO PLACE IT AND RESIZE IT - DO
	// NOT USE HARDCODED PIXEL VALUES AS ANY RESIZE WILL BREAK THE UI LAYOUT
	@Override
	public void drawScreen( int mouseX, int mouseY, float partialTicks) {		
		
		this.bindTexturesToRenderEngine();
		
		GlStateManager.enableRescaleNormal();
		
		this.drawScaledBGTexture();
		
		//this.drawTexturedModalRect(0,0,0,0,map_texture_width,map_texture_height);
					
		this.leaveShopButton.xPosition = this.width/2;
		this.leaveShopButton.yPosition = this.height/2;
		this.leaveShopButton.width = this.width/8;
		this.leaveShopButton.height = this.height/8;
		this.leaveShopButton.drawButton(minecraft, mouseX, mouseY);
			
			
		
		

		
	
		
		super.drawScreen(mouseX, mouseY, partialTicks);
	}
	
	
	
	public void drawScaledBGTexture() {
		
		try {
			GLContext.useContext(mc);				
			
			GlStateManager.pushMatrix();			
			
			float x = (float)width/this.bg_texture_width;  //512 -> my texture size
			float y = (float)height/this.bg_texture_height; //384 -> my texture size
			GlStateManager.scale(x, y, 1.0);
			this.mc.renderEngine.bindTexture(this.bg_texture);
			Gui.drawModalRectWithCustomSizedTexture(0, 0, 0, 0, this.bg_texture_width, this.bg_texture_height, this.bg_texture_width, this.bg_texture_height);			
			
			GlStateManager.popMatrix();
			
		} catch (LWJGLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public void initGui() {
		int fontheight = this.fontRendererObj.FONT_HEIGHT;
		centerX = (width/2); 
		centerY = (height/2); 
		this.leaveShopButton = new GuiButton(22, (int)(width*.8),(int)(height*.8),(int)(width*.205),(int)(height*.05), "Leave Shop");
		this.addButton(this.leaveShopButton);
		super.initGui();
	}
	
	public void drawAtPosition(String value, float xFloatPercentFromRight, float yFloatPercentFromTop, String colorCode) {
		
		drawString(mc.fontRendererObj, value, (int)( widthScaledBackTo1 - ( widthScaledBackTo1 * xFloatPercentFromRight ) ), (int)( heightScaledBackTo1 - (heightScaledBackTo1*yFloatPercentFromTop) ), Integer.parseInt(colorCode, 16));
		
	}
	
	
	
	@Override
	public void actionPerformed( GuiButton button) throws IOException {
		
			
		//ContextCommunicatorUtilities.processButtonClick(button.id);
		
		//super.actionPerformed(button);
		
		//sentOnce = false;
	}
	
	public static void addChatToRenderedList( String[] text){
		chatList.add(text);
	}
	
	
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException{
		
		if (mouseButton == 0)
        {
			
            
            for (int i = 0; i < this.buttonList.size(); ++i)
            {
                GuiButton guibutton = (GuiButton)this.buttonList.get(i);

                if (guibutton.mousePressed(this.mc, mouseX, mouseY))
                {
                    guibutton.playPressSound(this.mc.getSoundHandler());                   
                    	
                    GuiShopUtilities.processButtonClick(guibutton.id);
                    
                    if (guibutton.id == 12) {
                    	 
                    	
                    	
                    }
                    
                    //PROCEED Button
                    else if (guibutton.id == 13) {
                    }
                    else if (guibutton.id >= 14 && guibutton.id <= 21) { 

                    }
                    else if (guibutton.id == 22) {
                    	
                    
                    	
                    }
                    
                   
                }
            }
        }
		
	}
	
	
	
	@Override
	public boolean doesGuiPauseGame( )  {
		return false;
	}
	
	@Override
	 protected void keyTyped(char typedChar, int keyCode) throws IOException
    {
	
	   ////System.out.println(typedChar);
	   ////System.out.println(keyCode);
	  
	   if (keyCode == 1)
       {
           this.mc.displayGuiScreen((GuiScreen)null);

           if (this.mc.currentScreen == null)
           {
               this.mc.setIngameFocus();
           }
       }
      
    }
	
    public void updateScreen()
    {
    	
    }
    
    public boolean isTextFieldClicked(GuiTextField tf,int mouseX, int mouseY) {
		
		int tfWidth = tf.width;
		int tfHeight = tf.height;
		int tfXPos = tf.xPosition;
		int tfYPos = tf.yPosition;
		
		////System.out.println(mouseX+","+mouseY+","+tfXPos+","+tfYPos);
		
		if( (mouseX <= tfXPos+tfWidth) && (mouseX>=tfXPos)) {
			if( (mouseY <= tfYPos+tfHeight) && (mouseY>=tfYPos)) {
				return true;
			}
		}
		
		return false;
	}   
    
    public void bindTexturesToRenderEngine() {
    	
    	Minecraft.getMinecraft().renderEngine.bindTexture(bg_texture);

    } 

}
