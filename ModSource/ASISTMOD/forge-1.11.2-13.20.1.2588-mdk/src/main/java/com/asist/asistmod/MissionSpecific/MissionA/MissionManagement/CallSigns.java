package com.asist.asistmod.MissionSpecific.MissionA.MissionManagement;

import java.util.Arrays;

public class CallSigns {
	
	public static String[] masterCallSignArray;
	
	
	
	public static void init( String[] callSignList) {
		masterCallSignArray = callSignList;
		//System.out.println( "Initialized Master CallSign Array :");
		//System.out.println( Arrays.toString(masterCallSignArray));
		
	}
	
	public static int getCallSignCode(String callSign) {
		
		for(int i =0; i< masterCallSignArray.length; i++) {
			if(  callSign.contentEquals(masterCallSignArray[i] ) ) {
				return i;
			}
		}
		
		return -1;
	}
	
}
