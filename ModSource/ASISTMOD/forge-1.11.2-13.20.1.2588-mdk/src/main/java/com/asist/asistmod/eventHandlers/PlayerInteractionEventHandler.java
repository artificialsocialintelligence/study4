package com.asist.asistmod.eventHandlers;

import java.util.HashMap;
import java.util.Map;

import com.asist.asistmod.MissionCommon.Utilities.AttributeManagement.AttributedObjectManager;
import com.asist.asistmod.MissionSpecific.MissionA.BlockManagement.BlockType;
import com.asist.asistmod.MissionSpecific.MissionA.BlockManagement.ModBlocks;
import com.asist.asistmod.MissionSpecific.MissionA.ItemManagement.ItemInteractionMode;
import com.asist.asistmod.MissionSpecific.MissionA.ItemManagement.ItemManager;
import com.asist.asistmod.MissionSpecific.MissionA.ItemManagement.ItemType;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.Player;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.PlayerManager;
import com.asist.asistmod.block.bombs.BlockBombBase;
import com.asist.asistmod.datamodels.Door.DoorModel;
import com.asist.asistmod.datamodels.EnvironmentRemoved.Single.EnvironmentRemovedSingleModel;
import com.asist.asistmod.datamodels.ItemStateChange.ItemStateChangeModel;
import com.asist.asistmod.datamodels.ItemUsed.ItemUsedModel;
import com.asist.asistmod.datamodels.Lever.LeverModel;
import com.asist.asistmod.datamodels.ModSettings.ModSettings_v3;
import com.asist.asistmod.datamodels.ObjectStateChange.ObjectStateChangeData;
import com.asist.asistmod.datamodels.ToolUsed.ToolUsedModel;
import com.asist.asistmod.mqtt.InternalMqttClient;
import com.asist.asistmod.tile_entity.BeaconTileEntity;
import com.asist.asistmod.tile_entity.BombBlockTileEntity;

import net.minecraft.block.Block;
import net.minecraft.block.BlockDoor;
import net.minecraft.block.BlockFire;
import net.minecraft.block.BlockLever;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;


public class PlayerInteractionEventHandler {

MinecraftServer server;
ModSettings_v3 modSettings;
Boolean missionStarted = false;
String worldName;

//HashMap< String, BlockPos > triageManager = new HashMap();
	
	public PlayerInteractionEventHandler( MinecraftServer server, ModSettings_v3 modSettings) {
		
		this.server = server;
		this.modSettings = modSettings;
		
	}
	

	@SubscribeEvent	
	public void onPlayerInteraction( PlayerInteractEvent event) {
		
		BlockPos blockPos = event.getPos();
		int x = blockPos.getX();
		int y = blockPos.getY();
		int z = blockPos.getZ();
		World world = event.getWorld();
		IBlockState blockState = world.getBlockState(blockPos);
		
		
		
		Block block = blockState.getBlock();		
		
		worldName = world.getWorldInfo().getWorldName();		
		
		EntityPlayer entityPlayer = event.getEntityPlayer();
		
		String playerName = entityPlayer.getName();
		
		// will create player if it does not exist
		Player player = PlayerManager.getPlayerByName(playerName);		
		
		String participant_id = InternalMqttClient.name_to_pid(playerName);
		
		String blockName = block.getRegistryName().toString();		
	
		BlockType modBlock = ModBlocks.getEnum(blockState);		
		
		ItemStack heldItemStack = entityPlayer.getHeldItemMainhand();
		String heldItemRegistryName = heldItemStack.getItem().getRegistryName().toString();
		ItemType heldModItem = ItemManager.getItemTypeFromRegistryName(heldItemRegistryName);
		
		
		
		
		try {
			// MAKES SURE THAT EVENT ONLY FIRES FOR 1 HAND NOT BOTH		
			if (event.getHand() == entityPlayer.getActiveHand()) {
				/*********************** LEFT CLICK INTERACTIONS **************************************************************************************************/	
				if(event instanceof PlayerInteractEvent.LeftClickBlock) {						
									
					if(heldModItem.compareTo(ItemType.NULL)!=0) {
						//We do this to Nullify the canDestroy which would allow left click to put out fires
						if( heldModItem.interactionMode.compareTo(ItemInteractionMode.RIGHT_MOUSE)==0 )
						{
							event.setCanceled(true);
							return;
						}
						
						String itemName = heldModItem.getName();
						String itemStackId = heldItemStack.getTagCompound().getTag("item_stack_id").toString();
						////System.out.println(itemStackId);
						itemStackId = itemStackId.replaceAll("\"", "");
						////System.out.println(itemStackId);
						String itemId = "ITEM"+Integer.toString(heldItemStack.getCount());
						String idForMessage = itemStackId+'_'+itemId;
						//System.out.println("ITEM ID FOR MESSAGE : "+idForMessage);
						
						int uses = 0;
						int maxUses = 0;
						int durability = 0;
									
						uses = heldItemStack.getItemDamage();
						maxUses = heldItemStack.getMaxDamage();
						durability = maxUses - uses;
						
						ToolUsedModel toolUsedModel = new ToolUsedModel();
						toolUsedModel.msg.experiment_id = InternalMqttClient.currentTrialInfo.experiment_id;
						toolUsedModel.msg.trial_id = InternalMqttClient.currentTrialInfo.trial_id;
						//toolUsedModel.data.playername = playerName;
						toolUsedModel.data.participant_id = participant_id;
						toolUsedModel.data.tool_type = itemName;
						toolUsedModel.data.durability = durability;
						toolUsedModel.data.target_block_x = x;
						toolUsedModel.data.target_block_y = y;
						toolUsedModel.data.target_block_z = z;
						toolUsedModel.data.target_block_type = blockName;
						InternalMqttClient.publish(toolUsedModel.toJsonString(), "observations/events/player/tool_used",playerName);
						
						ItemUsedModel itemUsedModel = new ItemUsedModel();
						itemUsedModel.data.participant_id = participant_id;						
						itemUsedModel.data.item_id = idForMessage;
						itemUsedModel.data.item_name = itemName;
						itemUsedModel.data.input_mode = "LEFT_MOUSE";
						itemUsedModel.data.target_x = x;
						itemUsedModel.data.target_y = y;
						itemUsedModel.data.target_z = z;
						
						InternalMqttClient.publish(itemUsedModel.toJsonString(), itemUsedModel.getTopic(), playerName);
						
						Map<String,String> currAttr = AttributedObjectManager.getItemAttributes(itemStackId,itemId);
						int usesRemaining = Integer.parseInt(currAttr.get("uses_remaining"));
						
						// DONT REDUCE BEACONS WHEN THEY ARE USED TO BREAK OTHER BEACONS
						if ( heldItemRegistryName.contains("beacon")  ) {
							// NO STATE CHANGE TO THE HELD ITEM
							
							// STATE CHANGE TO THE BROKEN BEACON
							if ( blockName.contains("beacon")  ) {
								// We are breaking a beacon
								BeaconTileEntity te = (BeaconTileEntity)event.getWorld().getTileEntity(blockPos);
								
								String id = te.getBeaconId();
								//System.out.println("PlayerInteractionLeftClick ID of DESTROYED BEACON : " + id  );
								String stackid = id.split("_")[0];
								String itemid = id.split("_")[1];
								
								// EnvironmentRemovedSingle
								ObjectStateChangeData data = new ObjectStateChangeData(true);
					        	data.id = id;
					    		data.type = blockName.split(":")[1];
					    		data.x = x;
					    		data.y = y;
					    		data.z = z;
					    		data.currAttributes = AttributedObjectManager.getItemAttributes(stackid, itemid);
								EnvironmentRemovedSingleModel envRemovedModel = new EnvironmentRemovedSingleModel(participant_id,data);
								InternalMqttClient.publish(envRemovedModel.toJsonString(), envRemovedModel.getTopic());
							}
							
						}
						else {							
							usesRemaining--;
							Map<String,String> newAttr = new HashMap<String,String>();
							newAttr.put("uses_remaining", Integer.toString(usesRemaining) );
							Map<String, String[]> changedAttributes = AttributedObjectManager.updateItemAttributes(itemStackId, itemId, newAttr);
							// for all single use items
							ItemStateChangeModel itemStateChangeModel = new ItemStateChangeModel(idForMessage,itemName,participant_id,changedAttributes,currAttr);
							InternalMqttClient.publish(itemStateChangeModel.toJsonString(), itemStateChangeModel.getTopic(), playerName);							
						}						
					}
					else {
						//System.out.println("PlayerInteractionEvent Left Click HeldItem has ItemType.NULL!!!! NOT GOOD.");
					}
				}
				/*********************** RIGHT CLICK INTERACTIONS **************************************************************************************************/
				if(event instanceof PlayerInteractEvent.RightClickBlock) {
					
					if( heldItemRegistryName.contains("beacon")  ) {
						// If they put out a fire by placing a beacon, we remove the fire appropriately, remove the beacon appropriately, 
						// and re-instantiate the fire appropriately with a new id
						if(world.getBlockState(blockPos.up()).getBlock() instanceof BlockFire) {
							
							event.setCanceled(true);
							
							//System.out.println("---PLAYER INTERACTION EVENT ----- BEACON PLACED ON FIRE ------");
							
							/*
							BlockPos firePos = blockPos.up();
							
							String id = "NOT_SET";
							
							FireCustomTileEntity te = (FireCustomTileEntity)( world.getTileEntity( firePos ) );					
							
							if( te != null ) {
								id = te.id;	
								te.unsubscribeToTimerTicks();
							}
							
							FireManager.removeFire(world, firePos);					

							// Build Message for server
							if(!world.isRemote) {								
								String pid = PlayerManager.getPlayerByName(playerName).getParticipantId();						
								ObjectStateChangeModel objectStateChangeModel = new ObjectStateChangeModel(
										id,
										world.getBlockState(firePos).getBlock().getRegistryName().toString().split(":")[1],
										pid,
										firePos.getX(),
										firePos.getY(),
										firePos.getZ(),
										null,
										null
								);


								// EnvironmentRemovedSingle
								EnvironmentRemovedSingleModel envRemovedModel = new EnvironmentRemovedSingleModel(pid, objectStateChangeModel.data);
								InternalMqttClient.publish(envRemovedModel.toJsonString(), envRemovedModel.getTopic());								
								FireManager.placeFire(world, firePos, id, true);
							}
							*/
						}
						
					}
					
					if ( block instanceof BlockBombBase) {
						
						BombBlockTileEntity te = (BombBlockTileEntity) world.getTileEntity(blockPos);
						
						player.lastBombId = te.getBombId();
						player.lastBombType = te.getBlockType().getRegistryName().toString();
						player.lastBombPosition = te.getPos();
						
						if ( block instanceof BlockBombBase) {						
							if(te != null) {
								te.outputSequenceToPlayer(playerName);
							}
						}
						
						/*
						// begins to tick on inspection
						else if( block instanceof BlockBombVolatile)
						{
							if(te != null) {
								String output = "";
								te.outputSequenceToPlayer(playerName);						

								
								if(te.isCountingDown()) {
									output = "TRIGGERED_ADVANCE_SEQUENCE";
								}else {
									te.beginCountDown();
									output = "TRIGGERED";
								}
								

								
								// START UPDATE ATTRIBUTES AND SEND OBJECT_STATE_CHANGE MESSAGE
								StringBuilder sb = new StringBuilder();
								for(char c : te.getDefuseSequence()) {
									sb.append(c);
								}
								Map<String,String> newAttr = new HashMap<String,String>();
								newAttr.put("sequence", sb.toString() );
								newAttr.put("sequence_index", Integer.toString(te.getDefuseIndex()) );
								newAttr.put("active", "true");
								newAttr.put("outcome", output);
								newAttr.put("fuse_duration", String.valueOf(te.getFuseDuration()));
								
								Map<String, String[]> changedAttributes = AttributedObjectManager.updateObjectAttributes(te.getBombId(), newAttr);
								
								// Build Message
								ObjectStateChangeModel objectStateChangeModel = new ObjectStateChangeModel(
										te.getBombId(),
										blockName.split(":")[1],
										participant_id,
										x,
										y,
										z,
										changedAttributes,
										AttributedObjectManager.getObjectAttributes(te.getBombId())
								);
								
								InternalMqttClient.publish(objectStateChangeModel.toJsonString(), objectStateChangeModel.getTopic());
								// END UPDATE ATTRIBUTES AND SEND OBJECT_STATE_CHANGE MESSAGE
							}
						}
						*/						
					}					
					
					/*********************** LEVER EVENT *****************************************************************/
					else if(  block instanceof BlockLever){	
			
						LeverModel leverModel = new LeverModel();
						leverModel.msg.experiment_id = InternalMqttClient.currentTrialInfo.experiment_id;
						leverModel.msg.trial_id = InternalMqttClient.currentTrialInfo.trial_id;
						//leverModel.data.playername = playerName;
						leverModel.data.participant_id = participant_id;
						leverModel.data.powered = !(Boolean) blockState.getProperties().get( BlockLever.POWERED );
						leverModel.data.lever_x = x;
						leverModel.data.lever_y = y;
						leverModel.data.lever_z = z;			
						InternalMqttClient.publish(leverModel.toJsonString(), "observations/events/player/lever", playerName);								
						
					}
					/*********************** DOOR EVENT *****************************************************************/
					else if ( block instanceof BlockDoor) {
						
						DoorModel doorModel = new DoorModel();
						doorModel.msg.experiment_id = InternalMqttClient.currentTrialInfo.experiment_id;
						doorModel.msg.trial_id = InternalMqttClient.currentTrialInfo.trial_id;
						//doorModel.data.playername = playerName;
						doorModel.data.participant_id = participant_id;
						doorModel.data.door_x = x;
						doorModel.data.door_y = y;
						doorModel.data.door_z = z;
						
						//IBlockState lower = null;
						//IBlockState upper = null;
						boolean upperClicked = false;
						
						// only the lower half indicates if a door is open or not
						if( blockState.getValue(BlockDoor.HALF) == BlockDoor.EnumDoorHalf.UPPER) {							
							// the action has occurred so its the opposite state of the door currently							
							// upper = event.getWorld().getBlockState(blockPos);
							// lower = event.getWorld().getBlockState(blockPos.down());
							upperClicked=true;
						}
						else {
							//upper = event.getWorld().getBlockState(blockPos.up());
							//lower = event.getWorld().getBlockState(blockPos);
						}
						
						/*
						// int[3]
						int[] result = RegionManager.checkDoor( blockPos );
						int half = (int)Math.ceil(RegionManager.regionTotals[result[2]]/2.0f);
						String successString = "Region " + (result[2]+1) + " Unlocked!";
						String failureString = " There are "+RegionManager.bombsPerRegion[result[2]].size() + " bombs remaining in this region. "
								+ "You must defuse at least half ("+ half +") of the bombs in this region to unlock the next one!";
						String message;
						
						if(result[0]==1) {
							if(result[1]==1) {
								message = successString;
								AsistMod.server.commandManager.executeCommand(AsistMod.server, "/tell @a " + message);
								MissionLevelProgressModel model = new MissionLevelProgressModel( result[2]+1 );
								model.publish();
							}
							else {	
								message = failureString;
								AsistMod.server.commandManager.executeCommand(AsistMod.server, "/tell "+playerName+" "+message);
								
								//give it the opposite state you want it to be in . ie the state that should trigger closed when toggled is "true" - so your passing the pre-toggle state;
								((BlockDoor) block).toggleDoor(world, blockPos, true);
								world.markBlockRangeForRenderUpdate(upperClicked==true?blockPos.down():blockPos, upperClicked==true?blockPos:blockPos.up());								
							}
							String gateId = "GATE_"+result[2];
							CommunicationEnvironmentModel comEnvModel = new CommunicationEnvironmentModel(
									gateId, 
									block.getRegistryName().toString().split(":")[1],
									new String[]{participant_id},
									message,
									x,
									y,
									z,
									null
							);
							InternalMqttClient.publish(comEnvModel.toJsonString(), comEnvModel.getTopic());
						}
						*/
						doorModel.data.open = !(Boolean) world.getBlockState(upperClicked==true?blockPos.down():blockPos).getProperties().get( BlockDoor.OPEN );
						InternalMqttClient.publish(doorModel.toJsonString(), "observations/events/player/door",playerName);
						
						
					}					
				}
			}			
		}
		catch(Exception e) {
			
			//System.out.println("Something went wrong in the PlayerInteractionEventHandler Class - summary below");
			
			//System.out.println( "BlockPos : " + blockPos.toString() );			
			//System.out.println( "IBlockStateObjectNull : " + (blockState==null?"True":"False"));
			//System.out.println( "WorldObjectNull : " + (world==null?"True":"False"));
			//System.out.println( "BlockObjectNull : " + (block==null?"True":"False"));
			//System.out.println( "BlockName : " + (blockName==null?"null":blockName));
			//System.out.println( "ModBlock : " + (modBlock==null?"null":modBlock.getName()));
			//System.out.println( "HeldItem : " + (heldModItem.getName()==null?"null":heldModItem.getName()));
			//System.out.println( "PID : " + (participant_id==null?"null":participant_id));
			
			e.printStackTrace();
			
		}
		
	}

	
}
