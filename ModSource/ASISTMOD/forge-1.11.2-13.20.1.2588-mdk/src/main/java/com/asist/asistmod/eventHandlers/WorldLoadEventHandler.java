package com.asist.asistmod.eventHandlers;

import com.asist.asistmod.MissionCommon.BlockPlacement.MapBlockManager;
import com.asist.asistmod.MissionCommon.BlockPlacement.MapBlockManager.MapBlockMode;
import com.asist.asistmod.datamodels.ModSettings.ModSettings_v3;

import net.minecraft.server.MinecraftServer;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class WorldLoadEventHandler {
	
	int count = 0;
	
	MinecraftServer server;
	
	ModSettings_v3 modSettings;
	
	public WorldLoadEventHandler(ModSettings_v3 m) {
		
		modSettings = m;
		
	}
	
	@SubscribeEvent
	public void onClientWorldLoad( WorldEvent.Load event) {
		
		server = event.getWorld().getMinecraftServer();
		
		// on the first Load Event
		
		if (count == 0) {
			
			++count;
			
			//System.out.println(" --------------------------------------"); 
			//System.out.println(" CLIENT MAPBLOCK FUNCTIONALITY IS ACTIVATED! ");
			//System.out.println(" LOADING THE FOLLOWING BLOCKS! ");
			//System.out.println(" --------------------------------------");
			
			// THIS SHOULD BE CHANGED TO SPECIFY WHAT TO LOAD FROM MODSETTINGS
			MapBlockManager.addBlocksFromFile("./mods/"+modSettings.clientSideMapBuilderMapBlockFile,event.getWorld().getMinecraftServer().worlds[0], MapBlockMode.MISSION_START);
		}		
	}	
}