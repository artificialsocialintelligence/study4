
"mqtthost" | string | the ip address and port # of the mqtt broker to be used, for example "tcp://10.20.30.4:1883"

"observationInterval" | number | the number of milliseconds between each player observation loop

"clientSideMapBuilder"| boolean | true value allows the mod to be used to build maps with custom blocks using a minecraft client instance without a server. 
Building a map can this way can utilize the map_block system. Setting should be false when not in use. Server will not work if client attempts to connect to server while this is set to true.

"triageScoreVisibleToPlayer"| boolean | toggles whether or not the player can see their own triage score 

"removePlayersOnMissionEnd"| boolean | toggles whether or not all player will be kicked from the server after the mission ends. 
This avoids collecting additionaly unnecessary positional data after the end of the mission.

"missionLength" | object |
    
    "minute"| number | the minute value of the mission duration
    "second"| number | the second value of the mission duration


"criticalVictimExpirationTime" | object |
    
    "minute"| number | the minute value of the time at which critical victims should expire
    "second"| number | the second value of the time at which critical victims should expire


"observerNames" | string array | an array containing a list of substrings that denote the avatar is an observer and not a player

For example "observerNames" = ['Observer'] --> Every player with the substring Observer contained in their name would be classified an observer.

"pauseTimes" | object array | 

    [

        "minute"| number | the minute value of the time at which a pause should occur
        "second"| number | the second value of the time at which a pause should occur

    ]

"triagePoints" | object |
    
    "green" | number | the number of points awarded for triaging a green victim

    "yellow"| number | the number of points awarded for triaging a yellow victim
    

"Test" | object |

    "SpawnPoint" | object |

        "X" | number | x value of coordinate location at which a player will spawn into the Test map,
        "Y" | number | y value of coordinate location at which a player will spawn into the Test map,
        "Z" | number | z value of coordinate location at which a player will spawn into the Test map,
    
    "MissionStartButton" | object |
    
        "X" | number | x value of coordinate location of the Mission Start Button, which triggers an Event:MissionState message and often triggers equipment being given to the player
        "Y" | number | y value of coordinate location of the Mission Start Button, which triggers an Event:MissionState message and often triggers equipment being given to the player
        "Z" | number | z value of coordinate location of the Mission Start Button, which triggers an Event:MissionState message and often triggers equipment being given to the player
    

The same schema is used for the following maps : 

"Training"

"Competency"

"Falcon"

"Custom"