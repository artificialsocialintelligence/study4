# Working With Blockbench for Minecraft 1.11.2

Blockbench is a 3D modeling program used to generate various things for Minecraft. 
When making models the work flow is similar to working with any other modeling program so this document won't cover how to do that.
However, The goal of this readme is to tell how to best setup and prepare a workflow to add content into minecraft 1.11.2 with forge. 
Blockbench supports older versions of MC but there's a few extra steps to take. 

When working with Blockbench there's two workflows for basic items and then clothing/armor.

## Armor/Clothing

You can start working in one of two ways, 
Open the Example.bbmodel file in Blockbench and begin working or go through the setup.
I would recommend going through the project setup to become more familiar with Blockbench. 

#### Modeling Setup

The reason we set up the models in this particular way is when we export as a Base model .java file into MC we want to 
have all the models attached to the correct parts since 1.11.2 MC has to "build" the model using block coordinates.
Lastly, this will go over folder hierarchy so when you go to UV and move models to prep for texturing, it's much easier 
to know what parts go where and UV in a good way. 

1. Make a New Blockbench file by selecting "Minecraft Skin"
![](.MODELING_README_images/newfile.png)
2. Make sure to uncheck "Pose"
![](.MODELING_README_images/fileSettings.png)
3. Once it's created you'll need to parse through all the folders and rename them as follows 
(green arrows indicate what to change the name to, red lines indicate delete.)
![](.MODELING_README_images/renammed_tree.png)
4. Once You're finished, go to File > Convert Project, then in the "Format" section select "Modded Entity"
5. Now that your file is a Modded Entity, go to (File > Project...) and adjust the settings as follows...
![](.MODELING_README_images/projectsettings.png)
    - make sure you have the Export Version set to 1.7-1.13 (or whatever target Forge Versions you're planning to send your model to)
    - You can fill out "File Name" and adjust the texture if you feel you need space. Model Identifier can be left alone
6. Finally, When working making sure to create sub folders with appropriate names to keep all your models organized.
Here's an example.
![](.MODELING_README_images/armorExample.png)
    - make note of the naming conventions; `[single word description]Head`

#### Texture Setup 

This setup is quick but the reason we do this is that in the MC version 1.11.2 armor is layered into Upper Torso (Arms, head and legs) and Lower Legs
You still have to make sure the UV's don't overlap when adjusting the position of the UV's in the upper left. 
You can get away with using one texture If preferred but the workflow I used has 2 layers. 

1. In the bottom left you will see the Textures, right click on "steve.png" or whatever the first texture layer is there
and then go to properties, rename it to the name of your choice but add "_layer_1.png" to the end and change the 
rendered mode to layered. 
![](.MODELING_README_images/LayerNameExample.png)
2. Then make a new texture unchecking "Rearrange UV", Changing the "Type" to blank, 
Naming it the same as your previous texture with an appended "_layer_2.png" at the end and then hitting Confirm. 
Your Texture panel should look like this.
![](.MODELING_README_images/texturePanelFinished.png)
3. When Ready to paint select "Paint" in the upper right and then using brushes,
colors and making sure to swap between your layers you can texture your model. 

#### Exporting 

At this point you will want to save your whole project, so you don't lose progress so make sure to do File > Save Project As...
So you can return to your work in Blockbench whenever you need to.

1. Exporting and then Importing the armor.
   1. Click File -> Export -> Export Java Entity. 
   2. Rename the saved file how you see fit (preferable the same as the class name contained inside, this can be changed later) then append "_ModelBase" to the end
      3. Place this Java file into your project in your Java/ModName/items folder. 
         - Make Note of where this file is, you may need to revisit it to readjust specific blocks on the model since the export is not perfect
         - You will see lines of code that look like this
         `armorLeftArm.cubeList.add(new ModelBox(armorLeftArm, 75, 47, 0.0F, -3.0F, -2.0F, 4, 1, 4, 0.0F, false));`
         - the ModelBox() class being used here are parts of your built model. 
         - Model box takes in (U, V, x, y, z, dx, dy, dz, delta, mirror) the following table explains it's Type and use

         | ModelBox Input | Type    | Usage                                                                                                                                                                                     |
         |----------------|---------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
         | U              | int     | position on the texture map where this block sits in the horizontal direction                                                                                                             |
         | V              | int     | position on the texture map where this block sits in the vertical direction                                                                                                               |
         | x              | float   | position of the block in the Left/Right position on the model, (if it has a parent it's from the origin of the parent)                                                                    |
         | y              | float   | position of the block in the Up/Down position of the model, (parent is origin)                                                                                                            |
         | z              | float   | position of the block in the Forward/Back position of the model, (parent is origin)                                                                                                       |
         | dx             | int     | Size of the block in the left/right direction (width)                                                                                                                                     |
         | dy             | int     | Size of the block in the Up/Down Direction (height)                                                                                                                                       |
         | dz             | int     | Size of the block in the Forward/Back Direction (Length)                                                                                                                                  |
         | delta          | float   | Not sure what this does, set to 0.0f                                                                                                                                                      |
         | mirror         | boolean | This flips all the values in the block I presume, however the export from Blockbench just does Separate ModelBlocks. Could use for model optimization if wanted. Otherwise, set to false. |

2. Exporting and then Importing the Textures.
   1. Click the Save Icon next to the textures in the bottom Left Textures Panel. 
   2. Then place your textures into resources/(modName)/textures/models/armor/items
   3. Finally, you will need to create Item textures for the armor so when it's in the crafting menus and in the user's hot bar you can see the item.
      - I would recommend locating some base textures from minecraft for this and just adjust the color for them. You can find examples here `ModSource/forge-(version)/src/resources/assets/asistmod/textures/items/item_bombppe_leggings.png`
3. Setting up the Armor for Scripting. 
   - This final step ties everything together, and it's CRITICAL you didn't miss a step above, and if you're sure you didn't miss a step, and it still isn't working please email me at jreynolds@aptima.com and I will do what I can to help. 
   1. Add a new Item to your project in your items folder naming it what you please following the naming conventions for your project and Inheriting from "ItemArmor". But make sure it's different from the "ModelBase" file you Added in step 1 earlier
   2. Item Armor has 4 input variables for its constructor (Armor Material, textureLayer, EquipmentSlot, itemTextureName)

        | Variable Name                  | Type                     | Usage                                                                                                                                                                                                                                                                                     |
        |--------------------------------|--------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
        | Armor Material (p_i46750_1_)   | enum ArmorMaterial       | This is the material, you can use the default ones minecraft provides or build your own, see ArmorMaterial enums for more.                                                                                                                                                                |
        | Texture Layer (p_i46750_2_)    | int                      | If you remember above I said you can setup multiple layers for your MC textures? Well this is where you set what layer the model should reference. Typically, Head and Chest are on layer 1, with Leggings and boots on layer 2                                                           |
        | EquipmentSlot (p_i46750_3_)    | enum EntityEquipmentSlot | This is the corresponding slot that armor is associated with. Depending on what armor parts you included in your armor, you could have anywhere between 1-4 registration entries. Each of these entries will need you to set this variable for each (Helmet, Chestplate, Leggings, Boots) |
        | textureName (unloacalizedName) | string                   | This is the associated ITEM texture, not the layered model texture. So be sure when registering the items you register them with the variable above as "Helmet" and this should reference the item_helmet.png picture.                                                                    |
      - Below you will find an example of how this is used.
      ![](.MODELING_README_images/armor setup.png)
   3. Finally, You need to set-up the "ModelBase" As long as you set things up with the proper hierarchy you should be able to add a few lines of code.
   ![](.MODELING_README_images/ModelBaseSetup.png)
      - This just associates the proper model with the right parts of the player and when to show them.
      
NOTE: One big note about this is you may have to run the client a few times and check the position of the models, they may be off, if that's the case play around with the x, y, z values in the ModelBase file to line everything up as intended. 

## Items
This process is much easier than the Armor process.
#### Modeling Setup
1. Open blockbench and make a new file selecting "Java Block/Item"
2. Once Open, Start modeling your intended block
3. Make sure to UV the blocks you place in the way you want so texturing is easier later on. 

#### Texture Setup

There's no additional steps for this, the texture should already be added for you, I'm sure there's a workflow for layers but usually one texture is preferred for optimization.
Just make sure in the modeling setup while modeling you UV periodically to lay out the blocks the way you want.

NOTE: You can also create a new texture and say "Rearrange UV" and hope the auto UV method will give you something nice to paint.
more often than not it doesn't so manually UV-ing is recommended. 

#### Exporting

1. Click "File > Export > Export Block/Item Model"
2. Move the exported Json into the associated folder, either resources/.../items or resources/.../blocks
3. Make an Item file like you would for any other Item and when you register the item use the .json file you
put into either /items or /blocks in the previous step
