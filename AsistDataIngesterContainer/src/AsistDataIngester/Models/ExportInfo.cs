﻿using System;
namespace AsistDataIngester.Models
{
    public class ExportInfo
    {
        public string ProgressId { get; set; }
        public double PercentComplete { get; set; }
        public string Status { get; set; }
    }
}
