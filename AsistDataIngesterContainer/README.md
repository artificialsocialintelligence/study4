# Asist Data Ingester

## Introduction

The ASIST Data Ingester has two main functions: 
- Monitoring Qualtrics for new survey responses and sending these survey results over a specified message topic ("surveyResponseTopic" in appsettings.json)
- Receive Minecraft client map audio data from the browser over a web socket connection and forward it to a specified endpoint
    - alternatively this service could connect to Google's Cloud Speech to Text technology to extract text from the audio stream and forward this over a message bus topic ("userSpeechTopic")

## Appsettings.json

```json
  "Mqtt": {
    "host": "host.docker.internal",
    "port": "1883",
    "clientID": "AsistDataIngester",
    "surveyResponseTopic": "status/asistdataingester/surveyresponse",
    "userSpeechTopic": "status/asistdataingester/userspeech",
    "heartBeatTopic": "status/asistdataingester/heartbeats",
    "controlTopic": "control/asistdataingester",
    "getTrialInfoTopic": "control/request/getTrialInfo",
    "trialTopic": "trial"
  },

  "SignalR": {
    "TextMethod": "ReceiveText",
    "AudioEndPoint": "/audio"
  },

  "App": {
    "QualtricsAPIToken": "BFzUdjstVdryWHXFUopQnqu85iBTMa4xK7pN0MD9",
    "DisableSpeechToText":  false,
    "ExternalSpeechProcessor": "ws://localhost:8888",
    "SaveAudioFiles": false
  },
```

| Key  | Description |
|---|---|
| surveyResponseTopic  | Message bus topic to send qualtrics survey responses to  |
| userSpeechTopic  | Message bus topic to send Google speech to text results to  |
| DisableSpeechToText  | Boolean: should Google speech to text processing be disabled?  |
| ExternalSpeechProcessor | External location to forward audio data (wav) to   |
| SaveAudioFiles  | Boolean: should audio data be saved to a file?  |

## General Instructions

### Building Docker container
- Navigate to the root (top level) of the AsistDataIngesterContainer directory, this is the same level that the dockerfile appears in
- Open a shell window and run *docker build -t asistdataingester:latest .*

### Updating Docker Container After a New Release --> Building From Source
- Navigate to the root (top level) of the AsistDataIngesterContainer directory, this is the same level that the dockerfile appears in
- Open a shell window in this directory and run *docker build -t asistdataingester:latest --build-arg CACHE_BREAKER=somestring .*
- You can replace the word "somestring" with any string of your choice, as long as it is unique each time
- This build is significantly faster, as the majority of the image layers are already built

### Updating Docker Container After a New Release --> Pulling New Image 
- Open a shell window and run docker login --username foo --password bar. Replace foo and bar with your username and password.
- Then run docker pull gcr.io/asist-2130/asistdataingester:latest
- This should update your current asistdataingester:latest image, correctly altering only the image layers that have changed

### Configuration
- You must configure the MQTT Host before using the container. This can be done in the Local/AsistDataIngester/appsettings.json file. Navigate to the Mqtt section of the file. You should see 2 values that look like this -->  "host" and "port". In the field labeled "host", enter the IP address of the machine running the ELk Stack like so : "host":"192.168.0.189" . Similarly, in the field labeled "port", enter the port number of the exposed broker in the ELK Stack like so : "port":"1883". This port number should almost always be 1883.
