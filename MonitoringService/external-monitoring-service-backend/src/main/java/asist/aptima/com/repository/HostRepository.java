package asist.aptima.com.repository;

import java.util.List;
import java.util.Optional;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import asist.aptima.com.domain.Host;
import asist.aptima.com.utility.SortingAndOrderArgumentsHost;

public interface HostRepository {

    Optional<Host> findById(long id);    
	Optional<Host> findByHostIp(String name);
    void deleteById(long id);
    void deleteByHostIp(String hostIp);
    List<Host> findAll(@NotNull SortingAndOrderArgumentsHost args);
    Host save(@NotBlank Host host);
//	Host save(@NotBlank Host host, @NotBlank Set<Service> services);
	Host update(@NotBlank Host host);
//	int update(long id, @NotBlank Host host, @NotBlank Set<Service> services);
}
