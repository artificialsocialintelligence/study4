package asist.aptima.com.model.model;

public enum HeartbeatStateType {
	ok, warning, error
}
