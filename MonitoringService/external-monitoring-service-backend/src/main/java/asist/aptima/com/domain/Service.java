package asist.aptima.com.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.micronaut.serde.annotation.Serdeable;


@Serdeable
@Entity
@Table(name = "service", uniqueConstraints={
	    @UniqueConstraint(columnNames = {"source", "host_fk"}) })
public class Service {
	
	private static final Logger logger = LoggerFactory.getLogger(Service.class);
	
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @JsonIgnore
    private Long id;

    @Version
    @JsonIgnore
    private Long version;

    @NotNull
    @Column(name="source", nullable=false)
    private String source;

    @NotNull
    @Column(name="timestamp", nullable=false)
    private String timestamp;
    
    @NotNull
    @Column(name="message_type", nullable=false)
    @JsonProperty("message_type")
    private String messageType;
    
    @NotNull
    @Column(name="sub_type", nullable=false)
    @JsonProperty("sub_type")
    private String subType;
    
    @NotNull
    @Column(name="state", nullable=false)
    private String state;
    
    @NotNull
    @Column(name="topic", nullable=false)
    private String topic;
    
    @ManyToOne
    @JsonBackReference
    @JoinColumn(name="host_fk")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Host host;

    public Service() {}

    public Service(
    		@NotNull String source,
    		@NotNull String timestamp,
    		@NotNull String messageType,
    		@NotNull String subType,
    		@NotNull String state,
    		@NotNull String topic
                ) {
        this.source = source;
        this.timestamp = timestamp;
        this.messageType = messageType;
        this.subType = subType;
        this.state = state;
        this.topic = topic;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
    
    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }
    
    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }
    
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
    
    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }
    
    public Host getHost() {
        return host;
    }

    public void setHost(Host host) {
        this.host = host;
    }

    @Override
    public String toString() {
        return "{" +
                "\"id\": \"" + id + "\"," +
                "\"source\": \"" + source + "\"," +
                "\"timestamp\": \"" + timestamp + "\"," +
                "\"message_type\": \"" + messageType + "\"," +
                "\"sub_type\": \"" + subType + "\"," +
                "\"state\": \"" + state + "\"," +
                "\"topic\": \"" + topic + "\"," +
                "\"host\": \"" + host + "\"" +
                "}";
    }
}
