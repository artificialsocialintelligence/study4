package asist.aptima.com.repository;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import asist.aptima.com.configuration.ApplicationConfiguration;
import asist.aptima.com.domain.Service;
import asist.aptima.com.utility.SortingAndOrderArgumentsService;
import io.micronaut.transaction.annotation.ReadOnly;
import jakarta.inject.Singleton;

@Singleton 
public class ServiceRepositoryImpl implements ServiceRepository {
	
	private static final Logger logger = LoggerFactory.getLogger(ServiceRepositoryImpl.class);

    private static final List<String> VALID_PROPERTY_NAMES = Arrays.asList("id", "source", "timestamp", "message_type", "sub_type", "state");

    private final EntityManager entityManager;  
    private final ApplicationConfiguration applicationConfiguration;

    public ServiceRepositoryImpl(EntityManager entityManager, ApplicationConfiguration applicationConfiguration) {
        this.entityManager = entityManager;
        this.applicationConfiguration = applicationConfiguration;
    }

    @Override
    @ReadOnly 
    public Optional<Service> findById(long id) {
        return Optional.ofNullable(entityManager.find(Service.class, id));
    }
    
    @Override
    @ReadOnly 
    public Optional<Service> findBySource(String source) {
    	try {
    		return entityManager.createQuery("SELECT s FROM Service as s WHERE s.source = :source", Service.class)
    				.setParameter("source", source)
    				.getResultList()
    				.stream()
    				.findFirst();
    	} catch (Exception e) {
    		logger.error(e.getMessage());
    		return Optional.empty();
    	}
    }

    @Override
    @Transactional 
    public Service save(@NotBlank Service service) {
    	try {            
            entityManager.persist(service);
            return service;
    	} catch (Exception e) {
    		logger.error(e.getMessage());
    		return null;
    	}

    }

    @Override
    @Transactional 
    public void deleteById(long id) {
        findById(id).ifPresent(entityManager::remove);
    }

    @ReadOnly 
    public List<Service> findAll(@NotNull SortingAndOrderArgumentsService args) {
        String qlString = "SELECT s FROM Service as s";
        if (args.getOrder().isPresent() && args.getSort().isPresent() && VALID_PROPERTY_NAMES.contains(args.getSort().get())) {
            qlString += " ORDER BY s." + args.getSort().get() + ' ' + args.getOrder().get().toLowerCase();
        }
        TypedQuery<Service> query = entityManager.createQuery(qlString, Service.class);
        query.setMaxResults(args.getMax().orElseGet(applicationConfiguration::getMax));
        args.getOffset().ifPresent(query::setFirstResult);

        return query.getResultList();
    }

    @Override
    @Transactional 
    public Service update(@NotBlank Service service) {
    	try {
    		return entityManager.merge(service);
    	} catch (Exception e) {
    		logger.error(e.getMessage());
    		return null;
    	}
    }
}
