package asist.aptima.com.model.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.serde.annotation.Serdeable;

@Serdeable
public class DataStateContainer {

	@JsonProperty("container_id")
    private String containerId;

    @JsonProperty("container_name")
    private String containerName;

    @JsonProperty("image_id")
    private String imageId;

    @JsonProperty("status")
    private String status;

    @JsonProperty("state")
    private String state;

    @JsonProperty("created")
    private String created;

	@JsonProperty("timestamp")
    private String timestamp;

	@JsonProperty("cpu_percent")
    private String cpuPercent;

	@JsonProperty("mem_percent")
    private String memPercent;

	@JsonCreator
	public DataStateContainer(
    		String containerId,
            String containerName,
            String imageId,
            String status,
            String state,
            String created,
    		String timestamp,
    		String cpuPercent,
    		String memPercent
			) {
        this.containerId = containerId;
        this.containerName = containerName;
        this.imageId = imageId;
        this.status = status;
        this.state = state;
        this.created = created;
        this.timestamp = timestamp;
        this.cpuPercent = cpuPercent;
        this.memPercent = memPercent;
	}
	
    public String getContainerId() {
        return containerId;
    }

    public void setContainerId(String containerId) {
        this.containerId = containerId;
    }

    public String getContainerName() {
        return containerName;
    }

    public void setContainerName(String containerName) {
        this.containerName = containerName;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
    
    public String getCpuPercent() {
        return cpuPercent;
    }

    public void setCpuPercent(String cpuPercent) {
        this.cpuPercent = cpuPercent;
    }
    
    public String getMemPercent() {
        return memPercent;
    }

    public void setMemPercent(String memPercent) {
        this.memPercent = memPercent;
    }

}
