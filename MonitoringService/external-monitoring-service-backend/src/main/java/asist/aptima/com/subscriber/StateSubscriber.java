package asist.aptima.com.subscriber;

import asist.aptima.com.domain.Container;
import asist.aptima.com.domain.Host;
import asist.aptima.com.domain.Service;
import asist.aptima.com.model.DataInstanceInfo;
import asist.aptima.com.model.DataInstanceState;
import asist.aptima.com.model.MessageState;
import asist.aptima.com.repository.ContainerRepository;
import asist.aptima.com.repository.HostRepository;
import asist.aptima.com.repository.ServiceRepository;
import io.micronaut.context.annotation.Property;
import io.micronaut.mqtt.annotation.MqttSubscriber;
import io.micronaut.mqtt.annotation.Topic;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import io.micronaut.serde.ObjectMapper;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

@ExecuteOn(TaskExecutors.IO)
@MqttSubscriber
public class StateSubscriber {

    private static final Logger logger = LoggerFactory.getLogger(StateSubscriber.class);
    private final ObjectMapper objectMapper;
    private final HostRepository hostRepository;
    private final ServiceRepository serviceRepository;
    private final ContainerRepository containerRepository;

    @Property(name = "jobs.testbedsState.timeout")
    private Duration TESTBED_STATE_TIMEOUT;

    private final Map<String, Disposable> timers = new ConcurrentHashMap<>();
    private final Map<String, Subject<Integer>> subjects = new ConcurrentHashMap<>();

    public StateSubscriber(ObjectMapper objectMapper, HostRepository hostRepository, ServiceRepository serviceRepository, ContainerRepository containerRepository) {
        this.objectMapper = objectMapper;
        this.hostRepository = hostRepository;
        this.serviceRepository = serviceRepository;
        this.containerRepository = containerRepository;
    }

    @Topic("state/+")
    public void receive(byte[] data, @Topic String topic) {
        MessageState messageState;
        try {
            if (topic.contains("/")) {
                String hostIp = topic.split("/")[1];
                if (subjects.containsKey(hostIp)) {
                    Subject<Integer> subject = subjects.remove(hostIp);
                    subject.onNext(0);
                    subject.onComplete();
                    if (timers.containsKey(hostIp)) {
                        Disposable disposable = timers.remove(hostIp);
                        disposable.dispose();
                    }
                }
                Subject<Integer> destroy = PublishSubject.create();
                Disposable disposable = Observable.timer(TESTBED_STATE_TIMEOUT.getSeconds(), TimeUnit.SECONDS)
                        .takeUntil(destroy)
                        .subscribe(t -> {
                            logger.info("Deleting host: [{}]", hostIp);
                            hostRepository.deleteByHostIp(hostIp);
                        });
                subjects.put(hostIp, destroy);
                timers.put(hostIp, disposable);
                String message = new String(data, StandardCharsets.UTF_8);
                messageState = objectMapper.readValue(message, MessageState.class);
                logger.info("Received state from: {}", hostIp);
                Optional<Host> host = hostRepository.findByHostIp(hostIp);
                if (host.isEmpty()) {
                    logger.debug("Adding new host from [{}] : [{}] at [{}]", messageState.getData().getHostIp(), messageState.getData().getState(), messageState.getHeader().getTimestamp());
                    Host newHost = new Host(hostIp, messageState.getData().getState().toString());
                    if (messageState.getData().getInstanceInfo() != null) {
                        newHost.setInstanceInfo(objectMapper.writeValueAsString(messageState.getData().getInstanceInfo()));
                    } else {
                        newHost.setInstanceInfo(objectMapper.writeValueAsString(new ArrayList<DataInstanceInfo>()));
                    }
                    if (messageState.getData().getInstanceState() != null) {
                        newHost.setInstanceState(objectMapper.writeValueAsString(messageState.getData().getInstanceState()));
                    } else {
                        newHost.setInstanceState(objectMapper.writeValueAsString(new DataInstanceState()));
                    }
                    Host savedHost = hostRepository.save(newHost);
                    Set<Service> services = new HashSet<>();
                    if (messageState.getData().getServices() != null) {
                        messageState.getData().getServices().forEach((key, value) -> {
                            Service newService = new Service(
                                    value.getSource(),
                                    value.getTimestamp(),
                                    value.getMessageType(),
                                    value.getSubType(),
                                    value.getState().toString(),
                                    value.getTopic());
                            newService.setHost(savedHost);
                            Service savedService = serviceRepository.save(newService);
                            services.add(savedService);
                        });
                    }
                    newHost.setServices(services);
                    Set<Container> containers = new HashSet<>();
                    if (messageState.getData().getContainers() != null) {
                        messageState.getData().getContainers().forEach((key, value) -> {
                            Container newContainer = new Container(
                                    value.getContainerId(),
                                    value.getContainerName(),
                                    value.getImageId(),
                                    value.getStatus(),
                                    value.getState(),
                                    value.getCreated(),
                                    value.getTimestamp(),
                                    value.getCpuPercent(),
                                    value.getMemPercent());
                            newContainer.setHost(savedHost);
                            Container savedContainer = containerRepository.save(newContainer);
                            containers.add(savedContainer);
                        });
                    }
                    newHost.setContainers(containers);
                    hostRepository.update(newHost);
                    logger.debug(this.objectMapper.writeValueAsString(savedHost));
                } else {
                    logger.debug("Updating host from [{}] : [{}] at [{}]", messageState.getData().getHostIp(), messageState.getData().getState(), messageState.getHeader().getTimestamp());
                    Host oldHost = host.get();
                    oldHost.getServices().forEach(service -> serviceRepository.deleteById(service.getId()));
                    oldHost.getContainers().forEach(container -> containerRepository.deleteById(container.getId()));
                    Set<Service> services = new HashSet<>();
                    messageState.getData().getServices().forEach((key, value) -> {
                        Service newService = new Service(
                                value.getSource(),
                                value.getTimestamp(),
                                value.getMessageType(),
                                value.getSubType(),
                                value.getState().toString(),
                                value.getTopic());
                        newService.setHost(oldHost);
                        services.add(newService);
                        serviceRepository.save(newService);
                    });
                    oldHost.setServices(services);
                    Set<Container> containers = new HashSet<>();
                    messageState.getData().getContainers().forEach((key, value) -> {
                        Container newContainer = new Container(
                                value.getContainerId(),
                                value.getContainerName(),
                                value.getImageId(),
                                value.getStatus(),
                                value.getState(),
                                value.getCreated(),
                                value.getTimestamp(),
                                value.getCpuPercent(),
                                value.getMemPercent());
                        newContainer.setHost(oldHost);
                        containers.add(newContainer);
                        containerRepository.save(newContainer);
                    });
                    oldHost.setContainers(containers);
                    oldHost.setState(messageState.getData().getState().toString());
                    if (messageState.getData().getInstanceInfo() != null) {
                        oldHost.setInstanceInfo(objectMapper.writeValueAsString(messageState.getData().getInstanceInfo()));
                    } else {
                        oldHost.setInstanceInfo(objectMapper.writeValueAsString(new ArrayList<DataInstanceInfo>()));
                    }
                    if (messageState.getData().getInstanceState() != null) {
                        oldHost.setInstanceState(objectMapper.writeValueAsString(messageState.getData().getInstanceState()));
                    } else {
                        oldHost.setInstanceState(objectMapper.writeValueAsString(new DataInstanceState()));
                    }
                    Host updatedHost = hostRepository.update(oldHost);
                    logger.debug(this.objectMapper.writeValueAsString(updatedHost));
                }
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            logger.error(e.getMessage());
        }
    }
}
