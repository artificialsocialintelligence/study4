package asist.aptima.com.controller;

import java.util.List;

import javax.validation.Valid;

import asist.aptima.com.domain.Host;
import asist.aptima.com.repository.HostRepository;
import asist.aptima.com.utility.SortingAndOrderArgumentsHost;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.scheduling.annotation.ExecuteOn;
import io.micronaut.scheduling.TaskExecutors;

@ExecuteOn(TaskExecutors.IO)  
@Controller("/hosts") 
public class HostController {

    private final HostRepository hostRepository;

    HostController(HostRepository hostRepository) { 
        this.hostRepository = hostRepository;
    }

    @Get("/{id}") 
    Host show(Long id) {
        return hostRepository
                .findById(id)
                .orElse(null); 
    }

    @Get(value = "/list{?args*}") 
    List<Host> list(@Valid SortingAndOrderArgumentsHost args) {
        return hostRepository.findAll(args);
    }
}
