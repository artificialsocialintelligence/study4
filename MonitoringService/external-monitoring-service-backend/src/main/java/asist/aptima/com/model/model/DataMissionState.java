package asist.aptima.com.model.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.serde.annotation.Serdeable;

@Serdeable
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataMissionState {

	@JsonProperty("mission_timer")
	private String missionTimer;
	@JsonProperty("elapsed_milliseconds")
	private String elapsedMilliseconds;
	@JsonProperty("mission")
	private String mission;
	@JsonProperty("mission_state")
	private String missionState;
	@JsonProperty("state_change_outcome")
	private String stateChangeOutcome;

	@JsonCreator
	public DataMissionState(
			String missionTimer,
			String elapsedMilliseconds,
			String mission,
			String missionState,
			String stateChangeOutcome
			) {
		this.missionTimer = missionTimer;
		this.elapsedMilliseconds = elapsedMilliseconds;
		this.mission = mission;
		this.missionState = missionState;
		this.stateChangeOutcome = stateChangeOutcome;
	}

	public DataMissionState() {
		// TODO Auto-generated constructor stub
	}

	public String getMissionTimer() {
		return missionTimer;
	}
	public void setMissionTimer(String missionTimer) {
		this.missionTimer = missionTimer;
	}

	public String getElapsedMilliseconds() {
		return elapsedMilliseconds;
	}
	public void setElapsedMilliseconds(String elapsedMilliseconds) {
		this.elapsedMilliseconds = elapsedMilliseconds;
	}

	public String getMission() {
		return mission;
	}
	public void setMission(String mission) {
		this.mission = mission;
	}

	public String getMissionState() {
		return missionState;
	}
	public void setMissionState(String missionState) {
		this.missionState = missionState;
	}

	public String getStateChangeOutcome() {
		return stateChangeOutcome;
	}
	public void setStateChangeOutcome(String stateChangeOutcome) {
		this.stateChangeOutcome = stateChangeOutcome;
	}
}
