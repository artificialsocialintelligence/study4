package asist.aptima.com.model.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.serde.annotation.Serdeable;
//import jakarta.json.bind.annotation.JsonbCreator;
//import jakarta.json.bind.annotation.JsonbProperty;

@Serdeable
public class DataStatusError {

	@JsonProperty("encoded_string")
	private String encodedString;
	
	@JsonCreator
	public DataStatusError(
			String encodedString
			) {
        this.encodedString = encodedString;
    }
	
	
	public String getEncodedString() {
		return encodedString;
	}
	public void setEncodedString(String encodedString) {
		this.encodedString = encodedString;
	}
}
