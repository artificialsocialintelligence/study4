package asist.aptima.com.model.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.serde.annotation.Serdeable;

@Serdeable
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataAutoExport {

	@JsonProperty("state")
	private AutoExportStateType state;

	@JsonCreator
	public DataAutoExport(
			AutoExportStateType state
			) {
		this.state = state;
	}

	public DataAutoExport() {
		// TODO Auto-generated constructor stub
	}

	public AutoExportStateType getState() {
		return state;
	}
	public void setState(AutoExportStateType state) {
		this.state = state;
	}

}
