package asist.aptima.com;

import org.slf4j.LoggerFactory;

import asist.aptima.com.appender.MqttAppender;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.filter.LevelFilter;
import ch.qos.logback.core.spi.FilterReply;
import io.micronaut.context.ApplicationContext;
import io.micronaut.runtime.Micronaut;

public class Application {
	private static String OS = System.getProperty("os.name").toLowerCase();
    public static boolean IS_WINDOWS = (OS.indexOf("win") >= 0);
    public static boolean IS_MAC = (OS.indexOf("mac") >= 0);
    public static boolean IS_UNIX = (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0);
    public static boolean IS_SOLARIS = (OS.indexOf("sunos") >= 0);
	
    public static void main(String[] args) {
    	ApplicationContext context = Micronaut.run(Application.class);

    	LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();    	
    	MqttAppender mqttAppender = context.getBean(MqttAppender.class);
    	mqttAppender.setContext(loggerContext);
    	
    	LevelFilter levelFilter = new LevelFilter();
    	levelFilter.setLevel(Level.ERROR);
    	levelFilter.setOnMatch(FilterReply.ACCEPT);
    	levelFilter.setOnMismatch(FilterReply.DENY);
    	levelFilter.start();                
    	mqttAppender.addFilter(levelFilter);    	
    	mqttAppender.start();
    	
    	Logger logbackLogger = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
    	logbackLogger.addAppender(mqttAppender);
    	logbackLogger.setAdditive(true);
    }
}