package asist.aptima.com.repository;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import io.micronaut.data.annotation.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import asist.aptima.com.configuration.ApplicationConfiguration;
import asist.aptima.com.domain.Host;
import asist.aptima.com.utility.SortingAndOrderArgumentsHost;
import io.micronaut.transaction.annotation.ReadOnly;
import jakarta.inject.Singleton;

@Singleton 
public class HostRepositoryImpl implements HostRepository {
	
	private static final Logger logger = LoggerFactory.getLogger(HostRepositoryImpl.class);

    private static final List<String> VALID_PROPERTY_NAMES = Arrays.asList("hostIp", "state", "services", "containers");

    private final EntityManager entityManager;  
    private final ApplicationConfiguration applicationConfiguration;

    public HostRepositoryImpl(EntityManager entityManager, ApplicationConfiguration applicationConfiguration) {
        this.entityManager = entityManager;
        this.applicationConfiguration = applicationConfiguration;
    }

    @Override
    @ReadOnly 
    public Optional<Host> findById(long id) {
        return Optional.ofNullable(entityManager.find(Host.class, id));
    }
    
    @Override
    @ReadOnly 
    public Optional<Host> findByHostIp(String hostIp) {
    	try {
    		return entityManager.createQuery("SELECT h FROM Host as h LEFT JOIN FETCH h.services LEFT JOIN FETCH h.containers WHERE h.hostIp = :hostIp", Host.class)
    				.setParameter("hostIp", hostIp)
    				.getResultList()
    				.stream()
    				.findFirst();
    	} catch (Exception e) {
    		logger.error(e.getMessage());
    		return Optional.empty();
    	}
    }

    @Override
    @Transactional 
    public Host save(Host host) {
    	try {
            entityManager.persist(host);
            return host;
    	} catch (Exception e) {
    		logger.error(e.getMessage());
    		return null;
    	}

    }

    @Override
    @Transactional 
    public void deleteById(long id) {
        findById(id).ifPresent(entityManager::remove);
    }

    @Override
    @Transactional
    public void deleteByHostIp(String hostIp) {
        entityManager.createQuery("DELETE FROM Host h WHERE h.hostIp = :hostIp")
                .setParameter("hostIp", hostIp)
                .executeUpdate();
    };

    @ReadOnly 
    public List<Host> findAll(@NotNull SortingAndOrderArgumentsHost args) {
        String qlString = "SELECT h FROM Host as h LEFT JOIN FETCH h.services LEFT JOIN FETCH h.containers";
        if (args.getOrder().isPresent() && args.getSort().isPresent() && VALID_PROPERTY_NAMES.contains(args.getSort().get())) {
            qlString += " ORDER BY h." + args.getSort().get() + ' ' + args.getOrder().get().toLowerCase();
        }
        TypedQuery<Host> query = entityManager.createQuery(qlString, Host.class);
        query.setMaxResults(args.getMax().orElseGet(applicationConfiguration::getMax));
        args.getOffset().ifPresent(query::setFirstResult);

        return query.getResultList();
    }

    @Override
    @Transactional 
    public Host update(@NotBlank Host host) {
    	try {
            entityManager.merge(host);
            return host;
    	} catch (Exception e) {
    		logger.error(e.getMessage());
    		return null;
    	}
    }
    
//    @Override
//    @Transactional 
//    public int update(@NotBlank Service heartbeat) {
//        return entityManager.createQuery("UPDATE Heartbeat h SET source = :source, timestamp = :timestamp, message_type = :message_type, sub_type = :sub_type, state = :state, topic = :topic where id = :id")
//                .setParameter("source", heartbeat.getSource())
//                .setParameter("timestamp", heartbeat.getTimestamp())
//                .setParameter("message_type", heartbeat.getMessageType())
//                .setParameter("sub_type", heartbeat.getSubType())
//                .setParameter("state", heartbeat.getState())
//                .setParameter("topic", heartbeat.getTopic())
//                .setParameter("id", heartbeat.getId())
//                .executeUpdate();
//    }
//    
//    @Override
//    @Transactional 
//    public int updateState(long id, @NotBlank String state) {
//        return entityManager.createQuery("UPDATE Heartbeat h SET state = :state where id = :id")
//                .setParameter("state", state)
//                .setParameter("id", id)
//                .executeUpdate();
//    }
}
