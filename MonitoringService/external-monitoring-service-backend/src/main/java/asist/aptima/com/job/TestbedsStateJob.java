package asist.aptima.com.job;

import java.io.IOException;
import java.time.Instant;
import java.util.*;

import asist.aptima.com.domain.Container;
import asist.aptima.com.model.*;
import io.micronaut.core.type.Argument;
import io.micronaut.core.type.GenericArgument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import asist.aptima.com.domain.Host;
import asist.aptima.com.domain.Service;
import asist.aptima.com.publisher.TestbedsStatePublisher;
import asist.aptima.com.repository.HostRepository;
import asist.aptima.com.utility.SortingAndOrderArgumentsHost;
import io.micronaut.context.annotation.Property;
import io.micronaut.scheduling.annotation.Scheduled;
import io.micronaut.serde.ObjectMapper;
import jakarta.inject.Singleton;

@Singleton
public class TestbedsStateJob {

	private static final Logger logger = LoggerFactory.getLogger(TestbedsStateJob.class);
	private TestbedsStatePublisher testbedsStatePublisher;
	private HostRepository hostRepository;
	private SortingAndOrderArgumentsHost hostSortingAndOrderArguments = new SortingAndOrderArgumentsHost(0, null, "hostIp", "asc");
	private ObjectMapper objectMapper;

	@Property(name = "external-monitoring-service-backend.schema.header.version")
	private String BACKEND_SCHEMA_HEADER_VERSION;

	@Property(name = "external-monitoring-service-backend.schema.header.messagetype")
	private String BACKEND_SCHEMA_HEADER_MESSAGETYPE;

	@Property(name = "external-monitoring-service-backend.schema.msg.version")
	private String BACKEND_SCHEMA_MSG_VERSION;

	@Property(name = "external-monitoring-service-backend.schema.msg.subtype")
	private String BACKEND_SCHEMA_MSG_SUBTYPE;

	@Property(name = "external-monitoring-service-backend.schema.msg.source")
	private String BACKEND_SCHEMA_MSG_SOURCE;

	@Property(name = "external-monitoring-service-backend.topic.publish")
	private String BACKEND_SUBSCRIBE_MQTT_TOPIC;

	private final String NOT_SET = "NOT_SET";

	public TestbedsStateJob(ObjectMapper objectMapper, TestbedsStatePublisher testbedsStatePublisher,
			HostRepository hostRepository) {
		this.objectMapper = objectMapper;
		this.testbedsStatePublisher = testbedsStatePublisher;
		this.hostRepository = hostRepository;
	}

	@Scheduled(fixedDelay = "${jobs.testbedsState.update.fixedDelay}", initialDelay = "${jobs.testbedsState.update.initialDelay}")
	void execute() {
		try {
			List<Host> hosts = Collections
					.synchronizedList(hostRepository.findAll(hostSortingAndOrderArguments));
			List<DataHost> dataHostList = new ArrayList<DataHost>();
			hosts.forEach(host -> {
				Set<Service> services = host.getServices();
				Map<String, DataService> dataServices = new HashMap<String, DataService>();
				services.forEach(service -> {
					DataService dataService = new DataService(
							service.getSource(),
							service.getTimestamp(),
							service.getMessageType(),
							service.getSubType(),
							Enum.valueOf(HeartbeatStateType.class, service.getState()),
							service.getTopic()
							);
					dataServices.put(service.getSource(), dataService);
				});
				Set<Container> containers = host.getContainers();
				Map<String, DataContainer> dataContainers = new HashMap<String, DataContainer>();
				containers.forEach(container -> {
					DataContainer dataContainer = new DataContainer(
							container.getContainerId(),
							container.getContainerName(),
							container.getImageId(),
							container.getStatus(),
							container.getState(),
							container.getCreated(),
							container.getTimestamp(),
							container.getCpuPercent(),
							container.getMemPercent()
					);
					dataContainers.put(container.getContainerName(), dataContainer);
				});
				List<DataInstanceInfo> dataInstanceInfos = new ArrayList<>();
				DataInstanceState dataInstanceState = new DataInstanceState();
				try {
					dataInstanceInfos = objectMapper.readValue(host.getInstanceInfo(), new GenericArgument<>() {
					});
//					dataInstanceInfos = objectMapper.readValue(host.getInstanceInfo(), Argument.listOf(DataInstanceInfo.class));
				} catch (IOException e) {
					logger.warn("InstanceInfo: {}", e.getMessage());
				}
				try {
					dataInstanceState = objectMapper.readValue(host.getInstanceState(), DataInstanceState.class);
				} catch (IOException e) {
					logger.warn("InstanceState: {}", e.getMessage());
				}
				DataHost dataHost = new DataHost(host.getHostIp(), Enum.valueOf(HeartbeatStateType.class, host.getState()), dataServices, dataContainers, dataInstanceInfos, dataInstanceState);
				dataHostList.add(dataHost);
			});
			DataHosts dataHosts = new DataHosts(dataHostList);
			Header header = new Header(Instant.now().toString(), BACKEND_SCHEMA_HEADER_MESSAGETYPE,
					BACKEND_SCHEMA_HEADER_VERSION);
			Msg msg = new Msg(BACKEND_SCHEMA_MSG_SUBTYPE, BACKEND_SCHEMA_MSG_SOURCE, NOT_SET, NOT_SET,
					BACKEND_SCHEMA_MSG_VERSION, null, null, null);
			MessageHosts messageState = new MessageHosts(header, msg, dataHosts);
			logger.debug("Sending {}: {}", BACKEND_SUBSCRIBE_MQTT_TOPIC, objectMapper.writeValueAsString(messageState));
			testbedsStatePublisher.send(BACKEND_SUBSCRIBE_MQTT_TOPIC, 2, objectMapper.writeValueAsBytes(messageState));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}
	}
}
