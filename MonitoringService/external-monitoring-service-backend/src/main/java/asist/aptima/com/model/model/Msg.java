package asist.aptima.com.model.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.serde.annotation.Serdeable;
//import jakarta.json.bind.annotation.JsonbCreator;
//import jakarta.json.bind.annotation.JsonbProperty;

@Serdeable
public class Msg {
	
	@JsonProperty("sub_type")
	private String subType;
	@JsonProperty("source")
	private String source;
	@JsonProperty("experiment_id")
	private String experimentId;
	@JsonProperty("trial_id")
	private String trialId;
	@JsonProperty("version")
	private String version;
	@JsonProperty("replay_id")
	private String replayId;
	@JsonProperty("replay_parent_id")
	private String replayParentId;
	@JsonProperty("replay_parent_type")
	private String replayParentType;
	
	@JsonCreator
	public Msg(
			String subType,
			String source,
			@Nullable String experimentId,
			@Nullable String trialId,
			String version,
			@Nullable String replayId,
			@Nullable String replayParentId,
			@Nullable String replayParentType
			) {
        this.subType = subType;
        this.source = source;
        this.experimentId = experimentId;
        this.trialId = trialId;
        this.version = version;
        this.replayId = replayId;
        this.replayParentId = replayParentId;
        this.replayParentType = replayParentType;
    }

	public String getSubType() {
		return subType;
	}
	public void setSubType(String subType) {
		this.subType = subType;
	}	
	
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	
	public String getExperimentId() {
		return experimentId;
	}
	public void setExperimentId(String experimentId) {
		this.experimentId = experimentId;
	}
	
	public String getTrialId() {
		return trialId;
	}
	public void setTrialId(String trialId) {
		this.trialId = trialId;
	}
	
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	
	public String getReplayId() {
		return replayId;
	}
	public void setReplayId(String replayId) {
		this.replayId = replayId;
	}

	public String getReplayParentId() {
		return replayParentId;
	}
	public void setReplayParentId(String replayParentId) {
		this.replayParentId = replayParentId;
	}
	
	public String getReplayParentType() {
		return replayParentType;
	}
	public void setReplayParentType(String replayParentType) {
		this.replayParentType = replayParentType;
	}
}
