# InternalMonitoringService

Description of the internal-monitoring-service env variables.

## APP_PORT
The port the application listens to for HTTP.
Several end-points are available:
- /heartbeats/id
    - get geartbeat using id
      - **id** long
- /heartbeats/list?args
    - get all heartbeats
      - **offset** int
      - **max** int
      - **sort** string (id|source)
      - **order** string (asc|ASC|desc|DESC)
- /health

## ERROR_MQTT_TOPIC

The MQTT topic for publishing error messages. For example `status/error`.

## ERROR_SCHEMA_HEADER_MESSAGETYPE

The header message_type to use in the error messages. For example `error`.

## ERROR_SCHEMA_HEADER_VERSION

The header version number to use in the error messages. For example `1.2`.

## ERROR_SCHEMA_MSG_SOURCEE

The msg source to use in the error messages. For example `InternalMonitoringService`.

## ERROR_SCHEMA_MSG_SUBTYPE

The msg sub_type to use in the error messages. For example `host`.

## ERROR_SCHEMA_MSG_VERSION

The msg version number to use in the error messages. For example `0.7`.

## EXTERNAL_MONITORING_SERVICE_UPDATE_FIXED_DELAY

The fixed delay to publish the aggregated state (heartbeats) of all services that have reported in. For example `5s` for a 5 second interval.

## EXTERNAL_MONITORING_SERVICE_UPDATE_INITIAL_DELAY

The initial delay to wait prior to starting to publish the aggregated state (heartbeats) of all services that have reported in. For example `5s` to wait an initial 5 seconds.

## EXTERNAL_MQTT_SERVER_HOST

The ip address of the **external** MQTT host, for example `10.0.0.1`.

## EXTERNAL_MQTT_SERVER_PORT

The port number of the **external** MQTT host, for example `1884`.

## EXTERNAL_MQTT_TOPIC

The first portion of the external MQTT topic for publishing state. The second portion is dynamically concatenated in code. For example `state`, **state\AGENT_01**.

## EXTERNAL_SCHEMA_HEADER_MESSAGETYPE

The header message_type to use in the external state message. For example `state`.

## EXTERNAL_SCHEMA_HEADER_VERSION

The header version number to use in the external state message. For example `1.2`.

## EXTERNAL_SCHEMA_MSG_SOURCE

The msg source to use in the external state message. For example `InternalMonitoringService`.

## EXTERNAL_SCHEMA_MSG_SUBTYPE

The msg sub_type to use in the external state message. For example `host`.

## EXTERNAL_SCHEMA_MSG_VERSION

The msg version number to use in the external state message. For example `0.7`.

## HEARTBEAT_COUNTDOWN_PERIOD

The timer count down to use for sending state the external-monitoring-service. For example `10s`.

## HOST_IP

The IP address of the internal-monitoring-service host machine. Used so the external-monitoring-service knows the identity of the message sender. In production this should be set by the adminless testbed up script. For example `192.168.0.1`

## HTTP_CLIENT_READTIMEOUT

The timeout for HTTP reads. For example `3m`.

## MQTT_BROKER_URL

The internal testbed MQTT broker url. Used for listening to heart beats sent from services. For example `tcp://host.docker.internal:1883`.

## MQTT_CLEAN_SESSION

The clean session value to use for the MQTT client. For example `true`.