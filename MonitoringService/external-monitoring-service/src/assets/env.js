(function(window) {
  window['env'] = window['env'] || {};

  // Environment variables
  window['env']['mqttHost'] = 'localhost';
  window['env']['mqttPort'] = 9002;
  window['env']['mqttProtocol'] = 'ws';
  window['env']['mqttPath'] = '/ws';
  window['env']['stateCombinedTopic'] = 'testbeds/state';
  window['env']['stateCombinedPeriod'] = 10000;
  window['env']['testbedTimeout'] = 30000;
})(this);
