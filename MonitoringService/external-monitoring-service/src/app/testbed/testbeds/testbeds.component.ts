import {Component, OnDestroy, OnInit} from '@angular/core';
import {StateService} from '../state/state.service';
import {Observable, of, Subscription, timer} from 'rxjs';
import {Testbeds} from './testbeds';
import {MatSelectionListChange} from '@angular/material/list';
import {MqttService} from 'ngx-mqtt';
import {environment} from '../../environments/environment';
import {InstanceInfo} from '../instance-panel/InstanceInfo';

@Component({
  selector: 'app-testbeds',
  templateUrl: './testbeds.component.html',
  styleUrls: ['./testbeds.component.scss']
})
export class TestbedsComponent implements OnInit, OnDestroy {
  title: string;
  public testbeds: Testbeds;
  private testbedSubscription: Subscription;
  public hostIps: string[];

  constructor(
    private stateService: StateService,
    private mqttService: MqttService,
    ) { }

  ngOnInit(): void {
    this.title = 'Testbeds';
    this.hostIps = [];
    this.testbedSubscription = this.stateService.testbeds.subscribe(testbeds => {
      this.testbeds = JSON.parse(testbeds) as Testbeds;
      this.hostIps = Object.keys(this.testbeds).sort(this.customComparator);
    });
    // Commenting this out for now. Publishing this on the external-monitoring-service-backend
    // this.publishTimer();
  }

  ngOnDestroy(): void {
    this.testbedSubscription.unsubscribe();
  }

  private customComparator(a, b) {
    // Breaking into the octets
    const octetsA = a.split('.');
    const octetsB = b.split('.');

    // Condition if the IP Address
    // is same then return 0
    if (octetsA === octetsB) {
      return 0;
    }
    else if (octetsA[0] > octetsB[0]) {
      return 1;
 }
    else if (octetsA[0] < octetsB[0]) {
      return -1;
 }
    else if (octetsA[1] > octetsB[1]) {
      return 1;
 }
    else if (octetsA[1] < octetsB[1]) {
      return -1;
 }
    else if (octetsA[2] > octetsB[2]) {
      return 1;
 }
    else if (octetsA[2] < octetsB[2]) {
      return -1;
 }
    else if (octetsA[3] > octetsB[3]) {
      return 1;
 }
    else if (octetsA[3] < octetsB[3]) {
      return -1;
 }
  }

  public mapClass(hostIps: string) {
    switch (this.testbeds[hostIps].state) {
      case 'ok':
        return 'state-ok';
      case 'warning':
        return 'state-warning';
      case 'error':
        return 'state-error';
      default:
        return 'state-error';
    }
  }

  public testbedSelectionChanged($event: MatSelectionListChange) {
    this.stateService.setSelectedTestbed($event.source.selectedOptions.selected[0].value);
  }

  publishTimer(): void {
    const source = timer(0, environment.stateCombinedPeriod);
    source.subscribe(val => {
      if (Object.keys(this.testbeds).length) {
        console.log(`Publishing combined state to ${environment.stateCombinedTopic}.`);
        this.mqttService.unsafePublish(environment.stateCombinedTopic, JSON.stringify(this.testbeds), {qos: 2});
        // this.mqttService.publish(environment.stateCombinedTopic, JSON.stringify(this.testbeds), {qos: 2}).pipe(
        //   tap(_ => {
        //     console.log(`Published combined state to ${environment.stateCombinedTopic}.`);
        //   }),
        //   catchError(this.handleError<void>('publishTimer'))
        // );
      } else {
        console.log(`Combined testbed state is empty.`);
      }
    });
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private newInstanceInfo(): InstanceInfo {
    return {} as InstanceInfo;
  }
}
