import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {StateService} from '../state/state.service';
import {Subscription} from 'rxjs';
import {Services} from '../services/services';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import {Details} from './details';
import { MatSort } from '@angular/material/sort';
import {Detail} from './detail';
import {MediaChange, MediaObserver} from '@angular/flex-layout';
import {filter, map} from 'rxjs/operators';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit, OnDestroy {
  title: string;
  selectedTestbed: string;
  selectedService: string;
  details: Details;
  detailValues: Detail[];
  dataSource = new MatTableDataSource(this.detailValues);
  displayedColumns: string[];

  currentScreenWidth = '';
  flexMediaWatcher: Subscription;

  private selectedTestbedSubscription: Subscription;
  private selectedServiceSubscription: Subscription;

  @ViewChild(MatTable) table: MatTable<Detail>;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private stateService: StateService,
    private mediaObserver: MediaObserver
    ) {
    this.flexMediaWatcher = mediaObserver.asObservable()
      .pipe(
        filter((changes: MediaChange[]) => changes.length > 0),
        map((changes: MediaChange[]) => changes[0])
      ).subscribe((change: MediaChange) => {
        if (change.mqAlias !== this.currentScreenWidth) {
          this.currentScreenWidth = change.mqAlias;
          this.setupTable();
        }
      });
  }

  ngOnInit(): void {
    this.title  = 'Details';
    this.selectedTestbedSubscription = this.stateService.selectedTestbed.subscribe(selectedTestbed => {
      if (this.selectedTestbed !== selectedTestbed) {
        this.selectedService = '';
        this.detailValues = [];
        this.selectedTestbed = selectedTestbed;
        this.dataSource.data = this.detailValues;
        this.dataSource.sort = this.sort;
      }
    });

    this.selectedServiceSubscription = this.stateService.selectedService.subscribe(selectedService => {
      this.detailValues = [];
      this.selectedService = selectedService;
      this.details = JSON.parse(this.stateService.getDetails(this.selectedTestbed, this.selectedService)) as Details;
      const detailKeys = Object.keys(this.details).sort();
      detailKeys.forEach(key => {
        this.detailValues.push({
          name: key,
          value: this.details[key]}
        );
      });

      this.dataSource.data = this.detailValues;
      this.dataSource.sort = this.sort;
    });
    // if (this.table) {
    //   this.table.renderRows();
    // }
  }

  ngOnDestroy(): void {
    this.selectedTestbedSubscription.unsubscribe();
    this.selectedServiceSubscription.unsubscribe();
  }

  setupTable() {
    this.displayedColumns = ['name', 'value'];
    if (this.currentScreenWidth === 'xs') {
      this.displayedColumns = ['name', 'value'];
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
