import {Component, OnDestroy, OnInit} from '@angular/core';
import {StateService} from '../state/state.service';
import {Subscription} from 'rxjs';
import {Testbeds} from '../testbeds/testbeds';
import {Services} from './services';
import {MatSelectionListChange} from '@angular/material/list';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit, OnDestroy {
  title: string;
  private services: Services;
  selectedTestbed: string;
  public serviceNames: string[];

  private selectedTestbedSubscription: Subscription;

  constructor(private stateService: StateService) { }

  ngOnInit(): void {
    this.title = 'Services';
    this.serviceNames = [];
    this.selectedTestbedSubscription = this.stateService.selectedTestbed.subscribe(selectedTestbed => {
      this.selectedTestbed = selectedTestbed;
      this.services = JSON.parse(this.stateService.getServices(this.selectedTestbed)) as Services;
      this.serviceNames = Object.keys(this.services).sort();
    });
  }

  ngOnDestroy(): void {
    this.selectedTestbedSubscription.unsubscribe();
  }

  public mapClass(serviceName: string) {
    switch (this.services[serviceName].state) {
      case 'ok':
        return 'state-ok';
      case 'warning':
        return 'state-warning';
      case 'error':
        return 'state-error';
      default:
        return 'state-error';
    }
  }

  public serviceSelectionChanged($event: MatSelectionListChange) {
    this.stateService.setSelectedService($event.source.selectedOptions.selected[0].value);
  }

}
