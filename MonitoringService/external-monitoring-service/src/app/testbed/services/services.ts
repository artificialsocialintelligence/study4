import {Details} from '../details/details';

export interface Services {
  [name: string]: Details;
}
