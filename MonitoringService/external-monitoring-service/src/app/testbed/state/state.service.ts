import { Injectable } from '@angular/core';
import {StateMessage} from '../state-message';
import {Testbeds} from '../testbeds/testbeds';
import {BehaviorSubject, Subject, Subscription, timer} from 'rxjs';
import {Details} from '../details/details';
import {Properties} from '../properties/properties';
import {InstanceInfo} from '../instance-panel/InstanceInfo';
import {ClientInfo} from '../instance-panel/ClientInfo';
import {InstanceState} from '../instance-panel/InstanceState';
import {repeatWhen, takeUntil} from 'rxjs/operators';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StateService {

  private testbedsObj: Testbeds = {};
  private testbedsSubject: BehaviorSubject<string> = new BehaviorSubject<string>(JSON.stringify(this.testbedsObj));
  public testbeds = this.testbedsSubject.asObservable();

  private selectedTestbedStr = '';
  private selectedTestbedSubject: BehaviorSubject<string> = new BehaviorSubject<string>(this.selectedTestbedStr);
  public selectedTestbed = this.selectedTestbedSubject.asObservable();

  private selectedServiceStr = '';
  private selectedServiceSubject: BehaviorSubject<string> = new BehaviorSubject<string>(this.selectedServiceStr);
  public selectedService = this.selectedServiceSubject.asObservable();

  private selectedContainerStr = '';
  private selectedContainerSubject: BehaviorSubject<string> = new BehaviorSubject<string>(this.selectedContainerStr);
  public selectedContainer = this.selectedContainerSubject.asObservable();

  private selectedInstanceInfoIndex = 0;
  private selectedInstanceInfoSubject: BehaviorSubject<number> = new BehaviorSubject<number>(this.selectedInstanceInfoIndex);
  public selectedInstanceInfo = this.selectedInstanceInfoSubject.asObservable();

  private hostSubjects = {};

  constructor() { }

  private updateTestbeds(stateMessage: StateMessage) {
    const host_ip = stateMessage.data.host_ip;
    const state = stateMessage.data.state;
    const serviceKeys = stateMessage.data.services ? Object.keys(stateMessage.data.services) : [];
    const services = {};
    const containerKeys = stateMessage.data.containers ? Object.keys(stateMessage.data.containers) : [];
    const containers = {};
    const instanceInfo: InstanceInfo[] = [];
    const instanceState: InstanceState = {
      timestamp: stateMessage.header.timestamp,
      ready: stateMessage.data.instance_state.ready,
      down: stateMessage.data.instance_state.down,
      down_reason: stateMessage.data.instance_state.down_reason,
      message: stateMessage.data.instance_state.message
    };
    serviceKeys.forEach(service => {
      services[stateMessage.data.services[service].source] = {
        source: stateMessage.data.services[service].source,
        timestamp: stateMessage.data.services[service].timestamp,
        message_type: stateMessage.data.services[service].message_type,
        sub_type: stateMessage.data.services[service].sub_type,
        state: stateMessage.data.services[service].state,
        topic: stateMessage.data.services[service].topic
      } as Details;
    });
    containerKeys.forEach(container => {
      containers[stateMessage.data.containers[container].container_name] = {
        container_id: stateMessage.data.containers[container].container_id,
        container_name: stateMessage.data.containers[container].container_name,
        image_id: stateMessage.data.containers[container].image_id,
        status: stateMessage.data.containers[container].status,
        state: stateMessage.data.containers[container].state,
        created: stateMessage.data.containers[container].created,
        timestamp: stateMessage.data.containers[container].timestamp,
        cpu_percent: stateMessage.data.containers[container].cpu_percent,
        mem_percent: stateMessage.data.containers[container].mem_percent
      } as Properties;
    });
    stateMessage.data.instance_info.forEach(info => {
      instanceInfo.push({
        trial_id: info.trial_id,
        trial_name: info.trial_name,
        trial_created: info.trial_created,
        experimenter: info.experimenter,
        subjects: info.subjects,
        trial_number: info.trial_number,
        group_number: info.group_number,
        study_number: info.study_number,
        condition: info.condition,
        notes: info.notes,
        experiment_id: info.experiment_id,
        experiment_name: info.experiment_name,
        experiment_created: info.experiment_created,
        experiment_author: info.experiment_author,
        experiment_mission: info.experiment_mission,
        map_name: info.map_name,
        map_block_filename: info.map_block_filename,
        players: info.players,
        team_id: info.team_id,
        intervention_agents: info.intervention_agents,
        observers: info.observers,
        trial_start: info.trial_start,
        trial_stop: info.trial_stop,
        auto_export_begin: info.auto_export_begin,
        auto_export_end: info.auto_export_end,
        mission_timer: info.mission_timer,
        mission_elapsed_milliseconds: info.mission_elapsed_milliseconds,
        mission_name: info.mission_name,
        mission_state: info.mission_state,
        mission_state_change_outcome: info.mission_state_change_outcome,
      });
    });
    this.testbedsObj[host_ip] = {
      state,
      services,
      containers,
      instance_info: instanceInfo,
      instance_state: instanceState
    };
    return host_ip;
  }

  public addState(stateMessage: StateMessage) {
    const hostIp = this.updateTestbeds(stateMessage);
    this.send();
    if (this.hostSubjects.hasOwnProperty(hostIp)) {
      this.hostSubjects[hostIp].next();
      this.hostSubjects[hostIp].complete();
      delete this.hostSubjects[hostIp];
    }
    const destroy = new Subject();
    this.hostSubjects[hostIp] = destroy;
    timer(environment.testbedTimeout)
      .pipe(
        takeUntil(destroy)
      ).subscribe(t => {
        delete this.testbedsObj[hostIp];
        this.send();
    });
    // Only for testing!
    // this.generate();
  }

  private send() {
    this.testbedsSubject.next(JSON.stringify(this.testbedsObj));
    if (this.selectedTestbedStr !== '') {
      this.selectedTestbedSubject.next(this.selectedTestbedStr);
    }
    if (this.selectedServiceStr !== '') {
      this.selectedServiceSubject.next(this.selectedServiceStr);
    }
    if (this.selectedContainerStr !== '') {
      this.selectedContainerSubject.next(this.selectedContainerStr);
    }
  }

  public getSelectedTestbed() {
    return this.selectedTestbed;
  }

  public setSelectedTestbed(hostIp: string) {
    if (this.testbedsObj.hasOwnProperty(hostIp)) {
      this.selectedTestbedStr = hostIp;
      this.selectedTestbedSubject.next(hostIp);
      this.selectedServiceStr = '';
      this.selectedServiceSubject.next('');
    } else {
      this.selectedTestbedStr = '';
      this.selectedTestbedSubject.next('');
      this.selectedServiceStr = '';
      this.selectedServiceSubject.next('');
      this.selectedContainerStr = '';
      this.selectedContainerSubject.next('');
      this.selectedInstanceInfoIndex = 0;
      this.selectedInstanceInfoSubject.next(0);
    }
  }

  public clearSelectedTestbed() {
    this.selectedTestbedStr = '';
    this.selectedTestbedSubject.next('');
    this.selectedServiceStr = '';
    this.selectedServiceSubject.next('');
    this.selectedContainerStr = '';
    this.selectedContainerSubject.next('');
    this.selectedInstanceInfoIndex = 0;
    this.selectedInstanceInfoSubject.next(0);
  }

  public getServices(hostIp: string): string {
    if (this.testbedsObj.hasOwnProperty(hostIp)) {
      return JSON.stringify(this.testbedsObj[hostIp].services);
    } else {
      return '[]';
    }
  }

  public setSelectedService(serviceName: string) {
    if (this.testbedsObj.hasOwnProperty(this.selectedTestbedStr)) {
      if (this.testbedsObj[this.selectedTestbedStr].services.hasOwnProperty(serviceName)) {
        this.selectedServiceStr = serviceName;
        this.selectedServiceSubject.next(serviceName);
      }
    }
  }

  public getDetails(hostIp: string, serviceName: string): string {
    if (this.testbedsObj.hasOwnProperty(hostIp)) {
      if (this.testbedsObj[this.selectedTestbedStr].services.hasOwnProperty(serviceName)) {
        return JSON.stringify(this.testbedsObj[hostIp].services[serviceName]);
      } else {
        return '[]';
      }
    } else {
      return '[]';
    }
  }

  getContainers(hostIp: string) {
    if (this.testbedsObj.hasOwnProperty(hostIp)) {
      return JSON.stringify(this.testbedsObj[hostIp].containers);
    } else {
      return '[]';
    }
  }

  public setSelectedContainer(containerName: string) {
    if (this.testbedsObj.hasOwnProperty(this.selectedTestbedStr)) {
      if (this.testbedsObj[this.selectedTestbedStr].containers.hasOwnProperty(containerName)) {
        this.selectedContainerStr = containerName;
        this.selectedContainerSubject.next(containerName);
      }
    }
  }

  public getProperties(hostIp: string, containerName: string): string {
    if (this.testbedsObj.hasOwnProperty(hostIp)) {
      if (this.testbedsObj[this.selectedTestbedStr].containers.hasOwnProperty(containerName)) {
        return JSON.stringify(this.testbedsObj[hostIp].containers[containerName]);
      } else {
        return '[]';
      }
    } else {
      return '[]';
    }
  }

  getInstanceInfos(hostIp: string) {
    if (this.testbedsObj.hasOwnProperty(hostIp)) {
      if (this.testbedsObj[hostIp].hasOwnProperty('instance_info')) {
        if (this.testbedsObj[hostIp].instance_info.length > 0) {
          return JSON.stringify(this.testbedsObj[hostIp].instance_info);
        }
        else {
          return '[]';
        }
      }
      else {
        return '[]';
      }
    } else {
      return '[]';
    }
  }

  public setSelectedInstanceInfo(index: number) {
    if (this.testbedsObj.hasOwnProperty(this.selectedTestbedStr)) {
      if (this.testbedsObj[this.selectedTestbedStr].hasOwnProperty('instance_info')) {
        if (index >= this.testbedsObj[this.selectedTestbedStr].instance_info.length) {
          this.selectedInstanceInfoIndex = 0;
        } else if (index < 0) {
          this.selectedInstanceInfoIndex = this.testbedsObj[this.selectedTestbedStr].instance_info.length - 1;
        } else {
          this.selectedInstanceInfoIndex = index;
        }
        this.selectedInstanceInfoSubject.next(this.selectedInstanceInfoIndex);
      }
    }
  }

  public getInstanceInfo(hostIp: string, index: number): string {
    if (this.testbedsObj.hasOwnProperty(hostIp)) {
      if (this.testbedsObj[hostIp].hasOwnProperty('instance_info')) {
        if (this.testbedsObj[hostIp].instance_info.length > index) {
          return JSON.stringify(this.testbedsObj[hostIp].instance_info[index]);
        } else {
          return '{}';
        }
      } else {
        return '{}';
      }
    } else {
      return '{}';
    }
  }

  public getInstanceInfoLength(hostIp: string): number {
    if (this.testbedsObj.hasOwnProperty(hostIp)) {
      if (this.testbedsObj[hostIp].hasOwnProperty('instance_info')) {
        if (Array.isArray(this.testbedsObj[hostIp].instance_info)) {
          return this.testbedsObj[hostIp].instance_info.length;
        } else {
          return 0;
        }
      } else {
        return 0;
      }
    } else {
      return 0;
    }
  }

  public getInstanceState(hostIp: string): string {
    if (this.testbedsObj.hasOwnProperty(hostIp)) {
      if (this.testbedsObj[hostIp].hasOwnProperty('instance_state')) {
        return JSON.stringify(this.testbedsObj[hostIp].instance_state);
      } else {
        return '{}';
      }
    } else {
      return '{}';
    }
  }

}
