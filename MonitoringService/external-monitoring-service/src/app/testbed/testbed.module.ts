import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TestbedsComponent } from './testbeds/testbeds.component';
import { SwiperModule } from 'swiper/angular';
import { MatCardModule } from '@angular/material/card';
import { FlexModule } from '@angular/flex-layout';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { LayoutModule } from '@angular/cdk/layout';
import { ServicesComponent } from './services/services.component';
import { DetailsComponent } from './details/details.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatListModule} from '@angular/material/list';
import {MatTableModule} from '@angular/material/table';
import { ContainersComponent } from './containers/containers.component';
import { PropertiesComponent } from './properties/properties.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatSliderModule} from '@angular/material/slider';
import {FormsModule} from '@angular/forms';
import { InstancePanelComponent } from './instance-panel/instance-panel.component';

@NgModule({
    declarations: [TestbedsComponent, ServicesComponent, DetailsComponent, DashboardComponent, ContainersComponent, PropertiesComponent, InstancePanelComponent],
  exports: [
    TestbedsComponent,
    DashboardComponent
  ],
  imports: [
    CommonModule,
    SwiperModule,
    MatCardModule,
    FlexModule,
    MatGridListModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    LayoutModule,
    BrowserAnimationsModule,
    MatListModule,
    MatTableModule,
    MatExpansionModule,
    MatSliderModule,
    FormsModule
  ]
})
export class TestbedModule { }
