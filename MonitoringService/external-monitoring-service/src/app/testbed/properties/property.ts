export interface Property {
  name: String;
  value: String;
}
