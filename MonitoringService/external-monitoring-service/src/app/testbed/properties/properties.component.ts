import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import {Subscription} from 'rxjs';
import {MatSort} from '@angular/material/sort';
import {Properties} from './properties';
import {Property} from './property';
import {StateService} from '../state/state.service';
import {MediaChange, MediaObserver} from '@angular/flex-layout';
import {filter, map} from 'rxjs/operators';

@Component({
  selector: 'app-properties',
  templateUrl: './properties.component.html',
  styleUrls: ['./properties.component.scss']
})
export class PropertiesComponent implements OnInit, OnDestroy {
  title: string;
  selectedTestbed: string;
  selectedContainer: string;
  properties: Properties;
  propertyValues: Property[];
  dataSource = new MatTableDataSource(this.propertyValues);
  displayedColumns: string[];

  currentScreenWidth = '';
  flexMediaWatcher: Subscription;

  private selectedTestbedSubscription: Subscription;
  private selectedContainerSubscription: Subscription;

  @ViewChild(MatTable) table: MatTable<Property>;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private stateService: StateService,
    private mediaObserver: MediaObserver
  ) {
    this.flexMediaWatcher = mediaObserver.asObservable()
      .pipe(
        filter((changes: MediaChange[]) => changes.length > 0),
        map((changes: MediaChange[]) => changes[0])
      ).subscribe((change: MediaChange) => {
        if (change.mqAlias !== this.currentScreenWidth) {
          this.currentScreenWidth = change.mqAlias;
          this.setupTable();
        }
      });
  }

  ngOnInit(): void {
    this.title  = 'Properties';
    this.selectedTestbedSubscription = this.stateService.selectedTestbed.subscribe(selectedTestbed => {
      if (this.selectedTestbed !== selectedTestbed) {
        this.selectedContainer = '';
        this.propertyValues = [];
        this.selectedTestbed = selectedTestbed;
        this.dataSource.data = this.propertyValues;
        this.dataSource.sort = this.sort;
      }
    });

    this.selectedContainerSubscription = this.stateService.selectedContainer.subscribe(selectedContainer => {
      this.propertyValues = [];
      this.selectedContainer = selectedContainer;
      this.properties = JSON.parse(this.stateService.getProperties(this.selectedTestbed, this.selectedContainer)) as Properties;
      const propertyKeys = Object.keys(this.properties).sort();
      propertyKeys.forEach(key => {
        this.propertyValues.push({
          name: key,
          value: this.properties[key]}
        );
      });

      this.dataSource.data = this.propertyValues;
      this.dataSource.sort = this.sort;
    });
    // if (this.table) {
    //   this.table.renderRows();
    // }
  }

  ngOnDestroy(): void {
    this.selectedTestbedSubscription.unsubscribe();
    this.selectedContainerSubscription.unsubscribe();
  }

  setupTable() {
    this.displayedColumns = ['name', 'value'];
    if (this.currentScreenWidth === 'xs') {
      this.displayedColumns = ['name', 'value'];
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
