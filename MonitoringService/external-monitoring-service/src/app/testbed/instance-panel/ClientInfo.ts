export interface ClientInfo {
  callsign: string;
  participant_id: string;
}
