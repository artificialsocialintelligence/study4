import {Component, OnDestroy, OnInit, Input} from '@angular/core';
import {StateService} from '../state/state.service';
import {InstanceInfo} from './InstanceInfo';
import {Subscription} from 'rxjs';
import {InstanceState} from './InstanceState';

@Component({
  selector: 'app-instance-panel',
  templateUrl: './instance-panel.component.html',
  styleUrls: ['./instance-panel.component.scss']
})
export class InstancePanelComponent implements OnInit, OnDestroy {
  instanceInfo: InstanceInfo = null;
  instanceInfoIndexMap = {};
  instanceState: InstanceState = null;
  selectedHostIp = '';
  customCollapsedHeight: '68px';

  private testbedSubscription: Subscription;
  private selectedTestbedSubscription: Subscription;
  private selectedInstanceInfoSubscription: Subscription;

  constructor(
    public stateService: StateService,
  ) { }

  @Input() host: any;
  ngOnInit(): void {
    this.instanceInfoIndexMap[this.host.host_ip] = 0;
    this.selectedTestbedSubscription = this.stateService.selectedTestbed.subscribe((selectedTestbed) => {
      this.selectedHostIp = selectedTestbed;
    });
    this.testbedSubscription = this.stateService.testbeds.subscribe(() => {
      this.instanceInfo = JSON.parse(this.stateService.getInstanceInfo(this.host.host_ip, this.instanceInfoIndexMap[this.host.host_ip])) as InstanceInfo;
      this.instanceState = JSON.parse(this.stateService.getInstanceState(this.host.host_ip)) as InstanceState;
    });
    this.selectedInstanceInfoSubscription = this.stateService.selectedInstanceInfo.subscribe(index => {
      if (this.selectedHostIp === this.host.host_ip) {
        this.instanceInfoIndexMap[this.host.host_ip] = index;
        this.instanceInfo = JSON.parse(this.stateService.getInstanceInfo(this.host.host_ip, this.instanceInfoIndexMap[this.host.host_ip])) as InstanceInfo;
      }
    });
  }

  ngOnDestroy(): void {
    this.selectedTestbedSubscription.unsubscribe();
    this.testbedSubscription.unsubscribe();
    this.selectedInstanceInfoSubscription.unsubscribe();
  }

  public testbedPanelOpened(hostIp: string) {
    this.stateService.setSelectedTestbed(hostIp);
    this.instanceInfo = JSON.parse(this.stateService.getInstanceInfo(this.host.host_ip, this.instanceInfoIndexMap[this.host.host_ip])) as InstanceInfo;
    this.instanceState = JSON.parse(this.stateService.getInstanceState(this.host.host_ip)) as InstanceState;
  }

  public mapClass(host: any) {
    switch (host.state) {
      case 'ok':
        return 'state-ok';
      case 'warning':
        return 'state-warning';
      case 'error':
        return 'state-error';
      default:
        return 'state-error';
    }
  }

  // public getInstanceInfoItem(hostIp: string, item: string) {
  //   return this.testbeds[hostIp].instance_info[this.instanceInfoNum][item];
  // }

  public incrementSlider(){
    this.stateService.setSelectedInstanceInfo(this.instanceInfoIndexMap[this.host.host_ip] + 1);
  }

  public decrementSlider(){
    this.stateService.setSelectedInstanceInfo(this.instanceInfoIndexMap[this.host.host_ip] - 1);
  }

  playersAllocated() {
    if (this.instanceInfo.trial_stop === '') {
      if (this.instanceInfo.players.length > 0) {
        return 'icon-enabled';
      } else {
        return 'icon-disabled';
      }
    } else {
      return 'icon-disabled';
    }
  }

  instanceReady() {
    if (this.instanceState.down === undefined) {
      return 'icon-disabled';
    }
    if (this.instanceState.ready === '') {
      return 'icon-disabled';
    } else {
      return 'icon-enabled';
    }
  }

  missionRunning() {
    if (this.instanceInfo.mission_state === undefined) {
      return 'icon-disabled';
    }
    if (this.instanceInfo.mission_state === 'Start') {
      return 'icon-enabled';
    } else {
      return 'icon-disabled';
    }
  }

  autoExportComplete() {
    if (this.instanceInfo.auto_export_end === undefined) {
      return 'icon-disabled';
    }
    if (this.instanceInfo.auto_export_end === '') {
      return 'icon-disabled';
    } else {
      return 'icon-enabled';
    }
  }

  instanceDown() {
    if (this.instanceState.down === undefined) {
      return 'icon-disabled';
    }
    if (this.instanceState.down === '') {
      return 'icon-disabled';
    } else {
      return 'icon-enabled';
    }
  }

}
