export interface InstanceState {
  timestamp: string;
  ready: string;
  down: string;
  down_reason: string;
  message: string;
}
