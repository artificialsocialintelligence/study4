import {Properties} from '../properties/properties';

export interface Containers {
  [name: string]: Properties;
}
