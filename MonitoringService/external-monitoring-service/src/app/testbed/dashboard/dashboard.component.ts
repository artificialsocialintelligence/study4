import { Component, OnInit } from '@angular/core';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  /** Based on the screen size, switch from standard to one column per row */
  cards = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(({ matches }) => {
      if (matches) {
        console.log('matches');
        return {
          testbeds: { cols: 1, rows: 1 },
          services: { cols: 1, rows: 1 },
          details: { cols: 1, rows: 1 },
          containers: { cols: 1, rows: 1 },
          properties: { cols: 1, rows: 1 },
          cols: 1
        };
      }
      console.log('no matches');
      return {
        testbeds: { cols: 1, rows: 2 },
        services: { cols: 1, rows: 1 },
        details: { cols: 1, rows: 1 },
        containers: { cols: 1, rows: 1 },
        properties: { cols: 1, rows: 1 },
        cols: 3
      };
    })
  );

  constructor(private breakpointObserver: BreakpointObserver) {}

  ngOnInit(): void {
  }

}
