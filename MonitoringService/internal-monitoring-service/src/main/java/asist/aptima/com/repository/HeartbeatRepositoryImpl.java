package asist.aptima.com.repository;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import asist.aptima.com.configuration.ApplicationConfiguration;
import asist.aptima.com.domain.Heartbeat;
import asist.aptima.com.model.MessageHeartbeat;
import asist.aptima.com.utility.SortingAndOrderArgumentsHeartbeat;
import io.micronaut.transaction.annotation.ReadOnly;
import jakarta.inject.Singleton;

@Singleton 
public class HeartbeatRepositoryImpl implements HeartbeatRepository {
	
	private static final Logger logger = LoggerFactory.getLogger(HeartbeatRepositoryImpl.class);

    private static final List<String> VALID_PROPERTY_NAMES = Arrays.asList("id", "source", "timestamp", "message_type", "sub_type", "state");

    private final EntityManager entityManager;  
    private final ApplicationConfiguration applicationConfiguration;

    public HeartbeatRepositoryImpl(EntityManager entityManager, ApplicationConfiguration applicationConfiguration) {
        this.entityManager = entityManager;
        this.applicationConfiguration = applicationConfiguration;
    }

    @Override
    @ReadOnly 
    public Optional<Heartbeat> findById(long id) {
        return Optional.ofNullable(entityManager.find(Heartbeat.class, id));
    }
    
    @Override
    @ReadOnly 
    public Optional<Heartbeat> findBySource(String source) {
    	try {
    		return entityManager.createQuery("SELECT h FROM Heartbeat as h WHERE h.source = :source", Heartbeat.class)
    				.setParameter("source", source)
    				.getResultList()
    				.stream()
    				.findFirst();
    	} catch (Exception e) {
    		logger.error(e.getMessage());
    		return Optional.empty();
    	}
    }

    @Override
    @Transactional 
    public Heartbeat save(@NotBlank MessageHeartbeat messageHeartbeat, String topic) {
    	try {
        	Heartbeat heartbeat = new Heartbeat(
        			messageHeartbeat.getMsg().getSource(),
        			messageHeartbeat.getHeader().getTimestamp(),
        			messageHeartbeat.getHeader().getMessageType(),
        			messageHeartbeat.getMsg().getSubType(),
        			messageHeartbeat.getData().getState().toString(),
        			topic
        			);
            entityManager.persist(heartbeat);
            return heartbeat;
    	} catch (Exception e) {
    		logger.error(e.getMessage());
    		return null;
    	}

    }

    @Override
    @Transactional 
    public void deleteById(long id) {
        findById(id).ifPresent(entityManager::remove);
    }

    @ReadOnly 
    public List<Heartbeat> findAll(@NotNull SortingAndOrderArgumentsHeartbeat args) {
        String qlString = "SELECT h FROM Heartbeat as h";
        if (args.getOrder().isPresent() && args.getSort().isPresent() && VALID_PROPERTY_NAMES.contains(args.getSort().get())) {
            qlString += " ORDER BY h." + args.getSort().get() + ' ' + args.getOrder().get().toLowerCase();
        }
        TypedQuery<Heartbeat> query = entityManager.createQuery(qlString, Heartbeat.class);
        query.setMaxResults(args.getMax().orElseGet(applicationConfiguration::getMax));
        args.getOffset().ifPresent(query::setFirstResult);

        return query.getResultList();
    }

    @Override
    @Transactional 
    public int update(@NotBlank Heartbeat heartbeat) {
        return entityManager.createQuery("UPDATE Heartbeat h SET source = :source, timestamp = :timestamp, message_type = :message_type, sub_type = :sub_type, state = :state, topic = :topic where id = :id")
                .setParameter("source", heartbeat.getSource())
                .setParameter("timestamp", heartbeat.getTimestamp())
                .setParameter("message_type", heartbeat.getMessageType())
                .setParameter("sub_type", heartbeat.getSubType())
                .setParameter("state", heartbeat.getState())
                .setParameter("topic", heartbeat.getTopic())
                .setParameter("id", heartbeat.getId())
                .executeUpdate();
    }
    
    @Override
    @Transactional 
    public int updateState(long id, @NotBlank String state) {
        return entityManager.createQuery("UPDATE Heartbeat h SET state = :state where id = :id")
                .setParameter("state", state)
                .setParameter("id", id)
                .executeUpdate();
    }
}
