package asist.aptima.com.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.serde.annotation.Serdeable;

@Serdeable
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataStateInstanceState {

	@JsonProperty("ready")
	private String ready;
	@JsonProperty("down")
	private String down;

	@JsonProperty("down_reason")
	private String downReason;

	@JsonProperty("message")
	private String message;

	@JsonCreator
	public DataStateInstanceState(
			String ready,
			String down,
			String downReason,
			String message
			) {
		this.ready = ready;
		this.down = down;
		this.downReason = downReason;
		this.message = message;
	}

	public DataStateInstanceState() {
		// TODO Auto-generated constructor stub
	}

	public String getReady() {
		return ready;
	}

	public void setReady(String ready) {
		this.ready = ready;
	}

	public String getDown() {
		return down;
	}

	public void setDown(String down) {
		this.down = down;
	}

	public String getDownReason() {
		return downReason;
	}

	public void setDownReason(String downReason) {
		this.downReason = downReason;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
