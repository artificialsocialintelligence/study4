package asist.aptima.com.usecase;

import asist.aptima.com.event.ContainerMonitoring;
import asist.aptima.com.repository.ContainerRepository;
import asist.aptima.com.task.ContainerMonitorTask;
import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.model.Container;
import io.micronaut.context.annotation.Property;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.TaskScheduler;
import jakarta.inject.Named;
import jakarta.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.concurrent.ScheduledFuture;

@Singleton
public class ContainerUseCase {

    private static final Logger logger = LoggerFactory.getLogger(ContainerUseCase.class);

    @Property(name = "container.taskSchedule.delay")
    private Duration DELAY;

    protected final DockerClient dockerClient;
    protected final TaskScheduler taskScheduler;
    //private Map<String, ScheduledFuture<?>> schedules = new HashMap<String, ScheduledFuture<?>>();
    protected final ContainerSchedules schedules;
    protected final ContainerRepository containerRepository;
    protected final ContainerMonitoring containerMonitoring;

    public ContainerUseCase(
            DockerClient dockerClient,
            @Named(TaskExecutors.SCHEDULED) TaskScheduler taskScheduler,
            ContainerSchedules schedules,
            ContainerRepository containerRepository,
            ContainerMonitoring containerMonitoring
    ) {
        this.dockerClient = dockerClient;
        this.taskScheduler = taskScheduler;
        this.schedules = schedules;
        this.containerRepository = containerRepository;
        this.containerMonitoring = containerMonitoring;
    }

    public void scheduleContainer(Container container) {
        schedule(container);
    }

    public void cancelAllSchedules() {
        schedules.cancelAll();
        containerRepository.deleteAll();
    }

    public int schedulesSize() {
        return schedules.schedulesSize();
    }

    private void schedule(Container container) {
        String containerName = container.getNames()[0].substring(1);
        String containerId = container.getId();
        if (schedules.containsKey(containerId)) {
            return;
        }
        logger.debug("Scheduling container monitoring for [{}]", containerName);
        ContainerMonitorTask task = new ContainerMonitorTask(dockerClient, containerId, schedules, containerRepository, containerMonitoring);
        ScheduledFuture<?> schedule = taskScheduler.schedule(DELAY, task);
        schedules.put(containerId, schedule);        
    }
}
