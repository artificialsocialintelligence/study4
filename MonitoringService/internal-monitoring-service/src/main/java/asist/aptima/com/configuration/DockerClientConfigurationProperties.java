package asist.aptima.com.configuration;

import io.micronaut.context.annotation.ConfigurationProperties;

import javax.validation.constraints.NotNull;

@ConfigurationProperties("docker.client")
public class DockerClientConfigurationProperties implements DockerClientConfiguration {
	
    private String host;
    private boolean tlsVerify;

    /**
     * @return The docker host
     */
    @NotNull
    public String getHost() {
        return host;
    }

    /**
     * @param host The docker host
     */
    public void setHost(String host) {
        this.host = host;
    }
    
    /**
     * @return The tlsVerify
     */
    @NotNull
    public boolean getTlsVerify() {
        return tlsVerify;
    }

    /**
     * @param tlsVerify The tlsVerify
     */
    public void setTlsVerify(boolean tlsVerify) {
        this.tlsVerify = tlsVerify;
    }

}
