package asist.aptima.com.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.serde.annotation.Serdeable;

@Serdeable
public class MessagePlayerState {

	@JsonProperty("header")
	private Header header;
	@JsonProperty("msg")
	private Msg msg;
	@JsonProperty("data")
	private DataPlayerState data;

	@JsonCreator
	public MessagePlayerState(
			Header header,
			Msg msg,
			DataPlayerState data
			) {
		this.header = header;
		this.msg = msg;
		this.data = data;
	}

	public MessagePlayerState() {
		// TODO Auto-generated constructor stub
	}

	public Header getHeader() {
		return header;
	}
	public void setHeader(Header header) {
		this.header = header;
	}

	public Msg getMsg() {
		return msg;
	}
	public void setMsg(Msg msg) {
		this.msg = msg;
	}

	public DataPlayerState getData() {
		return data;
	}
	public void setData(DataPlayerState data) {
		this.data = data;
	}
}
