package asist.aptima.com.configuration;

import java.time.Duration;
import java.util.Properties;

import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.validation.constraints.NotNull;

import io.micronaut.runtime.context.scope.Refreshable;
import org.eclipse.paho.mqttv5.client.MqttConnectionOptions;
import org.eclipse.paho.mqttv5.common.MqttMessage;
import org.eclipse.paho.mqttv5.common.packet.MqttProperties;

import io.micronaut.context.annotation.ConfigurationBuilder;
import io.micronaut.context.annotation.ConfigurationProperties;


@ConfigurationProperties("external-mqtt.client")
public class ExternalMqttConfigurationProperties implements ExternalMqttConfiguration {
	
    private String serverHost;
    private int serverPort;
    private String clientId;

    /**
     * @return The server host
     */
    @NotNull
    public String getServerHost() {
        return serverHost;
    }

    /**
     * @param serverHost The server host
     */
    public void setServerHost(String serverHost) {
        this.serverHost = serverHost;
    }
    
    /**
     * @return The server port
     */
    @NotNull
    public int getServerPort() {
        return serverPort;
    }

    /**
     * @param serverPort The server port
     */
    public void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }

    /**
     * @return The client id
     */
    @NotNull
    public String getClientId() {
        return clientId;
    }

    /**
     * @param clientId The client ID
     */
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

}
