package asist.aptima.com.publisher;

import io.micronaut.mqtt.annotation.Qos;
import io.micronaut.mqtt.annotation.Topic;
import io.micronaut.mqtt.v5.annotation.MqttPublisher;

import java.util.concurrent.CompletableFuture;

@MqttPublisher
public interface GeneralPublisher {

	CompletableFuture<Void> send(@Topic String topic, @Qos int qos, byte[] data);

}
