package asist.aptima.com.usecase;

import jakarta.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;

@Singleton
public class ContainerSchedules {

	private static final Logger logger = LoggerFactory.getLogger(ContainerSchedules.class);

	private final Map<String, ScheduledFuture<?>> schedules = new ConcurrentHashMap<String, ScheduledFuture<?>>();
    
    public void put(String source, ScheduledFuture<?> schedule) {
		this.schedules.put(source, schedule);
    }
    
    public ScheduledFuture<?> remove(String source) {
		return this.schedules.remove(source);
    }
    
    public boolean containsKey(String source) {
		return this.schedules.containsKey(source);
    }
    
    public boolean cancel(String containerId) {
		if (schedules.containsKey(containerId)) {
			logger.debug("Canceling container monitoring scheduler for [{}]", containerId);
			ScheduledFuture<?> schedule = schedules.remove(containerId);
			return schedule.cancel(false);
		} else {
			return true;
		}
    }

	public void cancelAll() {
		logger.debug("Canceling all container monitoring schedules.");
		schedules.forEach((containerId, scheduledFuture) -> {
			cancel(containerId);
		});
	}

	public int schedulesSize() {
		return schedules.size();
	}
}
