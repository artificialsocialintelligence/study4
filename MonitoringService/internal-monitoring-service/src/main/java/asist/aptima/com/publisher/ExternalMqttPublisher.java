package asist.aptima.com.publisher;

import java.io.IOException;
import java.text.MessageFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hivemq.client.mqtt.datatypes.MqttQos;
import com.hivemq.client.mqtt.mqtt5.Mqtt5AsyncClient;
import com.hivemq.client.mqtt.mqtt5.Mqtt5Client;

import asist.aptima.com.model.MessageState;
import asist.aptima.com.usecase.HeartbeatUseCase;
import io.micronaut.context.annotation.Property;
import io.micronaut.serde.ObjectMapper;
import jakarta.inject.Singleton;

@Singleton
public class ExternalMqttPublisher {
	
	private static final Logger logger = LoggerFactory.getLogger(ExternalMqttPublisher.class);

	private Mqtt5AsyncClient client;
	private ObjectMapper objectMapper;
	
    @Property(name = "external-monitoring-service.topic")
	private String TOPIC;
	
	public ExternalMqttPublisher(Mqtt5AsyncClient client, ObjectMapper objectMapper) {
		this.client = client;
		this.objectMapper = objectMapper;
	}
	
	public void publish(MessageState messageState) {
		String topic = MessageFormat.format("{0}/{1}", TOPIC, messageState.getData().getHostIp());
		String payload;
		try {
			payload = objectMapper.writeValueAsString(messageState);
			client.connect()
	        .thenAccept(connAck -> logger.debug("External MQTT connected {}", connAck.getReasonString()))
	        .thenCompose(v -> client.publishWith().topic(topic).qos(MqttQos.EXACTLY_ONCE).payload(payload.getBytes()).send())
	        .thenAccept(publishResult -> logger.debug("Published state on topic [{}] {}", topic, publishResult))
	        .thenCompose(v -> client.disconnect())
	        .thenAccept(v ->  logger.debug("External MQTT disconnected"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
