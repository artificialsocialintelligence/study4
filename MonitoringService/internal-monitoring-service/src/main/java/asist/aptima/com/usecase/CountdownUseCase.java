package asist.aptima.com.usecase;

import java.text.MessageFormat;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import asist.aptima.com.domain.Heartbeat;
import asist.aptima.com.model.HeartbeatStateType;
import asist.aptima.com.repository.HeartbeatRepository;
import jakarta.inject.Singleton;

@Singleton
public class CountdownUseCase {

	private static final Logger logger = LoggerFactory.getLogger(CountdownUseCase.class);
	private HeartbeatRepository heartbeatRepository;
	
	public CountdownUseCase(HeartbeatRepository heartbeatRepository) {
		this.heartbeatRepository = heartbeatRepository;
	}
	
    public HeartbeatStateType update(String source) {
    	Optional<Heartbeat> heartbeat = heartbeatRepository.findBySource(source);
		if (heartbeat.isPresent()) {
			Heartbeat currentHeartbeat = heartbeat.get();
			HeartbeatStateType currentState = Enum.valueOf(HeartbeatStateType.class, currentHeartbeat.getState());
			HeartbeatStateType newState = currentState.equals(HeartbeatStateType.ok) ? HeartbeatStateType.warning : HeartbeatStateType.error;			
			long id = currentHeartbeat.getId();
			heartbeatRepository.updateState(id, newState.toString());
			logger.debug("Changed heartbeat state for [{}] from [{}] to [{}]", source, currentState.toString(), newState.toString());
			return newState;
		}
		return HeartbeatStateType.error;
    }
}
