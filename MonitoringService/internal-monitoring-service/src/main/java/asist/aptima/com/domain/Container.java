package asist.aptima.com.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.serde.annotation.Serdeable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Serdeable
@Entity
@Table(name = "container")
public class Container {

	private static final Logger logger = LoggerFactory.getLogger(Container.class);

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @JsonProperty("id")
    private Long id;

    @Version
    @JsonIgnore
    private Long version;

    @NotNull
    @Column(name="container_id", nullable=false, unique=true)
    @JsonProperty("container_id")
    private String containerId;

    @NotNull
    @Column(name="container_name", nullable=false)
    @JsonProperty("container_name")
    private String containerName;

    @NotNull
    @Column(name="image_id", nullable=false)
    @JsonProperty("image_id")
    private String imageId;

    @NotNull
    @Column(name="status", nullable=false)
    @JsonProperty("status")
    private String status;

    @NotNull
    @Column(name="state", nullable=false)
    @JsonProperty("state")
    private String state;

    @NotNull
    @Column(name="created", nullable=false)
    @JsonProperty("created")
    private String created;

    @NotNull
    @Column(name="timestamp", nullable=false)
    @JsonProperty("timestamp")
    private String timestamp;

    @NotNull
    @Column(name="cpu_percent", nullable=false)
    @JsonProperty("cpu_percent")
    private String cpuPercent;

    @NotNull
    @Column(name="mem_percent", nullable=false)
    @JsonProperty("mem_percent")
    private String memPercent;

    public Container() {}

    public Container(
    		@NotNull String containerId,
    		@NotNull String containerName,
    		@NotNull String imageId,
    		@NotNull String status,
    		@NotNull String state,
    		@NotNull String created,
    		@NotNull String timestamp,
    		@NotNull String cpuPercent,
    		@NotNull String memPercent
                ) {
        this.containerId = containerId;
        this.containerName = containerName;
        this.imageId = imageId;
        this.status = status;
        this.state = state;
        this.created = created;
        this.timestamp = timestamp;
        this.cpuPercent = cpuPercent;
        this.memPercent = memPercent;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContainerId() {
        return containerId;
    }

    public void setContainerId(String containerId) {
        this.containerId = containerId;
    }

    public String getContainerName() {
        return containerName;
    }

    public void setContainerName(String containerName) {
        this.containerName = containerName;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getCpuPercent() {
        return cpuPercent;
    }

    public void setCpuPercent(String cpuPercent) {
        this.cpuPercent = cpuPercent;
    }

    public String getMemPercent() {
        return memPercent;
    }

    public void setMemPercent(String memPercent) {
        this.memPercent = memPercent;
    }

    @Override
    public String toString() {
        return "{" +
                "\"id\": \"" + id + "\"," +
                "\"container_id\": \"" + containerId + "\"," +
                "\"container_name\": \"" + containerName + "\"," +
                "\"image_id\": \"" + imageId + "\"," +
                "\"status\": \"" + status + "\"," +
                "\"state\": \"" + state + "\"," +
                "\"created\": \"" + created + "\"," +
                "\"timestamp\": \"" + timestamp + "\"," +
                "\"cpu_percent\": \"" + cpuPercent + "\"," +
                "\"mem_percent\": \"" + memPercent + "\"" +
                "}";
    }
}
