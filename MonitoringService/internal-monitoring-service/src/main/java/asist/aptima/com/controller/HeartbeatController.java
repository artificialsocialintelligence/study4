package asist.aptima.com.controller;

import java.util.List;

import javax.validation.Valid;

import asist.aptima.com.domain.Heartbeat;
import asist.aptima.com.repository.HeartbeatRepository;
import asist.aptima.com.utility.SortingAndOrderArgumentsHeartbeat;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.scheduling.annotation.ExecuteOn;
import io.micronaut.scheduling.TaskExecutors;

@ExecuteOn(TaskExecutors.IO)  
@Controller("/heartbeats") 
public class HeartbeatController {

    private final HeartbeatRepository heartbeatRepository;

    HeartbeatController(HeartbeatRepository heartbeatRepository) { 
        this.heartbeatRepository = heartbeatRepository;
    }

    @Get("/{id}") 
    Heartbeat show(Long id) {
        return heartbeatRepository
                .findById(id)
                .orElse(null); 
    }

    @Get(value = "/list{?args*}") 
    List<Heartbeat> list(@Valid SortingAndOrderArgumentsHeartbeat args) {
        return heartbeatRepository.findAll(args);
    }
}
