package asist.aptima.com.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.micronaut.serde.annotation.Serdeable;
//import jakarta.json.bind.annotation.JsonbCreator;
//import jakarta.json.bind.annotation.JsonbProperty;

@Serdeable
public class DataHeartbeat {
	
	@JsonProperty("state") 
	private HeartbeatStateType state;

	@JsonCreator
	public DataHeartbeat(
			HeartbeatStateType state
			) {
		this.state = state;
	}

	public HeartbeatStateType getState() {
		return state;
	}
	public void setState(HeartbeatStateType state) {
		this.state = state;
	}
}
