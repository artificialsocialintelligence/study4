package asist.aptima.com.repository;

import asist.aptima.com.configuration.ApplicationConfiguration;
import asist.aptima.com.domain.Container;
import asist.aptima.com.domain.InstanceInfo;
import asist.aptima.com.utility.SortingAndOrderArgumentsContainer;
import asist.aptima.com.utility.SortingAndOrderArgumentsInstanceInfo;
import io.micronaut.context.ApplicationContext;
import io.micronaut.context.env.PropertySource;
import io.micronaut.core.util.CollectionUtils;
import io.micronaut.runtime.context.scope.refresh.RefreshEvent;
import io.micronaut.transaction.annotation.ReadOnly;
import jakarta.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Singleton 
public class InstanceInfoRepositoryImpl implements InstanceInfoRepository {

    private static final Logger logger = LoggerFactory.getLogger(InstanceInfoRepositoryImpl.class);

    private static final List<String> VALID_PROPERTY_NAMES = Arrays.asList("trialId", "trialName", "trialCreated", "experimenter", "subjects", "trialNumber", "groupNumber", "studyNumber", "condition", "notes", "experimentId", "experimentName", "experimentCreated", "experimentAuthor", "experimentMission", "mapMame", "mapBlockFilename", "players", "trialStart", "trialStop", "autoExport", "missionTimer", "missionElapsedMilliseconds", "missionName", "missionState", "missionStateChangeOutcome");

    private final EntityManager entityManager;
    private final ApplicationConfiguration applicationConfiguration;

    public InstanceInfoRepositoryImpl(EntityManager entityManager, ApplicationConfiguration applicationConfiguration) {
        this.entityManager = entityManager;
        this.applicationConfiguration = applicationConfiguration;
    }

    @Override
    @ReadOnly 
    public Optional<InstanceInfo> findById(long id) {
        return Optional.ofNullable(entityManager.find(InstanceInfo.class, id));
    }
    
    @Override
    @ReadOnly 
    public Optional<InstanceInfo> findByTrialId(String trialId) {
        try {
            return entityManager.createQuery("SELECT i FROM InstanceInfo as i WHERE i.trialId = :trialId", InstanceInfo.class)
                    .setParameter("trialId", trialId)
                    .getResultList()
                    .stream()
                    .findFirst();
        } catch (Exception e) {
            logger.error(e.getMessage());
            return Optional.empty();
        }
    }

    @Override
    @ReadOnly
    public List<InstanceInfo> findAll(@NotNull SortingAndOrderArgumentsInstanceInfo args) {
        String qlString = "SELECT i FROM InstanceInfo as i";
        if (args.getOrder().isPresent() && args.getSort().isPresent() && VALID_PROPERTY_NAMES.contains(args.getSort().get())) {
            qlString += " ORDER BY i." + args.getSort().get() + ' ' + args.getOrder().get().toLowerCase();
        }
        TypedQuery<InstanceInfo> query = entityManager.createQuery(qlString, InstanceInfo.class);
        query.setMaxResults(args.getMax().orElseGet(applicationConfiguration::getMax));
        args.getOffset().ifPresent(query::setFirstResult);

        return query.getResultList();
    }

    @Override
    @Transactional
    public InstanceInfo save(@NotBlank InstanceInfo instanceInfo) {
        try {
            entityManager.persist(instanceInfo);
            return instanceInfo;
        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    @Override
    @Transactional
    public InstanceInfo update(@NotBlank InstanceInfo instanceInfo) {
        try {
            return entityManager.merge(instanceInfo);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    @Override
    @Transactional 
    public void deleteById(long id) {
        findById(id).ifPresent(entityManager::remove);
    }
}
