package asist.aptima.com.subscriber;

import asist.aptima.com.domain.InstanceInfo;
import asist.aptima.com.model.ClientInfoItem;
import asist.aptima.com.model.MessageMissionState;
import asist.aptima.com.model.MessageTrial;
import asist.aptima.com.repository.HeartbeatRepository;
import asist.aptima.com.repository.InstanceInfoRepository;
import io.micronaut.mqtt.annotation.MqttSubscriber;
import io.micronaut.mqtt.annotation.Topic;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import io.micronaut.serde.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

@ExecuteOn(TaskExecutors.IO)
@MqttSubscriber
public class MissionStateSubscriber {

	private static final Logger logger = LoggerFactory.getLogger(MissionStateSubscriber.class);
	private final ObjectMapper objectMapper;
	private final InstanceInfoRepository instanceInfoRepository;

	public MissionStateSubscriber(ObjectMapper objectMapper, HeartbeatRepository heartbeatRepository, InstanceInfoRepository instanceInfoRepository) {
		this.objectMapper = objectMapper;
		this.instanceInfoRepository = instanceInfoRepository;
	}

    @Topic("observations/events/mission")
    public void receive(byte[] data, @Topic String topic) {  
    	MessageMissionState messageMissionState;
		try {
			String message = new String(data, StandardCharsets.UTF_8);
			messageMissionState = objectMapper.readValue(message, MessageMissionState.class);

			String timestamp = messageMissionState.getHeader().getTimestamp();
			String messageType = messageMissionState.getHeader().getMessageType();
			String subType = messageMissionState.getMsg().getSubType();
			String source = messageMissionState.getMsg().getSource();
			String trialId = messageMissionState.getMsg().getTrialId();
			String experimentId = messageMissionState.getMsg().getExperimentId();
			String missionTimer = messageMissionState.getData().getMissionTimer();
			String elapsedMilliseconds = messageMissionState.getData().getElapsedMilliseconds();
			String mission = messageMissionState.getData().getMission();
			String missionState = messageMissionState.getData().getMissionState();
			String stateChangeOutcome = messageMissionState.getData().getStateChangeOutcome();

			logger.info("Mission [{}] state for trial [{}] at [{}].", mission, trialId, missionTimer);

			Optional<InstanceInfo> instanceInfoOptional = instanceInfoRepository.findByTrialId(trialId);
			if (instanceInfoOptional.isEmpty()) {
				logger.error("Trial [{}] does not exist for Mission [{}]!", trialId, mission);
			} else {
				logger.info("Updating Mission [{}] for [{}].", mission, trialId);
				InstanceInfo instanceInfo = instanceInfoOptional.get();
				instanceInfo.setMissionTimer(missionTimer);
				instanceInfo.setMissionElapsedMilliseconds(elapsedMilliseconds);
				instanceInfo.setMissionName(mission);
				instanceInfo.setMissionState(missionState);
				instanceInfo.setMissionStateChangeOutcome(stateChangeOutcome);
				instanceInfoRepository.update(instanceInfo);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e.getMessage());
		}			
    }
}
