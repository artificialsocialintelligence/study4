package asist.aptima.com.subscriber;

import asist.aptima.com.domain.InstanceInfo;
import asist.aptima.com.model.AutoExportStateType;
import asist.aptima.com.model.MessageAutoExport;
import asist.aptima.com.model.MessageMissionState;
import asist.aptima.com.repository.HeartbeatRepository;
import asist.aptima.com.repository.InstanceInfoRepository;
import io.micronaut.mqtt.annotation.MqttSubscriber;
import io.micronaut.mqtt.annotation.Topic;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import io.micronaut.serde.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.Optional;

@ExecuteOn(TaskExecutors.IO)
@MqttSubscriber
public class AutoExportSubscriber {

	private static final Logger logger = LoggerFactory.getLogger(AutoExportSubscriber.class);
	private final ObjectMapper objectMapper;
	private final InstanceInfoRepository instanceInfoRepository;

	public AutoExportSubscriber(ObjectMapper objectMapper, HeartbeatRepository heartbeatRepository, InstanceInfoRepository instanceInfoRepository) {
		this.objectMapper = objectMapper;
		this.instanceInfoRepository = instanceInfoRepository;
	}

    @Topic("export/auto")
    public void receive(byte[] data, @Topic String topic) {  
    	MessageAutoExport messageAutoExport;
		try {
			String message = new String(data, StandardCharsets.UTF_8);
			messageAutoExport = objectMapper.readValue(message, MessageAutoExport.class);

			String timestamp = messageAutoExport.getHeader().getTimestamp();
			String messageType = messageAutoExport.getHeader().getMessageType();
			String subType = messageAutoExport.getMsg().getSubType();
			String source = messageAutoExport.getMsg().getSource();
			String trialId = messageAutoExport.getMsg().getTrialId();
			String experimentId = messageAutoExport.getMsg().getExperimentId();
			String state = messageAutoExport.getData().getState().toString();

			logger.info("Auto Export [{}] state for trial [{}] at [{}].", state, trialId, timestamp);

			Optional<InstanceInfo> instanceInfoOptional = instanceInfoRepository.findByTrialId(trialId);
			if (instanceInfoOptional.isEmpty()) {
				logger.error("Trial [{}] does not exist for Auto Export [{}]!", trialId, state);
			} else {
				logger.info("Updating Auto Export [{}] for [{}].", state, trialId);
				InstanceInfo instanceInfo = instanceInfoOptional.get();
				if (messageAutoExport.getData().getState().equals(AutoExportStateType.begin)) {
					instanceInfo.setAutoExportBegin(Instant.now().toString());
				} else if (messageAutoExport.getData().getState().equals(AutoExportStateType.end)) {
					instanceInfo.setAutoExportEnd(Instant.now().toString());
				} else {
					logger.error("Invalid state for Auto Export [{}] for Trial [{}]!", state, trialId);
				}
				instanceInfoRepository.update(instanceInfo);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e.getMessage());
		}			
    }
}
