package asist.aptima.com.controller;

import asist.aptima.com.domain.InstanceInfo;
import asist.aptima.com.domain.InstanceState;
import asist.aptima.com.repository.InstanceInfoRepository;
import asist.aptima.com.repository.InstanceStateRepository;
import asist.aptima.com.utility.SortingAndOrderArgumentsInstanceInfo;
import asist.aptima.com.utility.SortingAndOrderArgumentsInstanceState;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;

import javax.validation.Valid;
import java.util.List;

@ExecuteOn(TaskExecutors.IO)  
@Controller("/instance/state")
public class InstanceStateController {

    private final InstanceStateRepository instanceStateRepository;

    InstanceStateController(InstanceStateRepository instanceInfoRepository) {
        this.instanceStateRepository = instanceInfoRepository;
    }

    @Get(value = "/list{?args*}")
    List<InstanceState> list(@Valid SortingAndOrderArgumentsInstanceState args) {
        return instanceStateRepository.findAll(args);
    }
}
