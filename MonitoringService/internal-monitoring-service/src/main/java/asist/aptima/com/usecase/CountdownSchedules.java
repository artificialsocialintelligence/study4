package asist.aptima.com.usecase;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.inject.Singleton;

@Singleton
public class CountdownSchedules {
	
	private static final Logger logger = LoggerFactory.getLogger(CountdownSchedules.class);
	
    private final Map<String, ScheduledFuture<?>> schedules = new HashMap<String, ScheduledFuture<?>>();
    
    public void put(String source, ScheduledFuture<?> schedule) {
    	this.schedules.put(source, schedule);
    }
    
    public ScheduledFuture<?> remove(String source) {
    	return this.schedules.remove(source);
    }
    
    public boolean containsKey(String source) {
    	return this.schedules.containsKey(source);
    }
    
    public boolean cancel(String source) {
    	if (schedules.containsKey(source)) {
    		logger.debug("Canceling scheduler countdown for [{}]", source);
    		ScheduledFuture<?> schedule = schedules.remove(source);
    		return schedule.cancel(false);
    	} else {
    		return true;
    	}
    }
}
