package asist.aptima.com.job;

import java.io.IOException;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import asist.aptima.com.domain.Container;
import asist.aptima.com.domain.InstanceInfo;
import asist.aptima.com.domain.InstanceState;
import asist.aptima.com.model.*;
import asist.aptima.com.repository.ContainerRepository;
import asist.aptima.com.repository.InstanceInfoRepository;
import asist.aptima.com.repository.InstanceStateRepository;
import asist.aptima.com.utility.SortingAndOrderArgumentsContainer;
import asist.aptima.com.utility.SortingAndOrderArgumentsInstanceInfo;
import asist.aptima.com.utility.SortingAndOrderArgumentsInstanceState;
import com.fasterxml.jackson.core.type.TypeReference;
import io.micronaut.core.type.Argument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import asist.aptima.com.domain.Heartbeat;
import asist.aptima.com.publisher.ExternalMqttPublisher;
import asist.aptima.com.repository.HeartbeatRepository;
import asist.aptima.com.utility.SortingAndOrderArgumentsHeartbeat;
import io.micronaut.context.annotation.Property;
import io.micronaut.scheduling.annotation.Scheduled;
import io.micronaut.serde.ObjectMapper;
import jakarta.inject.Singleton;

@Singleton
public class ExternalMonitoringServiceJob {

	private static final Logger logger = LoggerFactory.getLogger(ExternalMonitoringServiceJob.class);
	private ExternalMqttPublisher externalMqttPublisher;
	private HeartbeatRepository heartbeatRepository;
	private ContainerRepository containerRepository;
	private InstanceInfoRepository instanceInfoRepository;
	private InstanceStateRepository instanceStateRepository;
	private SortingAndOrderArgumentsHeartbeat sortingAndOrderArgumentsHeartbeat = new SortingAndOrderArgumentsHeartbeat(0, null, "source", "asc");
	private SortingAndOrderArgumentsContainer sortingAndOrderArgumentsContainer = new SortingAndOrderArgumentsContainer(0, null, "name", "asc");
	private SortingAndOrderArgumentsInstanceInfo sortingAndOrderArgumentsInstanceInfo = new SortingAndOrderArgumentsInstanceInfo(0, null, "name", "trialId");
	private SortingAndOrderArgumentsInstanceState sortingAndOrderArgumentsInstanceState = new SortingAndOrderArgumentsInstanceState(0, null, "name", "hostIp");
	private ObjectMapper objectMapper;

    @Property(name = "host.ip", defaultValue = "localhost")
	private String HOST_IP;

    @Property(name = "external-monitoring-service.schema.header.version")
	private String EXTERNAL_SCHEMA_HEADER_VERSION;

    @Property(name = "external-monitoring-service.schema.header.messagetype")
	private String EXTERNAL_SCHEMA_HEADER_MESSAGETYPE;

    @Property(name = "external-monitoring-service.schema.msg.version")
	private String EXTERNAL_SCHEMA_MSG_VERSION;

    @Property(name = "external-monitoring-service.schema.msg.subtype")
	private String EXTERNAL_SCHEMA_MSG_SUBTYPE;

    @Property(name = "external-monitoring-service.schema.msg.source")
	private String EXTERNAL_SCHEMA_MSG_SOURCE;

    private final String NOT_SET = "NOT_SET";

	public ExternalMonitoringServiceJob(ObjectMapper objectMapper, ExternalMqttPublisher externalMqttPublisher, HeartbeatRepository heartbeatRepository, ContainerRepository containerRepository, InstanceInfoRepository instanceInfoRepository, InstanceStateRepository instanceStateRepository) {
		this.objectMapper = objectMapper;
		this.externalMqttPublisher = externalMqttPublisher;
		this.heartbeatRepository = heartbeatRepository;
		this.containerRepository = containerRepository;
		this.instanceInfoRepository = instanceInfoRepository;
		this.instanceStateRepository = instanceStateRepository;
	}

	 @Scheduled(fixedDelay = "${jobs.external-monitoring-service.update.fixedDelay}", initialDelay = "${jobs.external-monitoring-service.update.initialDelay}")
	    void execute() {
		 logger.info("Task: Executing ExternalMonitoringServiceJob");

		 Map<String, DataStateHeartbeat> serviceMap = new ConcurrentHashMap<>();
		 Map<String, DataStateContainer> containerMap = new ConcurrentHashMap<>();
		 List<DataStateInstanceInfo> instanceInfoList = new ArrayList<>();
		 HeartbeatStateType overallState = HeartbeatStateType.ok;
		 List<Heartbeat> heartbeats = Collections
				 .synchronizedList(heartbeatRepository.findAll(sortingAndOrderArgumentsHeartbeat));
		 for (Heartbeat heartbeat : heartbeats) {
			 DataStateHeartbeat dataStateHeartbeat = new DataStateHeartbeat(
					 heartbeat.getSource(),
					 heartbeat.getTimestamp(),
					 heartbeat.getMessageType(),
					 heartbeat.getSubType(),
					 Enum.valueOf(HeartbeatStateType.class, heartbeat.getState()),
					 heartbeat.getTopic()
			 );
			 serviceMap.put(heartbeat.getSource(), dataStateHeartbeat);
			 HeartbeatStateType heartbeatState = Enum.valueOf(HeartbeatStateType.class, heartbeat.getState());
			 if (heartbeatState.compareTo(overallState) > 0) {
				 overallState = heartbeatState;
			 }
		 }
		 List<Container> containers = Collections
				 .synchronizedList(containerRepository.findAll(sortingAndOrderArgumentsContainer));
		 for (Container container : containers) {
			 DataStateContainer dataStateContainer = new DataStateContainer(
					 container.getContainerId(),
					 container.getContainerName(),
					 container.getImageId(),
					 container.getStatus(),
					 container.getState(),
					 container.getCreated(),
					 container.getTimestamp(),
					 container.getCpuPercent(),
					 container.getMemPercent()
			 );
			 containerMap.put(container.getContainerName(), dataStateContainer);
		 }
		 List<InstanceInfo> instanceInfos = Collections
				 .synchronizedList(instanceInfoRepository.findAll(sortingAndOrderArgumentsInstanceInfo));
		 InstanceInfo latestInstanceInfo = null;
		 for (InstanceInfo instanceInfo : instanceInfos) {
			 if (latestInstanceInfo == null) {
				 latestInstanceInfo = instanceInfo;
			 } else {
				 Instant latest = Instant.parse(latestInstanceInfo.getTrialStart());
				 Instant instant = Instant.parse(instanceInfo.getTrialStart());
				 if (latest.isBefore(instant)) {
					 latestInstanceInfo = instanceInfo;
				 }
			 }
			 DataStateInstanceInfo  dataStateInstanceInfo = null;
			 try {
				 dataStateInstanceInfo = new DataStateInstanceInfo(
						 instanceInfo.getTrialId(),
						 instanceInfo.getTrialName(),
						 instanceInfo.getTrialCreated(),
						 instanceInfo.getExperimenter(),
						 objectMapper.readValue(instanceInfo.getSubjects(), Argument.listOf(String.class)),
						 instanceInfo.getTrialNumber(),
						 instanceInfo.getGroupNumber(),
						 instanceInfo.getStudyNumber(),
						 instanceInfo.getCondition(),
						 objectMapper.readValue(instanceInfo.getNotes(), Argument.listOf(String.class)),
						 instanceInfo.getExperimentId(),
						 instanceInfo.getExperimentName(),
						 instanceInfo.getExperimentCreated(),
						 instanceInfo.getExperimentAuthor(),
						 instanceInfo.getExperimentMission(),
						 instanceInfo.getMapName(),
						 instanceInfo.getMapBlockFilename(),
						 objectMapper.readValue(instanceInfo.getPlayers(), Argument.listOf(ClientInfoItem.class)),
						 instanceInfo.getTeamId(),
						 objectMapper.readValue(instanceInfo.getInterventionAgents(), Argument.listOf(String.class)),
						 objectMapper.readValue(instanceInfo.getObservers(), Argument.listOf(String.class)),
						 instanceInfo.getTrialStart(),
						 instanceInfo.getTrialStop(),
						 instanceInfo.getAutoExportBegin(),
						 instanceInfo.getAutoExportEnd(),
						 instanceInfo.getMissionTimer(),
						 instanceInfo.getMissionElapsedMilliseconds(),
						 instanceInfo.getMissionName(),
						 instanceInfo.getMissionState(),
						 instanceInfo.getMissionStateChangeOutcome()
				 );
			 } catch (IOException e) {
				 throw new RuntimeException(e);
			 }
			 instanceInfoList.add(dataStateInstanceInfo);
		 }
		 Optional<InstanceState> instanceStateOptional = instanceStateRepository.findByHostIp(HOST_IP);
		 DataStateInstanceState dataStateInstanceState = new DataStateInstanceState();
		 if (instanceStateOptional.isPresent()) {
			 InstanceState instanceState = instanceStateOptional.get();
			 dataStateInstanceState = new DataStateInstanceState(
					 instanceState.getReady(),
					 instanceState.getDown(),
					 instanceState.getDownReason(),
					 getMessage(latestInstanceInfo, instanceState)
			 );
		 }
		 Header header = new Header(Instant.now().toString(), EXTERNAL_SCHEMA_HEADER_MESSAGETYPE, EXTERNAL_SCHEMA_HEADER_VERSION);
		 Msg msg = new Msg(EXTERNAL_SCHEMA_MSG_SUBTYPE, EXTERNAL_SCHEMA_MSG_SOURCE, NOT_SET, NOT_SET, EXTERNAL_SCHEMA_MSG_VERSION, null, null, null);
		 DataState dataState = new DataState(HOST_IP, overallState, serviceMap, containerMap, instanceInfoList, dataStateInstanceState);
		 MessageState messageState = new MessageState(header, msg, dataState);
		 logger.info("Services: [{}], Containers: [{}], InstanceInfo: [{}]", serviceMap.size(), containerMap.size(), instanceInfoList.size());
		 externalMqttPublisher.publish(messageState);
	 }

	private String getMessage(InstanceInfo latestInstanceInfo, InstanceState instanceState) {
		if (instanceState.getReady().equals("")) {
			return InstanceStateType.NOT_READY.toString();
		} else {
			String message = InstanceStateType.WAITING_FOR_PLAYERS.toString();
			if (latestInstanceInfo == null) {
				return message;
			}
			List<ClientInfoItem> players = new ArrayList<>();
			try {
				players = objectMapper.readValue(latestInstanceInfo.getPlayers(), Argument.listOf(ClientInfoItem.class));
			} catch (IOException e) {
				message = InstanceStateType.WAITING_FOR_PLAYERS.toString();
			};
			if (players.size() > 0) {
				message = InstanceStateType.PLAYERS_ALLOCATED.toString();
			}
			if (latestInstanceInfo.getMissionState().equalsIgnoreCase("start")) {
				message = InstanceStateType.GAME_IN_PROGRESS.toString();
			}
			if (!latestInstanceInfo.getAutoExportEnd().equals("")) {
				message = InstanceStateType.EXPORT_COMPLETED.toString();
			}
			// need instance down completed message for next item.
			return message;
		}
	}
}
