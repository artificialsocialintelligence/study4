package asist.aptima.com.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.serde.annotation.Serdeable;

@Serdeable
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataInstanceDown {

	@JsonProperty("reason")
	private String reason;

	@JsonCreator
	public DataInstanceDown(
			String reason
			) {
		this.reason = reason;
	}

	public DataInstanceDown() {
		// TODO Auto-generated constructor stub
	}

	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}

}
