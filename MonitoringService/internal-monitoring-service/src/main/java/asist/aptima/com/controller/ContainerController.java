package asist.aptima.com.controller;

import asist.aptima.com.domain.Container;
import asist.aptima.com.repository.ContainerRepository;
import asist.aptima.com.utility.SortingAndOrderArgumentsContainer;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import io.micronaut.http.annotation.Post;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;

import javax.validation.Valid;
import java.util.List;

@ExecuteOn(TaskExecutors.IO)  
@Controller("/containers")
public class ContainerController {

    private final ContainerRepository containerRepository;

    ContainerController(ContainerRepository containerRepository) {
        this.containerRepository = containerRepository;
    }

    @Get(value = "/list{?args*}")
    List<Container> list(@Valid SortingAndOrderArgumentsContainer args) {
        return containerRepository.findAll(args);
    }

    @Get(value = "/{containerName}")
    Container findByName(@PathVariable String containerName) {
        return containerRepository.findByName(containerName).orElse(null);
    }

    @Post(value="monitoring/enable")
    void monitoringEnable() {
        containerRepository.monitoringEnable();
    }

    @Post(value="monitoring/disable")
    void monitoringDisable() {
        containerRepository.monitoringDisable();
    }
}
