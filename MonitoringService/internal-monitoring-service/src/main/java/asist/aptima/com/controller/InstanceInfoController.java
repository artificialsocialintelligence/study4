package asist.aptima.com.controller;

import asist.aptima.com.domain.Container;
import asist.aptima.com.domain.InstanceInfo;
import asist.aptima.com.repository.ContainerRepository;
import asist.aptima.com.repository.InstanceInfoRepository;
import asist.aptima.com.utility.SortingAndOrderArgumentsContainer;
import asist.aptima.com.utility.SortingAndOrderArgumentsInstanceInfo;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import io.micronaut.http.annotation.Post;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;

import javax.validation.Valid;
import java.util.List;

@ExecuteOn(TaskExecutors.IO)  
@Controller("/instance/info")
public class InstanceInfoController {

    private final InstanceInfoRepository instanceInfoRepository;

    InstanceInfoController(InstanceInfoRepository instanceInfoRepository) {
        this.instanceInfoRepository = instanceInfoRepository;
    }

    @Get(value = "/list{?args*}")
    List<InstanceInfo> list(@Valid SortingAndOrderArgumentsInstanceInfo args) {
        return instanceInfoRepository.findAll(args);
    }
}
