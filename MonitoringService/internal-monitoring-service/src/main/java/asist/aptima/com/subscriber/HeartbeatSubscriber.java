package asist.aptima.com.subscriber;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.Optional;

import org.eclipse.paho.mqttv5.client.MqttAsyncClient;
import org.eclipse.paho.mqttv5.common.MqttException;
import org.eclipse.paho.mqttv5.common.MqttMessage;
import org.eclipse.paho.mqttv5.common.MqttPersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hivemq.client.mqtt.mqtt5.Mqtt5Client;

import asist.aptima.com.domain.Heartbeat;
import asist.aptima.com.model.MessageHeartbeat;
import asist.aptima.com.publisher.ExternalMqttPublisher;
import asist.aptima.com.repository.HeartbeatRepository;
import asist.aptima.com.usecase.HeartbeatUseCase;
import io.micronaut.mqtt.annotation.MqttSubscriber;
import io.micronaut.mqtt.annotation.Topic;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import io.micronaut.serde.ObjectMapper;
import jakarta.inject.Named;

@ExecuteOn(TaskExecutors.IO)
@MqttSubscriber
public class HeartbeatSubscriber {

	private static final Logger logger = LoggerFactory.getLogger(HeartbeatSubscriber.class);
	private ObjectMapper objectMapper;
	private HeartbeatRepository heartbeatRepository;
	private HeartbeatUseCase heartbeatUseCase;
	
	public HeartbeatSubscriber(ObjectMapper objectMapper, HeartbeatRepository heartbeatRepository, HeartbeatUseCase heartbeatUseCase) {
		this.objectMapper = objectMapper;
		this.heartbeatRepository = heartbeatRepository;
		this.heartbeatUseCase = heartbeatUseCase;
	}

    @Topic("status/+/heartbeats")
    public void receive(byte[] data, @Topic String topic) {  
    	MessageHeartbeat messageHeartbeat;
		try {
			String message = new String(data, StandardCharsets.UTF_8);
			messageHeartbeat = objectMapper.readValue(message, MessageHeartbeat.class);

			String timestamp = messageHeartbeat.getHeader().getTimestamp();
			String messageType = messageHeartbeat.getHeader().getMessageType();
			String subType = messageHeartbeat.getMsg().getSubType();
			String source = messageHeartbeat.getMsg().getSource();
			String state = messageHeartbeat.getData().getState().toString();
			logger.debug(MessageFormat.format("Hearbeat [{0}] : [{1}] at [{2}]", source, state, timestamp));
			
			Optional<Heartbeat> heartbeat = heartbeatRepository.findBySource(source);
			if (heartbeat.isEmpty()) {
				logger.debug(MessageFormat.format("Adding new heartbeat from [{0}] : [{1}] at [{2}]", source, state, timestamp));
				heartbeatRepository.save(messageHeartbeat, topic);
			} else {
				logger.debug(MessageFormat.format("Updating heartbeat from [{0}] : [{1}] at [{2}]", source, state, timestamp));
				Heartbeat update = heartbeat.get();
				long id = update.getId();
				update.setTimestamp(timestamp);
				update.setMessageType(messageType);
				update.setSubType(subType);
				update.setSource(source);
				update.setState(state);
				heartbeatRepository.update(update);
			}
			heartbeatUseCase.received(source);
						
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e.getMessage());
		}			
    }
}
