package asist.aptima.com.configuration;

import java.util.Properties;

import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;

public interface ExternalMqttConfiguration {

    String getServerHost();
    int getServerPort();
    String getClientId();
}