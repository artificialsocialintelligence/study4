package asist.aptima.com.domain;

import java.io.IOException;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import asist.aptima.com.subscriber.HeartbeatSubscriber;
import io.micronaut.serde.ObjectMapper;
import io.micronaut.serde.annotation.Serdeable;
import jakarta.inject.Inject;

@Serdeable
@Entity
@Table(name = "heartbeat")
public class Heartbeat {
	
	private static final Logger logger = LoggerFactory.getLogger(Heartbeat.class);
	
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @JsonProperty("id")
    private Long id;

    @Version
    @JsonIgnore
    private Long version;

    @NotNull
    @Column(name="source", nullable=false, unique=true)
    @JsonProperty("source")
    private String source;

    @NotNull
    @Column(name="timestamp", nullable=false)
    @JsonProperty("timestamp")
    private String timestamp;
    
    @NotNull
    @Column(name="message_type", nullable=false)
    @JsonProperty("message_type")
    private String messageType;
    
    @NotNull
    @Column(name="sub_type", nullable=false)
    @JsonProperty("sub_type")
    private String subType;
    
    @NotNull
    @Column(name="state", nullable=false)
    @JsonProperty("state")
    private String state;
    
    @NotNull
    @Column(name="topic", nullable=false)
    @JsonProperty("topic")
    private String topic;

    public Heartbeat() {}

    public Heartbeat(
    		@NotNull String source,
    		@NotNull String timestamp,
    		@NotNull String messageType,
    		@NotNull String subType,
    		@NotNull String state,
    		@NotNull String topic
                ) {
        this.source = source;
        this.timestamp = timestamp;
        this.messageType = messageType;
        this.subType = subType;
        this.state = state;
        this.topic = topic;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
    
    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }
    
    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }
    
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
    
    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    @Override
    public String toString() {
        return "{" +
                "\"id\": \"" + id + "\"," +
                "\"source\": \"" + source + "\"," +
                "\"timestamp\": \"" + timestamp + "\"," +
                "\"message_type\": \"" + messageType + "\"," +
                "\"sub_type\": \"" + subType + "\"," +
                "\"state\": \"" + state + "\"," +
                "\"topic\": \"" + topic + "\"" +
                "}";
    }
}
