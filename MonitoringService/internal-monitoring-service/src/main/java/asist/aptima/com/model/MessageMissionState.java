package asist.aptima.com.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.serde.annotation.Serdeable;

@Serdeable
public class MessageMissionState {

	@JsonProperty("header")
	private Header header;
	@JsonProperty("msg")
	private Msg msg;
	@JsonProperty("data")
	private DataMissionState data;

	@JsonCreator
	public MessageMissionState(
			Header header,
			Msg msg,
			DataMissionState data
			) {
		this.header = header;
		this.msg = msg;
		this.data = data;
	}

	public MessageMissionState() {
		// TODO Auto-generated constructor stub
	}

	public Header getHeader() {
		return header;
	}
	public void setHeader(Header header) {
		this.header = header;
	}

	public Msg getMsg() {
		return msg;
	}
	public void setMsg(Msg msg) {
		this.msg = msg;
	}

	public DataMissionState getData() {
		return data;
	}
	public void setData(DataMissionState data) {
		this.data = data;
	}
}
