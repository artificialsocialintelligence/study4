package asist.aptima.com.job;

import asist.aptima.com.domain.InstanceInfo;
import asist.aptima.com.domain.InstanceState;
import asist.aptima.com.event.ContainerMonitoring;
import asist.aptima.com.repository.InstanceStateRepository;
import asist.aptima.com.repository.InstanceStateRepositoryImpl;
import asist.aptima.com.usecase.ContainerUseCase;
import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.model.Container;
import io.micronaut.context.annotation.Property;
import io.micronaut.scheduling.annotation.Scheduled;
import jakarta.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@Singleton
public class ListContainersJob {
	private static final Logger logger = LoggerFactory.getLogger(ListContainersJob.class);
	private final ContainerUseCase containerUseCase;
    private final DockerClient dockerClient;
	private final ContainerMonitoring containerMonitoring;
	private final InstanceStateRepository instanceStateRepository;

	@Property(name = "container.core-services")
	private Set<String> CONTAINER_CORE_SERVICES;

	@Property(name = "host.ip", defaultValue = "localhost")
	private String HOST_IP;

	public ListContainersJob(DockerClient dockerClient, ContainerUseCase containerUseCase, ContainerMonitoring containerMonitoring, InstanceStateRepository instanceStateRepository) {
		this.dockerClient = dockerClient;
		this.containerUseCase = containerUseCase;
		this.containerMonitoring = containerMonitoring;
		this.instanceStateRepository = instanceStateRepository;
	}

	@Scheduled(fixedDelay = "${jobs.container.list.fixedDelay}", initialDelay = "${jobs.container.list.initialDelay}")
    void execute() {
		logger.info("Task: Executing ListContainersJob");

		List<Container> containers = dockerClient.listContainersCmd().withShowAll(true).exec();
		containers.sort(new ContainerSort());
		if (containerMonitoring.getContainerMonitoring()) {
			containers.forEach(containerUseCase::scheduleContainer);
		} else {
			if (containerUseCase.schedulesSize() > 0) {
				containerUseCase.cancelAllSchedules();
			}
		}
		boolean instantUp = coreServiceCheck(containers.stream().map(container -> {
			return container.getNames()[0].substring(1);
		}).collect(Collectors.toSet()));
		Optional<InstanceState> instanceStateOptional = instanceStateRepository.findByHostIp(HOST_IP);
		if (instanceStateOptional.isEmpty()) {
			String now = Instant.now().toString();
			logger.info("Instance State [{}] at [{}]!", instantUp ? "ready" : "not ready", now);
			InstanceState instanceState = new InstanceState(HOST_IP, instantUp ? now : "", "", "");
			instanceStateRepository.save(instanceState);
		} else {
			String now = Instant.now().toString();
			logger.info("Updating Instance State [{}] at [{}]!", instantUp ? "ready" : "not ready", now);
			InstanceState instanceState = instanceStateOptional.get();
			if (instantUp) {
				instanceState.setReady(now);
			} else {
				instanceState.setReady("");
			}
			instanceStateRepository.update(instanceState);
		}
	}

	private boolean coreServiceCheck(Set<String> containerNames) {
		return containerNames.containsAll(CONTAINER_CORE_SERVICES);
	}

	static class ContainerSort implements Comparator<Container> {
		public int compare(Container a, Container b) {
			return (int) (a.getCreated() - b.getCreated());
		}
	}
}
