package asist.aptima.com.subscriber;

import asist.aptima.com.domain.InstanceInfo;
import asist.aptima.com.domain.InstanceState;
import asist.aptima.com.model.MessageInstanceDown;
import asist.aptima.com.model.MessageMissionState;
import asist.aptima.com.repository.HeartbeatRepository;
import asist.aptima.com.repository.InstanceInfoRepository;
import asist.aptima.com.repository.InstanceStateRepository;
import io.micronaut.context.annotation.Property;
import io.micronaut.mqtt.annotation.MqttSubscriber;
import io.micronaut.mqtt.annotation.Topic;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import io.micronaut.serde.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.Optional;

@ExecuteOn(TaskExecutors.IO)
@MqttSubscriber
public class InstanceDownSubscriber {

	private static final Logger logger = LoggerFactory.getLogger(InstanceDownSubscriber.class);
	private final ObjectMapper objectMapper;
	private final InstanceStateRepository instanceStateRepository;

	@Property(name = "host.ip", defaultValue = "localhost")
	private String HOST_IP;

	public InstanceDownSubscriber(ObjectMapper objectMapper, HeartbeatRepository heartbeatRepository, InstanceStateRepository instanceStateRepository) {
		this.objectMapper = objectMapper;
		this.instanceStateRepository = instanceStateRepository;
	}

    @Topic("control/scaling/instance_down")
    public void receive(byte[] data, @Topic String topic) {
		MessageInstanceDown messageInstanceDown;
		try {
			String message = new String(data, StandardCharsets.UTF_8);
			messageInstanceDown = objectMapper.readValue(message, MessageInstanceDown.class);

			String timestamp = messageInstanceDown.getHeader().getTimestamp();
			String messageType = messageInstanceDown.getHeader().getMessageType();
			String subType = messageInstanceDown.getMsg().getSubType();
			String source = messageInstanceDown.getMsg().getSource();
			String trialId = messageInstanceDown.getMsg().getTrialId();
			String reason = messageInstanceDown.getData().getReason();

			logger.info("Instance down [{}].", reason);

			Optional<InstanceState> instanceStateOptional = instanceStateRepository.findByHostIp(HOST_IP);
			if (instanceStateOptional.isEmpty()) {
				logger.error("Host IP [{}] does not exist for Instance State!", HOST_IP);
			} else {
				logger.info("Updating Instance Down [{}] for Host IP [{}].", reason, HOST_IP);
				InstanceState instanceState = instanceStateOptional.get();
				instanceState.setDown(Instant.now().toString());
				instanceState.setDownReason(reason);
				instanceStateRepository.update(instanceState);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e.getMessage());
		}
    }
}
