package asist.aptima.com.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.serde.annotation.Serdeable;

@Serdeable
public class MessageInstanceDown {

	@JsonProperty("header")
	private Header header;
	@JsonProperty("msg")
	private Msg msg;
	@JsonProperty("data")
	private DataInstanceDown data;

	@JsonCreator
	public MessageInstanceDown(
			Header header,
			Msg msg,
			DataInstanceDown data
			) {
		this.header = header;
		this.msg = msg;
		this.data = data;
	}

	public MessageInstanceDown() {
		// TODO Auto-generated constructor stub
	}

	public Header getHeader() {
		return header;
	}
	public void setHeader(Header header) {
		this.header = header;
	}

	public Msg getMsg() {
		return msg;
	}
	public void setMsg(Msg msg) {
		this.msg = msg;
	}

	public DataInstanceDown getData() {
		return data;
	}
	public void setData(DataInstanceDown data) {
		this.data = data;
	}
}
