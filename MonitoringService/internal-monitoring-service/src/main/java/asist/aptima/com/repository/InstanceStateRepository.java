package asist.aptima.com.repository;

import asist.aptima.com.domain.InstanceInfo;
import asist.aptima.com.domain.InstanceState;
import asist.aptima.com.utility.SortingAndOrderArgumentsInstanceInfo;
import asist.aptima.com.utility.SortingAndOrderArgumentsInstanceState;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

public interface InstanceStateRepository {

    Optional<InstanceState> findById(long id);
    List<InstanceState> findAll(@NotNull SortingAndOrderArgumentsInstanceState args);
    Optional<InstanceState> findByHostIp(String hostIp);
    InstanceState save(@NotBlank InstanceState instanceInfo);
    InstanceState update(@NotBlank InstanceState instanceInfo);
    void deleteById(long id);
}
