package asist.aptima.com.subscriber;

import asist.aptima.com.domain.Heartbeat;
import asist.aptima.com.domain.InstanceInfo;
import asist.aptima.com.model.ClientInfoItem;
import asist.aptima.com.model.MessageHeartbeat;
import asist.aptima.com.model.MessageTrial;
import asist.aptima.com.repository.HeartbeatRepository;
import asist.aptima.com.repository.InstanceInfoRepository;
import asist.aptima.com.usecase.HeartbeatUseCase;
import io.micronaut.mqtt.annotation.MqttSubscriber;
import io.micronaut.mqtt.annotation.Topic;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import io.micronaut.serde.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

@ExecuteOn(TaskExecutors.IO)
@MqttSubscriber
public class TrialSubscriber {

	private static final Logger logger = LoggerFactory.getLogger(TrialSubscriber.class);
	private final ObjectMapper objectMapper;
	private final InstanceInfoRepository instanceInfoRepository;

	public TrialSubscriber(ObjectMapper objectMapper, HeartbeatRepository heartbeatRepository, InstanceInfoRepository instanceInfoRepository) {
		this.objectMapper = objectMapper;
		this.instanceInfoRepository = instanceInfoRepository;
	}

    @Topic("trial")
    public void receive(byte[] data, @Topic String topic) {
    	MessageTrial messageTrial;
		try {
			String message = new String(data, StandardCharsets.UTF_8);
			messageTrial = objectMapper.readValue(message, MessageTrial.class);

			String timestamp = messageTrial.getHeader().getTimestamp();
			String messageType = messageTrial.getHeader().getMessageType();
			String subType = messageTrial.getMsg().getSubType();
			String source = messageTrial.getMsg().getSource();
			String trialId = messageTrial.getMsg().getTrialId();
			String experimentId = messageTrial.getMsg().getExperimentId();
			String trialName = messageTrial.getData().getName();
			String trialDate = messageTrial.getData().getDate();
			String experimenter = messageTrial.getData().getExperimenter();
			List<String> subjects = messageTrial.getData().getSubjects();
			String trialNumber = messageTrial.getData().getTrialNumber();
			String groupNumber = messageTrial.getData().getGroupNumber();
			String studyNumber = messageTrial.getData().getStudyNumber();
			String condition = messageTrial.getData().getCondition();
			List<String> notes = messageTrial.getData().getNotes();
			String experimentName = messageTrial.getData().getExperimentName();
			String experimentDate = messageTrial.getData().getExperimentDate();
			String experimentAuthor = messageTrial.getData().getExperimentAuthor();
			String experimentMission = messageTrial.getData().getExperimentMission();
			String mapName = messageTrial.getData().getMapName();
			String mapBlockFilename = messageTrial.getData().getMapBlockFilename();
			List<ClientInfoItem> clientInfo = messageTrial.getData().getClientInfo();
			String teamId = messageTrial.getData().getTeamId();
			List<String> interventionAgents = messageTrial.getData().getInterventionAgents();
			List<String> observers = messageTrial.getData().getObservers();

			logger.info("Trial [{}] : [{}] at [{}].", trialId, subType, timestamp);

			Optional<InstanceInfo> instanceInfoOptional = instanceInfoRepository.findByTrialId(trialId);
			if (instanceInfoOptional.isEmpty()) {
				if (subType.equalsIgnoreCase("start")) {
					logger.info("Adding new instance info for [{}] : [{}].", trialId, subType);
					InstanceInfo instanceInfo = new InstanceInfo(
							trialId,
							trialName,
							trialDate,
							experimenter,
							objectMapper.writeValueAsString(subjects),
							trialNumber,
							groupNumber,
							studyNumber,
							condition,
							objectMapper.writeValueAsString(notes),
							experimentId,
							experimentName,
							experimentDate,
							experimentAuthor,
							experimentMission,
							mapName,
							mapBlockFilename,
							objectMapper.writeValueAsString(clientInfo),
							teamId,
							objectMapper.writeValueAsString(interventionAgents),
							objectMapper.writeValueAsString(observers),
							Instant.now().toString()
					);
					instanceInfoRepository.save(instanceInfo);
				} else {
					logger.error("Expected Trial [START] but received {}!", subType);
				}
			} else {
				if (subType.equalsIgnoreCase("stop")) {
					logger.info("Updating new instance info for [{}] : [{}].", trialId, subType);
					InstanceInfo instanceInfo = instanceInfoOptional.get();
					instanceInfo.setTrialStop(Instant.now().toString());
					instanceInfoRepository.update(instanceInfo);
				} else {
					logger.error("Expected Trial [STOP] but received {}!", subType);
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e.getMessage());
		}
    }
}
