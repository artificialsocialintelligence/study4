package asist.aptima.com.repository;

import asist.aptima.com.domain.Container;
import asist.aptima.com.utility.SortingAndOrderArgumentsContainer;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

public interface ContainerRepository {

    Optional<Container> findById(long id);
	Optional<Container> findByName(String name);
    List<Container> findAll(@NotNull SortingAndOrderArgumentsContainer args);
    Container save(Container container);
    Container update(Container container);
    void deleteById(long id);
    void deleteAll();
    void monitoringEnable();
    void monitoringDisable();

}
