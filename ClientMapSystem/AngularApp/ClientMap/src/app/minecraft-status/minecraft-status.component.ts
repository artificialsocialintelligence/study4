import { Component, OnInit } from '@angular/core';
import { WebsocketService } from '../Services/websocket.service';

@Component({
  selector: 'app-minecraft-status',
  templateUrl: './minecraft-status.component.html',
  styleUrls: ['./minecraft-status.component.scss']
})
export class MinecraftStatusComponent implements OnInit {


  minecraft_url = "NOT_SET";
  constructor( public websocketService:WebsocketService) { }

  ngOnInit(): void {
    this.minecraft_url = window.location.hostname
  }

}
