import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MinecraftStatusComponent } from './minecraft-status.component';

describe('MinecraftStatusComponent', () => {
  let component: MinecraftStatusComponent;
  let fixture: ComponentFixture<MinecraftStatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MinecraftStatusComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MinecraftStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
