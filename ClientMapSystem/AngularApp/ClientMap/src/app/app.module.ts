import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { SocketIoModule,SocketIoConfig} from 'ngx-socket-io';
import { MatTooltipModule } from '@angular/material/tooltip'; 
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ActionDialogComponent } from './action-dialog/action-dialog.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressBarModule } from '@angular/material/progress-bar'
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatRadioModule } from '@angular/material/radio'
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { InteractionPaneComponent } from './interaction-pane/interaction-pane.component';
import { MapParentComponent } from './map-parent/map-parent.component';
import { SurveyComponent } from './survey/survey.component';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { OrderByPipe } from './shared/pipes/order-by.pipe';
import {MatSelectModule} from '@angular/material/select';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatGridListModule } from '@angular/material/grid-list';
import { MinecraftStatusComponent } from './minecraft-status/minecraft-status.component';
import { UserDialogComponent } from './user-dialog/user-dialog.component';
import { TeamContinuePromptComponent } from './team-continue-prompt/team-continue-prompt.component';
import { MapV2Component } from './map-v2/map-v2.component';
import { MainComponent } from './main/main.component';
import { MapLabelsComponent } from './map-labels/map-labels.component';
import { PlanningPaneComponent } from './planning-pane/planning-pane.component';
import { StorePaneComponent } from './store-pane/store-pane.component';
import { TextchatComponent } from './textchat/textchat.component';
import { AdvisorWidgetComponent } from './advisor-widget/advisor-widget.component';
import { MissionBriefComponent } from './mission-brief/mission-brief.component';
import { ToolDialogComponent } from './tool-dialog/tool-dialog.component';
import { IconUpdaterComponent } from './icon-updater/icon-updater.component';
import { TeamDissolvePromptComponent } from './team-dissolve-prompt/team-dissolve-prompt.component';
import { FlagPlacerMenuComponent } from './flag-placer-menu/flag-placer-menu.component';
import { TrialSummaryComponent } from './trial-summary/trial-summary.component';

import { FaqHelpComponent } from './faq-help/faq-help.component';

const socket_io_config: SocketIoConfig = { url: `${location.protocol}//${location.host}`, options: { transports: ['websocket',"polling"], path: `${location.pathname}socket.io` } };

@NgModule({
  declarations: [
    
    AppComponent,
    ActionDialogComponent,
    LoginComponent,    
    InteractionPaneComponent,
    MapParentComponent,
    SurveyComponent,
    OrderByPipe,    
    MinecraftStatusComponent,
    UserDialogComponent,
    TeamContinuePromptComponent,
    MapV2Component,
    MainComponent,
    MapLabelsComponent,
    PlanningPaneComponent,
    StorePaneComponent,
    TextchatComponent,
    AdvisorWidgetComponent,
    MissionBriefComponent,
    ToolDialogComponent,
    IconUpdaterComponent,
    TeamDissolvePromptComponent,
    FlagPlacerMenuComponent,
    TrialSummaryComponent,    
    FaqHelpComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    // DEBUG
    //SocketIoModule.forRoot({ url: 'ws://' + environment.wspath + ':3080', options: { transports: ['websocket'], allowUpgrades : true } }),
    // PROD
    SocketIoModule.forRoot(socket_io_config),
    BrowserAnimationsModule,
    MatDialogModule,
    MatSnackBarModule,
    MatCardModule,
    FormsModule,
    MatCheckboxModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatTabsModule,
    MatMenuModule,
    HttpClientModule,
    FlexLayoutModule,
    MatIconModule,   
    MatFormFieldModule,
    MatSelectModule,
    MatGridListModule,
    MatProgressBarModule,
    MatRadioModule,
    MatTooltipModule
  ],
  providers: [

  ],
  entryComponents:[
    ActionDialogComponent,
    ToolDialogComponent,
    UserDialogComponent,
    MissionBriefComponent
  ],
  bootstrap: [AppComponent]
  
})
export class AppModule { 

  
}
