import { TestBed } from '@angular/core/testing';

import { BombSensorService } from './bomb-sensor.service';

describe('BombSensorService', () => {
  let service: BombSensorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BombSensorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
