import { EventEmitter, Injectable } from '@angular/core';
import { FlagData } from '../CustomClasses/FlagData';
import { RenderedIconWrapper } from '../CustomClasses/RenderedIconWrapper';
import { BeaconService } from './beacon.service';
import { SessionService } from './session.service';


@Injectable({
  providedIn: 'root'
})
export class IconSelectorService {

  lastSelectedIcon:RenderedIconWrapper = null;
  currentSelectedIcon:RenderedIconWrapper = null; 

  flagPlacingMode = false;
  
  incomingFlagEmitter:EventEmitter<any> = new EventEmitter();
  
  flagKeeperMap:Map<string,RenderedIconWrapper[]> = new Map<string,RenderedIconWrapper[]>();
  
  iconSelectEmitter:EventEmitter<RenderedIconWrapper> = new EventEmitter<RenderedIconWrapper>();

  currentHoveredOverIcon = null;

  constructor( 
    public beaconService:BeaconService,

    
  ) {                 

  }

  setCurrentIcon(icon:RenderedIconWrapper){

    this.lastSelectedIcon = this.currentSelectedIcon;
    this.currentSelectedIcon = icon;    
    if(this.lastSelectedIcon!= null){
      const lastIconImageElement = document.getElementById(this.lastSelectedIcon.node.id+"iconImage");
      lastIconImageElement.style.border = "";      
    }
    const iconImageElement = document.getElementById(this.currentSelectedIcon.node.id+"iconImage");   
    if( iconImageElement!=undefined && iconImageElement!=null){
      iconImageElement.style.border = "5px solid #7d38a1";
    }        

    this.iconSelectEmitter.emit(icon);

  }
  
  clearIconSelection(){

    if(this.currentSelectedIcon!= null){
      const iconImageElement = document.getElementById(this.currentSelectedIcon.node.id+"iconImage");
      iconImageElement.style.border = "";
      
    }
    if(this.lastSelectedIcon!= null){
      const lastIconImageElement = document.getElementById(this.lastSelectedIcon.node.id+"iconImage");
      lastIconImageElement.style.border = "";      
    }

    this.currentSelectedIcon=null;  
    this.lastSelectedIcon=null; 
  }
  
  processIncomingIconUpdate(message){

    setTimeout( ()=>{

      //console.log(message);
      const pid = message.additional_info.owner;
      //console.log(pid);
      const type = (message.type as string);
      //console.log(type);
      const iconId = message.element_id
      //console.log(iconId);    
      //console.log(pid + " : " + type);

      let iconWrapper:RenderedIconWrapper; 

      const element = document.getElementById(iconId);
      if(element != null){
        if( type.toLocaleLowerCase().localeCompare("flag")==0 ){      
          //console.log("It's a flag.");
          const array:RenderedIconWrapper[] = this.flagKeeperMap.get(pid)
          //console.log(this.flagKeeperMap)
          //console.log(array)
          for(let i=0; i<array.length; i++){
            if( array[i].node.id.localeCompare(iconId)==0 ){
              iconWrapper = array[i];
              break;
            }
          }
          const flagData:FlagData = iconWrapper.data as FlagData;
          flagData.content = (message.additional_info.content as string); 
          flagData.schedule_minute = (message.additional_info.schedule_minute as number);
          flagData.schedule_second = (message.additional_info.schedule_second as number);        
        } 
        
        else if(type.toLocaleLowerCase().localeCompare("bomb_beacon")==0){
    
          //console.log("It's a bomb beacon.");
        
          const array:RenderedIconWrapper[] = this.beaconService.beaconKeeperMap.get(pid)
          //console.log(this.beaconService.beaconKeeperMap)
          //console.log(array)
          for(let i=0; i<array.length; i++){
            if( array[i].node.id.localeCompare(iconId)==0 ){
              iconWrapper = array[i];
              break;
            }
          }

          // THIS STUFF IN BOMB BEACON DATA DOESN'T CHANGE
          //const beaconData:BombBeaconData = iconWrapper.data as BombBeaconData;
          // beaconData.bomb_id = (message.additional_info.bomb_id as string);
          // beaconData.chained_id = (message.additional_info.chained_id as string);
          // beaconData.sequence = (message.additional_info.remaining_sequence as string);
          // beaconData.fuse_length = (message.additional_info.fuse_start_minute as string);      
          
          //console.log(beaconData)
        }
    
        else if(type.toLocaleLowerCase().localeCompare("hazard_beacon")==0){
    
          //console.log("It's a hazard beacon.");       
          const array:RenderedIconWrapper[] = this.beaconService.beaconKeeperMap.get(pid)
          //console.log(this.beaconService.beaconKeeperMap)
          //console.log(array)
          for(let i=0; i<array.length; i++){
            if( array[i].node.id.localeCompare(iconId)==0 ){
              iconWrapper = array[i];
              break;
            }
          }

          // THIS STUFF IN HAZARD BEACON DATA DOESN'T CHANGE
          // const beaconData:HazardBeaconData = iconWrapper.data as HazardBeaconData;
          // beaconData.message = (message.additional_info.text as string);         
          // console.log(beaconData)
        }
        
        iconWrapper.data.priority = (message.additional_info.prioritization as number);    
        iconWrapper.responses = new Map<string,string>(Object.entries(message.additional_info.responses));

        (document.getElementById(iconWrapper.node.id +"_priority") as HTMLParagraphElement).innerText = iconWrapper.data.priority.toString();
        //console.log("IconSelectorService icon after trying to update Icon : ")
        //console.log(iconWrapper) 

      }
      else{
        console.log(" The icon was not found. Skipping this update.")
      }

    },1000)
  }

  toggleFlagMode(){       
    
    this.flagPlacingMode = !this.flagPlacingMode;

    if(this.flagPlacingMode){
      (document.getElementById('flagButtonHighlight') as HTMLDivElement).style.backgroundColor='aqua';
    }
    else{
      (document.getElementById('flagButtonHighlight') as HTMLDivElement).style.backgroundColor='transparent';
    }
  }

  

  setFlagMode(value:boolean):boolean{
    this.flagPlacingMode = value;   
      (document.getElementById('flagButtonHighlight') as HTMLDivElement).style.backgroundColor='transparent';
    return value;
  }

  undoFlagPlacement(event:PointerEvent, pid, callSignCode):any{
    const target:HTMLButtonElement = event.target as HTMLButtonElement;
    const id =  target.id;
    //console.log(this.currentSelectedIcon.owner_pid)
    //console.log(pid)
    //console.log(this.currentSelectedIcon.owner_callsign)
    //console.log(callSignCode)
    if(this.currentSelectedIcon.owner_pid.localeCompare(pid)==0 ){
      
      let foundWrapper:RenderedIconWrapper = null;
      let wrapperIndex = 0;
      
      this.flagKeeperMap.get(pid).forEach( (wrapper)=>{
        if(wrapper.node.id.localeCompare(this.currentSelectedIcon.node.id)==0){
          foundWrapper = wrapper;
        }
        wrapperIndex++;
      })
      if(foundWrapper != null){      

        
        const undoFlagObject = {
          
          "participant_id":pid,
          "element_id": id,
          "parent_id":"Map",
          "type": "BUTTON",
          "x": target.clientLeft,
          "y": target.clientTop,
          "additional_info":{
            "meta_action":"UNDO_PLANNING_FLAG_PLACED",
            "meta_type":"WAYPOINT_FLAG", 
            "call_sign_code":callSignCode,      
            "flag_index" : foundWrapper.index,
            "flag_id": foundWrapper.node.id
          }      
      
        }

         // REMOVE FROM DOM AND STORAGE ARRAY
         const div:HTMLDivElement = document.getElementById('GridLayer') as HTMLDivElement;      
         div.removeChild( foundWrapper.node);
         this.flagKeeperMap.get(pid).splice(wrapperIndex,1)
         return undoFlagObject;

      }
     
    }
    else{
      alert("You cannot remove Icons owned by your teammates, only your own.")
    }
    
  }

  incomingUndoFlagPlacement(message:any){

    console.log("Incoming undo flag placement")
    
    setTimeout( ()=>{

      try{

        const pid = message.participant_id;
        const flag_id = message.additional_info.flag_id;
  
        let foundWrapper:RenderedIconWrapper = null;
        let wrapperIndex = 0;
        
        this.flagKeeperMap.get(pid).forEach( (wrapper)=>{
          if(wrapper.node.id.localeCompare(flag_id)==0){
            foundWrapper = wrapper;
          }
          wrapperIndex++;
        })
        if(foundWrapper != null){
          const elementToRemove = document.getElementById(foundWrapper.node.id) as HTMLDivElement;
          // REMOVE FROM DOM AND STORAGE ARRAY
          if(elementToRemove !== null && elementToRemove !== undefined){
            const div:HTMLDivElement = document.getElementById('GridLayer') as HTMLDivElement;      
            div.removeChild( foundWrapper.node);
            this.flagKeeperMap.get(pid).splice(wrapperIndex,1)   
          } 
        } 
      }
      catch(e){
        console.log(e)
      }

    },500)
    
      
  }
  
}
