import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { SessionService } from './session.service';

@Injectable({
  providedIn: 'root'
})
export class ConnectionService {

  isConnected:boolean = false;
  timeDisconnected:number = 0;
  disconnectDialogUp = false;
  timeToDisconnect = 0;
  setTimeToDisconnect = false;

  constructor( private sessionService:SessionService,
    public snackbar:MatSnackBar) {
    //console.log("Connection Service Starting.")
     
    this.startSyncAnConnectionTask()
  }

  setConnected(state:boolean){ 

    this.isConnected = state;
    if( state == false){
      if(!isNaN(this.sessionService.config.player_timeout_ms) && this.setTimeToDisconnect == false){
        this.timeToDisconnect = this.sessionService.config.player_timeout_ms
        this.setTimeToDisconnect = true;
      }
      setTimeout( ()=>{
        if( !this.isConnected ){
          alert( "You have disconnected for 3 minutes. Your client is considered timed out. You will now be returned to the Login Page.")    
          setTimeout(()=>{
            const url = 'https://'+this.sessionService.config.admin_stack_ip+":"+this.sessionService.config.admin_stack_port+'/SPSO/Login'
            window.location.href = url
          },3000)
        }
      },this.sessionService.config.player_timeout_ms);
      setInterval( ()=>{
        if( !this.isConnected ){          
          this.timeToDisconnect-=1000;
        }
      },1000);    
    }
    else{
      if(!isNaN(this.sessionService.config.player_timeout_ms) && this.setTimeToDisconnect == true){
        this.timeToDisconnect = this.sessionService.config.player_timeout_ms
      }
    }  
  }

  startSyncAnConnectionTask(){
   
      setInterval(()=>{
        if(!this.isConnected){
          this.timeDisconnected++;
          console.log("Time Disconnected : " + this.timeDisconnected); 
          //this.checkTimeDisconnected()
        }
        else{
          this.timeDisconnected = 0;
          //console.log("Connected.");
          //this.sessionService.syncLocalStorage();
        }
      },1000);
    
  }

  checkTimeDisconnected(){
    if(!this.disconnectDialogUp ){

      if( this.timeDisconnected>=5){
        const config:MatSnackBarConfig = new MatSnackBarConfig()
        config.verticalPosition = 'bottom';
        config.panelClass = "custom-snack";
        this.disconnectDialogUp = true;     
        const ref = this.snackbar.open ( 
          "Your socket connection has disconnected. Try refreshing this page to reconnect.","Ok" ,config
        );
        ref.afterDismissed().subscribe( ()=>{
          this.disconnectDialogUp = false; 
        });
        
      }

    }
    
  }

  
}
