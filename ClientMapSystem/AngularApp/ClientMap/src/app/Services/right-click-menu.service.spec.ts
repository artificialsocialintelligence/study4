import { TestBed } from '@angular/core/testing';

import { RightClickMenuService } from './right-click-menu.service';

describe('RightClickMenuService', () => {
  let service: RightClickMenuService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RightClickMenuService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
