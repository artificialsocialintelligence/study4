
import { EventEmitter, Injectable} from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { ClientMapConfig} from '../Interfaces/types';
import { SessionService } from './session.service';
import { ConnectionService } from './connection.service';
import { BeaconService } from './beacon.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { UserDialogComponent } from '../user-dialog/user-dialog.component';
import { SurveyComponent } from '../survey/survey.component';
import { TeamContinuePromptComponent } from '../team-continue-prompt/team-continue-prompt.component';
import { AgentIntervention, PositionEvent } from '../EventModels/event_models';
import { AdvisorService } from './advisor.service';
import { ToolService } from './tool.service';
import { IconSelectorService } from './icon-selector.service';
import { AppMode } from '../CustomClasses/Enums';
import { TeamDissolvePromptComponent } from '../team-dissolve-prompt/team-dissolve-prompt.component';
import { RenderedIconWrapper } from '../CustomClasses/RenderedIconWrapper';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { TrialSummaryComponent } from '../trial-summary/trial-summary.component';


@Injectable({
  providedIn: 'root'
})
export class WebsocketService  {

  config:ClientMapConfig;
  role_text;

  positionEmitter: EventEmitter<any> = new EventEmitter(); 
  trialActiveEmitter: EventEmitter<boolean> = new EventEmitter();
  missionStateEmitter: EventEmitter<string> = new EventEmitter();
  textInterventionEmitter: EventEmitter<any[]> = new EventEmitter(); 
  commBlackoutEmitter:EventEmitter<any> = new EventEmitter();
  trialInfoEmitter:EventEmitter<any> = new EventEmitter();
  minecraftLoadingEmitter:EventEmitter<any> = new EventEmitter();
  bombSensorEmitter:EventEmitter<[number,number,number,string]> = new EventEmitter();
  trialSummaryEmitter: EventEmitter<boolean> = new EventEmitter();

  socketInitialized = false;

  playerName: string = '';

  map: string = '';

  url = ""

  modLoadingProgress = 0
  minecraftContainerInitialized = false
  asistModIsReady= false
  trial_running = false
  
  // STAGES FOR INTERACTION PANE
  // maybe make these enums?


  trial_end_survey = true
  team_continue_prompt

  timeString = ""
  min
  sec

  disconnected = false;
  timeToDisconnect=60;  

  connectionEmitter:EventEmitter<any> = new EventEmitter();
  
  constructor( 
    public socket: Socket,
    public http:HttpClient,
    public dialog: MatDialog,
    public sessionService:SessionService,
    public connectionService:ConnectionService,
    private beaconService:BeaconService,
    private advisorService:AdvisorService, 
    private toolService:ToolService, 
    public iconSelectorService:IconSelectorService,
    private route: ActivatedRoute) {
      
      this.connectionEmitter.subscribe((bool)=>{
        if(bool){
          console.log("Calling parseAndEmitName from emitter ... but not cause crashy crash")
          //this.socket.emit("hello",{})
        }
      })
  }

  socketInit(){

    if(!this.socketInitialized){
      
      this.setupNamelessSocketHooks(); 
      
      //this.parseAndEmitClientNameToServer();

      this.setupAuthenticationAndSocketHooks();    

      this.keepAlivePing();  

      this.socketInitialized = true;
    }   
  
  }

  parseAndEmitClientNameToServer(){
    let name = this.route.snapshot.queryParams["player"];
    this.sessionService.assignedName = name;
    this.sessionService.messageIdsReceivedByClient;
    
    const payload = {
      "name":name,
      "receivedMessages":this.sessionService.messageIdsReceivedByClient
    }

    this.socket.emit('assign_name_to_socket',payload)
    console.log("Name emitted to server : " + name)
  }

  keepAlivePing() {

    setInterval(()=>{
      const clientStateObject = {
        playerName : this.sessionService.assignedName,
        receivedMessages : this.sessionService.messageIdsReceivedByClient
      }
      this.socket.emit('KeepAlive', clientStateObject )
    },5000)

  }

  setupNamelessSocketHooks(){

    try{

      this.socket.on("disconnect", (error) => {      
      
        console.log(error)
        console.log("socket.ondisconnect")
        this.disconnected = true;
        this.connectionService.setConnected(false);
        this.connectionEmitter.emit(false);
      
      });
  
      this.socket.on("connect", (error) => {
        
        console.log(error)
        console.log("socket.onconnect")
        this.connectionService.setConnected(true);
        this.parseAndEmitClientNameToServer();        
          
        this.disconnected = false;
        
      });
      
  
      this.socket.on("teamBudget",(messageWrapper ) => {      
       
        const message_id = messageWrapper.message_id;
        this.sessionService.messageIdsReceivedByClient.push(message_id);
        console.log(this.sessionService.messageIdsReceivedByClient)
        const message = messageWrapper.message;
  
        this.sessionService.teamBudget = message.team_budget;
      })
  
      this.socket.on("dissolve_team",(messageWrapper ) => {      
       
        const message_id = messageWrapper.message_id;
        this.sessionService.messageIdsReceivedByClient.push(message_id);
        console.log(this.sessionService.messageIdsReceivedByClient)
        const data = messageWrapper.message;
  
        this.sessionService.team_is_dissolving = true;
  
        console.log("this.sessionService.team_is_dissolving : " + this.sessionService.team_is_dissolving);
        console.log("this.sessionService.post_trial_surveys_open : " + this.sessionService.post_trial_surveys_open);
        console.log("this.sessionService.player_left_first : " + this.sessionService.player_left_first);
        
        if( this.sessionService.post_trial_surveys_open === false ){
          if(this.sessionService.player_left_first === false){
  
             // Open Dialog      
            const dialogConfig = new MatDialogConfig();
            dialogConfig.autoFocus = true;
            dialogConfig.maxWidth='90vw';
            dialogConfig.maxHeight='90vh';
            dialogConfig.id = "TeamDissolveDialog";
            dialogConfig.disableClose = true;
            dialogConfig.data = data;
            const dialogRef = this.dialog.open(TeamDissolvePromptComponent, dialogConfig )
  
          }
        }    
      });
  
      this.socket.on('trial', ( messageWrapper ) => {      
       
        const message_id = messageWrapper.message_id;
        this.sessionService.messageIdsReceivedByClient.push(message_id);
        console.log(this.sessionService.messageIdsReceivedByClient)
  
        const message = messageWrapper.message;   
  
        this.sessionService.appMode = AppMode.LOADING;   
        this.sessionService.trialMessage = message;
        const sub_type = message['msg']['sub_type']
        if (sub_type == 'start') {
          console.log('---------------- trial started -----------------')
          this.trial_running = true;
          this.sessionService.team_is_dissolving = false;
          this.trialActiveEmitter.emit(true); 
          //this.trialSummaryEmitter.emit(false);               
        }
        else if (sub_type == 'end' || sub_type == 'stop') {
          console.log('---------------- trial stopped -----------------')
         
          console.log('Starting Trial Cleanup. ' + new Date().toUTCString() );
          
         
          this.modLoadingProgress = 0
          this.minecraftContainerInitialized = false
          this.asistModIsReady= false
          this.trial_running = false
          this.trialActiveEmitter.emit(false);
          this.sessionService.appMode = AppMode.EXIT;        
          this.asistModIsReady = false;
          this.sessionService.resetAppState();
          this.sessionService.messageIdsReceivedByClient=[]
          this.socket.emit('trialStopAck',{'name':this.sessionService.assignedName})             
          console.log('Ending Trial Cleanup. ' + new Date().toUTCString() )
        }
      });
  
      this.socket.on('trialInfo',(messageWrapper ) => {      
       
        const message_id = messageWrapper.message_id;
        this.sessionService.messageIdsReceivedByClient.push(message_id);
        console.log(this.sessionService.messageIdsReceivedByClient)
        const message = messageWrapper.message;   
  
        // just to be extra sure
        this.sessionService.team_is_dissolving = false;
        //////////////////////////////////
        this.sessionService.trialInfoMessage = message;
        console.log(message);
        let cIndex=0;
        this.sessionService.callSignMap = new Map(Object.entries(message.callsigns));
        this.sessionService.callSignMap.forEach((v,k)=>{
          console.log("CallSignMap : " + k + " : " + v)
          if(k.localeCompare(this.sessionService.assignedName)==0){          
            this.sessionService.callSign = v;
            this.sessionService.callSignCode=cIndex;          
          }              
          cIndex++;
        })
        this.sessionService.callSignArray = this.sessionService.config.callSigns as string[];
        
        this.sessionService.pidMap = new Map(Object.entries(message.participant_ids));
        this.sessionService.pidMap.forEach((v,k)=>{
          console.log("PidMap : " + k + " : " + v)
          if(k.localeCompare(this.sessionService.assignedName)==0){          
            this.sessionService.participant_id = v;
            console.log( " Participant Id set to : " + v)         
          }
          this.iconSelectorService.flagKeeperMap.set(v,[] as RenderedIconWrapper[]);
          this.beaconService.beaconKeeperMap.set(v,[] as RenderedIconWrapper[]);
        })
        console.log("Listening on Front End to positionUpdate_(pid) messages");
        
        this.socket.on('positionUpdate_' + this.sessionService.participant_id, ( posObject:PositionEvent) => { 
          
          this.timeString = posObject.mission_timer;      
  
          posObject.callsign = this.sessionService.callSign;        
  
          this.positionEmitter.emit(posObject);
    
        });
        
        //this.sessionService.resetAppState();        
        this.trialInfoEmitter.emit( "data");
        
        console.log("Set callsign and pid : [" + this.sessionService.callSignCode + "] : "+ this.sessionService.callSign + " : " + this.sessionService.participant_id)
                  
      });

      this.socket.on("redirectToLogin",(payload)=>{

        this.config = payload['config'] as ClientMapConfig ;        
        this.sessionService.config = this.config as ClientMapConfig;
        this.sessionService.admin_host = this.sessionService.config.admin_stack_ip+':'+this.sessionService.config.admin_stack_port
        console.log("Set admin_host as : " + this.sessionService.admin_host);

        console.log(" Redirecting to Waiting Room "); 
        console.log('https://'+this.sessionService.admin_host+'/SPSO/Login');
        const url = 'https://'+this.sessionService.admin_host+'/SPSO/Login'
        window.location.href = url 

      });
  
      this.socket.on('socket_test',(messageWrapper ) => {      
       
        const message_id = messageWrapper.message_id;
        this.sessionService.messageIdsReceivedByClient.push(message_id);
        console.log(this.sessionService.messageIdsReceivedByClient)
        const data = messageWrapper.message;
        console.log(data)
      })

      this.socket.on('trialSummary',( messageWrapper ) => {
        //console.log("TrialSummary ...")
        const message_id = messageWrapper.message_id;
        this.sessionService.messageIdsReceivedByClient.push(message_id);
        //console.log(this.sessionService.messageIdsReceivedByClient)
        const data = messageWrapper.message; 
        //console.log(data)  
        this.sessionService.trialSummary = data
        const config:MatDialogConfig = new MatDialogConfig() 
        //config.height = '75vh';           
        const dialogRef = this.dialog.open(TrialSummaryComponent,config)   
      });
  
      this.socket.on('minecraft_loading',( messageWrapper ) => {
        
       
        const message_id = messageWrapper.message_id;
        this.sessionService.messageIdsReceivedByClient.push(message_id);
        console.log(this.sessionService.messageIdsReceivedByClient)
        const data = messageWrapper.message;   
        
        this.minecraftLoadingEmitter.emit(data);
        try{
          this.minecraftContainerInitialized = true;
          this.trial_running = true; 
          this.modLoadingProgress = Number.parseInt(data.loaded)        
          if ( this.modLoadingProgress == 100){
            
            //this.sessionService.resetAppState();  
            
            this.asistModIsReady = true;
            const config:MatDialogConfig = new MatDialogConfig()
            //config.height = '75vh';
            config.disableClose = true;
            const dialogRef = this.dialog.open(UserDialogComponent,config)          
            this.sessionService.appMode = AppMode.PLAY;          
          }        
        }catch(error){
          console.log(error)
        }
      });
  
      this.socket.on('missionState', ( messageWrapper ) => {      
       
        const message_id = messageWrapper.message_id;
        this.sessionService.messageIdsReceivedByClient.push(message_id);
        console.log(this.sessionService.messageIdsReceivedByClient)
        const message = messageWrapper.message;
  
        if( ( message.data.mission_state as string).localeCompare("Stop")==0 ){
          
          const dialogConfig = new MatDialogConfig();
          dialogConfig.autoFocus = true;
          dialogConfig.maxWidth='90vw';
          dialogConfig.maxHeight='90vh';
          dialogConfig.id = "TeamDissolveDialog";
          dialogConfig.disableClose = true;
          let dialogRef=null;
  
          if(message.data.state_change_outcome.localeCompare("MISSION_STOP_SERVER_CRASHED")==0){
            
            dialogConfig.data = {"reason":"MISSION_STOP_SERVER_CRASHED"};
            dialogRef = this.dialog.open(TeamDissolvePromptComponent, dialogConfig)
            
          }
          // THESE 3 OTHERS BELOW ARE HANDLED BY "dissolve_team" hook above, as they dissolve the team before the mission ends.
          // Otherwise we either show the postrial survey and then dissolve the team, OR we show post trial survey and continue
          // with team
          else if(message.data.state_change_outcome.localeCompare("MISSION_STOP_MINECRAFT_DISCONNECT")==0){
     
            //dialogConfig.data = {"reason":"MISSION_STOP_MINECRAFT_DISCONNECT"};
            //dialogRef = this.dialog.open(TeamDissolvePromptComponent, dialogConfig)
          }
          else if(message.data.state_change_outcome.localeCompare("MISSION_STOP_CLIENTMAP_DISCONNECT")==0){
           
            //dialogConfig.data = {"reason":"MISSION_STOP_CLIENTMAP_DISCONNECT"};
            //dialogRef = this.dialog.open(TeamDissolvePromptComponent, dialogConfig)
          }
          else if(message.data.state_change_outcome.localeCompare("MISSION_STOP_PLAYER_LEFT")==0){
            
            //dialogConfig.data = {"reason":"MISSION_STOP_PLAYER_LEFT"};
            //dialogRef = this.dialog.open(TeamDissolvePromptComponent, dialogConfig)
          }
          else{
            
            dialogConfig.id = "SurveyDialog";          
            dialogRef = this.dialog.open(SurveyComponent, dialogConfig )
                  
            dialogRef.afterClosed().subscribe(()=>{
              
              this.sessionService.post_trial_surveys_open = false;
              console.log("this.sessionService.team_is_dissolving : " + this.sessionService.team_is_dissolving);
              console.log("this.sessionService.post_trial_surveys_open : " + this.sessionService.post_trial_surveys_open);
              console.log("this.sessionService.player_left_first : " + this.sessionService.player_left_first);
              
              // TEAM IS NOT DISSOLVING AT THIS POINT BECAUSE RESET APPSTATE IS GETTING CALLED
              if ( this.sessionService.team_is_dissolving ){
                const dialogConfig = new MatDialogConfig();
                dialogConfig.autoFocus = true;
                dialogConfig.maxWidth='90vw';
                dialogConfig.maxHeight='90vh';
                dialogConfig.id = "TeamDissolveDialog";
                dialogConfig.disableClose = true;
                dialogConfig.data = {
                  "reason":"DISSOLVE_TEAM_PLAYER_CHOICE"
                };
                const dialogRef = this.dialog.open(TeamDissolvePromptComponent, dialogConfig )
  
              }
              else{
                this.openTeamContinuePrompt();
              }        
            });        
  
          }
          
        }
  
        //EMIT TO MAP COMPONENT
        this.missionStateEmitter.emit(message.data.mission_state as string);
  
      });
  
      this.socket.on('budgetUpdate',(messageWrapper ) => {      
       
        const message_id = messageWrapper.message_id;
        this.sessionService.messageIdsReceivedByClient.push(message_id);
        console.log(this.sessionService.messageIdsReceivedByClient)
        const message = messageWrapper.message;
        this.sessionService.teamBudget = message.data.team_budget as number;
      });
  
      this.socket.on('stageTransition', ( messageWrapper ) => {      
       
        const message_id = messageWrapper.message_id;
        this.sessionService.messageIdsReceivedByClient.push(message_id);
        console.log(this.sessionService.messageIdsReceivedByClient)
        const message = messageWrapper.message;
  
        const stage = (message.data.mission_stage as string).toLowerCase();
        const teamBudget = (message.data.team_budget as number);
        this.sessionService.teamBudget = teamBudget;
        this.sessionService.numTransitionsToShop = (message.data.transitionsToShop as number);
        this.sessionService.numTransitionsToField = (message.data.transitionsToField as number);
        
  
        if( stage.localeCompare('shop_stage')==0){
          console.log("STAGE_TRANSITION : SHOP_STAGE")
          this.sessionService.tabIndex = "1";
          this.sessionService.mission_stage = 'shop_stage';       
        }      
        else if ( stage.localeCompare('field_stage')==0 ){
          console.log("STAGE_TRANSITION : FIELD_STAGE")
          this.sessionService.tabIndex = "0";
          this.sessionService.mission_stage = 'field_stage';
          
          // clear the tool counts
          for(let i=0; i<this.sessionService.toolCounts.length;i++){
            for(let j=0; j<this.sessionService.toolCounts[i].length; j++){
              this.sessionService.toolCounts[i][j][3]=0;
            }
          }
        }
        else if (stage.localeCompare('recon_stage')==0){
          console.log("STAGE_TRANSITION : RECON_STAGE")
          this.sessionService.tabIndex = "0";
          this.sessionService.mission_stage = 'recon_stage';
        }
  
      });
  
      // content and callsign
      this.socket.on('storeChat', (messageWrapper ) => {      
       
        const message_id = messageWrapper.message_id;
        this.sessionService.messageIdsReceivedByClient.push(message_id);
        console.log(this.sessionService.messageIdsReceivedByClient)
        const message = messageWrapper.message;
  
        this.sessionService.receivedChats.push(message.record);
        setTimeout(()=>{
          const chatContainer = document.getElementById("chatContainer") as HTMLDivElement;
          if(chatContainer != null){
            chatContainer.scrollTop = chatContainer.scrollHeight;
          }
        },300)
        
      });
  
      this.socket.on('proposedPurchase', (messageWrapper ) => {      
       
        const message_id = messageWrapper.message_id;
        this.sessionService.messageIdsReceivedByClient.push(message_id);
        console.log(this.sessionService.messageIdsReceivedByClient)
        const message = messageWrapper.message;
       
        console.log("Incoming Proposed Purchase : " + message);
        const callSignCode = message.additional_info.call_sign_code;
        const meta_action = message.additional_info.meta_action;
        const itemCode = message.additional_info.item_code;
        
        if(meta_action.localeCompare('PROPOSE_TOOL_PURCHASE_+')==0){          
          this.sessionService.toolCounts[itemCode][callSignCode][3]++;          
        }
        else if(meta_action.localeCompare('PROPOSE_TOOL_PURCHASE_-')==0){
          this.sessionService.toolCounts[itemCode][callSignCode][3]--;          
        }
      });
  
      this.socket.on('iconUpdate', (messageWrapper ) => {      
       
        const message_id = messageWrapper.message_id;
        this.sessionService.messageIdsReceivedByClient.push(message_id);
        console.log(this.sessionService.messageIdsReceivedByClient)
        const message = messageWrapper.message;        
       
        this.iconSelectorService.processIncomingIconUpdate(message);       
        
      });
  
      this.socket.on('beaconUpdated', (messageWrapper ) => {      
       
        const message_id = messageWrapper.message_id;
        this.sessionService.messageIdsReceivedByClient.push(message_id);
        console.log(this.sessionService.messageIdsReceivedByClient)
        const message = messageWrapper.message;
  
        //console.log("Incoming Beacon Updated Message : " + JSON.stringify(message));
  
        this.beaconService.updateBeaconFromMinecraftMessage(message);
  
      });
  
      this.socket.on('beaconPlaced', (messageWrapper ) => {      
       
        const message_id = messageWrapper.message_id;
        this.sessionService.messageIdsReceivedByClient.push(message_id);
        console.log(this.sessionService.messageIdsReceivedByClient)
        const message = messageWrapper.message;
       
        //console.log("Incoming Beacon Placed Message : " + JSON.stringify(message));
  
        this.beaconService.processBeaconPlacedMessage( message );
        
      });
  
      this.socket.on('beaconRemoved', (messageWrapper ) => {      
       
        const message_id = messageWrapper.message_id;
        this.sessionService.messageIdsReceivedByClient.push(message_id);
        console.log(this.sessionService.messageIdsReceivedByClient)
        const message = messageWrapper.message;
       
        //console.log("Incoming Beacon Removed Message : " + message);
  
        this.beaconService.processBeaconRemovedMessage(message);
        
      });
  
      this.socket.on('flagPlaced', (messageWrapper ) => {      
       
        const message_id = messageWrapper.message_id;
        this.sessionService.messageIdsReceivedByClient.push(message_id);
        console.log(this.sessionService.messageIdsReceivedByClient)
        const message = messageWrapper.message;
       
        //console.log("Incoming Flag Placed Message : " + message);
        const pid = message.participant_id;
  
        //console.log( pid+' : '+ this.sessionService.participant_id);
  
        /*
        if(pid.localeCompare(this.sessionService.participant_id)==0){
          // DO NOTHING THIS CAME FROM US
          //console.log( " Do nothing this flag came from us ... ");
        }        
        else{
          this.iconSelectorService.incomingFlagEmitter.emit(message);
        }
        */ 
        
        this.iconSelectorService.incomingFlagEmitter.emit(message);

      });    
  
      this.socket.on('undoFlagPlaced', (messageWrapper ) => {      
       
        const message_id = messageWrapper.message_id;
        this.sessionService.messageIdsReceivedByClient.push(message_id);
        console.log(this.sessionService.messageIdsReceivedByClient)
        const message = messageWrapper.message;
       
        console.log("Incoming undoFlagPlaced Message : " + message);
        const pid = message.participant_id;
        
        /*
        if(pid.localeCompare(this.sessionService.participant_id)==0){
          // DO NOTHING THIS CAME FROM US
        }        
        else{
         this.iconSelectorService.incomingUndoFlagPlacement(message);
        }
        */
        
        this.iconSelectorService.incomingUndoFlagPlacement(message);
      });  
  
      this.socket.on('agentIntervention',(messageWrapper ) => {      
       
        const message_id = messageWrapper.message_id;
        this.sessionService.messageIdsReceivedByClient.push(message_id);
        console.log(this.sessionService.messageIdsReceivedByClient)
        const message:AgentIntervention = messageWrapper.message;
  
        console.log("Agent Intervention received");
        console.log("Current sessionService.mission_stage : " + this.sessionService.mission_stage );
        console.log(this.sessionService.mission_stage.localeCompare('shop_stage'));
        if(this.sessionService.mission_stage.localeCompare('shop_stage')==0){
          console.log("Agent Intervention processed during SHOP_STAGE.");
          // console.log(this.sessionService.participant_id)
          // console.log(message.data.receivers)
          // console.log(message.data.receivers.includes(this.sessionService.participant_id))
  
          if(message.data.receivers.includes(this.sessionService.participant_id)){          
              this.advisorService.processAgentIntervention(message);                 
          }
  
        }
        
      })
  
      this.socket.on('bombSensor',(messageWrapper ) => {      
       
        const message_id = messageWrapper.message_id;
        this.sessionService.messageIdsReceivedByClient.push(message_id);
        console.log(this.sessionService.messageIdsReceivedByClient)
        const message = messageWrapper.message;
  
        const pid = message.data.participant_id as string;
        const x = message.data.target_x as number;
        const y = message.data.target_y as number;
        const z = message.data.target_z as number;
        const id = message.data.item_id as string;
        
        console.log("Bomb Sensor received : " + pid + " : " + pid.localeCompare(this.sessionService.participant_id));  
  
        if( pid.localeCompare(this.sessionService.participant_id)==0 ){
          //if(this.sessionService.mission_stage == 'shop_stage'){
            this.bombSensorEmitter.emit([x,y,z,id]);
          //}        
        }
      })
      
      this.socket.on('inventoryUpdate',(messageWrapper ) => {      
       
        const message_id = messageWrapper.message_id;
        this.sessionService.messageIdsReceivedByClient.push(message_id);
        console.log(this.sessionService.messageIdsReceivedByClient)
        const message = messageWrapper.message;      
        
        //console.log(message);
        //console.log(this.sessionService.mission_stage);
        try{       
            
          setTimeout( ()=>{
  
            console.log("Inventory Update received");
      
            console.log(message);
      
            const currInv: Map<string,Map<string,number>> = new Map(Object.entries( message.data.currInv ));
      
            //console.log(currInv);
      
            currInv.forEach( (v:Map<string,number>,k)=>{      
              
              const callSignCode:number = this.sessionService.getCallSignCodeFromCallSign(this.sessionService.getCallSignFromPid(k));
              
              console.log("Inventory update for : " + k + " : " + callSignCode);      
              
              const inv: Map<string,number> = new Map(Object.entries(v));
      
              //console.log("Test 1" );
              inv.forEach((v2,k2)=>{                
                for(let i=0;i<this.toolService.itemAssets.length;i++){ 
                  //console.log(i)                   
                  if(  k2.localeCompare(this.toolService.itemAssets[i][1])==0  ){
                    //console.log(" ITEM FOUND : " + k2 );            
                    this.sessionService.toolCounts[i][callSignCode][2]=v2;
                  }
                  //console.log(i)
                }        
              });
            });      
          },500); 
           
        }
        catch(e){
          console.log(e);
        }
             
      });  

    }
    catch(e){
      console.log(e)
    }

      
  }

  setupNamedSocketHooks(name: string){

    this.sessionService.assignedName = name;
   
    // LISTEN TO POSITION UPDATES FROM EVERYONE IF ADVISOR OR CONFIG SAYS SO

    if(name === "asist_advisor" || (this.config['showGlobalPositions']===true)){

      console.log("Listening on Front End to positionUpdate_global messages");

      this.socket.on('positionUpdate_global', (message ) => {        

        // change the last index to a callsign instead of a playername

        // [x, y, z, mission_timer, playername]
        
        const player = message[4];

        message[4]=this.sessionService.trialInfoMessage.client_info[player].callSign;

        this.positionEmitter.emit(message);
  
      });      
    }  
    
    // LISTEN FOR JUST YOUR OWN POSITION UPDATES
    else{
      
      console.log("Listening on Front End to positionUpdate_(pid) messages");
      
      this.socket.on('positionUpdate_' + this.sessionService.participant_id, ( posObject:PositionEvent) => { 
        
        this.timeString = posObject.mission_timer;      

        posObject.callsign = this.sessionService.callSign;        

        this.positionEmitter.emit(posObject);
  
      });
    }

    this.socket.on('textIntervention_' + name, ( messageWrapper ) => {      
     
      const message_id = messageWrapper.message_id;
      this.sessionService.messageIdsReceivedByClient.push(message_id);
      console.log(this.sessionService.messageIdsReceivedByClient)
      const message = messageWrapper.message;
      
      console.log("received text intervention in websocketservice ", message);

      this.textInterventionEmitter.emit(message);

    });

  }

  setupAuthenticationAndSocketHooks(){
    
    this.socket.on('authenticationResponse', (payload) => {

      try{

        console.log(payload);

        if ( payload['response'] === true ){
          //this.map = payload['map'];
          this.config = payload['config'] as ClientMapConfig ;
          const refresh = payload['refresh'] as boolean
          this.sessionService.config = this.config as ClientMapConfig;
          this.sessionService.admin_host = this.sessionService.config.admin_stack_ip+':'+this.sessionService.config.admin_stack_port
          this.sessionService.bearerToken = this.sessionService.config.db_api_token;
          this.sessionService.faqIframeSrc = "https://"+this.sessionService.admin_host +"/SPSO/FAQ?noheader=true";         
          
          console.log("Set admin_host as : " + this.sessionService.admin_host);
          //this.setupNamelessSocketHooks()
          this.setupNamedSocketHooks(this.sessionService.assignedName)
          /*
          if(refresh){
            console.log("Recovering App State from Saved Local Storage.");
            this.sessionService.recoverFromLocalStorage()
          } 
          else{
            this.sessionService.clearLocalStorage();
            console.log("Initializaing App State from Blank Local Storage.");
          } 
          */        
        }

      }
      catch(e){
        console.log(e)
      }

    });
  }

  openTeamContinuePrompt(){
    const dialogRef = this.dialog.open(TeamContinuePromptComponent,{disableClose: true});
  } 
  
  
}
