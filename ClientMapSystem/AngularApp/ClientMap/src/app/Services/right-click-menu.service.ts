import { Injectable } from '@angular/core';
import { RenderedIconWrapper } from '../CustomClasses/RenderedIconWrapper';
import { SessionService } from './session.service';
import { WebsocketService } from './websocket.service';
import { FlagData } from '../CustomClasses/FlagData';
import { IconType,getIconTypeName } from '../CustomClasses/Enums';
import { IconSelectorService } from './icon-selector.service';

@Injectable({
  providedIn: 'root'
})
export class RightClickMenuService {

  menu_open:boolean = false;
  currentActiveIcon:RenderedIconWrapper = null;
  
  constructor(
    public iconSelectorService:IconSelectorService,
    public sessionService:SessionService,
    public websocketService:WebsocketService
  ) { 
    
    iconSelectorService.iconSelectEmitter.subscribe( (iconWrapper)=>{
      this.currentActiveIcon = iconWrapper;
    } );

   }

  updateIconPriority( priority:number ){
    this.currentActiveIcon.data.priority = priority;
    const priorityElement = this.getPriorityElement();
    if(priorityElement!=null && priorityElement!= undefined)
    {
      priorityElement.innerText = priority.toString();
    }
    this.syncIconData();
  }

  getPriorityElement():HTMLParagraphElement{
    return document.getElementById(this.currentActiveIcon.node.id +"_priority") as HTMLParagraphElement;
  }

  syncIconData(){        
    const pid = this.sessionService.participant_id;    
    let iconUpdateObject = {
      
    };
        
    const flagData = (this.currentActiveIcon.data as FlagData);      
    iconUpdateObject = {
      "owner" : this.currentActiveIcon.owner_pid,
      "participant_id":pid,
      "element_id": this.currentActiveIcon.node.id,
      "parent_id":"PlanningInteractionContainer",
      "type": getIconTypeName(IconType.FLAG),
      "x": this.currentActiveIcon.node.style.top,
      "y": this.currentActiveIcon.node.style.left,
      "additional_info":{
        "owner" : this.currentActiveIcon.owner_pid,
        "meta_action":"PLANNING_FLAG_UPDATE",
        "meta_type":"PLANNING_FLAG",
        "call_sign_code":this.sessionService.callSignCode,
        "index": this.currentActiveIcon.index,
        "x_percentage": this.currentActiveIcon.x_percentage,
        "y_percentage": this.currentActiveIcon.y_percentage,
        "content" : flagData.content,
        "prioritization" : flagData.priority,
        "schedule_minute": flagData.schedule_minute,
        "schedule_second":flagData.schedule_second,
        "responses": {}
        }
      }
    

    const responseObject = {};
    
    this.currentActiveIcon.responses.forEach((v,k)=>{
      responseObject[k] = v
    });

    iconUpdateObject['additional_info']['responses'] = responseObject;
    
    //console.log(iconUpdateObject)
    
    this.websocketService.socket.emit("iconUpdate",iconUpdateObject);
  }

}
