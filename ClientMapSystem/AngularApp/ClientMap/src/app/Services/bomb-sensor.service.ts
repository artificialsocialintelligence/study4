import { Injectable } from '@angular/core';
import { RenderedIconWrapper } from '../CustomClasses/RenderedIconWrapper';

@Injectable({
  providedIn: 'root'
})
export class BombSensorService {

  bombSensorMap:Map<string,RenderedIconWrapper> = new Map<string,RenderedIconWrapper>();

  constructor() { }
}
