export interface PositionEvent{

    x_pos: number;
    y_pos: number;
    z_pos: number;
    mission_timer: string;
    playername:string;
    callsign?:string;

}

export interface CommonHeader{
    timestamp: string
    message_type: string
    version: string
}
export interface CommonMessage{
    trial_id: string
    experiment_id:string
    replay_id?: string
    replay_root_id?: string
    source: string
    sub_type: string
    version: string

}

export interface AsistGenericModel{
    header: CommonHeader,
    msg : CommonMessage
}

export interface AgentInterventionData{
    id: string,
    //created: "2019-12-26T14:05:02.3412Z",
    start: number,
    //duration: 10000,
    content: string,
    receivers: string[],
    response_options: string[]
    response_index?:number
    //explanation: "{<agent custom json object>}"  
}

export interface AgentIntervention extends AsistGenericModel{
    data:AgentInterventionData
}


export interface InventoryUpdateData {
    currInv:Map<string,Map<string,string>>
}
export interface InventoryUpdate extends AsistGenericModel{
    data: InventoryUpdateData
}
