import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { AppMode } from '../CustomClasses/Enums';
import { SessionService } from '../Services/session.service';
import { WebsocketService } from '../Services/websocket.service';

@Component({
  selector: 'app-team-continue-prompt',
  templateUrl: './team-continue-prompt.component.html',
  styleUrls: ['./team-continue-prompt.component.scss']
})
export class TeamContinuePromptComponent implements OnInit {

  

  constructor(public websocketService:WebsocketService,public sessionService:SessionService, public http:HttpClient, public dialogRef:MatDialogRef<TeamContinuePromptComponent>) { 
    dialogRef.disableClose = true;
  }

  ngOnInit(): void {

    this.websocketService.socket.emit("fetchTrialSummary")
   
  }

  leaveTeam(){

    this.websocketService.socket.emit("leave_team",{"name":this.sessionService.assignedName});
    
    const body = {
      'name':this.sessionService.assignedName,
      'participant_id':this.sessionService.participant_id
    }
    
    console.log("Contacting URL : ");  
    console.log('https://'+this.sessionService.admin_host+'/WaitingRoom/return_player_from_trial')
     

    this.http.post<any>(
      'https://'+this.sessionService.admin_host+'/WaitingRoom/return_player_from_trial',
      body).subscribe(
      {
        next: (v) => console.log(v),
        error: (e) => console.error(e),
        complete: () => {
          {
              console.log("Contacting URL : ");  
              console.log('https://'+this.sessionService.admin_host+'/WaitingRoom/player/'+this.sessionService.assignedName);
              const url = 'https://'+this.sessionService.admin_host+'/WaitingRoom/player/'+this.sessionService.assignedName
              window.location.href = url
              this.dialogRef.close();     
          }  
        } 
      }  
    );

    
  }

  continueTeam(){

    this.sessionService.appMode = AppMode.LOADING;    

    this.websocketService.socket.emit("continue_team",{"name":this.sessionService.assignedName});
    this.dialogRef.close()

  }

}
