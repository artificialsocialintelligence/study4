import { AfterViewInit, Component, OnInit } from '@angular/core';
import { DialogPosition, MatDialog, MatDialogConfig } from '@angular/material/dialog';

import { ActionDialogComponent } from '../action-dialog/action-dialog.component';
import { WebsocketService } from '../Services/websocket.service';
import { TeamContinuePromptComponent } from '../team-continue-prompt/team-continue-prompt.component';
import { TeamDissolvePromptComponent } from '../team-dissolve-prompt/team-dissolve-prompt.component';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit, AfterViewInit {

  dialogRef

  constructor(
    public websocketService:WebsocketService,
    public dialog: MatDialog,
    ) {}

  ngOnInit(): void {

  }

  ngAfterViewInit(){

    // UNCOMMENT FOR PRODUCTION    
    
    let dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.id = "MinecraftLoadingDialog";
    dialogConfig.disableClose = true;
    dialogConfig.position = { top: '5vh'} 
    let data = "Welcome to the ClientMap! Once all of your teammates join the experiment (as you just did), the Trial Active indicator below should change to 'true'. The Minecraft Server will then begin loading. For the moment just hang tight!"
    dialogConfig.data = data;
    this.dialogRef = this.dialog.open(ActionDialogComponent, dialogConfig);

    

    // COMMENT THIS FOR PRODUCTION

    // dialogConfig = new MatDialogConfig();
    // dialogConfig.autoFocus = true;
    // dialogConfig.id = "MinecraftLoadingDialog";
    // dialogConfig.disableClose = true;
    // dialogConfig.position = { top: '55vh'} 
    // data = "The Minecraft Server is now loading. You should see the progress bar above start to fill within a minute. Please wait while it loads the mission. Once loading is complete, you will be presented with instructions explaining how to connect to the Minecraft Server. Do not try to connect to the Minecraft Server before then."
    // dialogConfig.data = data;
    // this.dialogRef = this.dialog.open(ActionDialogComponent, dialogConfig);

    // const dialogRef = this.dialog.open(TeamContinuePromptComponent,{disableClose: true});


    this.websocketService.trialActiveEmitter.subscribe( ( bool:boolean)=>{
      if(bool){

        if(this.dialogRef !=null && this.dialogRef != undefined){
          this.dialogRef.close();
          this.dialogRef = null;
        }
  
        const dialogConfig = new MatDialogConfig();
        dialogConfig.autoFocus = true;
        dialogConfig.id = "MinecraftLoadingDialog";
        dialogConfig.disableClose = true;
        dialogConfig.position = { top: '65vh'} 
        const data = "The Minecraft Server is now starting up.  You should see the progress bar above start to fill after at most 30 seconds. Please wait while it loads. You will then be presented with instructions. Do not try to connect to the Minecraft Server before then."
        dialogConfig.data = data;
        this.dialogRef = this.dialog.open(ActionDialogComponent, dialogConfig);

      }
      

    });
    
    this.websocketService.minecraftLoadingEmitter.subscribe( ()=>{
      if(this.dialogRef !=null && this.dialogRef != undefined){
        this.dialogRef.close();
        this.dialogRef = null;
      }
    });

   

  } 


}
