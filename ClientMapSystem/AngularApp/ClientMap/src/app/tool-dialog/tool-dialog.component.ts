import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToolService } from '../Services/tool.service';

@Component({
  selector: 'app-tool-dialog',
  templateUrl: './tool-dialog.component.html',
  styleUrls: ['./tool-dialog.component.scss']
})
export class ToolDialogComponent implements OnInit {

  itemCode:number;
  

  toolTuple:[string,string,number,string,string];
  
  
  constructor(
    public toolService:ToolService,
    public dialogRef: MatDialogRef<ToolDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    
  ){
   

  }

  ngOnInit(): void {
    console.log(this.data)
    this.itemCode = this.data.itemCode as number;
    console.log(this.toolService.toolInfoMap)    
    this.toolTuple = this.toolService.toolInfoMap.get(this.itemCode);
    console.log(this.toolTuple)
  }

  close(){
    this.dialogRef.close();
  }

}
