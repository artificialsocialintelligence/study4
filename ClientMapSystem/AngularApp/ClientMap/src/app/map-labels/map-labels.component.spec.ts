import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapLabelsComponent } from './map-labels.component';

describe('MapLabelsComponent', () => {
  let component: MapLabelsComponent;
  let fixture: ComponentFixture<MapLabelsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapLabelsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MapLabelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
