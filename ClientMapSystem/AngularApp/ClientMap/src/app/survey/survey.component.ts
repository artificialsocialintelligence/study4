import { Component, EventEmitter, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatRadioChange } from '@angular/material/radio';
import { MatSliderChange } from '@angular/material/slider';
import { Survey, SurveyQuestion, SurveyText, SurveysObject } from '../Interfaces/SurveyModels';

import { SessionService } from '../Services/session.service';
import { WebsocketService } from '../Services/websocket.service';


@Component({
  selector: 'app-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.scss']
})
export class SurveyComponent implements OnInit {

  surveyCompleteEmitter:EventEmitter<boolean>=new EventEmitter();

  public test:number = 0;

  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<SurveyComponent>, 
    public websocketService:WebsocketService, 
    public sessionService:SessionService, 
    public http:HttpClient
    ) { }

  ngOnInit(): void {
    this.sessionService.post_trial_surveys_open = true;
  }

  isSurveyQuestion( data:object){

    const incomingProperties:string[]  = Object.getOwnPropertyNames(data);
    const validationProperties:string[] = SurveyQuestion.getProperties();
    for(let i=0; i<validationProperties.length;i++){
      if( !incomingProperties.includes(validationProperties[i]) ){
        return false;
      }
    }
    return true;
  }

  isSurveyText( data:object):boolean{
    const incomingProperties:string[]  = Object.getOwnPropertyNames(data);
    const validationProperties:string[] = SurveyText.getProperties();

    //reverse of the other one - if there's anything but text ist not a text object
    for(let i=0; i<incomingProperties.length;i++){
      if( !validationProperties.includes(incomingProperties[i]) ){
        return false;
      }
    }

    return true;

  }

  isMultipleChoice(data:unknown):boolean{

    const castData = data as SurveyQuestion;
    if(castData.input_type.localeCompare('MULTI_CHOICE')==0){
      return true
    }
    return false;
  }

  isSlider(data:unknown):boolean{

    const castData = data as SurveyQuestion;
    if(castData.input_type.localeCompare('SLIDER')==0){
      return true
    }
    return false;
  }

  isTextField(data:unknown):boolean{

    const castData = data as SurveyQuestion;
    if(castData.input_type.localeCompare('TEXT')==0){
      return true
    }
    return false;
  }

  isTextArea(data:unknown):boolean{

    const castData = data as SurveyQuestion;
    if(castData.input_type.localeCompare('TEXTAREA')==0){
      return true
    }
    return false;
  }

  getMinLabel(surveyIndex:number, dataindex:number):number{
    const split = (this.sessionService.surveysObject.surveys[surveyIndex].data[dataindex] as SurveyQuestion).labels[0].split(new RegExp('[()]'));
    //console.log(split)
    return Number.parseInt(split[1]);
  }

  getMaxLabel(surveyIndex:number, dataindex:number):number{
    const labels:string[] = (this.sessionService.surveysObject.surveys[surveyIndex].data[dataindex] as SurveyQuestion).labels;
    const split = labels[labels.length-1].split(new RegExp('[()]'));
    //console.log(split)
    return Number.parseInt(split[1]);
  }

  getStepSize(surveyIndex:number, dataindex:number):number{
   
    const labels:string[] = (this.sessionService.surveysObject.surveys[surveyIndex].data[dataindex] as SurveyQuestion).labels;  
    const min:number = Number.parseInt(labels[0].split(new RegExp('[()]'))[1])
    const max:number = Number.parseInt(labels[labels.length-1].split(new RegExp('[()]'))[1])  
    //console.log(min,max, labels.length-1 , max-min)
    //console.log( (max-min) / (labels.length-1) )
    return (max-min) / (labels.length-1);
  }

  radioChange(event:MatRadioChange, surveyIndex:number, dataIndex:number){

    //console.log(event, surveyIndex,dataIndex);
    (this.sessionService.surveysObject.surveys[surveyIndex].data[dataIndex] as SurveyQuestion).response = event.value;
    //console.log(this.sessionService.surveysObject.surveys[surveyIndex].data[dataIndex]);
  }

  sliderChange(event:MatSliderChange, surveyIndex:number, dataIndex:number){
    //console.log(event, surveyIndex,dataIndex);
    (this.sessionService.surveysObject.surveys[surveyIndex].data[dataIndex]as SurveyQuestion).response = event.value?.toString();
    //console.log(this.sessionService.surveysObject.surveys[surveyIndex].data[dataIndex]);
  }

  textInputChange(event:any, surveyIndex:number, dataIndex:number){
    //console.log(event, surveyIndex,dataIndex);
    (this.sessionService.surveysObject.surveys[surveyIndex].data[dataIndex]as SurveyQuestion).response = event.target.value;
    //console.log(this.sessionService.surveysObject.surveys[surveyIndex].data[dataIndex]);
  }

  getImagePath( assetName:string ){
    return this.sessionService.getAssetPath(assetName);
  }

 

  completeSurvey(){

    const survey = this.sessionService.surveysObject.surveys[this.sessionService.activeSurveyIndex] as Survey
    let complete = true;
    
    for (let i=0; i< survey.data.length; i++){
      if ( this.isSurveyQuestion(survey.data[i])){
        const question = survey.data[i] as SurveyQuestion
        
        if(question.response == null){
          // can be null///////////////////////////////////////////////////////////////////////////////
          if( survey.survey_id.localeCompare("TEAM_FAMIL")==0 && question.qid.localeCompare('2')==0){

          }
          else if( survey.survey_id.localeCompare("TEAM_FAMIL")==0 && question.qid.localeCompare('3')==0){

          }
          else if( survey.survey_id.localeCompare("EVAL")==0 && question.qid.localeCompare('6')==0){

          }
          else if( survey.survey_id.localeCompare("SELF_EFF")==0 && question.qid.localeCompare('9')==0){

          }
          ///////////////////////////////////////////////////////////////////////////////////////////////////
          else{

            alert( "Please answer all the survey questions before clicking 'Complete Survey' ")
            complete = false;
            break;
          }
          
        }
        if( survey.survey_id.localeCompare("DEMO")==0 && question.qid.localeCompare('1')==0){
          let num = null;
          try{
            num = Number(question.response);
            console.log(num)
          }catch(e){
            console.log(e)
          }
          if(num==null || num==undefined || num<18 || isNaN(num) ) {
            alert( "Please enter a valid age over 18 for the first Survey item.")
            complete = false;
            break;
          }
        }
      }
    }

    if ( complete ){      
      
      //const mainDiv:HTMLDivElement = document.getElementById("mainWrapper") as HTMLDivElement;
      //mainDiv.scrollTop=0;
      const surveyDialog:HTMLDivElement = document.getElementById("SurveyDialog") as HTMLDivElement;
      surveyDialog.scrollTop=0;     

      this.sessionService.surveysObject.surveys[this.sessionService.activeSurveyIndex].complete=true;
      this.sessionService.surveysObject.participant_id = this.sessionService.participant_id;

      const headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'Bearer '+this.sessionService.bearerToken });
      console.log("Contacting URL : ");  
      console.log("https://"+this.sessionService.admin_host+"/SSOService/survey/UpdatePostTrialSurveyData/"+this.sessionService.participant_id)
      this.http.put( 
        // update for scaling mode
        "https://"+this.sessionService.admin_host+"/SSOService/survey/UpdatePostTrialSurveyData/"+this.sessionService.participant_id,
        this.sessionService.surveysObject.surveys[this.sessionService.activeSurveyIndex],
        {headers}
      ).subscribe( (response)=>{
        console.log("Survey Updated for : " + this.sessionService.participant_id)
      });

      

      this.sessionService.activeSurveyIndex++;

      if(this.sessionService.activeSurveyIndex == (this.sessionService.surveysObject as SurveysObject).surveys.length ){
        
        //publish on mqtt bus
        this.websocketService.socket.emit('publishSurveyObject',this.sessionService.surveysObject)
        
        // close at the end only
        this.dialogRef.close(); 
      }      
      
      console.log(this.sessionService.surveysObject);
      console.log ( "Active Survey Index: " + this.sessionService.activeSurveyIndex);
    }    
  }

}
