import { Component, OnInit} from '@angular/core';
import { MatDialog, MatDialogConfig} from '@angular/material/dialog';
import { ConnectionService } from '../Services/connection.service';
import { SessionService } from '../Services/session.service';
import { ToolService } from '../Services/tool.service';
import { WebsocketService } from '../Services/websocket.service';
import { ToolDialogComponent } from '../tool-dialog/tool-dialog.component';

@Component({
  selector: 'app-store-pane',
  templateUrl: './store-pane.component.html',
  styleUrls: ['./store-pane.component.scss']
})
export class StorePaneComponent implements OnInit {  

  itemAssets:[string,string,number,string][] = [
    ['Store/Icons/item_wirecutters_red.png',"WIRECUTTERS_RED",1,"NAME:WIRECUTTERS_RED, COST: 1, USAGE: LEFT-CLICK BOMB"],
    ['Store/Icons/item_wirecutters_green.png',"WIRECUTTERS_GREEN",1,"NAME:WIRECUTTERS_GREEN, COST: 1, USAGE: LEFT-CLICK BOMB"],
    ['Store/Icons/item_wirecutters_blue.png',"WIRECUTTERS_BLUE",1,"NAME:WIRECUTTERS_BLUE, COST: 1, USAGE: LEFT-CLICK BOMB"],
    ['Store/Icons/block_beacon_bomb.png',"BOMB_BEACON",0,"NAME:BOMB_BEACON, COST: 0, USAGE: RIGHT-CLICK BOMB"],
    ['Store/Icons/block_beacon_hazard.png',"HAZARD_BEACON",0,"NAME:HAZARD_BEACON, COST: 0, USAGE: RIGHT-CLICK GROUND"],
    ['Store/Icons/item_bomb_sensor.png',"BOMB_SENSOR",2,"NAME:BOMB_SENSOR, COST: 2, USAGE: RIGHT-CLICK WHILE HOLDING"],
    ['Store/Icons/item_fire_extinguisher.png',"FIRE_EXTINGUISHER",1,"NAME:FIRE_EXTINGUISHER, COST: 1, USAGE: RIGHT-CLICK FIRE"],
    ['Store/Icons/block_bomb_disposer.png',"BOMB_DISPOSER",10,"NAME:BOMB_DISPOSER, COST: 3, USAGE: RIGHT-CLICK BOMB"],
    ['Store/Icons/item_bomb_ppe.png',"BOMB_PPE",3,"NAME:BOMB_SENSOR, COST: 3, USAGE: RIGHT-CLICK WHILE HOLDING TO EQUIP"]
  ];  
  
  itemsAssetsWithFullPathAndName = [];
  trashCanAssetPath;
  
  

  constructor( 
      public sessionService:SessionService,       
      public websocketService:WebsocketService,
      public dialog: MatDialog, 
      public toolService:ToolService,
      public connectionService:ConnectionService 
    ) 
    { }

  ngOnInit(): void {
    
    // TESTING
    //this.drawToolsAndResetCounts();
    
    this.websocketService.trialInfoEmitter.subscribe( (event)=>{
      this.drawToolsAndResetCounts();
    });

    this.sessionService.recoveredStoredDataEmitter.subscribe( (event) =>{      
      let index = 0;
      this.itemAssets.forEach( assetPath =>{
        this.itemsAssetsWithFullPathAndName.push([this.sessionService.getAssetPath(assetPath[0]),assetPath[1],assetPath[2]]);       
        index++;  
      });      
    });

    this.trashCanAssetPath = this.sessionService.getAssetPath('Store/Icons/trashcan.png')    
  }

  drawToolsAndResetCounts(){    

    this.sessionService.toolCounts=[]
    this.itemsAssetsWithFullPathAndName=[]
    let index = 0;
    this.itemAssets.forEach( assetPath =>{
      this.itemsAssetsWithFullPathAndName.push([this.sessionService.getAssetPath(assetPath[0]),assetPath[1],assetPath[2]]);
      // callSignCode, itemName, itemCurrCount, itemProposedCount
      const rowRecord: [number,string,number,number][] = []
      for(let i = 0; i<this.sessionService.callSignArray.length; i++){
        rowRecord.push([i,assetPath[1],0,0])
      }       
      this.sessionService.toolCounts.push(rowRecord);
      //console.log(this.getToolCount(index,0))     
      index++;  
    });    
    
    //console.log(this.sessionService.toolCounts)
  }

  getToolNameFromAssetPath(assetPath:string):string{

    const split = assetPath.split(new RegExp('[.\/_]'));
    //console.log( split);
    const out = split[split.length-3]+'_'+split[split.length-2];
    //console.log(out);
    return out;
  }

  getToolCount(toolRow:number,callSignCode:number):[number,number]{    
    //sessionService.toolCounts is a 9x(numCallSigns) matrix with
    // tools being the column (9), and [callsign, itemname, currCount, propCount] x numCallSigns being the row (3 if full)
    // so 9 x 3 matrix with each member being [callsign, itemname, currCount, propCount]
    return [ this.sessionService.toolCounts[toolRow][callSignCode][2],this.sessionService.toolCounts[toolRow][callSignCode][3] ];

  }

  showToolInfo(event){

    const target:HTMLImageElement = event.target as HTMLImageElement;
    const id =  target.id;
    const split = id.split('_');    
    const code = Number.parseInt(split[0]);
    console.log("ItemCode : " + code );
    const config:MatDialogConfig = new MatDialogConfig()    
    config.data = {
      "itemCode":code
    }    
    this.dialog.open(ToolDialogComponent,config)

  }

  processPlusMinusClick(event:PointerEvent){
    //console.log(event);    
    const target:HTMLButtonElement = event.target as HTMLButtonElement;
    const id =  target.id;
    const split = id.split('_');
    const action = target.innerText.toLowerCase();
    const itemCode = Number.parseInt(split[0]);
    const callSignCode = Number.parseInt(split[1]);
    const cost:number = this.toolService.itemAssets[itemCode][2];
    let metaAction = "NOT_SET";
    const currentProposedCount = this.sessionService.toolCounts[itemCode][callSignCode][3];    
    let emitUpdate = false;
    if(action.localeCompare('+')==0){
      const isValidPurchaseWireCutters = this.checkValidWirecutterPurchase(itemCode,callSignCode);
      const isisValidPurchaseBudget = this.checkValidBudgetPurchase(itemCode);
      const isValidPurchase = isValidPurchaseWireCutters && isisValidPurchaseBudget;
      if( isValidPurchase ){        
        //this.sessionService.toolCounts[Number.parseInt(split[0])][Number.parseInt(split[1])][3]++;        
        metaAction = "PROPOSE_TOOL_PURCHASE_+";
        emitUpdate = true;      
      }
    } 
    else if(action.localeCompare('-')==0){
      if(currentProposedCount>0){
        //this.sessionService.toolCounts[Number.parseInt(split[0])][Number.parseInt(split[1])][3]--;
        //this.sessionService.teamBudget++;
        metaAction = "PROPOSE_TOOL_PURCHASE_-";
        emitUpdate = true
      }      
    }
    if( emitUpdate ){
      const proposedPurchaseObject = {    
          
        "participant_id":this.sessionService.participant_id,
        "element_id": id,
        "parent_id":"StoreInteractionContainer",
        "type": "BUTTON",
        "x": target.clientLeft,
        "y": target.clientTop,
        "additional_info":{
          "meta_action":metaAction,
          "item_code":itemCode,
          "call_sign_code":callSignCode,
          "cost":cost
        }
      }
      this.websocketService.socket.emit("proposedPurchase",proposedPurchaseObject);
    }   

  }

  checkValidBudgetPurchase(itemCode:number):boolean{
    // CHECK IF WE HAVE ENOUGH MONEY
    const budget = this.sessionService.teamBudget;
    const cost:number = this.itemAssets[itemCode][2];
    let out = true;
    if ( budget - cost < 0){
      console.log(" Not enough funds ");
      out = false;
      alert(" You lack the funds for this purchase.")
    }
    return out;
  }

  // CHECK THIS IS WORKING
  checkValidWirecutterPurchase(itemCode:number,callSignCode:number):boolean{

    let out = true;

    // CHECK MAX RWO WIRECUTTERS
    // wirecutters are 0,1,2
    if(itemCode <=2 ){
      const wirecuttersOwnedArray = [
        this.sessionService.toolCounts[0][callSignCode][2],
        this.sessionService.toolCounts[1][callSignCode][2],
        this.sessionService.toolCounts[2][callSignCode][2]
      ]
      const wirecuttersProposedArray = [
        this.sessionService.toolCounts[0][callSignCode][3],
        this.sessionService.toolCounts[1][callSignCode][3],
        this.sessionService.toolCounts[2][callSignCode][3]
      ]
      const ownedOrProposed = []      
      
      for(let i=0; i<wirecuttersOwnedArray.length; i++){
        if( wirecuttersOwnedArray[i] > 0 ){          
          if ( !ownedOrProposed.includes(i) ){
            ownedOrProposed.push(i);
          }
        }
      }

      for(let i=0; i<wirecuttersProposedArray.length; i++){
        if( wirecuttersProposedArray[i] > 0 ){          
          if ( !ownedOrProposed.includes(i) ){
            ownedOrProposed.push(i);
          }
        }
      }

      console.log(ownedOrProposed);

      if(!ownedOrProposed.includes(itemCode) && ownedOrProposed.length>=2){
        console.log("Too many wirecutters");
        out = false ;
        alert("2 Wirecutters already proposed or purchased! Please discard one type or set it's proposed count to 0")
      }
    }

    return out;
  }

  discardTool(event){

    const target:HTMLImageElement = event.target as HTMLImageElement;
    const id =  target.id;
    const split = id.split('_');
   
    const itemCode = Number.parseInt(split[0]);
    const callSignCode = Number.parseInt(split[1]);
   
    const countDiscarded = this.sessionService.toolCounts[itemCode][callSignCode][2];
    this.sessionService.toolCounts[itemCode][callSignCode][2] = 0;
    
    const itemDiscardObject = {      
      
      "participant_id":this.sessionService.participant_id,
      "item_name": this.itemAssets[itemCode][1],
      "amount_discarded" : countDiscarded
    }

    this.websocketService.socket.emit("itemDiscard",itemDiscardObject);

  }
  
  setReadyToLeaveStore(event:PointerEvent){

    const target:HTMLButtonElement = event.target as HTMLButtonElement;
    const id =  target.id;

    const readyToLeaveStoreObject = {      
      
      "participant_id":this.sessionService.participant_id,
      "element_id": id,
      "parent_id":"StoreInteractionContainer",
      "type": "BUTTON",
      "x": target.clientLeft,
      "y": target.clientTop,
      "additional_info":{
        "meta_action":"VOTE_LEAVE_STORE",        
        "call_sign_code":this.sessionService.callSignCode
      }
    }

    this.websocketService.socket.emit("readyToLeaveStore",readyToLeaveStoreObject);
    target.innerText = "Waiting on Teammates ..."

  }

  testInventoryUpdate(){
    const testObj:any = {
      "header": {
        "timestamp": "2023-02-28T15:47:40.616Z",
        "message_type": "simulator_event",
        "version": "1.2"
      },
      "msg": {
        "experiment_id": "946d4567-ab65-cfe7-b208-426305dc1234",
        "trial_id": "85ed4567-ab65-cfe7-b208-426305dc1234",
        "source": "MINECRAFT_SERVER",
        "sub_type": "0.0.0",
        "version": "Event:InventoryUpdate"
      },
      "data": {
        "mission_timer": "0 : 47",
        "elapsed_milliseconds": 118232,
        "currInv": {
          "P000007": {
            "BOMB_SENSOR": 10,
            "FIRE_EXTINGUISHER": 1,
            "BOMB_PPE": 10,
            "WIRECUTTERS_RED": 10,
            "HAZARD_BEACON": 10,
            "WIRECUTTERS_GREEN": 10,
            "WIRECUTTERS_BLUE": 10,
            "BOMB_BEACON": 10,
            "BOMB_DISPOSER": 10
          }
        }
      }
    }

    this.toolService.processInventoryUpdate(testObj);
    
  }
}
