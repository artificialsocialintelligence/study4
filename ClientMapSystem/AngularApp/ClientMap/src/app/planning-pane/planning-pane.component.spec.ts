import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanningPaneComponent } from './planning-pane.component';

describe('PlanningPaneComponent', () => {
  let component: PlanningPaneComponent;
  let fixture: ComponentFixture<PlanningPaneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanningPaneComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PlanningPaneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
