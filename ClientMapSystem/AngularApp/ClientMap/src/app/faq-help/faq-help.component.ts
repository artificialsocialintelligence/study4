import { AfterViewInit, Component, OnInit } from '@angular/core';
import { SessionService } from '../Services/session.service';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { WebsocketService } from '../Services/websocket.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MissionBriefComponent } from '../mission-brief/mission-brief.component';


@Component({
  selector: 'app-faq-help',
  templateUrl: './faq-help.component.html',
  styleUrls: ['./faq-help.component.scss']
})
export class FaqHelpComponent implements OnInit, AfterViewInit{

  faqIframeSrc: SafeResourceUrl;
  constructor(
    public websocketService:WebsocketService,
    public sessionService:SessionService,
    public sanitizer: DomSanitizer,
    public dialog:MatDialog
  ){

    websocketService.trialInfoEmitter.subscribe( ()=>{ this.loadUrl();})
    
  }

  ngOnInit(): void {
    
  }

  ngAfterViewInit(){
    //(document.getElementById('faq-iframe') as HTMLIFrameElement).src = this.sessionService.faqIframeSrc;
   
  }

  loadUrl(){
    //return  this.sanitizer.bypassSecurityTrustResourceUrl(this.sessionService.faqIframeSrc);
    this.faqIframeSrc = this.sanitizer.bypassSecurityTrustResourceUrl(this.sessionService.faqIframeSrc);
    return this.faqIframeSrc;
  }

  viewMissionBrief(){
    let dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.id = "MinecraftLoadingDialog";
    dialogConfig.disableClose = true;
    dialogConfig.position = { top: '5vh'} 
    //let data = "Welcome to the ClientMap! Once all of your teammates join the experiment (as you just did), the Trial Active indicator below should change to 'true'. The Minecraft Server will then begin loading. For the moment just hang tight!"
    //dialogConfig.data = data;
    this.dialog.open(MissionBriefComponent, dialogConfig);
  }
}
