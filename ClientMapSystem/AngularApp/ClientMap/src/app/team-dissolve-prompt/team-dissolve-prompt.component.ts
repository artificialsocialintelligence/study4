import { HttpClient } from '@angular/common/http';
import { Component, OnInit, Inject } from '@angular/core';
import { SessionService } from '../Services/session.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { WebsocketService } from '../Services/websocket.service';

@Component({
  selector: 'app-team-dissolve-prompt',
  templateUrl: './team-dissolve-prompt.component.html',
  styleUrls: ['./team-dissolve-prompt.component.scss']
})
export class TeamDissolvePromptComponent implements OnInit {

  reason:string;

  dialogContent:string;

  crash:boolean = false;

  data = null;

  showCancelButton = false;

  constructor( 
    public sessionService:SessionService, 
    public websocketService:WebsocketService,
    public http:HttpClient,
    public dialogRef:MatDialogRef<TeamDissolvePromptComponent>,
    @Inject (MAT_DIALOG_DATA) data    
  ) { 

    this.data = data;
     
  }

  ngOnInit(): void {
    this.websocketService.socket.emit("fetchTrialSummary")

  }

  ngAfterContentInit(){

    console.log(this.data);
    
    if( ((this.data.reason) as string).localeCompare('DISSOLVE_TEAM_PLAYER_LEFT')==0 ){
      
      if(this.sessionService.player_left_first){
        this.showCancelButton = true;
        this.dialogContent = 
        "You have decided to leave the trial in the middle of a game. This will end the trial for all participants. Press Confirm to be returned to the Waiting Room in roughly 5 seconds. If this is an error you may press the Cancel button." 
      }
      else{
        this.dialogContent = 
        "One of your teammates has decided to leave the game and the trial session must now end. Press Confirm to be returned to the Waiting Room in roughly 5 seconds." 
      }      
    }
    else if( ((this.data.reason) as string).localeCompare('DISSOLVE_TEAM_PLAYER_CHOICE')==0 ){
      this.dialogContent = 
      "One of your teammates has decided not to continue with this current team. Press Confirm to be returned to the Waiting Room in roughly 5 seconds." 
    }
    else if( ((this.data.reason) as string).localeCompare('MISSION_STOP_MINECRAFT_DISCONNECT')==0 ){
      this.dialogContent = 
      "One of your teammates has disconnected from Minecraft for over 3 minutes during an active trial. This trial session cannot continue. Press Confirm to be returned to the Waiting Room in roughly 5 seconds." 
    }
    else if( ((this.data.reason) as string).localeCompare('DISSOLVE_TEAM_CLIENTMAP_DISCONNECT')==0 ){
      this.dialogContent = 
      "One of your teammates has disconnected from their ClientMap for over 3 minutes during an active trial. This trial session cannot continue. Press Confirm to be returned to the Waiting Room in roughly 5 seconds." 
    }
    else if( ((this.data.reason) as string).localeCompare('DISSOLVE_TEAM_PROCEED_TIMEOUT')==0 ){
      this.dialogContent = 
      "One of your teammates never proceeded to this experiment, and the timeout has exceeded. Press Confirm to be returned to the Waiting Room in roughly 5 seconds."  
    }
    else if( ((this.data.reason) as string).localeCompare('MISSION_STOP_SERVER_CRASHED')==0 ){
      this.crash = true;
      this.dialogContent = 
      "The Minecraft Server has crashed unexpectedly ... this happens occasionally during research. Hold tight, we are restarting the Trial for you and your team!" 
    }
  }

  cancel(){
    this.dialogRef.close();
  }

  confirm(){
    if(this.sessionService.player_left_first){
      this.websocketService.socket.emit("leave_team_during_trial",{"name":this.sessionService.assignedName});
      console.log("Emitting Leave Tea During Trial ... ");
    }
    else{
      this.websocketService.socket.emit('confirm_leave_team', this.sessionService.assignedName);
      console.log("Emitting Confirm Leave Team ... ");
    }      
       
    console.log("Contacting URL : "); 
    console.log('https://'+this.sessionService.admin_host+'/WaitingRoom/return_player_from_trial');
    const body = {
      'name':this.sessionService.assignedName,
      'participant_id':this.sessionService.participant_id
    } 
    this.http.post<any>('https://'+this.sessionService.admin_host+'/WaitingRoom/return_player_from_trial',body).subscribe(
        {
          next: (v) => console.log(v),
          error: (e) => console.error(e),
          complete: () => {
            {              
              console.log(" Return Waiting Player call complete, inititating redirect after timeout.");
              setTimeout(()=>{ 
      
                console.log(" Redirecting to Waiting Room "); 
                console.log('https://'+this.sessionService.admin_host+'/WaitingRoom/player/'+this.sessionService.assignedName);
                const url = 'https://'+this.sessionService.admin_host+'/WaitingRoom/player/'+this.sessionService.assignedName
                window.location.href = url 
                
          
              },3000)  
            }  
          } 
        });
  }

  crashOk(){
    this.dialogRef.close();
  }

}
