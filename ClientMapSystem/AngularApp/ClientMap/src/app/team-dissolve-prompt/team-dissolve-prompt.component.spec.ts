import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamDissolvePromptComponent } from './team-dissolve-prompt.component';

describe('TeamDissolvePromptComponent', () => {
  let component: TeamDissolvePromptComponent;
  let fixture: ComponentFixture<TeamDissolvePromptComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TeamDissolvePromptComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TeamDissolvePromptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
