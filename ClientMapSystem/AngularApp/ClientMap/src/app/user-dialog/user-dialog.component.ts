import { AfterViewInit, Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { AppMode } from '../CustomClasses/Enums';
import { MissionBriefComponent } from '../mission-brief/mission-brief.component';
import { SessionService } from '../Services/session.service';
import { WebsocketService } from '../Services/websocket.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-dialog',
  templateUrl: './user-dialog.component.html',
  styleUrls: ['./user-dialog.component.scss']
})
export class UserDialogComponent implements OnInit, AfterViewInit {

  server_url

  constructor(
    public dialogRef: MatDialogRef<UserDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data, 
    public websocketService:WebsocketService,
    public sessionService:SessionService,
    public dialog: MatDialog,
    private route: ActivatedRoute
    ) {

    this.server_url = this.route.snapshot.queryParams["externalIP"];
      
    if (this.server_url == null) {
      this.server_url = window.location.hostname;
    }

    dialogRef.disableClose = true;
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(){
    
  }

  close(){
    const config:MatDialogConfig = new MatDialogConfig()
    //config.height = '94vh';
    config.disableClose = true;
   
    this.dialog.open(MissionBriefComponent,config).afterOpened().subscribe(()=>{
      setTimeout( ()=>{
        document.getElementById('mission-brief-div').scrollTo(0,0)
      },100)
      
    })
    
    
    this.dialogRef.close( ()=>{      
      this.sessionService.appMode = AppMode.PLAY;
    })
  }

}
