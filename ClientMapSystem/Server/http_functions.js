const http = require('http');
const https = require('https');
const config = require('./ClientMapConfig.json');

module.exports={

    createExperimentAndStartTrial: async function( author, name, condition, selected_players_array ){

        try{

            const url = `http://asistcontrol:5002/api/Experiment/CreateExperiment?author=${author}&name=${name}&condition=${condition}`
        
            http.get(url, res => {
    
                let rawData = ''
            
                 res.on('data', chunk => {
                    rawData += chunk
                })
                
                res.on('end', () => {
                    try{
                        const parsedData = JSON.parse(rawData)
                        console.log(parsedData)
                    }catch(e){
                        console.log(e)
                    }
                    
                    console.log("Starting Trial after Experiment created.")
                    this.startTrial(selected_players_array)
                })
            
            }).on("error", (e) =>{
                console.log("ERROR : " + e)
            })

        }
        catch(err){
            console.log(err)
        }
    },

    startTrial: async function( selected_players_array ) {

        console.log(" ------- RUNNING START TRIAL -------")
        var postData = JSON.stringify( selected_players_array );
        
        var options = {
            hostname: 'asistcontrol',
            port:'5002',
            path:'/api/Trial/StartTrial',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': postData.length
                }
        };
        
        var req = http.request(options, (res) => {
            console.log('statusCode:', res.statusCode);
            console.log('headers:', res.headers);
        
            res.on('data', (d) => {
            process.stdout.write(d);            
            });
        });
        
        req.on('error', (e) => {
            console.error(e);
        });
        
        await req.write(postData);
        await req.end();
        console.log(" ------- END START TRIAL CALL -------")
        this.startMinecraft()

        
    },
    
    startMinecraft:function( ){
        console.log(" ------- RUNNING START MINECRAFT -------")
        const mission_dto = {
            "name":"Demo_Mission",
            "mapname":config.map_name,
            "missionname":"Demo_Mission"
        }
        
        var postData = JSON.stringify( mission_dto );
        
        var options = {
            hostname: 'asistcontrol',
            port:'5002',
            path:'/api/Minecraft/StartMission',
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': postData.length
                }
        };
        
        var req = http.request(options, (res) => {
            console.log('statusCode:', res.statusCode);
            console.log('headers:', res.headers);
        
            res.on('data', (d) => {
                process.stdout.write(d);
            });
        });
        
        req.on('error', (e) => {
            console.error(e);
        });
        
        req.write(postData);
        req.end();
        console.log(" ------- ENDING START MINECRAFT CALL -------")
    },
    
    launchAdvisor:function( advisor ){
        console.log(" ------- Launching Advisor -------")
        const advisor_dto = {
            "agent_id": advisor,
            "command": "up"
        }
        
        var postData = JSON.stringify( advisor_dto );
        
        var options = {
            hostname: 'asistcontrol',
            port:'5002',
            path:'/api/Agent/LaunchAgent',
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': postData.length
                }
        };
        
        var req = http.request(options, (res) => {
            console.log('statusCode:', res.statusCode);
            console.log('headers:', res.headers);
        
            res.on('data', (d) => {
                process.stdout.write(d);
            });
        });
        
        req.on('error', (e) => {
            console.error(e);
        });
        
        req.write(postData);
        req.end();
        console.log(" ------- ENDING Launch Agent Call -------")
    },

    experimentAbandoned:async function( selected_players_array, admin_stack_ip, admin_stack_port ){

        console.log(" ------- START HTTPS : SENDING TRIAL ABANDONED TO WAITING ROOM -------")
        var postData = JSON.stringify( selected_players_array );
        
        var options = {
            hostname: admin_stack_ip,
            port: admin_stack_port,
            path:'/WaitingRoom/experiment_abandoned',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': postData.length
                }
        };
        
        
        var req = https.request(options, (res) => {
            console.log('statusCode:', res.statusCode);
            console.log('headers:', res.headers);
        
            res.on('data', (d) => {
            process.stdout.write(d);            
            });
        });
        
        req.on('error', (e) => {
            console.error(e);
        });
        
        await req.write(postData);
        await req.end();
        console.log(" ------- END HTTPS : SENDING TRIAL ABANDONED TO WAITING ROOM -------")

    },

    fetchTrialSummary:async function(ws, playername){

        let parsedData = null
        try{

            const url = `http://asistcontrol:5002/api/trial/getTrialRecord`
        
            http.get(url, async res => {
    
                let rawData = ''
            
                 res.on('data', chunk => {
                    rawData += chunk
                })
                
                res.on('end', () => {
                    try{
                        parsedData = JSON.parse(rawData)
                        console.log("Trial Summary fetched : " + parsedData)
                        ws.sendMessageToOneSocketAndStore("trialSummary",parsedData,playername)                       
                    }catch(e){
                        console.log(e)
                    }                    
                })
            
            }).on("error", (e) =>{
                console.log("ERROR : " + e)
            })
        }
        catch(err){
            console.log(err)
        }
        
        return parsedData
        
        

    }

}