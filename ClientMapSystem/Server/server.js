// ASIST
const mqtt = require('./mqtt_manager');

const http = require('./http_functions')

const express = require('express');

const app = express();

const bodyParser = require("body-parser");

const port = 3080;

const cors = require('cors');

const socketIO = require('socket.io');

const pm = require('./player_manager')
const sm = require('./websockets')
const StateManager = require('./state_manager')

const trial_info_manager = require('./trial_info_manager')

var corsOptions = {
  origin: 'http://localhost:4200',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

// read in config

const config = require('./ClientMapConfig.json');


const admin_stack_ip = process.env.admin_stack_ip;                
const admin_stack_port = process.env.admin_stack_port;
config.admin_stack_ip = admin_stack_ip
config.admin_stack_port = admin_stack_port
config.db_api_token = process.env.DB_API_TOKEN;
console.log(config);

// Initiate server object
server = app.listen(port, () => {
    
  console.log(`Server listening on the port::${port}`);     

});

const stateKeeperInstance = new StateManager.StateKeeper();

const player_manager = new pm.PlayerManager()
const socket_manager = new sm.SocketManager(server,player_manager,stateKeeperInstance);

console.log('Attempting to start mqtt connection ...')
const mqtt_manager = new mqtt.MqttManager(socket_manager,player_manager,stateKeeperInstance);

//this is messy see if we can deal with the circular refs later
player_manager.linkManagers(socket_manager,mqtt_manager)
socket_manager.linkMqttManager(mqtt_manager)

checkForDisconnects = function() {  
  //player_manager.printPlayers()
  if( (trial_info_manager.team_health === 'OK' || trial_info_manager.team_health === 'CONTINUE_WITH_TEAM') && trial_info_manager.trial_running ){

    let disconnect = false;
    player_manager.player_map.forEach( (v,k) =>{      
        if(v.disconnectTimestamp !== -1 ){
          console.log(k + ":" + (Date.now() - v.disconnectTimestamp) );        
          // make this timeout a setting in seconds
          if( Date.now() - v.disconnectTimestamp >= config.player_timeout_ms ){
              console.log(k + ": socket timeout detected. Removing Player and returning team to the waiting room.")
              player_manager.player_map.get(k).confirmed_left = true
              disconnect = true; 
          }
        }
    })
    if(disconnect){
      if( trial_info_manager.team_health === "OK" || trial_info_manager.team_health === "CONTINUE_WITH_TEAM" ){
        
        trial_info_manager.team_health = "DISSOLVE_TEAM_CLIENTMAP_DISCONNECT"
        
        const obj={
          "reason":trial_info_manager.team_health
        }
        player_manager.socket_manager.sendMessageToAllSocketsAndStore("dissolve_team",obj)
        
        if( player_manager.haveAllPlayersLeft() ){
          console.log( "All players confirmed as disconnected.")                    
          mqtt_manager.publish( "control/status/premature_mission_stop",JSON.stringify(obj));
          
          setTimeout(()=>{
            mqtt_manager.sendTrialStop(false) 
          },3000);                      
        }
        else{   
          setTimeout(()=>{
            console.log( "3 minute timeout after ClientMap disconnect ... sending Trial Stop.")                    
            mqtt_manager.publish( "control/status/premature_mission_stop",JSON.stringify(obj));            
          },180000);     
          setTimeout(()=>{
            mqtt_manager.sendTrialStop(false) 
          },183000);
        }
      }   
    } 
  }   
}

setInterval(checkForDisconnects,5000)

app.use(bodyParser.json());

app.use(express.static(process.cwd()+"/frontend/"));

app.use(cors());

app.get('/player/:from', (req,res) => {  
  res.sendFile(process.cwd()+"/frontend/index.html")
});

app.get('', (req,res) => {
  res.sendFile(process.cwd()+"/frontend/index.html")
});

//bodyparser.json() automatically turns the body into json so it's not necessary to parse it
app.post('/incoming_players', function(req, res){
  try{

    console.log("-------------------- NEW EXPERIMENT CYCLE -------------------------");

    trial_info_manager.team_health = "OK"
    trial_info_manager.trial_running = false
    player_manager.player_map.clear(); 
    socket_manager.client_sockets.clear();    
    trial_info_manager.trial_info = "NOT_SET"
    trial_info_manager.store_chat_id = 0;
    trial_info_manager.advisor_condition = "NOT_SET"
    trial_info_manager.experiment_created = false;   

    // stringified json array of Waiting Room Player objects
    const jsonArray = req.body 
    
    console.log("Incoming Players - Waiting Room Player Object: " + jsonArray)
    
    player_manager.setIncomingPlayers(jsonArray);
    stateKeeperInstance.init(jsonArray,socket_manager);
    
    res.send(jsonArray)

    console.log("Starting Trial Start Timeout routine with timeout value : " + config.experiment_proceed_timeout)

    // Sart timeout timer
    setTimeout(()=>{

      if(trial_info_manager.all_players_present === false){
        
        console.log("Players have not proceeded within alotted timeout period. Considering experiment abandoned." )
        
        trial_info_manager.team_health = "DISSOLVE_TEAM_PROCEED_TIMEOUT"
        
        socket_manager.sendMessageToAllSocketsAndStore("dissolve_team",{"reason":trial_info_manager.team_health})
        
       
      /*
       // This Event:MissionState message is necessary because Minecraft never starts, so it cannot publish a mission stop
       // message to populate the trial end condition ... so ClientMap does it instead.       
       const data = {
          "mission_timer":"NOT_SET",
          "elapsed_milliseconds":-1,
          "mission":"NOT_SET",
          "mission_state":"NOT_SET",
          "state_change_outcome": trial_info_manager.team_health
        }        
        const obj = mqtt_manager.generateAsistMqttMessage('simulator',"1.2","Event:MissionState","1.0.0",data)        
        mqtt_manager.publish("observations/events/mission", JSON.stringify(obj));
       /////////////////////////////////////////////////////////////////////////////////////////////////////////////
       
        mqtt_manager.sendTrialStop({"validTrial":false})        
      */

        console.log("Cleanup ClientMap Manually." )
        // CLEANUP MANUALLSY SINCE TRIAL STOP OR EXPORT
        trial_info_manager.team_health = "OK"
        trial_info_manager.trial_stop_in_progress = false
        trial_info_manager.store_chat_id = 0; 
        trial_info_manager.trial_info = "NOT_SET" 
        trial_info_manager.trial_running = false
        player_manager.player_map.clear(); 
        socket_manager.client_sockets.clear();
        trial_info_manager.advisor_condition = "NOT_SET"
        trial_info_manager.experiment_created = false;
        trial_info_manager.all_players_present = false;                       

        // send to WaitingRoom as http request

        http.experimentAbandoned(jsonArray,admin_stack_ip,admin_stack_port);

        setTimeout(()=>{                           
          // SEND INSTANCE DOWN
          const downMessage = {
              "reason":"EXPERIMENT_ABANDONED"                           
          }                            
          const message = JSON.stringify(mqtt_manager.generateAsistMqttMessage("control","1.2","Control:InstanceDown","0.0.0",downMessage))                                                                                    
          mqtt_manager.publish( "control/scaling/instance_down", message );          
        },10000);
      
      }

    },config.experiment_proceed_timeout)
    
  }
  catch(error){
    console.log(error)
    res.send(error)    
  }  
});

// END ROUTING





















