let trial_info = "NOT_SET"
let store_chat_id = 0;
let advisor_condition = "NOT_SET"
let experiment_created = false;
let trial_running = false;
let team_health = 'OK';
let trial_stop_in_progress = false
let all_players_present = false
let crash_recovery_in_progress = false

module.exports= {
    team_health, 
    trial_running,   
    trial_info,
    store_chat_id,
    advisor_condition,
    experiment_created,
    trial_stop_in_progress,
    all_players_present,
    crash_recovery_in_progress
}