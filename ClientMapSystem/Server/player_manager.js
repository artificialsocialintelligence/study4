const http = require('./http_functions')
const trial_info_manager = require('./trial_info_manager')
const state_manager = require('./state_manager')
class Player {
    
    constructor(playerinfo){
        
        this.name = "NOT_SET"
        this.participant_id ="NOT_SET"
        this.socket = "DISCONNECTED"
        this.callsign = "NOT_SET"
        this.continue_with_team = false 
        this.advisor_condition = "NOT_SET"
        this.lastPingMilliseconds = -1
        this.disconnectTimestamp = -1
        this.surveysPublished = false
        this.confirmed_left = false       
        this.setJsonModel(playerinfo)
    }    

    setJsonModel(playerinfo) {

        const keysComingIn = Object.keys(playerinfo)
        const keysWeHave = Object.keys(this)

        keysComingIn.forEach( (k)=>{
            //console.log("Coming In :" + k)
            if(keysWeHave.includes(k)){
                //console.log("Has this key - setting it now.")
                this[k]=playerinfo[k]
            }
        })

        console.log("SetJsonModel : " + this.getJsonModel())
    }
    
    // returns the player model the frontend expects
    getJsonModel(){
        
        return JSON.stringify(this)
    }
}
class PlayerManager{
    
    constructor() {
        this.player_map=new Map()
        this.callsign_map = new Map() 
                    
    }

    linkManagers(sm,mm){
        this.socket_manager = sm
        this.mqtt_manager = mm
    }

    linkSocketManager(sm){
        this.socket_manager=sm
        this.socket_manager.sendMessageToAllSocketsAndStore("socket_test","Socket is working from PlayerManager class, non-broadcast" )       
    }

    async setIncomingPlayers(player_json_array){
        this.player_map.clear()
        this.callsign_map.clear()
        player_json_array.forEach( (p) =>{
            p.socket = "DISCONNECTED" 
            this.player_map.set(p['name'],new Player(p))
            this.callsign_map.set(p['callsign'],p['name'])
        })
        
        trial_info_manager.advisor_condition = player_json_array[0].advisor_condition
        console.log("Set Advisor Condition : " + trial_info_manager.advisor_condition) 
        
        // launch advisor
        if(trial_info_manager.advisor_condition !== "NO_ADVISOR"){
            console.log("Bringing up " + trial_info_manager.advisor_condition )
            await http.launchAdvisor(trial_info_manager.advisor_condition)
        } 
        
        

        this.printPlayers()
    }

    haveAllPlayersLeft(){
        
        const num_players = this.player_map.size;
        let num_confirmed_left = 0;
        this.player_map.forEach( (v,k)=>{
            if( v.confirmed_left === true ){
                num_confirmed_left++;
            }
        })        
        
        console.log( "Players Confirmed Left : " + num_confirmed_left + " /" + num_players)

                   
        let out = true;

        for (let [k, v] of this.player_map) {
            console.log(k+" : "+v.confirmed_left)
            if (v.confirmed_left === false){
                out = false;
                break;
            }
        }

        console.log( "All confirmed left : " + out )
        
        return out;
        
        

    }

    areAllPlayersConnected(){
        
        if(this.player_map.size>0){
            
            let out = true
            this.player_map.forEach((v,k)=>{
                console.log(k+" : "+v.socket)
                if(v.socket == "DISCONNECTED"){
                    out=false
                    // returns from foreach - use like break
                    return
                }
            });
            console.log( "All Connected : " + out )
            return out
        }
        else{
            return false
        }        
    }

    async startExperiment(){
        
       
           

            const player_array = []
            const player_pid_array = []
            this.player_map.forEach((v,k)=>{
                player_array.push(v)
                player_pid_array.push(v.participant_id)
            })
            const author = "adminless"
            const name = player_pid_array.join("_")+("_")+(new Date().toUTCString())
            if(trial_info_manager.advisor_condition !== "NO_ADVISOR"){
                console.log(" Wait 15 seconds for ASI to start. ")
                setTimeout( async ()=>{
                    await http.createExperimentAndStartTrial(author,name,trial_info_manager.advisor_condition,player_array)
                    
                    const obj = {
                        "ready":false
                    }           
                    this.mqtt_manager.publish("status/clientmap/ready_for_team",JSON.stringify(obj));
                    
                    setTimeout( ()=>{
                        console.log("Requesting trial info 3 seconds after starting trial.");
                        this.mqtt_manager.publish("control/request/getTrialInfo","{}")
                    },3000)
                },15000);
            }
            else{
                await http.createExperimentAndStartTrial(author,name,trial_info_manager.advisor_condition,player_array)
                    
                const obj = {
                    "ready":false
                }           
                this.mqtt_manager.publish("status/clientmap/ready_for_team",JSON.stringify(obj));
                
                setTimeout( ()=>{
                    console.log("Requesting trial info 3 seconds after starting trial.");
                    this.mqtt_manager.publish("control/request/getTrialInfo","{}")
                },3000)
            }
    
    }

    async startTrial(){
        const player_array = []
        this.player_map.forEach((v,k)=>{
            player_array.push(v)
        })
        http.startTrial(player_array)
    
    }

    startMission(){        
        http.startMission()
    }

    getPlayers(){
        return this.player_map
    }

    printPlayers(){
        this.player_map.forEach( (v,k) =>
            console.log(k + " : " + v.getJsonModel() )
        )
    }

    

}

module.exports = {
    PlayerManager,
    Player
}