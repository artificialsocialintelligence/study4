docker network create asist_net

#@echo off
echo Bring up Mosquitto Broker
pushd ..\mqtt
docker compose up -d 
sleep 5

#echo off
echo Bring up ELK
pushd ..\ELK-Container
#set MQTT_HOST=mosquitto
docker compose --compatibility up -d --build
sleep 5

popd
echo Bring up metadata server
pushd ..\metadata\metadata-docker
docker compose up --build -d
popd

# echo Bring up Import/Export dashboard
# pushd ..\metadata\metadata-web
# docker compose up --build -d
# popd

# NEED TO COPY MINECRAFT VOLUME OVER HERE

#


echo Bring up Core Stack
pushd ..\Local
docker compose -f docker compose.instance.adminless.yml up -d
popd
docker ps