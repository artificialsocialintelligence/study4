#!/bin/bash
export root_dir="."

# THESE WILL HAVE BEEN EXPORTED BY ASISTCONTROL PRIOR TO RUNNING THIS SCRIPT
ADMIN_STACK_IP=$ADMIN_STACK_IP
INSTANCE_IP=$INSTANCE_IP

helpFunction()
{
    echo ""
    echo "Usage: $0 [-h] [-f] [-a] [-d] [-e] [-g] [-j] [-k] [-l] [-r] [-m]"
    echo -e "\t-h display help text"
    echo -e "\t-a Do not start up the ASI_CMU_TA1_ATLAS"
    echo -e "\t-c Do not start up the AC_CMUFMS_TA2_Cognitive Agent"
    echo -e "\t-d Do not start up the AC_UAZ_TA1_DialogAgent"    
    echo -e "\t-f Do not start up the AC_CMU_TA1_PyGLFoVAgent"
    echo -e "\t-g Do not start up the AC_GALLUP_TA2_GEM"
    echo -e "\t-i Set ADMIN_STACK_IP to INTERNAL IP IF USING GOOGLE CLOUD"
    echo -e "\t-j Do not start up the AC_IHMC_TA2_Joint-Activity-Interdependence Agent"
    echo -e "\t-k Do not start up the AC_UCF_TA2_Flocking Agent"
    echo -e "\t-l Do not start up the AC_IHMC_TA2_Location-Monitor"
    echo -e "\t-r Do not start up the ASI_DOLL_TA1_Rita"
    echo -e "\t-m Start up the Aptima TA3 Measures agent"
    echo -e "\t-p Do not start up the AC_Cornell_TA2_Cooperation"
    echo -e "By default all agents are started up."
    exit 1
}

dont_run_lm="false"
dont_run_jai="false"
dont_run_cog="false"
dont_run_flocking="false"
dont_run_rita="false"
dont_run_atlas="false"
dont_run_gallup="false"
dont_run_dialog="false"
dont_run_fov="false"
dont_run_measures="false"
dont_run_cc="false"

while getopts "abcdefghijklmnopqrstuvwxyz" opt
do
        case "$opt" in
            h ) helpFunction ;;
            a ) dont_run_atlas="true";;
            c ) dont_run_cog="true";;
            d ) dont_run_dialog="true";;            
            f ) dont_run_fov="true";;
            g ) dont_run_gallup="true";;            
            j ) dont_run_jai="true";;
            k ) dont_run_flocking="true";;
            l ) dont_run_lm="true";;
            r ) dont_run_rita="true";;
            m ) dont_run_measures="true";;            
            p ) dont_run_cc = "true";;           
        esac
done

echo "Bouncing all Agent Container with root dir : $root_dir - DOWN STEP." 

echo "--------------DOWN STEP----------------"

################ AC_IHMC_TA2_Location-Monitor Agent ####################
echo "Bringing down the AC_IHMC_TA2_Location-Monitor Agent"
    pushd "$root_dir"/Agents/AC_IHMC_TA2_Location-Monitor;./agent.sh down 
popd

################ AC_IHMC_TA2_Joint-Activity-Interdependence Agent ####################
echo "Bringing down the AC_IHMC_TA2_Joint-Activity-Interdependence Agent"
    pushd "$root_dir"/Agents/AC_IHMC_TA2_Joint-Activity-Interdependence;./agent.sh down 
popd

################ AC_CMUFMS_TA2_Cognitive Agent ####################
echo "Bringing down the AC_CMUFMS_TA2_Cognitive Agent"
    pushd "$root_dir"/Agents/AC_CMUFMS_TA2_Cognitive;./agent.sh down 
popd

################ AC_UCF_TA2_Flocking Agent ####################
echo "Bringing down the AC_UCF_TA2_Flocking Agent"
    pushd "$root_dir"/Agents/AC_UCF_TA2_Flocking;./agent.sh down 
popd

################ AC_UAZ_TA1_DialogAgent ####################
pushd "$root_dir"/Agents/AC_UAZ_TA1_DialogAgent
    ./agent.sh down 
popd

################ AC_CMU_TA1_PyGLFoVAgent ###################
echo "Bringing down AC_CMU_TA1_PyGLFoVAgent"
pushd "$root_dir"/Agents/AC_CMU_TA1_PyGLFoVAgent
    ./down.sh 
popd

################ ASI_CMU_TA1_ATLAS #########################
echo "Bringing down ASI_CMU_TA1_ATLAS"
pushd "$root_dir"/Agents/ASI_CMU_TA1_ATLAS
    ./down.sh 
popd

################ AC_Cornell_TA2_Cooperation #########################
echo "Bringing down AC_Cornell_TA2_Cooperation"
    pushd "$root_dir"/Agents/AC_Cornell_TA2_Cooperation;./agent.sh down
popd

################   ASI_DOLL_TA1_Rita ############################################################
echo "Bringing down ASI_DOLL_TA1_Rita"
pushd "$root_dir"/Agents/Rita_Agent
    docker compose --env-file settings.env down --remove-orphans 
popd


################  GEM  ############################################################
pushd "$root_dir"/Agents/AC_GALLUP_TA2_GEM
    ./agent.sh down 
popd


################  AC_Aptima_TA3_measures  ############################################################
pushd "$root_dir"/Agents/AC_Aptima_TA3_measures
    ./agent.sh down 
popd

#################################################################################################

echo "--------------UP STEP----------------"
################   AC_IHMC_TA2_Location-Monitor Agent ####################
if [ $dont_run_lm = "false" ]; then
    echo "Bringing up the AC_IHMC_TA2_Location-Monitor Agent"
    pushd "$root_dir"/Agents/AC_IHMC_TA2_Location-Monitor;./agent.sh upd
    popd
else
    echo "Skipping bring up AC_IHMC_TA2_Location-Monitor Agent"
fi
################################################################################################

################   AC_IHMC_TA2_Joint-Activity-Interdependence Agent ####################
if [ $dont_run_jai = "false" ]; then
    echo "Bringing up the AC_IHMC_TA2_Joint-Activity-Interdependence Agent"
    pushd "$root_dir"/Agents/AC_IHMC_TA2_Joint-Activity-Interdependence;./agent.sh upd
    popd
else
    echo "Skipping bring up AC_IHMC_TA2_Joint-Activity-Interdependence Agent"
fi
#################################################################################################

################   AC_CMUFMS_TA2_Cognitive Agent ####################
if [ $dont_run_cog = "false" ]; then
    echo "Bringing up the AC_CMUFMS_TA2_Cognitive Agent"
    pushd "$root_dir"/Agents/AC_CMUFMS_TA2_Cognitive;./agent.sh upd
    popd
else
    echo "Skipping bring up AC_CMUFMS_TA2_Cognitive Agent"
fi
#################################################################################################

################   AC_UCF_TA2_Flocking Agent ####################
if [ $dont_run_flocking = "false" ]; then
    echo "Bringing up the AC_UCF_TA2_Flocking Agent"
    pushd "$root_dir"/Agents/AC_UCF_TA2_Flocking;./agent.sh upd
    popd
else
    echo "Skipping bring up AC_UCF_TA2_Flocking Agent"
fi
#################################################################################################

################# DIALOG AGENT ##################################################################
if [ $dont_run_dialog = "false" ]; then
    echo "Bringing up the UAZ Dialog Agent"
    pushd "$root_dir"/Agents/AC_UAZ_TA1_DialogAgent
        ./agent.sh upd
    popd
else
    echo "Skipping bringing up the UAZ Dialog agent"
fi

################   ASI_DOLL_TA1_Rita ############################################################
if [ $dont_run_rita = "false" ]; then
    echo "Bringing up the Doll/MIT Rita Agent"
    pushd "$root_dir"/Agents/Rita_Agent
        echo "$PWD: Starting Rita Agent"
        dte=$(date "+%B-%Y-%d")
        log_dir="logs-${dte}"
        export SERVICE_LOGS_DIR="$log_dir/"
        echo "${SERVICE_LOGS_DIR} Log dirs for this instantiation of Rita"
        docker compose --env-file settings.env up -d
    popd
else
    echo "Skipping bringing up the Doll/MIT Rita agent"
fi
#################################################################################################
################# AC_CMU_TA1_PyGLFoVAgent ##################################
if [ $dont_run_fov = "false" ]; then
	echo "Bringing up CMU-TA1 PyGLFoVAgent"
	pushd "$root_dir"/Agents/AC_CMU_TA1_PyGLFoVAgent
		echo "$PWD: Staring PyGLFoVAgent"
		./up.sh
	popd
else
	echo "Skipping bringing up PyGLFoVAgent"
fi
################# ASI_CMU_TA1_ATLAS ########################################
if [ $dont_run_atlas = "false" ]; then
    echo "Bringing up CMU-TA1 ATLAS Agent"
    pushd "$root_dir"/Agents/ASI_CMU_TA1_ATLAS
        echo "$PWD: Starting ATLAS"
        ./up.sh
    popd
else
    echo "Skipping bringing up ATLAS"
fi

################# AC_Cornell_TA2_Cooperation ########################################
if [ $dont_run_cc = "false" ]; then
    echo "Bringing up the AC_Cornell_TA2_Cooperation Agent"
    pushd "$root_dir"/Agents/AC_Cornell_TA2_Cooperation;./agent.sh upd
    popd
else
    echo "Skipping bring up AC_Cornell_TA2_Cooperation Agent"
fi

################# AC_Aptima_TA3_measures ########################################
if [ $dont_run_measures = "false" ]; then
    echo "Bringing up AC_Aptima_TA3_measures Agent"
    pushd "$root_dir"/Agents/AC_Aptima_TA3_measures
        echo "$PWD: Starting Measures Agent"
        ./agent.sh upd
    popd
else
    echo "Skipping bringing up Measures Agent"
fi

################  GEM  ############################################################
if [ $dont_run_gallup = "false" ]; then
    echo "Bringing up the GEM Agent"
    pushd "$root_dir"/Agents/AC_GALLUP_TA2_GEM
        echo "$PWD: Starting GEM Agent"
        dte=$(date "+%B-%Y-%d")
        log_dir="logs-${dte}"
        export SERVICE_LOGS_DIR="$log_dir/"
        echo "${SERVICE_LOGS_DIR} Log dirs for this instantiation of GEM"
        docker compose --env-file settings.env up -d
    popd
else
    echo "Skipping bringing up the GEM agent"
fi
#################################################################################################
#

