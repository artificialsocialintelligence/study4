#!/bin/bash
export ADMIN_STACK_IP=
export ADMIN_STACK_PORT=9000
export DB_API_TOKEN=
export DB_API_ISSUER_KEY=
export ADMIN_ID=
export ADMIN_PWD=
export PGADMIN_DEFAULT_EMAIL=
export PGADMIN_DEFAULT_PASSWORD=
export POSTGRES_PASSWORD=
export POSTGRES_USER=
export POSTGRES_DB=
export DB_CONNECTION_PWD=
export DB_CONNECTION_USER=
export DB_CONNECTION_HOST=
export DB_CONNECTION_PORT=
export DB_CONNECTION_DATABASE=
export EMAIL_PWD=
export SSO_SECRET_KEY= 
export GCLOUD_KEYFILE=
export GCLOUD_PROJECT_ID=
export GCS_BACKUP_BUCKET= 

sleep 5
sh ./testbed_up_adminless.sh -ar
