#!/bin/bash

set -e
set -u

# Script to automatically build the testbed on Linux and macOS systems.
# Usage: ./testbed_build_adminless.sh

# Get the top-level ASIST testbed repo directory. The pushd/popd commands use
# this directory, so that this script can be safely executed from any
# directory.
export root_dir="$( cd "$(dirname "${BASH_SOURCE[0]}" )/../" >/dev/null 2>&1 && pwd)"
FILE=../version.txt
if test -f "$FILE"; then
    TAG=$(cat ../version.txt)
    # echo $TAG > 'latest-version.txt'
    BUILD_TYPE=""
    BUILD_NUMBER=""
    if [[ "$TAG" == *"-"* ]]; then
        BUILD="${TAG#*-}"
        BUILD_TYPE="-${BUILD%%.*}"
        BUILD_NUMBER=".${BUILD#*.}"
    fi

    # Creates strings with the following structure:
    # BUILD: 1.0.0-dev.0
    # PATCH: 1.0.0-dev
    # MINOR: 1.0-dev
    # MAJOR: 1-dev
    PATCH="${TAG%-*}"
    MINOR="${PATCH%.*}"
    MAJOR="${MINOR%.*}"
    BUILD="$PATCH"

    BUILD="$PATCH$BUILD_TYPE$BUILD_NUMBER"
    PATCH="$PATCH$BUILD_TYPE"
    MINOR="$MINOR$BUILD_TYPE"
    MAJOR="$MAJOR$BUILD_TYPE"
else
    PATCH=latest
    MINOR=latest
    MAJOR=latest
    LATEST=latest
    BUILD=latest
fi

echo "$BUILD"

echo "Updating AsistControl container"
pushd "$root_dir"/AsistControl
    if [ ! $BUILD == "latest" ]
    then
        sed -i "s/\"system_version\": \"NOT SET\"/\"system_version\": \"${BUILD}\"/" ../Local/AsistControl/appsettings.Production.json
    fi
    docker build -t asistcontrol:latest --build-arg CACHE_BREAKER=$(date +%s) .
    if [[ $? -ne 0 ]]; then
        echo "Failed to build AsistControl container, exiting now."
        exit 1
    fi
    # if [ ! -z "${APTIMADOCKERREGPW:-}" ] ; then
    #     docker login gitlab.asist.aptima.com:5050 --username=dhoward --password=${APTIMADOCKERREGPW}
    #     docker tag asistcontrol:${BUILD} gitlab.asist.aptima.com:5050/asist/testbed/asistcontrol:${BUILD}
    #     docker tag asistcontrol:${BUILD} gitlab.asist.aptima.com:5050/asist/testbed/asistcontrol:${MINOR}
    #     docker tag asistcontrol:${BUILD} gitlab.asist.aptima.com:5050/asist/testbed/asistcontrol:${MAJOR}
    #     docker tag asistcontrol:${BUILD} gitlab.asist.aptima.com:5050/asist/testbed/asistcontrol:latest
    #     docker push gitlab.asist.aptima.com:5050/asist/testbed/asistcontrol:${BUILD}
    #     docker push gitlab.asist.aptima.com:5050/asist/testbed/asistcontrol:${MINOR}
    #     docker push gitlab.asist.aptima.com:5050/asist/testbed/asistcontrol:${MAJOR}
    #     docker push gitlab.asist.aptima.com:5050/asist/testbed/asistcontrol:latest
    # fi
popd

echo "Building/updating the ClientMap container"
pushd "$root_dir"/ClientMapSystem
    #docker build -t client_map:${BUILD} --build-arg CACHE_BREAKER=$(date +%s) .
    docker build -t client_map:latest --build-arg CACHE_BREAKER=$(date +%s) .
    if [[ $? -ne 0 ]]; then
        echo "Failed to build client_map container, exiting now."
        exit 1
    fi
    # if [ ! -z "${APTIMADOCKERREGPW:-}" ] ; then
    #     docker login gitlab.asist.aptima.com:5050 --username=dhoward --password=${APTIMADOCKERREGPW}
    #     docker tag client_map:${BUILD} gitlab.asist.aptima.com:5050/asist/testbed/client_map:${BUILD}
    #     docker tag client_map:${BUILD} gitlab.asist.aptima.com:5050/asist/testbed/client_map:${MINOR}
    #     docker tag client_map:${BUILD} gitlab.asist.aptima.com:5050/asist/testbed/client_map:${MAJOR}
    #     docker tag client_map:${BUILD} gitlab.asist.aptima.com:5050/asist/testbed/client_map:latest
    #     docker push gitlab.asist.aptima.com:5050/asist/testbed/client_map:${BUILD}
    #     docker push gitlab.asist.aptima.com:5050/asist/testbed/client_map:${MINOR}
    #     docker push gitlab.asist.aptima.com:5050/asist/testbed/client_map:${MAJOR}
    #     docker push gitlab.asist.aptima.com:5050/asist/testbed/client_map:latest
    # fi
popd

echo "Building/updating the Waiting Room container"
pushd "$root_dir"/WaitingRoom
    #docker build -t client_map:${BUILD} --build-arg CACHE_BREAKER=$(date +%s) .
    docker build -t waitingroom:latest --build-arg CACHE_BREAKER=$(date +%s) .
    if [[ $? -ne 0 ]]; then
        echo "Failed to build waitingroom container, exiting now."
        exit 1
    fi
    # if [ ! -z "${APTIMADOCKERREGPW:-}" ] ; then
    #     docker login gitlab.asist.aptima.com:5050 --username=dhoward --password=${APTIMADOCKERREGPW}
    #     docker tag waitingroom:${BUILD} gitlab.asist.aptima.com:5050/asist/testbed/waitingroom:${BUILD}
    #     docker tag waitingroom:${BUILD} gitlab.asist.aptima.com:5050/asist/testbed/waitingroom:${MINOR}
    #     docker tag waitingroom:${BUILD} gitlab.asist.aptima.com:5050/asist/testbed/waitingroom:${MAJOR}
    #     docker tag waitingroom:${BUILD} gitlab.asist.aptima.com:5050/asist/testbed/waitingroom:latest
    #     docker push gitlab.asist.aptima.com:5050/asist/testbed/waitingroom:${BUILD}
    #     docker push gitlab.asist.aptima.com:5050/asist/testbed/waitingroom:${MINOR}
    #     docker push gitlab.asist.aptima.com:5050/asist/testbed/waitingroom:${MAJOR}
    #     docker push gitlab.asist.aptima.com:5050/asist/testbed/waitingroom:latest
    # fi
popd

echo "Building/updating the SSOService container"
pushd "$root_dir"/SinglePointSignOn/SPSOService
    #docker build -t client_map:${BUILD} --build-arg CACHE_BREAKER=$(date +%s) .
    docker build -t ssoservice:latest --build-arg CACHE_BREAKER=$(date +%s) .
    if [[ $? -ne 0 ]]; then
        echo "Failed to build ssoservice container, exiting now."
        exit 1
    fi
    # if [ ! -z "${APTIMADOCKERREGPW:-}" ] ; then
    #     docker login gitlab.asist.aptima.com:5050 --username=dhoward --password=${APTIMADOCKERREGPW}
    #     docker tag ssoservice:${BUILD} gitlab.asist.aptima.com:5050/asist/testbed/ssoservice:${BUILD}
    #     docker tag ssoservice:${BUILD} gitlab.asist.aptima.com:5050/asist/testbed/ssoservice:${MINOR}
    #     docker tag ssoservice:${BUILD} gitlab.asist.aptima.com:5050/asist/testbed/ssoservice:${MAJOR}
    #     docker tag ssoservice:${BUILD} gitlab.asist.aptima.com:5050/asist/testbed/ssoservice:latest
    #     docker push gitlab.asist.aptima.com:5050/asist/testbed/ssoservice:${BUILD}
    #     docker push gitlab.asist.aptima.com:5050/asist/testbed/ssoservice:${MINOR}
    #     docker push gitlab.asist.aptima.com:5050/asist/testbed/ssoservice:${MAJOR}
    #     docker push gitlab.asist.aptima.com:5050/asist/testbed/ssoservice:latest
    # fi
popd

echo "Building/updating the MQTT Message Validator container"
pushd "$root_dir"/MQTTValidationServiceContainer
    docker build  -t mqttvalidationservice:latest --build-arg CACHE_BREAKER=$(date +%s) .
    if [[ $? -ne 0 ]]; then
        echo "Failed to build mqttvalidationservice container, exiting now."
        exit 1
    fi
    # if [ ! -z "${APTIMADOCKERREGPW:-}" ] ; then
    #     docker login gitlab.asist.aptima.com:5050 --username=dhoward --password=${APTIMADOCKERREGPW}
    #     docker tag mqttvalidationservice:${BUILD} gitlab.asist.aptima.com:5050/asist/testbed/mqttvalidationservice:${BUILD}
    #     docker tag mqttvalidationservice:${BUILD} gitlab.asist.aptima.com:5050/asist/testbed/mqttvalidationservice:${MINOR}
    #     docker tag mqttvalidationservice:${BUILD} gitlab.asist.aptima.com:5050/asist/testbed/mqttvalidationservice:${MAJOR}
    #     docker tag mqttvalidationservice:${BUILD} gitlab.asist.aptima.com:5050/asist/testbed/mqttvalidationservice:latest
    #     docker push gitlab.asist.aptima.com:5050/asist/testbed/mqttvalidationservice:${BUILD}
    #     docker push gitlab.asist.aptima.com:5050/asist/testbed/mqttvalidationservice:${MINOR}
    #     docker push gitlab.asist.aptima.com:5050/asist/testbed/mqttvalidationservice:${MAJOR}
    #     docker push gitlab.asist.aptima.com:5050/asist/testbed/mqttvalidationservice:latest
    # fi
popd

echo "Building/updating the Scaling Metric container"
pushd "$root_dir"/ScalingMetricContainer
    docker build  -t scalingmetricservice:latest --build-arg CACHE_BREAKER=$(date +%s) .
    if [[ $? -ne 0 ]]; then
        echo "Failed to build scalingmetricservice container, exiting now."
        exit 1
    fi
popd

echo "Building/updating the Logstash container"
pushd "$root_dir"/ELK-Container/context/logstash
    docker build  -t logstash:latest .
    if [[ $? -ne 0 ]]; then
        echo "Failed to build logstash container, exiting now."
        exit 1
    fi
    # if [ ! -z "${APTIMADOCKERREGPW:-}" ] ; then
    #     docker login gitlab.asist.aptima.com:5050 --username=dhoward --password=${APTIMADOCKERREGPW}
    #     docker tag logstash:${BUILD} gitlab.asist.aptima.com:5050/asist/testbed/logstash:${BUILD}
    #     docker tag logstash:${BUILD} gitlab.asist.aptima.com:5050/asist/testbed/logstash:${MINOR}
    #     docker tag logstash:${BUILD} gitlab.asist.aptima.com:5050/asist/testbed/logstash:${MAJOR}
    #     docker tag logstash:${BUILD} gitlab.asist.aptima.com:5050/asist/testbed/logstash:latest
    #     docker push gitlab.asist.aptima.com:5050/asist/testbed/logstash:${BUILD}
    #     docker push gitlab.asist.aptima.com:5050/asist/testbed/logstash:${MINOR}
    #     docker push gitlab.asist.aptima.com:5050/asist/testbed/logstash:${MAJOR}
    #     docker push gitlab.asist.aptima.com:5050/asist/testbed/logstash:latest
    # fi
popd



echo "Building/updating the metadata-app stack"
pushd "$root_dir"/metadata/metadata-docker
    if [ ! $BUILD == "latest" ]
    then
        sed -i "s/TESTBED_VERSION=NOT SET/TESTBED_VERSION=${BUILD}/" metadata-app.env
    fi    
    docker compose build --no-cache
    if [[ $? -ne 0 ]]; then
        echo "Failed to build metadata-app stack, exiting now."
        exit 1
    fi    
popd

echo "Building/updating the metadata-web container"
pushd "$root_dir"/metadata/metadata-web
    if [ ! $BUILD == "latest" ]
    then
        sed -i "s/TESTBED_VERSION=NOT SET/TESTBED_VERSION=${BUILD}/" metadata-web.env
        # WE SHOULD UPDATE THE HOST HERE TO SO WE DON'T HAVE TO MANUALLY DO IT EACH TIME
    fi
    docker build  -t metadata-web:latest .
    if [[ $? -ne 0 ]]; then
        echo "Failed to build metadata-web container, exiting now."
        exit 1
    fi
popd

echo "Building/updating the internal-monitoring-service container"
pushd "$root_dir"/MonitoringService/internal-monitoring-service
    docker build  -t internal-monitoring-service:latest -f docker/context/internal-monitoring-service/Dockerfile .
    if [[ $? -ne 0 ]]; then
        echo "Failed to build internal-monitoring-service container, exiting now."
        exit 1
    fi
popd

echo "Building/updating the external-monitoring-service container"
pushd "$root_dir"/MonitoringService/external-monitoring-service
    docker build  -t external-monitoring-service:latest .
    if [[ $? -ne 0 ]]; then
        echo "Failed to build external-monitoring-service container, exiting now."
        exit 1
    fi
popd

echo "Building/updating the external-monitoring-service-backend container"
pushd "$root_dir"/MonitoringService/external-monitoring-service-backend
    docker build  -t external-monitoring-service-backend:latest -f docker/context/external-monitoring-service-backend/Dockerfile .
    if [[ $? -ne 0 ]]; then
        echo "Failed to build external-monitoring-service-backend container, exiting now."
        exit 1
    fi
popd

################   AC_IHMC_TA2_Location-Monitor Agent ####################
echo "Building/updating AC_IHMC_TA2_Location-Monitor"
pushd "$root_dir"/Agents/AC_IHMC_TA2_Location-Monitor
    ./agent.sh build
    if [[ $? -ne 0 ]]; then
        echo "Failed to build AC_IHMC_TA2_Location-Monitor container, exiting now."
        exit 1
    fi
popd
################################################################################################
################   AC_IHMC_TA2_Joint-Activity-Interdependence Agent ####################
echo "Building/updating AC_IHMC_TA2_Joint-Activity-Interdependence"
pushd "$root_dir"/Agents/AC_IHMC_TA2_Joint-Activity-Interdependence
    ./agent.sh build
    if [[ $? -ne 0 ]]; then
        echo "Failed to build AC_IHMC_TA2_Joint-Activity-Interdependence container, exiting now."
        exit 1
    fi
popd
################################################################################################
################   AC_CMUFMS_TA2_Cognitive Agent ####################
echo "Building/updating AC_CMUFMS_TA2_Cognitive"
pushd "$root_dir"/Agents/AC_CMUFMS_TA2_Cognitive
    ./agent.sh build
    if [[ $? -ne 0 ]]; then
        echo "Failed to build AC_CMUFMS_TA2_Cognitive container, exiting now."
        exit 1
    fi
popd
################################################################################################
################   AC_UCF_TA2_Flocking Agent ####################
echo "Building/updating AC_UCF_TA2_Flocking"
pushd "$root_dir"/Agents/AC_UCF_TA2_Flocking
    ./agent.sh build
    if [[ $? -ne 0 ]]; then
        echo "Failed to build AC_UCF_TA2_Flocking container, exiting now."
        exit 1
    fi
popd
################################################################################################
################   AC_UAZ_TA1_DialogAgent Agent ####################
echo "Building/updating AC_UAZ_TA1_DialogAgent"
pushd "$root_dir"/Agents/AC_UAZ_TA1_DialogAgent
    ./agent.sh build
    if [[ $? -ne 0 ]]; then
        echo "Failed to build AC_UAZ_TA1_DialogAgent container, exiting now."
        exit 1
    fi
popd
###############################################################################################
###############   ASI_DOLL_TA1_RITA ############################################################
echo "Building/updating the Doll/MIT Rita Agent"
pushd "$root_dir"/Agents/Rita_Agent
    dte=$(date "+%B-%Y-%d")
    log_dir="logs-${dte}"
    export SERVICE_LOGS_DIR="$log_dir/"
    echo "${SERVICE_LOGS_DIR} Log dirs for this instantiation of Rita"
    docker compose --env-file settings.env pull
popd
################################################################################################
#################  AC_CMU_TA1_PyGLFoVAgent ####################
echo "Building/updating AC_CMU_TA1_PyGLFoVAgent"
pushd "$root_dir"/Agents/AC_CMU_TA1_PyGLFoVAgent
    ./build.sh
    if [[ $? -ne 0 ]]; then
	    echo "Failed to build AC_CMU_TA1_PyGLFoVAgent container, exiting now."
	    exit 1
    fi
popd
################################################################################################
###############    ASI_CMU_TA1_ATLAS ##########################
echo "Building/updating ASI_CMU_TA1_ATLAS"
pushd "$root_dir"/Agents/ASI_CMU_TA1_ATLAS
    ./build.sh
    if [[ $? -ne 0 ]]; then
        echo "Failed to build ASI_CMU_TA1_ATLAS container, exiting now."
        exit 1
    fi
popd

################################################################################################
###############    AC_Cornell_TA2_Cooperation ##########################
echo "Building/updating AC_Cornell_TA2_Cooperation"
pushd "$root_dir"/Agents/AC_Cornell_TA2_Cooperation
    ./agent.sh build
    if [[ $? -ne 0 ]]; then
        echo "Failed to build AC_Cornell_TA2_Cooperation container, exiting now."
        exit 1
    fi
popd
###############    AC_Aptima_TA3_measures   ##########################
echo "Building/updating AC_Aptima_TA3_measures"
pushd "$root_dir"/Agents/AC_Aptima_TA3_measures
    ./agent.sh build
    if [[ $? -ne 0 ]]; then
        echo "Failed to build measures_agent container, exiting now."
        exit 1
    fi
popd

################  GEM  ############################################################
echo "Building/updating up the GEM Agent"
pushd "$root_dir"/Agents/AC_GALLUP_TA2_GEM
    dte=$(date "+%B-%Y-%d")
    log_dir="logs-${dte}"
    export SERVICE_LOGS_DIR="$log_dir/"
    echo "${SERVICE_LOGS_DIR} Log dirs for this instantiation of GEM"
    docker compose --env-file settings.env pull
popd
#################################################################################################

# Check amount of virtual memory and increase it if needed.
if [[ $OSTYPE == "linux-gnu" ]]; then
    virtual_memory_limit=`sysctl vm.max_map_count | cut -d' ' -f3`
    min_virtual_memory_required=262144
    if [[ $virtual_memory_limit -lt $min_virtual_memory_required ]]; then
        echo "Increasing virtual memory limit from ${virtual_memory_limit} to"
        echo "${min_virtual_memory_required}, the minimum required for the ELK stack."
        echo "(See https://www.elastic.co/guide/en/elasticsearch/reference/current/vm-max-map-count.html)"
        echo "If you want this value to persist across reboots of the computer,"
        echo "add the following line to /etc/sysctl.conf\n:"
        echo "    vm.max_map_count=262144"

	memTest=$(sudo sysctl -w vm.max_map_count=$min_virtual_memory_required 2>&1)
	if [[ $memTest == *"permission"* ]]; then
            echo "Failed to increase virtual memory, exiting now. You "\
                 "may need to run the following command as root:"
            echo ""
            echo "     sysctl -w vm.max_map_count=${min_virtual_memory_required}"
            exit 1
        fi
    fi
fi

exit 0
