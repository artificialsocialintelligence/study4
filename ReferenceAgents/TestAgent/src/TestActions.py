import paho.mqtt.client as client
import os, json
import uuid
from datetime import datetime
from DataModels import AgentTextInterventionModel, AgentBlockInterventionModel, HeaderModel, MessageModel, TextDataModel, BlockDataModel 

def runChatInterventionTest(client:client):

    with open('settings/Test_Interventions.json') as jsonTestFile:

        interventions:[] = json.load(jsonTestFile)["chat"]


    for i in interventions:

        agent_name = i['msg']['source']    
        timestamp = str(datetime.utcnow().isoformat())+'Z'    
        i['header']['timestamp'] = timestamp
        i['msg']['timestamp'] = timestamp
        i['data']['created'] = timestamp

        #player_name = inteventions[['chat']['data']['receiver']]

        client.publish("agent/intervention/" + agent_name + '/chat', json.dumps(i) )
    
        print('Sent Chat Intervention.')
    
    
   
    
    # TO BUILD INTERVENTION FROM SCRATCH
    # header = HeaderModel( str(datetime.utcnow().isoformat())+'Z', 'agent', '0.1')
    # msg = MessageModel( 'Not Set', str(datetime.utcnow().isoformat())+'Z','Not Set','Not Set', 'Intervention:Text', agent_name, '0.1' )
    # data = TextDataModel(str(datetime.utcnow().isoformat())+'Z',uuid.uuid4(),'9:30','9:15','A helpful message!',playerName,'text','Minecraft_Chat',{'explanation':'Because I said so.'})
    # agentInterventionModel = AgentTextInterventionModel(header,msg,data)   
    # client.publish("agent/intervention/" + agent_name + '/chat', agentInterventionModel.getJsonString() )

def runBlockInterventionTest(client:client):

    with open('settings/Test_Interventions.json') as jsonTestFile:

        interventions:[] = json.load(jsonTestFile)['block']
    
    for i in interventions:

        agent_name = i['msg']['source']
        timestamp = str(datetime.utcnow().isoformat())+'Z'    
        i['header']['timestamp'] = timestamp
        i['msg']['timestamp'] = timestamp
        i['data']['created'] = timestamp
        
        #player_name = i['data']['receiver']]

        client.publish("agent/intervention/" + agent_name + '/block', json.dumps(i) )

        print('Sent Block Intervention.')

    # TO BUILD INTERVENTION FROM SCRATCH
    # header = HeaderModel( str(datetime.utcnow().isoformat())+'Z', 'agent', '0.1')
    # msg = MessageModel( 'Not Set', str(datetime.utcnow().isoformat())+'Z','Not Set','Not Set', 'Intervention:Text', agent_name, '0.1' )
    # data = BlockDataModel(str(datetime.utcnow().isoformat())+'Z',uuid.uuid4(),'9:30','9:15','A helpful message!',playerName, 'block_intervention_forward',-2102, 60, 147,'text','Minecraft_Block',{'explanation':'Because I said so.'})
    # agentInterventionModel = AgentBlockInterventionModel(header,msg,data)   
    # client.publish("agent/intervention/" + agent_name + '/block', agentInterventionModel.getJsonString() )

def runMapInterventionTest(client:client):

    with open('settings/Test_Interventions.json') as jsonTestFile:

        interventions = json.load(jsonTestFile)['map']

    for i in interventions:

        agent_name = i['msg']['source']
        timestamp = str(datetime.utcnow().isoformat())+'Z'    
        i['header']['timestamp'] = timestamp
        i['msg']['timestamp'] = timestamp
        i['data']['created'] = timestamp
        
        #player_name = i['data']['receiver']]
    

        client.publish("agent/intervention/" + agent_name + '/map', json.dumps(i) )
    
        print('Sent Map Intervention.')

    # TO BUILD INTERVENTION FROM SCRATCH  
    # header = HeaderModel( str(datetime.utcnow().isoformat())+'Z', 'agent', '0.1')
    # msg = MessageModel( 'Not Set', str(datetime.utcnow().isoformat())+'Z','Not Set','Not Set', 'Intervention:Text', agent_name, '0.1' )
    # data = TextDataModel(str(datetime.utcnow().isoformat())+'Z',uuid.uuid4(),'9:30','9:15','A helpful message!',playerName,'text','Client_Map',{'explanation':'Because I said so.'})
    # agentInterventionModel = AgentTextInterventionModel(header,msg,data)   
    # client.publish("agent/intervention/" + agent_name + '/map', agentInterventionModel.getJsonString() )
   

