import json, os, time
from datetime import datetime
from MqttManager import MqttManager
from TestActions import runChatInterventionTest,runBlockInterventionTest,runMapInterventionTest
# import logging
import sys


heartbeatDict = {}
heartbeatDict["header"] = {}
heartbeatDict["header"]["timestamp"] = "Not Set"
heartbeatDict["header"]["message_type"] = "status"
heartbeatDict["header"]["version"] = "1.1"
heartbeatDict["msg"] = {}
heartbeatDict["msg"]["component"] = "Not Set"
heartbeatDict["msg"]["state"] = "ok"


def heartbeat( client, agent_name:str ):
    
    heartbeatDict["header"]["timestamp"] = str(datetime.utcnow().isoformat())+'Z'
    heartbeatDict["header"]["component"] = agent_name
    client.publish( "status/"+ agent_name +"/heartbeats", json.dumps(heartbeatDict) )

if __name__ == "__main__":

    
    #Check runtime args
    runtimeArgs = sys.argv[1:]

    if (len(runtimeArgs) == 3 ):

        mqttHost = str(runtimeArgs[0])
        mqttPort = int(runtimeArgs[1])
        triggeredByMQTT = str(runtimeArgs[2]).lower() in ['1','t','true','y','yes']

        print(mqttHost,mqttPort,triggeredByMQTT)
        
    else:
        
        print("No runtime arguments, are you running in docker?")

        try:
            mqttHost = os.getenv('mqtthost')
            mqttPort = int(os.getenv('mqttport'))
            triggeredByMQTT = str( os.getenv('triggered_by_mqtt') ).lower() in ['1','t','true','y','yes']

            print(mqttHost,mqttPort,triggeredByMQTT)
        
        
        except Exception as e:            
            print('Something went wrong reading the environment variables for mqttHost and mqttPort.')
            print('They need to be set in docker-compose.asistmod.yml, in the test_agent service. ')
            print('If you are not running in docker pass the following 3 arguments IN ORDER to the Initialize.py script like so: \"python Initialize.py <mqtthost> <mqttport> <triggeredByMQTT> \" ')
        
     
    # this uses loop_start() so it is on it's own thread and is non blocking
    print('Test Agent Inititalizing')
    
    mqttManager = MqttManager(mqttHost,mqttPort)

    if(triggeredByMQTT == False):
        
        print('Test not triggered by mqtt message, running test right away.')
        
        runChatInterventionTest(mqttManager.client)
        
        runBlockInterventionTest(mqttManager.client)
        
        runMapInterventionTest(mqttManager.client)
        

    else:
        print('Test will be triggered by sending ANY mqtt message on one of the following topics:\n"agent/intervention/test/chat_test"\n"agent/intervention/test/block_test"\n"agent/intervention/test/map_test"')

    # sends the heartbeat messages    
    while(1):
        
        try:
            
            heartbeat(mqttManager.client,'test_agent')
        
        except Exception as e:
            
            print(e)

        time.sleep(10)

    