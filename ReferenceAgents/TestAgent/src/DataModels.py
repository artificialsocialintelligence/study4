import json,os, jsonpickle, uuid
from datetime import datetime

class HeaderModel(object): 

    def __init__(self, timestamp:str, message_type:str, version:str):

        self.dict = {}

        # self.dict['timestamp'] = str(datetime.utcnow().isoformat())+'Z'

        self.dict['timestamp'] = timestamp

        self.dict['message_type'] = message_type

        self.dict['version'] = version
        
        print('HeaderModel Created')


class MessageModel(object):

    def __init__(self, trial_id:str, timestamp:str, replay_id:str, replay_root_id:str, sub_type:str, source:str, version:str):

        self.dict = {}

        self.dict['trial_id'] = trial_id
        
        # self.dict['timestamp'] = str(datetime.utcnow().isoformat())+'Z'

        self.dict['timestamp'] = timestamp
        
        self.dict['replay_id'] = replay_id
    
        self.dict['replay_root_id'] = replay_root_id
        
        self.dict['sub_type'] = sub_type

        self.dict['source'] = source
        
        self.dict['version'] = version
            
        print('MessageModel Created') 

# class MissionTime(object):

#     def __init__(self,minute,second):

#         self.dict={}
#         self.dict["minute"] = minute
#         self.dict["second"] = second     


class TextDataModel(object):

     def __init__(self, created:str, id:str, start:str, end:str, content:str, receiver:str,_type:str, renderer:str, explanation:dict):        

        self.dict = {}

        self.dict['created'] = str(datetime.utcnow().isoformat())+'Z'

        self.dict['id'] = str(uuid.uuid4()) 

        self.dict['start'] = start

        self.dict['end'] = end

        self.dict['content'] = content

        self.dict['receiver'] = receiver

        self.dict['type'] = _type

        self.dict['renderer'] = renderer

        self.dict['explanation'] = explanation
        
        print("TextInterventionModel Created : " +  self.dict['id'])


class BlockDataModel(object):

     def __init__(self,  created:str, id:str,start:str, end:str, content:str, receiver:str, block_type:str, block_x:int, block_y:int, block_z:int, _type:str, renderer:str, explanation:dict):        

        self.dict = {}

        self.dict['created'] = str(datetime.utcnow().isoformat())+'Z'

        self.dict['id'] = str(uuid.uuid4()) 

        self.dict['start'] = start

        self.dict['end'] = end

        self.dict['content'] = content

        self.dict['receiver'] = receiver

        self.dict['block_type'] = block_type

        self.dict['block_x'] = block_x

        self.dict['block_y'] = block_y

        self.dict['block_z'] = block_z

        self.dict['type'] = _type

        self.dict['renderer'] = renderer

        self.dict['explanation'] = explanation
        
        print("InterventionModel Created : " +  self.dict['id'])
# class DataModel(object):

#     def __init__(self):

#         self.dict={}
        
#         self.dict['created']= str(datetime.utcnow().isoformat())+'Z'
    
#         self.dict['interventions'] = []        

#         self.dict['explanation'] = {}

        


class AgentTextInterventionModel(object):

    def __init__(self, header:HeaderModel, msg:MessageModel, data:TextDataModel):

        self.dict={}

        self.dict['header'] = header.dict

        self.dict['msg']= msg.dict

        self.dict['data'] = data.dict
        
        print('AgentTextInterventionModel Created')
    
    # def appendIntervention(self,intervention:InterventionModel):

    #     self.dict['data']['interventions'].append(intervention.dict)
    
    def getJsonString(self)->str:        
        
        return jsonpickle.encode(self.dict)   
    
    
    
class AgentBlockInterventionModel(object):

    def __init__(self, header:HeaderModel, msg:MessageModel, data:BlockDataModel):

        self.dict={}

        self.dict['header'] = header.dict

        self.dict['msg']= msg.dict

        self.dict['data'] = data.dict
        
        print('AgentBlockInterventionModel Created')
    
    # def appendIntervention(self,intervention:InterventionModel):

    #     self.dict['data']['interventions'].append(intervention.dict)
    
    def getJsonString(self)->str:        
        
        return jsonpickle.encode(self.dict)
    

