# Reference Agent Instructions

## Building Docker container
- Navigate to the root (top level) of the MQTTPythonReferenceAgent directory, this is the same level that the dockerfile appears in
- Open a shell window and run `docker build -t aptima_reference_agent .`

## Updating Docker Container After a New Release --> Building From Source
- Pull the new version from the gitlab repository using `git pull`
- Navigate to the root (top level) of the MQTTPythonReferenceAgent directory, this is the same level that the dockerfile appears in
- Open a shell window in this directory and run `docker build -t aptima_reference_agent .`
- This build is significantly faster, as the majority of the image layers are already built

## Updating Docker Container After a New Release --> Pulling New Image 
- Open a shell window and run docker login --username foo --password bar. Replace foo and bar with your username and password.
- Then run docker pull gcr.io/asist-2130/aptima_reference_agent:latest
- This should update your current aptima_reference_agent image, correctly altering only the image layers that have changed

## Start and Stopping the Agent

- The reference agent can be manually started or stopped using `docker-compose up` and `docker-compose down` from the directory with the docker-compose file in it.

## Configuration
- You must configure the MQTT Host before using the container. THis can be done in the "ReferenceAgents\MQTTPythonReferenceAgent\ConfigFolder\config.json" file before you do a build.
 In the field labeled "host", enter the IP address of the machine running MQTT broker in quotes and
 for the field labeled "port" the port number the broker is running on as a string. 