from builtins import range
import requests
import os
import sys
import time
import io
import datetime
import json
import asyncio
import uuid
from datetime import datetime
from RawConnection import RawConnection, RawMessage

running = False
# SETTING UP MQTT CLIENT
p1 = RawConnection("Heartbeats")

# Define the MQTT topics to subscribe to
subscribe_to_topcis = ["control","trial","agent/control/rollcall/request"]

# Set some parameters
do_rollcall = True      #set to true if you want a roll call request published every nth heartbeat interval
rollcall_period = 10    #set to the number of heartbeats to skip for rollcall request
heartbeat_count = 0     #used to count the heartbeats that are published
testing_mode = False     #normally set to False.  Set to True to get published rollcall messages and more logging
trial_msg_header = {}

################################################################################
# Connection callback for when connection to MQTT broker is made
def onConnection(isConnected, rc):
    print('Connected? {0} ({1})'.format(isConnected, rc))

################################################################################
# Message callback
# this is the function that handles all of the messages that are subscribed to
def onMessage(message):
    try:
        global running
        if testing_mode: print(f'onMessage callback: {message}')
        jsonDict = message.jsondata
        messageType = jsonDict["header"]["message_type"]
        if testing_mode: print(f'message type: {messageType}')
        if (messageType == "trial"):
            msg = jsonDict["msg"]
            sub_type = msg["sub_type"]
            if sub_type == "start":
              print(f'Received start trial message')
              global trial_msg_header
              trial_msg_header['experiment_id'] = msg['experiment_id']
              trial_msg_header['trial_id'] = msg['trial_id']
              trial_msg_header['replay_parent_type'] = msg['replay_parent_type'] if msg['replay_parent_type'] is not None else None
              trial_msg_header['replay_parent_id'] = msg['replay_parent_id'] if msg['replay_parent_id'] is not None else None
              trial_msg_header['replay_id'] = msg['replay_id'] if msg['replay_id'] is not None else None
              #publish agent version message
              publish_agent_version_message()
              running = True
            elif sub_type == "stop":
              print(f'Received stop trial message')
              running = False

        else:
            #handle the rollcall request message
            if messageType == "agent":
                jsonDict = message.jsondata
                msg = jsonDict["msg"]
                sub_type = msg["sub_type"]
                if sub_type == "rollcall:request":
                    publish_rollcall_response(message.jsondata["data"]["rollcall_id"])
                    print(f'Received rollcall response message')
       # add additional message type checks here
    except Exception as ex:
        print(ex)
        print('RX Error, topic = {0}'.format(message.key))

################################################################################
# Add common headers
# Generate the structure and properties for the common header section of the message
def addCommonHeader(jsonDict: dict):
    jsonDict["header"] = {}
    jsonDict["header"]["timestamp"] = str(datetime.utcnow().isoformat())+'Z'
    jsonDict["header"]["message_type"] = "agent"
    jsonDict["header"]["version"] = "0.6"    

################################################################################
# publish the agent version message
def publish_agent_version_message():
    ver_msg = {}
    addCommonHeader(ver_msg)
    ver_msg["msg"] = trial_msg_header
    ver_msg["msg"]["timestamp"] = str(datetime.utcnow().isoformat())+'Z'
    ver_msg["msg"]["source"] = "aptima_reference_agent"
    ver_msg["msg"]["sub_type"] = "versioninfo"
    ver_msg["msg"]["version"] = "0.1"
    data = {}
    data["agent_name"] = "AptimaRefereneAgent"
    data["version"] = "0.1-2"
    data["owner"] = "Aptima"
    data["config"] = [{"name": "agent_name", "value": "reference agent"},{"name": "purpose", "value": "testing"}]
    data["source"] = ["https://gitlab.asist.aptima.com/asist/testbed/-/tree/develop/ReferenceAgents/MQTTPythonReferenceAgent"]
    data["dependencies"] = ["dependency1", "dependency2"]
    data["publishes"] = [{"topic": "agent/aptimareferenceagent", "message_type": "status", "sub_type": "heartbeats"}]
    data["subscribes"] = [{"topic": "trial", "message_type": "trial", "sub_type": "start/stop"}]
    ver_msg["data"] = data
    p1.publish(RawMessage("agent/aptimareferenceAgent/versioninfo", jsondata=ver_msg))
    if testing_mode: print(ver_msg)

################################################################################
# publish a rollcall request message
def publish_rollcall_request():
    rollcall_request = {}
    addCommonHeader(rollcall_request)
    rollcall_request["msg"] = {}
    rollcall_request["msg"] = trial_msg_header
    rollcall_request["msg"]["timestamp"] = str(datetime.utcnow().isoformat())+'Z'
    rollcall_request["msg"]["source"] = "testbed"
    rollcall_request["msg"]["sub_type"] = "rollcall:request"
    rollcall_request["msg"]["version"] = "0.1"
    rollcall_request["data"] = {}
    rollcall_request["data"]["rollcall_id"] = str(uuid.uuid4())
    p1.publish(RawMessage("agent/control/rollcall/request",jsondata=rollcall_request))

################################################################################
# publish a rollcall response message
def publish_rollcall_response(rollcall_id):
    rc_resp = {}
    addCommonHeader(rc_resp)
    rc_resp["msg"] = {}
    rc_resp["msg"] = trial_msg_header
    rc_resp["msg"]["timestamp"] = str(datetime.utcnow().isoformat())+'Z'
    rc_resp["msg"]["source"] = "aptima_reference_agent"
    rc_resp["msg"]["sub_type"] = "rollcall:response"
    rc_resp["msg"]["version"] = "0.1"
    rc_resp["data"] = {}
    rc_resp["data"]["rollcall_id"] = rollcall_id
    rc_resp["data"]["version"] = "0.1-2"
    rc_resp["data"]["status"] = "up"
    rc_resp["data"]["uptime"] = (datetime.now() - agent_start_time).total_seconds() 
    p1.publish(RawMessage("agent/control/rollcall/response",jsondata=rc_resp))

################################################################################
# Asynch function to publish a heartbeat message periodically
async def publishHeartbeatLoop(seconds):
  global heartbeat_count
  while(True):
    #only generate hearbeat messages while the trial is running
    if running:
        jsonDict = {}
        addCommonHeader(jsonDict)
        jsonDict["msg"] = {}
        jsonDict["msg"]["sub_type"] = "heartbeat"
        jsonDict["msg"]["source"] = "agent:aptima_reference_gent"
        jsonDict["msg"]["timestamp"] = str(datetime.utcnow().isoformat())+'Z'
        jsonDict["msg"]["version"] = "0.3"
        jsonDict["data"] = {}
        jsonDict["data"]["state"] = "ok"
        jsonDict["data"]["active"] = True
        p1.publish(RawMessage("status/aptimareferenceagent/heartbeats", jsondata=jsonDict))
        #print(jsonDict)
        if do_rollcall and heartbeat_count % rollcall_period == 0 and testing_mode:
            publish_rollcall_request()
        heartbeat_count += 1
    await asyncio.sleep(seconds)
    # we never return as we want this to run indefinitely

################################################################################
# function to setup the asynch heartbeat message publications
def heartBeatLoop():  
    loop = asyncio.get_event_loop()
    asyncio.set_event_loop(loop)
    asyncio.ensure_future(publishHeartbeatLoop(10))
    loop.run_forever()

# get the time the agent started up
agent_start_time = datetime.now()
# Set up callbacks
p1.onConnectionStateChange = onConnection
p1.onMessage = onMessage

# Connect to the MQTT broker
p1.connect()

#subscribe to all of the topics that are needed
for t in subscribe_to_topcis:
    p1.subscribe(t)

#start up the hearbeat messages
heartBeatLoop() 

# Don't put any code here, it will not be executed because of the heartbeat loop
